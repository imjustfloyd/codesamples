# Introduction #

In the master branch above you will see three directories. These directories relate to the projects that I have worked on over four years ago. 

In the IPHIS(Integrated Plant Health Inspection System) directory you will find the code for two of the larger pieces of work I have contributed to on that particular project. 
*for more information about what IPHIS is please go to:* https://www.aphis.usda.gov/aphis/ourfocus/planthealth/pest-detection/sa_iphis 

In the CARPOL(Certificates, Accreditations,Registrations, Permits, and OtherLicenses )directory you will find a 'CarpolResources' directory which contains the static resources for the CARPOLUI. If you are curious I have added a copy of my local CARPOL repo with all of the code for that application. If you are curious most of my work can be found under the components, and pages directories. The UI design and concept was derived from the USDA APHIS Website design. 
*for more information about what CARPOL is please review:* https://www.aphis.usda.gov/biotechnology/downloads/Meetings/2014_sh_mtg/sh14_efile.pdf

The Talk Stream Live UI Directory is just the static UI that I did for a client. Which is currently under development. 

## Project - IPHIS - Spring MVC, Hibernate:##

Click on the tabs labeled "Locations\ Work Book" and "Assignments" .  
This will be a demo of the larger functionalities that i have done within this project. We have not won the award for UI. So please don't judge me on the look and feel. This UI was designed in the 90's . We have been able to implement some modern items here and there but not many.

## Last Project - CARPOL - Custom UI/UX - Within Salesforce: ##

The static version of the UI is available in the Themes directory. 
```
#!html

/ CarpolResources / carpol / themes / neopix / build / catalog.html
```

I will have to do a conference call with you in order to demo the actual UI in the application. 

## Most Recent Project Under Black Box Innovation Technologies ##

New UI Design for http://www.talkstreamlive.com
In the repo you can download the directory and demo the ui in your browser. 

## Old sites that were built in between 2011-2013 ##
**These sites are built within Concrete5 which is PHP.** 

1.  http://www.gqsi.net 

**Site for my company that I never launched.** 

* http://test.blackboxitech.com 

**Live Site is under construction**

* http://www.blackboxitech.com