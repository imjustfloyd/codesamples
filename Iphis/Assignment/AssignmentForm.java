package gov.usda.aphis.iphis.ui.form;

import gov.usda.aphis.iphis.ui.view.ViewConstants;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author justinfloyd
 */
public class AssignmentForm
{
	private final int MAX_COMMENT_LENGTH = 1000;
	
	private Long assignmentID;
	@NotNull
	private List<Long> workbooks;
	@NotNull
	private List<Long> templateIDs;
	@NotEmpty
	private String assignmentName;
	@Size(max = MAX_COMMENT_LENGTH)
	private String comments;
	@DateTimeFormat(pattern = ViewConstants.DATE_FORMAT)
	@NotNull
	private Date startDate;
	@DateTimeFormat(pattern = ViewConstants.DATE_FORMAT)
	private Date endDate;
	
	// Recurrence Pattern Type
	private boolean recurring;
	private RecurrencePattern recurrencePatten;
	
	// ** DAILY
	private int dailyDays;
	
	// ** WEEKLY
	private boolean weeklySunday;
	private boolean weeklyMonday;
	private boolean weeklyTuesday;
	private boolean weeklyWednesday;
	private boolean weeklyThursday;
	private boolean weeklyFriday;
	private boolean weeklySaturday;
	
	// ** Monthly
	private int monthlyDate;
	private boolean monthlyLast;
	
	public AssignmentForm()
	{
		// set defaults for this form
		setDailyDays(1);
		setMonthlyDate(1);
	}
	
	public Long getAssignmentID()
	{
		return assignmentID;
	}
	
	public void setAssignmentID(Long assignmentID)
	{
		this.assignmentID = assignmentID;
	}
	
	public List<Long> getWorkbooks()
	{
		return workbooks;
	}
	
	public void setWorkbooks(List<Long> workbooks)
	{
		this.workbooks = workbooks;
	}
	
	public String getAssignmentName()
	{
		return assignmentName;
	}
	
	public void setAssignmentName(String assignmentName)
	{
		this.assignmentName = assignmentName;
	}
	
	public List<Long> getTemplateIDs()
	{
		return templateIDs;
	}
	
	public void setTemplateIDs(List<Long> templateIDs)
	{
		this.templateIDs = templateIDs;
	}
	
	public String getComments()
	{
		return comments;
	}
	
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	
	public Date getStartDate()
	{
		return startDate;
	}
	
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}
	
	public Date getEndDate()
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}
	
	public boolean isRecurring()
	{
		return recurring;
	}
	
	public void setRecurring(boolean recurring)
	{
		this.recurring = recurring;
	}
	
	public RecurrencePattern getRecurrencePatten()
	{
		return recurrencePatten;
	}
	
	public void setRecurrencePatten(RecurrencePattern recurrencePatten)
	{
		this.recurrencePatten = recurrencePatten;
	}
	
	public int getDailyDays()
	{
		return dailyDays;
	}
	
	public void setDailyDays(int dailyDays)
	{
		this.dailyDays = dailyDays;
	}
	
	public boolean isWeeklySunday()
	{
		return weeklySunday;
	}
	
	public void setWeeklySunday(boolean weeklySunday)
	{
		this.weeklySunday = weeklySunday;
	}
	
	public boolean isWeeklyMonday()
	{
		return weeklyMonday;
	}
	
	public void setWeeklyMonday(boolean weeklyMonday)
	{
		this.weeklyMonday = weeklyMonday;
	}
	
	public boolean isWeeklyTuesday()
	{
		return weeklyTuesday;
	}
	
	public void setWeeklyTuesday(boolean weeklyTuesday)
	{
		this.weeklyTuesday = weeklyTuesday;
	}
	
	public boolean isWeeklyWednesday()
	{
		return weeklyWednesday;
	}
	
	public void setWeeklyWednesday(boolean weeklyWednesday)
	{
		this.weeklyWednesday = weeklyWednesday;
	}
	
	public boolean isWeeklyThursday()
	{
		return weeklyThursday;
	}
	
	public void setWeeklyThursday(boolean weeklyThursday)
	{
		this.weeklyThursday = weeklyThursday;
	}
	
	public boolean isWeeklyFriday()
	{
		return weeklyFriday;
	}
	
	public void setWeeklyFriday(boolean weeklyFriday)
	{
		this.weeklyFriday = weeklyFriday;
	}
	
	public boolean isWeeklySaturday()
	{
		return weeklySaturday;
	}
	
	public void setWeeklySaturday(boolean weeklySaturday)
	{
		this.weeklySaturday = weeklySaturday;
	}
	
	public int getMonthlyDate()
	{
		return monthlyDate;
	}
	
	public void setMonthlyDate(int monthlyDate)
	{
		this.monthlyDate = monthlyDate;
	}
	
	public boolean isMonthlyLast()
	{
		return monthlyLast;
	}
	
	public void setMonthlyLast(boolean monthlyLast)
	{
		this.monthlyLast = monthlyLast;
	}
	
	public enum RecurrencePattern
	{
		DAILY, WEEKLY, MONTHLY;
	}
}
