package gov.usda.aphis.iphis.ui.model.assignments;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class AssignmentEventJson
{
	private long id;
	private String title;
	@DateTimeFormat(pattern = "yyyy-MM-DD")
	private Date start;
	@DateTimeFormat(pattern = "yyyy-MM-DD")
	private Date end;
	private String description;
	private Boolean allDay;
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public Date getStart()
	{
		return start;
	}
	
	public void setStart(Date start)
	{
		this.start = start;
	}
	
	public Date getEnd()
	{
		return end;
	}
	
	public void setEnd(Date end)
	{
		this.end = end;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	public Boolean getAllDay()
	{
		return allDay;
	}
	
	public void setAllDay(Boolean allDay)
	{
		this.allDay = allDay;
	}
	
}
