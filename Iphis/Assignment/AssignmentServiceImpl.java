 package gov.usda.aphis.iphis.service.assignment;

import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentTemplateDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentWorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.PhisTemplateDTO;
import gov.usda.aphis.iphis.data.dto.web.PhisUserDTO;
import gov.usda.aphis.iphis.data.web.dao.AssignmentDAO;
import gov.usda.aphis.iphis.domain.assignment.Assignment;
import gov.usda.aphis.iphis.domain.assignment.AssignmentTemplate;
import gov.usda.aphis.iphis.domain.workbook.Workbook;
import gov.usda.aphis.iphis.schedule.Schedule;
import gov.usda.aphis.iphis.service.OfficeService;
import gov.usda.aphis.iphis.service.TemplateService;
import gov.usda.aphis.iphis.util.GenericObjectWrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AssignmentServiceImpl implements AssignmentService
{
	private GenericObjectWrapper<Assignment, AssignmentDTO> wrapper;
	
	@Autowired
	private AssignmentDAO assignmentDAO;
	
	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private TemplateService templateService;
	
	public AssignmentServiceImpl()
	{
		wrapper = new GenericObjectWrapper<Assignment, AssignmentDTO>(Assignment.class, AssignmentDTO.class);
	}
	
	@Transactional
	@Override
	public Assignment getAssignmentByID(long assignmentID)
	{
		AssignmentDTO assignmentDTO = assignmentDAO.getByID(assignmentID);
		if (null != assignmentDTO)
		{
			Hibernate.initialize(assignmentDTO.getTemplates());
			Hibernate.initialize(assignmentDTO.getWorkbooks());
			return new Assignment(assignmentDTO);
		}
		else
		{
			return null;
		}
	}
	
	@Override
	@Transactional
	public List<Assignment> getAssignmentsByOfficeID(long officeID)
	{
		List<AssignmentDTO> assignmentDTOs = assignmentDAO.retrieveAssignmentsByOfficeID(officeID);
		return wrapper.wrap(assignmentDTOs);
	}
	
	@Override
	@Transactional
	public List<Assignment> getAssignmentsByTimePeriodAndOfficeID(long officeID, final Date startDate,
			final Date endDate)
	{
		List<AssignmentDTO> assignmentDTOs = assignmentDAO.retrieveAssignmentsByTimePeriodAndOfficeID(officeID,
				startDate, endDate);
		return wrapper.wrap(assignmentDTOs);
	}
	
	@Override
	@Transactional
	public Assignment getAssignmentByOfficeID(long officeID)
	{
		AssignmentDTO assignmentDTO = assignmentDAO.getByOfficeID(officeID);
		if (null != assignmentDTO)
		{
			return new Assignment(assignmentDTO);
		}
		else
		{
			return null;
		}
	}
	
	@Override
	@Transactional
	public Long saveAssignment(Long assignmentID, Long officeID, String assignmentName, Date startDate, Date endDate,
			List<Long> workbooks, String comments, Long userID, List<Long> templateIDs, Schedule schedule)
	{
		Assignment assignment = null;
		// get Assignment ID
		if (null != assignmentID)
		{
			assignment = getAssignmentByID(assignmentID);
		}
		else
		{
			if (null == assignment)
			{
				assignment = new Assignment();
				assignment.getAssignmentDTO().setActive(true);
			}
		}
		assignment.getAssignmentDTO().setOffice(officeService.getOffice(officeID).getOfficeDTO());
		assignment.getAssignmentDTO().setStartDate(startDate);
		assignment.getAssignmentDTO().setAssignmentName(assignmentName);
		assignment.getAssignmentDTO().setEndDate(endDate);
		assignment.getAssignmentDTO().setComments(comments);
		assignment.getAssignmentDTO().setSchedule(schedule);
		
		// set modified by to current user
		PhisUserDTO user = new PhisUserDTO();
		user.setUserID(userID);
		assignment.getAssignmentDTO().setModifiedBy(user);
		
		// set modified time by current time
		assignment.getAssignmentDTO().setModifiedDate(new Date());
		
		// If new Assignment, set createdBy and created date to the current user and the current time respectively
		if (null == assignmentID)
		{
			assignment.getAssignmentDTO().setCreatedBy(user);
			assignment.getAssignmentDTO().setCreatedDate(new Date());
			
		}
		
		if (workbooks != null)
		{
			List<AssignmentWorkbookDTO> workbookList = new ArrayList<AssignmentWorkbookDTO>();
			for (Long id : workbooks)
			{
				WorkbookDTO workbookDTO = new WorkbookDTO();
				workbookDTO.setWorkbookID(id);
				AssignmentWorkbookDTO assignmentWorkbookDTO = new AssignmentWorkbookDTO();
				assignmentWorkbookDTO.setAssignment(assignment.getAssignmentDTO());
				assignmentWorkbookDTO.setWorkbook(workbookDTO);
				workbookList.add(assignmentWorkbookDTO);
			}
			if (null != assignment.getAssignmentDTO().getWorkbooks())
			{
				assignment.getAssignmentDTO().getWorkbooks().clear();
				assignment.getAssignmentDTO().getWorkbooks().addAll(workbookList);
			}
			else
			{
				assignment.getAssignmentDTO().setWorkbooks(workbookList);
			}
			
		}
		if (templateIDs != null)
		{
			List<AssignmentTemplateDTO> templateList = new ArrayList<AssignmentTemplateDTO>();
			for (Long templateID : templateIDs)
			{
				PhisTemplateDTO phisTemplateDTO = new PhisTemplateDTO();
				phisTemplateDTO.setTemplateID(templateID);
				AssignmentTemplateDTO assignmentTemplateDTO = new AssignmentTemplateDTO();
				assignmentTemplateDTO.setAssignmentID(assignment.getAssignmentDTO());
				assignmentTemplateDTO.setTemplateID(phisTemplateDTO);
				templateList.add(assignmentTemplateDTO);
			}
			if (null != assignment.getAssignmentDTO().getTemplates())
			{
				assignment.getAssignmentDTO().getTemplates().clear();
				assignment.getAssignmentDTO().getTemplates().addAll(templateList);
			}
			else
			{
				assignment.getAssignmentDTO().setTemplates(templateList);
			}
			
		}
		
		Long newAssignmentID = assignmentDAO.merge(assignment.getAssignmentDTO()).getAssignmentID();
		return newAssignmentID;
	}
	
	@Override
	@Transactional
	public List<AssignmentTemplate> getTreatmentTemplateList(final long assignmentID)
	{
		GenericObjectWrapper<AssignmentTemplate, AssignmentTemplateDTO> assignmentTemplateWrapper = new GenericObjectWrapper<AssignmentTemplate, AssignmentTemplateDTO>(
				AssignmentTemplate.class, AssignmentTemplateDTO.class);
		return assignmentTemplateWrapper.wrap(assignmentDAO.getTreatmentTemplateList(assignmentID));
	}
	
	@Override
	@Transactional
	public void deleteAssignmentByID(long assignmentID)
	{
		assignmentDAO.deleteByID(assignmentID);
		
	}
	
	@Override
	@Transactional
	public List<Workbook> getWorkBookList(long assignmentID)
	{
		GenericObjectWrapper<Workbook, WorkbookDTO> assignmentTemplateWrapper = new GenericObjectWrapper<Workbook, WorkbookDTO>(
				Workbook.class, WorkbookDTO.class);
		return assignmentTemplateWrapper.wrap(assignmentDAO.getWorkBookList(assignmentID));
	}
	
	@Override
	public List<Date> getAssignmentDates(final AssignmentDTO assignmentDTO, Date rangeStartDate, Date rangeEndDate)
	{
		List<Date> assignmentDates = null;
		Schedule assignmentSchedule = assignmentDTO.getSchedule();
		if (assignmentSchedule != null)
		{
			List<LocalDate> assignmentLocalDates = assignmentSchedule.getDates(new LocalDate(rangeStartDate),
					new LocalDate(rangeEndDate));
			if (assignmentLocalDates != null && assignmentLocalDates.size() > 0)
			{
				assignmentDates = new ArrayList<Date>();
				for (LocalDate assignmentLocalDate : assignmentLocalDates)
				{
					assignmentDates.add(assignmentLocalDate.toDateTimeAtStartOfDay().toDate());
				}
			}
		}
		return assignmentDates;
	}
}
