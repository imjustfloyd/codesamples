package gov.usda.aphis.iphis.ui.controller.assignment;

import gov.usda.aphis.iphis.domain.ActiveUser;
import gov.usda.aphis.iphis.domain.assignment.Assignment;
import gov.usda.aphis.iphis.domain.web.PhisSurveyTemplate;
import gov.usda.aphis.iphis.domain.workbook.Workbook;
import gov.usda.aphis.iphis.schedule.Schedule;
import gov.usda.aphis.iphis.service.SurveyTemplateService;
import gov.usda.aphis.iphis.service.assignment.AssignmentService;
import gov.usda.aphis.iphis.service.workbook.WorkbookService;
import gov.usda.aphis.iphis.ui.UiUtils;
import gov.usda.aphis.iphis.ui.form.AssignmentForm;
import gov.usda.aphis.iphis.ui.form.validator.AssignmentValidator;
import gov.usda.aphis.iphis.ui.model.*;
import gov.usda.aphis.iphis.ui.model.assignments.AssignmentEventJson;
import gov.usda.aphis.iphis.ui.util.AssignmentScheduleConverter;
import gov.usda.aphis.iphis.ui.view.Tiles;
import gov.usda.aphis.iphis.util.GenericObjectWrapper;
import org.dozer.Mapper;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/assignments")
public class AssignmentController
{
	public static final String ASSIGNMENT_FORM = "assignmentForm";
	public static final String WORKBOOKS = "workbooks";
	public static final String TEMPLATES = "templates";
	public static final String ASSIGNMENT = "assignment";
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	
	@Autowired
	private AssignmentService assignmentService;
	
	@Autowired
	private Mapper mapper;
	
	@Autowired
	private WorkbookService workbookService;
	
	@Autowired
	private SurveyTemplateService templateService;
	
	@Autowired
	private MessageSource messageSource;
	
	@InitBinder(ASSIGNMENT_FORM)
	protected void initBinder(final WebDataBinder binder)
	{
		binder.setValidator(new AssignmentValidator());
	}
	
	// Add a day or days to a calendar date.
	private Date addDays(Date date, int days)
	{
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}
	
	@RequestMapping(value = "/manage.htm")
	private String getAssignmentsForm(ModelMap model, final ActiveUser activeUser)
	{
		AssignmentForm assignmentForm = new AssignmentForm();
		
		// retrieve the phis user details
		List<Assignment> assignmentList = assignmentService.getAssignmentsByOfficeID(activeUser.getSelectedOfficeID());
		
		// get the list of templates by the user selected office ID
		List<PhisSurveyTemplate> templates = templateService.getSystemOfficeTreatmentTemplates(activeUser
				.getSelectedOfficeID());
		GenericObjectWrapper<PhisSurveyTemplateView, PhisSurveyTemplate> templateWrapper = new GenericObjectWrapper<PhisSurveyTemplateView, PhisSurveyTemplate>(
				PhisSurveyTemplateView.class, PhisSurveyTemplate.class);
		
		// get the list of workbooks by the user selected office ID
		List<Workbook> allWorkbooks = workbookService.getWorkbooksByOffice(activeUser.getSelectedOfficeID(),false, false);
		GenericObjectWrapper<WorkbookDetailView, Workbook> workbookWrapper = new GenericObjectWrapper<WorkbookDetailView, Workbook>(
				WorkbookDetailView.class, Workbook.class);
		model.addAttribute("assignmentList", new GenericObjectWrapper<AssignmentView, Assignment>(AssignmentView.class,
				Assignment.class).wrap(assignmentList));
		model.addAttribute(ASSIGNMENT_FORM, assignmentForm);
		List<PhisSurveyTemplateView> templateViewList = templateWrapper.wrap(templates);
		Collections.sort(templateViewList, new Comparator<PhisSurveyTemplateView>()
		{
			@Override
			public int compare(PhisSurveyTemplateView o1, PhisSurveyTemplateView o2)
			{
				return o1.getSurveyTemplateName().compareTo(o2.getSurveyTemplateName());
			}
		});
		List<WorkbookDetailView> workBookList = workbookWrapper.wrap(allWorkbooks);
		Collections.sort(workBookList, new Comparator<WorkbookDetailView>()
		{
			@Override
			public int compare(WorkbookDetailView o1, WorkbookDetailView o2)
			{
				return o1.getWorkbookName().compareTo(o2.getWorkbookName());
			}
		});
		model.addAttribute(TEMPLATES, templateViewList);
		model.addAttribute(WORKBOOKS, workBookList);
		return Tiles.ASSIGNMENTS_MANAGE;
	}
	
	@RequestMapping(value = "/getAssignments.htm", method = RequestMethod.GET)
	public @ResponseBody
	List<AssignmentEventJson> getAssignmentList(
			@RequestParam("start") @DateTimeFormat(pattern = DATE_PATTERN) final Date start,
			@RequestParam("end") @DateTimeFormat(pattern = DATE_PATTERN) final Date end, final ActiveUser activeUser)
	{
		// retrieve the assignments based on the office id and the dates
		List<Assignment> assignmentList = assignmentService.getAssignmentsByTimePeriodAndOfficeID(
				activeUser.getSelectedOfficeID(), start, end);
		
		// holds the list of assignment events for the calendar to display
		List<AssignmentEventJson> resultList = new ArrayList<AssignmentEventJson>();
		
		// for each of the assignments
		for (Assignment assignment : assignmentList)
		{
			// retrieve the schedule for this assignment
			Schedule schedule = assignment.getSchedule();
			
			// check to see if this schedule is not null
			if (null != schedule)
			{
				// retrieve the dates that this schedule is active between the
				// start and end dates
				List<LocalDate> dates = schedule.getDates(new LocalDate(start), new LocalDate(end));
				
				// for each of the dates
				for (LocalDate date : dates)
				{
					AssignmentEventJson response = new AssignmentEventJson();
					response.setId(assignment.getAssignmentID());
					response.setTitle(assignment.getAssignmentName());
					response.setStart(date.toDateMidnight().toDate());
					response.setEnd(date.toDateMidnight().toDate());
					response.setDescription(assignment.getComments());
					response.setAllDay(true);
					resultList.add(response);
				}
			}
			else
			{
				AssignmentEventJson response = new AssignmentEventJson();
				Date startDate = assignment.getStartDate();
				Date endDate = assignment.getEndDate();
				Date myDate = endDate;
				response.setId(assignment.getAssignmentID());
				response.setTitle(assignment.getAssignmentName());
				response.setStart(assignment.getStartDate());
				response.setAllDay(true);
				response.setDescription(assignment.getComments());
				// if end date is null or startdate and endate are the same then the assignment is an all day event
				// which means that the date is already inclusive.
				// otherwise the assignment is a long event and to solve the problem with the long event being exclusive
				// in the fullcalendar.io plugin.
				// We need to add a day to the endate to make the endate inclusive.
				if (endDate == null || startDate == endDate)
				{
					response.setEnd(endDate);
				}
				else
				{
					myDate = addDays(myDate, 1);
					response.setEnd(myDate);
				}
				resultList.add(response);
			}
		}
		
		return resultList;
	}
	
	@RequestMapping(value = "/save.htm", method = RequestMethod.POST)
	private @ResponseBody
	AjaxFormResponse<AssignmentView> saveAssignment(
			@ModelAttribute(ASSIGNMENT_FORM) @Valid AssignmentForm assignmentForm, final BindingResult result,
			final ActiveUser activeUser)
	{
		// check for errors
		if (!result.hasErrors())
		{
			// holds the recurring schedule object
			Schedule schedule;
			
			// check to see the assignment is recurring
			if (assignmentForm.isRecurring())
			{
				// build the recurring schedule object from the form
				schedule = AssignmentScheduleConverter.toSchedule(assignmentForm);
			}
			else
			{
				// there is no recurring schedule
				schedule = null;
			}
			
			// save assignment
			Long newAssignmentID = assignmentService.saveAssignment(assignmentForm.getAssignmentID(),
					activeUser.getSelectedOfficeID(), assignmentForm.getAssignmentName(),
					assignmentForm.getStartDate(), assignmentForm.getEndDate(), assignmentForm.getWorkbooks(),
					assignmentForm.getComments(), activeUser.getUserID(), assignmentForm.getTemplateIDs(), schedule);
			
			// set the created assignment to response object
			return new AjaxFormResponse<AssignmentView>(true, null, new AssignmentView(
					assignmentService.getAssignmentByID(newAssignmentID)), null);
		}
		else
		{
			List<FieldErrorMessage> fieldErrorMessages = new ArrayList<FieldErrorMessage>();
			for (FieldError fieldError : result.getFieldErrors())
			{
				fieldErrorMessages.add(UiUtils.makeFieldErrorMessage(fieldError, messageSource));
			}
			return new AjaxFormResponse<AssignmentView>(false, null, null, fieldErrorMessages);
		}
	}
	
	@RequestMapping(value = "/{assignmentID}/update.htm", method = RequestMethod.POST)
	private @ResponseBody
	AjaxFormResponse<AssignmentView> updateAssignment(
			@ModelAttribute(ASSIGNMENT_FORM) @PathVariable("assignmentID") final long assignmentID,
			@Valid AssignmentForm assignmentForm, final BindingResult result, final ActiveUser activeUser)
	{
		// check for errors
		if (!result.hasErrors())
		{
			// holds the recurring schedule object
			Schedule schedule;
			
			// check to see the assignment is recurring
			if (assignmentForm.isRecurring())
			{
				// build the recurring schedule object from the form
				schedule = AssignmentScheduleConverter.toSchedule(assignmentForm);
			}
			else
			{
				// there is no recurring schedule
				schedule = null;
			}
			
			// save assignment
			Long newAssignmentID = assignmentService.saveAssignment(assignmentForm.getAssignmentID(),
					activeUser.getSelectedOfficeID(), assignmentForm.getAssignmentName(),
					assignmentForm.getStartDate(), assignmentForm.getEndDate(), assignmentForm.getWorkbooks(),
					assignmentForm.getComments(), activeUser.getUserID(), assignmentForm.getTemplateIDs(), schedule);
			
			// set the created assignment to response object
			return new AjaxFormResponse<AssignmentView>(true, null, new AssignmentView(
					assignmentService.getAssignmentByID(newAssignmentID)), null);
		}
		else
		{
			List<FieldErrorMessage> fieldErrorMessages = new ArrayList<FieldErrorMessage>();
			for (FieldError fieldError : result.getFieldErrors())
			{
				fieldErrorMessages.add(UiUtils.makeFieldErrorMessage(fieldError, messageSource));
			}
			return new AjaxFormResponse<AssignmentView>(false, null, null, fieldErrorMessages);
		}
	}
	
	@RequestMapping(value = "/{assignmentID}/edit.htm", method = RequestMethod.GET)
	private String editAssignmentForm(@PathVariable("assignmentID") final long assignmentID,
			AssignmentForm assignmentForm, ModelMap model, final ActiveUser activeUser)
	{
		// build the assignment form from the assignment that was loaded out of
		// the database
		Assignment assignment = assignmentService.getAssignmentByID(assignmentID);
		assignmentForm = mapper.map(assignment, AssignmentForm.class);
		// retrieve the schedule for this assignment
		Schedule schedule = assignment.getSchedule();
		
		// check to see if this schedule is not null
		if (null != schedule)
		{
			// update the form with the schedule information
			AssignmentScheduleConverter.buildAssignmentForm(assignmentForm, schedule);
		}
		
		// get the list of published templates by the user selected office ID
		List<PhisSurveyTemplate> templates = templateService.getSystemOfficeTreatmentTemplates(activeUser
				.getSelectedOfficeID());
		GenericObjectWrapper<PhisSurveyTemplateView, PhisSurveyTemplate> templateWrapper = new GenericObjectWrapper<PhisSurveyTemplateView, PhisSurveyTemplate>(
				PhisSurveyTemplateView.class, PhisSurveyTemplate.class);
		
		// get the list of workbooks by the user selected office ID
		List<Workbook> allWorkbooks = workbookService.getWorkbooksByOffice(activeUser.getSelectedOfficeID(),false, false);
		GenericObjectWrapper<WorkbookView, Workbook> workbookWrapper = new GenericObjectWrapper<WorkbookView, Workbook>(
				WorkbookView.class, Workbook.class);
		List<PhisSurveyTemplateView> templateViewList = templateWrapper.wrap(templates);
		Collections.sort(templateViewList, new Comparator<PhisSurveyTemplateView>()
		{
			@Override
			public int compare(PhisSurveyTemplateView o1, PhisSurveyTemplateView o2)
			{
				return o1.getSurveyTemplateName().compareTo(o2.getSurveyTemplateName());
			}
		});
		List<WorkbookView> workBookList = workbookWrapper.wrap(allWorkbooks);
		Collections.sort(workBookList, new Comparator<WorkbookView>()
		{
			@Override
			public int compare(WorkbookView o1, WorkbookView o2)
			{
				return o1.getWorkbookName().compareTo(o2.getWorkbookName());
			}
		});
		model.addAttribute(ASSIGNMENT_FORM, assignmentForm);
		model.addAttribute(TEMPLATES, templateViewList);
		model.addAttribute(WORKBOOKS, workBookList);
		
		return Tiles.EDIT_ASSIGNMENT;
	}
	
	@RequestMapping(value = "/{assignmentID}/delete.htm")
	private String deleteAssignment(@PathVariable("assignmentID") final long assignmentID)
	{
		// delete assignment
		assignmentService.deleteAssignmentByID(assignmentID);
		return "redirect:/assignments/manage.htm";
	}
}
