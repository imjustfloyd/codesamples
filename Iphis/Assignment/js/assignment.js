$(document).ready(function() {
	$('#templateIDs').multipleSelect({
		filter : true,
		placeholder : "Select One or more Templates",
		width : '100%'
	});
	$('#workbooks').multipleSelect({
		filter : true,
		placeholder : "Select One or more Workbooks",
		width : '100%'
	});
	$("input#startDate").attr('readonly', 'readonly');
	$("input#endDate").datepicker();

	function updateAssignment() {
		location.reload();
	}
	$("#assignmentTable").dataTable({
		"bFilter" : true,
		"autoWidth" : false,
		"bLengthChange" : false,
		"bPaginate" : true,
		"info" : true,
		"sPaginationType" : "input",
		"oColVis" : {
			"aiExclude" : [ 2 ]
		}
	});
	$('input#cancel').click(function() {
		$.magnificPopup.close();
	});
	$('.error').hide();
	$("#messageBox").hide();
	$('#addAssignmentForm').ajaxForm({
		beforeSubmit : function() {
			$('#addAssignmentForm').mask("Saving...");
			$('.error').hide();
			$('.formField').css('border', '1px solid #2088B6');
		},
		success : function(data) {
			if (data.successful == true) {
				try {
					$("#messageBox").hide();
					$.magnificPopup.close();
					updateAssignment(data.responseObject);
				} catch (e) {

				}
			} else {
				data.fieldErrors.forEach(function(errorField) {
					$("#messageBox").show();
					$(document.getElementById(errorField.code)).show();
					document.getElementById(errorField.field).style.border = "1px dotted #ff0000";
					if (errorField.field == "templateIDs") {
						$(".ms-choice").css({
							"float" : "right",
							"border-color" : "red",
							"border-width" : "1px",
							"border-style" : "dotted"
						});
						$(".ms-parent").css({
							"border" : "none",
						});
					}
					var fieldName = errorField.field;
					if (fieldName != null) {
						$("#" + fieldName + "Err").text(errorField.message);
						$("#" + fieldName + "Err").show();
					}
				});
			}
			$('#addAssignmentForm').unmask();
		}
	});

	/*
	 * initialize the external events
	 * -----------------------------------------------------------------
	 */

	$('#external-events .fc-event').each(function() {

		// store data so the calendar knows to render an event
		// upon drop
		$(this).data('event', {
			title : $.trim($(this).text()), // use the element's
			// text as the event
			// title
			stick : true
		// maintain when user navigates (see docs on the
		// renderEvent method)
		});

		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex : 999,
			revert : true, // will cause the event to go back
			// to its
			revertDuration : 0
		// original position after the drag
		});

	});

	/*
	 * initialize the calendar
	 * -----------------------------------------------------------------
	 */

	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title'
		},
		height : 700,
		defaultView : 'month',
		selectable : true,
		selectHelper : true,
		dayClick : function(date, jsEvent, view) {
			$("input#startDate").val(date.format('MM/DD/YYYY'));
			$.magnificPopup.open({
				items : {
					src : '#createForm',
					modal : true,
					type : 'inline'
				}
			});

			// $("#eventContent").dialog({
			// modal: true, title:
			// jsEvent.title, resizable: false,
			// width:350});

		},
		eventRender : function(event, element) {
			element.attr('href', 'javascript:void(0);');
			element.click(function() {
				var url = "assignments/" + event.id + "/edit.htm";
				var deleteUrl = "/phisUI/assignments/" + event.id + "/delete.htm";
				$("#editForm").html("<h1>Loading...</h1>");
				$('div#editForm').load(toRelativeURL(url));
				$("#eventLink1").attr('href', deleteUrl);
				$.magnificPopup.open({
					items : {
						src : 'div#editForm',
						modal : true,
						type : 'inline'
					}
				});
				$(this).css('background-color', '#ff8b61');
			});
		},
		editable : false,
		droppable : false, // this allows
		// things to be
		// dropped onto the
		// calendar
		eventLimit : true, // allow "more" link
		// when too many
		// events
		events : {
			url : 'getAssignments.htm'
		}

	});
	$("#btnCancel").click(function() {
		// check if the form is a popup
		if ($('#createForm').length) {
			$.magnificPopup.close();
		} else {
			window.location.href = "/phisUI/assignments/manage.htm";
		}
	});

});
