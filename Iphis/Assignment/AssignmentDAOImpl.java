package gov.usda.aphis.iphis.data.web.dao.impl;

import gov.usda.aphis.iphis.data.NamedQueries;
import gov.usda.aphis.iphis.data.dao.impl.BaseDAOImpl;
import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentTemplateDTO;
import gov.usda.aphis.iphis.data.web.dao.AssignmentDAO;
import gov.usda.aphis.iphis.util.Constants;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class AssignmentsDAOImpl extends BaseDAOImpl<AssignmentDTO> implements AssignmentDAO
{
	
	// holds the logger for this class
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public AssignmentsDAOImpl()
	{
		super(AssignmentDTO.class);
		
	}
	
	@Override
	public AssignmentDTO getByID(long assignmentID)
	{
		logger.debug("retrieving assignment by its ID");
		return getId(assignmentID);
	}
	
	@Override
	public AssignmentDTO getByOfficeID(Long officeID)
	{
		logger.debug("retrieving assignment by its ID");
		Query query = getSession().createQuery("SELECT a FROM AssignmentDTO a WHERE a.office.officeID = :officeID");
		query.setParameter("officeID", officeID);
		return (AssignmentDTO) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AssignmentDTO> retrieveAssignmentsByOfficeID(Long officeID)
	{
		logger.debug("retrieving the assignments list by officeID");
		Query query = getSession().createQuery("SELECT a FROM AssignmentDTO a WHERE a.office.officeID = :officeID");
		query.setParameter("officeID", officeID);
		return (List<AssignmentDTO>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AssignmentDTO> retrieveAssignmentsByTimePeriodAndOfficeID(Long officeID, Date rangeStartDate,
			Date rangeEndDate)
	{
		Criteria criteria = getSession().createCriteria(AssignmentDTO.class);
		criteria.createAlias("office", "office");
		
		// restrict the results to an office
		criteria.add(Restrictions.eq("office.officeID", officeID));
		
		// check to see if the start date of the record falls before or on the end date of the range
		criteria.add(Restrictions.le("startDate", rangeEndDate));
		
		// check to see if the end date of the records is either null or comes after the range start date
		criteria.add(Restrictions.or(Restrictions.ge("endDate", rangeStartDate), Restrictions.isNull("endDate")));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AssignmentTemplateDTO> getTreatmentTemplateList(final long assignmentID)
	{
		Query query = getSession().getNamedQuery(NamedQueries.GET_TREATMENT_TEMPLATE_LIST_BY_ASSIGNMENT);
		query.setParameter("assignmentID", assignmentID);
		query.setParameter("surveySituation", Constants.TREATMENT_SURVEY_SITUATION);
		return query.list();
	}
	
	@Override
	public void deleteByID(long assignmentID)
	{
		Query query1 = getSession().createQuery(
				"DELETE FROM AssignmentTemplateDTO at WHERE at.assignmentID.assignmentID = :assignmentID");
		query1.setParameter("assignmentID", assignmentID);
		query1.executeUpdate();
		
		Query query2 = getSession().createQuery(
				"DELETE FROM AssignmentWorkbookDTO aw WHERE aw.assignment.assignmentID = :assignmentID");
		query2.setParameter("assignmentID", assignmentID);
		query2.executeUpdate();
		
		Query query3 = getSession().createQuery("DELETE FROM AssignmentDTO a WHERE a.assignmentID = :assignmentID");
		query3.setParameter("assignmentID", assignmentID);
		query3.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<WorkbookDTO> getWorkBookList(long assignmentID)
	{
		Query query = getSession()
				.createQuery(
						"SELECT DISTINCT b.workbook FROM AssignmentWorkbookDTO b WHERE  b.assignment.assignmentID = :assignmentID) ");
		query.setParameter("assignmentID", assignmentID);
		return query.list();
	}
	
}
