package gov.usda.aphis.iphis.service.assignment;

import gov.usda.aphis.iphis.data.dto.web.AssignmentDTO;
import gov.usda.aphis.iphis.domain.assignment.Assignment;
import gov.usda.aphis.iphis.domain.assignment.AssignmentTemplate;
import gov.usda.aphis.iphis.domain.workbook.Workbook;
import gov.usda.aphis.iphis.schedule.Schedule;

import java.util.Date;
import java.util.List;

public interface AssignmentService
{
	Assignment getAssignmentByID(long assignmentID);
	
	Assignment getAssignmentByOfficeID(long officeID);
	
	List<Assignment> getAssignmentsByOfficeID(long officeID);
	
	List<Assignment> getAssignmentsByTimePeriodAndOfficeID(long officeID, Date startDate, Date endDate);
	
	Long saveAssignment(Long assignmentID, Long officeID, String assignmentName, Date startDate, Date endDate,
			List<Long> workbooks, String comments, Long userID, List<Long> templates, Schedule schedule);
	
	List<AssignmentTemplate> getTreatmentTemplateList(long assignmentID);
	
	List<Workbook> getWorkBookList(long assignmentID);
	
	void deleteAssignmentByID(long assignmentID);
	
	List<Date> getAssignmentDates(AssignmentDTO assignmentDTO, Date rangeStartDate, Date rangeEndDate);
	
}
