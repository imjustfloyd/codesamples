package gov.usda.aphis.iphis.schedule;

import gov.usda.aphis.iphis.schedule.temporal.TemporalExpression;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;

public class Schedule
{
	// holds the start and end dates for this schedule
	private final LocalDate scheduleStartDate;
	private final LocalDate scheduleEndDate;
	
	private final List<TemporalExpression> temporalExpressions;
	
	public Schedule(final Date scheduleStartDate, final Date scheduleEndDate)
	{
		this((null != scheduleStartDate ? new LocalDate(scheduleStartDate) : null),
				(null != scheduleEndDate ? new LocalDate(scheduleEndDate) : null));
	}
	
	public Schedule(final LocalDate scheduleStartDate, final LocalDate scheduleEndDate)
	{
		// make sure the start date is not null
		if (null == scheduleStartDate)
		{
			throw new IllegalArgumentException("startDate cannot be null");
		}
		
		this.scheduleStartDate = scheduleStartDate;
		this.scheduleEndDate = scheduleEndDate;
		
		temporalExpressions = new ArrayList<TemporalExpression>();
	}
	
	public void addTemporalExpression(final TemporalExpression temporalExpression)
	{
		temporalExpressions.add(temporalExpression);
	}
	
	public List<TemporalExpression> getTemporalExpressions()
	{
		return temporalExpressions;
	}
	
	public LocalDate getFirstOccurrence()
	{
		return getNextOccurrence(scheduleStartDate);
	}
	
	public LocalDate getNextOccurrence()
	{
		return getNextOccurrence(new LocalDate());
	}
	
	private LocalDate getNextOccurrence(final LocalDate startDate)
	{
		final int YEARS_TO_CHECK = 10;
		final LocalDate endDate = startDate.plusYears(YEARS_TO_CHECK);
		
		// for each of the dates between the range
		for (LocalDate date = startDate; !date.isAfter(endDate); date = date.plusDays(1))
		{
			// check to see if this date is occurring according to this schedule
			if (isOccuring(date))
			{
				return date;
			}
		}
		
		throw new NoDateFoundException();
	}
	
	/**
	 * Returns all of the dates for this calendar between the start and end date.<br />
	 * <br />
	 * NOTE:This method will throw an {@link IllegalArgumentException} if the end date was set to null during
	 * initialization
	 * 
	 * @return
	 */
	public List<LocalDate> getDates()
	{
		return getDates(scheduleStartDate, scheduleEndDate);
	}
	
	/**
	 * Returns a list of dates between the start and end date (inclusive) that match the current schedule
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<LocalDate> getDates(final LocalDate startDate, final LocalDate endDate)
	{
		// make sure the start date is not null
		if (null == startDate)
		{
			throw new IllegalArgumentException("startDate cannot be null");
		}
		
		// make sure the end date is not null
		if (null == endDate)
		{
			throw new IllegalArgumentException("endDate cannot be null");
		}
		
		// holds the list of dates to return
		List<LocalDate> dates = new ArrayList<LocalDate>();
		
		// for each of the dates between the range
		for (LocalDate date = startDate; !date.isAfter(endDate); date = date.plusDays(1))
		{
			// check to see if this date is occurring according to this schedule
			if (isOccuring(date))
			{
				dates.add(date);
			}
		}
		
		return dates;
	}
	
	/**
	 * Determines if the schedule is occurring on the target date
	 * 
	 * @param target
	 * @return
	 */
	public boolean isOccuring(final LocalDate target)
	{
		// make sure the target date is not null
		if (null == target)
		{
			throw new IllegalArgumentException("target cannot be null");
		}
		
		// check to see if the target date is after or equal to the start date
		if (isWithinSchedule(target))
		{
			// check to see if there are any temporal expressions
			if (!temporalExpressions.isEmpty())
			{
				// for each of the temporal expressions
				for (TemporalExpression temporalExpression : temporalExpressions)
				{
					// check to see if this expression includes the target calendar
					if (temporalExpression.includes(target, scheduleStartDate))
					{
						return true;
					}
				}
				
				// no match was found
				return false;
			}
			else
			{
				// this date falls within the range of this schedule
				return true;
			}
		}
		else
		{
			// this date falls outside the range of this schedule
			return false;
		}
	}
	
	private boolean isWithinSchedule(final LocalDate target)
	{
		// make sure the target date is not null
		if (null == target)
		{
			throw new IllegalArgumentException("target cannot be null");
		}
		
		// check to see if the target date is after or equal to the start date
		if (target.isAfter(scheduleStartDate) || target.isEqual(scheduleStartDate))
		{
			// check to see if the end date is not null
			if (null != scheduleEndDate)
			{
				// check to see if the target date is before or equal to the end date
				if (target.isBefore(scheduleEndDate) || target.isEqual(scheduleEndDate))
				{
					// this date falls within the range of this schedule
					return true;
				}
				else
				{
					// the target date is past the end of this schedule
					return false;
				}
			}
			else
			{
				// there is no end date so we can assume this schedule continues indefinitely
				return true;
			}
		}
		else
		{
			// the target date is before the start of this schedule
			return false;
		}
	}
	
	public LocalDate getScheduleStartDate()
	{
		return scheduleStartDate;
	}
	
	public LocalDate getScheduleEndDate()
	{
		return scheduleEndDate;
	}
}
