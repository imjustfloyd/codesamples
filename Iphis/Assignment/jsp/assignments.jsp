<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="titleBar" style="margin-bottom:10px;">
			<strong>Assignments</strong>
</div>

<div id="external-events">
	<div class="titleBar" style="margin-bottom:5px;">
			<strong>Assignment List</strong>
	</div>
	<table id="assignmentTable" class="display">
				<thead>
					<tr>
						<th>Event Name</th>
						<td></td>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${assignmentList}" var="assignment">
						<tr>
							<td>${assignment.assignmentName}</td>
							<td><a id="deleteAssignment" onclick="return confirm('Are you sure you want to delete this Assignment?');" href="<c:url value="/assignments/${assignment.assignmentID}/delete.htm"/>" ><img alt=""
									src="<c:url value="/images/delete_icon.png"/>"></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
		</table>
</div>

<div id='calendar' style="margin-bottom:10px;"></div>
<div id="createForm" class="white-popup mfp-hide">
	<c:url value="/assignments/save.htm" var="formAction"/>
	<form:form id="addAssignmentForm" modelAttribute="assignmentForm" method="POST" action="${formAction}">
	<div class="titleBar" style="margin-bottom:5px;">
			<strong>Create Assignment</strong>
	</div>
		<div class="messagebar messagebar-warning" id="messageBox">
			<error class="error" id="startDateErr"></error>
			<error class="error" id="assignmentNameErr"></error>
			<error class="error" id="workbooksErr"></error>
			<error class="error" id="templateIDsErr"></error>
		</div>
		<div class="greyBox">
		<fieldset>
			<label>Start Date:<span class="required">*</span></label>
			<form:input id="startDate" path="startDate" style="font-weight: bold;" type="text" readonly="readonly" placeholder="select a date from the calendar"/>
		</fieldset>
		<fieldset>
			<label>End Date:</label>
			<form:input id="endDate" path="endDate" style="font-weight: bold;" type="text" />
		</fieldset>
		<fieldset>
			<label>Event Name:<span class="required">*</span></label>
			<form:input  id="assignmentName" class="formField" path="assignmentName"  type="text"/>
		</fieldset>
		<fieldset>
			<label>Workbook:<span class="required">*</span></label>
			<div class="form-group ">
				<form:select id="workbooks" path="workbooks" class="formField" multiple="true">
					<form:options items="${workbooks}" itemLabel="workbookName" itemValue="workbookID" />
				</form:select>
			</div>
		</fieldset>
		<fieldset>
			<label>Template:<span class="required">*</span></label>
			<div class="form-group ">
				<form:select id="templateIDs" path="templateIDs" class="formField" multiple="true">
					<form:options items="${templates}" itemLabel="surveyTemplateName"
						itemValue="templateID" />
				</form:select>
			</div>
		</fieldset>	
		<fieldset>
			<label>Comments:</label>
			<form:textarea  class="formField" path="comments"/>
		</fieldset>
	</div>
		<input class="button" type="submit" value="Create"  style="float:left;" title="Create new assignment"/>
		<input type="button" id="btnCancel" class="button" value="Cancel" style="float:right; margin-top:5px;">
	</form:form>
</div>

<div id="editForm" class="white-popup mfp-hide">
</div>

  
