package gov.usda.aphis.iphis.data.dto.web;

import gov.usda.aphis.iphis.data.dto.AbstractBaseDTO;
import gov.usda.aphis.iphis.schedule.Schedule;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "PHIS_ASSIGNMENT")
public class AssignmentDTO extends AbstractBaseDTO
{
	private static final long serialVersionUID = 630152273111919136L;
	
	public static final String PHIS_ASSIGNMENT_SEQ = "PHIS_ASSIGNMENT_SEQ";
	
	@Id
	@Column(name = "ASSIGNMENT_ID", unique = true, nullable = false, precision = 15, scale = 0)
	@SequenceGenerator(name = PHIS_ASSIGNMENT_SEQ, allocationSize = 1, sequenceName = PHIS_ASSIGNMENT_SEQ)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PHIS_ASSIGNMENT_SEQ)
	private long assignmentID;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assignmentID", orphanRemoval = true)
	@Cascade({ org.hibernate.annotations.CascadeType.ALL })
	private List<AssignmentTemplateDTO> templates;
	
	@ManyToOne
	@JoinColumn(name = "OFFICE_ID")
	private OfficeDTO office;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assignment", orphanRemoval = true)
	@Cascade({ org.hibernate.annotations.CascadeType.ALL })
	private List<AssignmentWorkbookDTO> workbooks;
	
	@Column(name = "ASSIGNMENT_NAME")
	private String assignmentName;
	
	@Column(name = "ASSIGNMENT_COMMENTS")
	private String comments;
	
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Column(name = "END_DATE")
	private Date endDate;
	
	@Column(name = "SCHEDULE")
	@Type(type = "gov.usda.aphis.iphis.data.type.ScheduleType")
	private Schedule schedule;
	
	public long getAssignmentID()
	{
		return assignmentID;
	}
	
	public void setAssignmentID(long assignmentID)
	{
		this.assignmentID = assignmentID;
	}
	
	public OfficeDTO getOffice()
	{
		return office;
	}
	
	public void setOffice(OfficeDTO office)
	{
		this.office = office;
	}
	
	public List<AssignmentTemplateDTO> getTemplates()
	{
		return templates;
	}
	
	public void setTemplates(List<AssignmentTemplateDTO> templates)
	{
		this.templates = templates;
	}
	
	public List<AssignmentWorkbookDTO> getWorkbooks()
	{
		return workbooks;
	}
	
	public void setWorkbooks(List<AssignmentWorkbookDTO> workbooks)
	{
		this.workbooks = workbooks;
	}
	
	public String getAssignmentName()
	{
		return assignmentName;
	}
	
	public void setAssignmentName(String assignmentName)
	{
		this.assignmentName = assignmentName;
	}
	
	public String getComments()
	{
		return comments;
	}
	
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	
	public Date getStartDate()
	{
		return startDate;
	}
	
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}
	
	public Date getEndDate()
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}
	
	public Schedule getSchedule()
	{
		return schedule;
	}
	
	public void setSchedule(Schedule schedule)
	{
		this.schedule = schedule;
	}
}
