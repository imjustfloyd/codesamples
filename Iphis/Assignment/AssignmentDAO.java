package gov.usda.aphis.iphis.data.web.dao;

import gov.usda.aphis.iphis.data.dao.BaseDAO;
import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentTemplateDTO;

import java.util.Date;
import java.util.List;

public interface AssignmentDAO extends BaseDAO<AssignmentDTO>
{
	AssignmentDTO getByID(long assignmentID);
	
	AssignmentDTO getByOfficeID(Long officeID);
	
	public List<AssignmentDTO> retrieveAssignmentsByOfficeID(Long officeID);
	
	List<AssignmentDTO> retrieveAssignmentsByTimePeriodAndOfficeID(Long officeID, Date startDate, Date endDate);
	
	List<AssignmentTemplateDTO> getTreatmentTemplateList(long assignmentID);
	
	List<WorkbookDTO> getWorkBookList(long assignmentID);
	
	void deleteByID(long assignmentID);
}
