package gov.usda.aphis.iphis.ui.model;

import gov.usda.aphis.iphis.domain.assignment.Assignment;

import java.util.Date;

public class AssignmentView
{
	private final Assignment assignment;
	
	public AssignmentView(Assignment assignment)
	{
		this.assignment = assignment;
	}
	
	public long getAssignmentID()
	{
		return assignment.getAssignmentID();
	}
	
	public String getAssignmentName()
	{
		return assignment.getAssignmentName();
	}
	
	public Date getStart()
	{
		return assignment.getStartDate();
	}
	
	public Date getEnd()
	{
		return assignment.getEndDate();
	}
	
	public String getComments()
	{
		return assignment.getComments();
	}
	
}
