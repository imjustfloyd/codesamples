package gov.usda.aphis.iphis.domain.assignment;

import gov.usda.aphis.iphis.data.dto.web.AssignmentDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentTemplateDTO;
import gov.usda.aphis.iphis.data.dto.web.AssignmentWorkbookDTO;
import gov.usda.aphis.iphis.schedule.Schedule;
import gov.usda.aphis.iphis.util.Preconditions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Assignment implements Serializable
{
	private static final long serialVersionUID = 8299038341007747874L;
	
	private final AssignmentDTO assignmentDTO;
	
	public Assignment()
	{
		this(new AssignmentDTO());
	}
	
	public Assignment(final AssignmentDTO assignmentDTO)
	{
		Preconditions.checkNotNull(assignmentDTO);
		this.assignmentDTO = assignmentDTO;
	}
	
	public AssignmentDTO getAssignmentDTO()
	{
		return assignmentDTO;
	}
	
	public Long getAssignmentID()
	{
		return assignmentDTO.getAssignmentID();
	}
	
	public String getAssignmentName()
	{
		return assignmentDTO.getAssignmentName();
	}
	
	public List<AssignmentWorkbookDTO> getWorkbookID()
	{
		return assignmentDTO.getWorkbooks();
	}
	
	public List<Long> getWorkbooks()
	{
		List<Long> wBookIDs = new ArrayList<Long>();
		for (AssignmentWorkbookDTO assignmentWorkbookDTO : assignmentDTO.getWorkbooks())
		{
			wBookIDs.add(assignmentWorkbookDTO.getWorkbook().getWorkbookID());
		}
		return wBookIDs;
	}
	
	public Date getStartDate()
	{
		return assignmentDTO.getStartDate();
	}
	
	public List<Long> getTemplateIDs()
	{
		List<Long> templateIDs = new ArrayList<Long>();
		for (AssignmentTemplateDTO assignmentTemplateDTO : assignmentDTO.getTemplates())
		{
			templateIDs.add(assignmentTemplateDTO.getTemplateID().getTemplateID());
		}
		
		return templateIDs;
	}
	
	public Date getEndDate()
	{
		return assignmentDTO.getEndDate();
	}
	
	public String getComments()
	{
		return assignmentDTO.getComments();
	}
	
	public Schedule getSchedule()
	{
		return assignmentDTO.getSchedule();
	}
	
	public void setSchedule(final Schedule schedule)
	{
		assignmentDTO.setSchedule(schedule);
	}
}
