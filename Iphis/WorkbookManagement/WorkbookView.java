package gov.usda.aphis.iphis.ui.model;

import gov.usda.aphis.iphis.domain.workbook.Workbook;
import org.codehaus.jackson.annotate.JsonIgnore;

public class WorkbookView {

	protected final Workbook workbook;

	public WorkbookView(final Workbook workbook) {
		if (null != workbook) {
			this.workbook = workbook;
		} else {
			this.workbook = new Workbook();
		}
	}

	@JsonIgnore
	public Workbook getWorkbook() {
		return workbook;
	}

	public long getWorkbookID() {
		return workbook.getWorkbookId();
	}

	public String getWorkbookName() {
		return workbook.getWorkbookName();
	}

}
