package gov.usda.aphis.iphis.service.workbook;

import gov.usda.aphis.iphis.data.dao.WorkbookDAO;
import gov.usda.aphis.iphis.data.dao.WorkbookFolderDAO;
import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.LocationDTO;
import gov.usda.aphis.iphis.data.dto.web.PhisUserDTO;
import gov.usda.aphis.iphis.data.dto.web.WorkbookFolderDTO;
import gov.usda.aphis.iphis.domain.workbook.Workbook;
import gov.usda.aphis.iphis.results.PartialResults;
import gov.usda.aphis.iphis.service.DataAccessPolicyService;
import gov.usda.aphis.iphis.type.DataAccessPolicyAreaType;
import gov.usda.aphis.iphis.util.GenericObjectWrapper;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WorkbookService
{
	@Autowired
	private WorkbookDAO workbookDAO;

	@Autowired
	private WorkbookFolderDAO workbookFolderDAO;

	@Autowired
	private DataAccessPolicyService dataAccessPolicyService;

	private GenericObjectWrapper<Workbook, WorkbookDTO> workbookWrapper;

	public WorkbookService()
	{
		// create a new generic wrapper so that we can convert the dto objects
		// to business objects
		workbookWrapper = new GenericObjectWrapper<Workbook, WorkbookDTO>(Workbook.class, WorkbookDTO.class);
	}
	
	/**
	 * Retrieve a workbook by its ID
	 * 
	 * @param workbookID
	 * @return
	 */
	@Transactional
	public Workbook getWorkbookByID(long workbookID)
	{
		// retrieve the workbook from the database
		WorkbookDTO workbookDTO = workbookDAO.getWorkbookByID(workbookID);
		
		// make sure the workbook exist
		if (workbookDTO != null)
		{
			Hibernate.initialize(workbookDTO.getLocations());
			// make sure the locations / sites are loaded while we still have an
			// open database transaction
			for (LocationDTO locationDTO : workbookDTO.getLocations())
			{
				// make sure the site is loaded
				locationDTO.getSites().size();
			}
			
			return new Workbook(workbookDTO);
		}
		
		return null;
	}
	
	/**
	 * Retrieve a workbook by its ID provided that it belongs to the selected office
	 * 
	 * @param workbookID
	 * @param selectedOfficeID
	 * @return
	 */
	@Transactional
	public Workbook getWorkbookByID(final long workbookID, final long selectedOfficeID)
	{
		// retrieve the workbook from the database
		WorkbookDTO workbookDTO = workbookDAO.getWorkbookByID(workbookID);
		
		// make sure the workbook exist and that the workbook belongs to the
		// selected office
		if (workbookDTO != null && workbookDTO.getOfficeID() == selectedOfficeID)
		{
			// make sure the locations / sites are loaded while we still have an
			// open database transaction
			for (LocationDTO locationDTO : workbookDTO.getLocations())
			{
				// make sure the site is loaded
				locationDTO.getSites().size();

			}
			
			return new Workbook(workbookDTO);
		}
		
		return null;
	}
	
	/**
	 * Retrieves all of the workbooks that a given office is allowed to see
	 * 
	 * @param officeID
	 * @return
	 */
	@Transactional
	public List<Workbook> getWorkbooksByOffice(final long officeID, boolean archived, boolean includeLocations)
	{
		// by default, only retrieve workbooks in the same office
		// :TODO: we may want to explicitly add this into the DataPolicyService?
		Long[] officeIDs = new Long[] { officeID };

		// retrieve all of the workbooks
		List<WorkbookDTO> workbookDTOs = workbookDAO.getWorkbooksInOffices(archived, officeIDs);

		if(includeLocations){
			for (WorkbookDTO w : workbookDTOs)
			{
				Hibernate.initialize(w.getLocations().size());
            }
        }
        return workbookWrapper.wrap(workbookDTOs) ;

	}

	@Transactional
	public PartialResults<Workbook> searchWorkbooks(String workbookName, long officeId)
	{
		System.out.println("inside service "+workbookName +"--- " +officeId);
		// look up the office ids using the data access policy
		Long[] relatedOfficeIDs = dataAccessPolicyService.getRelatedOfficeIDs(DataAccessPolicyAreaType.LOCATION,
				officeId);

		PartialResults<WorkbookDTO> results = workbookDAO.searchWorkbooks(workbookName, relatedOfficeIDs);
		System.out.println(" list size "+results.getPartialList().size());
		for(WorkbookDTO workbookDTO: results.getPartialList()){
			workbookDTO.getLocations().size();
		}
		return new PartialResults<Workbook>(workbookWrapper.wrap(results.getPartialList()), results.getTotalResults());
	}

	@Transactional
	public PartialResults<Workbook> searchArchived(long officeId){
		System.out.println("inside searchArchived ");
		Long[] relatedOfficeIDs = dataAccessPolicyService.getRelatedOfficeIDs(DataAccessPolicyAreaType.LOCATION,
				officeId);

		PartialResults<WorkbookDTO> results = workbookDAO.searchArchived(relatedOfficeIDs);
		System.out.println(" list size "+results.getPartialList().size());
		for(WorkbookDTO workbookDTO: results.getPartialList()){
			workbookDTO.getLocations().size();
		}
		return new PartialResults<Workbook>(workbookWrapper.wrap(results.getPartialList()), results.getTotalResults());
	}



	@Transactional
	public Long save(Long workbookID, String workbookName, String description,
							   Long parentFolderId, Long officeID, Long userID, boolean archived){

		Workbook workbook = null;
		WorkbookFolderDTO folderDTO = null;
		if (null != workbookID)
		{
			workbook = getWorkbookByID(workbookID);
		}
		else
		{
			if (null == workbook)
			{
				workbook = new Workbook();
				workbook.getWorkbookDTO().setActive(true);
			}
		}
		workbook.getWorkbookDTO().setName(workbookName);
		workbook.getWorkbookDTO().setDescription(description);
		workbook.getWorkbookDTO().setOfficeID(officeID);

		if(!workbook.isArchived() && archived){

			workbook.getWorkbookDTO().setArchived(archived);

		}else if(workbook.isArchived()&& !archived){
			workbook.getWorkbookDTO().setArchived(archived);
		}

		if(parentFolderId != null)
		{
			//if the parent folder exists then retrieve the parent folder by its id
			folderDTO = workbookFolderDAO.getWorkbookFolderByID(parentFolderId);
			//set the workbooks folder to the existing parent folder.
			workbook.getWorkbookDTO().setFolder(folderDTO);
		}else{
			//if the workbooks parent folder is empty set an empty folder.
			workbook.getWorkbookDTO().setFolder(folderDTO);
		}


		//set modified by to current user
		PhisUserDTO user = new PhisUserDTO();
		user.setUserID((userID));
		workbook.getWorkbookDTO().setModifiedBy(user);
		//set modified time by current time
		workbook.getWorkbookDTO().setModifiedDate(new Date());

		//if new workbook, set createdBy and created date to the current user and the current time respectively
		if(null == workbookID)
		{
			workbook.getWorkbookDTO().setCreatedBy(user);
			workbook.getWorkbookDTO().setCreatedDate(new Date());
		}








		Long newWorkbookID = workbookDAO.merge(workbook.getWorkbookDTO()).getWorkbookID();
		return newWorkbookID;
	}

	@Transactional
	public Workbook getWorkbookByWBID(long workbookId)
	{
		WorkbookDTO workbookDTO = workbookDAO.getWorkbookByID(workbookId);
		if (null != workbookDTO)
		{
			return new Workbook(workbookDTO);
		}
		else
		{
			return null;
		}
	}

	@Transactional
	public WorkbookDTO searchByWorkbookId(long workbookId){
		return workbookDAO.searchByWorkbookId(workbookId);
	}

	@Transactional
	public void archive(PhisUserDTO user, Long... workbookIDs) {

		System.out.println("no of ids in service class"+workbookIDs);
		List<WorkbookDTO> workbooks = workbookDAO.get(workbookIDs);
		System.out.println("no of ids after get"+workbooks.size());
		for (WorkbookDTO workbook : workbooks) {
			workbook.setModifiedBy(user);
			workbook.setModifiedDate(new Date());
			workbook.setArchived(true);
		}
		workbookDAO.mergeAll(workbooks);
	}

	@Transactional
	public void deleteByID(long workbookID)
	{
		workbookDAO.deleteByID(workbookID);

	}
	
	public List<Long> getLocationIdsForWorkbook(Long workbookID)
	{
		return workbookDAO.getLocationIdsForWorkbook(workbookID);
	}

	@Transactional
	public Long getActiveLocationCountByWorkbook(Long workbookID) {
		Long count = workbookDAO.getActiveLocationCountByWorkbook(workbookID);
		return count;
	}
}