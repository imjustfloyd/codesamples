<%--
  Created by IntelliJ IDEA.
  User: justinfloyd
  Date: 2/8/17
  Time: 5:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
--><%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="titleBar" style="margin-bottom:10px;">
    <strong>Location Work book Management</strong>
</div>

<div id="workbookSection">
    <div class="titleBar" style="margin-bottom:5px;">
        <strong>Work book List</strong>
        <span style="float:right">
        <input type="button" class="button" value="Create Work Book"
               onclick="createWorkbook()">
        <input type="button" class="button" value="Create Folder" onclick="createWorkbookFolder()"></span>

    </div>

    <form id="workbookResultsForm" method="POST" action="">
        <table id="workbookTable" class="display">
            <thead>
            <tr>
                <td>Workbook ID</td>
                <td>Actions</td>
                <td>Workbook Name</td>
                <td>Folder</td>
                <td>Archived Status</td>
                <td></td>
                <td>Created Date</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </form>
    <div id="wbSearch">
        <form:form id="workbookSearchForm" name="workBookSearchForm"
                   method="POST" action="workbookSearch.htm">
            <input type="hidden" name="archived"/>
            <div class="buttons">

                <input id="showArchived" type="button" class="button" value="Show Archived" onclick="showArchivedWorkbooks(true);">
                <input id="showUnarchived" type="submit" class="button" value="Show Unarchived" onclick="showArchivedWorkbooks(false)">
            </div>
        </form:form>

    </div>


</div>

<div id="locationSection">

    <jsp:include page="search.jsp"/>

</div>

<div id="viewForm" class="white-popup mfp-hide" >
    <div class="mfp-s-loading"></div>
    <div class="mfp-s-error "></div>
</div>
<div id="viewFormMap" class="white-popup_map mfp-hide" >

    <div class="mfp-iframe-scaler">
        <iframe src="/phisWeb/areaMap.htm?action=areaMap&locID=-1"
                class="mfp-iframe" frameBorder="0" scrolling="auto" width="100%" height="100%"
                title="Create on Map">Define Location
        </iframe>
    </div>
</div>





