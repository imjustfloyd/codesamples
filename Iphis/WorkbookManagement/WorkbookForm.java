package gov.usda.aphis.iphis.ui.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by reena on 1/25/2017.
 */
public class WorkbookForm {
    private final int MAX_DESCRIPTION_LENGTH = 1000;

    private Long workbookID;
    @NotEmpty
    private String workbookName;
    @Size(max = MAX_DESCRIPTION_LENGTH)
    private String description;
    private Long folderID;
    private Long officeID;
    private boolean archived;

    public Long getWorkbookID() {
        return workbookID;
    }

    public void setWorkbookID(Long workbookID) {
        this.workbookID = workbookID;
    }

    public String getWorkbookName() {
        return workbookName;
    }

    public void setWorkbookName(String workbookName) {
        this.workbookName = workbookName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getFolderID() {
        return folderID;
    }

    public void setFolderID(Long folderID) {
        this.folderID = folderID;
    }

    public Long getOfficeID() {
        return officeID;
    }

    public void setOfficeID(Long officeID) {
        this.officeID = officeID;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }
}
