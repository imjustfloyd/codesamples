package gov.usda.aphis.iphis.ui.controller.workbook;

import gov.usda.aphis.iphis.domain.ActiveUser;
import gov.usda.aphis.iphis.domain.web.*;
import gov.usda.aphis.iphis.domain.workbook.Workbook;
import gov.usda.aphis.iphis.domain.workbook.WorkbookFolder;
import gov.usda.aphis.iphis.results.PartialResults;
import gov.usda.aphis.iphis.rules.config.ReferenceDataLoader;
import gov.usda.aphis.iphis.service.CountryStateService;
import gov.usda.aphis.iphis.service.OfficeService;
import gov.usda.aphis.iphis.service.TemplateService;
import gov.usda.aphis.iphis.service.location.LocationService;
import gov.usda.aphis.iphis.service.workbook.WorkbookFolderService;
import gov.usda.aphis.iphis.service.workbook.WorkbookLocationService;
import gov.usda.aphis.iphis.service.workbook.WorkbookService;
import gov.usda.aphis.iphis.ui.UiUtils;
import gov.usda.aphis.iphis.ui.form.LocationSearchForm;
import gov.usda.aphis.iphis.ui.form.WorkBookSearchForm;
import gov.usda.aphis.iphis.ui.form.WorkbookForm;
import gov.usda.aphis.iphis.ui.model.*;
import gov.usda.aphis.iphis.ui.model.Tree.ChildNode;
import gov.usda.aphis.iphis.ui.model.Tree.TreeNode;
import gov.usda.aphis.iphis.ui.util.AjaxSearchResultsUtil;
import gov.usda.aphis.iphis.ui.view.Tiles;
import gov.usda.aphis.iphis.util.GenericObjectWrapper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;


/**
 * Created by justinfloyd on 2/8/17.
 */

@Controller
@RequestMapping("/workbook/")
public class LocationWorkbookController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String WORKBOOK_SEARCH_FORM_NAME = "command";

    public static final String LOCATION_SEARCH_FORM_NAME = "command";
    public static final String WORKBOOKS = "workbooks";
    public static final String COUNTRIES = "countries";
    public static final String ARCHIVED = "archived";
    public static final String STATES = "states";
    public static final String LOCATION_TYPES = "locationTypes";
    public static final String STATUSES = "statuses";
    public static final String TEMPLATE_TYPE = "templateType";
    public static final String OFFICES = "offices";
    public static final String TEMPLATES = "templates";
    public static final String COUNTIES = "counties";
    public static final String WORKBOOK_ID = "workbookID";
    public static final String WORKBOOK_FORM = "workbookForm";
    public static final String WORKBOOK_ICON = "/phisUI/images/notebook.png";
    public static final String LOCATIONS = "locationList";
    public static final String WORKBOOK_NAME = "workbookName";

    public static final String FOLDERS = "folders";

    @Autowired
    private OfficeService officeService;
    @Autowired
    private WorkbookService workbookService;

    @Autowired
    private CountryStateService countryStateService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private ReferenceDataLoader referenceDataLoader;

    @Autowired
    private WorkbookFolderService workbookFolderService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private WorkbookLocationService workbookLocationService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Mapper mapper;


    @RequestMapping(value = "/locationWorkbookSearch.htm", method = RequestMethod.GET)
    private String getLocationsWorkbookPage(final Model model, final ActiveUser activeUser) {
        LocationSearchForm locationSearchForm = new LocationSearchForm();
        locationSearchForm.setLocOffice(activeUser.getSelectedOfficeID());
        WorkBookSearchForm workbookSearchForm = new WorkBookSearchForm();

        locationSearchForm.setCountry("USA");
        locationSearchForm.setState(activeUser.getSelectedOffice().getOfficeState());

        model.addAttribute(LOCATION_SEARCH_FORM_NAME, locationSearchForm);

        //get the list of workbooks by the user selected officeID
        List<Workbook> allWorkbooks = workbookService.getWorkbooksByOffice(activeUser.getSelectedOfficeID(), false, false);
        GenericObjectWrapper<WorkbookDetailView, Workbook> workbookWrapper = new GenericObjectWrapper<WorkbookDetailView, Workbook>(
        		WorkbookDetailView.class, Workbook.class);
        model.addAttribute(COUNTRIES, countryStateService.getActiveCountryCodeMappings());
        model.addAttribute(COUNTIES, new GenericObjectWrapper<CountyModel, RefCounty>(CountyModel.class, RefCounty.class)
                .wrap(countryStateService.getActiveCountyMappings(locationSearchForm.getState())));
        model.addAttribute(LOCATION_TYPES, referenceDataLoader.loadLocationTypeList());
        model.addAttribute(STATES,
                new GenericObjectWrapper<StateModel, RefState>(StateModel.class, RefState.class)
                        .wrap(countryStateService.getActiveStateCodeMappings(locationSearchForm.getCountry())));
        model.addAttribute(WORKBOOKS, workbookWrapper.wrap(allWorkbooks));
        model.addAttribute(STATUSES, getValidationStatus());
        model.addAttribute(TEMPLATE_TYPE, getTemplateType());
        // get the list of all offices
        List<PhisOffice> allOffices = officeService.getOffices();
        // create a wrapper object
        GenericObjectWrapper<PhisOfficeView, PhisOffice> officeWrapper = new GenericObjectWrapper<PhisOfficeView, PhisOffice>(
                PhisOfficeView.class, PhisOffice.class);
        model.addAttribute(OFFICES, officeWrapper.wrap(allOffices));
        model.addAttribute(TEMPLATES,
                new GenericObjectWrapper<TemplateView, PhisTemplate>(TemplateView.class, PhisTemplate.class)
                        .wrap(new ArrayList<PhisTemplate>()));
        return Tiles.LOCATIONS_WORKBOOK_MANAGE;
    }

    @RequestMapping(value="/getWorkbookFolders.htm", method = RequestMethod.GET)
    public @ResponseBody
    List<TreeNode> getFoldersAndWorkbooks(final ActiveUser activeUser){
        //retrieve workbookfolders
        List<Workbook> pWorkbookList = workbookService.getWorkbooksByOffice(activeUser.getSelectedOfficeID(), false, true);
        Workbook cWorkbook = new Workbook();
        List<TreeNode> resultList = new ArrayList<TreeNode>();
        List<ChildNode> childNodes = new ArrayList<ChildNode>();
        for(Workbook workbook : pWorkbookList)
        {
            TreeNode node = new TreeNode();
            ChildNode cNode = new ChildNode();
            if(null == workbook.getFolder()){
                node.setText(workbook.getWorkbookName());
                node.setIcon(WORKBOOK_ICON);
                resultList.add(node);

            }
            else
            {
                node.setParentId(workbook.getFolder().getFolderID().toString());
                node.setText(workbook.getFolder().getWorkbookFolderName());
                cNode.setId( String.valueOf(workbook.getWorkbookId()));
                cNode.setText(workbook.getWorkbookName());
                cNode.setIcon(WORKBOOK_ICON);
                childNodes.add(cNode);
                node.setChildren(childNodes);
                resultList.add(node);

            }


        }
        return resultList;
    }


    public HashMap<String, String> getValidationStatus() {
        HashMap<String, String> refValidationStatusMap = new LinkedHashMap<String, String>();
        refValidationStatusMap.put("all", "All");
        List<String> validationStatuses = locationService.getAllValidationStatuses();
        for(String status : validationStatuses){
            refValidationStatusMap.put(status,StringUtils.capitalize(status.toLowerCase()));
        }
        return refValidationStatusMap;
    }

    public HashMap<String, String> getTemplateType() {
        HashMap<String, String> templateTypeMap = new LinkedHashMap<String, String>();
        List<String> templateTypes = templateService.getTemplateTypes();
        for(String type : templateTypes){
           templateTypeMap.put(type,StringUtils.capitalize(type.toLowerCase()));
        }
        return templateTypeMap;
    }

    @RequestMapping(value = "/search.htm", method = RequestMethod.POST)
    private
    @ResponseBody
    Map<String, Object> doSearch(
            @ModelAttribute(LOCATION_SEARCH_FORM_NAME) final LocationSearchForm locationSearchForm,
            @RequestParam(value = WORKBOOK_ID, required = false) final Long workbookID, final ActiveUser activeUser) {

        PartialResults<Location> results = null;
        if (workbookID == null) {
            results = locationService.search(locationSearchForm.getName(), locationSearchForm.getAddress(),
                    locationSearchForm.getCountry(), locationSearchForm.getState(), locationSearchForm.getCity(),
                    locationSearchForm.getCounty(), locationSearchForm.getStatus(), locationSearchForm.getType(),
                    locationSearchForm.getIds(), locationSearchForm.getLocOffice(), locationSearchForm.getTemplateType(),
                    locationSearchForm.getTemplateName(), locationSearchForm.getSearchFormWbID(), locationSearchForm.getStartDate(), locationSearchForm.getEndDate());
        } else {
            List<Location> locations = locationService.retrieveLocationsByWorkbook((locationSearchForm.getWorkbookID()));
            results = new PartialResults<>(locations, locations.size());
        }

        // create the object wrapper that will wrap the list of results into view objects
        GenericObjectWrapper<LocationView, Location> wrapper = new GenericObjectWrapper<LocationView, Location>(
                LocationView.class, Location.class);

        return AjaxSearchResultsUtil.buildResponseMap(results, wrapper.wrap(results.getPartialList()));
    }

    //create method to load the form in its own page.
    @RequestMapping(value="/workbookManage.htm", method = RequestMethod.GET)
    private String getWorkbookForm(ModelMap model, final ActiveUser activeUser)
    {
        WorkbookForm workbookForm = new WorkbookForm();
        List<WorkbookFolder> allWorkbookFolders = workbookFolderService.getWorkbookFolderList();
        GenericObjectWrapper<WorkbookFolderView, WorkbookFolder> workbookFolderWrapper = new GenericObjectWrapper<WorkbookFolderView, WorkbookFolder>(
                WorkbookFolderView.class, WorkbookFolder.class);
        List<WorkbookFolderView> folderViewList = workbookFolderWrapper.wrap(allWorkbookFolders);

        model.addAttribute(FOLDERS, folderViewList);
        model.addAttribute(WORKBOOK_FORM, workbookForm);
        return Tiles.WORKBOOK_MANAGE_FORM_PAGE;
    }

    @RequestMapping(value = "/locationCount.htm", method = RequestMethod.GET)
    private @ResponseBody Long getActiveLocationCountForWorkbook(@RequestParam(value = "workbookID", required = true)final Long workbookID)
    {

        Long count = workbookService.getActiveLocationCountByWorkbook(workbookID);
        logger.debug(String.valueOf(count));
        return count;
    }
    //New Create Workbook Method
    @RequestMapping(value = "/archiving.htm", method = RequestMethod.POST)
    private @ResponseBody AjaxFormResponse<WorkbookView> update(@RequestParam(value = "workbookID", required = false) final Long workbookID,
            @RequestParam(value = "archived", required=false) final boolean archived,
            @ModelAttribute(WORKBOOK_FORM) @Valid WorkbookForm workbookForm, final BindingResult result,
              final ActiveUser activeUser) {
        Long newWorkbookID;
        if(workbookID != null){
            Workbook workbook = workbookService.getWorkbookByID(workbookID);
            workbookForm.setWorkbookID(workbook.getWorkbookId());
            workbookForm.setWorkbookName(workbook.getWorkbookName());

            if(workbook.getFolder() != null){
                workbookForm.setFolderID(workbook.getFolder().getFolderID());
            }
            workbookForm.setArchived(archived);
        }
        newWorkbookID = workbookSave(workbookForm, activeUser, workbookForm.isArchived());
        return new AjaxFormResponse<WorkbookView>(true, null, new WorkbookView(workbookService.getWorkbookByID(newWorkbookID)), null);
    }

    //New Create Workbook Method
    @RequestMapping(value = "/save.htm", method = RequestMethod.POST)
    private @ResponseBody AjaxFormResponse<WorkbookView> saveWorkbook(@RequestParam(value = "workbookID", required = false) final Long workbookID,
            @RequestParam(value = "archived", required=false) final boolean archived,
            @ModelAttribute(WORKBOOK_FORM) @Valid WorkbookForm workbookForm, final BindingResult result,
            final ActiveUser activeUser) {
       Long newWorkbookID;

        if (!result.hasErrors()) {
            if (workbookID != null) {
                workbookForm.setWorkbookID(workbookID);

            }
            //save workbook
            newWorkbookID = workbookSave(workbookForm, activeUser, workbookForm.isArchived());
            return new AjaxFormResponse<WorkbookView>(true, null, new WorkbookView(workbookService.getWorkbookByID(newWorkbookID)), null);
        } else {
            List<FieldErrorMessage> fieldErrorMessages = new ArrayList<FieldErrorMessage>();
            for (FieldError fieldError : result.getFieldErrors()) {
                fieldErrorMessages.add(UiUtils.makeFieldErrorMessage(fieldError, messageSource));
            }
            return new AjaxFormResponse<WorkbookView>(false, null, null, fieldErrorMessages);
        }
    }
    public Long  workbookSave(WorkbookForm workbookForm, ActiveUser activeUser, boolean archived){
         Long newWorkbookID = workbookService.save(workbookForm.getWorkbookID(), workbookForm.getWorkbookName(), workbookForm.getDescription(),
                workbookForm.getFolderID(), activeUser.getSelectedOfficeID(), activeUser.getPhisUserDTO().getUserID(), archived);
        return newWorkbookID;
    }

    @RequestMapping(value = "/workbookSearch.htm")
    private @ResponseBody Map<String, Object> doWBSearch(@RequestParam(value = ARCHIVED, required = false, defaultValue = "false")  Boolean archived,
            @ModelAttribute(WORKBOOK_SEARCH_FORM_NAME) final WorkBookSearchForm workBookSearchForm,
            final ActiveUser activeUser) {

        //get the list of workbooks by the user selected officeID, and archived option
        List<Workbook> allWorkbooks = workbookService.getWorkbooksByOffice(activeUser.getSelectedOfficeID(), archived , true);
        List<Workbook> resultList  = new ArrayList<Workbook>();
        for(Workbook w : allWorkbooks){
          Long count =  getActiveLocationCountForWorkbook(w.getWorkbookId());
            w.setActiveLocationCount(count);
            resultList.add(w);
        }
        PartialResults<Workbook> results = null;

        GenericObjectWrapper<WorkbookDetailView, Workbook> wrapper = new GenericObjectWrapper<WorkbookDetailView, Workbook>(
                WorkbookDetailView.class, Workbook.class);
        results = new PartialResults<>(resultList, resultList.size());

        return AjaxSearchResultsUtil.buildResponseMap(results, wrapper.wrap(results.getPartialList()));
    }

    @RequestMapping(value = "/{workbookID}/editWorkbook.htm", method = RequestMethod.GET)
    private String editWorkbookForm(@PathVariable("workbookID") final long workbookID,
            WorkbookForm workbookForm, ModelMap model, final ActiveUser activeUser)
    {
        //build the workbook form from the work book that was loaded out of the database
        Workbook workbook = workbookService.getWorkbookByID(workbookID);
        workbookForm = mapper.map(workbook, WorkbookForm.class);
        workbookForm.setWorkbookID(workbookID);
        workbookForm.setDescription(workbook.getDescription());

        if (workbook.getFolder() != null)
        {
            workbookForm.setFolderID(workbook.getFolder().getFolderID());
        }


        List<WorkbookFolder> allWorkbookFolders = workbookFolderService.getWorkbookFolderList();
        GenericObjectWrapper<WorkbookFolderView, WorkbookFolder> workbookFolderWrapper = new GenericObjectWrapper<WorkbookFolderView, WorkbookFolder>(
                WorkbookFolderView.class, WorkbookFolder.class);
        List<WorkbookFolderView> folderViewList = workbookFolderWrapper.wrap(allWorkbookFolders);
        model.addAttribute(FOLDERS, folderViewList);
        model.addAttribute(WORKBOOK_FORM, workbookForm);

        return Tiles.WORKBOOK_EDIT_FORM_PAGE;
    }

    @RequestMapping(value = "/{workbookID}/delete.htm", method = RequestMethod.POST)
    private @ResponseBody boolean deleteWorkbook(@PathVariable("workbookID") final long workbookID)
    {
        Workbook workbook = workbookService.getWorkbookByID(workbookID);
        Long locationCount = workbook.getLocationCount();
        if(locationCount > 0) {
            return false;

        }else{
            workbookService.deleteByID(workbookID);
            return true;
        }
    }
    @RequestMapping(value = "/print.htm", method = RequestMethod.GET)
    public String printLocationsForWB(@RequestParam("workBookId") Long workBookId, Model model) throws Exception {

        List<Location> locList = locationService.retrieveLocationsByWorkbook(workBookId);
        //wrap the location list in the location view . this will give everything needed to prepare the print screen.
        GenericObjectWrapper<LocationView, Location> locationWrapper = new GenericObjectWrapper<LocationView, Location>(
                LocationView.class, Location.class);
        List<LocationView> locationList = locationWrapper.wrap(locList);

        Workbook workbook = workbookService.getWorkbookByID(workBookId);
        model.addAttribute(LOCATIONS, locationList);
        model.addAttribute(WORKBOOK_NAME, workbook.getWorkbookName());
        return Tiles.WORKBOOK_PRINT_PAGE;
    }



    @RequestMapping(value = "/locationIDs.htm", method = RequestMethod.GET)
    public List<Long> getLocationIdsForWorkbook(@RequestParam Long workbookID)
    {
    	return workbookService.getLocationIdsForWorkbook(workbookID);
    }

    @RequestMapping(value = "/{workbookID}/unassignLocations.htm", method = RequestMethod.POST)
    public @ResponseBody boolean  unassignLocationWorkBook(@PathVariable("workbookID") final Long workbookID,
                                                           @RequestBody Long[] locationIds) throws Exception {
        workbookLocationService.unassignLocation(workbookID, locationIds);
        return true;
    }

    @RequestMapping(value = "/{workbookID}/assignLocations.htm", method = RequestMethod.POST)
    public @ResponseBody boolean assignLocationToWorkBook(@PathVariable("workbookID") final long workbookID,
                                                          @RequestBody long[] locationIds, final ActiveUser activeUser) throws Exception {

        workbookLocationService.assignLocations(workbookID, locationIds, activeUser.getPhisUserDTO());
        return true;
    }

}