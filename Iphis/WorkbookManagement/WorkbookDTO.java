package gov.usda.aphis.iphis.data.dto.scheduling;

import gov.usda.aphis.iphis.data.dto.AbstractBaseDTO;
import gov.usda.aphis.iphis.data.dto.web.LocationDTO;
import gov.usda.aphis.iphis.data.dto.web.WorkbookFolderDTO;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PHIS_WORKBOOK")
public class WorkbookDTO extends AbstractBaseDTO {
	private static final long serialVersionUID = 8047394472305083653L;

	private static final String WORKBOOK_ID_SEQ = "WORKBOOK_ID_SEQ";

	@Id
	@Column(name = "WORKBOOK_ID")
	@SequenceGenerator(name = WORKBOOK_ID_SEQ, allocationSize = 1, sequenceName = WORKBOOK_ID_SEQ)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = WORKBOOK_ID_SEQ)
	private Long workbookID;

	@Column(name = "OFFICE_ID")
	private Long officeID;

	@Column(name = "WORKBOOK_NAME")
	private String name;

	@Column(name = "WORKBOOK_DESCRIPTION")
	private String description;

	@Column(name = "ARCHIVED")
	@Type(type = "yes_no")
	private boolean archived;

	@ManyToMany
	@JoinTable(name = "PHIS_WORKBOOK_LOCATION", joinColumns = {
			@JoinColumn(name = "WORKBOOK_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "LOCATION_ID", nullable = false, updatable = false) })
	@LazyCollection(LazyCollectionOption.EXTRA)
	@Where(clause = "active='Y'")
	private List<LocationDTO> locations;

	@ManyToOne
	@JoinColumn(name = "FOLDER_ID")
	private WorkbookFolderDTO folder;

	public Long getWorkbookID() {
		return workbookID;
	}

	public void setWorkbookID(Long workbookID) {
		this.workbookID = workbookID;
	}

	public Long getOfficeID() {
		return officeID;
	}

	public void setOfficeID(Long officeID) {
		this.officeID = officeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<LocationDTO> getLocations() {
		return locations;
	}

	public int getLocationSize() {
		return locations.size();
	}

	public void setLocations(List<LocationDTO> locations) {
		this.locations = locations;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(boolean archived) {
		this.archived = archived;
	}

	public WorkbookFolderDTO getFolder() {
		return folder;
	}

	public void setFolder(WorkbookFolderDTO folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "WorkbookDTO{" + "workbookID=" + workbookID + ", officeID=" + officeID + ", name='" + name + '\''
				+ ", description='" + description + '\'' + ", locations=" + locations + '}';
	}

}
