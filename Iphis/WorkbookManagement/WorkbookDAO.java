package gov.usda.aphis.iphis.data.dao;

import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.results.PartialResults;

import java.util.List;

public interface WorkbookDAO extends BaseDAO<WorkbookDTO> {
	public WorkbookDTO getWorkbookByID(long workbookID);

	/**
	 * Retrieve all of the active workbooks for a given office and order them by
	 * name
	 * 
	 * @param
	 * @return
	 */
	public List<WorkbookDTO> getWorkbooksInOffices(boolean archived, Long... officeIDs);

	public PartialResults<WorkbookDTO> getAllWorkbooks();

	public PartialResults<WorkbookDTO> searchWorkbooks(String workbookName, Long[] officeIDs);

	public PartialResults<WorkbookDTO> searchArchived(Long[] officeIDs);

	public PartialResults<WorkbookDTO> getWorkbooks();

	public WorkbookDTO searchByWorkbookId(Long workbookId);

	void deleteByID(long workbookID);

	public List<WorkbookDTO> get(Long... ids);
	
	public List<Long> getLocationIdsForWorkbook(Long workbookId);

	public Long getActiveLocationCountByWorkbook(Long workbookID);

}
