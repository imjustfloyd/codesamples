var workbookSearchResults;
$(document).ready(function() {
	$(window).keydown(function(event) {
		if (event.keyCode == 13) {
			event.preventDefault();
			return false;
		}

	});
	$('.datepicker').each(function() {
		$(this).datepicker({
			beforeShow : function(input, inst) {
				inst.dpDiv.css({
					marginTop : -input.offsetHeight + 'px',
					marginLeft : input.offsetWidth + 'px'
				});
			}
		});
	});
	var wbID;
	var locID = null;
	$('#showUnarchived').hide();
	$('#showUnarchived').on('click',function(){
		$(this).hide();
		$('#showArchived').show();
	});
	$('#cancelButton').on('click', function() {
		$.magnificPopup.close();
	});

	$('.worbookCheckboxCol input[type="checkbox"]').on('change', function() {
		wbID = $(this).val();
		$('.worbookCheckboxCol input[type="checkbox"]').not(this).prop('checked', false);

	});

	var workbookTable = $("#workbookTable").DataTable({
		"bAutoWidth" : false,
		"stateSave" : false,
		"language" : {
			"sSearch" : "_INPUT_ "
		},
		"scrollY" : "300px",
		"scrollCollapse" : false,
		"iDisplayLength" : 10,
		"lengthMenu" : [ 10, 25, 50, 100 ],
		"sDom" : '<"wblengthMenu"l><"wbSearchFilter"f>rtip',
		"pagingType" : "full",

		// Don't display the DataTables filter since we
		// do our own advanced filtering.
		"order" : [ [ 3, 'desc' ] ],
		"bFilter" : true,
		"createdRow" : function(row, data, index) {
			// set ID of the row row_ + location ID (Third column), so
			// it's possible to update that particular
			// row whenever wanted

			$(row).attr("id", "row_" + data[0]);
			$(row).addClass("workbook-row" + data[0]);
			if(data[4] == "ARCHIVED"){
				$(row).addClass("archived-row-" + data[0]);
			}

			$(row).attr("onclick", "highlightLocation(" + data[0] + ")");

		},
		"fnDrawCallback" : function(settings) {
			var api = this.api();
			var rows = api.rows({
				page : 'current'
			}).nodes();
			var last = null;

			api.column(3, {
				page : 'current'
			}).data().each(function(group, i) {
				if (last !== group) {
					$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');

					last = group;
				}
			});
		
		},
		"sPaginationType" : "input",
		"aoColumnDefs" : [ {
			"bVisible" : false,
			"aTargets" : [ 0, 3, 6]
		}, {
			// the checkbox field should not be sortable
			bSortable : false,
			"aTargets" : [ 1, 2, 4,5,6 ]
		}, {
			"aDataSort" : [ 2, 2 ],
			"aTargets" : [ 2 ]
		}, {
			"aDataSort" : [ 3, 2 ],
			"aTargets" : [ 3 ]
		}, {

			"aDataSort" : [ 4, 2 ],
			"aTargets" : [ 4 ]
		}, {
			"className" : 'workbookCheckboxCol',
			"aDataSort" : [ 5, 2 ],
			"aTargets" : [ 5 ]
		}, {
			"aDataSort" : [6, 2],
			"aTargets" : [5]
		} ]
	});

	$(' #workbookCxBx').on('change', function() {
		wbID = $(this).val();
		$(' #workbookCxBx').not(this).prop('checked', false);

	});

	$("#workbookSearchForm").ajaxForm({
		beforeSubmit : function() {
			$("#workbookSection").mask("Loading Workbooks...");
		},
		success : function(data) {
			workbookSearchResults = data.results;
			workbookTable.clear();
			workbookSearchResults.forEach(function(row) {
				addRowToWorkbookTable(workbookTable, row);
			});
			try {
				workbookTable.draw();
			} catch (e) {
				console.log(e);
			}
			$("#workbookSection").unmask();

		}
	});

	$('.wbSearchFilter input').attr('placeholder', 'Search Workbooks');

	// Order by the grouping
	$('#workbookTable tbody').on('click', 'tr.group', function() {
		var currentOrder = workbookTable.order()[0];
		if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
			workbookTable.order([ 3, 'desc' ]).draw();
		} else {
			workbookTable.order([ 3, 'asc' ]).draw();
		}
	});

	/**
	 * Workbooks Context Menu
	 */
	$('a.workbooksMenu').click(function() {
		var id = $(this).attr('id');
		wbID = id;
		initializeWBContextMenu(wbID);
		$("input#workbookID").val(wbID);
		// $('#messageBox').hide();
	});

	$("#locationID").click(function() {
		var checkall = $("#locationID").prop('checked');
		$("[name='locationID']").prop('checked', checkall);
	});

	var searchFilterToggle = $('#searchFilterToggle');
	searchFilterToggle.change(function(event) {
		if (this.checked) {
			jQuery('div#search').show();
		} else {
			jQuery('div#search').hide();
		}
	});
	searchFilterToggle.trigger('change');

	$.ajax({
		url : toRelativeURL('/statesService/USA/states.htm'),
		success : function(data) {
			$.each(data, function(index, state) {
				$('<option>', {
					value : state.stateCode
				}).html(state.stateName).appendTo(".statesList");
			});
		}
	});
	$("#locationSearchForm").find("#country").change(function() {
		try {
			changeCountryCode($("#locationSearchForm").find('#state'), $(this).val());

		} catch (err) {
			console.log(err);
		}
		try {
			changeStateCode($("#locationSearchForm").find('#county'), "");
		} catch (err) {
			console.log(err);
		}

	});

	$("#locationSearchForm").find("#state").change(function() {
		changeStateCode($("#locationSearchForm").find('#county'), $(this).val());
	});

	$("#locationSearchForm").find("#templateType").change(function() {
		changeTemplateType($("#locationSearchForm").find('#templateName'), $(this).val());
	});

	var resultsDataTable = $("#locationResults").DataTable({
		"bAutoWidth" : false,
		"stateSave" : true,
		"language" : {
			"sSearch" : "_INPUT_ "
		},
		"sDom" : 'lC<"locationFilter"f>rtip',
		"oColVis" : {
			"aiExclude" : [ 0, 7, 8 ]
		},
		// Don't display the DataTables filter since we
		// do our own advanced filtering.
		"order" : [ [ 9, 'desc' ] ],
		"bFilter" : true,
		"createdRow" : function(row, data, index) {
			// set ID of the row row_ + location ID (Third column), so
			// it's possible to update that particular
			// row whenever wanted
			$(row).attr("id", "row_" + data[1]);

			$(row).addClass("location-row " + data[1]);

			$.each(data[8], function(index, workbookID) {
				$(row).addClass("workbook-id-" + workbookID);
			});

			$(row).on('click', function(){
				highlightWorkbook(data[8]);
			})

//			$(row).attr("onclick", "highlightWorkbook(" + data[1] + ")");

		},
		"fnDrawCallback" : function() {

		},
		"sPaginationType" : "input",
		"aoColumnDefs" : [ {
			"bVisible" : false,
			"aTargets" : [ 1, 8, 9 ]
		}, {
			// the checkbox field should not be sortable
			bSortable : false,
			"aTargets" : [ 0, 7 ]
		}, {
			"aDataSort" : [ 2, 2 ],
			"aTargets" : [ 2 ]
		}, {
			"aDataSort" : [ 3, 2 ],
			"aTargets" : [ 3 ]
		}, {
			"aDataSort" : [ 4, 2 ],
			"aTargets" : [ 4 ]
		}, {
			"aDataSort" : [ 5, 2 ],
			"aTargets" : [ 5 ]
		}, {
			"aDataSort" : [ 6, 2 ],
			"aTargets" : [ 6 ]
		}, {
			"aDataSort" : [ 7, 2 ],
			"aTargets" : [ 7 ]
		}, {
			"aDataSort" : [ 8, 2 ],
			"aTargets" : [ 8 ],
		} ]
	});
	$("#locationSearchForm").ajaxForm({
		beforeSubmit : function() {
			$("#locationSection").mask("Searching...");
		},
		success : function(data) {
			searchResults = data.results;
			resultsDataTable.clear();

			if (data.exceededMaxResults) {
				$("#exceededMaxResults").show();
				$("#totalResults").text(data.totalResults);
			} else {
				$("#exceededMaxResults").hide();
			}

			searchResults.forEach(function(row) {
				addRowToTable(resultsDataTable, row);
			});

			resultsDataTable.draw();

			var length = data.results.length;

			if (length == 0) {
				$("#searchFilterToggle").prop('checked', true);
			} else {
				$("#searchFilterToggle").prop('checked', false);
			}

			$("#searchFilterToggle").trigger('change');

			$("#locationSection").unmask();

		}
	});
	$('.locationFilter input').attr('placeholder', 'Search Location Results');
	activateWorkbookSearch();
	activateLocationSearch();
	$('.workbookCheckboxCol input[type="checkbox"]').on('change', function() {
		$('.workbookCheckboxCol input[type="checkbox"]').not(this).prop('checked', false);
	});
	// activateLocationSearch();
	$('#assignButton').on('click', function() {
		var locationIds = [];
		$.each($("input[name='locationID']:checked"), function() {
			locationIds.push($(this).val());
		});

		var workbookIds = [];
		var checkBox = $(".workbookCheckboxCol input[type='checkbox']:checked");
		var ischecked = checkBox.is(':checked');
		if (ischecked) {
			workbookIds.push(checkBox.val());
			wbookId = checkBox.val();
			console.log("the selected chekbox workbook id is " + wbookId);
			// highlightWBRow();
		}

		if (workbookIds.length < 1) {
			alert('Please first select a workbook');
		} else {
			if (workbookIds.length > 1) {
				alert('Please select only one workbook');
			} else {
				if (locationIds.length < 1) {
					alert('Please select at least one location');
				} else {
					var str = workbookIds[0];
					var res = str.split("|");
					var ret = confirm('Are you sure you want to assign ' + locationIds.length + ' location(s) to the work book');

					if (ret) {

						var url = '/workbook/' + res[0] + '/assignLocations.htm';

						$.ajax({
							type : "POST",
							url : toRelativeURL(url),
							dataType : "json",
							contentType : "application/json; charset=UTF-8",
							data : JSON.stringify(locationIds),
							success : function(successful) {
								if (successful) {
									wbID = checkBox.val();
									addToCount(wbID, locationIds.length);
									activateLocationSearch();
									$("#viewForm").html("<h1>Locations Assigned Successfully !</h1>");
									$.magnificPopup.open({
										items : {
											src : '#viewForm',
											modal : true,
											type : 'inline'
										}
									});

								} else {
									alert('Error while assigning locations to work book !');
								}
							}
						});

					}
				}
			}
		}
	});

	$('#unassignButton').on('click', function() {
		var locationIds = [];
		$.each($("input[name='locationID']:checked"), function() {
			locationIds.push($(this).val());
		});

		var workbookIds = [];
		var checkBox = $(".workbookCheckboxCol input[type='checkbox']:checked");
		var ischecked = checkBox.is(':checked');
		if (ischecked) {
			workbookIds.push(checkBox.val());
			wbookId = checkBox.val();
			console.log(wbookId);
		}

		if (workbookIds.length < 1) {
			alert('Please first select a workbook');
		} else {
			if (workbookIds.length > 1) {
				alert('Please select only one workbook');
			} else {
				if (locationIds.length < 1) {
					alert('Please select at least one location');
				} else {
					var str = workbookIds[0];
					var res = str.split("|");
					var ret = confirm('Are you sure you want to unassign ' + locationIds.length + ' location(s) from the work book? ');

					if (ret) {
						var url = '/workbook/' + res[0] + '/unassignLocations.htm';
						$.ajax({
							type : "POST",
							url : toRelativeURL(url),
							dataType : "json",
							contentType : "application/json; charset=UTF-8",
							data : JSON.stringify(locationIds),
							success : function(successful) {
								if (successful) {
									wbID = checkBox.val();
									console.log("workbook id " + wbID);
									activateLocationSearch();
									console.log(wbID);
									changeCount(wbID, locationIds.length);
									$("#viewForm").html("<h1>Locations removed from work book  Successfully !</h1>");
									$.magnificPopup.open({
										items : {
											src : '#viewForm',
											modal : true,
											type : 'inline'
										}
									});

								} else {
									$("#viewForm").html("<h1>Error while assigning locations to work book </h1>");
									$.magnificPopup.open({
										items : {
											src : '#viewForm',
											modal : true,
											type : 'inline'
										}
									});
								}
							}
						});

					}
				}
			}
		}
	});
 


});

function closePopUp() {
	$('#cancelButton').on('click', function() {
		$.magnificPopup.close();
	});
}
function addToCount(e, f) {
	console.log("addToCount()")
	var currentCount = +($('span#count-' + e).text());
	console.log("the span count is " + currentCount);
	var arrLength = f;
	console.log("the array length is " + arrLength);
	var result = currentCount + arrLength;
	console.log("the result is " + result);
	currentCount = result;
	console.log("the current count is  " + currentCount);
	$('span#count-' + e).text(result);
	console.log("the new span count is" + $('span#count-' + e).text());
}

function changeCount(e, f) {
	console.log("changeCount()")
	var currentCount = +($('span#count-' + e).text());
	console.log("the span count is " + currentCount);
	var arrLength = f;
	console.log("the array length is " + arrLength);
	var result = currentCount - arrLength;
	console.log("the result is " + result);
	currentCount = result;
	console.log("the current count is  " + currentCount);
	$('span#count-' + e).text(result);
	console.log("the new span count is" + $('span#count-' + e).text())

}

function setCount(e,f)
{
	var count = $('span#count-' + e).text()
	if(count == 0){
		$('span#count-' + e).text(f)
	}
	$('span#count-' + e).text(f);
}

function initializeJsTree() {
	$('#workbookTree').jstree({
		'core' : {
			"check_callback" : true,
			'data' : {
				'url' : "/phisUI/workbook/getWorkbookFolders.htm",
				'data' : function(node) {
					return {
						'id' : node.id
					};
				}
			}
		},
		"plugins" : [ "search", "sort", "unique", "wholerow" ]
	});
}

function initializeWBContextMenu(elementID) {
	context.init({
		preventDoubleContext : false
	});
	wbID = elementID;
	var archivedRow = $('#row_'+wbID).hasClass('archived-row-'+wbID);
	$("input#workbookID").val(wbID);
	// $('#messageBox').hide();
	context.attach('.workbooksMenu', [ {
		header : 'Options'
	}, {
		text : 'Edit',
		action : function(e, selector) {
			editWorkbook(wbID);
		}

	}, {
		text : 'Delete',
		action : function(e, selector) {
			deleteWorkbook(wbID);
		}
	}, {
		text : 'Archive' ,
		action : function(e, selector) {

			console.log(archivedRow);
			if(archivedRow==true){
				$("#viewForm").html("<h1>NOTICE! This work book is currently archived. Please contact support for further assistance.</h1>");
				$.magnificPopup.open({
					items : {
						src : '#viewForm',
						modal : true,
						type : 'inline'
					}
				});
			}else{
				archiveOrUnArchiveWorkbook(wbID, true);
			}

		}
	}, {
		text : 'UnArchive',
		action: function(e, selector){
			if(archivedRow == true)
			{
				archiveOrUnArchiveWorkbook(wbID, false);
			}else{
				$("#viewForm").html("<h1>NOTICE! This work book is currently unarchived. Please contact support for further assistance.</h1>");
				$.magnificPopup.open({
					items : {
						src : '#viewForm',
						modal : true,
						type : 'inline'
					}
				});
			}

		}
	},{
		divider : true
	}, {
		text : 'Highlight Associated Locations',
		action : function(e, selector) {
			highlightLocation(wbID);
		}
	}, {
		text : 'Show Associated Locations',
		action : function(e, selector) {
			showLocations(wbID);
		}
	}, {
		text : 'Print',
		action : function(e, selector) {
			printWorkbook(wbID);
		}
	}, ]);

}
function removeArrayBracket(e) {
	var row = $('#row_' + e).attr('class');
	var res = row.replace(/[ | ]/, '');
}

function removeHighlight(e) {
	$('#row_' + e).removeClass('selected');
}
function highlightLocation(e) {
	unhighlight($(".location-row"));
	highlight($('.workbook-id-' + e));
}
function highlightWorkbook(workbookIDs) {
	try {
//		console.log($('.location-id-' + [ e ]))
		unhighlight($(".workbook-row"));

		$.each(workbookIDs, function(index, workbookID){
			highlight($("." + workbookID));
		})


	} catch (e) {
		console.log(e);
		console.log($('.' + [ workbookIDs ]))

	}
}
function unhighlight($element) {
	if ($element)
		$element.removeClass('selected');
}
function highlight($element) {
	if ($element)
		$element.addClass('selected');
}

function showArchivedWorkbooks(e) {
	$('[name="archived"]').val(e);
	$('#showUnarchived').show();
	$('#showArchived').hide();
	activateWorkbookSearch();
}

function showLocations(e) {
	console.log("showlocations function workbookID =" + e);
	$('[name="workbookID"]').val(e);
	var inputvalue = $('[name="workbookID"]').val();
	console.log("workbookID input value = " + inputvalue);
	activateLocationSearch();
}

function activateLocationSearch() {
	$('#locationSearchForm').trigger("submit");
}
function activateWorkbookSearch() {
	$('#workbookSearchForm').trigger("submit");
}

function changeCountryCode(selectDom, countryCode) {
	$(selectDom).empty();
	$(selectDom).append($('<option/>', {
		value : '',
		text : '--- Select a State ---'
	}));
	$.ajax({
		url : "/phisUI/statesService/" + countryCode + "/states.htm",
		success : function(data) {
			data.forEach(function(state) {
				$(selectDom).append($('<option/>', {
					value : state.stateCode,
					text : state.stateName
				}));
				$(selectDom).trigger("chosen:updated");
			});
		}
	});
}
function changeStateCode(selectDom, stateCode) {
	$(selectDom).empty();
	$(selectDom).append($('<option/>', {
		value : '',
		text : '--- Select a County ---'
	}));
	return $.ajax({
		url : "/phisUI/statesService/" + stateCode + "/counties.htm",
		success : function(data) {
			data.forEach(function(county) {
				$(selectDom).append($('<option/>', {
					value : county.countyID,
					text : county.countyName
				}));
				$(selectDom).trigger("chosen:updated");
			});
		}
	});
}

function changeTemplateType(selectDom, templateType) {
	$(selectDom).empty();
	$(selectDom).append($('<option/>', {
		value : '',
		text : '--- Select a Template Name ---'
	}));
	$.ajax({
		url : "/phisUI/workbook/" + templateType + "/templates.htm",
		success : function(data) {
			data.forEach(function(template) {
				$(selectDom).append($('<option/>', {
					value : template.templateName,
					text : template.fullTemplateName
				}));
				$(selectDom).trigger("chosen:updated");
			});
		}
	});
}

function addRowToTable(table, row) {
	locID = row.locationID;
	var editLocation = $('<a>', {
		href : toRelativeURL("/location/" + row.locationID + "/edit.htm", true)
	});
	var contextMenu = $('<a>', {
		href : "#"
	});
	contextMenu.addClass('locationsMenu');

	$('<img>', {
		src : toRelativeURL('/images/context_menu.png', true),
	}).appendTo(contextMenu);

	var addrLink = $('<a>', {
		href : toRelativeURL("/location/addressBook.htm?locationID=" + row.locationID, true)
	});

	$('<img>', {
		src : toRelativeURL('/images/ICON_address_book.gif', true),
		alt : 'edit address book'
	}).appendTo(addrLink);

	var hierarchyLink = "<a href = \"#\" onclick=\"showHierarchy(" + row.locationID + ")\">" + "<img src=\"/phisUI/images/ICON_tree.gif\" alt='show hierarchy'/></a>";

	var locDetailLink = $('<a>', {
		href : toRelativeURL("/location/" + row.locationID + "/detail.htm")
	}).html(row.locationName).wrap('<div>').parent().html();

	var numSitesLink = $('<a>', {
		href : toRelativeURL("/location/" + row.locationID + "/detail.htm")
	}).html(row.numberOfSites).wrap('<div>').parent().html();

	var checkbox = "<input type=\"checkbox\" name=\"locationID\"\n" + "id=\"locationID\"\n" + "	value=\"" + row.locationID + "\" />";

	for (var i = 0; i < row.length; i++) {
		if (row[i] == null)
			row[i] = "";
	}
	// var edit = editLink[0].outerHTML + "&nbsp;" + addrLink[0].outerHTML +
	// "&nbsp;" + hierarchyLink;
	contextMenu.attr('onclick', 'initializeContextMenu()');
	contextMenu.attr('id', row.locationID);
	var actions = contextMenu[0].outerHTML;

	table.row.add([ actions, row.locationID, locDetailLink, numSitesLink, row.address, row.officeName, row.validationStatus, checkbox, row.workbookIDs, row.createdDate ]);
}

function initializeContextMenu() {
	/**
	 * Locations Context Menu
	 */
	context.init({
		preventDoubleContext : false
	});
	$('a.locationsMenu').click(function() {
		locID = $(this).attr('id');
		// $("input#locationID").val(locID);
		// $('#messageBox').hide();
	});
	context.attach('.locationsMenu', [ {
		header : 'Options'
	}, {
		text : 'Edit',
		href : toRelativeURL("/location/" + locID + "/edit.htm", true)

	}, {
		text : 'Location Address Book',
		href : toRelativeURL('/location/addressBook.htm?locationID=' + locID, true),
	}, {
		divider : true
	}, {
		text : 'Hierarchy',
		action : function(e, selector) {
			goToHierarchy(locID);
		}
	}, {
		text : 'Highlight Associated Workbooks',
		action : function(e, selector) {
			highlightWorkbook(locID);
		}
	}, ]);

}
function goToHierarchy(e) {

	var hLink = "/phisUI/workbook/hierarchy.htm?locationId=" + e;
	$("#viewForm").html("<h1>Loading...</h1>");
	$("#viewForm").load(hLink);
	$.magnificPopup.open({
		items : {
			src : '#viewForm',
			modal : true,
			type : 'inline'
		}
	});
	return false;

}
function createWorkbook() {
	var createWorkbookLink = toRelativeURL("/workbook/workbookManage.htm");
	$("#viewForm").html("<h1>Loading...</h1>");
	$("#viewForm").load(createWorkbookLink);
	$.magnificPopup.open({
		items : {
			src : '#viewForm',
			modal : true,
			type : 'inline'
		}
	});
	return false;
}

function editWorkbook(e) {
	var editWorkbookLink = toRelativeURL("/workbook/" + e + "/editWorkbook.htm");
	$("#viewForm").html("<h1>Loading...</h1>");
	$("#viewForm").load(editWorkbookLink);
	$.magnificPopup.open({
		items : {
			src : '#viewForm',
			modal : true,
			type : 'inline'
		}
	});
	return false;
}
function archiveOrUnArchiveWorkbook(elementID, archived){
	var ret= confirm("Are you sure you want to Archive this work book ?");
	if(ret){
		var archiveLink = "/phisUI/workbook/archiving.htm?workbookID="+elementID + "&archived="+ archived;
		$.ajax({
			type:"Post",
			url: archiveLink,
			success:function(data){
				if(data.successful == true){
					try{
						activateWorkbookSearch();
						$("#viewForm").html("<h1>Work book updated Successfully!</h1>");
						$.magnificPopup.open({
							items : {
								src : '#viewForm',
								modal : true,
								type : 'inline'
							}
						});
					}catch(e){
						console.log(e);
					}
				}

			}
		});
	}


}

function createWorkbookFolder() {
	var createFolder = toRelativeURL("/workbook/createFolder.htm");
	$("#viewForm").html("<h1>Loading...</h1>");
	$("#viewForm").load(createFolder);
	$.magnificPopup.open({
		items : {
			src : '#viewForm',
			modal : true,
			type : 'inline'
		}
	});
	return false;
}

function printWorkbook(e) {
	var URL = "/phisUI/workbook/print.htm?workBookId=" + e;
	window.open(URL, null, "toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1200,height=600");
}

function linktoParent(e) {
	$.ajax({
		url : toRelativeURL('/workbook/submitHierarchy.htm?locationId=' + e, true),
		type : 'POST',
		contentType : "application/json; charset=UTF-8",
		dataType : "json",
		success : function(successful) {
			if (successful) {
				$("#viewForm").html("<h1>Loading...</h1>");
				$("#viewForm").load("/phisUI/workbook/" + e + "/linktoParent.htm");
				$.magnificPopup.open({
					items : {
						src : '#viewForm',
						modal : true,
						type : 'inline'
					}
				});
				return false;
			} else {
				alert('Can not delete work book because it still contains ' + locCount + ' locations.');
			}
		}
	});
}
function createOnMap() {
	$.magnificPopup.open({
		items : {
			src : '#viewFormMap',
			modal : true,
			type : 'inline'
		}
	});
	return false;
}

function createLocation() {
	window.location.replace('/phisUI/location/newLocation.htm?isAjaxRequest=' + false);

}
function removeWorkbook(elementID) {
	// delete row using row ID
	$("#workbookTable").DataTable().row('#row_' + elementID).remove();
	$("#workbookTable").DataTable().draw();
}

// handle user click delete on a work book
function deleteWorkbook(e) {
	var ret = confirm('Are you sure you want to delete this workbook?');
	var url = "/workbook/" + e + "/delete.htm";

	if (ret) {
		$.ajax({
			type : "POST",
			url : toRelativeURL(url),
			dataType : "json",
			success : function(successful) {
				if (successful) {
					removeWorkbookFromTable(e);
					$("#viewForm").html("<h1>Workbook Deleted Successfully ! </h1>");
					$.magnificPopup.open({
						items : {
							src : '#viewForm',
							modal : true,
							type : 'inline'
						}
					});

				} else {
					$("#viewForm").html("<h1>Can not delete work book. This workbook is currently in use.</h1>");
					$.magnificPopup.open({
						items : {
							src : '#viewForm',
							modal : true,
							type : 'inline'
						}
					});
					showLocations(e);
				}
			}
		});
	}

}
function selectOneWorkbookCheckbox() {
	$('.workbookCheckboxCol input').change(function(e) {

		if ($(this).is(":checked")) {
			$(".workbookCheckboxCol input").prop("checked", false);
			$(this).prop("checked", true);
		}
	});

}
function setHiddenField(e) {
	var idResult;
	$('[name="workbookID"]').val(e);
	if (e == null) {
		idResult = "field is null";
	}
	console.log("the hidden workbook field is now " + idResult);
}

function getActiveLocationCount(e){
	var link = "/phisUI/workbook/locationCount.htm?workbookID="+e;
	var result = null; 
	return $.ajax({
		type: 'GET',
		url: link,
		success:function(response){

			setCount(e,response);	// console.log(result);
		}
	});
	
}


function addRowToWorkbookTable(table, row) {

	wBID = row.workbookID;
	var editWorkbook;
	var contextMenu = $('<a>', {

	});
	contextMenu.addClass('workbooksMenu');
	$('<img>', {
		src : toRelativeURL('/images/context_menu.png', true),
	}).appendTo(contextMenu);

	var count = "count-" + row.workbookID;
	var locationsLink = row.workbookName + "<a href = \"#\" onclick=\"showLocations(" + wBID + ")\">(<span id=" + count + ">"+row.locationCount+"</span>)</a>";
	var workbookName = locationsLink;
	var checkbox = "<input id=\"workbookCxBx\" type=\"checkbox\"\n" + "	value=" + row.workbookID + " " + "/>";

	for (var i = 0; i < row.length; i++) {
		if (row[i] == null)
			row[i] = "";
	}
	contextMenu.attr('onclick', 'initializeWBContextMenu(' + row.workbookID + ')');
	contextMenu.attr('id', row.workbookID);
	var actions = contextMenu[0].outerHTML;

	console.log("row wbid " + row.workbookID);
	console.log("row chbox " + checkbox);

	var folderName='All Work Books';

	if(row.folder && row.folder!=null){
		folderName=row.folder.folderName;
	}
	var archivedStatus = row.archivedStatus;

	table.row.add([ row.workbookID, actions, workbookName, folderName, row.archivedStatus,checkbox, row.createdDate ]);
}
function removeWorkbookFromTable(e) {
	// delete row using row ID
	
	$('#workbookTable').DataTable().row('#row_' + e).remove();
	$('#workbookTable').DataTable().draw();
}

