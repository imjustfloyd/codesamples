package gov.usda.aphis.iphis.data.dao.impl;

import gov.usda.aphis.iphis.data.dao.WorkbookDAO;
import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.util.CriteriaUtil;
import gov.usda.aphis.iphis.data.util.SearchUtil;
import gov.usda.aphis.iphis.results.PartialResults;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WorkbookDAOImpl extends BaseDAOImpl<WorkbookDTO> implements WorkbookDAO {

	private final int queryCount = 50;

	protected WorkbookDAOImpl() {

		super(WorkbookDTO.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public PartialResults<WorkbookDTO> getAllWorkbooks() {
		Query query = getSession().createQuery("SELECT w FROM WorkbookDTO w");

		return new PartialResults<WorkbookDTO>((List<WorkbookDTO>) query.list(), queryCount);
	}

	@Override
	public PartialResults<WorkbookDTO> getWorkbooks() {
		// create a new criteria for retrieving workbooks
		Criteria criteria = getSession().createCriteria(WorkbookDTO.class);

		return SearchUtil.getPartialResults(criteria, queryCount);
	}

	@Override
	public WorkbookDTO getWorkbookByID(long workbookID) {
		return getId(workbookID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkbookDTO> getWorkbooksInOffices(final boolean archived, final Long... officeIDs) {
		Query query = getSession().createQuery("FROM WorkbookDTO w WHERE (w.archived =:archived AND w.officeID IN :officeIDs) ORDER BY w.createdDate DESC");
		query.setParameterList("officeIDs", officeIDs);
		query.setParameter("archived", archived);
		return query.list();
	}

	@Override
	public PartialResults<WorkbookDTO> searchWorkbooks(String workbookName, Long[] officeIDs) {

		System.out.println("inside service " + workbookName + "--- " + officeIDs);
		// create a new criteria for retrieving workbooks
		Criteria criteria = getSession().createCriteria(WorkbookDTO.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		if (null != workbookName && !workbookName.isEmpty()) {
			criteria.add(CriteriaUtil.like("name", workbookName));
		}

		if (officeIDs != null) {
			criteria.add(Restrictions.in("officeID", officeIDs));
		}

		// make sure the workbook is active
		criteria.add(Restrictions.eq("active", true));
		criteria.add(Restrictions.eq("archived", false));

		// order the list by the name in ascending order
		// criteria.addOrder(Order.asc("folder.name"));

		// order the list by the name in ascending order
		criteria.addOrder(Order.asc("name"));

		// Not using any search limit
		return SearchUtil.getPartialResults(criteria, 100);
	}

	@Override
	public PartialResults<WorkbookDTO> searchArchived(Long[] officeIDs) {
		Criteria criteria = getSession().createCriteria(WorkbookDTO.class);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.add(Restrictions.eq("archived", true));

		if (officeIDs != null) {
			criteria.add(Restrictions.in("officeID", officeIDs));
		}

		// order the list by the name in ascending order
		criteria.addOrder(Order.asc("name"));

		// Not using any search limit
		return SearchUtil.getPartialResults(criteria, 100);

	}

	@Override
	public WorkbookDTO searchByWorkbookId(Long workbookId) {

		Criteria criteria = getSession().createCriteria(WorkbookDTO.class);

		// restrict the workbook by Id
		criteria.add(Restrictions.eq("workbookID", workbookId));

		// make sure the workbook is active
		criteria.add(Restrictions.eq("active", true));

		return (WorkbookDTO) criteria.list().get(0);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<WorkbookDTO> get(Long... ids) {
		Query query = getSession().createQuery("from WorkbookDTO where workbookID  in :ids");
		query.setParameterList("ids", ids);
		return query.list();
	}

	@Override
	public void deleteByID(long workbookID)
	{
		Query query = getSession().createQuery("DELETE FROM WorkbookDTO w WHERE w.workbookID = :workbookID");
		query.setParameter("workbookID", workbookID);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getLocationIdsForWorkbook(Long workbookId) {
		Query query = getSession().createQuery("SELECT l.locationID from WorkbookDTO w join w.locations l where w.workbookID=:workbookID and l.active=:active");
		query.setParameter("workbookID", workbookId);
		query.setParameter("active", true);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long getActiveLocationCountByWorkbook(Long workbookID){
		Query query = getSession().createQuery("SELECT COUNT(l) FROM WorkbookDTO w JOIN w.locations l WHERE w.workbookID=:workbookID and l.active=:active ");
		query.setParameter("workbookID", workbookID);
		query.setParameter("active", true);
		Long count = (Long) query.uniqueResult();

		return count;
	}
}