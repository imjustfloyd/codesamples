package gov.usda.aphis.iphis.domain.workbook;

import gov.usda.aphis.iphis.data.dto.scheduling.WorkbookDTO;
import gov.usda.aphis.iphis.data.dto.web.LocationDTO;
import gov.usda.aphis.iphis.domain.web.Location;
import gov.usda.aphis.iphis.util.GenericObjectWrapper;
import gov.usda.aphis.iphis.util.Preconditions;

import java.io.Serializable;
import java.util.List;

public class Workbook implements Serializable
{
	private static final long serialVersionUID = 8299038341007747874L;

	private final WorkbookDTO workbookDTO;
	private Long activeLocationCount;
	public Workbook()
	{
		this(new WorkbookDTO());
	}

	public Workbook(final WorkbookDTO workbookDTO)
	{
		Preconditions.checkNotNull(workbookDTO);
		this.workbookDTO = workbookDTO;
	}

	public WorkbookDTO getWorkbookDTO() {
		return workbookDTO;
	}

	public long getWorkbookId() {
		return workbookDTO.getWorkbookID();
	}

	public String getWorkbookName() {
		return workbookDTO.getName();
	}

	public long getOfficeId() {
		return workbookDTO.getOfficeID();
	}

	public long getLocationCount() {
		if(getActiveLocationCount() == null){
			return workbookDTO.getLocationSize();
		}else{
			return getActiveLocationCount();

		}

	}

	public Long getActiveLocationCount() {
		return activeLocationCount;
	}

	public void setActiveLocationCount(Long activeLocationCount) {
		this.activeLocationCount = activeLocationCount;
	}

	public String getDescription(){
		return workbookDTO.getDescription();
	}

	public boolean isArchived(){
		return workbookDTO.isArchived();
	}

	public WorkbookFolder getFolder() {
		if (null != workbookDTO.getFolder()) {
			return new WorkbookFolder(workbookDTO.getFolder());
		} else {
			return null;
		}
	}
	public List<gov.usda.aphis.iphis.domain.web.Location> getLocations(){

		if(null != workbookDTO.getLocations()){
			return new GenericObjectWrapper<Location, LocationDTO>(Location.class, LocationDTO.class).wrap(workbookDTO.getLocations());
		}
		return null;
	}
}
