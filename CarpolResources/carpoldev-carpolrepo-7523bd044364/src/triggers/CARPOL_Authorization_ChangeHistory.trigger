trigger CARPOL_Authorization_ChangeHistory on Authorizations__c (after update) {
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Authorizations__c');
}