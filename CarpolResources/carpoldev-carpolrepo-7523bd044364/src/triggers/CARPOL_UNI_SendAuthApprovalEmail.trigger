/***********************************************************
//Trigger Name: CARPOL_UNI_SendAuthApprovalEmail
//Purpose: Send email notification on attached PDF of approved authorizations
//Author: Sahir Ali
//Cretaed Date: 03/03/2016
***********************************************************/

trigger CARPOL_UNI_SendAuthApprovalEmail on Attachment (after insert, after update) {
    
    //send approved and issued authorization email after a PDF/Letter has been attached.
    String objectAPIName;
    Map<Id, Authorizations__c> mAuth;
    
    for(Attachment att: Trigger.New)
    {
        System.debug(att.ParentId + ' this got passed');
        if(att.ParentID!=null )
        {
            //check sobject type of the parentID
            //if it is authorization, proceed
            objectAPIName = att.ParentID.getSObjectType().getDescribe().getName();
            if(objectAPIName == 'Authorizations__c')
            {
                //send email 
                mAuth = new Map<Id, Authorizations__c>([Select id, Application__c, Authorization_Type__c, Name, status__c, Documents_Sent_to_Applicant__c, AC_Applicant_Email__c
                                           from Authorizations__c where Id=:att.parentId]);
                system.debug(mAuth);
                CARPOL_UNI_SendIssuedAttachment runClass =  new CARPOL_UNI_SendIssuedAttachment(mAuth);
                //runClass.sendIssuedAttachment();
                runClass.sendIssuedAttachmentWithoutWFT();
            }
        }
    }
    

}