trigger CARPOL_Applicant_Attachment_Count on Attachment (after insert, after update, after delete, after undelete) {
  Map<Id,List<Attachment>> parent = new Map<Id,List<Attachment>>();
  set<id> attids = new set<id>();

  if(Trigger.new<>null){
       for(Attachment c:Trigger.new){
           Applicant_Attachments__c l;
           if(c.ParentId != null)
               attids.add(c.parentid);
       }
           
   }else if(Trigger.old != null){
       for(Attachment c:Trigger.old){
           if(c.ParentId<>null)      
               attids.add(Trigger.oldMap.get(c.id).parentid);
       }
   }
   if(attids.size()>0){
       try{
           List<Attachment> a = new List<Attachment>();
           Map<id,Applicant_Attachments__c> aAttMap = new Map<id,Applicant_Attachments__c>([select Id, Status__c, Total_Files__c from Applicant_Attachments__c where id IN: attids]);
           a = [select id,parentid from Attachment where parentid IN:attids];
           
           for(Attachment at: a){
               List<Attachment> llist = new List<Attachment>();
               if(parent.get(at.parentid) == null){
                   llist = new List<Attachment>();
                   llist.add(at);
                   parent.put(at.parentid,llist);
               }else if(parent.get(at.parentid) != null){
                   llist = new List<Attachment>();
                   llist = parent.get(at.parentid);
                   llist.add(at);
                   parent.put(at.parentid,llist);
               }
           }
           
           for(Id i: attids){
               if(aAttMap.get(i) != null && parent.get(i) != null){
                  aAttMap.get(i).Total_Files__c = parent.get(i).size(); 
               
               }else if(aAttMap.get(i) != null && parent.get(i) == null){
                  aAttMap.get(i).Total_Files__c = 0; 
               }
           }
           update aAttMap.values();
           System.Debug(aAttMap.values());
       }catch(Exception e){}
    }

}