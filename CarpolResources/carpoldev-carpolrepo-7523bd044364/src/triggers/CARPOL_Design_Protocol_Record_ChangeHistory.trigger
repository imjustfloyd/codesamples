trigger CARPOL_Design_Protocol_Record_ChangeHistory on Design_Protocol_Record__c (after update) {    
    GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Design_Protocol_Record__c');    
}