trigger CARPOL_GetFieldAPINames on Program_Line_Item_Field__c (before insert,before update) {
    
     Map <String, String> labelToAPIName = new Map <String, String> ();
     
     system.debug('Program Line Item Section ###'+trigger.new[0].Program_Line_Item_Section__c);
     
    // String destination = [select Id,Destination_Object__c from Program_Line_Item_Section__c  where Id =: trigger.new[0].Program_Line_Item_Section__c].Destination_Object__c;
     // ALL APIS ARE IN AC_C Destination object is not used for the line item fields. Only for new record save of applicant contacts.
     
     List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();  
     
     Map<String,String> objName = new Map<String,String>();

     for(Schema.SObjectType f : gd)
    {
      if(f.getDescribe().isCustom())
      {
        objName.put(f.getDescribe().getLabel(),f.getDescribe().getName()); 
      }
        
    }

     System.debug('Object Names'+objName);
     
    // String objApi = objName.get(destination);
     
     Map<String, Schema.SObjectField> fieldsMap =  new Map<String,Schema.SObjectField>(); 
    
   // if(objApi == 'AC__c')
    //{
      fieldsMap = Schema.SObjectType.AC__c.fields.getMap();
    //}
    /*
    if(objApi == 'Application__c')
    {
        fieldsMap = Schema.SObjectType.Application__c.fields.getMap();     
    }
    if(objApi == 'authorizations__c')
    {
        fieldsMap = Schema.SObjectType.Authorizations__c.fields.getMap();     
    }
    */
    for (Schema.SObjectField field : fieldsMap.values())
    {
    labelToAPIName.put(field.getDescribe().getLabel(), field.getDescribe().getName());
    system.debug('labelToAPIName'+labelToAPIName);
    }
    system.debug('labelToAPIName'+labelToAPIName);
    String Name = trigger.new[0].Name;
    system.debug('Name'+Name);    
    String APINAME = labelToAPIName.get(Name);
    system.debug('APINAME'+APINAME);
    trigger.new[0].Field_API_Name__c = APINAME;


}