trigger CARPOL_Self_Reporting_ChangeHistory on Self_Reporting__c (after update) {
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Self_Reporting__c');
}