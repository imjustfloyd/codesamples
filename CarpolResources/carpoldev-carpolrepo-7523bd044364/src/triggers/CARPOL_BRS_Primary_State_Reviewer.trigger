trigger CARPOL_BRS_Primary_State_Reviewer on Contact (before insert,before update) {

for (Contact rev: System.Trigger.new) {
    if(rev.RecordType.Name == 'State SPRO' || rev.RecordType.Name == 'State SPHD'){
        /// Check if any existing primary
        List<Contact> cnt = [SELECT ID, Name,Contact_checkbox__c FROM Contact WHERE RecordType.Name = :rev.RecordType.Name AND Contact_checkbox__c = true AND State__c = :rev.State__c LIMIT 1];
        // if not let save as primary
        if(cnt.size() > 0 && rev.Contact_checkbox__c == true)
        {
            //throw error
            rev.addError('A Primary Reviewer for this state already exist, please check your submitted records.'); 
        } 
    }
 }
}