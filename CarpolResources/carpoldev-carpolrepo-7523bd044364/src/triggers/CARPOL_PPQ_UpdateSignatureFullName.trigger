trigger CARPOL_PPQ_UpdateSignatureFullName on Signature__c (after insert, after update) {
Set<ID> setScmIDs = new Set<ID>();
List<Signature__c> scmToUpdate = new List<Signature__c>();

String A1= '';
String A2= '';
String A3= '';
String A4= '';
String A5= '';
String A6= '';
String A7= '';
String A8= '';
String A9= '';
String A10= '';
String A11= '';
String A12= '';
String A13= '';
String A14= '';
String A15= '';
String A16= '';
String A17= '';
String A18= '';
String A19= '';
String A20= '';
String fullSig = '';

if(Trigger.isInsert){
for (Signature__c scm : Trigger.new) {
if (scm.Answer_1__c!= null
  || scm.Answer_2__c!= null
  || scm.Answer_3__c!= null
  || scm.Answer_4__c!= null
  || scm.Answer_5__c!= null
  || scm.Answer_6__c!= null
  || scm.Answer_7__c!= null
  || scm.Answer_8__c!= null
  || scm.Answer_9__c!= null
  || scm.Answer_10__c!= null
  || scm.Answer_11__c!= null
  || scm.Answer_12__c!= null
  || scm.Answer_13__c!= null
  || scm.Answer_14__c!= null
  || scm.Answer_15__c!= null
  || scm.Answer_16__c!= null
  || scm.Answer_17__c!= null
  || scm.Answer_18__c!= null
  || scm.Answer_19__c!= null
  || scm.Answer_20__c!= null){
  setScmIDs.add(scm.Id);
  }
}
}
else if (Trigger.isUpdate){
for (Signature__c scm : Trigger.new) {
  Signature__c oldscm = Trigger.oldMap.get(scm.Id);
  if(scm.Answer_1__c!=oldscm.Answer_1__c
  || scm.Answer_2__c!=oldscm.Answer_2__c
  || scm.Answer_3__c!=oldscm.Answer_3__c
  || scm.Answer_4__c!=oldscm.Answer_4__c
  || scm.Answer_5__c!=oldscm.Answer_5__c
  || scm.Answer_6__c!=oldscm.Answer_6__c
  || scm.Answer_7__c!=oldscm.Answer_7__c
  || scm.Answer_8__c!=oldscm.Answer_8__c
  || scm.Answer_9__c!=oldscm.Answer_9__c
  || scm.Answer_10__c!=oldscm.Answer_10__c
  || scm.Answer_11__c!=oldscm.Answer_11__c
  || scm.Answer_12__c!=oldscm.Answer_12__c
  || scm.Answer_13__c!=oldscm.Answer_13__c
  || scm.Answer_14__c!=oldscm.Answer_14__c
  || scm.Answer_15__c!=oldscm.Answer_15__c
  || scm.Answer_16__c!=oldscm.Answer_16__c
  || scm.Answer_17__c!=oldscm.Answer_17__c
  || scm.Answer_18__c!=oldscm.Answer_18__c
  || scm.Answer_19__c!=oldscm.Answer_19__c
  || scm.Answer_20__c!=oldscm.Answer_20__c){
  if (scm.Answer_1__c!= null
  || scm.Answer_2__c!= null
  || scm.Answer_3__c!= null
  || scm.Answer_4__c!= null
  || scm.Answer_5__c!= null
  || scm.Answer_6__c!= null
  || scm.Answer_7__c!= null
  || scm.Answer_8__c!= null
  || scm.Answer_9__c!= null
  || scm.Answer_10__c!= null
  || scm.Answer_11__c!= null
  || scm.Answer_12__c!= null
  || scm.Answer_13__c!= null
  || scm.Answer_14__c!= null
  || scm.Answer_15__c!= null
  || scm.Answer_16__c!= null
  || scm.Answer_17__c!= null
  || scm.Answer_18__c!= null
  || scm.Answer_19__c!= null
  || scm.Answer_20__c!= null){
  setScmIDs.add(scm.Id);
  }
  }
}
}

List<Signature__c> scmList = new List<Signature__c>([Select ID, Name, Full_Signature__c, Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c, Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c From Signature__c WHERE ID IN :setScmIDs]);
System.Debug('scmList '+ scmList.size());

for(Signature__c scm : scmList){

If(scm.Answer_1__c != null){
A1 = scm.Answer_1__c+'_';
}
else {
A1 = '';}

If(scm.Answer_2__c != null){
A2 = scm.Answer_2__c+'_';
}
else {
A2 = '';}

If(scm.Answer_3__c != null){
A3 = scm.Answer_3__c+'_';
}
else {
A3 = '';}

If(scm.Answer_4__c != null){
A4 = scm.Answer_4__c+'_';
}
else {
A4 = '';}

If(scm.Answer_5__c != null){
A5 = scm.Answer_5__c+'_';
}
else {
A5 = '';}

If(scm.Answer_6__c != null){
A6 = scm.Answer_6__c+'_';
}
else {
A6 = '';}

If(scm.Answer_7__c != null){
A7 = scm.Answer_7__c+'_';
}
else {
A7 = '';}

If(scm.Answer_8__c != null){
A8 = scm.Answer_8__c+'_';
}
else {
A8 = '';}

If(scm.Answer_9__c != null){
A9 = scm.Answer_9__c+'_';
}
else {
A9 = '';}

If(scm.Answer_10__c != null){
A10 = scm.Answer_10__c+'_';
}
else {
A10 = '';}

If(scm.Answer_11__c != null){
A11 = scm.Answer_11__c+'_';
}
else {
A11 = '';}

If(scm.Answer_12__c != null){
A12 = scm.Answer_12__c+'_';
}
else {
A12 = '';}

If(scm.Answer_13__c != null){
A13 = scm.Answer_13__c+'_';
}
else {
A13 = '';}

If(scm.Answer_14__c != null){
A14 = scm.Answer_14__c+'_';
}
else {
A14 = '';}

If(scm.Answer_15__c != null){
A15 = scm.Answer_15__c+'_';
}
else {
A15 = '';}

If(scm.Answer_16__c != null){
A16 = scm.Answer_16__c+'_';
}
else {
A16 = '';}

If(scm.Answer_17__c != null){
A17 = scm.Answer_17__c+'_';
}
else {
A17 = '';}

If(scm.Answer_18__c != null){
A18 = scm.Answer_18__c+'_';
}
else {
A18 = '';}

If(scm.Answer_19__c != null){
A19 = scm.Answer_19__c+'_';
}
else {
A19 = '';}

If(scm.Answer_20__c != null){
A20 = scm.Answer_20__c+'_';
}
else {
A20 = '';}

fullSig = A1+A2+A3+A4+A5+A6+A7+A8+A9+A10+A11+A12+A13+A14+A15+A16+A17+A18+A19+A20;
scm.Full_Signature__c = fullSig.Substring(0,fullSig.length()-1);
scm.name = scm.Full_Signature__c;

scmToUpdate.add(scm);
}


update scmToUpdate;
}