trigger CARPOL_UNI_Ins_QuestionnaireTrigger on EFL_Inspection_Questionnaire__c(before insert, after insert, before update, after update, before delete, after delete) {
    if (Trigger.isBefore) //====================================================BEFORE=================================================
    {

     if (Trigger.isUpdate){
       for (EFL_Inspection_Questionnaire__c Question: trigger.new) 
            {
                if(Question.Status__c.contains('Finalize Questionnaire')){  
                   List<String> devnames = new List<String>{'EFL_BRS_Enforcement_Officer_Queue','AC_Enforcement_Officer_Queue','PPQ_Compliance_Officer','VS_Enforcement_Officer_Queue','EFL_Enforcement_Officer_Queue'};
                   List<Group> reviewerqueue = [select Id, DeveloperName from Group WHERE DeveloperName IN :devnames  AND Type = 'Queue' ];
                   String QueueDeveloperName;
                   Map<String, ID> QueueIdmap = new Map<String, ID>();
                     for(Group gp : reviewerqueue)
                     {
                        QueueIdmap.put(gp.DeveloperName,gp.ID);
                     }
                   //for(Workflow_Task__c WF:trigger.new)
                    
                         if(Question.Program__c=='BRS'){
                         QueueDeveloperName = 'EFL_BRS_Enforcement_Officer_Queue';
                         Question.OwnerId=QueueIdmap.get(QueueDeveloperName);
                         }
                         
                          if(Question.Program__c=='AC'){
                         QueueDeveloperName = 'AC_Enforcement_Officer_Queue';
                         Question.OwnerId=QueueIdmap.get(QueueDeveloperName);
                         }
                         
                          if(Question.Program__c=='PPQ'){
                         QueueDeveloperName = 'PPQ_Compliance_Officer';
                         Question.OwnerId=QueueIdmap.get(QueueDeveloperName);
                         }
                         
                          if(Question.Program__c=='VS'){
                         QueueDeveloperName = 'VS_Enforcement_Officer_Queue';
                         Question.OwnerId=QueueIdmap.get(QueueDeveloperName);
                         }
             
             
       
                    
            
            }  
     }
    }
    }
    if (Trigger.IsAfter) //====================================================AFTER=================================================
    {

        if (Trigger.isInsert) 
        {
           
        }
        
        
        
         if (Trigger.isUpdate)
         {
              
            List < EFL_Inspection_Questions__c > QuestionsToCreate = new List < EFL_Inspection_Questions__c > ();
            List < EFL_Inspection_Responses__c > OptionsToCreate = new List < EFL_Inspection_Responses__c > ();
            List < EFL_INS_Template_Question_Junction__c > TempQJunctionList = new List<EFL_INS_Template_Question_Junction__c>();
            List < ID > TemplateIDs = new List < ID > ();
             List < ID > JunctionIDs = new List < ID > ();

            Map < EFL_Inspection_Questions__c, EFL_Inspection_Questionnaire_Questions__c > templateQQuestionnaireQmap = new Map < EFL_Inspection_Questions__c, EFL_Inspection_Questionnaire_Questions__c > ();
            for (EFL_Inspection_Questionnaire__c Question: trigger.new) 
            {
                if ((Question.EFL_Inspection_Questions_Template__c != null || Question.EFL_Inspection_Questions_Template__c != '') && Question.Status__c.contains('Open')) 
                {
                    TemplateIDs.add(Question.EFL_Inspection_Questions_Template__c);
                }
                
                if (Question.Status__c.contains('Approved') && (Question.Inspection__c!=null || Question.Inspection__c!='')) 
                {
                    Inspection__c InspectionToUpdate = [SELECT ID,Name,Status__c FROM Inspection__c WHERE ID=:Question.Inspection__c];
                    InspectionToUpdate.Status__c = 'Questionnaire Approved';
                    update InspectionToUpdate;
                }
            }

            if (TemplateIDs != null || !TemplateIDs.isEmpty()) 
            {   
                TempQJunctionList = [SELECT ID, Name, Inspection_Template_Questions__c FROM EFL_INS_Template_Question_Junction__c WHERE EFL_Inspection_Questions_Template__c IN:TemplateIDs];
                for(EFL_INS_Template_Question_Junction__c Junc: TempQJunctionList){
                    JunctionIDs.add(Junc.Inspection_Template_Questions__c);
                }
                QuestionsToCreate = [SELECT ID, Name, Question__c FROM EFL_Inspection_Questions__c WHERE ID IN: JunctionIDs];
                OptionsToCreate = [SELECT ID, Name, EFL_Inspection_Questions__c, Response__c FROM EFL_Inspection_Responses__c WHERE EFL_Inspection_Questions__c IN: QuestionsToCreate];
                List < EFL_Inspection_Questionnaire_Questions__c > QuestionsToInsert = new List < EFL_Inspection_Questionnaire_Questions__c > ();
                List < EFL_Inspection_Questionnaire_Responses__c > OptionsToInsert = new List < EFL_Inspection_Questionnaire_Responses__c > ();
                for (EFL_Inspection_Questions__c Question: QuestionsToCreate) 
                {
                    EFL_Inspection_Questionnaire_Questions__c QCreate = new EFL_Inspection_Questionnaire_Questions__c();
                    QCreate.Question__c = Question.Question__c;
                    QCreate.Source_Question__c = Question.Id;
                    QCreate.EFL_Inspection_Questionnaire__c = trigger.new[0].id;
                    QuestionsToInsert.add(QCreate);
                }

                if (QuestionsToInsert != null || !QuestionsToInsert.isEmpty()) 
                {
                    system.debug('This is it ' + QuestionsToInsert);
                    insert QuestionsToInsert;
                    Map < ID, ID > templateQQuestionnaireQmapp = new Map < ID, ID > ();
                    for (EFL_Inspection_Questionnaire_Questions__c Question: QuestionsToInsert) 
                    {
                        templateQQuestionnaireQmapp.put(Question.Source_Question__c, Question.ID);
                    }

                    for (EFL_Inspection_Responses__c Opt: OptionsToCreate) 
                    {
                         system.debug('******potion'+Opt);
                         if (templateQQuestionnaireQmapp.containsKey(Opt.EFL_Inspection_Questions__c)) 
                         {
                            EFL_Inspection_Questionnaire_Responses__c OCreate = new EFL_Inspection_Questionnaire_Responses__c();
                            OCreate.Response__c = Opt.Response__c;
                            OCreate.EFL_Inspection_Questionnaire_Questions__c = templateQQuestionnaireQmapp.get(Opt.EFL_Inspection_Questions__c);
                             system.debug('******OCreate'+OCreate);
                            OptionsToInsert.add(OCreate);
                        }
                        
                        
                    }

                    if (OptionsToInsert != null || !OptionsToInsert.isEmpty()) 
                    {
                        insert OptionsToInsert;
                    }

                }
            }
         }
        
    }
}