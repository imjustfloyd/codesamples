trigger CARPOL_BRS_DesignProtocol on Design_Protocol_Record__c(before update,before insert) {    
  list<Design_Protocol_Record__c> lstconst= trigger.new;
    list<string> lstconstids = new list<string>();
    list<string> lstatts = new list<string>();
    string strprofileid = UserInfo.getProfileId();
    string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;
    string brsappstatus = '';
    string brsappid = '';
    if(BRSProfile=='BRS External Users' || BRSProfile=='System Administrator'){
        for(Design_Protocol_Record__c c:trigger.new){
            lstconstids.add(c.id);
        }
        for(Design_Protocol_Record__c c:[select id,Name,Status__c,Associate_Application__c,Associate_Application__r.Application_Status__c,Attaching_or_Entering_Design_Protocols__c from Design_Protocol_Record__c where id in: lstconstids]){
            if(c.Associate_Application__r.Application_Status__c=='Submitted')
            {
           // if(c.Status__c!='Waiting on Customer'){
                lstconst[0].addError('Application is submitted you cannot modify the design protocol records');
           // }  
           }
          if(c.Status__c=='Waiting on Customer')
          {          
                    brsappstatus = 'Waiting on Customer';
                    brsappid = c.Associate_Application__c;
            }   
    if(trigger.isUpdate)
       {
        if(c.Attaching_or_Entering_Design_Protocols__c=='Attaching')
        {
            system.debug('************Attaching***************');
             for(Attachment att:[select id from Attachment where parentid in: lstconstids])
             {
                system.debug('************Attachment Found***************');
                 lstatts.add(att.Id);    
                 }
                  if(lstatts.size()==0)
                  {     
                    system.debug('No Attachments');
                  // lstconst[0].addError('Please hit cancel and return to the previous screen to add an attachment.');

                    system.debug('*****Design Protocol Updated******');
                  }
                }
          }
          }
      
           if(brsappstatus!='')
            {
                 CARPOL_BRS_Approval.updateapp(brsappid,brsappstatus);
            }
          
             if(trigger.isInsert)
             {
            list<string> lst = new list<string>();
              for(Design_Protocol_Record__c l:trigger.new)
              {
              if(l.Associate_Application__c!=null){
                  lst.add(l.Associate_Application__c);
              }
              }
             for(Application__c a:[select id,Name,Application_Status__c from Application__c where id in:lst])
             {
             if(a.Application_Status__c=='Submitted')
             {
                 lstconst[0].addError('Application is submitted you cannot insert new Design Protocol');
             }
             }
             }   
          
             
        
       } 
       if(trigger.isbefore)
       {
         if(trigger.isupdate || trigger.isinsert){
            list<string> lstappids = new list<string>();
            map<string,string> appemails = new map<string,string>();
            //for(Design_Protocol_Record__c c: [select id,Name,Application__r.Applicant_Email__c,Application__c from Design_Protocol_Record__c ])
            for(Design_Protocol_Record__c c : trigger.new){
                if(c.Associate_Application__c!=null && c.BRS_Applicant_Response__c==true){
                    lstappids.add(c.Associate_Application__c);
  
                } 
                if(lstappids.size()>0)
                {
                    for(Application__c a: [select id,Name,Applicant_Email__c from Application__c where id in: lstappids])
                    {
                        appemails.put(a.id,a.Applicant_Email__c);                       
                    } 
                    for(Design_Protocol_Record__c c2:trigger.new)
                    {
                        if(c2.Associate_Application__c!=null && c2.BRS_Applicant_Response__c==true){ 
                            c2.BRS_Applicant_Response_Email__c = appemails.get(c2.Associate_Application__c);
                         }
                    }
                }
            }
          }
         
       }
      
}