trigger CARPOL_BRS_AuthorizationTrigger on Authorizations__c (after update) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_AuthorizationTrigger                 */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */ 
/*           Notifications,Tasks Update,                  */
/*           Status Update, Post Approval Activites       */    
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Kishore 04/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */
// Biotechnology Regulatory Services - Standard Permit

CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_AuthorizationTrigger');
 if(!dt.disable__c && CARPOL_UNI_TriggerMonitor.runCARPOL_BRS_AuthorizationTrigger) 
    { 
               
            CARPOL_UNI_TriggerMonitor.runCARPOL_BRS_AuthorizationTrigger = false;
            if(trigger.isupdate)
                { 
                    if(TRG_HLP_checkRecursive.runOnce())
                        {
                            string authrt = '';
                            string authid = '';
                            boolean iscreatelabel =  false;
                            string authautono = '';
                            date authexpdt = null;
                            integer nooflabs = 0;
                            string labHandcry = '';
                            string laboriginadd = '';
                            boolean isrevnoti =  false;
                            boolean iscreaterev =  false;
                            list<Label__c> lstlabels = new list<Label__c>();
                            // check record type of authorization
                            Map<ID, Schema.RecordTypeInfo> locrtMap = Schema.SObjectType.Location__c.getRecordTypeInfosById();
                            Map<ID, Schema.RecordTypeInfo> authrtMap = Schema.SObjectType.Authorizations__c.getRecordTypeInfosById();
                            list<AC__c> lstlineitem = new list<AC__c>();
                            list<Location__c> loctlist = new list<Location__c>();
                            // contact stpro record type
                            Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
                            Schema.SObjectType s = sObjectMap.get('Contact') ; // getting Sobject Type
                            Schema.DescribeSObjectResult resSchema = s.getDescribe() ;
                            Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
                            Id contrtId = recordTypeInfo.get('State SPRO').getRecordTypeId();//particular RecordId by  Name
                            
                            for(Authorizations__c a:trigger.new)
                                 {
                                    authid = a.id;
                                 } 
                            integer countlineitem = [select count()  from AC__c where Authorization__c=:authid];
                            if(countlineitem>0)
                            {
                                lstlineitem = [select id,Name,Authorization__c,Introduction_Type__c,Purpose_of_the_Importation__c,Hand_Carry__c,Number_of_Labels__c from AC__c where Authorization__c=:authid] ;
                                if(Trigger.isAfter)
                                    { 
                                        for(Authorizations__c a:trigger.new)
                                            {
                                                  authrt = authrtMap.get(a.RecordTypeId).getName();
                                                  if( authrt=='Biotechnology Regulatory Services-Acknowledgement' || 
                                                      authrt=='Biotechnology Regulatory Services - Courtesy Permit' ||
                                                      authrt=='Biotechnology Regulatory Services - Standard Permit')
                                                        { 
                                                            Authorizations__c oldauth = new Authorizations__c();
                                                            oldauth = trigger.oldmap.get(a.id);
                                           
                                                            if(a.BRS_Create_Labels__c==true && oldauth.BRS_Create_Labels__c==false && a.BRS_Introduction_Type__c=='Import' && ( a.Status__c=='Approved' || a.Status__c=='Issued') && (authrt=='Biotechnology Regulatory Services-Acknowledgement' || authrt=='Biotechnology Regulatory Services - Standard Permit'))
                                                                { 
                                                                      authid = a.id;
                                                                      iscreatelabel = true;
                                                                      authautono = a.Name;
                                                                      if(a.Expiry_Date__c !=null)
                                                                      authexpdt = a.Expiry_Date__c;
                                                                      if(a.BRS_Number_of_Labels__c!=null)
                                                                      {
                                                                        nooflabs = a.BRS_Number_of_Labels__c.intValue();
                                                                        labHandcry = a.BRS_Hand_Carry_For_Importation_Only__c;
                                                                      }
                                                                 }
                                             
                                                              if(a.BRS_Create_State_Review_Records__c==true && oldauth.BRS_Create_State_Review_Records__c==false && (authrt=='Biotechnology Regulatory Services-Acknowledgement' || authrt=='Biotechnology Regulatory Services - Standard Permit'))
                                                                 {
                                                                          authid = a.id;
                                                                          iscreaterev = true;
                                                                 } 
                                                               if(a.BRS_State_Review_Notification__c==true && oldauth.BRS_State_Review_Notification__c==false && (authrt=='Biotechnology Regulatory Services-Acknowledgement' || authrt=='Biotechnology Regulatory Services - Standard Permit'))
                                                                 {
                                                                          authid = a.id;
                                                                          isrevnoti = true;
                                                                  } 
                                                             
                                                               if(a.Status__c=='Disapproved')
                                                                  {
                                                                         list<Workflow_Task__c> nextWF = new list <Workflow_Task__c>();
                                                                         nextWF = [select Id,Status__c from Workflow_Task__c where Status__c IN ('Waiting') and Authorization__c=:a.Id and Defer_all_Subsequent_Tasks__c = false];
                                                                         for(Workflow_Task__c authWorkFlowVar:nextWF )
                                                                             {
                                                                                 authWorkFlowVar.Status__c = 'Deferred';
                                                                             }  
                                                                         if(nextwf.size() >0)
                                                                            {
                                                                                update  nextwf;
                                                                            }
                                                                 }
                                             
                                                        }
                                       
                                                } 
                                    // creating labels
                                        if(iscreatelabel)
                                            { 
                                                for(Location__c orl:[select id,Name,RecordTypeId from Location__c where Authorization__c=:authid])
                                                    {
                                                        string orgrtname = '';
                                                        orgrtname = locrtMap.get(orl.RecordTypeId).getName();
                                                        if(orgrtname=='Origin Location')
                                                            {
                                                                laboriginadd = orl.id;
                                                            }
                                                     }
                                                 system.debug('nooflabs#' + nooflabs );
                                                 system.debug('labHandcry#' + labHandcry );
                                                 
                                                 if(nooflabs>0 && nooflabs<999 && labHandcry=='Yes') 
                                                    {   
                                                        for(integer i=1;i<nooflabs+1;i++)
                                                            {
                                                                Label__c l = new Label__c();
                                                                l.Name = authautono+'-'+i;
                                                                l.Authorization__c = authid;
                                                                l.Notification_Expiration_Date__c = authexpdt;
                                                                l.Notification_Status__c = 'Active';
                                                                l.Status__c = 'Active';
                                                                system.debug('status#' + l.Status__c );
                                                                if(laboriginadd!='' && laboriginadd!=null)
                                                                    {
                                                                        l.Origin_Address__c = laboriginadd;
                                                                    }
                                                                lstlabels.add(l);
                                                    
                                                             }
                                                    } 
                                                    
                                                  if(nooflabs>0 && nooflabs<999 && labHandcry=='No')
                                                    {
                                                        for(integer i=1;i<nooflabs+1;i++)
                                                            {
                                                                Label__c l = new Label__c();
                                                                l.Name = authautono+'-'+i;
                                                                l.Authorization__c = authid;
                                                                l.Notification_Expiration_Date__c = authexpdt;
                                                                l.Notification_Status__c = 'Active';
                                                                l.Status__c = 'Active';
                                                                system.debug('status#' + l.Status__c );
                                                                if(laboriginadd!='' && laboriginadd!=null)
                                                                    {
                                                                        l.Origin_Address__c = laboriginadd;
                                                                    }
                                                                lstlabels.add(l);
                                                        
                                                             }
                                                     } 
                                                     system.debug('lstlabels@@@'+lstlabels);
                                                    if(lstlabels.size()>0)
                                                        {
                                                            insert lstlabels;
                                                        }
                                            }
                                
                                   }
                                
                            }
                             
                    }
            } 
 }  
    
     
// Remove Applicant Instructions from Locations , SOPs, Constructs
If(trigger.isAfter)
{    
    
    try{
    
    
 string authrt = '';
 string lirt ='';
 Map<ID, Schema.RecordTypeInfo> authrtMap = Schema.SObjectType.Authorizations__c.getRecordTypeInfosById();
 Map<ID, Schema.RecordTypeInfo> lirtMap = Schema.SObjectType.AC__c.getRecordTypeInfosById();

 //MAP<id,Authorizations__c> changedauth = new MAP<id,Authorizations__c>();
    

    Set<id> authparentids = new set<id>();
    Set<id> lineitemids = new set<id>();
    For(Authorizations__c auth: trigger.new )
    {
        Authorizations__c old = trigger.oldMAP.get(auth.id);
        authrt = authrtMap.get(auth.RecordTypeId).getName();
        if(auth.status__c != old.status__c && auth.status__c == 'Approved' && (authrt=='Biotechnology Regulatory Services-Acknowledgement' || authrt=='Biotechnology Regulatory Services - Standard Permit'))
        {
            authparentids.add(auth.id);
            system.debug('id numbers'+auth.id);            
        }
     }
   
    if(!authparentids.isempty())
    {
        list <AC__c> update_lineitem = [ SELECT Id ,RecordTypeId,Applicant_Instructions__c FROM AC__c Where Authorization__c
                                                                 in : authparentids] ;
         
        
        if(!update_lineitem.isempty())
        {
            for(AC__c li : update_lineitem)
            {
                lirt = lirtMap.get(li.RecordTypeId).getName(); 
                if(lirt == 'Biotechnology Regulatory Services - Notification')
                {
                system.debug('line item id numbers'+li.id);
                lineitemids.add(li.id);
                li.Applicant_Instructions__c = '';
                    
                }

                                

            }
           update update_lineitem; 
        }  
    list <Construct__c> update_contruct = [SELECT Id,Corrections_Required__c FROM Construct__c WHERE Line_Item__c
                                                           in : lineitemids] ;
        
        
        if(!update_contruct.isempty())
        {
            for(Construct__c cons : update_contruct)
            {
                system.debug('construct id numbers'+cons.id);
                cons.Corrections_Required__c = '';                
            }
           update update_contruct; 
        }  
    
    list <Design_Protocol_Record__c> update_SOP = [SELECT Id,Corrections_Required__c FROM Design_Protocol_Record__c WHERE Line_Item__c
                                                           in :lineitemids] ;
        if(!update_SOP.isempty())
        {
            for(Design_Protocol_Record__c SOP : update_SOP)
            {
                system.debug('SOP id numbers'+ SOP.id);
                SOP.Corrections_Required__c = '';                
            }
           update update_SOP; 
        }  

    list <Location__c> update_loc = [SELECT Id,Applicant_Instructions__c FROM Location__c WHERE Line_Item__c
                                                           in :lineitemids] ;
        if(!update_loc.isempty())
        {
            for(Location__c loc : update_loc)
            {
                system.debug('location id numbers'+ loc.id);
                loc.Applicant_Instructions__c = '';                
            }
           update update_loc; 
        }  
        
    }

    }
    catch(Exception e)
    {
                            system.debug('----'+e);
    }
}
// End of Changes Ram 10/01/2015 US955    
    
  
}