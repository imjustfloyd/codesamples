trigger CARPOL_BRS_LocationsTrigger on Location__c (before update,before insert,after update , after insert,after delete) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_LocationsTrigger                     */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */ 
/*           Notifications,Status Update,                 */
/*           Validation Checks,Error handling             */    
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Kishore 05/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */  
CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_LocationsTrigger');
 if(!dt.disable__c) {
    list<Location__c> lstloc  = trigger.new;
    list<string> lstlocids = new list<string>();
    list<string> lstorglocids = new list<string>();
    list<string> lstsite = new list<string>();
      List<String> sortedvalue= new list<string>();
    list<AC__c> lineitemlst = new list<AC__c>();
    list<ID> BRSlineitemlst = new list<ID>();
    list<ID> locids = new list<ID>();
    list<AC__c> listlineitemToUpdate = new list<AC__c>();
    list<Location__c> listlocToUpdate = new list<Location__c>();
    list<string> lstappids = new list<string>();
    string strprofileid = UserInfo.getProfileId();
    string brsappstatus = '';
    string brsappid = '';
    string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;
    string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
    string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
    string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
    string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
    boolean olocrectype;
    boolean rslocrectype;
    boolean dlocrectype;
    boolean oanddlocrectype;
    
        //----------------------------------------------------BEFORE-------------------------------------------------------------------------// 
          if(trigger.isbefore){
          
           
           //--------------------------------UPDATE------------------------------------------------------------------//
           if(trigger.isupdate){
                 if(BRSProfile=='APHIS Applicant' || BRSProfile=='system administrator'){ 
    
                for(Location__c c:trigger.new){
                    lstlocids.add(c.id);
     
                }
                for(Location__c c:[select id,Name,Application__c,Primary_State_Text__c,Primary_State_CBI__c,Primary_State__c,Primary_State__r.name,Application__r.Application_Status__c,Status__c,Line_Item__c,Line_Item__r.Status__c from Location__c where id in: lstlocids])
                {
                                if(c.Line_Item__r.Status__c=='Submitted' && c.Status__c!='Waiting on Customer'){
                                      lstloc[0].addError('Application is submitted you cannot modify the Location');
                                    }     

                } 
                  // Capture changes of fields on location object
                    GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Location__c');
                            
               }
                
         } 
         
           //--------------------------------INSERT------------------------------------------------------------------// 
           if(trigger.isInsert){
               
            list<string> lstall = new list<string>();
            list<string> newLineitemlist = new list<string>(); // newly Added Niharika-to get all new line items 
             for(Location__c l:trigger.new)
              {
                   system.debug('olocrectypeid ###'+ olocrectypeid);
                    system.debug('l.recordtype.name ###'+l.recordtypeid );
                  if(l.Line_Item__c!=null ){//&& l.recordtypeid == olocrectypeid ){
                      lstall.add(l.Line_Item__c);
                  }
                  if(l.Line_Item__c!=null && l.recordtypeid == locrectypeid )
                  {
                      lstsite.add(l.Line_Item__c);
                  }            
                  newLineitemlist.add(l.Line_Item__c);// newly Added Niharika
              }
              
                for(AC__c ac:[select id,Name,RecordType.Name,Status__c,Purpose_of_the_Importation__c,(select id,RecordType.Name from locations__r) from AC__c where id in:lstall]){
                   system.debug('line item ###'+ac);
                    if(ac.Status__c=='Submitted')// && (BRSProfile=='APHIS Applicant' || BRSProfile=='system administrator'))
                     {
                         lstloc[0].addError('Application is submitted you cannot insert new Location');
                     } 
                                  
                 } 
                 
             // Newly Added - Niharika- April 13
             List<location__c> locationList = new list<location__c>();      
            // string Purpose_of_the_Importation ='Importation';
             string movementtype ='Import';
             String RecordTypeCourtesy ='Biotechnology Regulatory Services - Courtesy Permit';      
             String RecordTypeNotification ='Biotechnology Regulatory Services - Notification'; 
             
             list<AC__c> newLineitemlistTemp = [SELECT ID,RecordType.Name,Purpose_of_the_Importation__c From AC__c WHERE ID in:newLineitemlist AND movement_type__c=:movementtype AND (RecordType.Name=:RecordTypeCourtesy OR RecordType.Name=:RecordTypeNotification)];        
             list<string> newLineitemlist1=new list<string>();      
            
             for(ac__c ac: newLineitemlistTemp){        
                 newLineitemlist1.add(ac.id);       
             }  
             
             locationList = [select id,RecordType.Name,Line_Item__c from location__c WHERE Line_Item__c in: newLineitemlist1];      
             Map<String,List<location__c>> mapLineItemWiseLocations = new Map<String,List<location__c>>();  
             
                    For(location__c cont : locationList)        
                    {       
                        if(mapLineItemWiseLocations.containsKey(cont.Line_Item__c))     
                        {       
                            List<location__c> lstCont = mapLineItemWiseLocations.get(cont.Line_Item__c);        
                            lstCont.add(cont);      
                        }       
                        else        
                        {       
                            List<location__c> lstCont = new List<location__c>();        
                            lstCont.add(cont);      
                            mapLineItemWiseLocations.put(cont.Line_Item__c,lstCont);        
                        }       
                    }

               }
               
       }       
       
         //----------------------------------------------------AFTER-------------------------------------------------------------------------//  
           if(trigger.isafter){
             
                if(trigger.isinsert){
                          for(Location__c l:trigger.new)
                           {
                              BRSlineitemlst.add(l.Line_Item__c); 
                              lstlocids.add(l.id);
                           }
                        
                             list<AC__c> lineitemlistafter = [SELECT ID,RecordType.Name,movement_type__c ,(select id,recordtypeid from Locations__r) From AC__c WHERE ID in: BRSlineitemlst];       
                                            system.debug('<!!!!!!!! lineitemlistafter '+lineitemlistafter);
                            for (AC__c a : lineitemlistafter) {
                                    olocrectype = false;               //origin location
                                    oanddlocrectype = false;           // origin and destination
                                    dlocrectype = false;               //destination
                                    rslocrectype = false;              // release
                                if(a.Locations__r.size() > 0){
                                    for(Location__c lc:a.Locations__r){
                                        system.debug('<!!!!! lc.recordtypeid'+lc.recordtypeid);
                                        if(lc.recordtypeid == olocrectypeid)
                                            olocrectype = true;
                                         if(lc.recordtypeid == oanddlocrectypeId)
                                            oanddlocrectype = true;
                                         if(lc.recordtypeid == dlocrectypeId)
                                            dlocrectype = true;
                                        if(lc.recordtypeid == locrectypeid)
                                            rslocrectype = true;
                                        
                                    }
                                   if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && a.movement_type__c == 'Interstate Movement') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    }else if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && rslocrectype == true && a.movement_type__c == 'Interstate Movement and Release') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    } else if((olocrectype == true || oanddlocrectype == true) && (oanddlocrectype == true || dlocrectype == true) && a.movement_type__c == 'Import') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    } else if(rslocrectype == true && a.movement_type__c == 'Release') 
                                    {
                                      a.Location_Status__c = 'Ready to Submit';
                                      listlineitemToUpdate.add(a); 
                                    }  
                                    
                                }
                                  
                              }
                       
                            //-------------CBI Fields---------------//                    
                           system.debug('lstlocids###'+lstlocids);
                            for(Location__c loc1:[select id,Name,Country_CBI__c,Country__c,Country_Text__c,State__c,State_CBI__c,Level_2_Region__c,County__c,County_CBI__c,Primary_Country_CBI__c,Primary_Country__c,Primary_Country_Text__c, Primary_State__c, 
                                                        Primary_State_CBI__c,Primary_State__r.name, Contact_state__c,Primary_County__c, Contact_County__c, Primary_County_CBI__c, Secondary_Contact_County__c, Secondary_County__c, Secondary_Contact_State__c, Secondary_State__c,Secondary_Country__c, 
                                                        Secondary_Country_CBI__c, Material_Type_CBI__c,Material_Type_Text__c,Material_Type__c,Secondary_Country_Text__c,Secondary_County_CBI__c, Secondary_State_CBI__c,Primary_County__r.name,Primary_Country__r.name,Secondary_State__r.name,Secondary_Country__r.name,Secondary_County__r.name,Unit_of_Measure_Text__c,Unit_of_Measure__c,Unit_of_Measure_CBI__c from Location__c where id in: lstlocids])
                               {
                                   system.debug('Entered into the CBI check');
                                   system.debug('loc1'+loc1);
                                   system.debug('Primary_State_CBI__c.name@@@'+loc1.Primary_State_CBI__c);
                                   system.debug('Primary_State__r@@@'+loc1.Primary_State__r.name);
                                   system.debug('Primary_Country__r.name@@@'+loc1.Primary_Country__r.name);
                                   
                                   /*if(loc1.Material_Type__c!=null){
                                   String s = loc1.Material_Type__c;
                                   sortedvalue = s.split(';');
                                   system.debug('!!!!!!sortedvalue'+sortedvalue);*/
                                   
                                if(loc1.Unit_of_Measure_CBI__c == true){
                                    loc1.Unit_of_Measure_Text__c = '['+ loc1.Unit_of_Measure__c +']';
                                 }
                                 else{
                                     loc1.Unit_of_Measure_Text__c =loc1.Unit_of_Measure__c;
                                }
                                if(loc1.Material_Type_CBI__c == true){
                                    loc1.Material_Type_Text__c = '['+ loc1.Material_Type__c +']';
                                 }
                                else{
                                     loc1.Material_Type_Text__c =loc1.Material_Type__c;
                                }
                                system.debug('!!!!!!Material_Type_Text__c'+loc1.Material_Type_Text__c);
                               //}
                
                               if(loc1.Primary_Country_CBI__c == true){
                                loc1.Primary_Country_Text__c = '['+loc1.Primary_Country__r.name+']';
                               }else
                                {
                                     loc1.Primary_Country_Text__c = loc1.Primary_Country__r.name;
                                }
                                
                                if(loc1.Primary_State_CBI__c == true){
                                loc1.Primary_State_Text__c = '['+loc1.Primary_State__r.name+']';
                               }else
                                {
                                     loc1.Primary_State_Text__c = loc1.Primary_State__r.name;
                                }
                                if(loc1.Secondary_Country_CBI__c == true){
                                loc1.Secondary_Country_Text__c = '['+loc1.Secondary_Country__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Country_Text__c = loc1.Secondary_Country__r.name;
                                }
                                if(loc1.Secondary_County_CBI__c == true){
                                loc1.Secondary_Contact_County__c = '['+loc1.Secondary_County__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Contact_County__c = loc1.Secondary_County__r.name;
                                }
                                if(loc1.Secondary_State_CBI__c == true){
                                loc1.Secondary_Contact_State__c = '['+loc1.Secondary_State__r.name+']';
                               }else
                                {
                                     loc1.Secondary_Contact_State__c = loc1.Secondary_State__r.name;
                                }
                                 //System.Debug('Primary_State_Text__c@@@@'+loc1.Primary_State_Text__c);
                               listlocToUpdate.add(loc1);
                               
                               } 
                              
                             system.debug('listlocToUpdate@@@@'+listlocToUpdate);  
                            if(listlocToUpdate.size()>0){
                                update listlocToUpdate;}
                       }
                       if(checkRecursive.runOnce() || test.isRunningTest()){  
                if(trigger.isupdate){
                     //-------------CBI Fields---------------// 
                     for(Location__c l:trigger.old){
                           /* if(l.Line_Item__c!=null){
                                lstappids.add(l.Line_Item__c);
                            }*/
                            BRSlineitemlst.add(l.Line_Item__c); 
                              lstlocids.add(l.id);
                       }
                           system.debug('lstappids###'+lstappids);
                            for(Location__c loc2:[select id,Name,Country_CBI__c,Line_Item__c,Country__c,Country_Text__c,State__c,State_CBI__c,Level_2_Region__c,County__c,County_CBI__c,Primary_Country_CBI__c,Primary_Country__c,Primary_Country_Text__c, Primary_State__c, 
                                                        Primary_State_CBI__c,Primary_State__r.name, Contact_state__c,Primary_County__c, Contact_County__c, Primary_County_CBI__c, Secondary_Contact_County__c, Secondary_County__c, Secondary_Contact_State__c, Secondary_State__c,Secondary_Country__c, 
                                                        Secondary_Country_CBI__c, Material_Type_CBI__c,Material_Type_Text__c,Material_Type__c,Secondary_Country_Text__c,Secondary_County_CBI__c, Secondary_State_CBI__c,Primary_County__r.name,Primary_Country__r.name,Secondary_State__r.name,Secondary_Country__r.name,Secondary_County__r.name,Unit_of_Measure_CBI__c,Unit_of_Measure_Text__c,Unit_of_Measure__c from Location__c where id in:lstlocids])
                              {
                                 
                                   
                                   /* if(loc2.Material_Type__c!=null){
                                   String s = loc2.Material_Type__c;
                                   sortedvalue = s.split(';');
                                   system.debug('!!!!!!sortedvalue'+sortedvalue);*/
                                if(loc2.Unit_of_Measure_CBI__c == true){
                                    loc2.Unit_of_Measure_Text__c = '['+ loc2.Unit_of_Measure__c +']';
                                 }
                                else{
                                     loc2.Unit_of_Measure_Text__c =loc2.Unit_of_Measure__c;
                                }
                                if(loc2.Material_Type_CBI__c == true){
                                    loc2.Material_Type_Text__c = '['+ loc2.Material_Type__c +']';
                                
                               }
                               else{
                                     loc2.Material_Type_Text__c =loc2.Material_Type__c;
                                }
                                 system.debug('!!!!!!Material_Type_Text__c'+loc2.Material_Type_Text__c);
                               //}

                
                               if(loc2.Primary_Country_CBI__c == true){
                                loc2.Primary_Country_Text__c = '['+loc2.Primary_Country__r.name+']';
                               }else
                                {
                                     loc2.Primary_Country_Text__c = loc2.Primary_Country__r.name;
                                }
                                 system.debug('!!!!!!Primary_Country_Text__c'+loc2.Primary_Country_Text__c);
                               
                                if(loc2.Primary_State_CBI__c == true){
                                loc2.Primary_State_Text__c = '['+loc2.Primary_State__r.name+']';
                               }else
                                {
                                     loc2.Primary_State_Text__c = loc2.Primary_State__r.name;
                                }
                                system.debug('!!!!!!Primary_State_Text__c'+loc2.Primary_State_Text__c);
                                if(loc2.Secondary_Country_CBI__c == true){
                                loc2.Secondary_Country_Text__c = '['+loc2.Secondary_Country__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Country_Text__c = loc2.Secondary_Country__r.name;
                                }
                                 system.debug('!!!!!!Secondary_Country_Text__c'+loc2.Secondary_Country_Text__c);
                                if(loc2.Secondary_County_CBI__c == true){
                                loc2.Secondary_Contact_County__c = '['+loc2.Secondary_County__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Contact_County__c = loc2.Secondary_County__r.name;
                                }
                                 system.debug('!!!!!!Secondary_Contact_County__c'+loc2.Secondary_Contact_County__c);
                                if(loc2.Secondary_State_CBI__c == true){
                                loc2.Secondary_Contact_State__c = '['+loc2.Secondary_State__r.name+']';
                               }else
                                {
                                     loc2.Secondary_Contact_State__c = loc2.Secondary_State__r.name;
                                }
                                 system.debug('!!!!!!Secondary_Contact_State__c'+loc2.Secondary_Contact_State__c);
                                
                               listlocToUpdate.add(loc2);
                               system.debug('!!!!listlocToUpdate'+listlocToUpdate.size());
                               if(listlocToUpdate.size()>0)
                               {
                                update listlocToUpdate;}
                               }
                
                               } 
                    
                       }
                
                    
                if(trigger.isdelete){
         
                      for(Location__c l:trigger.old){
                            if(l.Line_Item__c!=null){
                                lstappids.add(l.Line_Item__c);
                            }
                       }

                    List<AC__c> LineItemList = [select id, (select id from Locations__r) from AC__c where id in : lstappids];
                    
                    for (AC__c a : LineItemList) {
                        if(a.Locations__r.size() == 0){
                            a.Location_Status__c = 'Yet to Add';
                            listlineitemToUpdate.add(a); 
                            }
                          
                        }
       
                 } 
             
                if(listlineitemToUpdate.size()>0){
                    update listlineitemToUpdate;
                   }
               
               
           }     
           

    }  // End of Custom setting If statement        
}