trigger CARPOL_UNI_MasterAppAttachTrigger on Applicant_Attachments__c (  before insert, after insert, 
  before update, after update, 
  before delete, after delete) {

  if (Trigger.isBefore) {
    if (Trigger.isInsert) {
      
    } 
    if (Trigger.isUpdate) {
     
    }
    if (Trigger.isDelete) {
      // Call class logic here!
    }
  }

  if (Trigger.IsAfter) {
    if (Trigger.isInsert) {
     CARPOL_UNI_Check_Required_Docs.check(trigger.new);
    } 
    if (Trigger.isUpdate) {
        //CARPOL_UNI_ApplicantAttachment checker =  new CARPOL_UNI_ApplicantAttachment(Trigger.oldMap,Trigger.newMap);
        //checker.ApplicantAttachments();
        CARPOL_UNI_Check_Required_Docs.check(trigger.new);
    }
    if (Trigger.isDelete) {
         CARPOL_UNI_DELApplicantAttachment checker =  new CARPOL_UNI_DELApplicantAttachment(trigger.old);
         checker.ApplicantAttachments();
    }
  }

}