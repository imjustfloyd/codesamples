trigger CARPOL_BRS_Link_RegulatedTrigger on Link_Regulated_Articles__c (before update,before insert,after insert,after delete) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_Link_RegulatedTrigger                */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities,                     */ 
/*           Locking factor,Added Validations             */    
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Kishore 04/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */

CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_Link_RegulatedTrigger');
 if(!dt.disable__c) {

// check record type of application
    Map<ID, Schema.RecordTypeInfo> apprtMap = Schema.SObjectType.AC__c.getRecordTypeInfosById();
    list<string> lstappids = new list<string>();
    list<ac__c> listlineitemToUpdate = new list<ac__c>();
    string apprt='';
    string RAStatus ='';
    list<Link_Regulated_Articles__c> lstlnkart = trigger.new;
    
    if(trigger.isbefore){
        if(trigger.isinsert || trigger.isupdate){
            for(Link_Regulated_Articles__c l:trigger.new){
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
                 }
              for(AC__c a:[select id,RecordTypeid,Status__c from AC__c where id in:lstappids])
              {
                  apprt = apprtMap.get(a.RecordTypeId).getName();
                  if(apprt=='Biotechnology Regulatory Services - Notification' || apprt=='Biotechnology Regulatory Services - Standard Permit' || apprt=='Biotechnology Regulatory Services - Courtesy Permit')
                  {
                      if(a.Status__c=='Submitted' || a.Status__c=='In Review' || a.Status__c=='In Review Biotechnologist' || a.Status__c=='In Review Compliance')
                      {
                          lstlnkart[0].addError('Application is submitted you cannot Add/Edit the Link Regulated Article');
                      }
                  }
              }
           }
     }
    // Added login for BRS line item status change
     if(trigger.isafter){
          
         if(trigger.isinsert){
             
             for(Link_Regulated_Articles__c l:trigger.new){
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
              }

              for(AC__c a:[select id,RecordTypeid,Status__c,Regulated_Article_status__c from AC__c where id in:lstappids])
              {
                  apprt = apprtMap.get(a.RecordTypeId).getName();
                  if(apprt=='Biotechnology Regulatory Services - Notification' || apprt=='Biotechnology Regulatory Services - Standard Permit' || apprt=='Biotechnology Regulatory Services - Courtesy Permit')
                  {
                      if(a.Status__c=='Saved' || a.Status__c== 'Ready to Submit')
                      {
                          a.Regulated_Article_status__c = 'Ready to Submit';
                         listlineitemToUpdate.add(a);
                      }
                  }
              }
              

           }
         if(trigger.isdelete){
             
              for(Link_Regulated_Articles__c l:trigger.old){
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
              }

             
             
     
      
        List<AC__c> LineItemList = [select id, (select id from Link_Regulated_Articles__r) from AC__c where id in : lstappids];
        for (AC__c a : LineItemList) {
	     	if(a.Link_Regulated_Articles__r.size() == 0){
	     	    a.Regulated_Article_status__c = 'Yet to Add';
	     	    listlineitemToUpdate.add(a); 
	     	}
	     	  
	      }
      
       } 
           
           
    if(listlineitemToUpdate.size()>0)
      update listlineitemToUpdate;    
     } 
     
 }    
}