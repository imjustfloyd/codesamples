trigger CARPOL_Application_Condition_ChangeHistory on Application_Condition__c (after update) {
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Application_Condition__c');
}