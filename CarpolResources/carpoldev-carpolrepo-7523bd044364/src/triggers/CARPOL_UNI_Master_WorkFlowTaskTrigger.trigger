trigger CARPOL_UNI_Master_WorkFlowTaskTrigger on Workflow_Task__c 
  (before insert, after insert, 
  before update, after update, 
  before delete, after delete) 
  {

       map<Id, Workflow_Task__c> mapTasksNew;
       map<Id, Workflow_Task__c> mapTasksOld;
       string lockVar;
    
      if (Trigger.isBefore) {
          
                if (Trigger.isInsert) 
                    {
                    }
        
                if (Trigger.isUpdate) 
                    {
                        // For all programs this sets the owner to the first person who edits the record
                         Id profileId=userinfo.getProfileId();
                         String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                         
                         if(profileName != 'APHIS Applicant')
                            {
                                
                                 for(Workflow_Task__c wfTasks: trigger.old)
                                     { 
                                         lockVar =   wfTasks.Locked__c;
                                     }
                                 for(Workflow_Task__c wfTasks: trigger.new)
                                     {  
                                          
                                           if(wfTasks.Initial_Owner_Changed__c == False && wfTasks.Locked__c == 'No' && lockVar == 'No')
                                               {
                                                    wfTasks.OwnerId = UserInfo.getUserId();
                                                    wfTasks.Initial_Owner_Changed__c = True;
                                               }
                                      }
                            } 
                            
                            
                    }
                    
        if (Trigger.isDelete) {
          // Call class logic here!
        }
      }
    
    
    
    
    
    
    
    
    
      if (Trigger.IsAfter) {
        if (Trigger.isInsert) {
      
        } 
        if (Trigger.isUpdate) {
             
        }
        if (Trigger.isDelete) {
          // Call class logic here!
        }
      }
}