trigger CARPOL_BRS_ConstructTrigger on Construct__c (before update,before insert,after update ,after insert) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_ConstructTrigger                     */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */  
/*           Status Update, Locking Functionality         */  
/*           Change History                               */
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION AUTHOR  DATE    DETAIL  RELEASE/CSR           */
/*  1.0 - D12/Kishore 05/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */
   
CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_ConstructTrigger');
 system.debug('dt.disable__c ### '+dt.disable__c);
 if(!dt.disable__c){ 
     
    list<Construct__c> lstconst  = trigger.new;
    list<string> lstconstids = new list<string>();
    string strprofileid = UserInfo.getProfileId();
    list<string> lstappids = new list<string>();
    //string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;
   // string brsappstatus = '';
   // string brsappid = '';
    list<ID> BRSlineitemlst = new list<ID>(); 
    list<ID> Constructslist = new list<ID>();
    list<AC__c> listlineitemToUpdate = new list<AC__c>();  
    
        
       //----------------------------------------------------AFTER-------------------------------------------------------------------------//  
       if(trigger.isafter){
           
           //--------------------------------AFTER INSERT------------------------------------------------------------------// 
           if (Trigger.isinsert) {
                    for(Construct__c con1:trigger.new)
                     {
                      BRSlineitemlst.add(con1.Line_Item__c); 
                     }
                     list<AC__c> lineitemlistafter = [SELECT ID,RecordType.Name,Construct_Status__c,(select id,Total_No_of_PhenoTypes__c,Total_No_of_GenoTypes__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst];
                      for (AC__c a : lineitemlistafter) {
                          if(a.Constructs__r.size() > 0 && a.Construct_Status__c=='Ready to Submit')
                                        a.Construct_Status__c = 'Yet to Add';
                                        listlineitemToUpdate.add(a);
                           	   }
                       }             
              
          //--------------------------------AFTER UPDATE------------------------------------------------------------------//   
           if (Trigger.isupdate) {
                 for(Construct__c con:trigger.new)
                     {
                      BRSlineitemlst.add(con.Line_Item__c); 
                      Constructslist.add(con.id);
                     }
                     AC__c a = [SELECT ID,RecordType.Name,Construct_Status__c,(select id,Total_No_of_PhenoTypes__c,Total_No_of_GenoTypes__c from Constructs__r) From AC__c WHERE ID in: BRSlineitemlst];
                       Integer i=0;
                       if(a.Constructs__r.size() > 0){
                            for(construct__c cons :a.constructs__r ){
                           	  if(cons.Total_No_of_PhenoTypes__c>0 && cons.Total_No_of_GenoTypes__c>0)
                                        {  
                                            i++;
                                           }
                           	   }
                            	   if(a.constructs__r.size()==i){
                            	       a.Construct_Status__c = 'Ready to Submit';
                                       listlineitemToUpdate.add(a);
                          	   }
                          	}
                  GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Construct__c');
             }
             
          //--------------------------------AFTER DELETE------------------------------------------------------------------//
           if(trigger.isdelete){
             
              for(Construct__c l:trigger.old){
                    if(l.Line_Item__c!=null){
                        lstappids.add(l.Line_Item__c);
                    }
              }

            List<AC__c> LineItemList = [select id, (select id from Constructs__r) from AC__c where id in : lstappids];
            for (AC__c a : LineItemList) {
    	     	if(a.Constructs__r.size() == 0){
    	     	    a.Construct_Status__c = 'Yet to Add';
    	     	    listlineitemToUpdate.add(a); 
    	        	}
    	        }
             } 
           
          //--------------------------------UPDATE Line Item------------------------------------------------------------------//   
    	   if(listlineitemToUpdate.size()>0){
    	         update listlineitemToUpdate;
    	      }               
       }
  }    
}