trigger CARPOL_UNI_MasterFacilityTrigger on Facility__c (  before insert, after insert, 
  before update, after update, 
  before delete, after delete) {

         FacilityTriggerHandler handler = new FacilityTriggerHandler();

  if (Trigger.isBefore) {
      
    if (Trigger.isInsert) {
    map<Id, Facility__c> mapLinesNew;
            mapLinesNew = new map<Id, Facility__c>();
            for(Facility__c o : trigger.new)
            {
               mapLinesNew.put(o.Id, o);
            }  
                  
           
            handler.updateTimeZone(mapLinesNew); 
         } 
    if (Trigger.isUpdate) {
    map<Id, Facility__c> mapLinesNew;
            mapLinesNew = new map<Id, Facility__c>();
            for(Facility__c o : trigger.new)
            {
               mapLinesNew.put(o.Id, o);
            }  
                  
            
            handler.updateTimeZone(mapLinesNew); 

      
    }
    if (Trigger.isDelete) {
      // Call class logic here!
    }
  }

  if (Trigger.IsAfter) {
    if (Trigger.isInsert) {
 
           
     
    } 
    if (Trigger.isUpdate) {
      // Call class logic here!
    }
    if (Trigger.isDelete) {
      // Call class logic here!
    }
  }
}