trigger CARPOL_BRS_Reviewer_DuplicateTrigger on Reviewer__c (before insert, before update) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_Reviewer_DuplicateTrigger            */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: primay State Reviewer Duplicate check        */   
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Nupur 06/11/2015  INITIAL DEVELOPMENT       */ 
/*  ====================================================  */

CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_Reviewer_DuplicateTrigger');
 if(!dt.disable__c) {

Map<String, Reviewer__c> revMap = new Map<String, Reviewer__c>();

for (Reviewer__c rev: System.Trigger.new) {
  // if(rev.Reviewer_Unique_Id__c.substring(0,1)!='P')
  // {
    //rev.State_Regulatory_Official__c.addError('The State Regulatory Official is not a Primary Contact, please select the Primary Contact.');
  // }else{
    if ((rev.Reviewer_Unique_Id__c != null) && (System.Trigger.isInsert || (rev.Reviewer_Unique_Id__c != System.Trigger.oldMap.get(rev.Id).Reviewer_Unique_Id__c))) {
       system.debug('#####'+revMap.keyset());
       if (revMap.containsKey(rev.Reviewer_Unique_Id__c)) {
           system.debug('#######');    
           rev.Reviewer_Unique_Id__c.addError('A reviewer with the same state details already exist, please check your submitted records.'); 
            } else {
                revMap.put(rev.Reviewer_Unique_Id__c, rev);
            }
       }
   // }
    }
    system.debug('revMap #### '+revMap);
    list<REviewer__c> revList =[SELECT Reviewer_Unique_Id__c FROM REviewer__c WHERE Reviewer_Unique_Id__c IN :revMap.KeySet()];
    system.debug('rList ###'+revList);
    for (Reviewer__c rev : [SELECT Reviewer_Unique_Id__c FROM REviewer__c WHERE Reviewer_Unique_Id__c IN :revMap.KeySet()]) {
        system.debug('Inside for Loop ####'+ rev);
        Reviewer__c newrev = revMap.get(rev.Reviewer_Unique_Id__c);
        newrev.Reviewer_Unique_Id__c.addError('A reviewer with the same state details already exist, please check your submitted records.');
   }
       if(trigger.isafter){
           if (Trigger.isupdate) {
               GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Reviewer__c');
             } 
       }   
   }
}