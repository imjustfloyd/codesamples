trigger CARPOL_BRS_GenotypeTrigger on Genotype__c (after update) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_GenotypeTrigger                      */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: Change History                               */      
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION AUTHOR  DATE    DETAIL  RELEASE/CSR           */
/*  1.0 - D12/Kishore 05/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */
CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_GenotypeTrigger');
 if(!dt.disable__c){
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Genotype__c');
        }
}