trigger FindContacts on Preparer_Request__c (before insert, before update) {
  for (Preparer_Request__c preparerContact : Trigger.new) {
    if (preparerContact.Contact_Email__c != null) {
      List<Contact> dupes = [SELECT Id FROM Contact
                               WHERE (recordtype.developername='Broker' or recordtype.developername='Preparer') and Email = :preparerContact.Contact_Email__c];
      if (dupes.size() > 0) {
        preparerContact.Preparer_Broker_Name__c = dupes[0].Id;
      } else {
        preparerContact.Preparer_Broker_Name__c = null;
      }                             
    }
  }
}