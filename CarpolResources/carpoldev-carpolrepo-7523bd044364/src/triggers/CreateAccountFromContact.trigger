trigger CreateAccountFromContact on Contact (before insert,before update) {


string strprofileid = UserInfo.getProfileId();
string BRSProfile = [select id,Name from Profile where id=:strprofileid].Name;

Map<ID,RecordType> rt_Map = New Map<ID,RecordType>([Select ID, Name From RecordType Where sObjectType = 'Contact']);

 
    List<Contact> needAccounts = new List<Contact>();
    
    //if(BRSProfile=='BRS Partner Community User'){
    for (Contact c : trigger.new) {
      if(rt_map.get(c.recordTypeID).name.containsIgnoreCase('Individual Owner')){
     if (c.Individual_Owner_Account__c!=true){
        if (String.isBlank(c.accountid)) {
            needAccounts.add(c);
        }
    }
    }
    }
    //}
    
    if (needAccounts.size() > 0) {
        List<Account> newAccounts = new List<Account>();
        Map<String,Contact> contactsByNameKeys = new Map<String,Contact>();
        
        //Create account for each contact
        for (Contact c : needAccounts) {
            String accountName = c.firstname + ' ' + c.lastname;
            contactsByNameKeys.put(accountName,c);
            Account a = new Account(name=accountName);
            newAccounts.add(a);
        }
        insert newAccounts;
        
        for (Account a : newAccounts) {
            
            //Put account ids on contacts
            if (contactsByNameKeys.containsKey(a.Name)) {
                contactsByNameKeys.get(a.Name).accountId = a.Id;
                contactsByNameKeys.get(a.Name).Individual_Owner_Account__c= true;
                a.BillingStreet = contactsByNameKeys.get(a.Name).MailingStreet;
                a.BillingCity= contactsByNameKeys.get(a.Name).MailingCity;
                a.BillingState= contactsByNameKeys.get(a.Name).MailingState;
                a.BillingCountry= contactsByNameKeys.get(a.Name).MailingCountry ;
                a.BillingPostalCode= contactsByNameKeys.get(a.Name).MailingPostalCode;
                a.ShippingStreet= contactsByNameKeys.get(a.Name).OtherStreet;
                a.ShippingCity= contactsByNameKeys.get(a.Name).Othercity;
                a.ShippingState= contactsByNameKeys.get(a.Name).Otherstate;
                a.ShippingCountry= contactsByNameKeys.get(a.Name).Othercountry ;
                a.ShippingPostalCode= contactsByNameKeys.get(a.Name).OtherPostalCode;
                a.Phone= contactsByNameKeys.get(a.Name).Phone;
                a.Other_Phone__c= contactsByNameKeys.get(a.Name).MobilePhone;
            }
            
        }
        update newAccounts;
        
        
    }
    
 
}