trigger CARPOL_UNI_MasterAuthJunctTrigger on Authorization_Junction__c (before insert, after insert, before update, after update, before delete, after delete) {
if (Trigger.isBefore) //====================================================BEFORE=================================================
          {
              if (Trigger.isInsert) //====================================================BEFORE=================================================
                  {
                      for(Authorization_Junction__c AJ:trigger.new){
                          if(AJ.Regulation__c!=null || AJ.Regulation__c!=''){
                              Regulation__c Reg = [SELECT ID, Name, Regulation_Description__c, Sub_Type__c, Type__c, Title__c, Custom_Name__c FROM Regulation__c WHERE ID=:AJ.Regulation__c LIMIT 1];
                              AJ.Regulation_Description__c = Reg.Regulation_Description__c;
                              
                          }
                      }
                  }
          }
    
}