trigger CARPOL_PPQ_ConstructSelectedRegulationsList on Signature_Regulation_Junction__c (after Insert, after Delete) {
    
    System.Debug('<<<<<<< ConstructSelectedRegulationsList Trigger Begins >>>>>>>');
    public Map<String, String> mapAllRegulations = new Map<String, String>();
    Signature__c signature = new Signature__c();
    List<Regulation__c> getAllRegulations = new List<Regulation__c>();
    List<Regulation__c> getAllRegulationsTemp = new List<Regulation__c>();
    List<Group__c> getAllRegulationsGroup = new List<Group__c>();
    String lstSelectedImportRequirements = '';
    String lstSelectedInstructionForCBPOfficers = '';
    Boolean updateFlag = false;
    Integer iterator = 0;
    String signatureID = '';
    System.Debug('<<<<<<< Trigger Size  : ' + Trigger.size + ' >>>>>>>');
    
    if (Trigger.isInsert) {
    
        for(Signature_Regulation_Junction__c signatureRegulation :Trigger.New){
            iterator++;
            if(iterator == Trigger.size)
            {
                signatureID = signatureRegulation.Signature__c;
                //Checking if Signature ID is null in the previous step then it is Group which is added/removed
                //if(signatureID == null){signatureID = signatureRegulation.Signature_Group__c;}
            }
        }
    }
    
    if (Trigger.isDelete) {
    
        for(Signature_Regulation_Junction__c signatureRegulation :Trigger.Old){
            iterator++;
            if(iterator == Trigger.size)
            {
                signatureID = signatureRegulation.Signature__c;
                //Checking if Signature ID is null in the previous step then it is Group which is added/removed
                //if(signatureID == null){signatureID = signatureRegulation.Signature_Group__c;}
            }
        }
    }
    
    System.Debug('<<<<<<< Signature ID : ' + signatureID + ' >>>>>>>');
    
    if(iterator == Trigger.size)
    {
        if(signatureID != null)
        {
            // Fetching the related Signature details
            signature = [SELECT ID, Name, Selected_Import_Requirements__c, Selected_Instruction_for_CBP_Officers__c FROM Signature__c WHERE ID = :ID.valueOf(signatureID)];
            
            System.Debug('<<<<<<< Fetching Indvidual Regulations >>>>>>>');
            getAllRegulationsTemp = [SELECT ID, Name, Custom_Name__c, Type__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :ID.valueOf(signatureID)) ORDER BY Custom_Name__c];
            //for(Integer i = 0; i < getAllRegulationsTemp.size(); i++){
            //    if((mapAllRegulations.get(String.ValueOf(getAllRegulationsTemp[i].ID)) == null)){mapAllRegulations.put(String.ValueOf(getAllRegulationsTemp[i].ID),getAllRegulationsTemp[i].Custom_Name__c);}
            //}
            getAllRegulations.addAll(getAllRegulationsTemp);
          
            //System.Debug('<<<<<<< Fetching Group Regulations >>>>>>>');
            //String groupIDs = '(';
            //getAllRegulationsTemp = new List<Regulation__c>();
            //getAllRegulationsGroup = [SELECT ID FROM Group__c WHERE ID IN (SELECT Regulations_Group__c FROM Signature_Condition_Junction__c WHERE Signature_Group__c = :ID.valueOf(signatureID))];
            
            //if(getAllRegulationsGroup.size() > 0)
            //{
            //    System.Debug('<<<<<<< Selectd Regulations Group Size : ' + getAllRegulationsGroup.size() + ' >>>>>>>');
            //    for(Integer x = 0; x < getAllRegulationsGroup.size(); x++)
            //   {
            //        System.Debug('<<<<<<< Intermediate Groupd ID : ' + groupIDs + ' >>>>>>>');
            //        System.Debug('<<<<<<< Groupd ID : ' + getAllRegulationsGroup[x].ID + ' >>>>>>>');
            //        groupIDs += '\'' + getAllRegulationsGroup[x].ID + '\'' + ', ';
            //    }
            //    groupIDs = groupIDs.substring(0,groupIDs.length() - 2);
            //    groupIDs += ')';
            //   System.Debug('<<<<<<< Group IDs : ' + groupIDs + ' >>>>>>>');
            //    String fetchGroupRelatedRegulationsQuery = 'SELECT ID, Name, Custom_Name__c, Type__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Group_Junction__c WHERE Regulation_Group__c IN ' + groupIds + ') ORDER BY Custom_Name__c';
            //    System.Debug('<<<<<<< Fetching Regulations Query From Group Temp : ' + fetchGroupRelatedRegulationsQuery + ' >>>>>>>');
            //    getAllRegulationsTemp = Database.Query(fetchGroupRelatedRegulationsQuery);
            //    for(Integer y = 0; y < getAllRegulationsTemp.size(); y++){
            //        if((mapAllRegulations.get(String.ValueOf(getAllRegulationsTemp[y].ID)) == null)){mapAllRegulations.put(String.ValueOf(getAllRegulationsTemp[y].ID),getAllRegulationsTemp[y].Custom_Name__c);}
            //    }
            //    getAllRegulations.addAll(getAllRegulationsTemp);
            //}
        
            
            System.Debug('<<<<<<< Current Selected Import Requirements : ' + signature.Selected_Import_Requirements__c + ' >>>>>>>');
            System.Debug('<<<<<<< Current Selected Instruction To CBP Officers  : ' + signature.Selected_Instruction_for_CBP_Officers__c + ' >>>>>>>');
            System.Debug('<<<<<<< Total Regulations : ' + getAllRegulations.size() + ' >>>>>>>');
            //System.Debug('<<<<<<< Total Unique Regulations : ' + mapAllRegulations.size() + ' >>>>>>>');
            for(Integer z = 0; z < getAllRegulations.size(); z ++)
            {
                if(getAllRegulations[z].Type__c == 'Import Requirements'){lstSelectedImportRequirements += getAllRegulations[z].Custom_Name__c + ', ';updateFlag = true;}
                if(getAllRegulations[z].Type__c == 'Instruction for CBP Officers'){lstSelectedInstructionForCBPOfficers += getAllRegulations[z].Custom_Name__c + ', ';updateFlag = true;}
                    
                //if((mapAllRegulations.get(String.ValueOf(getAllRegulations[z].ID)) != null))
                //{
                //    if(getAllRegulations[z].Type__c == 'Import Requirements'){lstSelectedImportRequirements += getAllRegulations[z].Custom_Name__c + ', ';updateFlag = true;}
                //    if(getAllRegulations[z].Type__c == 'Instruction for CBP Officers'){lstSelectedInstructionForCBPOfficers += getAllRegulations[z].Custom_Name__c + ', ';updateFlag = true;}
                //}
                //mapAllRegulations.remove(String.ValueOf(getAllRegulations[z].ID));
            }
            
            if(updateFlag == true)
            {
                System.Debug('<<<<<<< Final -1 Import Requirements : ' + lstSelectedImportRequirements + ' >>>>>>>');
                System.Debug('<<<<<<< Final -1 Instruction For CBP Officers : ' + lstSelectedInstructionForCBPOfficers + ' >>>>>>>');
                if(lstSelectedImportRequirements.length() > 0){lstSelectedImportRequirements = lstSelectedImportRequirements.substring(0, lstSelectedImportRequirements.length() - 2);}
                if(lstSelectedInstructionForCBPOfficers.length() > 0){lstSelectedInstructionForCBPOfficers = lstSelectedInstructionForCBPOfficers.substring(0, lstSelectedInstructionForCBPOfficers.length() - 2);}
            }
            System.Debug('<<<<<<< New Selected Import Requirements : ' + lstSelectedImportRequirements + ' >>>>>>>');
            signature.Selected_Import_Requirements__c = lstSelectedImportRequirements;
            signature.Selected_Instruction_for_CBP_Officers__c = lstSelectedInstructionForCBPOfficers;
            update(signature);
        }
    }
    System.Debug('<<<<<<< ConstructSelectedRegulationsList Trigger Ends >>>>>>>');
}