trigger CARPOL_UNI_MasterApplicationTrigger on Application__c (before insert, after insert, before update, after update, before delete, after delete) 
    {
      if (Trigger.isBefore) //====================================================BEFORE=================================================
              {
                  if (Trigger.isInsert) //----------------------------------------before insert------------------------------------
                    {
                       for(Application__c app: trigger.new)
                            {  
                               app.Authorized_User__c = app.Applicant_Name__c;
                            }
                    }
                
                
              }
    
        if (Trigger.IsAfter) //====================================================AFTER=================================================
              {
                
                if (Trigger.isUpdate) 
                    {
                         if(checkRecursive.runOnce())
                          {   
                            //------Passing trigger.new into CARPOL_UNI_SubmitApplication to create Authorizations and update Line items with the respective authorizations
                            Map < Authorizations__c, List < AC__c >> AuthLineListMap = CARPOL_UNI_SubmitApplication.onAfterUpdate(trigger.new);
                            
                            Map < ID, List < AC__c >> AuthIdLineListmap = new Map < ID, List < AC__c >> ();
                            Map < Authorizations__c, List < AC__c >> passingMap = new Map < Authorizations__c, List < AC__c >> ();
                            Map < ID, Authorizations__c > packageParentAuthmap = new Map < ID, Authorizations__c > ();
                            List < Authorizations__c > Ingredientauthlist = new List < Authorizations__c > ();
                            
                            
                            
                            if (AuthLineListMap != null) {
                            
                                Map < AC__c, Id > LineAuthIdMap = new Map < AC__c, Id > ();
                                List < AC__c > lineitemlist = new List < AC__c > ();
                                Map < ID, AC__c > lineIdlinemap = new Map < ID, AC__c > ();
                                Map < ID, Authorizations__c > authidauth = new Map < ID, Authorizations__c > ();
                            
                                for (Authorizations__c auth: AuthLineListMap.keyset()) {
                                    AuthIdLineListmap.put(auth.id, AuthLineListMap.get(auth));
                                    authidauth.put(auth.id, auth);
                                }
                            
                            
                            
                                for (List < AC__c > line: AuthLineListMap.values()) {
                                    lineitemlist.addall(line);
                                }
                            
                            
                            
                                for (AC__c line: lineitemlist) {
                                    LineAuthIdMap.put(line, line.Authorization__c);
                                    lineIdlinemap.put(line.id, line);
                            
                                    if (line.Program_Line_Item_Pathway__r.name == 'New Facility') {
                                        if (line.Parent_Facility_for_New_Facility__c != null) {
                                            ID parentline = line.Parent_Facility_for_New_Facility__c;
                                            AC__c parent = lineIdlinemap.get(line.Parent_Facility_for_New_Facility__c);
                                            Authorizations__c auth = authidauth.get(line.Authorization__c);
                                            system.debug('+++++ parentauth' + authidauth.get(parent.Authorization__c));
                                            Authorizations__c parentauth = authidauth.get(parent.Authorization__c);
                                            auth.Parent_Authorization__c = parentauth.id;
                                            passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                            
                                        }
                            
                                    }
                                }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                                for (Authorizations__c auth: AuthLineListMap.keyset()) {
                                    if (((auth.Auth_Type_Hidden_Flag__c == 'product') || (auth.Auth_Type_Hidden_Flag__c == 'Ingredient')) && (auth.Regulated_Article_PackageId__c != null || auth.Regulated_Article_PackageId__c != '')) {
                                        if (auth.Auth_Type_Hidden_Flag__c == 'product') {
                                            packageParentAuthmap.put(auth.Regulated_Article_PackageId__c, auth);
                                        }
                                        if (auth.Auth_Type_Hidden_Flag__c == 'Ingredient') {
                                            Ingredientauthlist.add(auth);
                                        }
                                    }
                            
                                }
                            
                            
                            
                                if (Ingredientauthlist != null) {
                                    for (Authorizations__c auth: Ingredientauthlist) {
                                        system.debug('packageParentAuthmap auth.Regulated_Article_PackageId__c ' + packageParentAuthmap.get(auth.Regulated_Article_PackageId__c));
                                        auth.Parent_Authorization__c = packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).id;
                                        Integer i = 1;
                                        if (packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c == null) {
                                            packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c = string.valueOf(i);
                                        } else {
                                            Integer j;
                                            j = integer.valueOf(packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c);
                                            j++;
                                            packageParentAuthmap.get(auth.Regulated_Article_PackageId__c).Child_auth_count__c = string.valueOf(j);
                                        }
                                        passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                            
                                    }
                                }
                            
                            
                                for (Authorizations__c auth: AuthLineListMap.keyset()) {
                                    if (!passingMap.containskey(auth)) {
                                        passingMap.put(auth, AuthIdLineListmap.get(auth.id));
                                    }
                                }
                            
                            }
                            
                            
                            
                            CARPOL_UNI_Auth_Junction_WF createAuthjunction = new CARPOL_UNI_Auth_Junction_WF();
                            createAuthjunction.createAuthorizationJunctionRecord(passingMap); //***************************** creates auth junctions, wf tasks and update authorizations
                            
                            GenericHistoryClass.CreateHistoryRecord(trigger.new, trigger.oldmap, 'Application__c');
                          }
                     }
              }
    }