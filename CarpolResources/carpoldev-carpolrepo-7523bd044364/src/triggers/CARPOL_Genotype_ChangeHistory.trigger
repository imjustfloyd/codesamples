trigger CARPOL_Genotype_ChangeHistory on Genotype__c (after update) {
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Genotype__c');
}