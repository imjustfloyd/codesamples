trigger CARPOL_BRS_PhenotypeTrigger on Phenotype__c (after update) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_PhenotypeTrigger                      */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: Change History                               */      
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION AUTHOR  DATE    DETAIL  RELEASE/CSR           */
/*  1.0 - D12/Kishore 05/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */
   
        GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Phenotype__c');
         
}