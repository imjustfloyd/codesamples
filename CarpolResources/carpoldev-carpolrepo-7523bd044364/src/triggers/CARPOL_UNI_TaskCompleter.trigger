trigger CARPOL_UNI_TaskCompleter on Workflow_Task__c  (After Update,before update) {
    
    Set<id> triggerIds = Trigger.newMap.keyset();
    map<id,Workflow_Task__c> wfMap=new map<id,Workflow_Task__c>([select id,Incident_Number__c,Incident_Number__r.Facility__c,Status__c FROM Workflow_Task__c WHERE ID IN: triggerIds]);
if(checkRecursive.runOnce() || test.isRunningTest()){    
if (Trigger.isBefore) {
   
    
    for(Workflow_Task__c WF :trigger.new){
    system.debug('WF.Incident_Number__c '+WF.Incident_Number__c);
    system.debug('WF.Status_Categories__c + '+WF.Status_Categories__c);
    system.debug('WF.status__c+ '+WF.status__c);
    system.debug('WF.Name +'+WF.Name);
    system.debug('');
       if(WF.Incident_Number__c !=null && WF.Status_Categories__c!=null && WF.Status_Categories__c.contains('Approval') && WF.status__c.contains('Pending') && WF.Name.contains('Inspection')) {
            if(wfMap.get(WF.id).Incident_Number__r.Facility__c == null){
                WF.adderror('Cannot initiate inspection because Incident is not related to a Facility');
            } 
        }else{
            continue;
        }
    }
    }
   
}
//}
if (Trigger.isAfter) {
        

if(CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter) {
       system.debug('Inside CARPOL_UNI_TaskCompleter ##### ');
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter = false;
        list<Workflow_Task__c> pauseWF = new list<Workflow_Task__c>();
         List<Inspection__c> InspectnList = new List<Inspection__c>(); //added by niha
         List <Authorizations__c> Authlist =  new list <Authorizations__c>();
 
           
         Authlist = [select id, name, status__c from Authorizations__c ];
            for(Workflow_Task__c a:trigger.new)
            {
                 String UpdateField = '';
                 String SourceField = '';
                 String DependentField = '';
                 String stopLoop='False';
                 Decimal FinalWorkFlowOrder=0;
                 
                 if(a.Incident_Number__c==null){
                     
                 Authorizations__c auth = new Authorizations__c();
                 auth = [select Id,Status__c from Authorizations__c where Id=:a.Authorization__c];
                 
                 
      /************************************* Workflow Task = IN PROGRESS ****************************************************************/
                if(a.Status__c == 'In Progress'){    
                   Map<String, Schema.SObjectField> M = Schema.SObjectType.Authorizations__c.fields.getMap();
                  for(String key : M.KeySet())
                  {
                    if(a.Update_Record_Field_Name__c == M.get(key).getDescribe().getLabel())
                    {
                     UpdateField = key;
                    }
                 }
            
                 if(Sourcefield=='' && a.Update_Record_to_Specific_Val_InProgress__c != null){
                    Sourcefield=a.Update_Record_to_Specific_Val_InProgress__c;
                    auth.put(UpdateField,SourceField); 
                 }
                 
                    if(a.In_Progress_Status__c <> ''  && a.Update_Record_Status__c==True){
                        auth.Status__c =  a.In_Progress_Status__c;
                    }
                 update auth;    
                 } 
                 
                 
 
      /************************************* Workflow Task = PENDING ****************************************************************/
                 if(a.Status__c=='Pending')
                 {
                     
                       pauseWF = [select Id,name, Dependent_On_Workflow__c,Trigger_Activated__c,Dependency_Field__c,Required_if_Prior_Task_Equals__c,Locked__c from Workflow_Task__c where Status__c = 'Not Started' and Dependent_On_Workflow__c=:a.Workflow_Order__c and Authorization__c=:a.Authorization__c Limit 1];
                       for(Workflow_Task__c pauseWFLst:pauseWF )  
                                 {
                                   pauseWFLst.Status__c = 'Waiting';
                                   pauseWFLst.Trigger_Activated__c = True;
                                   pauseWFLst.Locked__c = 'Yes';
                                   update  pauseWFLst;   //*********************** UPDATE AUTHORIZATION  *************************//
                                   stopLoop = 'True';
                                         }

                        Map<String, Schema.SObjectField> M = Schema.SObjectType.Authorizations__c.fields.getMap();
                        for(String key : M.KeySet())
                        {
                              if(a.Update_Record_Field_Name__c == M.get(key).getDescribe().getLabel())
                               {
                              UpdateField = key;
                              }
                        }
                    
                        if(Sourcefield=='' && a.Update_Record_to_Specific_Value_Pending__c != null){
                            Sourcefield=a.Update_Record_to_Specific_Value_Pending__c;
                            auth.put(UpdateField,SourceField); 
                        }
                        
 
                        if(a.In_Progress_Status__c <> ''  && a.Update_Record_Status__c==True){
                            auth.Status__c =  a.In_Progress_Status__c;
                        }
 
                       update auth; //*********************** UPDATE AUTHORIZATION  *************************//
            
                        if(a.Status_Categories__c == 'Customer Feedback'){
                           AC__c lineItem =  [select Id,Status__c,Authorization__c from AC__c where Authorization__c =: a.Authorization__c and status__c = 'submitted'  limit 1];
                           lineItem.Status__c = 'Waiting on Customer';
                           update lineItem;  //*********************** UPDATE LINE ITEM  *************************//
                        }
                  }
                  
                  
      /************************************* Workflow Task = COMPLETE/DENIED/DEFERRED ****************************************************************/
         
                 if(a.Status__c=='Complete' || a.Status__c=='Denied' || a.Status__c == 'Deferred')
                   {              
                        
                         if((a.Status__c == 'Complete') && (a.Program__c == 'BRS')){
                          CARPOL_UNI_LineItemRLStatusClass sClass = new CARPOL_UNI_LineItemRLStatusClass();
                          sClass.updateStatus(a);
                         }

                         // Check for dependent workflows
                         list<Workflow_Task__c> nextWF = new list <Workflow_Task__c>();
                         list<Workflow_Task__c> nextWF1 = new list <Workflow_Task__c>();
                         
                          nextWF = [select Id,Dependent_On_Workflow__c,Trigger_Activated__c,Workflow_Order__c,Dependency_Field__c, Status__c, Process_Type__c, Required_if_Prior_Task_Equals__c, OwnerId from Workflow_Task__c where Status__c = 'Waiting'  and Dependent_On_Workflow__c=:a.Workflow_Order__c and Authorization__c=:a.Authorization__c order by Workflow_Order__c];

                          for(Workflow_Task__c authWorkFlowVar:nextWF )  
                         {
                             Workflow_Task__c nextTask = new Workflow_Task__c();
                             FinalWorkFlowOrder = authWorkFlowVar.Workflow_Order__c;
                             Map<String, Schema.SObjectField> WTapi = Schema.SObjectType.Workflow_Task__c.fields.getMap();

                           for(String key : WTapi.KeySet())
                           {
                                 if(authWorkFlowVar.Dependency_Field__c == WTapi.get(key).getDescribe().getLabel())
                                 {
                                   DependentField = key;
                                 }
                            }

                          If(DependentField != '')
                          {
                               If(a.get(DependentField) == authWorkFlowVar.Required_if_Prior_Task_Equals__c)
                               {
                                    authWorkFlowVar.Status__c = 'Not Started';
                                    authWorkFlowVar.Trigger_Activated__c = True;
                                    update authWorkFlowVar;  //*********************** UPDATE Workflow Task  *************************//
                                    FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                                    stopLoop = 'True';
                               }
                                else if(stopLoop=='False')
                                {
                                        authWorkFlowVar.Status__c = 'Deferred'; 
                                        authWorkFlowVar.Trigger_Activated__c = True;
                                        FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                                        update authWorkFlowVar; //*********************** UPDATE AUTHORIZATION  *************************//
                                }
                            
                           }             
                   
                    
                            if(stopLoop=='False')
                            {
                                 //system.debug('=== FinalWorkFlowOrder = '+FinalWorkFlowOrder);
                                 //system.debug('=== a.Authorization__c = '+ a.Authorization__c);                                 
                                 Workflow_Task__c  finalWF = [select Id,Dependent_On_Workflow__c,Trigger_Activated__c,Dependency_Field__c,Required_if_Prior_Task_Equals__c from Workflow_Task__c where Status__c = 'Waiting' and Workflow_Order__c=:FinalWorkFlowOrder and Authorization__c=:a.Authorization__c Limit 1];

                                        finalWF.Status__c = 'Not Started';
                                        finalWF.Owner_Approver_is_Signatory__c = True;
                                        finalWF.Trigger_Activated__c = True;
                                   update finalWF;  //*********************** UPDATE Workflow Task  *************************//
                             }
                         }
               
               auth = [select Id, Status__c from Authorizations__c where Id=:a.Authorization__c];
                
               if(auth!=null){

                         if(a.Record_Status__c <> ''  && a.Update_Record_Status__c==True && a.Status__c != 'Denied' && a.Status__c != 'Deferred') 
                          {
                             auth.status__c = a.Record_Status__c;
                           }
                         else  if(a.Record_Status__c <> ''  && a.Update_Record_Status__c==True && a.Status__c == 'Denied') 
                               {
                                 auth.status__c = a.Stop_Status__c;
                               }

                        if(a.Owner_Approver_is_Signatory__c == True  && a.ApproverId__c <> null && a.Status__c == 'Complete' ) 
                           {
                                auth.Issuer__c = a.ApproverId__c;
                                auth.Issuer_Name__c = a.Approver_Name__c;
                            }
                         else  if(a.Owner_Approver_is_Signatory__c == True  && a.ApproverId__c == null && a.Status__c == 'Complete' ) 
                          {
                               auth.Issuer__c = UserInfo.getUserId();
                               auth.Issuer_Name__c = UserInfo.getFirstName()+' '+UserInfo.getLastName();
                           }
                           
                        if(a.Update_Record_Field_Name__c !=null)
                            {
    
                                 if(a.Task_Source_Field_Name__c != null)
                                 {
                                     Map<String, Schema.SObjectField> WTapi = Schema.SObjectType.Workflow_Task__c.fields.getMap();
                                       for(String key : WTapi.KeySet())
                                       {
                                         if(a.Task_Source_Field_Name__c == WTapi.get(key).getDescribe().getLabel())
                                         {
                                           SourceField = key;
                                         }                       
                                      }
                                 }
                                Map<String, Schema.SObjectField> M = Schema.SObjectType.Authorizations__c.fields.getMap();
                                for(String key : M.KeySet())
                                {
                                   // system.debug('--->'+M.get(key).getDescribe().getLabel());
                                    //system.debug('--->Record:'+a.Update_Record_Field_Name__c );
                                      if(a.Update_Record_Field_Name__c == M.get(key).getDescribe().getLabel())
                                      {
                                         UpdateField = key;
                                      }
                                }
                                       
                             IF(Sourcefield=='' && a.Status__C=='Complete' && a.Update_Record_to_Specific_Value__c != null)
                             {
                                 Sourcefield=a.Update_Record_to_Specific_Value__c;
                                 //if(Sourcefield== 'true')
                                 //Sourcefield = 'Yes';
                                 //system.debug('sourcefield--->'+Sourcefield+'    -    '+UpdateField);
                                 auth.put(UpdateField,SourceField);
                                 //system.debug('--->'+auth);
                                 
                             }
                             else IF(Sourcefield=='' && a.Status__C=='Denied' && a.Update_Record_to_Specific_Value_Denied__c != null)
                             {
                                Sourcefield=a.Update_Record_to_Specific_Value_Denied__c;
                                auth.put(UpdateField,SourceField);
                             }
                             else IF(Sourcefield!='')
                             {
                                 //Updates by Label configuration
                                 auth.put(UpdateField,a.get(SourceField));
                                
                             }                         

                        } 
                             update auth;   //*********************** UPDATE AUTHORIZATION  *************************//

                     }
         }
             
             }
             // For Incident
             else
             {
              if(a.Status__c == 'In Progress' && a.Name.contains('Inspection'))
                {
                   Inspection__c i = new Inspection__c();
                   i.Status__c = 'New';
                   i.Facility__c = wfMap.get(a.id).Incident_Number__r.Facility__c;
                   i.Incident__c = a.Incident_Number__c;
                    InspectnList.add(i);
                }
                Incident__c Inc = new Incident__c();
                Inc = [select Id,Status__c, Authorization__c,Facility__c from Incident__c where Id=:a.Incident_Number__c];
             
                /************************************* Workflow Task = IN PROGRESS ****************************************************************/
                if(a.Status__c == 'In Progress')
                {    
                  Map<String, Schema.SObjectField> M = Schema.SObjectType.Incident__c.fields.getMap();
                  for(String key : M.KeySet())
                  {
                    if(a.Update_Record_Field_Name__c == M.get(key).getDescribe().getLabel())
                    {
                     UpdateField = key;
                    }
                 }
            
                 if(Sourcefield=='' && a.Update_Record_to_Specific_Val_InProgress__c != null){
                    Sourcefield=a.Update_Record_to_Specific_Val_InProgress__c;
                    Inc.put(UpdateField,SourceField); 
                 }
                    if(a.In_Progress_Status__c <> ''  && a.Update_Record_Status__c==True){
                        Inc.Status__c =  a.In_Progress_Status__c;
                    }
                    update Inc;    
                } 
                /************************************* Workflow Task = DEFERRED/COMPLETE ****************************************************************/
                 if(a.Status__c=='Complete' || a.Status__c=='Denied' || a.Status__c == 'Deferred')
                   { 
                // Check for dependent workflows
                         list<Workflow_Task__c> nextWF = new list <Workflow_Task__c>();
                         list<Workflow_Task__c> nextWF1 = new list <Workflow_Task__c>();
                         
                          nextWF = [select Id,Dependent_On_Workflow__c,Trigger_Activated__c,Workflow_Order__c,Dependency_Field__c, Status__c, Process_Type__c, Required_if_Prior_Task_Equals__c, OwnerId from Workflow_Task__c where Status__c = 'Waiting'  and Dependent_On_Workflow__c=:a.Workflow_Order__c and Incident_Number__c=:a.Incident_Number__c order by Workflow_Order__c];

                          for(Workflow_Task__c authWorkFlowVar:nextWF )  
                         {
                             Workflow_Task__c nextTask = new Workflow_Task__c();
                             FinalWorkFlowOrder = authWorkFlowVar.Workflow_Order__c;
                             Map<String, Schema.SObjectField> WTapi = Schema.SObjectType.Workflow_Task__c.fields.getMap();

                           for(String key : WTapi.KeySet())
                           {
                                 if(authWorkFlowVar.Dependency_Field__c == WTapi.get(key).getDescribe().getLabel())
                                 {
                                   DependentField = key;
                                 }
                            }

                          If(DependentField != '')
                          {
                               If(a.get(DependentField) == authWorkFlowVar.Required_if_Prior_Task_Equals__c)
                               {
                                    authWorkFlowVar.Status__c = 'Not Started';
                                    authWorkFlowVar.Trigger_Activated__c = True;
                                    update authWorkFlowVar;  //*********************** UPDATE Workflow Task  *************************//
                                    FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                                    stopLoop = 'True';
                               }
                                else if(stopLoop=='False')
                                {
                                        authWorkFlowVar.Status__c = 'Deferred'; 
                                        authWorkFlowVar.Trigger_Activated__c = True;
                                        FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                                        update authWorkFlowVar; //*********************** UPDATE AUTHORIZATION  *************************//
                                }
                            
                           }             
                   
                    
                            if(stopLoop=='False')
                            {
                                 Workflow_Task__c  finalWF = [select Id,Dependent_On_Workflow__c,Trigger_Activated__c,Dependency_Field__c,Required_if_Prior_Task_Equals__c from Workflow_Task__c where Status__c = 'Waiting' and Workflow_Order__c=:FinalWorkFlowOrder and Incident_Number__c=:a.Incident_Number__c Limit 1];

                                        finalWF.Status__c = 'Not Started';
                                        finalWF.Owner_Approver_is_Signatory__c = True;
                                        finalWF.Trigger_Activated__c = True;
                                   update finalWF;  //*********************** UPDATE Workflow Task  *************************//
                             }
                         }
                if(a.Status__c == 'Deferred')
                {    
                 
                    if(a.Stop_Status__c <> ''  && a.Update_Record_Status__c==True){
                        Inc.Status__c =  a.Stop_Status__c;
                    }
                    update Inc;    
                } 
                  if(a.Status__c == 'Complete')
                {    
                    //system.debug('<! Status of the workflow task is '+a.Record_Status__c);
                    if(a.Record_Status__c <> ''  && a.Update_Record_Status__c==True){
                        Inc.Status__c =  a.Record_Status__c;
                    }
                    //system.debug('updating the Incident');
                    update Inc;    
                } 
             
                   }
                   }
             
        }
        if(InspectnList.size()>0){
            //system.debug('inspection records to insert'+InspectnList.size());
            try{
                insert InspectnList;
                
            }catch (system.Dmlexception e){
                system.debug(e);
            }
        }
      }
}
}