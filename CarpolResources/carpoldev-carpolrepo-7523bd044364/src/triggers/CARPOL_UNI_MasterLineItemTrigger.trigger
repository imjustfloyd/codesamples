trigger CARPOL_UNI_MasterLineItemTrigger on AC__c (before insert, after insert, before update, after update, before delete, after delete) 
    {
        if (Trigger.isBefore) //====================================================BEFORE=================================================
            {
                if (Trigger.isInsert) //-----------------------------------------before insert ----------------------------
                    {
                        
                        map<Id, AC__c> mapLinesNew;
                        mapLinesNew = new map<Id, AC__c>();
                        for(AC__c o : trigger.new)
                        {
                           mapLinesNew.put(o.Id, o);
                           
                           //this is necessary evil------------------------------------------------------------------------
                           List<Regulations_Association_Matrix__c> regDm = [Select id from Regulations_Association_Matrix__c where id in
                                                                            (Select Decision_Matrix__c from Related_Line_Decisions__c
                                                                            where Line_Item__c =: o.id)]; 
                             
                           o.Total_Fees__c = 0.00;                                              
                           List<Related_Fees__c> relatedFees = new List<Related_Fees__c>();
                           relatedFees = [Select id, Amount__c, Decision_Matrix__c from Related_Fees__c where Decision_Matrix__r.id in : regDm];
                           
                           if(relatedFees != null)
                           {
                               for(Related_Fees__c fee: relatedFees) //add the related fees from the associated DM to the line item
                                   o.Total_Fees__c += fee.Amount__c;
                           }
                            
                        }  
                        
                        //CARPOL_AssociateProgramPathway Getpathway = new CARPOL_AssociateProgramPathway(mapLinesNew); 
                        //Getpathway.CARPOL_AssociateProgramPathway();  
                        
                        CARPOL_UNI_CopyContacts.copying(trigger.new);
                         
                        CARPOL_DuplicatePreventer dupchk = new CARPOL_DuplicatePreventer(mapLinesNew);
                        dupchk.duplicatePreventer();
                         
                        CARPOL_ApplicationSharing appShare = new CARPOL_ApplicationSharing(mapLinesNew);//Added by RAM 
                        appShare.applicationSharing(); //Added by RAM 
                        
                        //CARPOL_EXPIRATIONDATE expdate = new CARPOL_EXPIRATIONDATE(mapLinesNew);//Added by RAM on 2/12/2016
                        //expdate.EXPIRATIONDATE_INSERT();//Added by RAM on 2/12/2016
                        
                
                    } 
                    
                if (Trigger.isUpdate) //-----------------------------------------before update ----------------------------
                    {
                        map<Id, AC__c> mapLinesNew;
                        mapLinesNew = new map<Id, AC__c>();
                        for(AC__c o : trigger.new)
                        {
                           mapLinesNew.put(o.Id, o);
                           
                           //this is necessary evil------------------------------------------------------------------------
                           List<Regulations_Association_Matrix__c> regDm = [Select id from Regulations_Association_Matrix__c where id in
                                                                            (Select Decision_Matrix__c from Related_Line_Decisions__c
                                                                            where Line_Item__c =: o.id)]; 
                             
                           o.Total_Fees__c = 0.00;                                              
                           List<Related_Fees__c> relatedFees = new List<Related_Fees__c>();
                           relatedFees = [Select id, Amount__c, Decision_Matrix__c from Related_Fees__c where Decision_Matrix__r.id in : regDm];
                           
                           if(relatedFees != null)
                           {
                               for(Related_Fees__c fee: relatedFees) //add the related fees from the associated DM to the line item
                                   o.Total_Fees__c += fee.Amount__c;
                           }
                            
                        }  
                        
                        //CARPOL_AssociateProgramPathway Getpathway = new CARPOL_AssociateProgramPathway(mapLinesNew); 
                        //Getpathway.CARPOL_AssociateProgramPathway();
            
                        CARPOL_ApplicationSharing appShare = new CARPOL_ApplicationSharing(mapLinesNew);
                        appShare.applicationSharing();  
                        
                        //CARPOL_EXPIRATIONDATE expdate = new CARPOL_EXPIRATIONDATE(mapLinesNew);//Added by RAM on 2/12/2016
                        //expdate.EXPIRATIONDATE_INSERT();//Added by RAM on 2/12/2016
                    }
                
                if (Trigger.isDelete) //-----------------------------------------before delete ----------------------------
                    {
                        
                        
                    }
           
             }
    
        if (Trigger.IsAfter) //====================================================AFTER=================================================
            {
                if (Trigger.isInsert) //-----------------------------------------after insert ----------------------------
                    {
                        //the next two classes update fields on the line items, we then take changes and update in storage
                        List<AC__c> lineitemlist = CARPOL_UNI_RelatedLineDecision.insertRLD(trigger.new); //Inserting Related Line decisions
                        lineitemlist = CARPOL_UNI_AuthGroupingString.groupString(lineitemlist); //Create Auth group string
                        //set now so this update will not run additional classes
                        checkRecursive.runOnce();
                        update lineitemlist;                      
                        if(!system.isFuture())
                          {
                              for(AC__c ac : Trigger.new)
                                  {
                                    ViolatorLookupService.lookupViolator(ac.ID);  
                                  }
                          }
                    } 
                    
                    
                 if (Trigger.isUpdate) //-----------------------------------------after update ----------------------------
                    {
                        //map<Id, AC__c> mapLinesNew = new map<Id, AC__c>();
                        Integer x=0;
                        for(AC__c o : trigger.new)
                        {
                            system.debug('< Status is '+o.status__c);
                            if(o.Status__c == 'Submitted'){
                                x++;
                            }
                        }
                        system.debug('X value is '+x);
                        //system.debug('<<!!! mapLinesNew '+mapLinesNew.get();
                          if(checkRecursive.runOnce() && (x<1))
                            {
                               system.debug('entering');
                                CARPOL_UNI_Update_RLD.updateRLD(trigger.old);
                                //the next two classes update fields on the line items, we then take changes and update in storage
                                List<AC__c> lineitemlist = CARPOL_UNI_RelatedLineDecision.insertRLD(trigger.new);
                                lineitemlist = CARPOL_UNI_AuthGroupingString.groupString(lineitemlist);
                                
                                update lineitemlist;
                                                      
                                if(!system.isFuture())
                                 {
                                      for(AC__c ac : Trigger.new)
                                          {
                                            ViolatorLookupService.lookupViolator(ac.ID);  
                                          }
                                  }
                             }
                    // Capture changes of fields on line item object
                    GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'AC__c');
                             
                    }
                    
                  if (Trigger.isDelete) //-----------------------------------------after delete ----------------------------
                        {
                            map<Id, AC__c> mapLinesNew;
                            mapLinesNew = new map<Id, AC__c>();
                            for(AC__c o : trigger.old)
                                {
                                   mapLinesNew.put(o.Id, o);
                                } 
                            CARPOL_ApplicationSharing appShare = new CARPOL_ApplicationSharing(mapLinesNew);//Added by RAM on 2/12/2016
                            appShare.applicationsharing_delete(); //Added by RAM on 2/12/2016
                           
                        }
             }
    }