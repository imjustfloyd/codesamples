trigger CARPOL_UNI_Questionnaire_Question_copy_trigger on EFL_Inspection_Questionnaire_Questions__c (before insert, after insert) {
    if(trigger.isBefore){
        if(trigger.isInsert){
    for(EFL_Inspection_Questionnaire_Questions__c Question:trigger.new){
        if(Question.Source_Question__c!=null || Question.Source_Question__c!=''){
            EFL_Inspection_Questions__c source = [SELECT ID,Name,Question__c FROM EFL_Inspection_Questions__c WHERE ID=: Question.Source_Question__c LIMIT 1];
            Question.Question__c = source.Question__c;
        }
    }
}
}
    if(trigger.isAfter){
        if(trigger.isInsert){
            Map<ID,ID>SourceQQuestionnaireQmapp = new Map<ID,ID>();
            List<EFL_Inspection_Questionnaire_Responses__c>OptionsToInsert = new List<EFL_Inspection_Questionnaire_Responses__c>();
            List<EFL_Inspection_Responses__c> Options = new List<EFL_Inspection_Responses__c>();
            for(EFL_Inspection_Questionnaire_Questions__c Question:trigger.new){
                if(Question.Source_Question__c!=null || Question.Source_Question__c!=''){
                    
                    Options  = [SELECT ID,Name, Response__c, EFL_Inspection_Questions__c FROM EFL_Inspection_Responses__c WHERE EFL_Inspection_Questions__c=:Question.Source_Question__c];
                    
                    SourceQQuestionnaireQmapp.put(Question.Source_Question__c, Question.ID);
                }
                
            }
           
           if(!Options.IsEmpty() || Options!=null){
            
            for (EFL_Inspection_Responses__c Opt:Options)
                    {
                        if(SourceQQuestionnaireQmapp.containsKey(Opt.EFL_Inspection_Questions__c)){
                            EFL_Inspection_Questionnaire_Responses__c  OCreate = new EFL_Inspection_Questionnaire_Responses__c();
                            OCreate.Response__c = Opt.Response__c;
                            OCreate.EFL_Inspection_Questionnaire_Questions__c = SourceQQuestionnaireQmapp.get(Opt.EFL_Inspection_Questions__c);
                            OptionsToInsert.add(OCreate);
                        }
                    }
            
             if (OptionsToInsert != null || !OptionsToInsert.isEmpty()) 
                    {
                        insert OptionsToInsert;
                    }
            
        }    

                    
        }
    }
}