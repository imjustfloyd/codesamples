trigger PreparationShareHandshake on Preparer_Request__c  (after update) 
{


//loop through each contact record, capturing the ownerid and accountid associated with it
// for each one, create a new AccountShare record specifying the accountid and the contactid to provide access to, as well as the access level
// we will compile all of the new AccountShare objects into a single List and insert them all into Salesforce at one time

//problem, if an AccountShare already exists this trigger will fail with an INSUFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY error
// you could query to verify that an account share doesn't already exist but that won't fly on bulk inserts/updates and it
// also doesn't account for groups that inherently have access
 
//create a container to hold all of the new AccountShares

string strprofileid = UserInfo.getProfileId();
String usremail = UserInfo.getUserEmail();
system.debug('usremail ## '+usremail);
string ProfileName = '';
string contid = '';
boolean issubmitted = false;
Profile objProfile  = new Profile();
objProfile = [select id,Name from Profile where id=:strprofileid];
ProfileName = objProfile.Name;
system.debug('ProfileName ###'+ProfileName);
 if(ProfileName=='System Administrator' ){
    
List<AccountShare> newAccountShares = new List<AccountShare>();
Set<ID> ids = Trigger.newMap.keySet();
User newUserAcc = new User();

    for(Preparer_Request__c theRequest : trigger.new) //loop through all contacts in the trigger's dataset
    {
    If(theRequest.Email_Confirmation_Sent__c == True) {
    
    system.debug('**********************' + theRequest.Preparer_Broker_Name__c);
    system.debug('**********************' + theRequest.Preparer_Broker_Name__r.Id);
    
    newUserAcc = [SELECT Id FROM user WHERE IsActive=True and ContactId =:theRequest.Preparer_Broker_Name__c LIMIT 1];
    
    
    AccountShare thisAccountShare = new AccountShare(); //a new empty AccountShare object
    thisAccountShare.userorgroupid = newUserAcc.Id;
    thisAccountShare.accountid = theRequest.Applicant_Account1__c;
    thisAccountShare.accountaccesslevel = 'Read';
    thisAccountShare.OpportunityAccessLevel = 'None';
    thisAccountShare.CaseAccessLevel = 'None';
    thisAccountShare.ContactAccessLevel = 'Read';
    
    newAccountShares.add(thisAccountShare);
    }

    }
    
    //now that we have created all of our new AccountShare objects, let's insert them into Salesforce
    system.debug(newAccountShares);
    insert newAccountShares;
    
}
}