trigger CARPOL_UNI_MasterIncidentTrigger on Incident__c (before insert, after insert, before update, after update, before delete, after delete) {
if (Trigger.isBefore) //====================================================BEFORE=================================================
          {
            
           // if (Trigger.isInsert) 
             //   {
                    List<String> devnames = new List<String>{'EFL_BRS_Enforcement_Officer_Queue','AC_Enforcement_Officer_Queue','PPQ_Compliance_Officer','VS_Enforcement_Officer_Queue','EFL_Enforcement_Officer_Queue'};
                         List<Group> reviewerqueue = [select Id, DeveloperName from Group WHERE DeveloperName IN :devnames  AND Type = 'Queue' ];
                         String QueueDeveloperName;
                         Map<String, ID> QueueIdmap = new Map<String, ID>();
                         for(Group gp : reviewerqueue)
                         {
                            QueueIdmap.put(gp.DeveloperName,gp.ID);
                         }
                         
                    for(Incident__c ic:trigger.new){
                         system.debug('Related_Program__c:::::'+ic.Related_Program__c);
                          system.debug('Program_compliance_officer__c:::::'+ic.Program_compliance_officer__c);
                        if(ic.Program_compliance_officer__c==null){
                            if(ic.Related_Program__c=='BRS')
                                QueueDeveloperName = 'EFL_BRS_Enforcement_Officer_Queue';
                                ic.OwnerId=QueueIdmap.get(QueueDeveloperName);
                            if(ic.Related_Program__c=='AC')
                                QueueDeveloperName = 'AC_Enforcement_Officer_Queue';
                                ic.OwnerId=QueueIdmap.get(QueueDeveloperName);
                            if(ic.Related_Program__c=='PPQ')
                                QueueDeveloperName = 'PPQ_Compliance_Officer';
                                ic.OwnerId=QueueIdmap.get(QueueDeveloperName);
                            if(ic.Related_Program__c=='VS')
                                QueueDeveloperName = 'VS_Enforcement_Officer_Queue';
                                ic.OwnerId=QueueIdmap.get(QueueDeveloperName);
                            if(ic.Related_Program__c=='Unknown')
                                QueueDeveloperName = 'EFL_Enforcement_Officer_Queue';
                                ic.OwnerId=QueueIdmap.get(QueueDeveloperName);
                        }else{
                             ic.OwnerId=ic.Program_compliance_officer__c;
                          }
                        }
                  //  }
                
          
//if (Trigger.isBefore) //====================================================BEFORE=================================================
         // 
            
            if (Trigger.isUpdate) 
                {
                    
                    for(Incident__c i:trigger.new){
                        // Changes by Niha.
                        if(i.Facility__c != null){
                        Facility__c fac =  [select Id,Status__c from Facility__c where Id=:i.Facility__c LIMIT 1];
                            if( i.status__c =='Not in Compliance' ){
                                        fac.Status__c = 'Inactive';
                                        system.debug('<!!! fac '+fac);
                                        update fac;
                                 } 
                            if( i.status__c =='Compliant' ){
                                        fac.Status__c = 'Active';
                                        system.debug('<!!! fac '+fac);
                                        update fac;
                                 } 
                                 
                        //End of changes.
                    }
                }
              
          }
          }
 if (Trigger.IsAfter) //====================================================AFTER=================================================
          {
            
            if (Trigger.isUpdate) 
                {
                    system.debug('<!!! incident.s');
                    if(checkRecursive.runOnce()){
                         system.debug('<!!! incident.s inside');
                     Incident__c Inc = new Incident__c();
                     Program_Prefix__c  prefix = new  Program_Prefix__c();
                     list < Workflow_Task__c > WFtasksinsert = new list < Workflow_Task__c > ();
                     Boolean execute=false;
                
                     for(Incident__c incident:trigger.new)
                      {   
                        system.debug('<!!! incident.status__c 1'+incident.status__c);
                                system.debug('<!!! incident.Facility__c 2'+incident.Facility__c);
                                
                        if(incident.status__c=='Formal Incident' && incident.EFL_WFTasks_created__c==FALSE){
                          try{  
                            Inc = [SELECT ID, NAME, EFL_WFTasks_created__c FROM Incident__c WHERE ID=:trigger.new[0].id];
                            prefix=[Select ID,Name,Program__r.Name  FROM Program_Prefix__c WHERE Program__r.Name=:incident.Related_Program__c AND Incident_Related__c=TRUE LIMIT 1];
                            execute = True;
                            system.debug('<!!! prefix is '+prefix);
                            }catch(Exception e){
                                 incident.adderror('Please select a Related Program before you deem this Incident to be a Formal Incident');
                            }
                        }
                       
                            
                      }
                
                      system.debug('<!!! prefix outside is '+prefix);
                      if(prefix.id!=null && execute==True)
                      {
                        system.debug('<!!! prefix somehow inside'+prefix);
                        list<Program_Workflow__c> programworkflows = [select id,Name,Stop_Status__c,Owner_Approver_is_Signatory__c, Requires_Approval__c,Only_for_this_Role__c, Update_Record_to_Specific_Value_Pending__c,
                                                         Update_Record_to_Specific_Value_Denied__c,Update_Record_to_Specific_Value__c,Assign_to_Queue__c,Authorization_Status__c,Active__c,
                                                         Object_to_Associate_this_Task_to__c,Comments_Destination_Field__c,Copy_Comments_to_Authorization__c,Default_Priority__c,Default_Status__c,
                                                         Dependency_Field__c,Dependent_On_Workflow__c,Description__c,Generate_as_Task__c,How_Executed__c,Internal_Only__c,Program__c,Program_Prefix__c,
                                                         Program_Prefix__r.Name,Program__r.name,Required_if_Prior_Task_Equals__c,Task_Record_Type__c,Update_Authorization_Status__c,Update_Record_Field_Name__c,
                                                         Task_Source_Field_Name__c,Workflow_Order__c,buttons__c,Email_Template__c,Update_Record_to_Specific_Val_InProgress__c,Update_Related_Record_to_Specific_Value__c,
                                                         In_Progress_Status__c,Defer_all_Subsequent_Tasks__c, process_type__c from Program_Workflow__c where Program_Prefix__r.Name =:prefix.Name];
                      system.debug('<!!! programworkflows is '+programworkflows);
                       Map<String, Schema.RecordTypeInfo> RecordTypeMap;
                    RecordTypeMap = Schema.SObjectType.Workflow_Task__c.getRecordTypeInfosByName();
                   
                      Map<string,Id> GroupMap = new map<string,Id>();
                      for(Group g:[select ID,Name from Group LIMIT 10000]){
                        GroupMap.put(g.Name,g.Id);
                      } 
                     
                    for(Program_Workflow__c programWorkFlowVar: programworkflows )
                    {       system.debug('<!!! programWorkFlowVar '+programWorkFlowVar);
                         Workflow_Task__c WFTask = new Workflow_Task__c();
                         if(programWorkFlowVar.Active__c=='Yes' )
                         {
                            
                             WFTask.RecordTypeId = RecordTypeMap.get(programWorkFlowVar.Task_Record_Type__c).getRecordTypeId();
                             WFTask.name = programWorkFlowVar.Name;
                             //WFTask.OwnerId = GroupMap.get(programWorkFlowVar.Assign_to_Queue__c); 
                             WFTask.Priority__c = programWorkFlowVar.Default_Priority__c;
                             WFTask.Status__c = programWorkFlowVar.Default_Status__c;
                             WFTask.Owner_Approver_is_Signatory__c = programWorkFlowVar.Owner_Approver_is_Signatory__c;
                             WFTask.Comments_Destination_Field__c = programWorkFlowVar.Comments_Destination_Field__c;
                             WFTask.Copy_Comments_to_Authorization__c = programWorkFlowVar.Copy_Comments_to_Authorization__c;
                             WFTask.Workflow_Order__c = programWorkFlowVar.Workflow_Order__c;
                             WFTask.Update_Record_Status__c = programWorkFlowVar.Update_Authorization_Status__c;
                             WFTask.Update_Record_Field_Name__c = programWorkFlowVar.Update_Record_Field_Name__c;
                             WFTask.Task_Source_Field_Name__c = programWorkFlowVar.Task_Source_Field_Name__c;
                             WFTask.Dependent_On_Workflow__c = programWorkFlowVar.Dependent_On_Workflow__c;
                             WFTask.Dependency_Field__c = programWorkFlowVar.Dependency_Field__c;
                             WFTask.Required_if_Prior_Task_Equals__c = programWorkFlowVar.Required_if_Prior_Task_Equals__c;
                             WFTask.Stop_Status__c = programWorkFlowVar.Stop_Status__c;
                             WFTask.Record_Status__c = programWorkFlowVar.Authorization_Status__c;
                             WFTask.Update_Record_to_Specific_Value__c = programWorkFlowVar.Update_Record_to_Specific_Value__c;
                             WFTask.Update_Record_to_Specific_Value_Denied__c = programWorkFlowVar.Update_Record_to_Specific_Value_Denied__c;
                             WFTask.Update_Record_to_Specific_Value_Pending__c = programWorkFlowVar.Update_Record_to_Specific_Value_Pending__c;//added by Kishore
                             WFTask.Update_Record_to_Specific_Val_InProgress__c = programWorkFlowVar.Update_Record_to_Specific_Val_InProgress__c;//added by Kishore 10/19/2015
                             WFTask.Update_Related_Record_to_Specific_Value__c = programWorkFlowVar.Update_Related_Record_to_Specific_Value__c;//added by Kishore 10/20/2015                      
                             WFTask.Program_Prefix__c = programWorkFlowVar.Program_Prefix__r.Name;// added by Kishore
                             WFTask.Program__c = programWorkFlowVar.Program__r.name; //added by kishore
                             WFTask.Approval_Roles__c = programWorkFlowVar.Only_for_this_Role__c;
                             WFTask.Requires_Approval__c = programWorkFlowVar.Requires_Approval__c;
                             WFTask.buttons__c = programWorkFlowVar.buttons__c; // added by kishore
                             WFTask.Instructions__c = programWorkFlowVar.Description__c; // added by Kishore 10/10/2015
                             WFTask.In_Progress_Status__c = programWorkFlowVar.In_Progress_Status__c; // added by kishore 10/26/2015
                             WFTask.Defer_all_Subsequent_Tasks__c = programWorkFlowVar.Defer_all_Subsequent_Tasks__c; // added by kishore
                             WFTask.process_type__C = programWorkFlowVar.process_type__c;
                             WFTask.Incident_number__c = Inc.id;
                             WFtasksinsert.add(WFTask);
                         }
                    } 
             
                if(WFtasksinsert!=null){
                    insert WFtasksinsert;
                    Inc.EFL_WFTasks_created__c = TRUE;
                    update Inc;
                }
                }
                 
                 }   
                }

            }


}