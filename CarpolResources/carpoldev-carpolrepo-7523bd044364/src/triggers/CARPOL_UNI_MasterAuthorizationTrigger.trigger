trigger CARPOL_UNI_MasterAuthorizationTrigger on Authorizations__c (  before insert, after insert, 
  before update, after update, 
  before delete, after delete) {

  if (Trigger.isBefore) {
    if (Trigger.isInsert) {

    } 
    if (Trigger.isUpdate) {
      CARPOL_GeneratePermitNumber checker = new CARPOL_GeneratePermitNumber(Trigger.oldMap, Trigger.newMap);
      checker.generatePermitNumbers();
      
      Date AuthDate;
      Date expAuthDate;
      Date authexpdt;
      for(Authorizations__c auth:Trigger.new){
            
            if(auth.Process_type__c!='Fast Track' && auth.Authorization_Type__c == 'Permit' && (auth.Status__c ==  'Approved' || auth.Status__c == 'Issued'))
                {     
                    auth.Date_Issued__c = System.today();
                    
                    Integer ExpirationTimeframe;
                    if(auth.Expiration_Timeframe__c!=null){
                    ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe__c);
                    AuthDate =  Date.newInstance(auth.Date_Issued__c.Year(),auth.Date_Issued__c.month(), auth.Date_Issued__c.day());
                    authexpdt = AuthDate.addDays(ExpirationTimeframe);
                    
                    //Added by Tharun-08/11/2016
                    if(trigger.oldmap.get(auth.Id).Expiration_Timeframe__c!=auth.Expiration_Timeframe__c || auth.Expiration_Date__c==null)
                       auth.Expiration_Date__c = authexpdt;
                    }
                }
      }
    
     list<Label__c> lstlabels = new list<Label__c>();
        string authrt = '';
        string authid = '';
        string laboriginadd = '';
        string authautono = '';
        integer nooflabs = 0;
        boolean iscreatelabel =  false;
        Map<ID, Schema.RecordTypeInfo> authrtMap = Schema.SObjectType.Authorizations__c.getRecordTypeInfosById();
        Map<ID, Schema.RecordTypeInfo> locrtMap = Schema.SObjectType.Location__c.getRecordTypeInfosById();
          
        for(Authorizations__c a:trigger.new)
        {
             authrt = authrtMap.get(a.RecordTypeId).getName();
             Authorizations__c oldauth = new Authorizations__c();
             oldauth = trigger.oldmap.get(a.id);
             
             
             if( (!authrt.contains('Biotechnology'))){
                 if(a.BRS_Create_Labels__c==true && oldauth.BRS_Create_Labels__c==false && (a.Status__c=='Approved' || a.Status__c== 'Issued'))
                 {
                      authid = a.id;
                      iscreatelabel = true;
                      authautono = a.Name;
                      if(a.Expiry_Date__c !=null)
                          authexpdt = a.Expiry_Date__c;
                          
                      if(a.BRS_Number_of_Labels__c!=null)
                      {
                          nooflabs = a.BRS_Number_of_Labels__c.intValue();
                      }
                 }    
             }
              
        }
        if(iscreatelabel)
        { 
                         for(Location__c orl:[select id,Name,RecordTypeId from Location__c where Authorization__c=:authid])
                         {
                            string orgrtname = '';
                            orgrtname = locrtMap.get(orl.RecordTypeId).getName();
                            if(orgrtname=='Origin Location')
                            {
                                laboriginadd = orl.id;
                            }
                         }
                        if(nooflabs>0 && nooflabs<999 ) 
                        {
                            for(integer i=1;i<nooflabs+1;i++)
                            {
                                Label__c l = new Label__c();
                                l.Name = authautono+'-'+i;
                                l.Authorization__c = authid;
                                l.Notification_Expiration_Date__c = authexpdt;
                                l.Notification_Status__c = 'Active';
                                l.Status__c = 'Active';
                                system.debug('status#' + l.Status__c );
                                if(laboriginadd!='' && laboriginadd!=null)
                                {
                                l.Origin_Address__c = laboriginadd;
                                }
                                lstlabels.add(l);
                                
                            }
                        } 
                       
                        if(lstlabels.size()>0)
                        {
                            insert lstlabels;
                        }
                      
        
        
       
      //  VV 3-17-16 end
        }
    }
    if (Trigger.isDelete) {
      // Call class logic here!
    }
  }

  if (Trigger.IsAfter) {
    if (Trigger.isInsert) {
    
       for (Authorizations__c auth: trigger.new) {
       system.debug('++++auth.status__c = '+auth.status__c);
       system.debug('++++auth.prefix__c = '+auth.prefix__c);
       system.debug('++++auth.Authorization_Type__c = '+auth.Authorization_Type__c);
       system.debug('++++auth.Process_type__c = '+auth.Process_type__c );              
            // 7-15-16 VV added for Permit auto attach once the status becomes Issued and Type = permit
                if (auth.Process_type__c != 'Fast Track' && auth.Authorization_Type__c == 'Permit' && auth.status__c == 'In Review' && auth.Is_violator__c == 'No' && auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c != null){
                   system.debug('+++starting to attach @@@ 1 ');
                   FutureAttachmentCreator.sendVF(string.valueOf(userinfo.getSessionId()), auth.application__c, auth.Id, auth.Authorization_Type__c);
                }
                //8-24-15 VV added for Cites Permit of Transit
                list <AC__c> lineitem = [select id, movement_type__c from Ac__c where application_number__c=:auth.application__c limit 1];
                 
                //8/29/16 - Dinesh - handling null values from above list
                if(!lineitem.isEmpty()){
                if (lineitem[0].movement_type__c == 'Transit' &&  auth.Process_type__c == 'Fast Track'&& auth.status__c == 'Issued' ){
                   system.debug('+++starting to attach @@@ 99 ');
                   FutureAttachmentCreator.sendVF(string.valueOf(userinfo.getSessionId()), auth.application__c, auth.Id, 'Permit');
                } 
                }
           }
    } 
    if (Trigger.isUpdate) {
       system.debug ('&&&& Before checkrecursive---'+checkRecursive.runOnce());
            // 7-13-16 VV added for Permit auto attach once the status becomes Issued and Type = permit
            for (Authorizations__c auth1: trigger.new) {
                if (auth1.Process_type__c != 'Fast Track' && auth1.Authorization_Type__c == 'Permit' && auth1.status__c == 'Issued'){
                   system.debug('+++starting to attach @@@ 2 ');
                   FutureAttachmentCreator.sendVF(string.valueOf(userinfo.getSessionId()), auth1.application__c, auth1.Id, auth1.Authorization_Type__c);
                } 
              }

        if(checkRecursive.runOnce()){
            system.debug ('&&&& after checkrecursive');                          
            List<Authorizations__c> approvelist = new List<Authorizations__c>();
            List<Authorizations__c> approvedfacilitiesauths = new List<Authorizations__c>();
            List<Authorizations__c> deniedfacilitiesauths = new List<Authorizations__c>();
            ID parentdenyauthID;

            for (Authorizations__c auth: trigger.new) {
                if (auth.Process_type__c != 'Fast Track' && auth.Auth_Type_Hidden_Flag__c == 'ingredient') {
                    if (auth.Status__c == 'Approved' || auth.status__c == 'Issued') //===========================Approved Case==============
                    {
                        Authorizations__c parentauth = [SELECT ID, Name, Child_auth_count__c, Approved_child_auth_num__c FROM Authorizations__c WHERE ID = : auth.Parent_Authorization__c LIMIT 1];
                        Integer i = 1;
                        if (parentauth.Approved_child_auth_num__c == null) {
                            parentauth.Approved_child_auth_num__c = string.valueOf(i);
                        } else {
                            Integer j;
                            j = integer.valueOf(parentauth.Approved_child_auth_num__c);
                            j++;
                            parentauth.Approved_child_auth_num__c = string.valueOf(j);
                            if (integer.valueOf(parentauth.Child_auth_count__c) == integer.valueOf(parentauth.Approved_child_auth_num__c)) {
                                parentauth.Email_AP_reviewer__c = true;
                            }
                        }
            
            
                        approvelist.add(parentauth);
                    }
            
                    if (auth.status__c == 'Denied') //===========================Denied Case==============
                    {
                        parentdenyauthID = auth.Parent_Authorization__c;
            
                    }
            
                }
            
                if (auth.RecordTypeName__c.contains('Facilities') && auth.status__c.contains('Approved')) {
                    approvedfacilitiesauths.add(auth);
                }
            
                if (auth.Auth_Type_Hidden_Flag__c != null && auth.Auth_Type_Hidden_Flag__c.contains('Parent Facility') && auth.status__c.contains('Denied')) {
                    deniedfacilitiesauths.add(auth);
                    parentdenyauthID = auth.ID;
                }
            } // ------------end of for loop
        
      
            if(approvelist!=null)
                {
                    update approvelist;
                }
            
            //this code works for both facilities/fastrack/ingredents   
            if (parentdenyauthID != null) {
                List < Authorizations__c > AuthListTodeny = new List < Authorizations__c > ();
                //get the auths of the parent and children then check if there are grandchildren
                Map < ID, Authorizations__c > denyList = new Map < ID, Authorizations__c > ([SELECT ID FROM Authorizations__c WHERE ID = : parentdenyauthID OR Parent_Authorization__c = : parentdenyauthID]);
                List < ID > children = new List < ID > ();
                for (ID c: denyList.keyset()) {
                    if (c != parentdenyauthID) {
                        children.add(c);
                    }
                }
                List < Authorizations__c > grandchildren = [SELECT ID FROM Authorizations__c WHERE Parent_Authorization__c = : children];
                if (grandchildren.size() > 0) {
                    for (Authorizations__c g: grandchildren) {
                        denyList.put(g.Id, g);
                    }
                }
              
                //deny everybody in the denial list
                for (ID authId: denyList.keyset()) {
                    Authorizations__c auth = denyList.get(authId);
                    auth.Status__c = 'Denied';
                    auth.Authorization_Type__c = 'Letter of Denial';
                    AuthListTodeny.add(auth);
                }
            
                update AuthListTodeny;
            }
        
            if(approvedfacilitiesauths!=null){
                
                CARPOL_UNI_Facilities facauth = new CARPOL_UNI_Facilities();
                for(Authorizations__c auth:approvedfacilitiesauths)
                {
                    facauth.CreateFacilities(auth);
                    
                }
            }
        //duplicate of denied code above really but could work on a list of denied facilities if rewritten
        /*if(deniedfacilitiesauths.size() > 0){
            List<Authorizations__c> AuthListTodeny = new  List<Authorizations__c>();
            List<Authorizations__c> deniallist = [SELECT ID FROM Authorizations__c WHERE ID =: parentdenyauthID OR Parent_Authorization__c=:parentdenyauthID];
            for(Authorizations__c auth2 :deniallist)
                {
                   auth2.Status__c = 'Denied';
                   AuthListTodeny.add(auth2);
                }
                
            update AuthListTodeny;
        }*/
           /*  CARPOL_UNI_SendIssuedAttachment runClass =  new CARPOL_UNI_SendIssuedAttachment(Trigger.newMap);
              runClass.sendIssuedAttachment(); */
              
     
    }
    
              CARPOL_UNI_SendIssuedAttachment runClass =  new CARPOL_UNI_SendIssuedAttachment(Trigger.newMap);
              runClass.sendIssuedAttachment(); 
}
    if (Trigger.isDelete) {
      // Call class logic here!
    }
  }
}