trigger CARPOL_UNI_Inspection_trigger on Inspection__c (before insert, before update, after insert, after update) {
     if(trigger.IsBefore){
        if(trigger.IsInsert){
            for(Inspection__c ins:trigger.new){
                 if(ins.Inspector__c!=null){
                     Contact con = [SELECT ID,NAme,Email FROM Contact WHERE ID=:ins.Inspector__c LIMIT 1];
                     ins.inspector_email__c=con.email;
                    
                 }
                 
                   system.debug('<!!! ins.ID'+ins.ID);
                     system.debug('<!!! ins name'+ins.name);
            }
            
          
        }
         
    
        if(trigger.IsUpdate){
            for(Inspection__c ins:trigger.new){
                 if(ins.Inspector__c!=null){
                    Contact con = [SELECT ID,NAme,Email FROM Contact WHERE ID=:ins.Inspector__c LIMIT 1];
                     ins.inspector_email__c=con.email;
                    
                 }
            }
        }
        
    }
   
}