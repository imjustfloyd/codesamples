trigger CARPOL_Getlineitem_FieldAPINameTrigger on Wizard_Questionnaire__c (before insert,before update) {
    
     Map <String, String> labelToAPIName = new Map <String, String> ();
     List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();  
     Map<String,String> objName = new Map<String,String>();
     Map<String, Schema.SObjectField> fieldsMap =  new Map<String,Schema.SObjectField>(); 

     for(Schema.SObjectType f : gd)
    {
      if(f.getDescribe().isCustom())
      {
        objName.put(f.getDescribe().getLabel(),f.getDescribe().getName()); 
      }
        
    }
    fieldsMap = Schema.SObjectType.AC__c.fields.getMap();
    
    for (Schema.SObjectField field : fieldsMap.values())
    {
    labelToAPIName.put(field.getDescribe().getLabel(), field.getDescribe().getName());
     
    }
    String Name = trigger.new[0].Mapping_Line_Item_Field_label__c;
    String APINAME = labelToAPIName.get(Name);
    trigger.new[0].Mapping_Field_Api_Name__c = APINAME;    

}