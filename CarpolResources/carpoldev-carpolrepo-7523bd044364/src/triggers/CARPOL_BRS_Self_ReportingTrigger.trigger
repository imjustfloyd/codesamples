trigger CARPOL_BRS_Self_ReportingTrigger on Self_Reporting__c (after update, before insert) {
/*  ====================================================  */
/*  Name: CARPOL_BRS_Self_ReportingTrigger                */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: Change History                               */   
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Kishore 06/11/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */

	CARPOL_UNI_DisableTrigger__c dt  = CARPOL_UNI_DisableTrigger__c.getInstance('CARPOL_BRS_Self_ReportingTrigger');
 	if(!dt.disable__c){ 
        if(Trigger.isAfter && Trigger.isUpdate) {
            GenericHistoryClass.CreateHistoryRecord(trigger.new,trigger.oldmap,'Self_Reporting__c');
        }
        
        if(Trigger.isBefore && Trigger.isInsert) {
            CheckForSelfReportingForInsert.ThrowErrorWhenConditionsMet(Trigger.new);
        }
    }
}