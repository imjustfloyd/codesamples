<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom history logging used for any object.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Application__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Application</description>
        <externalId>false</externalId>
        <label>Application</label>
        <referenceTo>Application__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Authorization__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Authorization</label>
        <referenceTo>Authorizations__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Construct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Construct</description>
        <externalId>false</externalId>
        <label>Construct</label>
        <referenceTo>Construct__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Design_Protocol_Record__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Design Protocol Record</description>
        <externalId>false</externalId>
        <label>Design Protocol Record</label>
        <referenceTo>Design_Protocol_Record__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Field_API_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Salesforce apiName</inlineHelpText>
        <label>Field API Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Field__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Name of the field changed.</inlineHelpText>
        <label>Field</label>
        <length>75</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Genotype_Phenotype__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Genotype/Phenotype</description>
        <externalId>false</externalId>
        <label>Genotype/Phenotype</label>
        <referenceTo>Genotype__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Line Item</description>
        <externalId>false</externalId>
        <label>Line Item</label>
        <referenceTo>AC__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Link_Regulated_Articles__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Link Regulated Articles</label>
        <referenceTo>Link_Regulated_Articles__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Location</description>
        <externalId>false</externalId>
        <label>Location</label>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>New_Value__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>The new value after the change was saved.</inlineHelpText>
        <label>New Value</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>14</visibleLines>
    </fields>
    <fields>
        <fullName>Object_ID__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Salesforce ID of the object that contains the field that was changed.</inlineHelpText>
        <label>Object ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Object_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Name of object that contains the field that was changed.</inlineHelpText>
        <label>Object Name</label>
        <length>75</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Official_Review_Record__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Official Review Record</description>
        <externalId>false</externalId>
        <label>Official Review Record</label>
        <referenceTo>Reviewer__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Old_Value__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Original value of the field prior to saving the change.</inlineHelpText>
        <label>Old Value</label>
        <length>131072</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>14</visibleLines>
    </fields>
    <fields>
        <fullName>Phenotype__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Phenotype</description>
        <externalId>false</externalId>
        <label>Phenotype</label>
        <referenceTo>Phenotype__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Record_ID__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Salesforce record ID that was changed.</inlineHelpText>
        <label>Record ID</label>
        <length>18</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Self_Reporting__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Self Reporting</description>
        <externalId>false</externalId>
        <label>Self Reporting</label>
        <referenceTo>Self_Reporting__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Supplemental_Condition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Supplemental Condition</label>
        <referenceTo>Application_Condition__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Thumbprint__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Thumbprint</description>
        <externalId>false</externalId>
        <label>Thumbprint</label>
        <referenceTo>Signature__c</referenceTo>
        <relationshipLabel>Change History</relationshipLabel>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Change_History</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Change History</label>
    <nameField>
        <label>Change History ID</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Change History</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
</CustomObject>
