<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object gives the many to many relationship between Regulations and Groups; Ports and Groups.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Read</externalSharingModel>
    <fields>
        <fullName>Facility__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field gives the related facility.</description>
        <externalId>false</externalId>
        <label>Facility</label>
        <referenceTo>Facility__c</referenceTo>
        <relationshipLabel>Group Junction</relationshipLabel>
        <relationshipName>Group_Junction_Facility</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Port_Description__c</fullName>
        <description>This field gives the Port Description.</description>
        <externalId>false</externalId>
        <formula>Facility__r.Description__c</formula>
        <label>Port Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Port_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field refers to Port Group to be mapped with various Ports.</description>
        <externalId>false</externalId>
        <label>Port Group</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Group__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Ports</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Group__c</referenceTo>
        <relationshipLabel>Group Junction (Port Group)</relationshipLabel>
        <relationshipName>Group_Junction_Ports</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Regulation_Custom_Name__c</fullName>
        <description>This field gives ustom name of Regulation attached to a specific group.</description>
        <externalId>false</externalId>
        <formula>Regulation__r.Custom_Name__c</formula>
        <label>Regulation Custom Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Regulation_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field relates to a regulation group to which a regulation has to be attached.</description>
        <externalId>false</externalId>
        <label>Regulation Group</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Group__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Regulations</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Group__c</referenceTo>
        <relationshipLabel>Group Junction (Regulation Group)</relationshipLabel>
        <relationshipName>Group_Junction</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Regulation_Short_Name__c</fullName>
        <description>This field gives short name of Regulation attached to a specific group.</description>
        <externalId>false</externalId>
        <formula>Regulation__r.Short_Name__c</formula>
        <label>Regulation Short Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Regulation_Type__c</fullName>
        <description>This field gives the type of Regulation attached to a specific group.</description>
        <externalId>false</externalId>
        <formula>TEXT(Regulation__r.Type__c)</formula>
        <label>Regulation Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Regulation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Regulation</label>
        <referenceTo>Regulation__c</referenceTo>
        <relationshipLabel>Group Junction</relationshipLabel>
        <relationshipName>Group_Junction_Regulation</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Signature_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>This field refers to Group to be mapped with various Signatures.</description>
        <externalId>false</externalId>
        <label>Signature Group</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Group__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Signature</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Group__c</referenceTo>
        <relationshipLabel>Group Junction</relationshipLabel>
        <relationshipName>Group_Signature</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Group Junction</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Signature_Group__c</columns>
        <columns>Port_Group__c</columns>
        <columns>Regulation_Group__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All_Ports_Group_Junction</fullName>
        <columns>NAME</columns>
        <columns>Port_Group__c</columns>
        <columns>Port_Description__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Group_Junction__c.Ports_RT</value>
        </filters>
        <label>All Ports Group Junction</label>
    </listViews>
    <listViews>
        <fullName>All_Regulations_Group_Junction</fullName>
        <columns>NAME</columns>
        <columns>Regulation__c</columns>
        <columns>Regulation_Custom_Name__c</columns>
        <columns>Regulation_Short_Name__c</columns>
        <columns>Regulation_Group__c</columns>
        <columns>Regulation_Type__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Group_Junction__c.RegulationsRT</value>
        </filters>
        <label>All Regulations Group Junction</label>
    </listViews>
    <nameField>
        <displayFormat>GJ-{0000000000}</displayFormat>
        <label>Group Junction Number</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Group Junction</pluralLabel>
    <recordTypeTrackFeedHistory>false</recordTypeTrackFeedHistory>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>Ports_RT</fullName>
        <active>true</active>
        <description>This type gives many to many relationship between Groups and Ports</description>
        <label>Ports</label>
    </recordTypes>
    <recordTypes>
        <fullName>RegulationsRT</fullName>
        <active>true</active>
        <description>This type gives many to many relationship between Groups and Regulations.</description>
        <label>Regulations</label>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Signature_Group__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Signature_Group__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Signature_Group__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Signature_Group__c</searchFilterFields>
        <searchResultsAdditionalFields>Signature_Group__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Read</sharingModel>
    <validationRules>
        <fullName>CARPOL_OneGroupAssociationRequired</fullName>
        <active>true</active>
        <description>This validation rule is to check if one of the Group field is filled.</description>
        <errorConditionFormula>(Signature_Group__c == null &amp;&amp; Port_Group__c == null &amp;&amp;  Regulation_Group__c == null)</errorConditionFormula>
        <errorMessage>Please select at least one Group field.</errorMessage>
    </validationRules>
</CustomObject>
