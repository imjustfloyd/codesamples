<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>CARPOL_UNI_ViewLinkRegArt</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Link Regulated Articles to Application and Authorization</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fieldSets>
        <fullName>TrackedFields</fullName>
        <description>Tracked Fields</description>
        <displayedFields>
            <field>Regulated_Article_Text__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Regulated_Article__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Common_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Scientific_Name__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <displayedFields>
            <field>Cultivar_and_or_Breeding_Line__c</field>
            <isFieldManaged>false</isFieldManaged>
            <isRequired>false</isRequired>
        </displayedFields>
        <label>TrackedFields</label>
    </fieldSets>
    <fields>
        <fullName>Application__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Application</description>
        <externalId>false</externalId>
        <label>Application</label>
        <referenceTo>Application__c</referenceTo>
        <relationshipName>Link_Regulated_Articles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Authorization__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Authorization</description>
        <externalId>false</externalId>
        <label>Authorization</label>
        <referenceTo>Authorizations__c</referenceTo>
        <relationshipName>Link_Regulated_Articles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Common_Name__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Regulated_Article_CBI__c==true,&apos;[&apos;+ Regulated_Article__r.Common_Name__c +&apos;]&apos;, Regulated_Article__r.Common_Name__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Common Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cultivar_and_or_Breeding_Line__c</fullName>
        <description>Cultivar and/or Breeding Line</description>
        <externalId>false</externalId>
        <inlineHelpText>Cultivar and/or Breeding Line for regulated article</inlineHelpText>
        <label>Cultivar and/or Breeding Line</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Line Item</label>
        <referenceTo>AC__c</referenceTo>
        <relationshipName>Link_Regulated_Articles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Regulated_Article_CBI__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Regulated Article CBI</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Regulated_Article_Text__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Regulated_Article_CBI__c==true, &apos;[&apos;+  Regulated_Article__r.Name  +&apos;]&apos;,  Regulated_Article__r.Name)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Regulated Article</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Regulated_Article__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Regulated Article</description>
        <externalId>false</externalId>
        <label>Regulated Article</label>
        <lookupFilter>
            <active>false</active>
            <filterItems>
                <field>Regulated_Article__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Biotechnology Regulatory Services (BRS)</value>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Regulated_Article__c</referenceTo>
        <relationshipLabel>Link Regulated Articles</relationshipLabel>
        <relationshipName>Link_Regulated_Articles</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Scientific_Name__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Regulated_Article_CBI__c==true,&apos;[&apos;+ Regulated_Article__r.Scientific_Name__c +&apos;]&apos;, Regulated_Article__r.Scientific_Name__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Scientific Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Link Regulated Articles</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Regulated_Article__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>APRA-{000000000}</displayFormat>
        <label>Link Regulated Articles Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Link Regulated Articles</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <webLinks>
        <fullName>New_UI</fullName>
        <availability>online</availability>
        <description>This Button is used by External Users.</description>
        <displayType>massActionButton</displayType>
        <encodingKey>UTF-8</encodingKey>
        <height>600</height>
        <linkType>url</linkType>
        <masterLabel>New</masterLabel>
        <openType>sidebar</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/apex/CARPOL_UNI_AddLinkRegArt?LineItemId={!AC__c.Id}&amp;retURL=/{!AC__c.Id}</url>
    </webLinks>
</CustomObject>
