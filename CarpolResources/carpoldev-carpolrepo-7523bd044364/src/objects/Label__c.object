<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object will house records related to shipment labels</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>Private</externalSharingModel>
    <fields>
        <fullName>Applicant_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Authorization__r.AC_Applicant_Name__c</formula>
        <label>Applicant Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Authorization__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Notification Number</label>
        <referenceTo>Authorizations__c</referenceTo>
        <relationshipLabel>Labels</relationshipLabel>
        <relationshipName>Labels</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>City__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.City__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>City</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Destination_Address__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Destination Address</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Location__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Destination Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Labels</relationshipLabel>
        <relationshipName>Labels</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Destination_County__c</fullName>
        <externalId>false</externalId>
        <formula>Destination_Address__r.County__c</formula>
        <label>County</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Destination_State__c</fullName>
        <externalId>false</externalId>
        <formula>Destination_Address__r.Contact_state__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>State</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Label_Number__c</fullName>
        <externalId>false</externalId>
        <label>Label Number</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Line Item</description>
        <externalId>false</externalId>
        <label>Line Item</label>
        <referenceTo>AC__c</referenceTo>
        <relationshipLabel>Labels</relationshipLabel>
        <relationshipName>Labels</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Notification_Expiration_Date__c</fullName>
        <externalId>false</externalId>
        <label>Notification Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Notification_Issue_Date__c</fullName>
        <externalId>false</externalId>
        <formula>Authorization__r.Date_Issued__c</formula>
        <label>Notification Issue Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Notification_Status__c</fullName>
        <externalId>false</externalId>
        <label>Notification Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Inactive</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Organization__c</fullName>
        <externalId>false</externalId>
        <formula>Authorization__r.Applicant_Organization__c</formula>
        <label>Organization</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Origin_Address__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Origin Name</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Location__c.RecordTypeId</field>
                <operation>equals</operation>
                <value>Origin Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Labels (Origin Address)</relationshipLabel>
        <relationshipName>Labels1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Origin_County__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.County__c</formula>
        <label>County</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Origin_State__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.State__r.Name</formula>
        <label>State</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Plant_Inspection_Station__c</fullName>
        <externalId>false</externalId>
        <formula>Authorization__r.Plant_Inspection_Station__r.Address_1__c +&apos;, &apos;+ Authorization__r.Plant_Inspection_Station__r.Address_2__c +&apos;,  &apos;+Authorization__r.Plant_Inspection_Station__r.City__c +&apos;,  &apos;+ Authorization__r.Plant_Inspection_Station__r.State_LV1__r.Name +&apos;,  &apos;+ Authorization__r.Plant_Inspection_Station__r.Zip__c +&apos;,  &apos;+
Authorization__r.Plant_Inspection_Station__r.Name</formula>
        <label>Plant Inspection Station</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ports_of_Arrival__c</fullName>
        <description>Port of Arrival information from related Authorization</description>
        <externalId>false</externalId>
        <formula>Authorization__r.Port_of_Entry_For_Importation_Only_del__c</formula>
        <label>Port of Arrival</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>QRcode_Barcode__c</fullName>
        <externalId>false</externalId>
        <formula>IMAGE(&apos;https://chart.googleapis.com/chart?chs=67x67&amp;cht=qr&amp;chl=&apos;+Id, &apos;No QR code available&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>QR code/Barcode</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <description>Label Status</description>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Active</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Voided-Expired</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Used</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Street_Address1__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.Street_Add1__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Street Address1</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street_Address2__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.Street_Add2__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Street Address2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Street_Address3__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.Street_Add3__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Street Address3</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Used__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Used</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Workflow_Setting__c</fullName>
        <externalId>false</externalId>
        <label>Workflow Setting</label>
        <picklist>
            <picklistValues>
                <fullName>Setting 1</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Setting 2</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Zipcode__c</fullName>
        <externalId>false</externalId>
        <formula>Origin_Address__r.Zip__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Zipcode</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Label</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Label Number</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Labels</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>BRS_Shipping_Labels</fullName>
        <active>true</active>
        <description>Shipping Labels Associated with BRS</description>
        <label>BRS Shipping Labels</label>
        <picklistValues>
            <picklist>Notification_Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Inactive</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Status__c</picklist>
            <values>
                <fullName>Active</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Used</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Voided-Expired</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Workflow_Setting__c</picklist>
            <values>
                <fullName>Setting 1</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>Setting 2</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <webLinks>
        <fullName>Additional_Labels</fullName>
        <availability>online</availability>
        <description>Additional Labels</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Additional Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/24.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/24.0/apex.js&quot;)} 
if((&quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos; ||  
&quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos;) &amp;&amp; &quot;{!Authorizations__c.BRS_Introduction_Type__c}&quot;==&apos;Import&apos;) 
{ 

function isNumeric(n) {
return !isNaN(parseFloat(n)) &amp;&amp; isFinite(n);
} 
var quantity = prompt(&apos;How many labels do you want to create?&apos;, &apos;&apos;);
if (!isNumeric(quantity)) { // check is it number
quantity=null;
alert(&quot;Please enter a number from 0 to 999&quot;);
} else { 
quantity=Math.floor(+quantity); //to number and round to floor
if (quantity&lt;=0||quantity&gt;999) { //check is it less than 1 or more than 999
quantity=null;
alert(&quot;Please enter a number from 1 to 999&quot;);
}
} 
if(quantity!=null){
// creating additional labels
var lstlabs = [];
var result = sforce.connection.create(lstlabs);
  var App = new sforce.SObject(&quot;Authorizations__c&quot;); 
	App.Id = &apos;{!Authorizations__c.Id}&apos;; 
	App.BRS_Send_Labels__c = false; 
	var strquery = &quot;select id,Name from Label__c where Authorization__c=&apos;&quot;+&quot;{!Authorizations__c.Id}&quot;+&quot;&apos;&quot;;
	var records = sforce.connection.query(strquery);
	var labcount = records.size;

var authexpdt = new Date({!Authorizations__c.Expiry_Date__c});
/*var authexpdt = authexpdt_dt.getDate() +&apos;/&apos; + (authexpdt_dt.getMonth()+1) +&apos;/&apos;+ authexpdt_dt.getFullYear();

var authexpdt =  new Date({!YEAR(Authorizations__c.Expiry_Date__c)}+ {!MONTH(Authorizations__c.Expiry_Date__c)-1}+ {!DAY(Authorizations__c.Expiry_Date__c)});*/

 labcount++;
  for (var i=0; i&lt;quantity; i++) {
    var l = new sforce.SObject(&quot;Label__c&quot;);
     l.Name = &quot;{!Authorizations__c.Name}&quot;+&quot;-&quot;+ labcount++;
     l.Authorization__c = &quot;{!Authorizations__c.Id}&quot;;
     l.Notification_Expiration_Date__c = authexpdt;
     l.Notification_Status__c = &quot;Active&quot;;
     l.Status__c = &quot;Active&quot;;	 
    lstlabs.push(l);
  }   
	
	var result = sforce.connection.create(lstlabs);
  
	if (result[0].success == &apos;false&apos;) { 
	alert(result[0].errors.message); 
	} 
	else { 
	alert(&apos;Labels are Created.&apos;); 
        var result2 = sforce.connection.update([App]); 
	location.reload(true); /* redisplay the detail page */ 
	} 
} 
} 
else{
 alert(&apos;Only for approved and Importation Authorization can create labels&apos;)
}</url>
    </webLinks>
    <webLinks>
        <fullName>Create_Labels</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)} 

if(&quot;{!Authorizations__c.BRS_Create_Labels__c}&quot; ==false &amp;&amp; (
  &quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos; ||
   &quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos; )&amp;&amp; 
  &quot;{!Authorizations__c.BRS_Introduction_Type__c}&quot;==&apos;Import&apos;) 
{ 
var App = new sforce.SObject(&quot;Authorizations__c&quot;); 
App.Id = &apos;{!Authorizations__c.Id}&apos;; 
App.BRS_Create_Labels__c = true; 
var result = sforce.connection.update([App]); 
if (result[0].success == &apos;false&apos;) { 
alert(result[0].errors.message); 
} 
else { 
alert(&apos;Labels are Created.&apos;); 
location.reload(true); /* redisplay the detail page */ 
} 

} 
else if(&quot;{!Authorizations__c.BRS_Create_Labels__c}&quot; ==true){ 
alert(&apos;Labels are already created.&apos;); 
} 
else{ 
alert(&apos;Labels can be created only for Authorizations with a status of Approved/Issued and Introduction type of Import&apos;); 
}</url>
    </webLinks>
    <webLinks>
        <fullName>Create_PPQ_Labels</fullName>
        <availability>online</availability>
        <description>PPQ Labels</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Create PPQ Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)} 

if(&quot;{!Authorizations__c.BRS_Create_Labels__c}&quot; ==false &amp;&amp; (&quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos; || &quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos;)) 
{ 
var App = new sforce.SObject(&quot;Authorizations__c&quot;); 
App.Id = &apos;{!Authorizations__c.Id}&apos;; 
App.BRS_Create_Labels__c = true;
var result = sforce.connection.update([App]); 
if (result[0].success == &apos;false&apos;) { 
alert(result[0].errors.message); 
} 
else { 
alert(&apos;Labels are Created.&apos;); 
location.reload(true); /* redisplay the detail page */ 
} 

} 
else if(&quot;{!Authorizations__c.BRS_Create_Labels__c}&quot; ==true){ 
alert(&apos;Labels are already created.&apos;); 
}
else{
alert(&apos;For only approved or issued records labels can be created&apos;);
}</url>
    </webLinks>
    <webLinks>
        <fullName>Generate_and_Send_Labels</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Generate PDF and Send Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>//{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)} 
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)}
if(&quot;{!Authorizations__c.BRS_Send_Labels__c}&quot; ==false) 
{ 
var App = new sforce.SObject(&quot;Authorizations__c&quot;); 
App.Id = &apos;{!Authorizations__c.Id}&apos;; 
App.BRS_Send_Labels__c= true; 
//var result = sforce.connection.update([App]); 
var result = sforce.apex.execute(&quot;CARPOL_BRS_Approval&quot;,&quot;brslabelspdf&quot;, {authid:&quot;{!Authorizations__c.Id}&quot;});
//alert(result);
//alert(result[0].success);
if (result[0].success == &apos;false&apos;) { 
alert(result[0].errors.message); 
} 
else { 
alert(&apos;PDF is Generated and sent to applicant.&apos;); 
location.reload(true); /* redisplay the detail page */ 
} 

} 
else{ 
alert(&apos;Labels are already Sent.&apos;); 
}</url>
    </webLinks>
    <webLinks>
        <fullName>PPQ_Additional_Labels</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>PPQ Additional Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/28.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/28.0/apex.js&quot;)}

if(&quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos; || &quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos;)
{

function isNumeric(n) {
return !isNaN(parseFloat(n)) &amp;&amp; isFinite(n);
}


var quantity = prompt(&apos;How many labels do you want to create?&apos;, &apos;&apos;);
if (!isNumeric(quantity)) { // check is it number
quantity=null;
alert(&quot;Please enter a number from 0 to 999&quot;);
} else {
quantity=Math.floor(+quantity); //to number and round to floor
if (quantity&lt;=0||quantity&gt;999) { //check is it less than 1 or more than 999
quantity=null;
alert(&quot;Please enter a number from 1 to 999&quot;);
}
}

if(quantity!=null){
// creating additional labels
var lstlabs = [];
var result = sforce.connection.create(lstlabs);
var App = new sforce.SObject(&quot;Authorizations__c&quot;);
App.Id = &apos;{!Authorizations__c.Id}&apos;;
App.BRS_Send_Labels__c = false;
var strquery = &quot;select id,Name from Label__c where Authorization__c=&apos;&quot;+&quot;{!Authorizations__c.Id}&quot;+&quot;&apos;&quot;;
var records = sforce.connection.query(strquery);
var labcount = records.size;

var authexpdt = new Date(&quot;{!YEAR(Authorizations__c.Expiry_Date__c)}&quot;, &quot;{!MONTH(Authorizations__c.Expiry_Date__c)-1}&quot;, &quot;{!DAY(Authorizations__c.Expiry_Date__c)}&quot;);
labcount++;
for (var i=0; i&lt;quantity; i++) {
var l = new sforce.SObject(&quot;Label__c&quot;);
l.Name = &quot;{!Authorizations__c.Name}&quot;+&quot;-&quot;+ labcount++;
l.Authorization__c = &quot;{!Authorizations__c.Id}&quot;;
l.Notification_Expiration_Date__c = authexpdt;
l.Notification_Status__c = &quot;Active&quot;;
l.Status__c = &quot;Active&quot;;
lstlabs.push(l);
}

var result = sforce.connection.create(lstlabs);

if (result[0].success == &apos;false&apos;) {
alert(result[0].errors.message);
}
else {
alert(&apos;Labels are Created.&apos;);
var result2 = sforce.connection.update([App]);
location.reload(true); /* redisplay the detail page */
}


}


}
else{
alert(&apos;Only Authorizations that are Approved or Issued can create labels&apos;)
}</url>
    </webLinks>
    <webLinks>
        <fullName>PPQ_Generate_PDF_and_Send_Labels</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>PPQ Generate PDF and Send Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>//{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/apex.js&quot;)}

var isHandCarry = {!Authorizations__c.BRS_Hand_Carry_For_Importation_Only__c};
CASE(isHandCarry, &quot;Yes&quot; , (if(&quot;{!Authorizations__c.BRS_Send_Labels__c}&quot; ==false )
{
var App = new sforce.SObject(&quot;Authorizations__c&quot;);
App.Id = &apos;{!Authorizations__c.Id}&apos;;
App.BRS_Send_Labels__c= true;
//var result = sforce.connection.update([App]);
var result = sforce.apex.execute(&quot;CARPOL_PPQ_sendlabels&quot;,&quot;brslabelspdf&quot;, {authid:&quot;{!Authorizations__c.Id}&quot;});
//alert(result);
//alert(result[0].success);
if (result[0].success == &apos;false&apos;) {
alert(result[0].errors.message);
}
else {
alert(&apos;PDF is Generated and sent to applicant.&apos;);
location.reload(true); /* redisplay the detail page */
}

}
else{
alert(&apos;Labels are already Sent.&apos;);
}), &quot;No&quot;, (alert(result[0].errors.message)))</url>
    </webLinks>
    <webLinks>
        <fullName>PPQ_Void_Labels</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>PPQ Void Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)}
if(&quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos; || &quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos;)
{
var strquery = &quot;select id,Name,Status__c from Label__c where Authorization__c=&apos;&quot;+&quot;{!Authorizations__c.Id}&quot;+&quot;&apos;&quot;+&quot;and Status__c = &apos;Active&apos;&quot;;
var labels = sforce.connection.query(strquery);
var records = labels.getArray(&quot;records&quot;);
var lstlabs = [];
if(records.length &gt;0) {

for (var i=0; i&lt; records.length; i++) {
var record = records[i];
record.Status__c = &apos;Voided-Expired&apos;;
lstlabs.push(record);
}

var result = sforce.connection.update(lstlabs);

if (result[0].success == &apos;false&apos;) {
alert(result[0].errors.message);
}
else {
alert(&apos;Labels are Voided.&apos;);
location.reload(true); /* redisplay the detail page */
}
}
else{
alert(&apos;No Active Labels to Void&apos;);
}
}
else{
alert(&apos;Only for approved Authorization can Void Active labels&apos;)
}</url>
    </webLinks>
    <webLinks>
        <fullName>Void_Labels</fullName>
        <availability>online</availability>
        <description>Analysts void of all related unused label records.</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Void Labels</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/22.0/connection.js&quot;)} 
if((&quot;{!Authorizations__c.Status__c}&quot;==&apos;Issued&apos; || &quot;{!Authorizations__c.Status__c}&quot;==&apos;Approved&apos; ) &amp;&amp; &quot;{!Authorizations__c.BRS_Introduction_Type__c
}&quot;==&apos;Import&apos;) 
{
  var strquery = &quot;select id,Name,Status__c from Label__c where Authorization__c=&apos;&quot;+&quot;{!Authorizations__c.Id}&quot;+&quot;&apos;&quot;+&quot;and Status__c = &apos;Active&apos;&quot;; 
  var labels = sforce.connection.query(strquery);
  var records = labels.getArray(&quot;records&quot;); 
  var lstlabs = []; 
 if(records.length &gt;0) { 
 
  for (var i=0; i&lt; records.length; i++) {
    var record = records[i];
    record.Status__c = &apos;Voided-Expired&apos;;
    lstlabs.push(record); 
  }
 
  var result = sforce.connection.update(lstlabs); 
  
  if (result[0].success == &apos;false&apos;) { 
   alert(result[0].errors.message); 
  } 
  else { 
     alert(&apos;Labels are Voided.&apos;); 
     location.reload(true); /* redisplay the detail page */ 
    }    
  }
  else{
     alert(&apos;No Active Labels to Void&apos;);
    }
}
else{ 
alert(&apos;Only for approved and Importation Authorization can Void Active labels&apos;) 
}</url>
    </webLinks>
</CustomObject>
