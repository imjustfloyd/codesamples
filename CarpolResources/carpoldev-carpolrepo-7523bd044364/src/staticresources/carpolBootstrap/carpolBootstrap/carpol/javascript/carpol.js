$(document).ready(function(){
    var pagePath = top.location.pathname;
    
    //we get the path of the page so that we can run functions depending on the page that User is on. 
    var lineItemPagePath = "/carpol_lineitempageforapplicant" || "/apex/carpol_lineitempageforapplicant";
    //this is the id of the list item that we want to move the button inside of for the line item page for applicant page.  
    var lineItemButtonIdName="li#returnToLineItem";
    // the switch below is using the page path to determine which button to move into the sidebar. 
    var uniLinePagePath = "/carpol_uni_lineItem" || "/apex/carpol_uni_lineitem";
    var appDetailPagePath = "/portal_application_detail" || "/apex/portal_application_detail";
    var brokerRequestPath = "/apex/brokerPrepareForm" || "/brokerPrepareForm" ;
    var constructPagePath = "/portal_construct_detail" || "/apex/portal_construct_detail";
    var brsWaitingPagePath = "/carpol_brs_waitingpage" || "/apex/carpol_brs_waitingpage";
    var appAttachEditPagePath = "/portal_applicantattachment_edit" || "/apex/portal_applicantattachment_edit";
    var locationEditPath = "/portal_location_edit" || "/apex/portal_location_edit";
    var externalWizardPath = "/CARPOL_HOMEPAGE" || "/apex/CARPOL_HOMEPAGE";
    console.log("Your Current Page Path is: " + pagePath);
    
    
    
    switch (pagePath){
        case lineItemPagePath:
                moveButtonToActionModule(lineItemButtonIdName);
                console.log("Moving the Edit return to line item button to the action module in the sidebar");
            break;
        case uniLinePagePath:
                    testUniLineItem(); 
                    changeDocTitle(pagePath);
                    collapseIcon();
                    validateForm();;
                    addPickListClass();
            break;
        case appDetailPagePath: 
                    getRegulatedArticlePackageId();

            break;
        case brokerRequestPath: 
                    disableClick(".brokerRequest");
            break;
        case constructPagePath:
                    addSpacer(".constructPanel");
            break; 
        case brsWaitingPagePath: 
                    disableButton();
            break;
        case appAttachEditPagePath: 
                    removeBootstrapFromPicklist();
                    addTitleToPicklistOptions();
                    addPickListClass();
            break;
        case locationEditPath: 
                    removeBootstrapFromPicklist();
                    addTitleToPicklistOptions();
                    addPickListClass();
            break;
        case externalWizardPath:
                    removeSectionBorder('div#noSectionLine.form-section');
                    
            break;

    }
    try{
        disableTextBox();
    } catch(e){
        console.log(e);
        
    }
});

function removeSectionBorder(e){
    var sectionName = $(e);
    sectionName.attr('style', 'border:none;');
}

function validateForm(){
$('form .lineItemForm.requiredInput input').blur(function()
    {
        var reqField = $('em.required'); 
        var reqPanel = reqField.attr('id');
        if( $(this).val().length < 1 ) {
            validateRequiredPanels();
        }
    });
}

//if em has class required then validate form
function checkForRequiredFields(){
    var reqPanel = $('em.required');
    var vals = [];
    // get required panels from em element id. 
    $(reqPanel).each(function(){
        vals.push($(this).attr('id'));
    });
    //remove duplicates 
    var uniqueVals = vals.filter(function(elem, pos) {
        return vals.indexOf(elem) == pos;
    });
   
    var collapsePanel =('div.panel-collapse');
    var headerPanel = ('div.panel-heading');
    var panelTitle = $('div.panel-collapse').attr('title');
    var title = "";
    $.each( uniqueVals, function( key, value ) {
        title = value;
        if(title == panelTitle){
            $(headerPanel).attr('style','background-color:red;');
            $(collapsePanel).addClass('in');
        }
    });
}

/*function that retrieves panel header by title*/
function getErrPanel(e){
    var panel = $('div.panel-header');
    var panelTitle = panel.attr('title');
    //if panel title and the parameter return true then change the panel header background red. 
    if(panelTitle === e){
        panel.addClass('error');
        panel.attr('aria-expanded','true');
        console.log(panel.attr('class'));
    }
}


function collapseIcon(){
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
    }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
    });
    var liTags = $('.mainbody li');
    if(liTags.parent().is("ul.noindent")){
        liTags.unwrap();
    }else{
        liTags.wrap("");
    }
}

function closeMessageWindow(){
    $('.message.confirmM3').hide();
}

function disableTextBox(){
    $('.input-disabled').attr('readonly', true);
}

function changeDocTitle(e){
   $('title').html(e).replace(/\_/g, ' ');
   $('title').html(e).replace(/\//g, ' ');
   $("title").html(e);
}

function intializeToolTips(){
    //initialize tooltips when the document is in ready state . 
    $('[data-toggle="tooltip"]').tooltip(); 
}

function removeBootstrapFromPicklist(){
     //remove bootstrap form-control class from the picklist arrow images
    $('.picklistArrowRight').removeClass('form-control');
    $('.picklistArrowLeft').removeClass('form-control');
}

function  addTitleToPicklistOptions(){
     $(".multiSelectPicklistTable select option").each(function(){
        $(this).attr("title", $(this).text());
        $(this).attr("data-toggle", "tooltip");
        });
}
function disableButton(){
    $("input.btnDisabled").addClass('disableClick').removeClass('btnDisabled');
    $("input.disableClick").addClass('disable').removeClass('customButtonGreen');
}
function addPickListClass(){$('jQuery selector').css({"css property name":"css property value"});
    var bootstrapStyle = $("tr.multiSelectPicklistRow td select.form-control");
    $('select.form-control').attr('style', 'width:180px');
    bootstrapStyle.attr("style","width:180px !important;");;
    bootstrapStyle.addClass('ui-resizable-se');
    bootstrapStyle.addClass('pickListBox');
    
}
function removeBtnClass(){
        //This functionality removes the btn class so that we can fully customize the apex:commandButton directive. Since it adds the btn class when the document is ready.
    // we can reuse this in the bottom of the output panels when the body rerenders on proceed with application. 

    $("input.btn.customButton.customButtonGreen").removeClass('btn');
    $("input.btn.customButton.customButtonBlue").removeClass('btn');
}

function moveButtonToActionModule(e){
    $('span#showButton').appendTo('li#returnToLineItem').show();
}
function getRegulatedArticlePackageId(){
    var id = $("[title='regulated article package id']").attr('class');
    console.log(id);
    if(id !== null){
        setRegulatedArticlePackageId(id);
    }
}
function setRegulatedArticlePackageId(e){
    var newurl ="carpol_uni_lineitem?appid=a0535000000PoHxAAK&regArticlePackageId=" + e;
    $('.btnLink').attr('href', newurl);
}
function testUniLineItem()
{
 var applicationId = $("a.retapp").attr('id');
            if (applicationId !== null){
                console.log(applicationId + "is undefined");
            }
            
}
//This is a function that can be used to disable a <a href> rendering it read-only. 
function disableClick(e){
    //link should be the jquery selector . Please use either class or id. Ex $('.myClassName'); or $('#myClassName'); 
    var linkToDisable = $(e).addClass('disableClick');
    console.log(linkToDisable);
}

function addSpacer(e){
    var addSpaceTo = $(e).addClass('spacer');
}


