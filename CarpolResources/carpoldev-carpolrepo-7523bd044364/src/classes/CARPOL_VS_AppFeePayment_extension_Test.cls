@isTest(seealldata=false)
private class CARPOL_VS_AppFeePayment_extension_Test {
    
      @IsTest static void CARPOL_VS_AppFeePayment_extension_Test1() 
      {

          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Application__c objapp = testData.newapplication();
          AC__c lineItem1 = testData.newLineItem('Resale',objapp);
          AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
          Signature__c sigObj = testData.newThumbprint();
          lineItem1.Thumbprint__c = sigObj.Id;
          update lineItem1;
          
          Signature__c sigObj1 = testData.newThumbprint();
          lineItem2.Thumbprint__c = sigObj1.Id;
          update lineItem2;
          
          Test.startTest();
          try{
              PageReference pageRef = Page.CARPOL_VS_ApplicationFee_Payment;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('Id',objapp.id);
              ApexPages.currentPage().getParameters().put('token','Test Token');
              CARPOL_VS_AppFeePayment_extension extclass = new CARPOL_VS_AppFeePayment_extension(sc);
              extclass.application = objapp;
              
              //test for both with and without fees
              lineItem1.Total_Fees__c = 150.00;
              lineItem2.Total_Fees__c = 200.00;
              update lineItem1;
              update lineItem2;
              extclass.computePaymentSummary();
              system.assert(extclass != null);
              
              lineItem1.Total_Fees__c = 0.00;
              lineItem2.Total_Fees__c = 0.00;
              update lineItem1;
              update lineItem2;
              extclass.computePaymentSummary();
              system.assert(extclass != null);
          }catch(exception e){
          }
          Test.stopTest();
     
      }
      
      @IsTest static void CARPOL_VS_AppFeePayment_extension_Test2() 
      {
          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Application__c objapp = testData.newapplication();
          AC__c lineItem1 = testData.newLineItem('Resale',objapp);
          AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
          Test.startTest();
          try{
              PageReference pageRef = Page.CARPOL_VS_ApplicationFee_Payment;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('Id',objapp.id);
              ApexPages.currentPage().getParameters().put('token','Test Token');
              CARPOL_VS_AppFeePayment_extension extclass = new CARPOL_VS_AppFeePayment_extension(sc);
              extclass.application = objapp;
              
              //test w/ fees
              lineItem1.Total_Fees__c = 150.00;
              lineItem2.Total_Fees__c = 200.00;
              update lineItem1;
              update lineItem2;
              extclass.paymentType = 'Electronically (pay.gov)';
              extclass.processAuthorization();
    	      extclass.getPaymentTypeOptions();
              extclass.paymentTypeChangeAction();
              extclass.savePaymentType();
              extclass.processPayGov();
              
              extclass.paymentType = 'Mail-in Check/Money Order';
              extclass.processAuthorization();
    	      extclass.getPaymentTypeOptions();
              extclass.paymentTypeChangeAction();
              extclass.savePaymentType();
              extclass.processPayGov();
              
              extclass.paymentType = 'APHIS User Account';
              extclass.processAuthorization();
    	      extclass.getPaymentTypeOptions();
              extclass.paymentTypeChangeAction();
              extclass.savePaymentType();
              extclass.processPayGov();
              
              
              extclass.doCancel();
              system.assert(extclass != null);
          }catch(exception e){
          }
          Test.stopTest();
      }

      
}