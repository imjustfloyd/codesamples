public class CARPOL_AssociateProgramPathway {

  Map<Id, AC__c> newLine;
  String lineitemRecName;

  public CARPOL_AssociateProgramPathway(
     Map<Id, AC__c> newTriggerLines) {
       newLine = newTriggerLines;
  }
 
 
 public void CARPOL_AssociateProgramPathway()  {
   for(AC__c ac : newLine.values())
     {
     if (ac.Status__c == 'Saved')
         {
            lineitemRecName = Schema.SObjectType.AC__c.getRecordTypeInfosByID().get(ac.Recordtypeid).getName();
            
            if(lineitemRecName=='Biotechnology Regulatory Services - Courtesy Permit' || lineitemRecName=='Biotechnology Regulatory Services - Notification' || lineitemRecName=='Biotechnology Regulatory Services - Standard Permit') 
                {
                       system.debug('++lineitemRecName++'+lineitemRecName);
                       if(lineitemRecName != Null || lineitemRecName != ' ') 
                        {
                           Id pathwayId = [Select id from Program_Line_Item_Pathway__c where Record_Type__c =: lineitemRecName].Id ;
                           system.debug('++pathwayId++'+pathwayId);
                  
                           If (pathwayId != Null || pathwayId != ' ') 
                               {
                                   ac.Program_Line_Item_Pathway__c = pathwayId; 
                               }
                        }    
                  }
          }
                    system.debug('****************************');
                    }
              } 
         }