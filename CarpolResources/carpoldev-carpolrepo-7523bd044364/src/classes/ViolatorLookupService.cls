public class ViolatorLookupService {
    
    @future(callout = true)
    public static void lookupViolator(ID acID) 
        {       
                List<AC__C> lineItems = [SELECT ID, Name, Importer_First_Name__c, Importer_Last_Name__c FROM AC__c WHERE ID = :acID LIMIT 1];

                if(!lineItems.isEmpty()){
                    AC__c lineItem = lineItems[0];

                    //look up the user record            
                    Applicant_Contact__c lastNameRecord = [SELECT ID, Name, First_Name__c FROM Applicant_Contact__c WHERE ID = :lineItem.Importer_Last_Name__c LIMIT 1];
                    String firstName = lastNameRecord.First_Name__c;
                    String lastName = lastNameRecord.Name;
                
                    //make the request
                    String response = initiateCall(firstName, lastName);
                
                    //check to see if this returned any potential violators
                    if(containsResults(response)) 
                        {
                            lineItem.Is_violator__c = 'Yes';
                        }
                    else 
                        {
                            lineItem.Is_violator__c = 'No';
                        }
              
                    updateItems(lineItem);
                }
         }
    
    public static void updateItems(AC__C lineItemList) 
        {
            //checkRecursive.runOnce();
            update lineItemList;
        }
    
     public static String initiateCall(String firstName, String lastName) 
         {
            String url = 'https://itemsuat.aphis.edc.usda.gov/itemsApi/rest/subject/search?name=' + firstName + '%20' + lastName;
            Map<String, String> headers = new Map<String, String>();
            headers.put('Content-Type', 'application/json');
            headers.put('USDAEAUTHID', '28992012111608101585641');
            RestClient restClient = new RestClient(url, 'GET', headers);
            return restClient.responseBody;
          }
    
    /**
     * Check to see if the response contains any returned results. If there is atleast 1, this returns true
     */
      public static Boolean containsResults(String response) 
        {
            if(response != null){
                JSONParser parser = JSON.createParser(response);
                while (parser.nextToken() != null) 
                    {
                        //look to see if the response has a subject returned
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'subjectID')) 
                        {
                           return true;
                        }
                    }
            }
            return false;
        }
}