@isTest(SeeAllData=true)
private class CARPOL_AC_EPRegulations_extension2Test {
    static testMethod void testCARPOL_AC_EPRegulations_extension2() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);      
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
      Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Attachment attach = testData.newattachment(ac.Id);           
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      objauth.RecordTypeID = ACauthRecordTypeId;
      update objauth;
      ac.Authorization__c = objauth.Id;
      update ac;
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
      // Added by VV 
      String PPQTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
      String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
      String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
      objreg4.RecordTypeid = ReguRecordTypeId;
      update objreg4;
      Domain__c objprog = testData.newProgram('PPQ');
       Program_Prefix__c objPrefix = new Program_Prefix__c();
     objPrefix.Program__c = objprog.Id;
     objPrefix.Name = '251';
     objPrefix.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
     insert objPrefix;
       Signature__c objTP = new Signature__c();
      objTP.Name = 'Test AC TP Jialin';
      objTP.Recordtypeid = PPQTPRecordTypeId ;
      objTP.Program_Prefix__c = objPrefix.Id;
      insert objTP; 
      
      Program_Prefix__c objPrefix1 = new Program_Prefix__c();
     objPrefix1.Program__c = objprog.Id;
     objPrefix1.Name = '252';
     objPrefix1.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
     insert objPrefix1;
       Signature__c objTP1 = new Signature__c();
      objTP1.Name = 'Test AC TP Jialin';
      objTP1.Recordtypeid = PPQTPRecordTypeId ;
      objTP1.Program_Prefix__c = objPrefix1.Id;
      insert objTP1; 
      
      Program_Prefix__c objPrefix2 = new Program_Prefix__c();
     objPrefix2.Program__c = objprog.Id;
     objPrefix2.Name = '253';
     objPrefix2.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
     insert objPrefix2;
       Signature__c objTP2 = new Signature__c();
      objTP2.Name = 'Test AC TP Jialin';
      objTP2.Recordtypeid = PPQTPRecordTypeId ;
      objTP2.Program_Prefix__c = objPrefix2.Id;
      insert objTP2;
      
      objauth.Thumbprint__c = objTP.Id;
      update objauth;
      
      
      PageReference pageRef = Page.CARPOL_AC_EditPermit_Regulations2;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_EPRegulations_extension2 acepreg = new CARPOL_AC_EPRegulations_extension2(sc);
      System.assert(acepreg != null);
      acepreg.regulation = objreg1;
      acepreg.getResults();
      acepreg.setRegulationType();
      acepreg.getAddByOptions();
      acepreg.selectInput();
      acepreg.getAddByOptions();
      acepreg.updateSignature();
      acepreg.viewDraftPDF();
      acepreg.createNewRegulation();
      //acepreg.attachPDF();
      objauth.Thumbprint__c = objTP.id;
      objauth.Recordtypeid = authRecordTypeId ;
      objauth.Authorization_Type__c = 'Permit';
      update objauth;
       Facility__c  entry = testData.newfacility('Domestic Port');
          Authorization_Junction__c objauthjun = new Authorization_Junction__c();
          objauthjun.Authorization__c = objauth.id;
          objauthjun.Port__c = entry.id;
          insert objauthjun;
       ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_EPRegulations_extension2 acepreg2 = new CARPOL_AC_EPRegulations_extension2(sc2);
      acepreg2.viewDraftPDF();
      objPrefix.Permit_PDF_Template__c = '';
      update objPrefix;
      acepreg2.viewDraftPDF();
      acepreg2.attachPDF();
      
      objauth.Thumbprint__c = objTP1.Id;
      update objauth;
      ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_EPRegulations_extension2 acepreg3 = new CARPOL_AC_EPRegulations_extension2(sc3);
      System.assert(acepreg3 != null);
      
      objauth.Thumbprint__c = objTP2.Id;
      update objauth;
      acepreg3.viewDraftPDF();
      ApexPages.Standardcontroller sc4 = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_EPRegulations_extension2 acepreg4 = new CARPOL_AC_EPRegulations_extension2(sc4);
      System.assert(acepreg4 != null);    
      
      acepreg4.viewDraftPDF();     
      
      
      //System.assert(acepreg != null);
    }
}