public with sharing class CARPOL_PPQ_PermitPDFController2 {

    public ID appId {get; set;}
    public ID authId {get;set;}
    public Authorizations__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineItem{get;set;}
    public String htmlText {get;set;}
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
    public CARPOL_PPQ_PermitPDFController2(ApexPages.StandardController controller) {
        app = (Authorizations__c)controller.getRecord();
        authId = ApexPages.currentPage().getParameters().get('id');
        app = [Select Id, Application__c from Authorizations__c where Id=: authId];
        appId = app.Application__c;
        //strPathway = ApexPages.currentPage().getParameters().get('strPathway');
        System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
    }
     public CARPOL_PPQ_PermitPDFController2()  //VV added
    {}
    public List<AC__c> getACRecords(){
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ',Country_Of_Origin__r.Name,Country__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Component__r.Name ,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            
            lineItem = aclst[0]; 
        }
        
        return aclst;
    }
    
      @InvocableMethod
     public static void saveAutoAttachment(list<id> lstauthids){ // VV added
         string authid= lstauthids[0];
      Authorizations__c app = [SELECT Id, Name, Application__c, Status__c, Authorization_Type__c, Response_Type__c, Thumbprint__c,RecordType.Name FROM Authorizations__c WHERE Id = :authid];
    //Application__c appl = [SELECT Applicant_Email__c, Name, ID, Applicant_Name__r.Name from Application__c where ID = :app.Application__c];
       if(app.Id != null &&  app.Authorization_Type__c == 'Permit'){
            System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
            PageReference pdf = Page.CARPOL_PPQ_PermitPDF;
            pdf.getParameters().put('id', app.Id);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                }
            att.Name = 'Permit.pdf';
            att.ParentId = App.Id;
            att.body = fileBody;
            insert att;
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Permit Successfully Saved'));
              
    }
    
           
}

}