public class CARPOL_GeneratePermitNumber {


// These variables store Trigger.oldMap and Trigger.newMap
  Map<Id, Authorizations__c> oldAuth;
  Map<Id, Authorizations__c> newAuth;


// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_GeneratePermitNumber(
    Map<Id, Authorizations__c> oldTriggerAuths, 
    Map<Id, Authorizations__c> newTriggerAuths) {
      oldAuth = oldTriggerAuths;
      newAuth = newTriggerAuths;
  }
 
 
 public void generatePermitNumbers() 
 {
     for(Authorizations__c newauth : newAuth.values())
     {
     // Generates on issued or approved authorization status.
         if(newAuth.Authorization_Type__c == 'Permit'  && (newAuth.Status__c == 'Complete' ||  newAuth.Status__c == 'Approved' || (newAuth.Status__c=='Issued' && newAuth.Process_Type__c=='Fast Track')))
         {
         // should run on all authorizations not only AC.
       // List<RecordType>  authorization  = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Authorizations__c'];
       // if (authorization != null)
       // {
         //   if(newAuth.RecordTypeId == authorization[0].Id && newAuth.Authorization_Type__c == 'Permit' && newAuth.Status__c == 'Approved')
          //  {
            //String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            String randomString = '';
            Boolean check = true;
            while(check)
            {
                System.debug('<<<<<<<<<<<<<<<< While Check >>>>>>>>>>>>>>>>>>>>>>>>');
                while(randomString.length() < 7)
                {
                Integer index = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
                randomString += chars.substring(index, index + 1);
                }
                // Concantenate the prefix and random string
                String tmp = newAuth.Prefix__c + '-'+ randomString.toUpperCase();
                randomString = '';
                System.debug('<<<<<<<<< Random ' + tmp + '>>>>>>>>>>>>>>>>>>>>>>');
                // Validate the permit number does not exist yet
                List<Authorizations__c> PermitNumberList = [select Permit_Number__c from Authorizations__c where Permit_Number__c = :tmp];
                if(PermitNumberList.size() == 0)
                {
                System.debug('<<<<<<<<<<<<<<<<<<<<<<<<<<< List size ' + PermitNumberList.size() + '>>>>>>>>>>>>>>>>>');
                // Update the permit number
                newAuth.Permit_Number__c = tmp;
                check = false;

                }
                else 
                {
                check = true;
                newAuth.Permit_Number__c = '';
                }
          //  }

        //   }
           }
       }
    }
}
}