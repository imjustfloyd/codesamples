@isTest(seealldata=true)
private class CARPOL_UNI_Auth_Junction_WF_Test {
      @IsTest static void CARPOL_UNI_Auth_Junction_WF_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);     
                
          Domain__c objProg = new Domain__c();
          objProg.Name = 'AC';
          objProg.Active__c = true;
          insert objProg;
          
          Program_Prefix__c prefix = new Program_Prefix__c();
          prefix.Program__c = objProg.Id;
          prefix.Name = '555';
          insert prefix; 
          
          Signature__c objTP = new Signature__c();
          objTP.Name = 'Test AC TP';
          objTP.Recordtypeid = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          objTP.Program_Prefix__c = prefix.Id;
          insert objTP;        
         
          Program_Workflow__c pw = new Program_Workflow__c();
          pw.Program_Prefix__c = prefix.id;
          pw.Assign_to_Queue__c = 'AC Reviewer';
          pw.Program__c = objProg.id;
          pw.Active__c = 'Yes';
          pw.Task_Record_Type__c = 'Standard';
          insert pw;          
          
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          objauth.Thumbprint__c = objTP.id;
          objauth.prefix2__c = '555';
          update objauth;
          
          ac1.Authorization__c = objauth.id;
          ac1.status__c = 'Waiting on Customer';
          //ac1.RecordTypeId = ACLIRecordTypeId;
          update ac1;
          List<AC__c> lstauth = new List<AC__c>();
          lstauth.add(ac1);
          
          Map<Authorizations__c,List<AC__c>> mapauth = new Map<Authorizations__c,List<AC__c>>();
          mapauth.put(objauth,lstauth);
          
          Workflow_Task__c wft = testData.newworkflowtask('Check Violator', objauth, 'Pending');          
          wft.Status_Categories__c = 'Customer Feedback'; 
          wft.Process_Type__c = 'Fast Track';     
          update wft;
          //Workflow_Task__c wft1 = testData.newworkflowtask('Process Authorization', objauth, 'Pending');
          //wft1.Status_Categories__c = 'Customer Feedback';
          //update wft1;
          Map<Id,Workflow_Task__c> mapId = new Map<Id,Workflow_Task__c>();
          mapId.put(wft.Id,wft);
          //mapId.put(wft1.Id,wft1);
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          insert objdm;
          
          Required_Documents__c rqdocs = new Required_Documents__c();
          rqdocs.Decision_Matrix__c = objdm.id;
          rqdocs.Required_Docs__c = 'Health;Test';
          insert rqdocs;
      
          Required_Documents__c rqdocs2 = new Required_Documents__c();
          rqdocs2.Decision_Matrix__c = objdm.id;
          rqdocs2.Required_Docs__c = 'Health';
          insert rqdocs2;
      
          Related_Line_Decisions__c objrld = new Related_Line_Decisions__c();
          objrld.Line_Item__c = ac1.id;
          objrld.Decision_Matrix__c = objdm.id;
          insert objrld; 
          Related_Line_Decisions__c objrld1 = new Related_Line_Decisions__c();
          objrld1.Line_Item__c = ac2.Id;
          objrld1.Decision_Matrix__c = objdm.id;
          insert objrld1;  
          Related_Line_Decisions__c objrld2 = new Related_Line_Decisions__c();
          objrld2.Line_Item__c = ac3.Id;
          objrld2.Decision_Matrix__c = objdm.id;
          insert objrld2;           

          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          Signature_Regulation_Junction__c objSRJ = testData.newSRJ(objTP.id,objreg1.id);
          objSRJ.Decision_Matrix__c = objdm.id;
          update objSRJ;
          Signature_Regulation_Junction__c objSRJ1 = testData.newSRJ(objTP.id,objreg2.id);
          objSRJ1.Decision_Matrix__c = objdm.id;
          update objSRJ1;          
          Signature_Regulation_Junction__c objSRJ2 = testData.newSRJ(objTP.id,objreg3.id);
          objSRJ2.Decision_Matrix__c = objdm.id;
          update objSRJ2;
          
          
          

          Test.startTest();
          
          CARPOL_UNI_Auth_Junction_WF extclass = new CARPOL_UNI_Auth_Junction_WF();
          //extclass.updateExpDate(objauth);
          extclass.createAuthorizationJunctionRecord(mapauth);
          system.assert(extclass != null);
                   
          Test.stopTest();     
      }
}