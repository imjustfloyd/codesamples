@isTest(seealldata=true)
private class CreateAuthorizationJunction_Test {
      @IsTest static void CreateAuthorizationJunction_Test() {
      
          
          
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newAuth(objapp.id);
          AC__c li = testData.newlineitem('Personal Use', objapp);
          li.Authorization__c = objauth.id;
          li.Program_Line_Item_Pathway__c = plip.id;
          update li;

          List<Authorizations__c> newlist = new List<Authorizations__c>();
          newlist.add(objauth);
          Domain__c prog = testData.newProgram('AC');
          Program_Prefix__c pp = new Program_Prefix__c();
          pp.Program__c = prog.Id;
          pp.Name = '555';
          insert pp;      
                    
          Signature__c thumb = testData.newThumbprint(); 
          thumb.Program_Prefix__c =  pp.id;

          update thumb;               
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.Thumbprint__c = thumb.id;
          insert objdm;
      
          Related_Line_Decisions__c objrld = new Related_Line_Decisions__c();
          objrld.Line_Item__c = li.id;
          objrld.Decision_Matrix__c = objdm.id;
          insert objrld;    
          

          Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
          Regulation__c newreg2 = testData.newRegulation('Additional Information','Test');        
          Signature_Regulation_Junction__c srj1 = testData.newSRJ(thumb.id,newreg1.id);
          srj1.Decision_Matrix__c = objdm.id;
          update srj1;
          Signature_Regulation_Junction__c srj2 = testData.newSRJ(thumb.id,newreg2.id);  
          
                  
          list<Group> gList = [select id,name from Group];
          System.debug('dawn group ' + gList[0]);
          
          Program_Workflow__c pw = new Program_Workflow__c();
          pw.Program_Prefix__c = pp.id;
          pw.Assign_to_Queue__c = 'AC Reviewer';
          pw.Program__c = prog.id;
          pw.Active__c = 'Yes';
          pw.Task_Record_Type__c = 'Standard';
          insert pw;
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_AC_ApplicationPDF;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CreateAuthorizationJunction extclass = new CreateAuthorizationJunction();
              extclass.createAuthorizationJunctionRecord(newlist);
              system.assert(extclass != null);                             

                      
          Test.stopTest();   
      }
}