public  class CARPOL_BRS_StateReviewRecords{

    public Id ReviewerId{get;set;}
    public list<Reviewer__c> Rev {get;set;}

    public CARPOL_BRS_StateReviewRecords(ApexPages.StandardController controller) {

      ReviewerId = ApexPages.currentPage().getParameters().get('id');
      Rev = [SELECT Authorization__c,Id,Authorization__r.name FROM Reviewer__c where Id =:ReviewerId] ;
    }

    public List<Attachment> getattach(){
       return [SELECT   Name, Description, ContentType, CreatedById, Id, ParentId FROM Attachment WHERE ParentId = : ReviewerId];
    }
    
     public List<Applicant_Attachments__c> getAppAttach(){
          system.debug('ReviewerId ####'+ReviewerId);
           
       return [SELECT Attachment_Type__c,Document_Types__c,Authorization__c,Id,Name FROM Applicant_Attachments__c WHERE Authorization__c = : Rev[0].Authorization__c and Attachment_Type__c='State Package' ];
    }
    
        public PageReference Save()
    {
       // upsert acct;
        PageReference pg= new PageReference ('/'+ReviewerId);
        pg.setRedirect(true);
        return pg;
        
    }
    
    public PageReference getEditPage()
    {
        //String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
       // system.debug('+++BaseUrl+++'+BaseUrl);
      //  PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Account_Edit?id='+accountID);
        PageReference dirpage= new PageReference('/apex/CARPOL_BRS_StateReviewEditRecords?id='+ReviewerId);
        dirpage.setRedirect(true);
        return dirpage;
    }    
    
    public PageReference SubmitReponses() { 
         list<Reviewer__c> revList = [select id,Status__c from Reviewer__c where Id =: ReviewerId];
         revList[0].Status__c = 'Completed';
         update revList;
         PageReference pg= new PageReference ('/'+ReviewerId);
        pg.setRedirect(true);
        return pg;
    }
}