public class CARPOL_PPQ_Transit_PermitPDFController {

    public ID appId {get; set;}
    public ID authId {get;set;}
    public Authorizations__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineItem{get;set;}
    public String htmlText {get;set;}
    public String tdSpan = 'font-size:9.0pt;color:#221F1F';
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
    public String defaultPart {get;set;}
    Private List<TableCol> tblColumns;
    public String firstentryport {get;set;}
    public String portexit {get;set;}    
    public String convinUS {get;set;}    
    public String convwhileinUS  {get;set;}        
    public String convoutUS {get;set;}                       
    public string portsSelected {get;set;}    
    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public string HandCarry {get;set;}
    public Decimal  facnum {get;set;}
    public transit_locale__c TransLoc{get;set;}

    public CARPOL_PPQ_Transit_PermitPDFController(ApexPages.StandardController controller){
        convinUS = '';
        convwhileinUS  = '';
        convoutUS = '';
        firstentryport  = '';
        portexit  = '';
        app = (Authorizations__c)controller.getRecord();
        authId = ApexPages.currentPage().getParameters().get('id');
        app = [Select Id, Application__c,Program_Pathway__c,Plant_Inspection_Station__c, Plant_Inspection_Station__r.name from Authorizations__c where Id=: authId];
        appId = app.Application__c;
        
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
        defaultPart = 'Seed';
        getPDFFields();

        system.debug('&&&Start&&&');
            portsSelected = ''; //5/16/16 VV 
        list<Authorization_Junction__c> lstAuthjunct = new list<Authorization_Junction__c>();
        for(Authorization_Junction__c authjunct: [select id,Name,Port__c,Port__r.Name,Authorization__c from Authorization_Junction__c where Authorization__c=:authId  and Port__c!=null])
        {
            lstAuthjunct.add(authjunct);
        }
        if(lstAuthjunct.size()>0){
            for(integer i=0;i<lstAuthjunct.size();i++){
                if(i<lstAuthjunct.size()-1){
                    portsSelected += lstAuthjunct[i].Port__r.Name+'; ';
                }
                else{
                    portsSelected += lstAuthjunct[i].Port__r.Name;
                }
            }
        }
        system.debug('----portsSelected---'+portsSelected);   //5/16/16 END VV 



    }
    public CARPOL_PPQ_Transit_PermitPDFController()  //VV added
    {
    }

    public List<AC__c> getACRecords(){
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ', Growing_Location_County__r.name,  Growing_Location_State__r.name, Country_Of_Origin__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name,Group__r.Name,Component__r.Name ,Country_of_Export__r.Name,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Regulated_Article__r.Plant_Part__r.Name, RA_Scientific_Name__r.name, Country_of_destination__r.name,Scientific_Name__r.Category_Group_Ref__r.name  from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            
            lineItem = aclst[0]; 
        }
        set<string> lineitemsidlist = new set<string>();
        for ( ac__c ac: aclst){
            lineitemsidlist.add(ac.id);
        }
        list<transit_locale__c> lstTransLoc = new list<transit_locale__c>();
        for (transit_locale__c TransLoc : [select Name, Arrival_Date_Time__c, Level_2_Region__c, Transit_Facility__c, From_Country__c,Line_Item__c,Method_of_Trans__c,Mode_of_Transportion__c, Packaging_Material_Category__c,Port_of_Entry__c,Port_of_Exit__c, Regulated_Article__c,Level_1_Region__c,To_Country__c,Type_of_Conveyance__c, Port_of_Exit__r.name, Port_of_Entry__r.name  from transit_locale__c where Line_Item__c in:lineitemsidlist] )
              {
                  if(TransLoc.Type_of_Conveyance__c=='Into the US' && TransLoc.Mode_of_Transportion__c!=null){
                      convinUS += TransLoc.Mode_of_Transportion__c+', ';
                  }
                  if(TransLoc.Type_of_Conveyance__c=='While in the US' && TransLoc.Mode_of_Transportion__c!=null){
                      convwhileinUS  += TransLoc.Mode_of_Transportion__c+', ';
                  }
                  if(TransLoc.Type_of_Conveyance__c=='Leaving the US' && TransLoc.Mode_of_Transportion__c!=null){
                      convoutUS += TransLoc.Mode_of_Transportion__c+', ';
                  }
                  portexit += TransLoc.Port_of_Exit__r.name+', ';
                  if (firstentryport  == '')
                     { firstentryport = TransLoc.Port_of_Entry__r.name;}
               }    
         convinUS = convinUS.removeEnd(', ');
         convwhileinUS = convwhileinUS.removeEnd(', ');
         convoutUS = convoutUS.removeEnd(', ');
         portexit = portexit.removeEnd(', ');                           
        return aclst;
    }
    
     public void getPDFFields()
    {

         //Regulations_Association_Matrix__c raMatrix = [Select ID, Name, Program_Line_Item_Pathway__c from Regulations_Association_Matrix__c where Id =:app.Final_Decision_Matrix_Id__c];
         pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                    Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                    PDF_Heading_1__c, PDF_Heading_2__c, PDF_Permit_Conditions_Static__c, Table_Section_Heading__c, Permit_PDF_Sub_Heading_1__c, Permit_PDF_Heading_1__c, Permit_PDF_Heading_2__c, 
                    PDF_Forwarded_Completed_App_Address__c from Program_Line_Item_Pathway__c where Id =: app.Program_Pathway__c];

         tblColumns = new List<TableCol>();           

        if(pdfLnItmPway.Column_1_API_Name__c!=NULL)        
            tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
        if(pdfLnItmPway.Column_2_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
        if(pdfLnItmPway.Column_3_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
        if(pdfLnItmPway.Column_4_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
        if(pdfLnItmPway.Column_5_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
        if(pdfLnItmPway.Column_6_API_Name__c!=NULL)
             tblColumns.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c)); 
                
    }
    
  /*  public String getTable()
    {
        htmlText ='';
        //List<CARPOL_UNI_ApplicationPDF_Table__c> tableSettings = CARPOL_UNI_ApplicationPDF_Table__c.getall().values();
        //tableSettings.sort();
        

        htmlText += '<tr>';
            //loop over column heads from tablecol object
            for(TableCol tbl: tblColumns){
                
                
                    htmlText += '<th align="center"><span style="' + tdSpan + '">';
                    htmlText +=  tbl.Heading;
                    htmlText += '</span></th>';
                    counter++;
                
            }
            
            htmlText += '</tr>';
        
        for(AC__c a: aclst)
        {
            counter =0;
            htmlText += '<tr>';
            for(TableCol tbl: tblColumns){
                htmlText += '<td align="center"><span style="' + tdSpan + '">';
                htmlText +=  (getFieldValue(a,tbl.APIName)!=null) ? getFieldValue(a,tbl.APIName): '';
                
                htmlText += '</span></td>';
                counter++;
            }
            htmlText += '</tr>';
            
            
        }
        
        return htmlText;
    }
    */
    public static Object getFieldValue(SObject o,String field){
 
     if(o == null){
        return null;
     }
     if(field.contains('.')){
    String nextField = field.substringAfter('.');
    String relation = field.substringBefore('.');
    return CARPOL_UNI_ApplicationPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
     }else{
    return o.get(field);
     }
    

    }
    
    //Added by Tharun 04/27/2016
    
    public List<Authorization_Junction__c> getConditions(){
        
        try{
    
scj= new List<Authorization_Junction__c>();
    scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :authId ORDER BY Order_Number__c ASC];
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
    scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' && SCJ.Is_Active__c=='Yes' ){scjA.add(SCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(SCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
            
            }
        }
        
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }
     return scj;
        
    }
  //End of Tharun changes --04/27/2016  
    
    //Added Niharika-to save and attach the PDF
   /* public void saveAttachment(){
    system.debug('$%$%$ in saveAttachment');
      app = [SELECT Id, Name, Application__c, Status__c, Authorization_Type__c, Response_Type__c, Thumbprint__c FROM Authorizations__c WHERE Id = :app.Id];
    Application__c appl = [SELECT Applicant_Email__c, Name, ID, Applicant_Name__r.Name from Application__c where ID = :app.Application__c];
       if(app.Id != null &&  app.Authorization_Type__c == 'Permit'){
            
            PageReference pdf = Page.CARPOL_PPQ_PermitPDF;
            pdf.getParameters().put('id', app.Id);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                   
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    
                }
            att.Name = 'Permit_non_process_builder.pdf';
            att.ParentId = App.Id;
            att.body = fileBody;
            insert att;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Permit Successfully Saved'));
              
    }
}
     @InvocableMethod
     public static void saveAutoAttachment(list<id> lstauthids){ // VV added
         system.debug('&*&*& in saveAutoAttachment');
         string authid= lstauthids[0];
      Authorizations__c app = [SELECT Id, Name, Application__c, Status__c, Authorization_Type__c, Response_Type__c, Thumbprint__c,RecordType.Name FROM Authorizations__c WHERE Id = :authid];
    //Application__c appl = [SELECT Applicant_Email__c, Name, ID, Applicant_Name__r.Name from Application__c where ID = :app.Application__c];
       if(app.Id != null &&  app.Authorization_Type__c == 'Permit'){
          
            PageReference pdf = Page.CARPOL_PPQ_PermitPDF;
            pdf.getParameters().put('id', app.Id);
            Attachment att = new Attachment();
            Blob fileBody;
               try {
                     fileBody = pdf.getContentasPDF();
    
                    
               } 
               catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    
               }
            att.Name = 'Permit.pdf';
            att.ParentId = App.Id;
            att.body = fileBody;
            insert att;
           // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Permit Successfully Saved'));
              
    }
    }
    */
 
    public Class TableCol
    {
        public String Heading;
        public String APIName;
        public String sortOrder;

        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }
    
}