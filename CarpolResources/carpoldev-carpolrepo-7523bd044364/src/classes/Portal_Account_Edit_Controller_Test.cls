@isTest(seealldata=false)
private class Portal_Account_Edit_Controller_Test {
      @IsTest static void Portal_Account_Edit_Controller_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();


          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='State Reviewer'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.Portal_Account_Edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objacct);
              Portal_Account_Edit_Controller extclassempty = new Portal_Account_Edit_Controller(sc);
              ApexPages.currentPage().getParameters().put('id',objacct.id);                    
              Portal_Account_Edit_Controller extclass = new Portal_Account_Edit_Controller(sc);
              extclass.bAddress = '';
              extclass.getEditPage();              
              extclass.Save();  
              System.assert(extclass != null);        
          Test.stopTest();   
      }
}