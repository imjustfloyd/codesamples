/* 
// Author : Dawn Sangree
// Date Created : 03/07/2016
// Purpose : Ability to Add Documents on Application Line Item using generated list of required documents
// Replacement for CARPOL_AddDocuments and CARPOL_PPQ_AddDocuments
*/

public with sharing class CARPOL_AddDocuments_Dynamic {

    //required documents map
    //public Map<String,documentResource> mapRequiredDocuments { get; set;}
    //select option list of documents remaining
    public List<SelectOption> remainingDocuments { get; set;}
    //select option list of documents requried
    public List<SelectOption> requiredDocuments { get; set; }
    //expect this one to go
    public Boolean renderRemainingDocuments { get; set; }
    //remove once save is rewritten - expect this to migrate onto the field
    public String[] selectedDocuments { get; set; }
    //line item id 
    public ID lineItemID{get;set;}
    public AC__c lineItem {get; set;} 

    public List<Applicant_Attachments__c> appAttachments {get;set;}
    //list of new attachments to add
    public List<DocumentWrap> newAttachments {get;set;}
    
    //the number of new attachments to add to the list when the user clicks 'Add More'
    //Save for future use
    //public static final Integer NUM_ATTACHMENTS_TO_ADD=1;

    //used to compare uploaded records with required records
    public Map<String,documentResource> mapUploadedDocuments = new Map<String,documentResource>();
    //should be removed, replace it's use in the map with something else
    public List<documentResource> docResource = new List<documentResource>();

      public CARPOL_AddDocuments_Dynamic() {
  
        System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        
        lineItemID = ApexPages.currentPage().getParameters().get('id');

        //get the line item we are going to attach files to
        if(lineItemID != null)
        {
            lineItem = [SELECT Name, Application_Number__c, Intended_Use__c, Intended_Use__r.Name, Application_Number__r.Name FROM AC__c WHERE ID = :lineItemID LIMIT 1];
        }        
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Line Item found.'));}

        // instantiate the list with a single attachment
        newAttachments=new List<DocumentWrap>{new DocumentWrap()};
        
        requiredDocuments = new List<SelectOption>();
        requiredDocuments = getRequiredDocument();

        System.Debug('<<<<<<< Total Required Documents : ' + requiredDocuments.size() + ' >>>>>>>');

        getExistingDocuments();
    }
    
    // Add more attachments action method - save for future use
    /*public void addMore()
    {
        // append NUM_ATTACHMENTS_TO_ADD to the new attachments list
        for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++)
        {
            newAttachments.add(new DocumentWrap());
        }
    } */    
    
    //Getting existing uploaded documents if any
    public void getExistingDocuments() {
        remainingDocuments = new List<SelectOption>();
        
        appAttachments = [SELECT Id, Name, Document_Types__c, File_Name__c, File_Description__c,(SELECT ID, Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Animal_Care_AC__c = :lineItemID];
        System.Debug('<<<<<<< Total Uploaded Documents : ' + appAttachments.size() + ' >>>>>>>');
        
        if(appAttachments.size() > 0)
        {
            for(Applicant_Attachments__c appAtt : appAttachments)
            {
                System.Debug('<<<<<<< Uploaded Documents : ' + appAtt.Document_Types__c + ' >>>>>>>');
                String[] documents = appAtt.Document_Types__c.split(';');
                System.Debug('<<<<<<< Uploaded Documents Stack : ' + documents.size() + ' >>>>>>>');

                //keep around but you may not need this to render the link on the component
                documentResource docRes = new documentResource();
                if(appAtt.Attachments.size() > 0)
                {
                    docRes.docURL = '/servlet/servlet.FileDownload?file=' + appAtt.Attachments[0].ID;
                    docRes.isUploaded = true;
                }
                for(Integer j = 0; j < documents.size(); j++)
                {
                    mapUploadedDocuments.put(documents[j],docRes);
                    System.Debug('<<<<<<< mapUploadedDocuments Size  : ' + mapUploadedDocuments.size() + ' >>>>>>>');
                }
            }
        }

        //cull the required documents list, remove items that are already uploaded.
        if(requiredDocuments.size() > 0)
        {
            for(Integer i = 0 ; i < requiredDocuments.size(); i++)
            {
                String documentName = String.ValueOf(requiredDocuments[i].getLabel());
                documentResource docResTemp = mapUploadedDocuments.get(requiredDocuments[i].getLabel());

                //if document wasn't already uploaded, add empty document resource to the map
                //add document to the remaining document list, it hasn't been uploaded yet.
                if(docResTemp == null)
                {
                    docResTemp = new documentResource();
                    docResTemp.isUploaded = false;
                    remainingDocuments.add(new SelectOption(documentName, documentName));
                }
            }
        }
        //set render Remaining documents flag, still going to use this
        if(remainingDocuments.size() > 0){renderRemainingDocuments = true;}else{renderRemainingDocuments = false;}
    }
    
    
    public void save()
    {   //must insert the app attachments, populate then populate applicant attachment id on attachments.  

        List<SObject> toInsert = new List<SObject>();
        //record type for all Applicant_Attachments__c
        ID tmpRecordType = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Uploaded Attachment').getRecordTypeId();

        for (DocumentWrap newAtt : newAttachments)
        {
            //validate that an attachment exists and a document type was selected.            
            if (newAtt.attachment.Body!=null & newAtt.documenttypes.size() > 0)
            {
                //add the standard attributes and update document types
                newAtt.appattach.RecordTypeID = tmpRecordType;
                newAtt.appAttach.Animal_Care_AC__c = lineItemID;
                newAtt.appattach.Application__c = lineItem.Application_Number__c;
                
                if(newAtt.appAttach.File_Name__c.length() == 0){
                    //no file name entered, make same as attachment name
                    newAtt.appAttach.File_Name__c = newAtt.attachment.name;
                }
                
                if(newAtt.appAttach.File_Description__c.length() == 0){
                    //no description entered, add one
                    newAtt.appAttach.File_Description__c = 'Uploaded ' + System.Now();
                }                
                  //handle document types
                  newAtt.appAttach.Document_Types__c = flattenSelectedDocuments(newAtt.documenttypes);
                  
                  toInsert.add(newAtt.appAttach);
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You must add a file to upload and select a document type before selecting Save.'));            
            }
        }

        insert toInsert;
        toInsert.clear();
        
        //new insert attachments
        for (DocumentWrap newAtt : newAttachments)
        {
            if (newAtt.attachment.Body!=null & newAtt.documenttypes.size() > 0)
            {
                  //handle the physical attachment
                  newAtt.attachment.OwnerId = UserInfo.getUserId();
                  newAtt.attachment.ParentId = newAtt.appAttach.ID;
                  newAtt.attachment.Description = 'Uploaded Document';

                  toInsert.add(newAtt.attachment);
            }
        }
        
        insert toInsert;
        
        newAttachments.clear();
        newAttachments.add(new DocumentWrap());
        
        //refresh the list of required and existing documents        
        getExistingDocuments();
    }
    
    public List<SelectOption> getRequiredDocument() {

        List<SelectOption> options = new List<SelectOption>();
        List<String> reqDocs = CARPOL_UNI_Get_Required_LI_Docs.getRequiredDocs(lineItemID);
        if(reqDocs.size() > 0){
            for(String opt : reqDocs){
                  options.add(new SelectOption(opt,opt));
            }
        } else {
            options.add(new SelectOption('No document types found','No document types found',true));        
        }
        return options;
    }
    
    //flatten the document types
    public String flattenSelectedDocuments(List<String> documenttypes) {
    
        System.debug('<<<<<<< Selected Documents List Size : ' + documenttypes.size());
        String strSelectedDocuments = '';        
        if(documenttypes.size() != 0)
        {

            for(Integer i = 0; i < documenttypes.size(); i++)
            {
                strSelectedDocuments += documenttypes[i] + ';';
            }
            //removing the last '; '
            strSelectedDocuments = strSelectedDocuments.substring(0,strSelectedDocuments.length() - 1);
        }
        return strSelectedDocuments;
    }
    
    
    //Class to hold uploaded documents information
    public class documentResource{
        public ID docID {get;set;}
        public Boolean isUploaded {get;set;}
        public string docName {get;set;}
        public String docURL {get;set;} 
    } 
    
    //Subclass : Wrapper Class 
    @TestVisible public class DocumentWrap {
       public Applicant_Attachments__c appAttach{get;set;}
       public Attachment attachment {get;set;}    
       public List<String> documenttypes{get;set;}    

       //Wrapper  Class Controller
       public DocumentWrap() {
            appAttach = new Applicant_Attachments__c();
            attachment= new Attachment();
            documenttypes = new List<String>();
        }
    }
}