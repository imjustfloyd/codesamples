global class CARPOL_BRS_SendEmailsBatch implements Schedulable {

    /*public static String CRON_EXP = '0 6 * * * ? ';
            global static String scheduleIt() {
                CARPOL_BRS_SendEmailsBatch sm = new CARPOL_BRS_SendEmailsBatch();
                 return System.schedule('Send Emails', CRON_EXP, sm);
            }*/
    String query;
    public Datetime day = System.now() + 5;
    public Datetime day2 = System.now() - 15;
    public List<id> usridlst {get;set;}
    Public List<Reviewer__c> orrlst = [SELECT CreatedDate, Status__c, Email_sent_on_status_Open__c, Authorization__r.Permit_Specialist__c,Authorization__r.Bio_Tech_Reviewer__c,Email_sent_on_status_Completed__c,Authorization__r.Name,Authorization__r.Id FROM Reviewer__c];// WHERE (Status__c = 'Open' AND Email_sent_on_status_Open__c = false) OR (Status__c = 'Completed' AND Email_sent_on_status_Completed__c = false)];
    public List<String> str;


    global void execute (SchedulableContext sc) {
        Set<Id> userids = new Set<Id>();
        Set<Id> userids2 = new Set<Id>();
        Set<Id> userids3 = new Set<Id>();
        for(Reviewer__c revi : orrlst){
            if(revi.LastModifiedDate < day2){
                userids3.add(revi.Authorization__r.Permit_Specialist__c);
            }
            if(revi.Status__c == 'Open' && revi.Email_sent_on_status_Open__c == false){
                userids.add(revi.Authorization__r.Permit_Specialist__c);
            } else if(revi.Status__c == 'Completed' && revi.Email_sent_on_status_Completed__c == false){
                userids2.add(revi.Authorization__r.Bio_Tech_Reviewer__c);
            }
        }
    LIst<User> usrlst = [SELECT Id,Email FROM User WHERE ID IN :userids AND IsActive = True];
    LIst<User> usrlst2 = [SELECT Id,Email FROM User WHERE ID IN :userids2 AND IsActive = True];
    LIst<User> usrlst3 = [SELECT Id,Email FROM User WHERE ID IN :userids3 AND IsActive = True];

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> emails2 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> emails3 = new List<Messaging.SingleEmailMessage>();

        if(orrlst.size() > 0) {
            for(Reviewer__c r : orrlst){
                if(r.Status__c == 'Open' && r.Email_sent_on_status_Open__c == false ){
                    System.Debug('<<<<<<<<<User Email List>>>>>>>>>> ' + usrlst);
                    DateTime Dt = r.CreatedDate;
                             System.debug('Created date : ' + Dt);
                             Date date1 = date.newInstance(Dt.year(), Dt.month(), Dt.day());
                             System.debug('converted date : ' + date1);
                             Date date2 = date1 +5;
                             DateTime date3 = System.now();
                             System.debug('date 2 : ' + date2);
                    if(date2 < date3 && usrlst.size()>0){
                    System.debug('Created Date is >>>>>> : ' + r.CreatedDate);
                        List<String> str = new List<String>();
                        List<Id> usridlst = new List<Id>();
                        for(User U : usrlst){
                            str.clear();
                            str.add(U.Email);
                            System.debug('Str List has the following : ' + str);
                            usridlst.clear();
                            usridlst.add(U.Id);
                            System.debug('The new list with User IDs has the following : ' + usridlst);
                        }
                            for(integer i = 0; i < str.size(); i++){
                                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                //String [] test = New String [] {'raghu.parupalli@creativesyscon.com'};
                                String [] toAddress = str;
                                email.setToAddresses(toAddress);
                                email.setSubject('List of States not responded yet for ' +  r.Authorization__r.Name);
                                String authName = r.Authorization__r.Name;
                                email.setHtmlBody('<html><body> This is an automatic notification to let you know that few states have not responded yet for ' +   r.Authorization__r.Name +' and is awaiting further action. <br/><br/>' +' Thank you,<br/><br/> '  +' Permits <br/>'+' Biotechnology Regulatory Services <br/>'+' USDA, Animal and Plant Health Inspection Service <br/>'+' Unit 91 <br/>'+' 4700 River Road  <br/>'+' Riverdale, MD 20737  <br/></body></html>');
                                email.saveAsActivity = false;
                                email.setTargetObjectId(usridlst[i]);
                                System.debug('The following email ids are captured to send email : ' + str);
                                emails.add(email);

                            }
                    }
                }
                r.Email_sent_on_status_Open__c = true;
                                update r;
            /*}
        }
        if(orrlst.size() > 0) {
            for(Reviewer__c r : orrlst){*/
                if(r.Status__c == 'Completed' && r.Email_sent_on_status_Completed__c == false){
                        if(usrlst2.size()>0){
                        List<String> str2 = new List<String>();
                        List<Id> usridlst2 = new List<Id>();
                            for(User U : usrlst2){
                                str2.clear();
                                str2.add(U.Email);
                                System.debug('Str List has the following : ' + str2);
                                usridlst2.clear();
                                usridlst2.add(U.Id);
                                System.debug('The new list with User IDs has the following : ' + usridlst2);
                            }
                            for(integer i = 0; i < str2.size(); i++){
                                Messaging.SingleEmailMessage email2 = new Messaging.SingleEmailMessage();
                                //String [] test2 = New String [] {'raghu.parupalli@creativesyscon.com'};
                                String [] toAddress2 = str2;
                                email2.setToAddresses(toAddress2);
                                email2.setSubject('List of States have responded for r.Authorization__r.Name');
                                String body = '<html lang="en"><body> This is an automatic notification to let you know that few states have  responded for '+ r.Authorization__r.Name +' and is awaiting further action. '+' Thank you, '+' Permits   '+' Biotechnology Regulatory Services '+' USDA, Animal and Plant Health Inspection Service '+' Unit 91 '+' 4700 River Road  '+' Riverdale, MD 20737  </body></html>';
                                email2.setplainTextBody(body);
                                email2.saveAsActivity = false;
                                email2.setTargetObjectId(usridlst2[i]);
                                System.debug('The following email ids are captured to send email : ' + str2);
                                emails2.add(email2);

                            }
                        }
                }
                r.Email_sent_on_status_Completed__c = true;
                update r;

                if(r.LastModifiedDate < day2){
                        if(usrlst3.size()>0){
                        List<String> str3 = new List<String>();
                        List<Id> usridlst3 = new List<Id>();
                            for(User U : usrlst3){
                                str3.clear();
                                str3.add(U.email);
                                usridlst3.clear();
                                usridlst3.add(U.id);
                            }
                            for(integer i = 0; i < str3.size(); i++){
                                Messaging.SingleEmailMessage email3 = new Messaging.SingleEmailMessage();
                                String [] toAddress3 = str3;
                                email3.setToAddresses(toAddress3);
                                email3.setSubject('States not responded for more than 15 days');
                                String body = '<html lang="en"><body> This is an automatic notification to let you know that few states have  responded for '+ r.Authorization__r.Name +' and is awaiting further action. '+' Thank you, '+' Permits   '+' Biotechnology Regulatory Services '+' USDA, Animal and Plant Health Inspection Service '+' Unit 91 '+' 4700 River Road  '+' Riverdale, MD 20737  </body></html>';
                                email3.setplainTextBody(body);
                                email3.saveAsActivity = false;
                                email3.setTargetObjectId(usridlst3[i]);
                                emails3.add(email3);
                            }
                        }
                }
                r.Email_sent_after_15_Days_of_no_Response__c = true;
                update r;
            }
        }

        if(emails.size() > 0) {
            Messaging.sendEmail(emails);
            Messaging.sendEmail(emails2);
            Messaging.sendEmail(emails3);
        }
    }
}