public with sharing class TLPortofEntryLookup {
public Account account {get;set;} // new account to create
  public List<Facility__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
   public string regartid {get;set;}
   public string porttype {get;set;}
  public TLPortofEntryLookup() {

    porttype = '';
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    regartid = System.currentPageReference().getParameters().get('regartid');
    porttype = System.currentPageReference().getParameters().get('txt');
    System.Debug('!!!!!Port type!!!'+porttype);
    if(porttype.contains('Portofentry') || porttype.contains('Portofentry1')){
        porttype = 'Port of Entry';
    }
    if(porttype.contains('Portofexit') || porttype.contains('Portofexit1')){
        porttype = 'Port of Exit';
    }
    system.debug('--porttype --'+porttype);
    runSearch();  
  }

  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    system.debug('--regartid--'+regartid);
    results = performSearch(searchString);               
  } 

  // run the search and return the records found. 
  private List<Facility__c> performSearch(string searchString) {
  system.debug('--searchString--'+searchString);
   set<string> portidslist = new set<string>();
   List<Facility__c> portslist = new List<Facility__c>();
   String strsoql = 'select id,Name from Facility__c where id in:portidslist ';
   list<RA_Scientific_Name_Junction__c> rajunlist = new list<RA_Scientific_Name_Junction__c>();
    /* if (porttype != null && porttype !=''){
         porttype = '%' + porttype + '%';
         rajunlist  =  [select id,Port__c,Type__c,Regulated_Article__c from RA_Scientific_Name_Junction__c where Regulated_Article__c=:searchString and Type__c like :porttype ];
         }
         */

   for(RA_Scientific_Name_Junction__c rajun: [select id,Port__c,Type__c,Regulated_Article__c from RA_Scientific_Name_Junction__c where Regulated_Article__c=:regartid and Type__c=:porttype])
   {
       portidslist.add(rajun.Port__c);
   }
   System.debug('!!!!!portidslist111111'+portidslist);
   if(portidslist.isEmpty()){
       for(RA_Scientific_Name_Junction__c rajun: [select id,Port__c,Type__c,Regulated_Article__c from RA_Scientific_Name_Junction__c where Regulated_Article__c=:regartid ])
   {
       portidslist.add(rajun.Port__c);
   }
    System.debug('!!!!!portidslist2222222'+portidslist);
   }
   
   if(searchString!=null && searchString!=''){
       String searchString1 = '%'+searchString+'%';
       //portslist= [select id,Name from Facility__c where id in:portidslist and Name like:searchString ];
       strsoql = strsoql +' and name LIKE \'%' + searchString +'%\'';
       system.debug('--strsoql--'+strsoql);
   }
   else{
  // portslist= [select id,Name from Facility__c where id in:portidslist];
   }
       portslist = database.query(strsoql); 
    return portslist;

  }

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }


}