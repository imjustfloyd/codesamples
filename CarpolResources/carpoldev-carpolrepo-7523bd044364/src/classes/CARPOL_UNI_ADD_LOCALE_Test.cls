@isTest(SeeAllData=true)
private class CARPOL_UNI_ADD_LOCALE_Test {
    static testMethod void myUnitTest() { 
        
            String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
            CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
            Application__c  newApp =dataObj.newapplication();
            AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
            test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_ADD_LOCALE')); 
            System.currentPageReference().getParameters().put('LineItemId',newLine.Id);
            //ApexPages.currentPage().getParameters().put('LineItemId',newLine.Id);
            CARPOL_UNI_ADD_LOCALE obj = new CARPOL_UNI_ADD_LOCALE();
            obj.CancelTransitLocal();
            obj.SaveTransitLocal();
            obj.Add();
            system.assert(obj != null);
            //obj.addMore();
        test.stopTest();
    }   
}