@isTest(seealldata=true)
private class CARPOL_AC_WorkflowButtonsControllerTest {
    static testMethod void CARPOL_AC_WorkflowButtonsControllerTest() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Application__c objapp = testData.newapplication();
      AC__c ac1 = testData.newlineitem('Personal Use',objapp);

      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac1.Authorization__c = objauth.id;
      update ac1;
      objauth.Authorization_Type__c = 'Permit';
      objauth.Date_Issued__c = Date.today();   
      update objauth;
      Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
      
      Test.startTest();
      PageReference pageRef = Page.WFTaskButtons;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(wft);
      
      ApexPages.currentPage().getParameters().put('Id',wft.id);
      CARPOL_AC_WorkflowButtonsController extclass = new CARPOL_AC_WorkflowButtonsController(sc);
      System.assert(extclass != null);
      Test.stopTest();
      }
}