public with sharing class CARPOL_WizardLookupPicklistValues2 {

    public Wizard_Selections__c objpicklst {get;set;} // new Pick list value to create
  public List<Wizard_Selections__c> lstpicklst {get;set;} // search results
  public string searchString {get;set;} // search keyword

  public CARPOL_WizardLookupPicklistValues2() {
  
  lstInner2 = new List<innerpicklst>();     
       addMoreRows2(); 
       selectedRowIndex2 = '0';  
       
    objpicklst = new Wizard_Selections__c();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
    runSearch();  
  }

  // performs the keyword search
  public PageReference search() {
    runSearch();
    return null;
  }

  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
    lstpicklst = performSearch(searchString);               
  } 

  // run the search and return the records found. 
  private List<Wizard_Selections__c> performSearch(string searchString) {
    //DMS - 5-18-2016 rewritten to avoid SOQl Injection issues
    List<Wizard_Selections__c> results = new List<Wizard_Selections__c>();
    if(searchString != '' && searchString != null){
        searchString = '%' + searchString + '%';
        results = [select id, name, Value__c from Wizard_Selections__c where name Like :searchString limit 25];
    }
    /*    String soql = 'select id, name,Value__c from Wizard_Selections__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 
    */
    return results;
  }

  // save the new account record
  public PageReference save() {
      list<Wizard_Selections__c> piklst = new list<Wizard_Selections__c>();
      for(Integer j = 0;j<lstInner2.size();j++){
            // lstInner[j].wq.RecordTypeid = '012r000000005ry';
            piklst.add(lstInner2[j].picklstobj); 
 
        } 
    insert piklst ;
    lstInner2 = new list<innerpicklst>();
    piklst = new list<Wizard_Selections__c>();
    addMoreRows2(); 
       selectedRowIndex2 = '0'; 
       count2 = 1;
    // reset the account
    objpicklst = new Wizard_Selections__c();
    return null;
  }

  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }

  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }

    public List<innerpicklst> lstInner2{get;set;}
 
    public String selectedRowIndex2{get;set;}  
 
    public Integer count2 = 1;
     public void Add2(){   
 
        count2 = count2+1;
 
        addMoreRows2();      
 
    }
    public void addMoreRows2(){      
 
      innerpicklst objInnerClass2 = new innerpicklst(count2);
 
      lstInner2.add(objInnerClass2);          
 
    }
 public void Del2(){      
 
      lstInner2.remove(Integer.valueOf(selectedRowIndex2)-1);
 
      count2 = count2 - 1;        
 
    }
    public class innerpicklst{               
 
        public String recCount2{get;set;}                 
 
        public Wizard_Selections__c picklstobj {get;set;}
 
        public innerpicklst(Integer intCount ){
 
            recCount2 = String.valueOf(intCount);                                
 
            picklstobj = new Wizard_Selections__c();        
 
        }
 
    }
    
    //http://www.tehnrd.com/visualforce-pop-up/
    //http://www.salesforcegeneral.com/salesforce-modal-dialog-box/
    //http://www.infallibletechie.com/2013/07/how-to-create-popup-by-clicking-custom.html
 
}