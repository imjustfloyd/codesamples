@isTest(seealldata=true)
private class CARPOL_AddPortsToGroup_Test {
      @IsTest static void CARPOL_AddPortsToGroup_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String PortsGrpRecordTypeId = Schema.SObjectType.Group__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();                    
          String PortsFacJuncRecordTypeId = Schema.SObjectType.Group_Junction__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();          
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          
          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
          ac1.Authorization__c = objauth.id;
          update ac1;       
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          ac2.Authorization__c = objauth.id;
          update ac2;                 
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);              
          ac3.Authorization__c = objauth.id;
          update ac3;                 
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           

          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 

		  Group__c objGroup = testData.newGroup();
          /*Group__c objgroup= new Group__c();
          objgroup.Name = 'Test';
          objgroup.Group_Custom_Name__c = 'Test';
          objgroup.Status__c = 'Active';
          insert objgroup;       
          //ports is not an active group so this code will not work
          Group_Junction__c grpjunc = new Group_Junction__c();
          grpjunc.Facility__c = fac.id;
          grpjunc.Port_Group__c = objGroup.id;
          grpjunc.Regulation__c = objreg1.Id;
          
          grpjunc.RecordTypeId = PortsFacJuncRecordTypeId;
          insert grpjunc;*/
          
          Test.startTest();
          PageReference pageRef = Page.CARPOL_AddPortsToGroup;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objGroup);
          ApexPages.currentPage().getParameters().put('Id',objGroup.id);
          CARPOL_AddPortsToGroup extclass = new CARPOL_AddPortsToGroup(sc);
          
          extclass.getPorts();
          extclass.updatePortGroup();
          system.assert(extclass != null);
          Test.stopTest();     
      }
}