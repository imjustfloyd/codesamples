public with sharing class CARPOL_Standard_Formal_Letter {
    
    public ID appId {get; set;}
    public Application__c application { get; set; }
    public Authorizations__c auth {get; set;}
    public Signature__c tp {get; set;}
    public Ac__c lineitem {get; set;}
    public CARPOL_Standard_Formal_Letter(ApexPages.StandardController controller) {
        appId = ApexPages.currentPage().getParameters().get('id');
        
        if (appId == null){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No application found. Details cannot be filled.'));  
        }
        else {
        
        application = [SELECT ID, Name,
        Applicant_Name__r.FirstName,
        Applicant_Name__r.LastName,
        Applicant_Name__r.MailingStreet, 
        Applicant_Name__r.MailingCity, 
        Applicant_Name__r.MailingState, 
        Applicant_Name__r.MailingCountry, 
        Applicant_Name__r.MailingPostalCode 
        FROM Application__c WHERE ID =:appId LIMIT 1];
        
        auth = [SELECT ID, Name, Application__c, Thumbprint__c FROM Authorizations__c WHERE Application__c =:appId LIMIT 1];
        
        lineitem = [SELECT ID, RA_Scientific_Name__r.name from ac__c where Authorization__c = :auth.id limit 1]; //7-25-16 VV Added for Sciname
        
        tp = [SELECT ID, Name,
        Communication_Manager__c, 
        Communication_Manager__r.Name,
        Line_Record_Type__c,
        Intended_Use__r.Name,
        To_Level_1_Region__r.Name,
        REF_Program_Name__c,
        Port__r.Name,
        Communication_Manager__r.Content__c,
        Regulated_Article__r.Name,
        From_Country__r.name 
        FROM Signature__c WHERE ID = :auth.Thumbprint__c LIMIT 1];
        
        if(auth.Thumbprint__c != null)
        {
        //Replacing Variables
            if(tp.Line_Record_Type__c!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLineRecordType}', tp.Line_Record_Type__c);
            }
            if(tp.Intended_Use__r.Name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPurposeOfImportation}', tp.Intended_Use__r.Name);
            }
            if(tp.To_Level_1_Region__r.Name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', tp.To_Level_1_Region__r.Name);
            }
            if(tp.REF_Program_Name__c!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpProgramName}', tp.REF_Program_Name__c);
            }
            if(tp.Port__r.Name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPorts}', tp.Port__r.Name);
            }
            if(tp.Regulated_Article__r.name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRegulatedArticle}', tp.Regulated_Article__r.Name);
            }
             if(tp.From_Country__r.name !=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpFromCountry}', tp.From_Country__r.Name );
            }
          //VV added 7-26-16
            if(lineitem.RA_Scientific_Name__r.name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', lineitem.RA_Scientific_Name__r.name );
            }
            
        
        }
   
    }
}
}