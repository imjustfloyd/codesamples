public class CARPOL_BRS_PreviewAndAttachPDFExtension {
    
    public ID AuthorizationID;
    public String baseURL { get; set; }
    public Authorizations__c auth{get;set;}
    list<Applicant_Attachments__c> AppAttachList {get;set;}
    
    public CARPOL_BRS_PreviewAndAttachPDFExtension(ApexPages.StandardController stdController){
        AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
        auth = [SELECT Id, Name, RecordTypeId, RecordType.Name FROM Authorizations__c  WHERE Id =: AuthorizationID ];
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('AuthorizationID ###'+AuthorizationID);
    }
    
 
    public pagereference AttachPermit(){
        
      
    if(auth.RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit'){
     Attachment att = new Attachment(name ='CourtesyPermit_'+ System.TODAY().format()+ '.pdf');
     PageReference permitPage = Page.CARPOL_BRS_CourtesyPermit;
     permitPage.getParameters().put('id',AuthorizationID);
     att.body = permitPage.getContent();
     att.parentid = AuthorizationID;
     insert att;
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Courtesy Permit attached successfully.'));
     }
   
   if(auth.RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit'){
     AppAttachList = [SELECT Attachment_Type__c,Authorization__c,Document_Types__c,Id 
                                       FROM Applicant_Attachments__c where Authorization__c =:AuthorizationID and Attachment_Type__c = 'Permit Package'];
     ID tmpRecordType = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Permit Package').getRecordTypeId();                                  
     if(AppAttachList.size()<1){
        
        Applicant_Attachments__c appAttach = new Applicant_Attachments__c();
        appAttach.Attachment_Type__c = 'Permit Package';
        appAttach.RecordTypeID = tmpRecordType;
        appAttach.Document_Types__c = 'Permit Document';
        appAttach.Authorization__c = AuthorizationID;
        insert appAttach;
        system.debug('appAttach ##'+appAttach);
        Attachment att = new Attachment(name ='StandardPermit_'+ System.TODAY().format()+ '.pdf');
        system.debug('att ##'+att);
        PageReference permitPage = Page.CARPOL_BRS_StandardPermit;
        system.debug('permitPage ##'+ permitPage);
        system.debug('AuthorizationID ###'+AuthorizationID);
        permitPage.getParameters().put('id',AuthorizationID);
        att.body = permitPage.getContent();
        att.parentid = appAttach.Id;
        insert att;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Standard Permit attached successfully.'));
      }
      else{
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Permit already exists for this Authorization,please check Permit Package for Standard Permit Document '));
      }

     }
     return null ;
    
    }

}