@isTest(seealldata=false)
private class Portal_genotype_edit_Test {
      @IsTest static void Portal_genotype_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Genotype__c geno = new Genotype__c();
          geno.Related_Construct_Record_Number__c = objcaj.id;
          geno.Construct_Component_Name__c = 'test';
          geno.Donor__c = 'test';
          insert geno;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.Portal_genotype_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new Genotype__c());
              
              Portal_genotype_edit extclass1 = new Portal_genotype_edit(sc);              
              extclass1.cancelgenotype();                             

              ApexPages.currentPage().getParameters().put('genid',geno.id);
              ApexPages.currentPage().getParameters().put('constid',objcaj.id); 
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(new Genotype__c());                                         
              Portal_genotype_edit extclass = new Portal_genotype_edit(sc1);
              extclass.updategentype();   
              extclass.cancelgenotype(); 
              extclass.genid = geno.id;
              System.assert(extclass != null);                      

          Test.stopTest();   
      }
}