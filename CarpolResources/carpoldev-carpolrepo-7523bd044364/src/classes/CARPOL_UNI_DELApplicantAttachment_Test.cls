@isTest(seealldata=false)
private class CARPOL_UNI_DELApplicantAttachment_Test {
      @IsTest static void CARPOL_UNI_DELApplicantAttachment_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
		  
		  List<Applicant_Attachments__c> lstAtt = new List<Applicant_Attachments__c>();
		  for(Integer i=0; i<5; i++){
		  	Applicant_Attachments__c objaa = testData.newAttach(objac.id);
		  	lstAtt.add(objaa);
		  }
		  	
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          CARPOL_UNI_DELApplicantAttachment extclass = new CARPOL_UNI_DELApplicantAttachment(lstAtt);
          extclass.ApplicantAttachments();     
          system.assert(extclass != null);                   
          Test.stopTest();   
      }
}