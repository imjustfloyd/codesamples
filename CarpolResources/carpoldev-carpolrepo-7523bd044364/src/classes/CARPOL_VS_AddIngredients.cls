/* 
// Author : Vinar Amrutia
// Date Created : 04/09/2015
// Purpose : Ability to Add/Edit/Delete Related Ingredients to VS Line Item
*/
public with sharing class CARPOL_VS_AddIngredients {
    
    public Product_Regulated_Article_Junction__c pRAjunction { get; set; }
    public List<Product_Regulated_Article_Junction__c> allPRAjunction { get; set; }
    public String selectedRowIndex { get; set; }
    public Integer totalIngredients { get; set; }
    public Map<String, Integer> ingredientsMap = new Map<String, Integer>();
    public String mapKeyValue = '';
    public ID VSLineItemID;
    public Integer ingredientCount = 0;
        
    public CARPOL_VS_AddIngredients(ApexPages.StandardController controller) {
        
        System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        VSLineItemID = ApexPages.currentPage().getParameters().get('id');
        allPRAjunction = new List<Product_Regulated_Article_Junction__c>();
        selectedRowIndex = '0';
        allPRAjunction = [SELECT ID, Name, Species__c, Ingredient__c, Ingredient_Country_of_Origin__c FROM Product_Regulated_Article_Junction__c WHERE VS_Line_Item__c = :VSLineItemID];
        
        for(Product_Regulated_Article_Junction__c praj : allPRAjunction)
        {
            mapKeyValue = praj.Ingredient_Country_of_Origin__c + '_' + praj.Ingredient__c + '_' + praj.Species__c;
            ingredientsMap.put(mapKeyValue, ingredientCount);
            ingredientCount += 1;
        }
        pRAjunction = new Product_Regulated_Article_Junction__c();
        allPRAjunction.add(pRAjunction);
        totalIngredients = allPRAjunction.size();
        System.Debug('<<<<<<< Total Ingredients : ' + totalIngredients + ' >>>>>>>');
    }
    
    public PageReference saveIngredients()
    {
        try
        {
            System.Debug('<<<<<<< Total Ingredients : ' + allPRAjunction.size() + ' >>>>>>>');
            Boolean showSuccess = true;
            
            for(Product_Regulated_Article_Junction__c praj  : allPRAjunction)
            {
                praj.VS_Line_Item__c = VSLineItemID;
                mapKeyValue = praj.Ingredient_Country_of_Origin__c + '_' + praj.Ingredient__c + '_' + praj.Species__c;
                if(ingredientsMap.get(mapKeyValue) == null)
                {
                    ingredientsMap.put(mapKeyValue, ingredientCount);
                    ingredientCount += 1;
                }
            }
            
            System.Debug('<<<<<<< Map Size : ' + ingredientsMap.size() + ' >>>>>>>');
            
            //Checking if List size and Map size are same else deleting the unwanted entries
            if(ingredientsMap.size() > allPRAjunction.size())
            {
                ingredientsMap.remove('null_null_null');
            }
            
            Integer index; 
            for(String key : ingredientsMap.keySet())
            {
                System.Debug('<<<<<<< Key : ' + key + ' >>>>>>>');
                if(!key.contains('null'))
                {
                    /*index = ingredientsMap.get(key);
                    System.Debug('<<<<<<< Index : ' + index + ' >>>>>>>');
                     if(index != null)
                    {
                        allPRAjunction[index].VS_Line_Item__c = VSLineItemID;
                        upsert allPRAjunction[index];
                    }
                    */
                }
                else
                {
                    showSuccess = false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Country of Origin, Ingredient and Species to save.'));
                }
            }
            if(showSuccess)
            {
                upsert allPRAjunction;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Ingredients Saved Successfuly.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
        }
        return null;
    }
    
    public PageReference addRow()
    {
        System.Debug('<<<<<<< Total Ingredients at start : ' + allPRAjunction.size() + ' >>>>>>>');
        totalIngredients = allPRAjunction.size();
        if(totalIngredients < 75)
        {
            Product_Regulated_Article_Junction__c pRAjunction = new Product_Regulated_Article_Junction__c(VS_Line_Item__c = VSLineItemID);
            //pRAjunction.VS_Line_Item__c = VSLineItemID;
            allPRAjunction.add(pRAjunction);
            totalIngredients = allPRAjunction.size();
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can only add 75 ingredients at a time. Please save the current ingredients and repeat the process.'));
        }
        System.Debug('<<<<<<< Total Ingredients : ' + totalIngredients + ' >>>>>>>');
        return null;
    }
    
    public PageReference removeRow()
    {
        System.Debug('<<<<<<< Selected Row Index : ' + selectedRowIndex + ' >>>>>>>');
        try
        {
            Integer index = allPRAjunction.size();
            if(index > 1)
            {
                List<Product_Regulated_Article_Junction__c> delIngredient = [SELECT ID, Ingredient_Country_of_Origin__c, Ingredient__c, Species__c FROM Product_Regulated_Article_Junction__c WHERE VS_Line_Item__c = :VSLineItemID AND Species__c = :allPRAjunction[Integer.valueOf(selectedRowIndex)].Species__c AND Ingredient__c = :allPRAjunction[Integer.valueOf(selectedRowIndex)].Ingredient__c AND Ingredient_Country_of_Origin__c = :allPRAjunction[Integer.valueOf(selectedRowIndex)].Ingredient_Country_of_Origin__c LIMIT 1];
                if(delIngredient.size() > 0)
                {
                    mapKeyValue = delIngredient[0].Ingredient_Country_of_Origin__c + '_' + delIngredient[0].Ingredient__c + '_' + delIngredient[0].Species__c;
                    ingredientsMap.remove(mapKeyValue);
                    delete(delIngredient[0]);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Ingredient removed successfuly.'));
                }
                else
                {
                    ingredientsMap.remove('null_null_null');
                }  
                allPRAjunction.remove(Integer.valueOf(selectedRowIndex));
                totalIngredients = allPRAjunction.size();
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cannot remove the last row.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
        }
        return null;
    }
}