@isTest
private class Portal_Contact_Edit_Controller_Test
{
  static testMethod void Portal_Contact_Edit_Controller_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          Applicant_Contact__c objacc = testData.newappcontact();
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;   

              ApexPages.StandardController stdControllerNo = new ApexPages.StandardController(objcont);
              Portal_Contact_Edit_Controller objAssNo = new Portal_Contact_Edit_Controller(stdControllerNo);
              objAssNo.cont = objcont;
              objAssNo.Save();

              ApexPages.currentPage().getParameters().put('id',objcont.id);                                                                                                             
              ApexPages.StandardController stdController = new ApexPages.StandardController(objcont);
              Portal_Contact_Edit_Controller objAss = new Portal_Contact_Edit_Controller(stdController);
              objAss.Save();
              objAss.getEditPage();
              objAss.controller = new ApexPages.StandardController(objcont);
              System.assert(objAss != null);        
  
  }
}