@isTest(seealldata=false)
private class CARPOL_AC_WizardTest {
    static testMethod void testCARPOL_AC_Wizard() {
    
        String ACWQRecordTypeId = Schema.SObjectType.Wizard_Questionnaire__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        Wizard_Questionnaire__c objwq = new Wizard_Questionnaire__c();
        objwq.RecordTypeid = ACWQRecordTypeId;
        objwq.Type__c = 'NO PERMIT REQUIRED';
        objwq.Type_of_Options__c = 'RADIO BUTTONS';        
        insert objwq;
        Wizard_Questionnaire__c objwq1 = new Wizard_Questionnaire__c();
        objwq1.RecordTypeid = ACWQRecordTypeId;
        objwq1.Type__c = 'START';
        objwq1.Type_of_Options__c = 'PICKLIST';
        insert objwq1; 
        
        Wizard_Questionnaire__c objwq2 = new Wizard_Questionnaire__c();
        objwq2.RecordTypeid = ACWQRecordTypeId;
        objwq2.Type__c = 'PERMITTED';
        objwq2.Type_of_Options__c = 'RADIO BUTTONS';
        insert objwq2;                
        
        Wizard_Selections__c wzsel1 = new Wizard_Selections__c();
        wzsel1.Wizard_Questionnaire__c = objwq.id;
        wzsel1.Value__c = 'Test 1';        
        insert wzsel1;
        
        Wizard_Selections__c wzsel2 = new Wizard_Selections__c();
        wzsel2.Wizard_Questionnaire__c = objwq1.id;
        wzsel2.Value__c = 'Test 2';
        insert wzsel2;
        
        Wizard_Selections__c wzsel3 = new Wizard_Selections__c();
        wzsel3.Wizard_Questionnaire__c = objwq2.id;
        wzsel3.Value__c = 'Test 3';
        insert wzsel3;   
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
             
        Signature__c newsig = testData.newThumbprint();
        Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
        Regulation__c newreg2 = testData.newRegulation('Additional Information','Test');        
        Signature_Regulation_Junction__c srj1 = testData.newSRJ(newsig.id,newreg1.id);
        Signature_Regulation_Junction__c srj2 = testData.newSRJ(newsig.id,newreg2.id);        
        
        CARPOL_AC_Wizard acwizard = new CARPOL_AC_Wizard();
        //acwizard.questionID = objwq.id;
        acwizard.startAgain();        

        acwizard.getRegulations(newsig.id);
        acwizard.getPickValSelectOptions();
        List<SelectOption> selList = acwizard.selValSelectOptions;

        acwizard.selectedAnswerID = selList[0].getvalue();
        acwizard.next();

        acwizard.selectedAnswerID = selList[0].getvalue();
        acwizard.next();
        acwizard.selectedAnswerTraceQuestionID = 'null';  

        acwizard.traceBack();
        acwizard.getLetterInformal();
        acwizard.getLetterNJFormal(); 
        acwizard.getLODFormal();
        acwizard.getNPRFormal();
        acwizard.getLetterFormal();
        acwizard.login();
        system.assert(acwizard != null);

    }
   }