@isTest
private class CARPOL_updateSignatureFullNameTest{
    /* Kishore
private static testMethod void CARPOL_updateSignatureFullNameTrigger(){

Regulated_Article__c ra = new Regulated_Article__c(
Name = 'TestRAJialin20150121'
);
insert ra;

Trade_Agreement__c ta = new Trade_Agreement__c(
Name = 'TestTAJialin20150121',
Trade_Agreement_Code__c = '01'
);
insert ta;

Country__c c1 = new Country__c(
Name = 'TestC1',
Country_Code__c = '02',
Trade_Agreement__c = ta.Id
);
insert c1;

Country__c c2 = new Country__c(
Name = 'TestC2',
Country_Code__c = '22',
Trade_Agreement__c = ta.Id
);
insert c2;

Group__c gr = new Group__c(
Name='Testgr');
insert gr;

Signature_Condition_Mapping__c scm = new Signature_Condition_Mapping__c(
Name = 'TestJialin20150121',
Unique_Signature__c = 'TestJialin20150121',
Primary_Regulated_Article__c = ra.Id,
From_Country__c = c1.Id,
To_Country__c = c2.Id,
Group__c = gr.Id,
Answer_1__c = 'T1',
Answer_2__c = 'T2',
Answer_3__c = 'T3',
Answer_4__c = 'T4',
Answer_5__c = 'T5',
Answer_6__c = 'T6',
Answer_7__c = 'T7',
Answer_8__c = 'T8',
Answer_9__c = 'T9',
Answer_10__c = 'T10',
Answer_20__c = 'T20'
);
insert scm;

Test.startTest();

scm=[Select Id, Name, Full_Signature__c, Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c, Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c From Signature_Condition_Mapping__c Where Id =:scm.Id ];

System.assertEquals(scm.Full_Signature__c, 'T1_T2_T3_T4_T5_T6_T7_T8_T9_T10_T20');
System.assertEquals(scm.Name,'T1_T2_T3_T4_T5_T6_T7_T8_T9_T10_T20');

scm.Answer_2__c = '';
scm.Answer_4__c = '';
scm.Answer_6__c = '';
scm.Answer_8__c = '';
scm.Answer_10__c = '';

update scm;

scm=[Select Id, Name, Full_Signature__c, Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c, Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c From Signature_Condition_Mapping__c Where Id =:scm.Id ];

System.assertEquals(scm.Full_Signature__c, 'T1_T3_T5_T7_T9_T20');
System.assertEquals(scm.Name,'T1_T3_T5_T7_T9_T20');

scm.Answer_1__c = '';
scm.Answer_2__c = '';
scm.Answer_3__c = '';
scm.Answer_4__c = '';
scm.Answer_5__c = '';
scm.Answer_6__c = '';
scm.Answer_7__c = '';
scm.Answer_8__c = '';
scm.Answer_9__c = '';
scm.Answer_10__c = '';
scm.Answer_20__c = '';

update scm;

scm=[Select Id, Name, Full_Signature__c, Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c, Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c From Signature_Condition_Mapping__c Where Id =:scm.Id ];

System.assertEquals(scm.Full_Signature__c, 'T1_T3_T5_T7_T9_T20');
System.assertEquals(scm.Name,'T1_T3_T5_T7_T9_T20');

Test.stopTest();
}
*/
}