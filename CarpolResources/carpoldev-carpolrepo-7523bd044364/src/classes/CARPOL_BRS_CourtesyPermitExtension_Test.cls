@isTest(seealldata=false)
private class CARPOL_BRS_CourtesyPermitExtension_Test {
      @IsTest static void CARPOL_BRS_CourtesyPermitExtension_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACLocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();                    
          String ACLoc1RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();                              
          String ACLoc2RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();                                        
          String ACLoc3RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();                                                                      
          

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        

          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          update ac1;
          
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = ac1.id;
          objcaj.Status__c = 'Waiting on Customer';
          insert objcaj;   
                    
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          lr.Level_1_Name__c = 'State';
          lr.State_Name__c = 'Test';
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          
          List<Location__c> listloc = new List<Location__c>();
          
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLocRecordTypeId
             );
            listloc.add(objloc); 
            
            Location__c objloc1 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc1RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc1);      
            
            Location__c objloc2 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc2RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc2);    
            
            //this record type has an error in the code
          /* Location__c objloc3 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc3RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc3); */
            
           Location__c objloc4 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLocRecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc4);
            insert listloc; 
            
            //Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
            //Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
            
            //can't insert this record without timing out the work
            /*Link_Regulated_Articles__c objlra = new Link_Regulated_Articles__c();
            //objlra.Regulated_Article__c = regart.id;  
            objlra.Line_Item__c = ac1.id;
            objlra.Authorization__c = objauth.id;
            objlra.Application__c = objapp.id;
            objlra.Cultivar_and_or_Breeding_Line__c = 'Test';         
            insert objlra;*/

          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_CourtesyPermit;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('Id',objauth.id);
          ApexPages.currentPage().getParameters().put('CBI','Yes');          
          CARPOL_BRS_CourtesyPermitExtension extclass = new CARPOL_BRS_CourtesyPermitExtension(sc);
          //getcontent fix coming
          extclass.generateAuthorization_PDF();
          system.assert(extclass != null);
          Test.stopTest();     
      }
}