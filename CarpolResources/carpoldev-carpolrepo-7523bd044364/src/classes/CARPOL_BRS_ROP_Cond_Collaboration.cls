public with sharing class CARPOL_BRS_ROP_Cond_Collaboration {
    
     public String AuthJuncRecordTypeId {get;set;}
     public Id authID{get;set;}
     public Id Id{get;set;}
     public Boolean biotechrole {get;set;}
     public Boolean managerandaboverole {get;set;}
     public Boolean ropreviwersrole {get; set;}
     public string options { get; set; }
     public List<Authorization_Junction__c> AssociatedRegulations;
     public List<Authorization_Junction__c> Regulations;
     public List<Authorization_Junction__c> AddInformation;
     public List<Authorization_Junction__c> ApprovedSuppConditions;
     public List<Authorization_Junction__c> OriginalRegulations = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalAddInformation = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalApprovedSuppConditions = new List<Authorization_Junction__c>(); 
     String roleId = UserInfo.getUserRoleId();
     public id wfid {get;set;}
     public string wfname {get;set;}
     public string authname {get;set;}

     
    public CARPOL_BRS_ROP_Cond_Collaboration(ApexPages.StandardSetController controller) {
         options= 'Collaborated';
        // authID=ApexPages.currentPage().getParameters().get('id');
        if(ApexPages.currentPage().getParameters().get('id') != null){
           authID = ApexPages.currentPage().getParameters().get('id');
           authname = [select name from Authorizations__c where id =:authID ].name;
         }
         if(ApexPages.currentPage().getParameters().get('wfid') != null){
           wfid = ApexPages.currentPage().getParameters().get('wfid');
           wfname = [select name from Workflow_Task__c where id =:wfid ].name;
         }
         Regulations = new List<Authorization_Junction__c>();
         AddInformation = new List<Authorization_Junction__c>();
         ApprovedSuppConditions = new List<Authorization_Junction__c>();        
         AuthJuncRecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();
            UserRole usrRole = [SELECT Name, Id FROM UserRole 
                                WHERE Id = :roleId LIMIT 1];
            String userRoleName = usrRole.Name;
            
 

        if(userRoleName == 'BRS Biotechnologist' || userRoleName == 'BRS Program Specialist' || userRoleName == 'ADMIN' || userRoleName == 'Developer' || userRoleName == 'USDA'){
                biotechrole = true;options= 'All';}
        else if(userRoleName == 'BRAP Manager' || userRoleName == 'BRAP Director'  || userRoleName == 'ADMIN' || userRoleName == 'Developer' || userRoleName == 'USDA')
                managerandaboverole = true;
        else if(userRoleName == 'ROP Director' || userRoleName == 'ROP CEEB Manager' || userRoleName == 'ROP Eastern Compliance Manager' || userRoleName == 'ROP Reviewer' || userRoleName == 'ROP Western Compliance Manager')
                ropreviwersrole = true;   
       
        system.debug('userRoleName###'+userRoleName);
    
    }     
     
    public ApexPages.StandardSetController ConditionRecords{
        get { system.debug('ConditionRecords intial##'+ConditionRecords);system.debug('options initial##'+options);
            if(ConditionRecords == null && options != 'Save') {
                if(options == 'Collaborated' && biotechrole == true){
                system.debug('options1##'+options);    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and (Needed_ROP_Collaboration__c =: 'Yes' or Needed_BRS_Manager_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ]));  
                                                        
            }else if(options == 'All'){
                system.debug('options non##'+options);    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ((Needed_ROP_Collaboration__c =: 'Yes' or Needed_ROP_Collaboration__c =: null )
                                                         or (Needed_BRS_Manager_Collaboration__c =: 'Yes'  or Needed_BRS_Manager_Collaboration__c =: null))
                                                        and Authorization__c =: authID order by Order_Number__c ]));  
                                                        
            }else if(options == 'Collaborated' && managerandaboverole == true){
                    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ( Needed_BRS_Manager_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ])); 
            }else if(options == 'Collaborated' && ropreviwersrole == true){
                    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ( Needed_ROP_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ]));  }
                                                        
          } 
            system.debug('ConditionRecords@@@3'+ConditionRecords);
            return ConditionRecords;
        }
        private set;
    }
    

    
    public List<Authorization_Junction__c> getAddInformation(){
        system.debug('options3###'+options); 
        return (List<Authorization_Junction__c>)ConditionRecords.getRecords();
    }
    
        public List<Authorization_Junction__c> getRegulations(){
        
         Regulations = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                    BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Standard'
                                                        and Authorization__c =: authID order by Order_Number__c];
         OriginalRegulations =  Regulations.deepClone();
         return Regulations;
       }
    
    public Pagereference save(){
        ConditionRecords.save();
        return null;
    }
    
        public Pagereference savesuppconditions(){
                system.debug('ConditionRecords@@@1'+ConditionRecords);
                options = 'Save';
                ConditionRecords.save();
                system.debug('ConditionRecords@@@2'+ConditionRecords);
                return null;
    }
    public List<Authorization_Junction__c> SObjectListToShow { get; set; }

         public void getFilteredConditions(){
             system.debug('options4###'+options);
              if(options == 'Collaborated' && biotechrole == true){
                system.debug('options6##'+options);    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and (Needed_ROP_Collaboration__c =: 'Yes' or Needed_BRS_Manager_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ]));  
                                                        
            }else if(options == 'All'){
                system.debug('options non##'+options);    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ((Needed_ROP_Collaboration__c =: 'Yes' or Needed_ROP_Collaboration__c =: null )
                                                         or (Needed_BRS_Manager_Collaboration__c =: 'Yes'  or Needed_BRS_Manager_Collaboration__c =: null))
                                                        and Authorization__c =: authID order by Order_Number__c ]));  
                                                        
            }else if(options == 'Collaborated' && managerandaboverole == true){
                    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ( Needed_BRS_Manager_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ])); 
            }else if(options == 'Collaborated' && ropreviwersrole == true){
                    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ( Needed_ROP_Collaboration__c =: 'Yes')
                                                        and Authorization__c =: authID order by Order_Number__c ]));  }             
       /*   if(options == 'All') {
                system.debug('options ALL##'+options);    
                ConditionRecords = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Needed_ROP_Collaboration__c,Needed_BRS_Manager_Collaboration__c, 
                                                                                                  BRS_Manager_Agree__c,BRS_Manager_Comments__c,ROP_Agree__c,ROP_Comments__c  
                                                        FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Supplemental Conditions'
                                                        and ((Needed_ROP_Collaboration__c =: 'Yes' or Needed_ROP_Collaboration__c =: null )
                                                         or (Needed_BRS_Manager_Collaboration__c =: 'Yes'  or Needed_BRS_Manager_Collaboration__c =: null))
                                                        and Authorization__c =: authID order by Order_Number__c ]));  system.debug('ConditionRecords@@@ALL@@'+ConditionRecords);}
            else{ SObjectListToShow = getAddInformation();}*/
       
       }  
       
         public PageReference redirect(){

            PageReference dirpage= new PageReference('/'+wfid);
            dirpage.setRedirect(true);
            return dirpage;
        }   
        
      public PageReference redirectPermitPage(){

            PageReference dirpage= new PageReference('/apex/CARPOL_BRS_EPRegulations?id=' + authID+'&wfid='+wfid);
            dirpage.setRedirect(true);
            return dirpage;
        }

}