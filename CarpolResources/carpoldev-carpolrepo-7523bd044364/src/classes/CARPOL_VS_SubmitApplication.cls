public class CARPOL_VS_SubmitApplication {

    public static void onAfterUpdate(List<Application__c> appList){
          VSSubmitApplication(appList);
    }
    
    private static void VSSubmitApplication(List<Application__c> appList){
        List<AC__c> vsLTList = [SELECT ID, Name, Status__c, Program_Line_Item_Pathway__r.Program__r.Name,Authorization_Group_String__c, Authorization__c,Program_Line_Item_Pathway__r.name // added Program_Line_Item_Pathway__r.name by kishore
                                FROM AC__c 
                                WHERE Application_Number__c IN :appList 
                                AND (Program_Line_Item_Pathway__r.Program__r.Name = 'VS' 
                                OR Program_Line_Item_Pathway__r.Program__r.Name = 'PPQ'
                                OR Program_Line_Item_Pathway__r.Program__r.Name = 'AC' 
                                OR Program_Line_Item_Pathway__r.Program__r.Name = 'BRS'
                                )];
                                
        System.Debug('<<<<<<< vsLTList ' + vsLTList+ ' >>>>>>>');                        
        Map<String, String> authGroupLTMap = new Map<String, String>();
        
        //ID authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        String authRTId = '';
        if(vsLTList != null && vsLTList.size()>0){
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'VS'){        
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
            }
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'PPQ'){
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
            }
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'AC'){
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
            }//start of changes kishore
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'BRS' && vsLTList[0].Program_Line_Item_Pathway__r.name == 'GE Non-Plant Authorizations'){
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
            }
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'BRS' && vsLTList[0].Program_Line_Item_Pathway__r.name == 'GE Plant Authorizations'){
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId();
            }
            if (vsLTList[0].Program_Line_Item_Pathway__r.Program__r.Name == 'BRS' && vsLTList[0].Program_Line_Item_Pathway__r.name == 'GE Courtesy Authorizations'){
                authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
            }            
            // end of changes kishore            
            
        }else{
            authRTId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
           
        }


        System.Debug('<<<<<<< authRTId ' + authRTId+ ' >>>>>>>'); 
        
        List<Authorizations__c> authToInsertList = new List<Authorizations__c>();
        
        List<String> tempstrlist = new List<String>();
        String tempString ='';
        
        for(Integer i = 0; i < vsLTList.size(); i++){
        
        
        String idstring = Id.valueOf(vsLTList[i].Id);
                   
            authGroupLTMap.put(vsLTList[i].Authorization_Group_String__c, idstring);
            
            System.Debug('<<<<<<< authGroupLTMap: ' + authGroupLTMap + ' >>>>>>>');
            
         //tempString = authGroupLTMap.get(_a0g350000000KeFAAU_a0335000001hMXaAAM_a0335000001hMXXAA2);
            
             System.Debug('<<<<<<< 1030india!!!!!!!!!!!!!!!!!!: ' + vsLTList[i].Authorization_Group_String__c + ' >>>>>>>'); 
             
            tempString = authGroupLTMap.get(vsLTList[i].Authorization_Group_String__c);
            tempstrlist.add(tempString);

            tempString +=','+vsLTList[i].Id;
            
            System.Debug('<<<<<<< tempString value: ' + tempString + ' >>>>>>>');
            
            
        }
        System.Debug('<<<<<<< final tempString value: ' + tempString + ' >>>>>>>');
        System.Debug('<<<<<<< tempstrlist: ' + tempstrlist + ' >>>>>>>');
        
        
        
        /*
        
        String[] results = tempString.split(',');
        System.debug('<<<<<<<<<<<<<<< results size ' + results.size() + '>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<< results ' + results + '>>>>>>>>>>>>>>>'); 
        */
        
        
                 
        Set <Id> vsLTGroupSet = new Set<Id>();
        
        System.debug('<<<<<<<<<<<<<<< new set start'+ vsLTGroupSet +'>>>>>>>>>>>>>>>'); 
         
        for(Integer i = 0; i <tempstrlist.size(); i++){
        
        System.debug('<<<<<<<<<<<<<<< tempstrlist[i]'+ tempstrlist[i]+'>>>>>>>>>>>>>>>'); 
         
            vsLTGroupSet.add(tempstrlist[i]);
        }
        System.debug('<<<<<<<<<<<<<<< vsLTGroupSet size ' + vsLTGroupSet.size() + '>>>>>>>>>>>>>>>'); 
        System.debug('<<<<<<<<<<<<<<< vsLTGroupSet: ' + vsLTGroupSet + '>>>>>>>>>>>>>>>'); 
        
        
        List<AC__c> vsLTGroupList = [SELECT ID, Name, Status__c, Authorization__c, Authorization_Group_String__c, Program_Line_Item_Pathway__c FROM AC__c WHERE ID IN: vsLTGroupSet];
        
        
        System.debug('<<<<<<<<<<<<<<< vsLTGroupList: ' + vsLTGroupList + '>>>>>>>>>>>>>>>'); 
        System.debug('<<<<<<<<<<<<<<< vsLTGroupList.size : ' + vsLTGroupList.size() + '>>>>>>>>>>>>>>>'); 
        
        
        List<AC__c> LTToUpdateList = new List<AC__c>();
        List <Regulations_Association_Matrix__c>  decisionsList = new List <Regulations_Association_Matrix__c>();
        Set<id> lineItemPathwayIds = new Set <id>();

        for (AC__c lineitem: vsLTGroupList){
            lineItemPathwayIds.add(lineitem.Program_Line_Item_Pathway__c);
        }

        //Added by Subbu - 12/18 - update thumbprint to authorization

        decisionsList = [SELECT Id, Program_Line_Item_Pathway__c, Country__c,Thumbprint__c FROM Regulations_Association_Matrix__c where Program_Line_Item_Pathway__c IN: lineItemPathwayIds  AND Thumbprint__c !=null ORDER BY CreatedDate DESC limit 1 ];

        if(decisionsList.size()==0)
            decisionsList =[SELECT Id, Program_Line_Item_Pathway__c, Country__c,Thumbprint__c FROM Regulations_Association_Matrix__c where Program_Line_Item_Pathway__c IN: lineItemPathwayIds  AND Thumbprint__c !=null  ORDER BY CreatedDate DESC limit 1 ];

        Set<String> associatedAGString = authGroupLTMap.keySet();
        System.debug('<<<<<<<<<<<<<<< associatedAGString size ' + associatedAGString.size() + '>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<< associatedAGString  ' + associatedAGString + '>>>>>>>>>>>>>>>'); 
        List<Authorizations__c> authtobesent =  new List<Authorizations__c>();
        for (Application__c app: appList){
            if(app.Application_Status__c == 'Submitted'){
                for (String LTString: associatedAGString){
                    Authorizations__c auth = new Authorizations__c();
                    auth.RecordTypeId = authRTId;
                    auth.Application__c = app.Id;
                    //auth.Port_of_Entry__c = app.
                    auth.Status__c = 'Draft';
                    auth.Applicant__c = app.Applicant_Name__c;
                    auth.AC_Applicant_Phone__c = app.Applicant_Phone__c;
                    auth.AC_Applicant_Email__c = app.Applicant_Email__c;
                    auth.AC_Applicant_Address__c = app.Applicant_Address__c;

                    //Added by Subbu - 12/18 - update thumbprint to authorization
                           
                    /*if(decisionsList.size()>0){
                        auth.Final_Decision_Matrix_Id__c= decisionsList[0].ID;
                        auth.Thumbprint__c = decisionsList[0].Thumbprint__c;
                    }*/
                    system.debug('@@@@@@@@@@@@@ insert authorization @@@@@@@@@@@@@@@');
                    insert(auth);

                    system.debug('--------authiD'+auth.id);
                    authtobesent.add(auth);
                    for (AC__c vsLT: vsLTGroupList){
                        if(vsLT.Status__c != 'Ready to Submit' &&vsLT.Status__c != 'Submitted' ){
                            app.addError('Please attach the required documents on the following line item ' + vsLT.Name  + ' before you submit the application for approval. Click back to the application. Locate the line item and click on New Applicant Attachment.');
                        }
                        else{
                            if(LTString==vsLT.Authorization_Group_String__c){
                            vsLT.Status__c = 'Submitted';
                            vsLT.Authorization__c = auth.Id;
                            LTToUpdateList.add(vsLT);
                            }
                        }
                    }
                System.Debug('<<<<<<< LTToUpdateList size: ' + LTToUpdateList.size() + ' >>>>>>>');    
                }
            }
        }
        
         System.Debug('<<<<<<< LTToUpdateList: ' + LTToUpdateList + ' >>>>>>>');
         
         Set<AC__c> dedupeSet = new Set<AC__c>();
         for(Integer i = 0; i <LTToUpdateList.size(); i++){
            dedupeSet.add(LTToUpdateList[i]);
            }    
            
            System.Debug('<<<<<<< dedupeSet size: ' + dedupeSet.size() + ' >>>>>>>');    
            System.Debug('<<<<<<< dedupeSet : ' + dedupeSet + ' >>>>>>>'); 
           
           
           List<AC__c> dedupeList = new List<AC__c>();
           for(AC__c ac: dedupeSet){
           dedupeList.add(ac);
           }
           
            System.Debug('<<<<<<< dedupeList size: ' + dedupeList.size() + ' >>>>>>>');    
            System.Debug('<<<<<<< dedupeList : ' + dedupeList + ' >>>>>>>'); 
             
           System.Debug('<<<<<<< authtobesent : ' + authtobesent + ' >>>>>>>'); 
             
        
           
               
        update (dedupeList);
        
                    
                    CreateAuthorizationJunction createAuthjunction = new CreateAuthorizationJunction();
                    createAuthjunction.createAuthorizationJunctionRecord(authtobesent);
                    
                            

    }
    
}