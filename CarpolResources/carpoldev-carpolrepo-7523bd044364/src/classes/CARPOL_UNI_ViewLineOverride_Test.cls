@isTest(seealldata=false)
private class CARPOL_UNI_ViewLineOverride_Test {
      @IsTest static void CARPOL_UNI_ViewLineOverride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Live Dogs').getRecordTypeId(); 
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Contact objcont1 = testData.newcontact();          
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();
          breed__c objbrd = testData.newbreed(); 
          Level_1_Region__c appL1R = testData.newlevel1regionAL();              
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Country__c uscntry = testData.newcountryus();
          AC__c mainli;
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare; 
          
         Test.startTest(); 
         System.runAs(usershare){
              Applicant_Contact__c apcont = new Applicant_Contact__c();
              apcont.First_Name__c = 'apcont';
              apcont.Email_Address__c = 'apcont@test.com';
              apcont.Name = 'Associated Contacts';
              //if the field below isn't assigned the AddressVerify class breaks //- DMS 4/4/2016
              apcont.Mailing_Country__c = 'United States';
              apcont.Mailing_Country_LR__c = appL1R.Country__c;
              apcont.Mailing_State_LR__c = appL1R.Id;
              apcont.Mailing_State__c = 'Alabama';
              apcont.RecordTypeId = ImpapcontRecordTypeId;
              Test.setMock(HttpCalloutMock.class, new AddressVerifyMockImpl());
              insert apcont;                        
              
              Application__c objapp = new Application__c();
              objapp.Applicant_Name__c = objcont.Id;
              objapp.Recordtypeid = ACAppRecordTypeId;
              objapp.Application_Status__c = 'Open';
              insert objapp;  
              
              AC__c ac = new AC__c();

              ac.Application_Number__c = objapp.Id;
              ac.Departure_Time__c = date.today()+11;
              ac.Arrival_Time__c = date.today()+15;
              ac.Proposed_date_of_arrival__c = date.today()+15;
              ac.Port_of_Entry__c = fac.Id;
              ac.Port_of_Embarkation__c = fac2.id;
              ac.Transporter_Type__c = 'Ground';
              ac.RecordTypeId = ACRecordTypeId;
              ac.Date_of_Birth__c = date.today()-400;
              ac.Breed__c = objbrd.id;
              ac.Color__c = 'Brown';
              ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
              ac.Sex__c = 'Male';
              ac.Country_Of_Origin__c = uscntry.Id;
              ac.Program_Line_Item_Pathway__c = objpathway.Id;    
              ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
              ac.Importer_Last_Name__c = apcont.id;
              ac.Exporter_Last_Name__c = apcont.id;
              ac.Status__c = 'Saved';
              ac.Purpose_of_the_Importation__c = 'Resale/Adoption';
              ac.DeliveryRecipient_Last_Name__c = apcont.id;
              insert ac;
              mainli = ac;

              //run as portal user
                  PageReference pageRef = Page.CARPOL_UNI_ViewApplicationLineItem;
                  Test.setCurrentPage(pageRef);
                  ApexPages.currentPage().getParameters().put('id',mainli.id);
                  CARPOL_UNI_ViewLineOverride extclassno = new CARPOL_UNI_ViewLineOverride();
                  ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mainli);
                  CARPOL_UNI_ViewLineOverride extclass = new CARPOL_UNI_ViewLineOverride(sc);
                  extclass.redirect(); 
                  system.assert(extclass != null);                           
         }
         
         //run as salesforce admin user
                  PageReference pageRef = Page.CARPOL_UNI_ViewApplicationLineItem;
                  Test.setCurrentPage(pageRef);
                  ApexPages.currentPage().getParameters().put('id',mainli.id);
                  CARPOL_UNI_ViewLineOverride extclassno = new CARPOL_UNI_ViewLineOverride();
                  ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(mainli);
                  CARPOL_UNI_ViewLineOverride extclass = new CARPOL_UNI_ViewLineOverride(sc);
                  extclass.redirect();
                  system.assert(extclass != null);
         Test.stopTest();            
      }
}