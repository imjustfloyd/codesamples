@isTest(SeeAllData=true)
private class CARPOL_BRS_EPRegulations_extensionTest {
    static testMethod void testCARPOL_BRS_EPRegulations_extension() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);
      Regulation__c objreg1 = testData.newRegulation('Standard','');
      Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
      Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Attachment attach = testData.newattachment(ac.Id);           
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac.Authorization__c = objauth.Id;
      update ac;
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
 	  Workflow_Task__c objWF = testData.newworkflowtask('Test', objauth, 'Complete');
      String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
      String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
      String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
      objreg4.RecordTypeid = ReguRecordTypeId;
      update objreg4;
      Domain__c objprog = testData.newProgram('BRS');
       Program_Prefix__c objPrefix = new Program_Prefix__c();
     objPrefix.Program__c = objprog.Id;
     objPrefix.Name = '101';
     objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
     insert objPrefix;
       Signature__c objTP = new Signature__c();
      objTP.Name = 'Test BRS KK';
      objTP.Recordtypeid = BRSTPRecordTypeId ;
      objTP.Program_Prefix__c = objPrefix.Id;
      insert objTP; 
      
      Applicant_Attachments__c objAtt = testData.newAttach(ac.Id);
      Applicant_Attachments__c objAtt1 = testData.newAttach(ac.Id);
      Applicant_Attachments__c objAtt2 = testData.newAttach(ac.Id);
      
      Test.startTest();
      PageReference pageRef = Page.CARPOL_BRS_EPRegulations;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      ApexPages.currentPage().getParameters().put('wfid',objWF.id);
      ApexPages.currentPage().getParameters().put('PermitPackage','True');
      
      CARPOL_BRS_EPRegulations_extension acepreg = new CARPOL_BRS_EPRegulations_extension(sc);
      acepreg.regulation = objreg1;
      acepreg.getResults();
      acepreg.setRegulationType();
      acepreg.getAddByOptions();
      acepreg.selectInput();
      acepreg.getAddByOptions();
      acepreg.updateSignature();
      
      acepreg.createNewRegulation();
      acepreg.BRSCollaboration();
      acepreg.attachPDF();
      acepreg.save();
      //string sDocType = acepreg.flattenSelectedDocuments();
      acepreg.deleteRecord();
      acepreg.redirect();
      acepreg.cancel();
      //acepreg.viewDraftPDF();
      
      objauth.Thumbprint__c = objTP.id;
      objauth.Recordtypeid = authRecordTypeId ;
      objauth.Authorization_Type__c = 'Permit';
      update objauth;
       Facility__c  entry = testData.newfacility('Domestic Port');
          Authorization_Junction__c objauthjun = new Authorization_Junction__c();
          objauthjun.Authorization__c = objauth.id;
          objauthjun.Port__c = entry.id;
          insert objauthjun;
       ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      ApexPages.currentPage().getParameters().put('wfid',objWF.id);
      ApexPages.currentPage().getParameters().put('PermitPackage','True');
      CARPOL_BRS_EPRegulations_extension acepreg2 = new CARPOL_BRS_EPRegulations_extension(sc2);
      //acepreg2.viewDraftPDF();
      acepreg2.attachPDF();
      objPrefix.Permit_PDF_Template__c = '';
      update objPrefix;
      acepreg2.viewDraftPDF();
      System.assert(acepreg != null);
      Test.stopTest();
    }
}