@isTest
public class CARPOL_BRS_Reviewer_DupePrevTest
{
   public static testmethod void test(){
           Recordtype ra =[select Id,Name from recordtype where Name='Accounts' and sobjecttype='Account' limit 1];            
            Account acc = new Account();
            acc.Name='Test';
            acc.recordtypeid = ra.id;
            insert acc;
   CARPOL_UNI_DisableTrigger__c DT = new CARPOL_UNI_DisableTrigger__c();
   dt.name = 'CARPOL_BRS_Reviewer_DuplicateTrigger';
   dt.Disable__c = false;
   insert dt;
   
 CARPOL_UNI_DisableTrigger__c DT1 = new CARPOL_UNI_DisableTrigger__c();
   dt1.name = 'CARPOL_BRS_State_ReviewerTrigger';
   dt1.Disable__c = false;
   insert dt1;
        
        Recordtype con =[select Id,Name from recordtype where Name='State SPRO' and sobjecttype='Contact' limit 1];            
            Contact cont = new contact();
            cont.firstName='TestFirst';
            cont.lastName = 'TestLast';
            cont.accountId = acc.Id;
            cont.Phone = '1234567890';
            cont.Email = 'test@test.com';
            cont.recordtypeid = con.id;
            cont.mailingcountry = 'United States';
            cont.MailingStreet = '123 Main St';
            cont.MailingState = 'Texas';
            Cont.MailingCity = 'Dallas';
            cont.MailingPostalCode = '75075';
            cont.Contact_checkbox__c  = true;
            insert cont;  

            Contact cont1 = new contact();
            cont1.firstName='TestFirst';
            cont1.lastName = 'TestLast';
            cont1.accountId = acc.Id;
            cont1.Phone = '1234567890';
            cont1.Email = 'test@test.com';
            cont1.recordtypeid = con.id;
            cont1.mailingcountry = 'United States';
            cont1.MailingStreet = '123 Main St';
            cont1.MailingState = 'Texas';
            Cont1.MailingCity = 'Dallas';
            cont1.MailingPostalCode = '75075';
            cont1.Contact_checkbox__c  = false;
            insert cont1;  

    // create one record on the object Reviewer__c 
    Reviewer__c  r = new Reviewer__c ();
    // write all your fields by making sure as below
    //r.Reviewer_Unique_Id__c='okp';
    r.State_Regulatory_Official__c = cont.id;
    
    insert r;

    system.assert(r != null);

    // create one record on the object Reviewer__c 
    Reviewer__c  r1 = new Reviewer__c ();
    // write all your fields by making sure as below
    //r.Reviewer_Unique_Id__c='okp';
    r1.State_Regulatory_Official__c = cont1.id;
    try{    
    insert r1;
    }catch(exception e){}
            cont.Contact_checkbox__c  = false;
            update cont;
            r.State_Regulatory_Official__c = cont.id;
            try{
            update r;
            }catch(exception e){}
    
    // create one more record in the object Reviewer__c 
    //Reviewer__c r2 = new Reviewer__c ();
    // write all your fields by making sure as below
    //r2.Reviewer_Unique_Id__c='Pok';
    //insert r2;
    }
}