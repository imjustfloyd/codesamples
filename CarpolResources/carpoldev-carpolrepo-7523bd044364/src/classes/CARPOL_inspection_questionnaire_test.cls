@isTest(seealldata=true)

private class CARPOL_inspection_questionnaire_test{
   private static testMethod void testCARPOL_inspection_questionnaire() {
      
         EFL_Inspection_Questions__c IQ = new EFL_Inspection_Questions__c();
         IQ.question__c = 'What is the date today';
         insert IQ;
         
         Facility__c fac = new Facility__c();
         fac.Name = 'Test Facility';
         insert fac;
         
         Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
         Inspection__c Ins = new Inspection__c();
         Ins.Facility__c = facc.id;
         insert Ins;
         
         EFL_Inspection_Responses__c IR = new EFL_Inspection_Responses__c();
         IR.Response__c = 'Response 1';
         IR.EFL_Inspection_Questions__c = IQ.Id;
         insert IR;
         
         EFL_Inspection_Questionnaire__c IQR = new EFL_Inspection_Questionnaire__c();
         IQR.Inspection__c = Ins.id;
         insert IQR;
         EFL_Inspection_Questionnaire_Questions__c recEIQ=new EFL_Inspection_Questionnaire_Questions__c ();
         recEIQ.Question__c='test question1';
         recEIQ.Answer__c='test';
         recEIQ.EFL_Inspection_Questionnaire__c=IQR.id;
         recEIQ.Source_Question__c=IQ.Id;
         insert recEIQ;
         
         EFL_Inspection_Questionnaire_Responses__c objEIQ=new EFL_Inspection_Questionnaire_Responses__c();
         objEIQ.EFL_Inspection_Questionnaire_Questions__c=recEIQ.Id;
         objEIQ.Response__c='test response';
         insert objEIQ;
         ApexPages.currentPage().getParameters().put('Id',IQR.id);
         ApexPages.StandardController stdCon = new ApexPages.StandardController(IQR);
         CARPOL_inspection_questionnaire objCPQMYA= new CARPOL_inspection_questionnaire(stdCon);
         List<EFL_Inspection_Questionnaire_Questions__c> RelatedQuestList=objCPQMYA.getRelatedQuestList();
         objCPQMYA.save();
         objCPQMYA.submit();
         objCPQMYA.first();
         objCPQMYA.cancel();
         objCPQMYA.last();
         objCPQMYA.next();
         objCPQMYA.previous();
         objCPQMYA.setPaginationSize();
         objCPQMYA.submit();
         objCPQMYA.hasNext=false;
         objCPQMYA.hasPrevious=false;
    }         

}