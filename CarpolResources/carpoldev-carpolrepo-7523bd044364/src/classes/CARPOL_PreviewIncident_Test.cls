@isTest(seealldata=true)
public class CARPOL_PreviewIncident_Test {
      @IsTest static void CARPOL_PreviewIncedent_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
         
          
          //Create Facility
         String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
         Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
          
          
          //Incident__c
          Incident__c incident = new Incident__c();
          incident.Requester_Name__c='testReqName';
          incident.EFL_Template__c=objcm.id;
          incident.Facility__c =fac.id;
          incident.Authorization__c= objauth.id;
          incident.EFL_Issued_Date__c = Date.today();
          insert incident;
          //Create Inspection__c
          
          Inspection__c inspection = new Inspection__c();
          inspection.Inspector__c=objcont.Id;
          inspection.Facility__c=fac.id;
          inspection.Incident__c=incident.id;
          inspection.Scheduled_Date_of_Inspection__c = date.today();
          Insert inspection;

           
            
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_PreviewIncident;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(incident);
              ApexPages.currentPage().getParameters().put('id',incident.id);
              ApexPages.currentPage().getParameters().put('tId',objwft.id);

              CARPOL_PreviewIncident extclass = new CARPOL_PreviewIncident(sc);
              extclass.populateTemplate(); 
              
              extclass.cancel(); 
              extclass.attachLetter();
              extclass.attachDraftLetter();

              extclass.saveDraft();
              extclass.previewTemplate();
              extclass.reviewerComments = '';
              system.assert(extclass != null);
             Test.stopTest();    

      }

}