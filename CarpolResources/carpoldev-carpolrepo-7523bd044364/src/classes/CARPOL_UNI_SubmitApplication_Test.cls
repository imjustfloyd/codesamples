@isTest(seealldata=false)
private class CARPOL_UNI_SubmitApplication_Test {
      @IsTest static void CARPOL_UNI_SubmitApplication_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          
          Program_Line_Item_Pathway__c objAcPthway = new Program_Line_Item_Pathway__c();
          objAcPthway.Program__c = testData.newProgram('BRS').Id;
          objAcPthway.Name = 'BRS';
          objAcPthway.Status__c = 'Active';  
          System.assert(objAcPthway != null);     
          insert objAcPthway;
          
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Signature__c thumb = testData.newThumbprint();
          thumb.name = 'PPQ--CITES--Fast Track';
          update thumb;
          Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
          Regulation__c newreg2 = testData.newRegulation('Additional Information','Test');        
          Signature_Regulation_Junction__c srj1 = testData.newSRJ(thumb.id,newreg1.id);
          Signature_Regulation_Junction__c srj2 = testData.newSRJ(thumb.id,newreg2.id);             
          Application__c objapp = testData.newapplication();
          objapp.Signature__c = thumb.id;
          objapp.Application_Status__c = 'Submitted';
          update objapp;
          List<Application__c> appList = new List<Application__c>();
          appList.add(objapp);
          AC__c ac1 = testData.newLineItem('Personal use',objapp);        
          ac1.Program_Line_Item_Pathway__c = objAcPthway.ID;
          update ac1;  
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          update ac1;
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 

          Test.startTest();
          CARPOL_UNI_SubmitApplication extclass = new CARPOL_UNI_SubmitApplication();
          CARPOL_UNI_SubmitApplication.onAfterUpdate(appList);
          CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(objauth);
          System.assert(extclass != null);
          Test.stopTest();     
      }
}