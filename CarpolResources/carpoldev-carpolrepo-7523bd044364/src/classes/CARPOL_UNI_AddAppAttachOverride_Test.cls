@isTest(seealldata=false)
private class CARPOL_UNI_AddAppAttachOverride_Test {
      @IsTest static void CARPOL_UNI_AddAppAttachOverride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               

          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          List<RecordType> rtypes = [SELECT ID FROM RecordType WHERE Name = 'Biotechnology Regulatory Services - Notification'];

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_AddAppAttach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objaa);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              if(!rtypes.isempty()){
                  ApexPages.currentPage().getParameters().put('RecordType',rtypes[0].id);
              }
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);              
              
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');                                                                      
              CARPOL_UNI_AddAppAttachOverride extclassempty = new CARPOL_UNI_AddAppAttachOverride();              
              CARPOL_UNI_AddAppAttachOverride extclass = new CARPOL_UNI_AddAppAttachOverride(sc);
              extclass.redirect();
              system.assert(extclass != null);
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_AddAppAttach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objaa);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              if(!rtypes.isempty()){
                  ApexPages.currentPage().getParameters().put('RecordType',rtypes[0].id);
              }
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');     
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                                                                                             
              CARPOL_UNI_AddAppAttachOverride extclassempty = new CARPOL_UNI_AddAppAttachOverride();
              CARPOL_UNI_AddAppAttachOverride extclass = new CARPOL_UNI_AddAppAttachOverride(sc);
              extclass.redirect(); 
              system.assert(extclass != null);                       
          Test.stopTest();   
      }
}