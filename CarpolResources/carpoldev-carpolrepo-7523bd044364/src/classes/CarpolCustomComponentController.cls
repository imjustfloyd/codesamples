public class CarpolCustomComponentController {
    public string htmlid        {get;set;}
    public String cssFileNameValue;
    public String jsFileNameValue;

    public void setCssFileNameValue (String c) {
        cssFileNameValue = c.deleteWhitespace();
    }
    
    public void setJsFileNameValue(String j) {
        jsFileNameValue = j.deleteWhitespace();
    }
   
    public String getCssFileNameValue (){
        
        return cssFileNameValue;
        
    }

     public String getJsFileNameValue (){
        return jsFileNameValue;
     }
     
    
}