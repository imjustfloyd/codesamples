public class portal_location_detail {
    public Id LocationID{get;set;}
    public String redirect {get;set;}
    public Id LineItemId {get;set;}
    public Id strPathwayId {get;set;}
    public String recordTypename {get;set;}
    public boolean ret {get;set;}
    public Location__c obj {get;set;}
    public ID recordTypeId {get;set;}
    public String DestLoc {get;set;}
    public String OrigLoc {get;set;}
    public String OrigDestLoc {get;set;}
    public String RelLoc {get;set;}
    
    public portal_location_detail(ApexPages.StandardController controller) {
         obj = (Location__c)Controller.getRecord();
         LocationID=ApexPages.currentPage().getParameters().get('id');
         redirect = ApexPages.currentPage().getParameters().get('redirect');
         strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
         LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
        recordTypeId = ApexPages.currentPage().getParameters().get('rectype');
            // obj = (location__c)controller.getRecord();
            // obj.Line_Item__c = ApexPages.currentPage().getParameters().get('LineItemId');
         if(redirect == 'yes'){
                    ret = true;
             }
        list<Location__c> rts = [SELECT RecordTypeId, RecordType.Name FROM Location__c WHERE ID =:LocationID ];
        System.debug('Record type queried : '+ rts);
        //list<RecordType> rts = [SELECT ID, DeveloperName FROM RecordType WHERE ID = :recordTypeId];
                if(rts.size()>0){
                    for(Location__c r : rts){
                        if(r.RecordType.Name == 'Destination Location' || r.RecordType.Name == 'Origin and Destination Location'){
                            DestLoc = 'Yes';
                            OrigDestLoc = 'Yes';
                            recordTypename = r.RecordType.Name;
                            System.debug('Destination marked YES');
                        } else if(r.RecordType.Name == 'Origin Location'){
                            OrigLoc = 'Yes';
                            System.debug('origin location marked YES');
                        } else if(r.RecordType.Name == 'Release Sites Location'){
                            RelLoc = 'Yes';
                            System.debug('Release location marked YES');
                        }
                    }
                }
    }
    public pageReference ReturnToLineItem(){

       PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
    
       LineItemId = obj.Line_Item__c;
       
       List<AC__c> pathway = new List<AC__c>();
       pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
       
       Id dummy;
       for(AC__c i : pathway){
       dummy = i.Program_Line_Item_Pathway__c;}
      
       pg.getParameters().put('strPathwayId',dummy);    
       pg.getParameters().put('LineId',LineItemId);
       pg.setRedirect(true);
       return pg;
    }
    public pageReference edit(){
        PageReference pg = Page.portal_location_edit;
        LineItemId = obj.Line_Item__c;
        if(recordTypeId == null){
            recordTypeId = [SELECT ID FROM Recordtype WHERE ID=:obj.RecordTypeID].ID;
        }
        pg.getParameters().put('id',LocationID);
        pg.getParameters().put('RecType',recordTypeId);
        /*if(LineItemId == null){
            LineItemId = [SELECT Line_Item__c FROM Location__c WHERE ID=:LocationId].Id;
        }
        pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);*/
        pg.getParameters().put('LineItemId',LineItemId);
        pg.setRedirect(true);
        return pg;
    }

}