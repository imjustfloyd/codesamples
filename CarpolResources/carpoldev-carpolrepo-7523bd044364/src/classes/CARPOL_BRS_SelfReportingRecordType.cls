public class CARPOL_BRS_SelfReportingRecordType{

public id recordTypeId{get;set;}
public string BaseUrl;
public String AuthId {get; set;}
public String rectype {get;set;}
public CARPOL_BRS_SelfReportingRecordType(ApexPages.StandardController controller) {
        BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        AuthId = ApexPages.currentPage().getParameters().get('AuthorizationId');


    }

public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Self_Reporting__c' ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
            }
        }
        return options;
    }
public pageReference continueAssociate(){
    Profile p = [select name from Profile where id =
                 :UserInfo.getProfileId()];
    if ('APHIS Applicant'.equals(p.name)
        || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name))
        {

        /*string URLString1  =  Label.Portal_Associate_Contact_Create_URL1;
        string URLString2  =  Label.Portal_Associate_Contact_Create_URL2;

        BaseUrl= BaseUrl +URLString1+recordTypeId+URLString2;
        PageReference pg = new PageReference(BaseUrl);
        pg.setRedirect(true);*/
        PageReference customPage = Page.portal_SelfReport_edit;
        customPage.getParameters().put('AuthoId', AuthId);
        customPage.getParameters().put('rectype',recordTypeId);
        customPage.setRedirect(true);
        return customPage;
        //return pg;
        }else{
        String hostname = ApexPages.currentPage().getHeaders().get('Host');
        // String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
        String optyURL2 = 'https://'+hostname+'/'+ SObjectType.Self_Reporting__c.keyPrefix +'/e?&RecordType='+recordTypeId+'&nooverride=1?';
        // pageReference.getParameters().put('nooverride', '1');
        pagereference pageref = new pagereference(optyURL2);
        pageref.setredirect(true);
        return pageref;
        }
}
}