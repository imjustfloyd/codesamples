public  class portal_selfreporting {
    public Applicant_Attachments__c obj {get; set;}
    public Id authorizationID{get;set;}
    public Id AppattachId {get;set;}
    public ID cancelid {get;set;}
    public string redirect {get;set;}
    public Id LineItemId{get;set;}
    public Id strPathwayId {get;set;}
    public ID recordTypeId {get;set;}
    public string recTypeName{get;set;}
    public id appId {get;set;}
    public List<SObject> toInsert = new List<SObject>();
    Public attachment objAttachment{get; set;}

    public portal_selfreporting(ApexPages.StandardController Controller) {
        
     obj = (Applicant_Attachments__c)Controller.getRecord();
        //obj.Animal_Care_AC__c = ApexPages.CurrentPage().getParameters().get('LineItemId');
       // LineItemId= ApexPages.currentPage().getParameters().get('LineItemId');
       // appId = ApexPages.currentPage().getParameters().get('appId');
       // AppattachId = ApexPages.currentPage().getParameters().get('genid');
        redirect = ApexPages.currentPage().getParameters().get('redirect');
       // strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
        authorizationID=ApexPages.currentPage().getParameters().get('authid');
  
        obj.RecordTypeId = ApexPages.CurrentPage().getParameters().get('RecordType');
        cancelid = ApexPages.currentPage().getParameters().get('LineItemId');
        recTypeName = CARPOL_UNI_RecordType.getObjectRecordTypeName(Applicant_Attachments__c.SObjectType,obj.RecordTypeId);
        recordTypeId = ApexPages.CurrentPage().getParameters().get('RecordType');
        system.debug('obj.Animal_Care_AC__c'+obj.Animal_Care_AC__c);
        getExistingDocuments();
        newAttachments=new List<DocumentWrap>{new DocumentWrap()};
        objAttachment = new Attachment();
                
    }
     public pageReference cancel(){
        PageReference pg = new PageReference('/'+cancelid);
        pg.setRedirect(true);
        return pg;
    }    

     public pagereference cancelappattach(){
   
    PageReference pg;
    if(redirect == 'yes'){
    pg = Page.CARPOL_LineItemPageForApplicant;
    pg.getParameters().put('LineId',LineItemId);
    pg.getParameters().put('strPathwayId',strPathwayId);
    pg.setRedirect(true);
    
    system.debug('<<<!!!!pg'+pg);
    //return pg;
    
    }else if(obj.id != null){
        appattachId = ApexPages.currentPage().getParameters().get('id');
        obj.id = appattachId;
        pg = Page.portal_applicantattachment_detail;
            pg.getParameters().put('id',obj.id);
            pg.getParameters().put('recordTypeId',recordTypeId);
            pg.setRedirect(true);
            system.debug('<<<<<<<<<!!!!! cancel page' + pg);
        //return pg;
        }
        else{
        pg = new PageReference('/'+cancelid);
        pg.setRedirect(true);
        //return pg;
        }
        return pg;
    }
    
    //required documents map
    //select option list of documents remaining
    public List<SelectOption> remainingDocuments { get; set;}
    //select option list of documents requried
    public List<SelectOption> requiredDocuments { get; set; }
    //expect this one to go
    public Boolean renderRemainingDocuments { get; set; }
    //remove once save is rewritten - expect this to migrate onto the field
    public String[] selectedDocuments { get; set; }
 

    public List<Applicant_Attachments__c> appAttachments {get;set;}
    //list of new attachments to add
    public List<DocumentWrap> newAttachments {get;set;}

    //used to compare uploaded records with required records
    public Map<String,documentResource> mapUploadedDocuments = new Map<String,documentResource>();
    //should be removed, replace it's use in the map with something else
    public List<documentResource> docResource = new List<documentResource>();



public pageReference saveappattach(){

try{
        AppattachId = ApexPages.currentPage().getParameters().get('id');
       // obj.id = AppattachId;
                insert obj;

        PageReference pg = Page.portal_applicantattachment_detail;
        pg.getParameters().put('recordTypeId',recordTypeId);
        pg.getParameters().put('id',obj.id);
        
    if(redirect == 'yes'){
       
            pg.getParameters().put('redirect',redirect);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.getParameters().put('LineItemId',LineItemId);
            //pg.getParameters().put('RecordType',recordTypeId);
        }
        
        system.debug('<<<<<<!!!!! save page'+pg);
            pg.setRedirect(true);
        return pg;
        
    }catch(Exception ex){
       // pagereference p = apexpages.Currentpage();
        string errmsg = ex.getMessage();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning,errmsg));
        //apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.WARNING,ex);
       // apexpages.addmessage(msg);
       // ApexPages.addMessages(ex);
       // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,ex));

        return null;
        } 
        
    

}
    public PageReference upload(){
        PageReference pg = Page.Portal_UploadAttachment;
        pg.getParameters().put('Id', appId );
        pg.getParameters().put('retPage','portal_applicantattachment_detail');
        system.debug('Construct ID passing to att: ' + appId );
        pg.setRedirect(true);
        return pg;
    }
    public List<Attachment> getattach(){
        return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
        }
        
    //-------------------------------------Applicant Attachments New Format-------------------------------------------------------//
        public pageReference save()
    {   //must insert the app attachments, populate then populate applicant attachment id on attachments.  

        //record type for all Applicant_Attachments__c
        ID tmpRecordType = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Self Reporting').getRecordTypeId();
        system.debug('tmpRecordType****'+tmpRecordType);
        Boolean docSave = true;
       // string appNumber = [select id,Application_Number__c from AC__c where Id =:LineItemId limit 1][0].Application_Number__c;
                 PageReference pageRef;
               if(objAttachment.body !=null){
                obj.RecordTypeID = tmpRecordType;
               // obj.Animal_Care_AC__c = LineItemId;
               // obj.Application__c = appNumber;
                obj.Authorization__c = authorizationID;
                obj.File_Description__c = 'Uploaded ' + System.Now();
                obj.Document_Types__c = 'Standard Operating Procedures (SOP’s)';
           
                 insert obj;
                
                  objAttachment.OwnerId = UserInfo.getUserId();
                  objAttachment.ParentId = obj.id;
                  objAttachment.Description = 'Uploaded Document';
        
                  insert objAttachment;      
                 }
     
        
        //refresh the list of required and existing documents        
        getExistingDocuments();
        
        if(docSave){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.confirm,'File uploaded Successfully.'));            
    
        pageReference pg1 = ApexPages.currentPage(); 
        Id applid = pg1.getParameters().get('appid');
        Id LineItemId = pg1.getParameters().get('LineItemId');
        id rectype = pg1.getParameters().get('RecordType');
        id strPathwayId = pg1.getParameters().get('strPathwayId');

        pg1.getParameters().clear();
        pg1.getParameters().put('authid',authorizationID);  
       // pg1.getParameters().put('LineItemId', LineItemId);
        pg1.getParameters().put('RecordType',rectype);  
       // pg1.getParameters().put('strPathwayId', strPathwayId);
       // pg1.getParameters().put('message', 'File Attached Successfully');
      //  pg1.getParameters().put('showReqDocsSection','true');
        pg1.setRedirect(true);   
        return pg1;  
       // return null;
        } else {
            return null;
        }  
    }
    
       //Getting existing uploaded documents if any
    public void getExistingDocuments() {
        remainingDocuments = new List<SelectOption>();
        system.debug('LineItemId ###'+LineItemId);
        appAttachments = [SELECT Id, Name, Document_Types__c, File_Name__c, File_Description__c,Status__c,(SELECT ID, Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Authorization__c =:authorizationID];
        
        System.Debug('<<<<<<< Total Uploaded Documents : ' + appAttachments.size() + ' >>>>>>>');
        
        if(appAttachments.size() > 0)
        {
            for(Applicant_Attachments__c appAtt : appAttachments)
            {
             //   System.Debug('<<<<<<< appAtt.Name : ' + appAtt.Name+ ' >>>>>>>');
             //   System.Debug('<<<<<<< Uploaded Documents : ' + appAtt.Document_Types__c + ' >>>>>>>');
            //    String[] documents = appAtt.Document_Types__c.split(';');
             //   System.Debug('<<<<<<< Uploaded Documents Stack : ' + documents.size() + ' >>>>>>>');

                //keep around but you may not need this to render the link on the component
                documentResource docRes = new documentResource();
                if(appAtt.Attachments.size() > 0)
                {
                    docRes.docURL = '/servlet/servlet.FileDownload?file=' + appAtt.Attachments[0].ID;
                    docRes.isUploaded = true;
                }
              /*  for(Integer j = 0; j < documents.size(); j++)
                {
                    mapUploadedDocuments.put(documents[j],docRes);
                    System.Debug('<<<<<<< mapUploadedDocuments Size  : ' + mapUploadedDocuments.size() + ' >>>>>>>');
                }*/
            }
        }
    }
    
    
    public List<SelectOption> getRequiredDocument() {

        List<SelectOption> options = new List<SelectOption>();
        List<String> reqDocs = CARPOL_UNI_Get_Required_LI_Docs.getRequiredDocs(LineItemId);
        if(reqDocs.size() > 0){
            for(String opt : reqDocs){
                  options.add(new SelectOption(opt,opt));
            }
        } else {
            options.add(new SelectOption('No document types found','No document types found',true));        
        }
        return options;
    }
    
    //flatten the document types
    public String flattenSelectedDocuments(List<String> documenttypes) {
    
        System.debug('<<<<<<< Selected Documents List Size : ' + documenttypes.size());
        String strSelectedDocuments = '';        
        if(documenttypes.size() != 0)
        {

            for(Integer i = 0; i < documenttypes.size(); i++)
            {
                strSelectedDocuments += documenttypes[i] + ';';
            }
            //removing the last '; '
            strSelectedDocuments = strSelectedDocuments.substring(0,strSelectedDocuments.length() - 1);
        }
        return strSelectedDocuments;
    }
    
    
    //Class to hold uploaded documents information
    public class documentResource{
        public ID docID {get;set;}
        public Boolean isUploaded {get;set;}
        public string docName {get;set;}
        public String docURL {get;set;} 
    } 
    
    //Subclass : Wrapper Class 
    @TestVisible public class DocumentWrap {
       public Applicant_Attachments__c appAttach{get;set;}
       public Attachment attachment {get;set;}    
       public List<String> documenttypes{get;set;}    

       //Wrapper  Class Controller
       public DocumentWrap() {
            appAttach = new Applicant_Attachments__c();
            attachment= new Attachment();
            documenttypes = new List<String>();
        }
    }
    
    public list < Attachment > attachmentList = new list < Attachment > ();
    public list < attWrapper > attWrapperList {
        get;
        set;
    }
    
    private AC__c acLineItem = new AC__c();
    public String picURL {
        get;
        set;
    }
   
    public String imageid {
        get;
        set;
    }
    
    public Boolean selected {
        get;
        set;
    }
   

    public Attachment uploadedFile {
        get {
            if (uploadedFile == null) uploadedFile = new Attachment();
            return uploadedFile;
        }
        set;
    }

 
    
    public String getreturnURL(){
        pageReference pg = ApexPages.currentPage();
        pg.getParameters().put('showReqDocsSection','true');

        if(pg.getParameters().containsKey('message')){
            pg.getParameters().remove('message');
            pg.getParameters().put('message','File successfully removed');        
        } else {
            pg.getParameters().put('message','File successfully removed'); 
        }
        String newURL = pg.getUrl();        
        return newURL;
    }

    public PageReference uploadFile() {

        try {
            String fileExtension = uploadedFile.Name;
            System.Debug('<<<<<<< Uploaded File Name : ' + fileExtension + ' >>>>>>>');

            if (uploadedFile.Name != null) {
                fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.') + 1, fileExtension.length());
                fileExtension = fileExtension.toUpperCase();
                System.Debug('<<<<<<< File Size : ' + uploadedFile.BodyLength + ' >>>>>>>');
                System.Debug('<<<<<<< File Extension : ' + fileExtension + ' >>>>>>>');
                if (fileExtension == 'PNG' || fileExtension == 'JPG' || fileExtension == 'JPEG') {
                    uploadedFile.OwnerId = UserInfo.getUserId();
                    uploadedFile.ParentId = LineItemId;
                    uploadedFile.Description = 'Uploaded Image';
                    insert uploadedFile;
                    picURL = '/servlet/servlet.FileDownload?file=' + uploadedFile.ID;
                    imageid = uploadedfile.id;
                    uploadedFile = new Attachment();
                   // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'File uploaded successfully.'));
                           pageReference pg1 = ApexPages.currentPage(); 
                             Id applid = pg1.getParameters().get('appid');
                             Id lineid = pg1.getParameters().get('id');
                             pg1.getParameters().clear();
                             pg1.getParameters().put('appid',applid);  
                           //  pg.getParameters().put('Id', sr.getid());
                             pg1.getParameters().put('Id', lineid);
                             pg1.getParameters().put('showPhotoSection','true');
                             pg1.getParameters().put('message', 'File uploaded successfully.');
                             pg1.setRedirect(true);   
                             return pg1; 
                   // getExisting();
                } else {
                    uploadedFile = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Only .png, .jpeg, .jpg files are allowed.'));
                }
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select an image to upload.'));
            }

            return null;


            //return null;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading file. Please re-try.'));
            return null;
        }
        
        
                            
    }
    
      public pageReference returntoauth(){
        pageReference pg1 = Page.portal_authorization_detail2; 
 
        pg1.getParameters().clear();
        pg1.getParameters().put('id',authorizationID);  
         pg1.setRedirect(true);   
        return pg1;  
      }
        //Wrapper


    public class attWrapper {
        public Attachment att {
            get;
            set;
        }
        public Boolean isChecked {
            get;
            set;
        }
        public attWrapper(Attachment att, boolean ischeck) {
            this.att = att;
            this.isChecked = ischeck;

        }
    }
    
}