@isTest

//Test class for AsyncFmsTreasGovServicesTcsonline
private class AsyncFmsTreas_test {

    private static testMethod void testCompleteOnlineCollectionResponseTypeFuture() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                AsyncFmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponseTypeFuture mocktest = new AsyncFmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponseTypeFuture();
                system.assert(mocktest != null);
                mocktest.getValue();
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testCreateForceResponseTypeFuture() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                AsyncFmsTreasGovServicesTcsonline.CreateForceResponseTypeFuture mocktest = new AsyncFmsTreasGovServicesTcsonline.CreateForceResponseTypeFuture();
                system.assert(mocktest != null);
                mocktest.getValue();
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testStartOnlineCollectionResponseTypeFuture() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                AsyncFmsTreasGovServicesTcsonline.StartOnlineCollectionResponseTypeFuture mocktest = 
                    new AsyncFmsTreasGovServicesTcsonline.StartOnlineCollectionResponseTypeFuture();
                system.assert(mocktest != null);
                mocktest.getValue();
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testGetDetailsResponseTypeFuture() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                AsyncFmsTreasGovServicesTcsonline.GetDetailsResponseTypeFuture mocktest = 
                    new AsyncFmsTreasGovServicesTcsonline.GetDetailsResponseTypeFuture();
                system.assert(mocktest != null);
                mocktest.getValue();
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testAsyncBeginStartOnlineCollection() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                System.Continuation c = new System.Continuation(120);
                fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest mocktest = 
                    new fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest();
                system.assert(mocktest != null);
                AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort req = 
                    new AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort();
                req.beginStartOnlineCollection(c, mocktest);
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testAsyncCompleteStartOnlineCollection() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                System.Continuation c = new System.Continuation(120);
                fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest mocktest = 
                    new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest();
                system.assert(mocktest != null);
                AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort req = 
                    new AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort();
                req.beginCompleteOnlineCollection(c, mocktest);
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    
    private static testMethod void testAsyncBeginCreateForce() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                System.Continuation c = new System.Continuation(120);
                fmsTreasGovServicesTcsonline.CreateForceRequest mocktest = 
                    new fmsTreasGovServicesTcsonline.CreateForceRequest();
                system.assert(mocktest != null);
                AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort req = 
                    new AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort();
                req.beginCreateForce(c, mocktest);
            } catch(Exception e){
            }
        Test.stopTest();
    }
    
    private static testMethod void testAsyncBeginGetDetails() 
    {
        // Null Web Service mock implementation
        Test.startTest();
            Test.setMock(WebServiceMock.class, new AsyncWebServiceMockImpl());
            try{
                System.Continuation c = new System.Continuation(120);
                fmsTreasGovServicesTcsonline.GetDetailsRequest mocktest = 
                    new fmsTreasGovServicesTcsonline.GetDetailsRequest();
                system.assert(mocktest != null);
                AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort req = 
                    new AsyncFmsTreasGovServicesTcsonline.AsyncTCSOnlineServicePort();
                req.beginGetDetails(c, mocktest);
            } catch(Exception e){
            }
        Test.stopTest();
    }



}