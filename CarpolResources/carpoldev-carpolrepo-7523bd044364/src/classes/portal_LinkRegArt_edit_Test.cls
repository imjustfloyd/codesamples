@isTest(seealldata=false)
private class portal_LinkRegArt_edit_Test {
      @IsTest static void portal_LinkRegArt_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);  
          Application__c objApp = TestData.newapplication();
          Authorizations__c objAuth = TestData.newAuth(objApp.id);
          Regulated_Article__c objRA = TestData.newRegulatedArticle(plip.Id);        
          Link_Regulated_Articles__c objlra = new Link_Regulated_Articles__c();
          objlra.Application__c = objApp.Id;
          objlra.Authorization__c = objAuth.Id;
          objlra.Line_Item__c = li.Id;
          objlra.Regulated_Article__c = objRA.Id;
          insert objlra;
          

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              PageReference pageRef = Page.portal_LinkRegArt_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objlra);
              ApexPages.currentPage().getParameters().put('id',objlra.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);              
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);

              portal_LinkRegArt_edit extclass = new portal_LinkRegArt_edit(sc);
              extclass.cancelLAR(); 
              extclass.SaveLAR(); 
              extclass.getCategory();
              extclass.searchArticle(); 
              extclass.displayRegArticles();       
              System.assert(extclass != null);                      
              ApexPages.currentPage().getParameters().put('redirect','no');
              portal_LinkRegArt_edit extclass1 = new portal_LinkRegArt_edit(sc);
              extclass1.obj = objlra;
              extclass1.cancelLAR(); 
              extclass1.lineitemlst = new List<AC__c>();
              extclass1.Lineitem = li.id;
              extclass1.larlst = new List<Link_Regulated_Articles__c>();
          Test.stopTest();   
      }
}