public class CARPOL_UNI_ADD_LOCALE_TRANSIT{



    
    public List<Transit_Locale__c> lstTransit  = new List<Transit_Locale__c>();
    public List<transitInnerClass> transitInner{get;set;}
    public String selectedRowIndex{get;set;}  
    public Integer count = 1;
    public string LineItem;
    public string appId;
    public String countryofOrigin; //added by Niharika
    public String ScientificName {get;set;}
    public string linestatus  {get;set;}
    public boolean autoaddTL {get; set;}
    public AC__c objLineItem {get; set;}
    public Date gettemp()
    {
        Date d = date.ValueOf('2013-12-10');
        return d;
    }
    public CARPOL_UNI_ADD_LOCALE_TRANSIT(){
        objLineItem  = new AC__c();
        //autoaddTL = true;
        
        LineItem = ApexPages.currentPage().getParameters().get('LineItemId');
        countryofOrigin = ApexPages.currentPage().getParameters().get('countryofOrigin'); //added by niharika
         ScientificName = ApexPages.currentPage().getParameters().get('ScientificName');
        objLineItem = [Select id,name,Application_Number__c, status__c, Program_Line_Item_Pathway__c,application_number__r.name,Regulated_Article__c, Scientific_name__r.name, Country_Of_Origin__r.name,Country_of_destination__r.name From AC__c Where id=: LineItem];
        appId = objLineItem.Application_Number__c;
        linestatus = objLineItem.status__c;
        transitInner = new List<transitInnerClass>();
        getOldTllst();
        system.debug('--autoaddTL--'+autoaddTL);
        if(autoaddTL){
        system.debug('--autoaddTL--'+autoaddTL);        
        Add();
        }
       
        selectedRowIndex = '0';
   }
    
    public void Add(){ 
        system.debug('autoaddTL ==='+autoaddTL);
        autoaddTL = true; 
        system.debug('autoaddTL ==='+autoaddTL);
        //transitInner[count-1].addTransitBtn = false;
        count = count+1;
        addMore();      
    } 
    public void removeTransit(){   
        //transitInner[count-1].addTransitBtn = false;
        count = count-1;
        transitInnerClass objInnerClass = new transitInnerClass(count,countryofOrigin,ScientificName);
        transitInner.remove(count);     
    }
    
    public void addMore(){
        transitInnerClass objInnerClass = new transitInnerClass(count,countryofOrigin,ScientificName);
        transitInner.add(objInnerClass);    
        system.debug('transitInner---->'+transitInner);            
    }
    public void removeMore(){
        //transitInnerClass objInnerClass = new transitInnerClass(count,countryofOrigin,ScientificName);
       // transitInner.remove(objInnerClass);    
        system.debug('transitInner---->'+transitInner);            
    }
     
     public class transitInnerClass{       
        public String recCount{get;set;}
        public Transit_Locale__c transit{get;set;}
        public boolean addTransitBtn{get;set;}
        //public transitInnerClass(Integer intCount){
        
        // updated by Niharika to autopopulate first record of fromcountry.
        public transitInnerClass(Integer intCount,String countryOrigin,String ScientificName){
            recCount = String.valueOf(intCount);        
            transit = new Transit_Locale__c();
           transit.Regulated_Article__c    =ScientificName;
            if(intCount == 1)
            transit.From_Country__c=countryOrigin;
            addTransitBtn = true;
        }   
    }
    public string delTransitid {get;set;}
     public PageReference deleteTransit() {
           system.debug('---delTransitid ---'+delTransitid);
           // Transit_Locale__c objTransit = [select id,name from Transit_Locale__c where id=:delTransitid];
           // delete objTransit ;
            for (Integer i = 0; i < lstTransit.size(); i++) {
                Transit_Locale__c a = lstTransit[i];
                if (a.Id == delTransitid) {
                    delete a;
                    lstTransit.remove(i);
                    break;
                }
            }
            return null;
            }
    public PageReference SaveTransitLocal(){
        PageReference pgRef;
        try{
            for(Integer j = 0;j<transitInner.size();j++)
            {
                Transit_Locale__c objTransit = transitInner[j].transit;
                objTransit.Line_Item__c = LineItem;
                if(objTransit.Type_of_Conveyance__c!=null){
                lstTransit.add(objTransit);
                }
            } 
            system.debug('lstTransit==='+lstTransit);
            upsert lstTransit;
            lstTransit.clear();
            autoaddTL = false;
            transitInner = new List<transitInnerClass>();
            count = 0;
            system.debug('----lstTransit size--'+lstTransit.size());
            getOldTllst();
           // addMore();
            //pgRef = new PageReference('/apex/portal_application_detail?id='+appId);
            //pgRef.setRedirect(True);
        }catch(Exception Ex){
            system.debug('Exception :::'+Ex);
        }
        return null;
    }
    
     public PageReference CancelTransitLocal(){
        PageReference pgRef = new PageReference('/apex/carpol_uni_lineitem?appid='+appid+'&ID='+LineItem+'&strPathway='+objLineItem.Program_Line_Item_Pathway__c);
        pgref.setredirect(true);
        return pgRef;
    } 
        public list<Transit_Locale__c> getOldTllst() {
        system.debug('autoaddTL ==='+autoaddTL);
        lstTransit  = [select id,Name,Type_of_Conveyance__c,Mode_of_Transportion__c,From_Country__c,To_Country__c,Port_of_Entry__c,Port_of_Exit__c,Packaging_Material_Category__c,Arrival_Date_Time__c, contact__c from Transit_Locale__c where Line_Item__c=:LineItem];
        if(lstTransit.size()>0){
         system.debug('autoaddTL ==='+autoaddTL);
            autoaddTL = false;
        }
        else{
             autoaddTL = true;
        }
        return lstTransit;
    }     
}