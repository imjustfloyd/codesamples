@isTest(seealldata=false)
private class portal_SelfReport_edit_Test {
      @IsTest static void portal_SelfReport_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Authorizations__c objauth = TestData.newauth(objapp.id);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Self_Reporting__c sr= new Self_Reporting__c();
          sr.Line_Events__c = objcaj.id;
          sr.Monitoring_Period_Start__c = date.today();
          sr.Monitoring_Period_End__c = date.today() + 60;
          sr.Observation_Date__c = date.today() + 61;
          sr.Number_of_Volunteers__c = 5;
          sr.Action_Taken__c = 'Test description';
          insert sr;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
          PageReference pageRef = Page.portal_SelfReport_edit;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
          ApexPages.currentPage().getParameters().put('id',sr.id);
          ApexPages.currentPage().getParameters().put('AuthoId',objauth.id);
          RecordType aRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Hand Carry Information Form' LIMIT 1];
          RecordType cRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Planting Report' LIMIT 1];          
          RecordType dRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Volunteer Monitoring Report' LIMIT 1];                    
          ApexPages.currentPage().getParameters().put('rectype',aRT.id);                            
          portal_SelfReport_edit extclass = new portal_SelfReport_edit(sc);
          extclass.rectype = aRT.id;
          ApexPages.currentPage().getParameters().put('rectype',cRT.id);                            
          portal_SelfReport_edit extclass2 = new portal_SelfReport_edit(sc);
          
          ApexPages.currentPage().getParameters().put('rectype',dRT.id);                            
          portal_SelfReport_edit extclass3 = new portal_SelfReport_edit(sc);                    
          //extclass.getattach();
          System.assert(extclass != null);        
          Test.stopTest();   
      }
}