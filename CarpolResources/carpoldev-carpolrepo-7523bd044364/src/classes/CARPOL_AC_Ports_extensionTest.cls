@isTest(seealldata=true)
private class CARPOL_AC_Ports_extensionTest {
    static testMethod void testCARPOL_AC_Ports_extension() {
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port'); 
      Application__c objapp = testData.newapplication();
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      AC__c ac = testData.newlineitem('Personal Use',objapp);
      ac.Authorization__c = objauth.id;
      update ac;
      AC__c ac3 = testData.newlineitem('Veterinary Treatment',objapp);
      ac3.Authorization__c = objauth.id;
      update ac3;
      Attachment attach = testData.newattachment(ac.Id);  
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
      Attachment att1 = testData.newattachment(ac.Id);  
      Attachment att2 = testData.newattachment(ac.Id);  
      Attachment att3 = testData.newattachment(ac.Id);                

      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
      Communication_Manager__c communmang = testData.newCommunicationManager('Letter of Denial');
      Communication_Manager__c communmang2 = testData.newCommunicationManager('Letter of No Jurisdiction');       
      Communication_Manager__c communmang3 = testData.newCommunicationManager('Permit Letter');        

      PageReference pageRef = Page.CARPOL_AC_EditPermit;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_Ports_extension cls = new CARPOL_AC_Ports_extension(sc);
        cls.savePorts();
        system.assert(cls != null);
    }
   }