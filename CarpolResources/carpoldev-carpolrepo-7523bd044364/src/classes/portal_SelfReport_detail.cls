public class portal_SelfReport_detail {

public Id SelfReportID{get;set;}

    public portal_SelfReport_detail (ApexPages.StandardController controller) {
    
     SelfReportID=ApexPages.currentPage().getParameters().get('id');
    
    }

public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }

}