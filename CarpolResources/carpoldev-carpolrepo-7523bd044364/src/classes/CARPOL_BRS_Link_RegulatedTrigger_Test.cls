/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CARPOL_BRS_Link_RegulatedTrigger_Test {

    static testMethod void test_CARPOL_BRS_Link_RegulatedTrigger() {
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);            
      Authorizations__c objauth = testData.newAuth(objapp.Id);      
      ac.Authorization__c = objauth.Id;
      update ac;
      
      Link_Regulated_Articles__c objLRA = new Link_Regulated_Articles__c();
      objLRA.Application__c = objapp.Id;
      objLRA.Authorization__c = objauth.Id;
      objLRA.Line_Item__c = ac.Id;
      insert objLRA;
      
      Test.startTest();
      objLRA.Cultivar_and_or_Breeding_Line__c = 'Test field goes here...';
      update objLRA;
      System.assert(objLRA != null);
      delete objLRA;
      Test.stopTest();      
        
    }
}