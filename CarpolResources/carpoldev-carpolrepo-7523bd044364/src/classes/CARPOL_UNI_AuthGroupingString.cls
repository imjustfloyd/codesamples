public class CARPOL_UNI_AuthGroupingString {

    public static List <AC__c> groupString(List <AC__c> LineList)
     {
     
         AC__c LineItem = LineList[0];
         List<AC__c> lineItemsToUpdate = new List<AC__c>();
         String TempString ='Program_Line_Item_Pathway__c,Thumbprint__c';
         
         If(LineItem.Program_Line_Item_Pathway__c!=null)
             {
                 List<Authorization_Grouping_Fields__c> AuthGroupFieldList = new List<Authorization_Grouping_Fields__c>();
                 AuthGroupFieldList = [SELECT Id, Program_Line_Item_Field_API__c 
                                        FROM Authorization_Grouping_Fields__c 
                                        WHERE Program_Line_Item_Pathway__c =: LineItem.Program_Line_Item_Pathway__c];
                  
                 If(AuthGroupFieldList.size()!=0)
                    {
                        for(Integer i = 0; i < AuthGroupFieldList.size(); i++)
                            {
                                tempString += ','+ AuthGroupFieldList[i].Program_Line_Item_Field_API__c;
                                
                            }
                    }
                      
                  String[] results = tempString.split(',');
                  LineItem.Authorization_Group_String__c = 'UniqueAuthGroupId:';
                 system.debug('<!!!!!!! results'+results);
                  for(Integer i = 0; i < results.size(); i++)
                    {
                        LineItem.Authorization_Group_String__c += '_'+ (String)LineItem.get(results[i]);
                            system.debug('<!!!!!!! results'+(String)LineItem.get(results[i]));
                             system.debug('<!!!!!!! results'+results[i]);
                    }
                   
                   lineItemsToUpdate.add(LineItem);
                    
                   if (!lineItemsToUpdate.isEmpty())
                      {
                          return lineItemsToUpdate;
                      }
                   else
                      {
                          return LineList;
                      }
             }
            else
              {
                  return LineList;
              }
    }
}