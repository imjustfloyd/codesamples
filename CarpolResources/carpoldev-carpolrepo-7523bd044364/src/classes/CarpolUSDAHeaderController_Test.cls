/*
* ClassName: CarpolUSDAHeaderController
* CreatedBy: Tharun Kumar
* LastModifiedBy: Dinesh Ramanadham
* LastModifiedOn: 29 July 2016
* Description: This is to Cover the code for the class: CarpolUSDAHeaderController
* *************************************************
* ---------------------------------------
* Revision History

*/
@IsTest(seeAlldata=false)
public class CarpolUSDAHeaderController_Test{
    static testMethod void testCARPOL_CarpolUSDAHeaderController(){
        
        //Added by Dinesh - 7/26
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('--->'+baseURL);
        
        string pageURL;
        PageReference page;
        
        //efiledev1 sandbox
        if(baseURL.containsIgnoreCase('efiledev1')){
            pageURL = 'https://efiledev1-aphis-efile.cs32.force.com';
        }
        //Merge Sandbox
        if(baseURL.containsIgnoreCase('eFilMerge')){
            pageURL = 'https://efilmerge-aphis-efile.cs32.force.com';
        }
        //UAT sandbox
        if(baseURL.containsIgnoreCase('eFileUAT')){
            pageURL = 'https://efileuat-aphis-efile.cs32.force.com';
        }
        //PROD
        if(baseURL.containsIgnoreCase('aphis.my.')){
            pageURL = 'https://aphis-efile.force.com';
        }

        if(pageURL.length()>0){
            page = new PageReference(pageURL);
        }else{
            system.debug('<<<<Not a valid sandbox>>>');
        }
        Test.setCurrentPageReference(page);

        CarpolUSDAHeaderController objCHC=new CarpolUSDAHeaderController();
        
        /*Commented By Dinesh -7/29
         * ApexPages.currentPage().getParameters().put('baseURLTest','aphis-efile.force');
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','t4dev');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','t4dev');

        ApexPages.currentPage().getParameters().put('baseURLTest','t3dev');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','efileuat');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','carpoldev');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','efileqa');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','efiledemo');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','t5dev');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','district12');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','efilemerge');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','jabberjay');
        
        objCHC.logout();
        ApexPages.currentPage().getParameters().put('baseURLTest','efilestage');*/
        
        objCHC.logout();
    }
}