public class portal_applicantattachment_detail {
 public Applicant_Attachments__c obj {get; set;}
 public Id LineItemId {get;set;}
 public String redirect {get;set;}
 public ID cancelid {get;set;}
 public boolean returnto {get;set;}
 public string recTypeName{get;set;}
 public Id strPathwayId {get;set;}
 public Id recordTypeId {get;set;}
 public Id appId{get;set;}
 public Id attachID{get;set;}

 public string SOPid{get;set;}
 
    public portal_applicantattachment_detail(ApexPages.StandardController controller) {
            obj = (Applicant_Attachments__c)Controller.getRecord();
            LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
            strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
            recTypeName = CARPOL_UNI_RecordType.getObjectRecordTypeName(Applicant_Attachments__c.SObjectType,obj.RecordTypeId);
            cancelid = ApexPages.currentPage().getParameters().get('LineItemId');
            redirect = ApexPages.currentPage().getParameters().get('redirect');
            SOPid = ApexPages.currentPage().getParameters().get('SOPId');
            recordTypeId = ApexPages.currentPage().getParameters().get('recordTypeId');
            attachID=ApexPages.currentPage().getParameters().get('id');
            if(redirect == 'yes'){
            returnto = true;
        } 
    }


 public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }

public PageReference edit(){
    PageReference pg = Page.portal_applicantattachment_edit;
    //pg.getParameters().put('LineItemId',LineItemId);
    pg.getParameters().put('RecordType',recordTypeId);
    
    system.debug('<<<<<!!!! Recordtype '+recordTypeId);
    system.debug('<<<<<!!!! obj.RecordTypeID '+obj.RecordTypeID);
    pg.getParameters().put('id',attachID);
    
    
    system.debug('<<<<<!!!! LineItemId when null is '+obj.Animal_Care_AC__c);
    LineItemId = obj.Animal_Care_AC__c;
    pg.getParameters().put('LineItemId',LineItemId);
    
    if(recordTypeId==NULL){
    recordTypeId = [SELECT ID FROM Recordtype WHERE ID=:obj.RecordTypeID].ID;
    
    system.debug('<<<<<!!!! Recordtype when null is '+recordTypeId);
    
    pg.getParameters().put('RecordType',recordTypeId);
    }
    //pg.getParameters().put('strPathwayId',strPathwayId);
    //pg.getParameters().put('appId',AppId);
    //pg.getParameters().put('redirect',redirect);
    system.debug('<<<<<!!!!! pg is '+pg);
    pg.setRedirect(true);
    return pg;
}

public PageReference editing(){
    PageReference pg = Page.portal_applicantattachment_edit;
    pg.getParameters().put('LineItemId',LineItemId);
    
    pg.setRedirect(true);
    return pg;
}

public PageReference upload(){
    PageReference pg = Page.Portal_UploadAttachment;
    pg.getParameters().put('Id',obj.Id);
    pg.getParameters().put('retPage','portal_applicantattachment_detail');
    system.debug('SOP ID passing to Attachments page : ' + SOPId);
    pg.setRedirect(true);
    return pg;
}


public PageReference returning(){
    PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
 
    LineItemId = obj.Animal_Care_AC__c;
    system.debug('<<<<<!!!!! LineItemId is '+ LineItemId);
    
    List<AC__c> pathway = new List<AC__c>();
    pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
    
    Id dummy;
    for(AC__c i : pathway){
    dummy = i.Program_Line_Item_Pathway__c;}
     system.debug('<<<<<!!!!! dummy is '+ dummy);
    

    pg.getParameters().put('strPathwayId',dummy);    
    pg.getParameters().put('LineId',LineItemId);
    pg.setRedirect(true);
    return pg;
}


public pageReference ret(){
    PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');
    system.debug('<<<<<<!!!!! Cancel ID'+ cancelid);
    system.debug('<<<<<<!!!!! Line item ID'+ LineItemId);
    pg.getParameters().put('LineId',LineItemId);   
    pg.getParameters().put('strPathwayId',strPathwayId);    
    pg.setRedirect(true);  
    
    system.debug('<<<<<<<<!!!!!!!!!!! page is' + pg); 
    return pg;
}
}