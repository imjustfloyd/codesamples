/* Class        : CARPOL_EXPIRATIONDATE
 * Description  : This controller is used to display the program line item pathways 
 * @author RAM KIRAN
 *         
 * ======================================================================
 *          Date        Author      Purposeå
 * Changes: 02/02/2015  RAM       Initial Version
 *         
 * ======================================================================
*/
@isTest(seealldata=false)
private class CARPOL_EXPIRATIONDATE_TEST {

 static testMethod void sharingtest(){
 
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager(); 
        testData.insertcustomsettings();

        Application__c objapp = testData.newapplication();
        AC__c objac = testData.newLineItem('Personal Use',objapp);        
        
        map<Id, AC__c> mapLinesNew = new map<Id, AC__c>();
        mapLinesNew.put(objac.Id, objac);
         
        CARPOL_EXPIRATIONDATE expdate = new CARPOL_EXPIRATIONDATE(mapLinesNew);
        expdate.EXPIRATIONDATE_INSERT();
        system.assert(expdate != null);
 }
}