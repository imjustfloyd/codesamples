public class CARPOL_UNI_Check_Required_Docs {
    public static void check(List <Applicant_Attachments__c> appattachList){
    
    list<ID> BRSlineitemlst = new list<ID>();
    list<AC__c> listlineitemToUpdate = new list<AC__c>();
    
    List <Related_Line_Decisions__c> RLDList = [SELECT ID, Decision_Matrix__c, Line_Item__c FROM Related_Line_Decisions__c WHERE Line_Item__c=:appattachList[0].Animal_Care_AC__c]; 
    
    List<AC__c> Linetoupdate = [SELECT ID, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Program__r.Name, Intended_Use__c, Intended_Use__r.Name, Status__c FROM AC__c WHERE ID=:appattachList[0].Animal_Care_AC__c LIMIT 1];
   
   if(Linetoupdate!=null){
   if(Linetoupdate.size()>0)
    {
    
    if(Linetoupdate[0].Program_Line_Item_Pathway__r.Program__r.Name != 'BRS'){
    system.debug('<###### Line Item '+Linetoupdate);
    Set<ID> DMIDs = new Set<ID>();
    for(Related_Line_Decisions__c r : RLDList){
        DMIDs.add(r.Decision_Matrix__c);
    }
    
    system.debug('<<!!! DMIDs '+DMIds);
    
    List <Regulations_Association_Matrix__c> DMList = [SELECT ID FROM Regulations_Association_Matrix__c WHERE ID IN: DMIDs];
    List<Required_Documents__c> Docslist = [SELECT ID, Decision_Matrix__c, Required_Docs__c FROM Required_Documents__c WHERE Decision_Matrix__c IN: DMList];
    system.debug('<<@@@@ DocsList '+DocsList );
    List<Applicant_Attachments__c> Lineattachments = [SELECT ID, Document_Types__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c=:appattachList[0].Animal_Care_AC__c];
    List<String> dmside = new List<String>();
    List<String> lineside = new List<String>();
    
    List<String> strlist1 = new List<String>();
    
    
    
    //------------------------------------------------------------------------------Decision Matrix Req docs
    
        
    for(Required_Documents__c req : Docslist){
        system.debug('<! req.docslist'+req.Required_Docs__c);
        
        if(req.Required_Docs__c.contains(';')){
            strlist1 = req.Required_Docs__c.split(';');
            }
        else{
            dmside.add(req.Required_Docs__c);
            }
        system.debug('<<!!! inside dmside '+dmside);
    }
    
    system.debug('<<<<!!! Dmside >>>>>>>>>>'+dmside);
    system.debug('<<<<!!! dmsize >>>>>>>>>>'+dmside.size());
    system.debug('<!!!! StrList1 '+strlist1);
    dmside.addall(strlist1);
   Set<String> dmcomp = new Set<String>();
   dmcomp.addall(dmside);
   system.debug('^^^^ dmcomp '+dmcomp);
    
    
    
    //--------------------------------------------------------------Line Item Docs
    
        
    for(Applicant_Attachments__c a: Lineattachments){
        if(a.Document_Types__c.contains(';'))
            lineside = a.Document_Types__c.split(';');
        else
            lineside.add(a.Document_Types__c);        
         system.debug('<<!!! inside lineside '+lineside);
    }
    
    system.debug('<<<<!!! lineside >>>>>>>>>>'+lineside);
    system.debug('<<<<!!! linesize >>>>>>>>>>'+lineside.size());
    //
    
    Set<String> linecomp = new Set<String>();
    linecomp.addall(lineside);
    system.debug('##### linecomp '+linecomp);
    
    
     if(linecomp.containsall(dmcomp)){
        
        ///testlinelist.Status__c = 'Ready to Submit'; 
         system.debug('<! Contains all');
     }
     else{
        //testlinelist.Status__c = 'Saved';  
        system.debug('<! Doesnt contain all');
     }
    
    //--------------------------------------------------compare both sets
    
    
if(Linetoupdate[0].Program_Line_Item_Pathway__r.Program__r.Name == 'AC')
    {

    /*if(Linetoupdate[0].Status__c == 'Saved' && Linetoupdate[0].Intended_Use__r.Name == 'Research')
    {
        if(((linecomp.contains('Health Certificate (AC7041)')) && linecomp.contains('Rabies Vaccination Certificate (AC7042)'))  || ((linecomp.contains('IACUC Approved Research Proposal')) && (linecomp.contains('Research Justification')))){
        system.debug('Reasearch stuff works');
        Linetoupdate[0].Status__c = 'Ready to Submit';
        checkRecursive.runOnce();
        update Linetoupdate[0];
        
        }
    }
    if(Linetoupdate[0].Status__c == 'Saved' && Linetoupdate[0].Intended_Use__r.Name == 'Veterinary Treatment')
    {
        if(((linecomp.contains('Health Certificate (AC7041)')) && linecomp.contains('Veterinary Treatment Agreement'))  || ((linecomp.contains('Health Certificate (AC7041)')) && linecomp.contains('Rabies Vaccination Certificate (AC7042)'))){
        system.debug('Vet stuff works ');
        Linetoupdate[0].Status__c = 'Ready to Submit';
        checkRecursive.runOnce();
        update Linetoupdate[0];
        
        }
    }*/
    if(Linetoupdate[0].Status__c == 'Saved' && Linetoupdate[0].Intended_Use__r.Name == 'Resale/Adoption')
    {
        if(((linecomp.contains('Health Certificate (AC7041)')) && linecomp.contains('Rabies Vaccination Certificate (AC7042)'))){
        system.debug('Resale stuff works ');
        system.debug('Resale stuff line item status before + '+Linetoupdate[0].Status__c);
        Linetoupdate[0].Status__c = 'Ready to Submit';
        system.debug('Resale stuff line item status after  + '+Linetoupdate[0].Status__c);
        checkRecursive.runOnce();
        system.debug('Resale stuff line item status just before updating + '+Linetoupdate[0].Status__c);
        update Linetoupdate[0];
        system.debug('Resale stuff line item status just after updating + '+Linetoupdate[0].Status__c);
        }
    }
    }
    
    else if(Linetoupdate[0].Program_Line_Item_Pathway__r.Program__r.Name != 'AC'){
    if(linecomp.containsall(dmcomp)){
        system.debug('<@@@@@ yesss');
        system.debug('!!#### Before '+Linetoupdate[0].Status__c);
        Linetoupdate[0].Status__c = 'Ready to Submit';
        checkRecursive.runOnce();
        update Linetoupdate[0];
        system.debug('!!#### After '+Linetoupdate[0].Status__c);
    }
  }

  
 }else if(Linetoupdate[0].Program_Line_Item_Pathway__r.Program__r.Name == 'BRS'){
     
             for(Applicant_Attachments__c aa:appattachList)
             {
              BRSlineitemlst.add(aa.Animal_Care_AC__c); 
             }
             
             list<Applicant_Attachments__c> SOPList = new list<Applicant_Attachments__c>();
             string SOPFilechecker;
             list<AC__c> lineitemlistafter = [SELECT ID,RecordType.Name,SOP_Status__c,(select id,recordtypeid,Total_Files__c from Applicant_Attachments__r) From AC__c WHERE ID in: BRSlineitemlst];
              system.debug('lineitemlistafter###'+lineitemlistafter);
              for (AC__c a : lineitemlistafter) {
                  	if(a.Applicant_Attachments__r.size() > 0 ){
                  	    SOPList = a.getSObjects('Applicant_Attachments__r');
                        for(Applicant_Attachments__c SP :SOPList)
                        { system.debug('SP.Total_Files__c@@@'+SP.Total_Files__c);
                          if(SP.Total_Files__c >0) {
                              SOPFilechecker = 'Ready to Submit';
                           }else {SOPFilechecker = 'Yet to Add';}
                           system.debug('SOPFilechecker@@@'+SOPFilechecker);
                        }
                          if(SOPFilechecker == 'Ready to Submit'){
                             a.SOP_Status__c = 'Ready to Submit'; 
                             listlineitemToUpdate.add(a); }else {  a.SOP_Status__c = 'Yet to Add'; 
                             listlineitemToUpdate.add(a);}
                  	}
              } 
              system.debug('listlineitemToUpdate@@@'+listlineitemToUpdate);
    	      if(listlineitemToUpdate.size()>0){
    	         update listlineitemToUpdate;
    	      }     
   }
 }  
    
    }
    }
}