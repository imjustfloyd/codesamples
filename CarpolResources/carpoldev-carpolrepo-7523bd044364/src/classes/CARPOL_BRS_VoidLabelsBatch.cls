global class CARPOL_BRS_VoidLabelsBatch implements Database.Batchable<sObject> { 
    Id BRSCourid = Schema.SObjectType.Authorizations__c.RecordTypeInfosByName.get('Biotechnology Regulatory Services - Courtesy Permit').RecordTypeId;
    Id BRSStandid = Schema.SObjectType.Authorizations__c.RecordTypeInfosByName.get('Biotechnology Regulatory Services - Standard Permit').RecordTypeId;
    Id BRSAckid = Schema.SObjectType.Authorizations__c.RecordTypeInfosByName.get('Biotechnology Regulatory Services-Acknowledgement').RecordTypeId;
    list<id> BRSRecordtyids = new list<id>{BRSCourid,BRSStandid,BRSAckid};
    string   strapproved = 'Approved';
    string  strintrtype = 'Importation';
    Date dttoday = date.today();
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string strvoidquery = 'select id,Name,Status__c,Authorization__c,Authorization__r.BRS_Introduction_Type__c,Authorization__r.Status__c,Authorization__r.RecordTypeid,Authorization__r.Expiry_Date__c  from Label__c where Authorization__r.Status__c=:strapproved and Authorization__r.BRS_Introduction_Type__c=:strintrtype and Authorization__r.RecordTypeid in:BRSRecordtyids and Authorization__r.Expiry_Date__c<:dttoday';
        return Database.getQueryLocator(strvoidquery);
    }
   
    global void execute(Database.BatchableContext BC, List<Label__c> scope) {
         for(Label__c l : scope)
         {
             l.Status__c = 'Voided-Expired';            
         } 
         update scope;
         //list<Label__c> voidlabls = scope;
         //system.debug('----executebatch--size--'+voidlabls.size());
       // system.debug('------size--'+voidlabls);
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}