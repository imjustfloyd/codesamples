@isTest(seealldata=false)
private class CARPOL_BRS_PreviewAndAttachPDFExt_Test {
      @IsTest static void CARPOL_BRS_PreviewAndAttachPDFExt_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
          ac1.Authorization__c = objauth.id;
          update ac1;                        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          ac2.Authorization__c = objauth.id;
          update ac2;                 
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);              
          ac3.Authorization__c = objauth.id;
          update ac3;                 
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           

          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 

          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_PreviewAndAttachPDF;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('Id',objauth.id);
          CARPOL_BRS_PreviewAndAttachPDFExtension extclass = new CARPOL_BRS_PreviewAndAttachPDFExtension(sc);
         // extclass.GeneratePermit();
         //extclass.AttachPermit();
          system.assert(extclass != null);
          Test.stopTest();     
      }
}