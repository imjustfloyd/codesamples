/*
// Author : Vinar Amrutia
// Created Date : 2/2/2015
// Purpose : This controller is used to display Wizard Questions pertaining to Animal Care (AC)
*/
public with sharing class CARPOL_AC_Wizard {

    public String communityURL;
    public String baseURL;
    public String currentUser { get; set; }
    public String NJLeterInformal { get; set; }
    public List<SelectOption> selValSelectOptions { get; set; }
    public List<Signature_Regulation_Junction__c> importRequirements { get; set; }
    public List<Signature_Regulation_Junction__c> additionalInformation { get; set; }
    public String selectedAnswerID { get; set; }
    public String selectedAnswerTraceQuestionID { get; set; }
    public Boolean isRadio { get; set; }
    public Boolean isPickList { get; set; }
    public Boolean showNext { get; set; }
    public Boolean showNJLetter { get; set; }
    public Boolean showLOD { get; set; }
    public Boolean isAnswerTrace { get; set; }
    public Boolean showRegulations { get; set; }
    public Boolean showImportRequirements { get; set; }
    public Boolean showAdditionalInformation { get; set; }
    public String wizardQuestion { get; set; }
    public Integer questionNo = 1;
    public String answer = '';
    public String questionID = '';
    //20151103 Jialin add for passing TPId to AC_Processing Auth
    public Boolean showNPR { get; set; }
    public String wqTPId = '';
    //20151103 Jialin add for passing TPId to AC_Processing Auth end
    public String questionType = '';
    public String optionType = '';
    public List<Wizard_Questionnaire__c> wizardQA = new List<Wizard_Questionnaire__c>();
    public List<answerTrace> answerTrace { get; set; }
    public Map<String, String> allAnswersMap = new Map<String, String>();
    public Map<String, String> allAnswersTraceMap = new Map<String, String>();
    
    public CARPOL_AC_Wizard()
    {
        if(UserInfo.getUserID() == null)
        {currentUser = 'Guest';}else{currentUser = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();}
        questionID = [SELECT Id, Name FROM Wizard_Questionnaire__c WHERE RecordType.Name = 'Animal Care (AC)' AND Type__c = 'START' LIMIT 1].ID;
        NJLeterInformal = '/apex/CARPOL_AC_LiveDogs_NJLetter_Informal';
        answerTrace = new List<answerTrace>();
        
        // Get the base URL for community salesforce instance
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('<<<<<<< Base URL: ' + baseURL + ' >>>>>>>');
        
        //UAT
        if(baseURL.contains('efileuat'))
        {communityURL = 'https://efileuat-aphis-efile.cs32.force.com';}
        
        //Dev
        if(baseURL.contains('carpoldev'))
        {communityURL = 'https://carpoldev-aphis--efilesystem.cs32.force.com';}
        
        //QA
        if(baseURL.contains('efileqa'))
        {communityURL = 'https://efileqa-aphis-efile.cs32.force.com';}
        
        //Demo
        if(baseURL.contains('efiledemo'))
        {communityURL = 'https://efiledemo-aphis-efile.cs32.force.com';}
        
        //Production
        //if(baseURL.contains('eFileUAT'))
        //{communityURL = 'https://carpoldev-aphis--efilesystem.cs32.force.com/';}
        
        getWizardQA();
    }
    
    public PageReference next() {
        
        System.Debug('<<<<<<< Line 74 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        if(selectedAnswerID != null)
        {
            System.Debug('<<<<<<< Map Answer Size : ' + allAnswersMap.size() + ' >>>>>>>');
            answerTrace ansTrc = new answerTrace();
            ansTrc.questionID = questionID;
            ansTrc.question = wizardQuestion;
            ansTrc.questionNo = questionNo;
            ansTrc.answer = allAnswersMap.get(selectedAnswerID);
            System.Debug('<<<<<<< Answer Trace Entry : ' + ansTrc + ' >>>>>>>');            
            
            allAnswersTraceMap.put(questionID, questionNo + '_' + questionID);
            answerTrace.add(ansTrc);
            System.Debug('<<<<<<< Total Answer Trace Entry : ' + answerTrace.size() + ' >>>>>>>');
            
            Cookie criteria = ApexPages.currentPage().getCookies().get('criteria');
    
            // If this is the first time the user is accessing the page, 
            // create a new cookie with name 'criteria', an initial value of '1', 
            // path 'null', maxAge '-1', and isSecure 'false'. 
            if (criteria == null) {
                criteria = new Cookie('criteria','First time cookie creation', null, 300, true);
                System.Debug('<<<<<<< Creating new Cookie : ' + criteria + ' >>>>>>>');
            } 
            else {
                // If this isn't the first time the user is accessing the page
                // create a new cookie, incrementing the value of the original count by 1
                String strQuestionsTemp = '';
                String strAnswersTemp = '';
                for(Integer i = 0; i < answerTrace.size(); i++)
                {
                    strQuestionsTemp += '[' + answerTrace[i].question + ']_';
                    strAnswersTemp += '[' + answerTrace[i].answer + ']_';
                }
                strQuestionsTemp = strQuestionsTemp.substring(0,strQuestionsTemp.length()-1);
                strAnswersTemp = strAnswersTemp.substring(0,strAnswersTemp.length()-1);
                criteria = new Cookie('criteria', strQuestionsTemp + strAnswersTemp, null, 300, true);
                System.Debug('<<<<<<< Updating existing Cookie : ' + criteria + ' >>>>>>>'); 
            }

            // Set the new cookie for the page
            ApexPages.currentPage().setCookies(new Cookie[]{criteria});
            
            questionID = selectedAnswerID.substring(selectedAnswerID.lastIndexOf('_') + 1, selectedAnswerID.length());
            questionNo += 1;
            isAnswerTrace = true;
            isRadio = false;
            isPickList = false;
            showNext = false;
            showRegulations = false;
            showImportRequirements = false;
            showAdditionalInformation = false;
            showNJLetter = false;
            showLOD = false;
            allAnswersMap.clear();
            getWizardQA();
        }
        else
        {
            // Add Error Message asking user to select a answer
        }
        return null;
    }
    
    public void getWizardQA() {
        
        wizardQA = [SELECT ID, Name, Type__c, Type_of_Options__c, Question__c, Signature__c FROM Wizard_Questionnaire__c WHERE RecordType.Name = 'Animal Care (AC)' AND ID = :questionID LIMIT 1];
        if(wizardQA.size() > 0)
        {
            wizardQuestion = wizardQA[0].Question__c;
            questionType = wizardQA[0].Type__c;
            System.Debug('<<<<<<< Type__c : ' + questionType + ' >>>>>>>'); 
            optionType = wizardQA[0].Type_of_Options__c;
            System.Debug('<<<<<<< Type_of_Options__c : ' + optionType + ' >>>>>>>'); 
            //11/08/2015 Jialin add for passing TPId to AC_Processing Auth
            wqTPId = wizardQA[0].Signature__c;
            System.Debug('<<<<<<< line 152 wqTPId : ' +wqTPId + ' >>>>>>>');
            if(questionType == 'NO PERMIT REQUIRED')
            {
                showNJLetter = true;
                showNext = false;
                showLOD = false;
                showRegulations = false;
                showImportRequirements = false;
                showAdditionalInformation = false;
            }
            if(questionType == 'NOT PERMITTED')
            {
                showNJLetter = false;
                showNext = false;
                showLOD = true;
                showRegulations = false;
                showImportRequirements = false;
                showAdditionalInformation = false;
            }
            
            if(questionType == 'PERMITTED')
            {
                showNJLetter = false;
                showNext = false;
                showLOD = false;
                getRegulations(wizardQA[0].Signature__c);
            }
            
            if(optionType == 'PICKLIST' || optionType == 'RADIO BUTTONS' || optionType == 'YES/NO')
            {
                showNext = true;
                showNJLetter = false;
                showLOD = false;
                showImportRequirements = false;
                showAdditionalInformation = false;
                getPickValSelectOptions();
            }
        }        
    }
    
    public class answerTrace
    {
        public Integer questionNo {get;set;}
        public String question {get;set;}
        public String answer {get;set;}
        public String questionID {get;set;}
    }
    
    public PageReference traceBack() {
        
        System.Debug('<<<<<<< Change Question ID : ' + selectedAnswerTraceQuestionID + ' >>>>>>>');
        
        showRegulations = false;
        String traceBackQuestionID = allAnswersTraceMap.get(selectedAnswerTraceQuestionID);
        Integer traceBackUntil = Integer.ValueOf(traceBackQuestionID.substring(0, traceBackQuestionID.lastIndexOf('_'))) - 1;
        
        Set<String> answerTraceMapKeySet = new Set<String>();
        answerTraceMapKeySet = allAnswersTraceMap.keySet();
        List<String> answerTraceMapKeys = new List<String>(answerTraceMapKeySet);
        List<answerTrace> tempAnswerTrace = new List<answerTrace>();
        
        for(Integer i = 0; i < answerTrace.size(); i++)
        {
            System.Debug('<<<<<<<  AnswerTrace : ' + answerTrace[i] + ' >>>>>>>');
            if(i+1 <= traceBackUntil)
            {
                tempAnswerTrace.add(answerTrace[i]);System.Debug('<<<<<<< Adding to Temp : ' + answerTrace[i] + ' >>>>>>>');
            }
            
        }
        
        answerTrace.clear();
        for(Integer j = 0; j < tempAnswerTrace.size(); j++)
        {
            tempAnswerTrace[j].questionNo = j + 1;
        }
        questionNo = tempAnswerTrace.size() + 1;
        if(questionNo == 1){isAnswerTrace = false;}
        answerTrace = tempAnswerTrace;
        questionID = selectedAnswerTraceQuestionID;
        getWizardQA();
        return null;
    }
    
    public PageReference startAgain() {
        //questionID = 'a0Ir00000008WFs';
        //getWizardQA();
        //return null;
        
        PageReference redirect = new PageReference('/apex/CARPOL_WelcomePage1'); 
        redirect.setRedirect(true); 
        return redirect;
    }
    
    public PageReference login() {
        
        /*Application__c application = new Application__c();
        application.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Application__c' AND Name = 'Standard Application' LIMIT 1].ID;
        Id contactID = [SELECT ID, Name, ContactID FROM User WHERE ID = :UserInfo.getUserID() LIMIT 1].ContactID;
        application.Applicant_Name__c = contactID;
        
        if(contactID == null)
        {
            application.Applicant_Name__c = '003r0000001eDbn';
        }
        application.Application_Status__c = 'Saved';
        insert(application);
        String appID = application.ID;
        System.Debug('<<<<<<< Application Inserted : ' + application.ID + ' >>>>>>');
        System.Debug('<<<<<<< Application Inserted : ' + appID + ' >>>>>>');
                
        PageReference appDetails = new PageReference('/' + application.ID);
        appDetails.setRedirect(true);
        return appDetails;*/
        
        PageReference login = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=21');
        login.setRedirect(true);
        return login;
    }
    
    public void getRegulations(String signatureID)
    {
        System.Debug('<<<<<<< Showing Regulations for Signature : ' + signatureID + ' >>>>>>>');
        showRegulations = true;
        List<Signature_Regulation_Junction__c> allRegulationsallRegulations = new List<Signature_Regulation_Junction__c>([SELECT ID, Signature__c, Regulation__r.Short_Name__c, Regulation__r.Type__c, Regulation__r.Regulation_Description__c  FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID]);
        importRequirements = new List<Signature_Regulation_Junction__c>();
        additionalInformation = new List<Signature_Regulation_Junction__c>();
        if(allRegulationsallRegulations.size() > 0)
        {
            for(Integer i = 0; i < allRegulationsallRegulations.size(); i++)
            {
                if(allRegulationsallRegulations[i].Regulation__r.Type__c == 'Import Requirements')
                {
                    showImportRequirements = true;
                    importRequirements.add(allRegulationsallRegulations[i]);
                }
                if(allRegulationsallRegulations[i].Regulation__r.Type__c == 'Additional Information')
                {
                    showAdditionalInformation = true;
                    additionalInformation.add(allRegulationsallRegulations[i]);
                }
            }
        }
    }
    
    public void getPickValSelectOptions()
    {
        System.Debug('<<<<<<< Getiting Answers from Answer Options Object of Type ' + optionType + ' >>>>>>>');
        if(optionType == 'PICKLIST' || optionType == 'RADIO BUTTONS')
        {
            List<SelectOption> options = new List<SelectOption>();
            for(Wizard_Selections__c slctVals : [SELECT ID, Name, Value__c, Wizard_Next_Question__c FROM Wizard_Selections__c WHERE Wizard_Questionnaire__c = :wizardQA[0].ID ORDER BY Display_Order__c])
            {
            
                options.add(new SelectOption(slctVals.ID + '_' + slctVals.Wizard_Next_Question__c, slctVals.Value__c));
                allAnswersMap.put(slctVals.ID + '_' + slctVals.Wizard_Next_Question__c, slctVals.Value__c);
            }
            selValSelectOptions = options;
            if(optionType == 'PICKLIST'){isPickList = true;isRadio = false;}
            else
            {
                if(optionType == 'RADIO BUTTONS'){isPickList = false;isRadio = true;}
            }
        }
        /*
        if(optionType == 'YES/NO')
        {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('1_' + wizardQA[0].If_Yes__c,'Yes'));
            options.add(new SelectOption('2_' + wizardQA[0].If_No__c,'No'));
            allAnswersMap.put('1_' + wizardQA[0].If_Yes__c,'Yes');
            allAnswersMap.put('2_' + wizardQA[0].If_No__c,'No');
            selValSelectOptions = options;
            isRadio = true;
            isPickList = false;
        } */
        System.Debug('<<<<<<< Map Answer Size : ' + allAnswersMap.size() + ' >>>>>>>');
    }

    /*public PageReference getLetterNJInformal() {
     System.Debug('<<<<<<< Line 327 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        PageReference noJurisdictionLetter_Informal = new PageReference('/apex/CARPOL_AC_LiveDogs_NJLetter_Informal');
        noJurisdictionLetter_Informal.setRedirect(true);
        return noJurisdictionLetter_Informal;
    }*/
    
    /*public PageReference getLetterNJFormal() {
          System.Debug('<<<<<<< Line 334 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        //PageReference noJurisdictionLetter_Formal = new PageReference('/apex/CARPOL_AC_LiveDogs_NJLetter_Formal?id=a00r0000000kCGb');
        PageReference noJurisdictionLetter_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=23');
        noJurisdictionLetter_Formal.setRedirect(true);
        return noJurisdictionLetter_Formal;
    }*/
    
    /*public PageReference getLODInformal() {
     System.Debug('<<<<<<< Line 342 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        PageReference LOD_Informal = new PageReference('/apex/CARPOL_Standard_Informal_Letter');
        LOD_Informal.getParameters().put('id', questionID);
        LOD_Informal.setRedirect(true);
        return LOD_Informal;
    }
    
    public PageReference getLODFormal() {
        //PageReference LOD_Formal = new PageReference('/apex/CARPOL_AC_LiveDogs_LOD_Formal?id=a00r0000000kCGb');
        PageReference LOD_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=22');
        LOD_Formal.setRedirect(true);
        return LOD_Formal;
    }*/
    
    //Jialin updated 10/26/2015 to display a global letter format
    public PageReference getLetterInformal() {
        System.Debug('<<<<<<< Line 327 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        System.Debug('<<<<<<< Line 334 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        System.Debug('<<<<<<< Line 342 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        
        PageReference Letter_Informal = new PageReference('/apex/CARPOL_Standard_Informal_Letter');
        //Jialin updated 11/18/2015 to change informal letter controller from wizard question to thumbprint
        Letter_Informal.getParameters().put('id', wqTPId);
        Letter_Informal.setRedirect(true);
        return Letter_Informal;
    }
    
    public PageReference getLetterFormal() {
        System.Debug('<<<<<<< Line 327 Selected Answer ID formal: ' + selectedAnswerID + ' >>>>>>>');
        System.Debug('<<<<<<< Line 334 Selected Answer ID formal: ' + selectedAnswerID + ' >>>>>>>');
        System.Debug('<<<<<<< Line 342 Selected Answer ID formal: ' + selectedAnswerID + ' >>>>>>>');
        System.Debug('<<<<<<< getLetterFormal wqTPId: ' + wqTPId + ' >>>>>>>');
        //PageReference Letter_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=22&TPId=wqTPId');
        PageReference Letter_Formal = new PageReference('/apex/CARPOL_AC_Processing?processID=22&TPId=wqTPId');
        Letter_Formal.setRedirect(true);
        return Letter_Formal;
        }
    
    public PageReference getLetterNJFormal(){
          System.Debug('<<<<<<< Line 334 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        //PageReference NJLetter_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=22&TPId=wqTPId');
        PageReference NJLetter_Formal = new PageReference('/apex/CARPOL_AC_Processing?processID=23&TPId=' + wqTPId);
        NJLetter_Formal.setRedirect(true);
        return NJLetter_Formal;
    }
    
    public PageReference getLODFormal() {
        System.Debug('<<<<<<< Line 342 Selected Answer ID : ' + selectedAnswerID + ' >>>>>>>');
        //PageReference LODLetter_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=22&TPId=wqTPId');
        PageReference LODLetter_Formal = new PageReference('/apex/CARPOL_AC_Processing?processID=22&TPId=' + wqTPId);
        LODLetter_Formal.setRedirect(true);
        return LODLetter_Formal;
    }
    
    public PageReference getNPRFormal() {
        //PageReference NPRLetter_Formal = new PageReference(communityURL + '/apex/CARPOL_AC_Processing?processID=22&TPId=wqTPId');
        PageReference NPRLetter_Formal = new PageReference('/apex/CARPOL_AC_Processing?processID=24&TPId=' + wqTPId);
        NPRLetter_Formal.setRedirect(true);
        return NPRLetter_Formal;
    }
}