@isTest(seealldata=false)
private class CARPOL_UNI_RegionLevel2_Test {
      @IsTest static void CARPOL_UNI_RegionLevel2_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Country__c objcountry = testData.newcountryus();
          Level_1_Region__c objL1R = new Level_1_Region__c();
          objL1r.Country__c = objcountry.Id;
          objL1r.Name = 'Alabama';
          objL1r.Level_1_Name__c = 'Alabama';
          objL1r.Level_1_Region_Code__c = 'AL';
          objL1r.Level_1_Region_Status__c = 'Active';
          insert objL1r;          
                   
          Level_2_Region__c objL2R = new Level_2_Region__c();
          objL2r.Name = 'TestMe';
          objL2r.Level_2_Name__c = 'TestMe';
          objL2r.Level_1_Region__c = objL1r.Id;
          objL2r.Level_2_Region_Status__c = 'Active';
          insert objL2r;          
          
          
          
          Test.startTest(); 

              CARPOL_UNI_RegionLevel2 extclass = new CARPOL_UNI_RegionLevel2();
              CARPOL_UNI_RegionLevel2.RegionLvl2(objL1r.Id);  
              system.assert(extclass != null);                 
          Test.stopTest();   
      }
}