@isTest(seealldata=true)
public class CARPOL_LineItemPageForApplicant_Test {
      @IsTest static void CARPOL_LineItemPageForApplicant_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Date_Issued__c = Date.today();          
          objauth.Template__c = objcm.id;
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');

          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_LineItemPageForApplicant;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objapp.id);
              ApexPages.currentPage().getParameters().put('tId',objwft.id);
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              ApexPages.currentPage().getParameters().put('LineId',li.id);                                                                                                                                                      
              CARPOL_LineItemPageForApplicant extclass = new CARPOL_LineItemPageForApplicant(ssc);
              extclass.AddConstruct();
              extclass.EditLineItem(); 
              extclass.AddSOP(); 
              extclass.AddingSOP();
              extclass.AddLocations();
              extclass.AddRegArt();
              extclass.RetApp();
              extclass.Retlineitem();              
              extclass.ReviewedConstructs();                            
              extclass.ReviewedSOPDesignProtocol();                            
              extclass.getConstruct();                                                        
              extclass.getLocation();                                                        
              extclass.getSOP();                                                                                          
              extclass.getRA();                                                        
              extclass.getRevCons();                                                        
              extclass.getRevSOP();     
              extclass.AuthId = objauth.id;
              extclass.aabutton = true;
              extclass.constructbutton = true;
              extclass.sopbutton = true; 
              extclass.locationbutton = true;
              extclass.RAbutton = true;
              extclass.prevReviewConst = true;
              extclass.recTypeName = '';
              extclass.SOPDesignProtocol = true;
              extclass.wfStatus = '';
              extclass.displayCompAgreementButton = true;
              extclass.wfLst = new List<Workflow_Task__c>();
              system.assert(extclass != null);                                                                         
          Test.stopTest();    

      }

}