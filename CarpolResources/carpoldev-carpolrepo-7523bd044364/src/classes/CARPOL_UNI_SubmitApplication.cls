public class CARPOL_UNI_SubmitApplication {
   
//NOTES ++++++++******************************
//This class does DML operations - 1.Creating Auths, 2.Updating Line Items - Need to look into with sharing capability
    
    public static Map<Authorizations__c , List<AC__c>> onAfterUpdate(List<Application__c> appList)
        {
                //applist is trigger.new
         //this class will have to be re-factored to accommodate bulkifying

                if(appList[0].Application_Status__c == 'Submitted')
                {
                        Application__c app = new Application__c();
                        app = appList[0];
                
                        List<AC__c> LineItemsList = [SELECT ID, Name, Status__c, Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.name, 
                                                    Program_Line_Item_Pathway__r.Program__c, Program_Line_Item_Pathway__r.Program__r.Name,
                                                    Program_Line_Item_Pathway__r.Expiration_Timeframe__c,Facility_Building_Type__c,
                                                    Parent_Facility_for_New_Facility__c,
                                                    Thumbprint__c, Thumbprint__r.Response_Type__c,Proposed_End_Date__c,
                                                    Authorization_Group_String__c, Purpose_of_the_Importation__c,
                                                    CITES_Needed__c, Regular_Permit_Needed__c,Proposed_Start_Date__c,
                                                    Authorization__c,Does_This_Application_Contain_CBI__c,movement_type__c,
                                                    Number_of_Labels__c,Hand_Carry__c,Type_of_Permit__c,
                                                    Regulated_Article_PackageId__c,
                                                    Approved_Facility__c, Facility_Name__c,
                                                    Thumbprint__r.Program_Prefix__c,
                                                    Thumbprint__r.Program_Prefix__r.Name,
                                                    Line_Item_Type_Hidden_Flag__c,
                                                    Is_violator__c,
                                                    select_agent__c //VV added 9/7/16 
                                                    FROM AC__c 
                                                    WHERE (Application_Number__c =:app.ID
                                            AND (Program_Line_Item_Pathway__r.Program__r.Name = 'VS' 
                                            OR Program_Line_Item_Pathway__r.Program__r.Name = 'PPQ'
                                            OR Program_Line_Item_Pathway__r.Program__r.Name = 'AC' 
                                            OR Program_Line_Item_Pathway__r.Program__r.Name = 'BRS'
                                            OR Program_Line_Item_Pathway__r.Program__r.Name = 'Operations'))];
                                          
                        
                         Map<String, AC__c> authGroupLTMap = new Map<String, AC__c>();
                         system.debug('--------LineItemsList.size() = '+LineItemsList.size());
                         for(Integer i = 0; i < LineItemsList.size(); i++)
                             {
                                authGroupLTMap.put(LineItemsList[i].Authorization_Group_String__c, LineItemsList[i]);
                            }
                        
                         system.debug('----------authGroupLTMap = '+authGroupLTMap);
                         List<AC__c> LineItemsToUpdate = new List<AC__c>();
                         Set<String> associatedAGString = authGroupLTMap.keySet(); //set of Auth grouping strings
                         List<Authorizations__c> AuthorizationsList =  new List<Authorizations__c>(); //new list of auths to be inserted
                         String QueueDeveloperName;
                         
                        
                         List<String> devnames = new List<String>{'VS_Reviewer','PPQ_Reviewer','AC_Reviewer','BRS_Reviewer','Facilities_Queue','Select_Agent_Permit_Specialist'};
                         List<Group> reviewerqueue = [select Id, DeveloperName from Group WHERE DeveloperName IN :devnames  AND Type = 'Queue' ]; // Kishore changed DeveloperName 09/26    //
                         Map<String, ID> QueueIdmap = new Map<String, ID>();
                              
                         for(Group gp : reviewerqueue)
                            {
                                QueueIdmap.put(gp.DeveloperName,gp.ID);
                            }
                        
                         Map<String, Schema.RecordTypeInfo> rtMapByName;
                         rtMapByName = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName();
                         
                         Map<String, Authorizations__c> authStringMap = new Map<String, Authorizations__c>();
                        
                             
                         for (String LTString: associatedAGString) // 1 auth group string ----- 2 line items
                            {
                            
                                    Authorizations__c auth = new Authorizations__c();
                                    auth.Application__c = app.Id;
                                    auth.Status__c = 'In Review';
                                    auth.Applicant__c = app.Applicant_Name__c;
                                    auth.AC_Applicant_Phone__c = app.Applicant_Phone__c;
                                    auth.AC_Applicant_Fax__c = app.Applicant_Fax__c;                                    
                                    auth.AC_Applicant_Email__c = app.Applicant_Email__c;
                                    auth.AC_Applicant_Address__c = app.Applicant_Address__c;
                                    auth.AC_Organization__c = app.Organization__c;
                                    auth.Authorized_User__c = app.Authorized_User__c;
                                    auth.Expiration_Timeframe__c =  string.valueOf(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Expiration_Timeframe__c);
                                    auth.CITES_Needed__c = authGroupLTMap.get(LTString).CITES_Needed__c;
                                    auth.Regular_Permit_Needed__c = authGroupLTMap.get(LTString).Regular_Permit_Needed__c;
                                    auth.Program_Pathway__c = authGroupLTMap.get(LTString).Program_Line_Item_Pathway__c;
                                    auth.Grouping_string__c = LTString;
                                    auth.Thumbprint__c = authGroupLTMap.get(LTString).Thumbprint__c;
                                    auth.Regulated_Article_PackageId__c = authGroupLTMap.get(LTString).Regulated_Article_PackageId__c;
                                    auth.Response_Type__c = authGroupLTMap.get(LTString).Thumbprint__r.Response_Type__c;
                                    auth.Authorization_Type__c = authGroupLTMap.get(LTString).Thumbprint__r.Response_Type__c;
                                    auth.BRS_Number_of_Labels__c = authGroupLTMap.get(LTString).Number_of_Labels__c;
                                    auth.BRS_Hand_Carry_For_Importation_Only__c = authGroupLTMap.get(LTString).Hand_Carry__c;
                                    auth.Containment_facility__c = authGroupLTMap.get(LTString).Approved_Facility__c;
                                    auth.Is_violator__c = authGroupLTMap.get(LTString).Is_violator__c;
                                    auth.Auth_Type_Hidden_Flag__c = authGroupLTMap.get(LTString).Line_Item_Type_Hidden_Flag__c;
                                    auth.prefix2__c = authGroupLTMap.get(LTString).Thumbprint__r.Program_Prefix__r.Name;
                                    auth.select_agent__c = authGroupLTMap.get(LTString).select_agent__c;//VV added 9/7/16
                                    
                                    
                                    
                                    if (authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Program__r.Name == 'VS') //******************************VS Auth*********************
                                        {        
                                                    auth.RecordTypeId = rtMapByName.get('Veterinary Services (VS)').getRecordTypeId();
                                                    QueueDeveloperName = 'VS_Reviewer';
                                        }
                                    if (authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Program__r.Name == 'PPQ') //******************************PPQ Auth*********************
                                        {
                                                    auth.RecordTypeId = rtMapByName.get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
                                                    QueueDeveloperName = 'PPQ_Reviewer';
                                        }
                                    if (authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Program__r.Name == 'AC') //******************************AC Auth*********************
                                        {
                                                    auth.RecordTypeId = rtMapByName.get('Animal Care (AC)').getRecordTypeId();
                                                    QueueDeveloperName = 'AC_Reviewer';
                                        }
                                    if (authGroupLTMap.get(LTString).select_agent__c){  // VV Added 9/7/16
                                                    QueueDeveloperName = 'Select_Agent_Permit_Specialist';
                                    }
                                    if (authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Program__r.Name == 'BRS')//******************************BRS Auth*********************
                                        {          system.debug('authGroupLTMap.get(LTString).type_of_permit__c#####'+authGroupLTMap.get(LTString).type_of_permit__c);
                                                    // If(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.name.contains('BRS Notification'))
                                                     If(authGroupLTMap.get(LTString).type_of_permit__c == 'Notification')
                                                        auth.RecordTypeId = rtMapByName.get('Biotechnology Regulatory Services-Acknowledgement').getRecordTypeId();
                                                   //  If(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.name == 'BRS Standard Permit')
                                                   If(authGroupLTMap.get(LTString).type_of_permit__c == 'Standard Permit')
                                                        auth.RecordTypeId = rtMapByName.get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
                                                   //  If(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.name == 'BRS Courtesy Permit')
                                                   If(authGroupLTMap.get(LTString).type_of_permit__c == 'Courtesy Permit')
                                                        auth.RecordTypeId = rtMapByName.get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
                                                    
                                                    auth.BRS_Introduction_Type__c = authGroupLTMap.get(LTString).movement_type__c;
                                                    //auth.BRS_Introduction_Type__c = authGroupLTMap.get(LTString).Purpose_of_the_Importation__c; 
                                                    auth.BRS_Hand_Carry_For_Importation_Only__c = authGroupLTMap.get(LTString).Hand_Carry__c;
                                                    auth.BRS_Number_of_Labels__c = authGroupLTMap.get(LTString).Number_of_Labels__c;
                                                    auth.Application_CBI__c = authGroupLTMap.get(LTString).Does_This_Application_Contain_CBI__c;
                                                    auth.BRS_Proposed_Start_Date__c = authGroupLTMap.get(LTString).Proposed_Start_Date__c;
                                                    auth.BRS_Proposed_End_Date__c = authGroupLTMap.get(LTString).Proposed_End_Date__c;
                                                    QueueDeveloperName = 'BRS_Reviewer';
                                         }
                                      if (authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Program__r.Name == 'Operations') //******************************Operations Auth*********************
                                        {
                                                    auth.RecordTypeId = rtMapByName.get('Facilities').getRecordTypeId();
                                                    QueueDeveloperName = 'Facilities_Queue';
                                                    if(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Name.contains('New Facility') && authGroupLTMap.get(LTString).Parent_Facility_for_New_Facility__c==null)
                                                    {
                                                        auth.Auth_Type_Hidden_Flag__c = 'Parent Facility';
                                                    }
                                                    
                                                    if(authGroupLTMap.get(LTString).Program_Line_Item_Pathway__r.Name.contains('New Facility') && authGroupLTMap.get(LTString).Parent_Facility_for_New_Facility__c!=null)
                                                    {
                                                        auth.Auth_Type_Hidden_Flag__c = 'Child Facility';
                                                    }
                                        }
                                  
                                      
                                      auth.OwnerId=QueueIdmap.get(QueueDeveloperName);
                                      authStringMap.put(LTString,auth);
                                      AuthorizationsList.add(auth);
                                
                                      //check if CITES and REGULAR permit both required - add CITES authorization
                                      if(authGroupLTMap.get(LTString).CITES_Needed__c &&
                                         authGroupLTMap.get(LTString).Regular_Permit_Needed__c)
                                      {
                                         AuthorizationsList.add(CARPOL_UNI_SubmitApplication.AddCITIESAuthorization(auth));
                                      }
                                   
                             }     
                        
             
                        insert AuthorizationsList;  //inserting the authorizations 
                        List<Facility__c> facilitylist = new List<Facility__c>();
                        Map<Authorizations__c, List<AC__c>> AuthListMap = new Map<Authorizations__c, List<AC__c>>();
                        for (AC__c Line: LineItemsList) 
                           {  

                                String gpstr = line.Authorization_Group_String__c;
                                
                                if(authStringMap.containsKey(gpstr))
                                   {
                                   line.Authorization__c = authStringMap.get(line.Authorization_Group_String__c).Id;
                                   Line.Status__c = 'Submitted';
                                      if(Line.Program_Line_Item_Pathway__r.Name.contains('New Facility')){
                                         // Line.Parent_Facility_for_New_Facility__c = 
                                         
                                      }
                                    LineItemsToUpdate.add(Line);   
                                    Authorizations__c auth = authStringMap.get(line.Authorization_Group_String__c);
                                    if(Line.Program_Line_Item_Pathway__r.Name.contains('New Facility')){
                                          //String facrectype = Line.Facility_Building_Type__c;
                                          Facility__c newfacility = new Facility__c();
                                          newfacility.Name = Line.Facility_Name__c;
                                          newfacility.RecordTypeID = Schema.Sobjecttype.Facility__c.getRecordTypeInfosByName().get(Line.Facility_Building_Type__c).getRecordTypeId();
                                          newfacility.Authorization__c = authStringMap.get(line.Authorization_Group_String__c).Id;
                                          facilitylist.add(newfacility);
                                      }
                                    if(AuthListMap.containsKey(auth)){
                                        //we need to get list from the existing record
                                        //update the list
                                        List<AC__c> MapLineList = AuthListMap.get(auth);
                                        MapLineList.add(line);
                                        AuthListMap.remove(auth);
                                        AuthListMap.put(auth,MapLineList);
                                    } else {
                                        List<AC__c> MapLineList = new List<AC__c>();
                                        MapLineList.add(line);
                                        AuthListMap.put(auth,MapLineList);                                        
                                    }

                                   }
                           }
                       
                       
                        update (LineItemsToUpdate);
                        if(!facilitylist.isEmpty()) 
                            {
                                insert facilitylist;
                            }
                         // updating the Line item 
                        return AuthListMap;
            
                         
          
                }
                else
                return null;
       }
    
     public static Authorizations__c AddCITIESAuthorization(Authorizations__c auth)
     {
         //clone the original auth
         Authorizations__c citesAuth = auth.Clone();
         //lookup CITES Thumbprint
         // assign to list to fail gracefully in case record missing
         List<Signature__c> CitiesTP = [select Id from Signature__c where name = 'PPQ--CITES--Fast Track' ];
         if(CitiesTP.size()!=0){
            citesAuth.Thumbprint__c = CitiesTP[0].Id;
            citesAuth.Response_Type__c = citesAuth.Thumbprint__r.Response_Type__c;
            citesAuth.Authorization_Type__c = citesAuth.Thumbprint__r.Response_Type__c;
            citesAuth.prefix2__c = citesAuth.Thumbprint__r.Program_Prefix__r.Name;
            citesAuth.Status__c = 'Issued';                                     
         }
         
         
         //lookup CITES thumbprint.
         return citesAuth;
     }
    
}