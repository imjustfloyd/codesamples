@isTest(seealldata=false)
private class Portal_UploadAttachment_Test {
      @IsTest static void Portal_UploadAttachment_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);

          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Attachment att = testData.newAttachment(objcaj.Id);
                    
          Genotype__c geno = new Genotype__c();
          geno.Related_Construct_Record_Number__c = objcaj.id;
          geno.Construct_Component_Name__c = 'test';
          geno.Donor__c = 'test';          
          insert geno;
                    
          Phenotype__c pheno = new Phenotype__c();
          pheno.Construct__c = objcaj.id;
          pheno.Construct_Name__c = 'test';
          pheno.Phenotypic_Category__c = 'OO-Other';
          pheno.Phenotypic_Description__c = 'test description';
          insert pheno;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.portal_phenotype_detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(pheno);
              ApexPages.currentPage().getParameters().put('id',att.id);
              ApexPages.currentPage().getParameters().put('phenoid',pheno.id);
              ApexPages.currentPage().getParameters().put('genid',geno.id);
              ApexPages.currentPage().getParameters().put('retPage','portal_construct_detail');              
              Portal_UploadAttachment extclass = new Portal_UploadAttachment();
              extclass.attachment.Name = 'Test';
              extclass.attachment.Body = blob.valueOf('test content');
              extclass.upload(); 
              extclass.cancel();               
              
              ApexPages.currentPage().getParameters().put('retPage','portal_applicantattachment_detail');              
              ApexPages.currentPage().getParameters().put('phenoid',null);              
              Portal_UploadAttachment extclass1 = new Portal_UploadAttachment();
              extclass1.attachment.Name = 'Test';
              extclass1.attachment.Body = blob.valueOf('test content');              
              extclass1.upload(); 
              extclass1.cancel(); 
              
              ApexPages.currentPage().getParameters().put('genid',null);              
              ApexPages.currentPage().getParameters().put('retPage','portal_phenotype_detail ');              
              Portal_UploadAttachment extclass2 = new Portal_UploadAttachment();
              extclass2.attachment.Name = 'Test';
              extclass2.attachment.Body = blob.valueOf('test content');              
              extclass2.upload();               
              extclass2.cancel(); 
                            
              ApexPages.currentPage().getParameters().put('retPage','portal_genotype_detail ');              
              Portal_UploadAttachment extclass3 = new Portal_UploadAttachment();
              extclass3.attachment.Name = 'Test';
              extclass3.attachment.Body = blob.valueOf('test content');              
              extclass3.upload();                             
              extclass3.cancel();               
              System.assert(extclass != null);        

          Test.stopTest();   
      }
}