@isTest(seealldata=true)
private class CARPOL_UNI_Get_Required_LI_DocsTest { 
    static testMethod void testCARPOL_UNI_Get_Required_LI_Docs() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);      
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      Attachment attach = testData.newattachment(ac.Id);           
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac.Authorization__c = objauth.Id;
      update ac;
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);  
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
      objdm.Program_Line_Item_Pathway__c = plip.id;
      insert objdm;
      
      Required_Documents__c rqdocs = new Required_Documents__c();
      rqdocs.Decision_Matrix__c = objdm.id;
      rqdocs.Required_Docs__c = 'Health;Test';
      insert rqdocs;
      
      Required_Documents__c rqdocs2 = new Required_Documents__c();
      rqdocs2.Decision_Matrix__c = objdm.id;
      rqdocs2.Required_Docs__c = 'Health';
      insert rqdocs2;
      
      Related_Line_Decisions__c objrld = new Related_Line_Decisions__c();
      objrld.Line_Item__c = ac.id;
      objrld.Decision_Matrix__c = objdm.id;
      insert objrld;
      
      CARPOL_UNI_Get_Required_LI_Docs acepreg = new CARPOL_UNI_Get_Required_LI_Docs();
      CARPOL_UNI_Get_Required_LI_Docs.getRequiredDocs(ac.id);
      System.assert(acepreg != null);
    }
}