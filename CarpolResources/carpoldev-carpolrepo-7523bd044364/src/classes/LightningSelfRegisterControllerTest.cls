/**
 * Class containing tests for LightningSelfRegisterController
 */
@IsTest public with sharing class LightningSelfRegisterControllerTest {
    @IsTest(SeeAllData=true) static void testRegistration() {
        Account objacct = new Account();
        objacct.Name = 'Global Account Test';
        objacct.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        insert objacct;
        
        LightningSelfRegisterController controller = new LightningSelfRegisterController();
        //controller.username = 'test@force.com';
        //controller.email = 'test@force.com';
        //controller.communityNickname = 'test';
        // registerUser will always return null when the page isn't accessed as a guest user
        //System.assert(controller.registerUser() == null);    
        
        //controller.password = 'abcd1234';
        //controller.confirmPassword = 'abcd123';
        LightningSelfRegisterController.selfRegister('Test' ,'User', 'test@force.com', 'abcd1234', 'abcd1234', objacct.id, '', null, '', false);
        LightningSelfRegisterController.selfRegister('Test' ,'', 'test@force.com', 'abcd1234', 'abcd1234', objacct.id, '', null, '', false);        
        LightningSelfRegisterController.selfRegister('Test' ,'User', '', 'abcd1234', 'abcd1234', objacct.id, '', null, '', false);                
        system.assert(controller != null);        
//        System.assert(controller.registerUser() == null);  
    }
}