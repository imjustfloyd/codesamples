@isTest(seealldata=false)
private class portal_LocationRecordType_Test {
      @IsTest static void portal_LocationRecordType_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();
          Program_Line_Item_Pathway__c objpathway1 = testData.newCaninePathwayType('Interstate Movement');
          Program_Line_Item_Pathway__c objpathway2 = testData.newCaninePathwayType('Interstate Movement and Release');
		  Program_Line_Item_Pathway__c objpathway3 = testData.newCaninePathwayType('Release');
		  
          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
                  
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Contact_Name1__c = 'Test',
             Day_Phone__c = '555-555-1212',
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Line_Item__c = objac.Id);
            insert objloc;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          System.runAs(usershare){
              PageReference pageRef = Page.portal_LocationRecordType;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);

              portal_LocationRecordType extclass = new portal_LocationRecordType(sc);
              extclass.getRecordTypes();          
              extclass.continueAssociate();   
              extclass.cancel();     
              
              System.assert(extclass != null);  
              //PageReference pageRef1 = Page.portal_LocationRecordType;
              //Test.setCurrentPage(pageRef1);
              objac.Movement_Type__c = 'Interstate Movement';
              update objac;
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway1.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);

              portal_LocationRecordType extclass1 = new portal_LocationRecordType(sc1);
              extclass1.getRecordTypes();          
              extclass1.continueAssociate();   
              extclass1.cancel();     
              
              System.assert(extclass1 != null); 
              objac.Movement_Type__c = 'Interstate Movement and Release';
              update objac;
              ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway2.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);

              portal_LocationRecordType extclass2 = new portal_LocationRecordType(sc2);
              extclass2.getRecordTypes();          
              extclass2.continueAssociate();   
              extclass2.cancel();     
              
              System.assert(extclass2 != null); 
              
              objac.Movement_Type__c = 'Release';
              update objac;
              ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway3.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);

              portal_LocationRecordType extclass3 = new portal_LocationRecordType(sc3);
              extclass3.getRecordTypes();          
              extclass3.continueAssociate();   
              extclass3.cancel();     
              
              System.assert(extclass3 != null);                            
          }
        
        
          //run as salesforce user
              PageReference pageRef = Page.portal_LocationRecordType;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);

              portal_LocationRecordType extclass = new portal_LocationRecordType(sc);
              extclass.getRecordTypes();          
              extclass.continueAssociate();   
              extclass.cancel();    
              ApexPages.currentPage().getParameters().put('redirect','No');
              portal_LocationRecordType extclass1 = new portal_LocationRecordType(sc);              
              extclass1.cancel();
              extclass1.recType = '';
              System.assert(extclass != null);                      
          Test.stopTest();   
      }
}