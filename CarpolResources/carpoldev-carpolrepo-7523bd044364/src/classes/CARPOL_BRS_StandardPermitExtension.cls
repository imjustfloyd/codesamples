public with sharing class CARPOL_BRS_StandardPermitExtension {



    public Authorizations__c Authorization;
    //public Construct__c construct {get; set;}
    private ID AuthorizationID;
    private string CBI;
    public string outputpanel {get;set;}
    public list<Location__c> lstOrigins {get;set;}
    public list<Location__c> lstDestinations {get;set;}
    public list<Location__c> lstReleases {get;set;}
    public list<Construct__c> lstConstruct {get;set;}
    public list<Genotype__c> lstGeno {get;set;}
    public list<Phenotype__c> lstPheno {get;set;}
    public list<Design_Protocol_Record__c> lstDesign {get;set;}
    public list<AC__c> lstLineItem {get;set;}
   // public list<Self_Reporting__c> lstSelfReport {get;set;}
    public list<Reviewer__c> lstReviewRec {get;set;}
    public list<Construct__c> lstcnstrs{get;set;}
    public list<Link_Regulated_Articles__c> lstRegArt {get; set;}
    public list<Design_Protocol_Record__c> lstSop {get;set;}
    public list<AC__c> lstCbijustif {get;set;}
    public list<Location__c> lstOandD {get;set;}
    public list<AC__c> lstlnitem {get;set;}
    // NO CBI LIST
    public list<Location__c> lstOriginsCBI {get;set;}
    public list<Location__c> lstDestinationsCBI {get;set;}
    public list<Location__c> lstReleasesCBI {get;set;}
    public list<Construct__c> lstConstructCBI {get;set;}
    public list<Genotype__c> lstGenoCBI {get;set;}
    public list<Phenotype__c> lstPhenoCBI {get;set;}
    public list<Design_Protocol_Record__c> lstDesignCBI {get;set;}
    public list<Link_Regulated_Articles__c> lstLinkRegArticles {get;set;}    
    public list<AC__c> lstLineItemCBI {get;set;}
   // public list<Self_Reporting__c> lstSelfReportCBI {get;set;}
    public list<Reviewer__c> lstReviewRecCBI {get;set;}
    public list<Construct__c> lstcnstrsCBI {get;set;}
    public list<Link_Regulated_Articles__c> lstRegArtCBI {get;set;}
    public list<Design_Protocol_Record__c> lstSopCBI {get;set;}
    public list<AC__c> lstCbijustifCBI {get;set;}
    public list<Location__c> lstOandDCBI {get;set;}
    public list<Link_Authorization_Regulation__c> lstLinkAuthorizationRegulations{get;set;}
    public List<Authorization_Junction__c> lstApplicationConditions {get;set;}
    public List<Authorization_Junction__c> lstStandardConditions {get;set;}
    public List<Authorization_Junction__c> lstSupplementalConditions {get;set;}
    public string MatchTxt;
   // First, instantiate a new Pattern object "MyPattern"
    Pattern p = Pattern.compile(
      '\\['       //literal left bracket
      + '[^\\[]*' //zero or more of anything other than a left bracket
  //    + '[^\\]]*' //zero or more of anything other than a right bracket
      + '\\]'     //literal right bracket
    );

    public PageReference generateAuthorization_PDF()
    {
        try{
        Attachment att = new Attachment(name ='Authorization_AnyUniqueName.pdf');
        PageReference AuthorizationPage = Page.CARPOL_BRS_AuthorizationPDF;
        AuthorizationPage .getParameters().put('id',AuthorizationID);
        // return page content as blob type
        // Alt: att.body = invoicePage.getContentAsPDF();
       // att.body = AuthorizationPage.getContent();
        if (Test.IsRunningTest())
           {
            att.body = Blob.valueOf('UNIT.TEST');
           }
           else
           {
            att.body = AuthorizationPage.getContent();
            }        
        // Associate with Authorizations's record Id
        att.parentid = AuthorizationID;
        insert att;
        return AuthorizationPage ;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;        
    }
    // Standard Controller Constructor
    public CARPOL_BRS_StandardPermitExtension(ApexPages.StandardController controller) {
        try
        {
            // Declare Variables
            AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
            List<AC__c> lstlnitem = [SELECT ID,Does_This_Application_Contain_CBI__c FROM AC__c WHERE Authorization__c = :AuthorizationID LIMIT 1];
            if(lstlnitem.size()>0){
                  for(AC__c l : lstlnitem){
                          if(l.Does_This_Application_Contain_CBI__c == 'Yes' ){
                                  outputpanel = 'Yes';
                          }else{
                                  outputpanel = 'No';
                          }
                   }
           }

            //else{
                   CBI = ApexPages.currentPage().getParameters().get('CBI');
                   //System.debug('<<<<<<<<<<<<<<<<< Both the Recordtype Ids did not match and skipped the loop >>>>>>>>>>>>>>>>');
           // }
            lstConstruct = new list<Construct__c>();
            lstOrigins = new list<Location__c>();
            lstDestinations = new list<Location__c>();
            lstReleases = new list<Location__c>();
            lstPheno = new list<Phenotype__c>();
            lstGeno = new list<Genotype__c>();
            lstDesign = new list<Design_Protocol_Record__c>();
            lstLineItem = new list<AC__c>();
          //  lstSelfReport = new list<Self_Reporting__c>();
            lstReviewRec = new list<Reviewer__c>();
            lstcnstrs = new List<Construct__c>();
            lstLinkRegArticles = new List<Link_Regulated_Articles__c>();
            lstSop = new List<Design_Protocol_Record__c>();
            lstCbijustif = new List<AC__c>();

            // NO CBI Lists
            lstReleasesCBI = new list<Location__c>();
            lstDestinationsCBI = new list<Location__c>();
            lstOriginsCBI = new list<Location__c>();
            lstPhenoCBI = new list<Phenotype__c>();
            lstGenoCBI = new list<Genotype__c>();
            lstDesignCBI = new list<Design_Protocol_Record__c>();
            lstConstructCBI = new list<Construct__c>();
            lstLineItemCBI = new list<AC__c>();
          //  lstSelfReportCBI = new list<Self_Reporting__c>();
            lstReviewRecCBI = new list<Reviewer__c>();
            lstcnstrsCBI = new List<Construct__c>();
            lstRegArtCBI = new List<Link_Regulated_Articles__c>();
            lstSopCBI = new List<Design_Protocol_Record__c>();
            lstCbijustifCBI = new List<AC__c>();
            lstLinkAuthorizationRegulations = new List<Link_Authorization_Regulation__c>();
            lstStandardConditions = new List<Authorization_Junction__c>();
            lstSupplementalConditions = new List<Authorization_Junction__c>();

            System.Debug('<<<<<<< Authorization ID : ' + AuthorizationID + ' >>>>>>>');
            if(AuthorizationID != null)
            {
                // Load Authorization Data

                      /*  lstOrigins = [select ID,Name,City__c, Contact_Address1__c, Contact_Address2__c, Contact_City__c, Contact_County__c, Contact_Name1__c, Contact_Name2__c,
                        Contact_state__c, Contact_Zip__c, Country__c, County__c, Critical_Habitat_Involved__c, Customer_Ref__c, Day_Phone__c, Email_1__c,
                        Email_2__c, Fax__c, Description__c,  Location_Id__c, Proposed_Planting__c, Organization__c, Org_L1__c, Org_L2__c,  Record_Id__c, Release_History__c, State__c, Status__c,
                        Street_Add1__c, Street_Add2__c, Street_Add3__c, Zip__c,Primary_Contact__r.Name,State__r.Name  from Location__c where  RecordType.DeveloperName='Origin_Location'
                        and Line_Item__c =:lstlnitem[0].id];
                        */
                         lstOrigins = [select ID,Name,City__c,Country__r.Name ,Contact_Address1__c, Contact_Address2__c, Contact_City__c, Contact_County__c, Contact_Name1__c,
                        Contact_Name2__c, Contact_state__c, Contact_Zip__c, Country__c, County__c,Country_Text__c,Primary_State_Text__c,Primary_Country_Text__c,Quantity_Text__c, Critical_Habitat_Involved__c, Customer_Ref__c,
                        Day_Phone__c, Email_1__c, Email_2__c, Fax__c, Description__c,  Location_Id__c, Proposed_Planting__c, Organization__c, Org_L1__c, Org_L2__c,  Record_Id__c,
                        Release_History__c, State__c, Status__c, Street_Add1__c, Street_Add2__c, Street_Add3__c,Street_Add4__c, Zip__c,Primary_Contact__c,State__r.Name,Primary_Contact__r.Name,
                        Level_2_Region__r.name,County_Text__c,Primary_Country__r.name from Location__c where  RecordType.DeveloperName='Origin_Location' and Line_Item__c =:lstlnitem[0].id];
                        
                     // Load Destinations

                        lstDestinations = [select ID,Name,City__c,Country__r.Name, Contact_Address1__c, Contact_Address2__c, Contact_City__c, Contact_County__c, Contact_Name1__c, Contact_Name2__c,
                        Contact_state__c, Contact_Zip__c, Country__c, County__c,County_Text__c,Country_Text__c,Primary_State_Text__c,Primary_Country_Text__c,Quantity_Text__c, Customer_Ref__c, Day_Phone__c, Email_1__c, Email_2__c, Fax__c, Description__c,  Location_Id__c,
                        Proposed_Planting__c, Organization__c, Org_L1__c, Org_L2__c, Proposed_end_date__c, Proposed_start_date__c, Quantity__c, Record_Id__c, Release_History__c, State__c,Level_2_Region__r.name,
                        Status__c, Street_Add1__c, Street_Add2__c, Street_Add3__c,Street_Add4__c, Zip__c,Primary_Contact__r.Name,State__r.Name,Primary_Country__r.name from Location__c where
                        RecordType.DeveloperName='Destination_Location' and Line_Item__c =:lstlnitem[0].id];
                        system.debug('lstDestinations+++'+lstDestinations);

                     // Load Release Sites

                        lstReleases = [select ID,Name,City__c, Contact_Address1__c, Contact_Address2__c, Contact_City__c, Contact_County__c, Contact_Name1__c, Contact_Name2__c,
                        Contact_state__c, Contact_Zip__c, Country__c, County__c,Country_Text__c, County_Text__c,Primary_State_Text__c,Primary_Country_Text__c,Quantity_Text__c,Critical_Habitat_Involved__c, Customer_Ref__c, Day_Phone__c, Email_1__c,
                        Email_2__c, Fax__c, Description__c, GPS_1__c, GPS_2__c, GPS_3__c, GPS_4__c, GPS_5__c, GPS_6__c, Location_Id__c, Proposed_Planting__c, Organization__c,Country__r.Name,
                        Org_L1__c, Org_L2__c, Quantity__c, Record_Id__c, Release_History__c, State__c, Status__c, Proposed_end_date__c, Proposed_start_date__c, Street_Add1__c, Street_Add2__c, Street_Add3__c,Street_Add4__c,
                        GPS_Coordinates_1__Latitude__s,GPS_Coordinates_1__Longitude__s,GPS_Coordinates_2__Latitude__s,GPS_Coordinates_2__Longitude__s,GPS_Coordinates_3__Latitude__s,GPS_Coordinates_3__Longitude__s,
                        GPS_Coordinates_4__Latitude__s,GPS_Coordinates_4__Longitude__s,GPS_Coordinates_5__Latitude__s,GPS_Coordinates_5__Longitude__s,GPS_Coordinates_6__Latitude__s,GPS_Coordinates_6__Longitude__s,
                        Zip__c,Primary_Contact__r.Name,State__r.Name,Level_2_Region__r.name,Primary_Country__r.name from Location__c where  RecordType.DeveloperName='Release_Location' and Line_Item__c =:lstlnitem[0].id];

                        // Load Origin and Destinations
                        lstOandD = [select ID,Name,City__c, Contact_Address1__c, Contact_Address2__c, Contact_City__c, Contact_County__c, Contact_Name1__c, Contact_Name2__c,
                        Contact_state__c, Contact_Zip__c, Country__c,Country__r.Name,County__c,Country_Text__c, County_Text__c,Primary_State_Text__c,Quantity_Text__c,Primary_Country_Text__c,Critical_Habitat_Involved__c, Customer_Ref__c, Day_Phone__c, Email_1__c,
                        Email_2__c, Fax__c, Description__c, GPS_1__c, GPS_2__c, GPS_3__c, GPS_4__c, GPS_5__c, GPS_6__c, Location_Id__c,Level_2_Region__r.name, Proposed_Planting__c, Organization__c,
                        Org_L1__c, Org_L2__c, Quantity__c, Record_Id__c, Release_History__c, State__c, Status__c, Proposed_end_date__c, Proposed_start_date__c,Street_Add1__c, Street_Add2__c, Street_Add3__c,Street_Add4__c,
                        Zip__c,Primary_Contact__r.Name,State__r.Name,Primary_Country__r.name from Location__c where  RecordType.DeveloperName='Origin_and_Destination' and Line_Item__c =:lstlnitem[0].id];

                    // Load Line Items
                        lstLineItem = [SELECT id, Name, Application_Number__c,Status__c,Purpose_of_the_Importation__c,
                                       Treatment_available_in_Country_of_Origin__c,Thumbprint__c,Breed__c,Microchip_Number__c,
                                       Breed_Description__c,Tattoo_Number__c,Color__c,Other_identifying_information__c,Sex__c,
                                       Date_of_Birth__c,Age_in_months__c,Age_yr_months__c,Importer_Last_Name__c,Importer_Mailing_CountryLU__c,
                                       Importer_First_Name__c,Importer_Mailing_Street__c,Importer_Email__c,Importer_Mailing_City__c,
                                       Importer_Phone__c,Importer_Mailing_State_ProvinceLU__c,Importer_Fax__c,Importer_Mailing_ZIP_code__c,
                                       Importer_USDA_License_Number__c,Importer_USDA_License_Expiration_Date__c,Importer_USDA_Registration_Number__c,
                                       Importer_USDA_Registration_Exp_Date__c,Exporter_Last_Name__c,Exporter_Mailing_CountryLU__c,Applicant_Instructions__c,
                                       Exporter_First_Name__c,Exporter_Mailing_Street__c,Exporter_Email__c,Exporter_Mailing_City__c,
                                       Exporter_Phone__c,Exporter_Mailing_State_ProvinceLU__c,Exporter_Fax__c,Exporter_Mailing_Zip_Postal_Code__c,
                                       Port_of_Embarkation__c,Departure_Date_and_Time__c,Port_of_Entry__c,Time_Zone_of_Departing_Place__c,Proposed_Start_Date__c,
                                       Move_out_Hawaii__c,Transporter_Type__c,Proposed_date_of_arrival__c,Transporter_Name__c,Proposed_End_Date__c,Hand_Carry__c,
                                       Arrival_Date_and_Time__c,Air_Transporter_Flight_Number__c,Time_Zone_of_Arriving_Place__c,Number_of_Labels__c,
                                       USDA_Registration_Number__c,USDA_Expiration_Date__c,USDA_License_Number__c,USDA_License_Expiration_Date__c,
                                       DeliveryRecipient_Last_Name__c,DeliveryRecipient_Mailing_CountryLU__c,DeliveryRecipient_First_Name__c,
                                       DeliveryRecipient_Mailing_Street__c,DeliveryRecipient_Email__c,DeliveryRecipient_Mailing_City__c,CBI_Justification__c,
                                       DeliveryRecipient_Phone__c,Delivery_Recipient_State_ProvinceLU__c,DeliveryRecipient_Fax__c,Does_This_Application_Contain_CBI__c,
                                       DeliveryRecipient_Mailing_Zip__c,DeliveryRecipient_USDA_License_Number__c,DeliveryRec_USDA_License_Expiration_Date__c,
                                       DeliveryRecipient_USDA_Registration_Num__c, Signature__r.Name,Authorization__r.Name,Application_Number__r.Name,Number_of_Release_Sites__c
                                       FROM AC__c WHERE Authorization__c =:AuthorizationID]; // and RecordType.DeveloperName='BRS_NOTIF_RT' ];
                                       System.debug('<<<<<<<<<< Line Items related to Authorization : ' + lstLineItem);
                                       System.debug('<<<<<<< Line Item list Size : ' + lstLineItem.size());

                      // Load Constructs related to Line Items
                           lstcnstrs = [SELECT Id,Construct_s__c,Mode_of_Transformation__c,Identifying_Line_s__c,Name,
                                              (Select  id,Name,Description__c,Genotype__c,Construct_Component__c,Construct_Component_Name__c,Donor__C,
                                               Related_Construct_Record_Number__c,RecordType.Name,Donor_2__c,Donor_3__c,Donor_4__c,Donor_5__c,Donor_List__c From Genotypes__r  ),
                                              (Select  id,Phenotypic_Category__c,Phenotypic_Description__c From Phenotypes__r ) FROM Construct__c WHERE Line_Item__c = :lstlnitem[0].id];

                      // Load Regulated Articles related to Line Items
                           lstLinkRegArticles = [SELECT Id, Name, Authorization__r.Name, Regulated_Article__r.Name, Regulated_Article__r.Scientific_Name__c,Common_Name__c,Cultivar_and_or_Breeding_Line__c FROM
                                                Link_Regulated_Articles__c WHERE Line_Item__c = :lstlnitem[0].id];
                                                
                    // Load Link_Authorization_Regulation__c from Authorizations
                    
                    lstLinkAuthorizationRegulations =  [SELECT Authorization__c,BRS_MGR_Status_Graphics__c,Id,Introduction_Type__c,Name,Regulation_Agreement__c,Regulation_Title__c,Regulation__c,Remaining_Agreement_Needed__c,Remaining_BRS_Manager_Agreement_Needed__c,
                     Remaining_ROP_Agreement_Needed__c,ROP_Status_Graphics__c,Total_Agreed__c,Total_BRS_Manager_Agreed__c,Total_BRS_Manager_Review_Conditions__c,Total_Conditions__c,Total_ROP_Reviewer_Agreed__c,Total_ROP_Review_Conditions__c 
                      FROM Link_Authorization_Regulation__c where Authorization__c =:AuthorizationID];
                      
                   //   for(Link_Authorization_Regulation__c linkAuthReg : lstLinkAuthorizationRegulations){
                      //   lstApplicationConditions = [SELECT Condition__c,Id,Link_Authorization_Regulation__c,Regulation_Title__c,Name,Link_Authorization_Regulation__r.Name,
                      //                               Link_Authorization_Regulation__r.Authorization__c FROM Application_Condition__c where  Link_Authorization_Regulation__r.Authorization__c =:AuthorizationID];
                     // }
                          lstStandardConditions = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  
                                                         from Authorization_Junction__c where Regulation__r.Type__c = 'Standard' and Authorization__c = :AuthorizationID ORDER BY Order_Number__c ASC];
                                                         
                          lstSupplementalConditions = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  
                                                         from Authorization_Junction__c where Regulation__r.Type__c = 'Supplemental Conditions' and Authorization__c = :AuthorizationID ORDER BY Order_Number__c ASC];                                                         

                    /*
                    // Load Self Operating Procedures to Line Items
                            lstSop = [SELECT Id,Name,Corrections_Required__c,Attaching_or_Entering_Design_Protocols__c,BRS_Applicant_Response__c,BRS_Applicant_Response_Email__c,
                                              Confinement_Protocols__c,Destination_or_Release_Description__c,Introduction_Type__c ,Final_Disposition_Description__c,NOTE__c,Production_Design__c,Performance_Standard__c,
                                              Reason_for_disapproval__c,Design_Protocol_Name__c,Status_Graphical__c,Status__c,Total_Attachments__c FROM Design_Protocol_Record__c
                                              WHERE Line_Item__c = :lstlnitem[0].id];


                    // load Self Reporting
                        lstSelfReport = [SELECT id,Name,Action_Additional_Comments__c,Action_Taken__c,Anticipated_Date_Time_of_Arrival__c,Anticipated_first_port_of_arrival__c,
                                        Anticipated_Harvest_Destruct_Date__c,Authorization__r.Name,Auto_Number__c,Comments__c,Construct_Record_Id_del__r.Name,Coordinates_CBI__c,
                                        Date_of_Birth__c,Destination_Airport__c,Do_attached_files_have_CBI_Information__c,Files_attached__c,Final_Destination__c,Flight__c,
                                        Latitude_1__c,Latitude_2__c,Latitude_3__c,Latitude_4__c,Latitude_5__c,Latitude_6__c,License_Plate_of_Vechile__c,Line_Item__r.Name,Longitude_1__c,
                                        Longitude_2__c,Longitude_3__c,Longitude_4__c,Longitude_5__c,Longitude_6__c,Manifest__c,Monitoring_Period_End__c,Monitoring_Period_Start__c,
                                        Name__c,No_Monitoring_Report__c,Number_of_Volunteers__c,Observation_Date__c,Organization__c,Origin_Airport__c,Planting_ID__c,Quantity_Acres__c,
                                        Release_Record_ID__r.Name,Release_site_del__r.Name,Start_Date__c,Type_of_Material_to_be_hand_carried__c,US_Passport__c,Volunteers_per__c
                                        FROM Self_Reporting__c WHERE Authorization__c =:AuthorizationID]; */

                    // load Official Review Records
                        lstReviewRec = [SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,
                                        State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,
                                        Status__c,Reviewer_Unique_Id__c,State__c FROM Reviewer__c WHERE Authorization__c =:AuthorizationID]; 
            integer intloop = 0;
            System.Debug('<<<<<<< CBI : ' + CBI + ' >>>>>>>');
            If(CBI =='Yes')
            {
                // If contains CBI Parse everything between [ ]
                               System.debug('<<<<<<<Line Items  list size ' + lstLineItem.size());
               //Get Line Items
                intloop = 0;
             while (lstLineItem.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstLineItem[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               AC__c restoredRelease = (AC__c)JSON.deserialize(MatchTxt, AC__c.class);
               lstLineItemCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI Line Item List : ' + lstLineItemCBI + ' >>>>>>>');
               }

              intloop = 0;
                            System.debug('<<<<<<<Constructs  list size ' + lstcnstrs.size());
            // Get Constructs
              while (lstcnstrs.size() > intloop) {
                // Loop through all  records
                Matcher pm = p.matcher(JSON.serialize(lstcnstrs[intloop]));
                MatchTxt = pm.replaceAll('');
                System.Debug('<<<<<<< Mtext' + intloop + ' : ' + MatchTxt + ' >>>>>>>');
               //Deserialize MatchTxt to Construct__c to access from list
               Construct__c restoredConstruct = (Construct__c)JSON.deserialize(MatchTxt, Construct__c.class);
               lstcnstrsCBI.add(restoredConstruct);
               intloop++;
               System.Debug('<<<<<<< CBI Constructs List : ' + lstcnstrsCBI + ' >>>>>>>');
               }


                System.debug('<<<<<<<SOP list size ' + lstSop.size());
               // Get Standard Operating Procedures
                intloop = 0;
             while (lstSop.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstSop[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Design_Protocol_Record__c restoredRelease = (Design_Protocol_Record__c)JSON.deserialize(MatchTxt, Design_Protocol_Record__c.class);
               lstSopCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI SOP List : ' + lstSopCBI + ' >>>>>>>');
               }
                // Loop through all origin records
                intloop = 0;
                while (lstOrigins.size() > intloop) {
                     Matcher pm = p.matcher(JSON.serialize(lstOrigins[intloop]));
                     MatchTxt = pm.replaceAll('');
                    System.Debug('<<<<<<< Origin' + intloop + ' : ' + MatchTxt + ' >>>>>>>');

                     //Deserialize MatchTxt to Location__c to access from list
                     Location__c restoredOrigin = (Location__c)JSON.deserialize(MatchTxt, Location__c.class);
                     lstOriginsCBI.add(restoredOrigin);
                     intloop++;
                     System.Debug('<<<<<<< CBI Originsi List : ' + lstOriginsCBI + ' >>>>>>>');
             }
                System.debug('<<<<<<<Destination list size ' + lstDestinations.size());
               intloop= 0;

               // Loop through all Origin and Destination records
               intloop = 0;
                while (lstOandD.size() > intloop) {
                     Matcher pm = p.matcher(JSON.serialize(lstOandD[intloop]));
                     MatchTxt = pm.replaceAll('');
                    System.Debug('<<<<<<< Origin and Destination ' + intloop + ' : ' + MatchTxt + ' >>>>>>>');

                     //Deserialize MatchTxt to Location__c to access from list
                     Location__c restoredOrigin = (Location__c)JSON.deserialize(MatchTxt, Location__c.class);
                     lstOandDCBI.add(restoredOrigin);
                     intloop++;
                     System.Debug('<<<<<<< CBI Origins and Destinations List : ' + lstOandDCBI + ' >>>>>>>');
             }


                System.debug('<<<<<<<Destination list size ' + lstDestinations.size());
               intloop= 0;
            // Get Destinations
              while (lstDestinations.size() > intloop) {
                // Loop through all Destination records
                Matcher pm = p.matcher(JSON.serialize(lstDestinations[intloop]));
                MatchTxt = pm.replaceAll('');
                System.Debug('<<<<<<< Destinations ' + intloop + ' : ' + MatchTxt + ' >>>>>>>');
               //Deserialize MatchTxt to Location__c to access from list
               Location__c restoredDestination = (Location__c)JSON.deserialize(MatchTxt, Location__c.class);
               lstDestinationsCBI.add(restoredDestination);
               System.debug('<<<<<<<<<< CBI LIST : ' + lstDestinationsCBI);
               intloop++;
               System.Debug('<<<<<<< CBI Destinations List : ' + lstDestinationsCBI + ' >>>>>>>');
               }

                             System.debug('<<<<<<<Releases list size ' + lstReleases.size());
           // Get Release Sites
              intloop = 0;
             while (lstReleases.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstReleases[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Location__c restoredRelease = (Location__c)JSON.deserialize(MatchTxt, Location__c.class);
               lstReleasesCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI Releases List : ' + lstReleasesCBI + ' >>>>>>>');
               }

             /*
                System.debug('<<<<<<<Regulated Articles  list size ' + lstRegArt.size());
               // Get Regulated Articles
                intloop = 0;
             while (lstRegArt.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstRegArt[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Link_Regulated_Articles__c restoredRelease = (Link_Regulated_Articles__c)JSON.deserialize(MatchTxt, Link_Regulated_Articles__c.class);
               lstRegArtCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI Regulated Articles List : ' + lstRegArtCBI + ' >>>>>>>');
               }
               */
                  //          System.debug('<<<<<<<Self Reports list size ' + lstSelfReport.size());
               // Get Self Reports
                intloop = 0;
          /*   while (lstSelfReport.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstSelfReport[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Self_Reporting__c restoredRelease = (Self_Reporting__c)JSON.deserialize(MatchTxt, Self_Reporting__c.class);
               lstSelfReportCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI Self Report List : ' + lstSelfReportCBI + ' >>>>>>>');
               }   */

                              System.debug('<<<<<<<Review Records  list size ' + lstReviewRec.size());
               // Get  Review
               intloop = 0;
             while (lstReviewRec.size() > intloop) {
                // Loop through all Release Sites records
                Matcher pm = p.matcher(JSON.serialize(lstReviewRec[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Reviewer__c restoredRelease = (Reviewer__c)JSON.deserialize(MatchTxt, Reviewer__c.class);
               lstReviewRecCBI.add(restoredRelease);
               intloop++;
               System.Debug('<<<<<<< CBI Review Records List : ' + lstReviewRecCBI + ' >>>>>>>');
               }

               intloop= 0;
               System.debug('<<<<<<<Designs list size ' + lstDesign.size());
        // Get Designs
          while (lstDesign.size() > intloop) {
            // Loop through all Design records
            Matcher pm = p.matcher(JSON.serialize(lstDesign[intloop]));
            MatchTxt = pm.replaceAll('');
            System.Debug('<<<<<<< Mtext' + intloop + ' : ' + MatchTxt + ' >>>>>>>');
           //Deserialize MatchTxt to Design_Protocol_Record__c to access from list
           Design_Protocol_Record__c restoredDesign = (Design_Protocol_Record__c)JSON.deserialize(MatchTxt, Design_Protocol_Record__c.class);
           lstDesignCBI.add(restoredDesign);
           intloop++;
           System.Debug('<<<<<<< CBI Designs List : ' + lstDesignCBI + ' >>>>>>>');
               }
               
      // Get Regulated Articles
            intloop = 0;
               while (lstLinkRegArticles.size()>intloop){
                Matcher pm = p.matcher(JSON.serialize(lstLinkRegArticles[intloop]));
                MatchTxt = pm.replaceAll('');

               //Deserialize MatchTxt to Location__c to access from list
               Link_Regulated_Articles__c restoredRelease = (Link_Regulated_Articles__c)JSON.deserialize(MatchTxt, Link_Regulated_Articles__c.class);
               lstLinkRegArticles.add(restoredRelease);
               intloop++;
               }               
            }
        }
       }

    catch(Exception e)
    {
        System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
    }
    }

}