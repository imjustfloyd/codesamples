@isTest
private class SomethingWentWrong_Test {

	private static testMethod void test() 
	{
	    CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c lineItem1 = testData.newLineItem('Resale',objapp);
        AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
          
        Test.startTest();
        PageReference pageRef = Page.CARPOL_SomethingWentWrong;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        CARPOL_SomethingWentWrong updateClass = new CARPOL_SomethingWentWrong(sc);
        updateClass.deleteTransaction(); //test for deletion
        updateClass.goBackApplication();
	    
	}   


}