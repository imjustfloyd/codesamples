@isTest(seealldata=false)
private class CARPOL_PPQ_UpdateSignatureFullName_Test {
      @IsTest static void insert_Test() {

          String ACTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          CARPOL_PPQ_TestDataManager testData = new CARPOL_PPQ_TestDataManager();
          testData.insertcustomsettings();
          
          Test.startTest();
          
          Domain__c objProg = new Domain__c();
          objProg.Name = 'AC';
          objProg.Active__c = true;
          insert objProg;

          Program_Prefix__c objPrefix = new Program_Prefix__c();
          objPrefix.Program__c = objProg.Id;
          objPrefix.Name = '555';
          insert objPrefix;        
          
          Signature__c objTP = new Signature__c();
          objTP.Name = 'Test AC TP';
          objTP.Recordtypeid = ACTPRecordTypeId;
          objTP.Program_Prefix__c = objPrefix.Id;
          objTP.Answer_20__c = 'Y';
          insert objTP;  
          
          objTP.Answer_20__c = 'Y Update';
          update objTP;      
          
          Signature__c objTP1 = new Signature__c();
          objTP1.Name = 'Test AC TP';
          objTP1.Recordtypeid = ACTPRecordTypeId;
          objTP1.Program_Prefix__c = objPrefix.Id;
          objTP1.Answer_1__c = 'Y';          
          objTP1.Answer_2__c = 'Y';          
          objTP1.Answer_3__c = 'Y';          
          objTP1.Answer_4__c = 'Y';          
          objTP1.Answer_5__c = 'Y';          
          objTP1.Answer_6__c = 'Y';          
          objTP1.Answer_7__c = 'Y';
          objTP1.Answer_8__c = 'Y';          
          objTP1.Answer_9__c = 'Y';          
          objTP1.Answer_10__c = 'Y';          
          objTP1.Answer_11__c = 'Y';          
          objTP1.Answer_12__c = 'Y';          
          objTP1.Answer_13__c = 'Y';          
          objTP1.Answer_14__c = 'Y';          
          objTP1.Answer_15__c = 'Y';
          objTP1.Answer_16__c = 'Y';          
          objTP1.Answer_17__c = 'Y';          
          objTP1.Answer_18__c = 'Y';
          objTP1.Answer_19__c = 'Y';                    

          insert objTP1;  
          
          system.assert(objTP1 != null);

          
          Test.stopTest();
      }
 }