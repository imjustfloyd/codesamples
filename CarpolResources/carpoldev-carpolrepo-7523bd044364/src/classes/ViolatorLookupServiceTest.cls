@IsTest
public class ViolatorLookupServiceTest { 
    @IsTest
    public static void integrationTest() {
       Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());     
       String response = ViolatorLookupService.initiateCall('John', 'Smith');
       System.Debug(response);
       System.assert(ViolatorLookupService.containsResults(response)==false);  
    }
    
    @IsTest 
    public static void testParseFoundMatch() {
        String response = '{"subjectDetails":[{"exactMatch":false,"matchCriteria":null,"subjectID":151453,"subjectType":"I","name":"John Doe","address":{"street1":"99 Canal Center Plz","city":"Alexandria","state":"VA","zip":"22314","country":"USA"},"caution":null,"phoneNumber":null,"emailAddress":null,"ssn":null,"taxId":null,"dunsNumber":null,"passportNumber":null,"birthDate":null,"gender":null}],"message":"Success. Number of records returned are 1"}';
        System.assert(ViolatorLookupService.containsResults(response));  
    }
    
    @IsTest 
    public static void testParseNoMatch() {
        String response = '{"subjectDetails":[],"message":"Success. Number of records returned are 0"}';
        System.assert(!ViolatorLookupService.containsResults(response));  
    }
}