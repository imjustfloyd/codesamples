@isTest

//Test class for fmsTreasGovServicesTcsonline
private class FmsTreas_Test {

    private static testMethod void testDetailsRequest() 
    {
    	Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.GetDetailsRequest mocktest = 
                new fmsTreasGovServicesTcsonline.GetDetailsRequest();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCompleteOnlineCollectionResponse() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponse mocktest = 
                new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponse();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testStartOnlineCollectionResponseType() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.StartOnlineCollectionResponseType mocktest = 
                new fmsTreasGovServicesTcsonline.StartOnlineCollectionResponseType();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testTransactionData() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.transactionData mocktest = 
                new fmsTreasGovServicesTcsonline.transactionData();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testClassification() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.classification mocktest = 
                new fmsTreasGovServicesTcsonline.classification();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testTransactions() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.transactions mocktest = 
                new fmsTreasGovServicesTcsonline.transactions();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testStartOnlineCollectionRequest() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest mocktest = 
                new fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCreateForceResponseType() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CreateForceResponseType mocktest = 
                new fmsTreasGovServicesTcsonline.CreateForceResponseType();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCustomCollectionFields() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CustomCollectionFields mocktest = 
                new fmsTreasGovServicesTcsonline.CustomCollectionFields();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testStartOnlineCollection() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.StartOnlineCollection mocktest = 
                new fmsTreasGovServicesTcsonline.StartOnlineCollection();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testTCSServiceFault() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.TCSServiceFault mocktest = 
                new fmsTreasGovServicesTcsonline.TCSServiceFault();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testGetDetailsResponse() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.GetDetailsResponse mocktest = 
                new fmsTreasGovServicesTcsonline.GetDetailsResponse();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCompleteOnlineCollectionRequest() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest mocktest = 
                new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testClassificationData() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.classificationData mocktest = 
                new fmsTreasGovServicesTcsonline.classificationData();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCreateForce() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CreateForce mocktest = 
                new fmsTreasGovServicesTcsonline.CreateForce();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testGetDetails() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.GetDetails mocktest = 
                new fmsTreasGovServicesTcsonline.GetDetails();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testGetDetailsResponseType() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.GetDetailsResponseType mocktest = 
                new fmsTreasGovServicesTcsonline.GetDetailsResponseType();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCompleteOnlineCollectionResponseType() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponseType mocktest = 
                new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponseType();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testStartOnlineCollectionResponse() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.StartOnlineCollectionResponse mocktest = 
                new fmsTreasGovServicesTcsonline.StartOnlineCollectionResponse();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCreateForceRequest() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CreateForceRequest mocktest = 
                new fmsTreasGovServicesTcsonline.CreateForceRequest();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCompleteOnlineCollection() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CompleteOnlineCollection mocktest = 
                new fmsTreasGovServicesTcsonline.CompleteOnlineCollection();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testCreateForceResponse() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.CreateForceResponse mocktest = 
                new fmsTreasGovServicesTcsonline.CreateForceResponse();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testTCSOnlineServicePort() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            fmsTreasGovServicesTcsonline.TCSOnlineServicePort mocktest = 
                new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testServicePortCompleteOnlineCollection() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
            fmsTreasGovServicesTcsonline.TCSOnlineServicePort mocktest = new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
            fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest req = new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest();
            mocktest.completeOnlineCollection(req);
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();

    }
    
    private static testMethod void testServicePortCreateForce() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());    
            fmsTreasGovServicesTcsonline.TCSOnlineServicePort mocktest = new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
            fmsTreasGovServicesTcsonline.CreateForceRequest req = new fmsTreasGovServicesTcsonline.CreateForceRequest();
            mocktest.createForce(req);
            system.assert(mocktest != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testServicePortStartOnlineCollection() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());    
            fmsTreasGovServicesTcsonline.TCSOnlineServicePort tcsPort = new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
            fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest req = new fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest();
            tcsPort.startOnlineCollection(req);
            system.assert(tcsPort != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    private static testMethod void testServicePortGetDetails() 
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
        try{
            Test.setMock(WebServiceMock.class, new FmsWebServiceMockImpl());
            fmsTreasGovServicesTcsonline.TCSOnlineServicePort tcsPort = new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
            fmsTreasGovServicesTcsonline.GetDetailsRequest req = new fmsTreasGovServicesTcsonline.GetDetailsRequest();
            tcsPort.getDetails(req);
            system.assert(tcsPort != null);
        } catch(Exception e){
        }
        Test.stopTest();
    }
    
    

}