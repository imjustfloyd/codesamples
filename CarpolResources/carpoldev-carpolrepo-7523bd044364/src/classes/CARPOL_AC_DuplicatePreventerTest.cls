@isTest
public class CARPOL_AC_DuplicatePreventerTest{

    static testMethod void testCARPOL_AC_DupPreventer() {
   
      //insert Account
      Account a= new Account (Name='TestAccAC20150211');
      insert a;
      
      
      //insert Contacts
      Contact con1 = new Contact (
      LastName='AC1Test20150211',
      AccountId=a.Id, 
      Phone='12182223333',
      Email='AC1Test20150211@test.com',
      MailingStreet='0211 ACTestMailingStreet',
      MailingState='Virginia',
      MailingCity='ACTestMailingCity',
      MailingPostalCode='22202',
      MailingCountry='United States' );
      insert con1;
      
      Contact con2 = new Contact (
      LastName='AC2Test20150211', 
      AccountId=a.Id, 
      Phone='12182223333',
      Email='AC2Test20150211@test.com',
      MailingStreet='0211 ACTestMailingStreet',
      MailingState='Virginia',
      MailingCity='ACTestMailingCity',
      MailingPostalCode='22202',
      MailingCountry='United States');
      insert con2;
      
      Contact con3 = new Contact (
      LastName='AC3Test20150211', 
      AccountId=a.Id, 
      Phone='12182223333',
      Email='AC3Test20150211@test.com',
      MailingStreet='0211 ACTestMailingStreet',
      MailingState='Virginia',
      MailingCity='ACTestMailingCity',
      MailingPostalCode='22202',
      MailingCountry='United States' );
      insert con3;
      
      //insert AC Application
      Application__c app = new Application__c (Applicant_Name__c=con1.Id, Exporter_Name__c=con2.Id, Importer_Name__c=con3.Id );
      insert app;
      
      RecordType facRT = [select id,Name from RecordType where SobjectType='Facility__c' and Name='Ports' Limit 1];
       RecordType impRT = [select id,Name from RecordType where SobjectType='Applicant_Contact__c' and Name='Applicant Contact' Limit 1];
        RecordType expRT = [select id,Name from RecordType where SobjectType='Applicant_Contact__c' and Name='Applicant Contact' Limit 1];
      
      Facility__c fac1= new Facility__c (RecordTypeId=facRT.Id, Name='ACTest1Facility0211',Type__c='Foreign Port');
      insert fac1;
      
       Facility__c fac2= new Facility__c (RecordTypeId=facRT.Id, Name='ACTest1Facility021122',Type__c='Domestic Port');
      insert fac2;
      
      Applicant_Contact__c assoc1= new Applicant_Contact__c (RecordTypeId=expRT.Id, Name='ExporterTest',Email_Address__c='a@test.com', First_Name__c='TestFirst');
      insert assoc1;
      
     Applicant_Contact__c assoc2= new Applicant_Contact__c (RecordTypeId=impRT.Id, Name='ImporterTest',Email_Address__c='a2@test.com', First_Name__c='TestFirst2');
      insert assoc2;
      
      Breed__c br = new Breed__c (Name = 'TestBreedName0225');
      insert br;
      
      date myDate = date.today();
      
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();

      // Seed the database with some ACs, and make sure they can be bulk inserted successfully.
      AC__c AC1 = new AC__c(Application_Number__c=app.Id, 
      Breed__c = br.Id,
      Color__c='ACTest0211Color1', 
      Date_of_Birth__c=myDate.addDays(-200),
      Sex__c='Male',
      Importer_Last_Name__c=assoc2.Id,
      Exporter_Last_Name__c=assoc1.Id,
      Status__c='Test',
      Purpose_of_the_Importation__c='Resale/Adoption',
      //Rabies_Vaccination_Date__c = myDate.addDays(-32),
      Transporter_Type__c = 'Sea',
      Transporter_Name__c = 'ACTestTransporterName0211',
      Port_of_Embarkation__c = fac1.Id,
      Port_of_Entry__c = fac2.Id,
      Proposed_date_of_arrival__c = myDate.addDays(30),
      Program_Line_Item_Pathway__c = plip.id
      //Name_Of_Person_Taking_Delivery__c = con3.Id
      );
      AC__c AC2 = new AC__c(Application_Number__c=app.Id, 
      Breed__c = br.Id,
      Color__c='ACTest0211Color2', 
      Date_of_Birth__c=myDate.addDays(-200),
      Sex__c='Male',
      Importer_Last_Name__c=assoc2.Id,
      Exporter_Last_Name__c=assoc1.Id,
      Status__c='Test',
      Purpose_of_the_Importation__c='Resale/Adoption',
      //Rabies_Vaccination_Date__c = myDate.addDays(-32),
      Transporter_Type__c = 'Sea',
      Transporter_Name__c = 'ACTestTransporterName0211',
      Port_of_Embarkation__c = fac1.Id,
      Port_of_Entry__c = fac2.Id,
      Proposed_date_of_arrival__c = myDate.addDays(30),
      Program_Line_Item_Pathway__c = plip.id
      //Name_Of_Person_Taking_Delivery__c = con3.Id
      );
      AC__c AC3 = new AC__c(Application_Number__c=app.Id, 
      Breed__c = br.Id,
      Color__c='ACTest0211Color3', 
      Date_of_Birth__c=myDate.addDays(-200),
      Sex__c='Male',
      Importer_Last_Name__c=assoc2.Id,
      Exporter_Last_Name__c=assoc1.Id,
      Status__c='Test',
      Purpose_of_the_Importation__c='Resale/Adoption',
      //Rabies_Vaccination_Date__c = myDate.addDays(-32),
      Transporter_Type__c = 'Sea',
      Transporter_Name__c = 'ACTestTransporterName0211',
      Port_of_Embarkation__c = fac1.Id,
      Port_of_Entry__c = fac2.Id,
      Proposed_date_of_arrival__c = myDate.addDays(30),
      Program_Line_Item_Pathway__c = plip.id
      //Name_Of_Person_Taking_Delivery__c = con3.Id
      );
      AC__c[] ACs = new AC__c[] {AC1, AC2, AC3};
      insert ACs;
        
      // Make sure that single row AC duplication prevention works
      //on insert.
      AC__c dup1 = new AC__c(Application_Number__c=app.Id, 
      Breed__c = br.Id,
      Color__c='ACTest0211Color3', 
      Date_of_Birth__c=myDate.addDays(-200),
      Sex__c='Male',
      Importer_Last_Name__c=assoc2.Id,
      Exporter_Last_Name__c=assoc1.Id,
      Status__c='Test',
      Purpose_of_the_Importation__c='Resale/Adoption',
      //Rabies_Vaccination_Date__c = myDate.addDays(-32),
      Transporter_Type__c = 'Sea',
      Transporter_Name__c = 'ACTestTransporterName0211',
      Port_of_Embarkation__c = fac1.Id,
      Port_of_Entry__c = fac2.Id,
      Proposed_date_of_arrival__c = myDate.addDays(30),
      Program_Line_Item_Pathway__c = plip.id      
      //Name_Of_Person_Taking_Delivery__c = con3.Id
      );
      
      try {
         insert dup1;
         
      dup1 = [Select Id, 
      Application_Number__c,
      Breed__c,
      Color__c,
      Date_of_Birth__c,
      Sex__c,
      Purpose_of_the_Importation__c,
      Transporter_Type__c,
      Transporter_Name__c,
      Port_of_Embarkation__c,
      Port_of_Entry__c,
      Proposed_date_of_arrival__c
      FROM AC__c Where Id =:dup1.Id];
         
         
      } catch (DmlException e) {
         System.assert(e.getNumDml() == 1);
         System.assert(e.getDmlIndex(0) == 0);
         System.assert(e.getDmlFields(0).size() == 1);
         //System.assert(e.getDmlFields(0)[0] == 'AC_Unique_Id__c');
         System.assert(e.getDmlMessage(0).indexOf('A live dog with the same details already exist, please check your submitted records.') > -1);
      }
        
      // Make sure that single row AC duplication prevention works
      // on update.
      AC__c dup2 = new AC__c(Application_Number__c=app.Id, 
      Breed__c = br.Id,
      Color__c='ACTest0211Color3', 
      Date_of_Birth__c=myDate.addDays(-200),
      Sex__c='Male',
      Purpose_of_the_Importation__c='Resale/Adoption',
      Status__c='Test',
      Importer_Last_Name__c=assoc2.Id,
      Exporter_Last_Name__c=assoc1.Id,
      Transporter_Type__c = 'Sea',
      Transporter_Name__c = 'ACTestTransporterName0211',
      Port_of_Embarkation__c = fac1.Id,
      Port_of_Entry__c = fac2.Id,
      Proposed_date_of_arrival__c = myDate.addDays(30),
      Program_Line_Item_Pathway__c = plip.id
      //Name_Of_Person_Taking_Delivery__c = con3.Id
      );
      
      insert dup2;
      
      dup2 = [Select Id, 
      Application_Number__c,
      Breed__c,
      Color__c,
      Date_of_Birth__c,
      Sex__c,
      Purpose_of_the_Importation__c,
      //Rabies_Vaccination_Date__c,
      Transporter_Type__c,
      Transporter_Name__c,
      Port_of_Entry__c,
      USDA_Expiration_Date__c,
      USDA_Registration_Number__c,
      Proposed_date_of_arrival__c
     // Name_Of_Person_Taking_Delivery__c
      FROM AC__c Where Id =:dup2.Id];
      
      dup2.Breed__c = br.Id;
      
      try {
         update dup2;
      } catch (DmlException e) {
         System.assert(e.getNumDml() == 1);
         System.assert(e.getDmlIndex(0) == 0);
         System.assert(e.getDmlFields(0).size() == 1);
         //System.assert(e.getDmlFields(0)[0] == 'AC_Unique_Id__c');
         System.assert(e.getDmlMessage(0).indexOf(
            'A live dog with the same details already exist, please check your submitted records.') > -1);
        }
 }   
}