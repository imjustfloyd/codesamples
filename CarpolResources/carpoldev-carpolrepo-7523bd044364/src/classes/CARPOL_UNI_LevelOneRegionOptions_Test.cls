@isTest(seealldata=false)
private class CARPOL_UNI_LevelOneRegionOptions_Test { 
      @IsTest static void CARPOL_UNI_LevelOneRegionOptions_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Country__c objcountry = testData.newcountryus();
          Level_1_Region__c objL1R = new Level_1_Region__c();
          objL1r.Country__c = objcountry.Id;
          objL1r.Name = 'Alabama';
          objL1r.Level_1_Name__c = 'Alabama';
          objL1r.Level_1_Region_Code__c = 'AL';
          objL1r.Level_1_Region_Status__c = 'Active';
          insert objL1r;          
                   
          Level_1_Region__c objL1R2 = new Level_1_Region__c();
          objL1r2.Country__c = objcountry.Id;
          objL1r2.Name = 'Texas';
          objL1r2.Level_1_Name__c = 'Texas';
          objL1r2.Level_1_Region_Code__c = 'TX';
          objL1r2.Level_1_Region_Status__c = 'Active';
          insert objL1r2;           
          List<Level_1_Region__c> lrList = new List<Level_1_Region__c>();
          lrList.add(objL1r);
          lrList.add(objL1r2);
          
          Test.startTest(); 

              CARPOL_UNI_LevelOneRegionOptions extclass = new CARPOL_UNI_LevelOneRegionOptions();
              CARPOL_UNI_LevelOneRegionOptions.Level1Regions(lrList,objcountry.id);     
              CARPOL_UNI_LevelOneRegionOptions.Level1Regions(lrList,'');                   
              System.assert(extclass != null);              
          Test.stopTest();   
      }
}