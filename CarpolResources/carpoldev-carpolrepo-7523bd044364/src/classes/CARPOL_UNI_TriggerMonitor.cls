public class CARPOL_UNI_TriggerMonitor {
/*  ====================================================  */
/*  Name: CARPOL_UNI_TriggerMonitor                       */                
/*  Copyright notice:                                     */                       
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: Helps in Avoiding Recursive Trigger Errors   */    
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION TEAM/AUTHOR  DATE    DETAIL  RELEASE/CSR      */
/*  1.0 - D12/Kishore 09/20/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */

        public static boolean runCARPOL_UNI_TaskCompleter = true;
        public static boolean runCARPOL_BRS_AuthorizationTrigger = true;
        
    }