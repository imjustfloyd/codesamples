/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class portal_selfreporting_test {

    @IsTest static void test_portal_selfreporting() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objApp = TestData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Applicant_Attachments__c objAtt = TestData.newAttach(li.Id);
          Authorizations__c objAuth = TestData.newAuth(objApp.Id);
          Attachment objA = TestData.newAttachment(li.Id);
          
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          	Test.startTest();
          	system.RunAs(usershare)
          	{
           
          	  //run as salesforce user
              PageReference pageRef = Page.portal_selfreporting;
              Test.setCurrentPage(pageRef);
              
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objAtt);
              ApexPages.currentPage().getParameters().put('id',objAtt.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);                                                                                                                                                      
              ApexPages.currentPage().getParameters().put('authid',objAuth.id);                                   
              ApexPages.currentPage().getParameters().put('RecordType',objAtt.RecordTypeId);                                                                                                                                                                
                                                                                                                                                                                            
              portal_selfreporting extclass = new portal_selfreporting(sc);
              extclass.uploadedFile = objA;
              extclass.picURL = 'http://Test.htm';
              extclass.imageid = '1-ABC3424242';
              extclass.selected = true;
              extclass.cancel(); 
              extclass.cancelappattach();   
              extclass.saveappattach();   
              extclass.upload();
              extclass.getattach();
              extclass.save();
              extclass.getRequiredDocument();
              extclass.getreturnURL();
              extclass.uploadFile();
              extclass.returntoauth();
              List<String> documenttypes = new List<String>{'Health Certificate (AC7041)', 'IACUC Approved Research Proposal', 'Rabies Vaccination Certificate (AC7042)', 'Research Justification','Veterinary Treatment Agreement'};
              extclass.flattenSelectedDocuments(documenttypes);
              ApexPages.currentPage().getParameters().put('redirect','no');
              portal_selfreporting extclass1 = new portal_selfreporting(sc);
              extclass1.cancelappattach();   
              System.assert(extclass != null);                
          	}
          	Test.stopTest();   
      }
}