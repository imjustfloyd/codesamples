public class portal_GenotypeRecordType {
public String LineItemId {get; set;}
 public genotype__c obj {get;set;}
public String rectype {get;set;}
public id recordTypeId{get;set;}
 public id id {get;set;}
 public id genid {get;set;}
public id strPathwayId {get;set;}
public id constid {get;set;}
public id LineId {get;set;}
public string redirect {get;set;}
    public portal_GenotypeRecordType(ApexPages.StandardController stdController) {
          obj = (genotype__c)stdcontroller.getRecord();
        LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
        strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
        LineId = ApexPages.currentPage().getParameters().get('LineId');
        redirect = ApexPages.currentPage().getParameters().get('redirect');
        constid = ApexPages.currentPage().getParameters().get('constid'); 
        //rectype = ApexPages.currentPage().getParameters().get('recordTypeId');
    }
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Genotype__c' ORDER BY name]) {
            for (RecordType rt : rts) {
                options.add(new SelectOption(rt.ID, rt.Name));
            }
        }
        options.add(0,new SelectOption('','--Select--'));
        return options;
    }
    public pageReference continueAssociate(){
    Profile p = [select name from Profile where id =
                 :UserInfo.getProfileId()];
    if(recordTypeId==null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter value'));
        
          }
    
    if ('APHIS Applicant'.equals(p.name)
        || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name))
        {
        PageReference customPage = Page.portal_genotype_edit;
       /* customPage.getParameters().put('LineItemId', LineId);
        customPage.getParameters().put('strPathwayId',strPathwayId);
        customPage.getParameters().put('redirect',redirect);*/
        customPage.getParameters().put('rectype',recordTypeId);
        customPage.getParameters().put('constid',constid);
        customPage.setRedirect(true);
        return customPage;
        }else{
        String hostname = ApexPages.currentPage().getHeaders().get('Host');
        String optyURL2 = 'https://'+hostname+'/'+ SObjectType.Genotype__c.keyPrefix +'/e?&RecordType='+recordTypeId+'&nooverride=1?';
        pagereference pageref = new pagereference(optyURL2);
        pageref.setredirect(true);
        return pageref;
        }

    }

    public pageReference cancel(){
        id = ApexPages.currentPage().getParameters().get('constid');
        genid = ApexPages.currentPage().getParameters().get('genid');
        if(genid != null){
                        obj.id = genid;
                        PageReference pg = Page.portal_genotype_detail;
            pg.getParameters().put('id',obj.id);
            pg.setRedirect(true);
            return pg;
        }else{
                PageReference pg = Page.portal_construct_detail;
            pg.getParameters().put('id',id);
            pg.setRedirect(true);
                return pg;
        }
       /* if(redirect == 'yes'){
            PageReference pg = Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }else{
        pageReference pg = new PageReference('/'+LineItemId);
        pg.setredirect(true);
        return pg;
        }*/
    }
}