public class portal_phenotype_detail {
    public Phenotype__c obj {get;set;}
    public ID id {get;set;}
    public Construct__c con;
    public ID conID;
    public ID lineitemid {get; set;}
    public portal_phenotype_detail(ApexPages.StandardController controller) {
                obj = (Phenotype__c)controller.getRecord();
                //con = obj.Construct__c;
                conid = obj.Construct__c;
                //obj.construct__c = ApexPages.currentPage().getParameters().get('constructid');   
                id = ApexPages.currentPage().getParameters().get('id');  
                if(conid != null)  
                   lineitemid = [select id, Line_Item__c from construct__c where id =: conid].Line_Item__c;

    }

public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }

public pageReference ret(){
          
       PageReference pg = Page.portal_construct_detail;
       //pg.getParameters().put('strPathwayId',strPathwayId); 
       pg.getParameters().put('id',conId);
       pg.setRedirect(true);
       return pg;
}

public pagereference Editphenotype(){
        PageReference pg = Page.portal_phenotype_edit;
        pg.getParameters().put('phenoid',id);
        System.debug('Id sending from detail to edit page :' + id);
        return pg; 
    }

    public pagereference upload(){
        PageReference pg = Page.Portal_UploadAttachment;
        pg.getParameters().put('phenoid',id);
        pg.getParameters().put('retPage','portal_phenotype_detail');
        System.debug('Id sending from detail to edit page :' + id);
        return pg; 
    }
    
}