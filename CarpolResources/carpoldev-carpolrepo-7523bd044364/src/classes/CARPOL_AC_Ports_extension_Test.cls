@isTest(SeeAllData=true)
public class CARPOL_AC_Ports_extension_Test{
    static testMethod void TestGetPorts2(){
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager(); 
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        AC__c ac = testData.newLineItem('Personal Use',objapp);    
        ac.Authorization__c = objauth.id;
        update ac;    
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        ac3.Authorization__c = objauth.id;
        update ac3;            
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
        Attachment attach = testData.newattachment(ac.Id);           

        
        CARPOL_AC_Ports_extension sport = new CARPOL_AC_Ports_extension(new ApexPages.StandardController(objauth));
        PageReference p = sport.getPorts();
        System.assertEquals(p, null);
    }
    static testMethod void TestGetPorts(){
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        AC__c ac = testData.newLineItem('Personal Use',objapp);    
        ac.Authorization__c = objauth.id;
        update ac;    
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        ac3.Authorization__c = objauth.id;
        update ac3;   
        
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      
        Attachment attach = testData.newattachment(ac.Id);           

        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        List<SelectOption> selPorts = new List<SelectOption>();
        selPorts.add(new SelectOption(fac.id, fac.Name));
        selPorts.add(new SelectOption(fac2.id, fac2.Name));
        
        CARPOL_AC_Ports_extension sport = new CARPOL_AC_Ports_extension(new ApexPages.StandardController(objauth));
        PageReference p = sport.getPorts();

        System.assertEquals(p, null);
        //System.assertEquals(auth.Expiration_Date__c, Date.newInstance(2015, 10, 20));
    }
    
    static testMethod void TestSavePorts(){
        try{
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id);
        AC__c ac = testData.newLineItem('Personal Use',objapp);    
        ac.Authorization__c = objauth.id;
        update ac;    
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
        ac3.Authorization__c = objauth.id;
        update ac3;   
        
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      
        Attachment attach = testData.newattachment(ac.Id);           

        
        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        
        List<Facility__c> lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :objauth.Id)];
        System.assertEquals(lstselectedPorts.size() == 0, true);
            
        CARPOL_AC_Ports_extension sport = new CARPOL_AC_Ports_extension(new ApexPages.StandardController(objauth));
        PageReference p = sport.savePorts();
       
        if(lstselectedPorts.size() == 0){
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean found=false;
            for (ApexPages.Message msg : msgs)
            {
               if (msg.getSummary()=='Please select at least 1 Port')
               {
                  found=true;
               }
            }
            //System.debug('<<<<<<<<<<<<<<<< Messages ' + msgs + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
            //System.assert(found);
            //System.assertEquals(found, true);
        }
        else {
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean found=false;
            for (ApexPages.Message msg : msgs)
            {
               if (msg.getSummary()=='Successfully Saved')
               {
                  found=true;
               }
            }
            System.debug('<<<<<<<<<<<<<<<< Messages ' + msgs + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
            //System.assert(found);
            //System.assertEquals(found, true);
        }   
        System.assertEquals(p, null);
        //System.assertEquals(auth.Expiration_Date__c, Date.newInstance(2015, 10, 20));
        }
        catch(Exception e){
            //System.assert(e.getMessage().contains('Something went wrong. Please re-try or contact your System Administrator.'));
            //System.assertEquals(p, null);
            } 
    }
    static testMethod void TestSavePorts2(){
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Applicant_Contact__c imp = testData.newappcontact(); 
        Applicant_Contact__c exp = testData.newappcontact(); 
        Applicant_Contact__c dd = testData.newappcontact(); 
        Breed__c b = testData.newbreed(); 

        RecordType rtp = [select ID from RecordType where sObjectType = 'Facility__c' AND Name = 'Ports'];

        Facility__c f = new Facility__c(Name = 'Halifax', Type__c = 'Foreign Port', RecordTypeId = rtp.Id);
        insert f;
        Facility__c port = new Facility__c(Name = 'Test', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port; 
        Facility__c port1 = new Facility__c(Name = 'Test1', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port1; 
        Facility__c port2 = new Facility__c(Name = 'Test2', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port2; 
        Facility__c port3 = new Facility__c(Name = 'Test3', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port3; 
        Facility__c port4 = new Facility__c(Name = 'Test4', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port4; 
        Facility__c port5 = new Facility__c(Name = 'Test5', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port5; 
        Account acc = testData.newAccount(AccountRecordTypeId);  
        Contact c = testData.newcontact();
        Application__c app = testData.newapplication();
        Authorizations__c auth = testData.newAuth(app.Id);

        AC__c ac = testData.newLineItem('Resale/Adoption',app);
        ac.Authorization__c = auth.id;
        update ac;            
        //List<Authorization_Junction__c> slctdports = [select Port__c, Authorization__c from Authorization_Junction__c where Authorization__c = :auth.Id];
       // List<Facility__c> lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :auth.Id)];
        //System.assertEquals(lstselectedPorts.size() > 0, true);
            
        CARPOL_AC_Ports_extension sport = new CARPOL_AC_Ports_extension(new ApexPages.StandardController(auth));
        PageReference p = sport.savePorts();
       
        System.debug('<<<<<<<<<<<<<<<< PageReference p ' + p + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.assertEquals(p, null);
        System.debug('<<<<<<<<<<<<<<<<<< Expiration Date ' + auth.Expiration_Date__c + '>>>>>>>>>>>>>>>>>>>>>');
     
    }
    
    static testMethod void TestSavePorts3(){
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Applicant_Contact__c imp = testData.newappcontact(); 
        Applicant_Contact__c exp = testData.newappcontact(); 
        Applicant_Contact__c dd = testData.newappcontact(); 
        Breed__c b = testData.newbreed(); 
        RecordType rtp = [select ID from RecordType where sObjectType = 'Facility__c' AND Name = 'Ports'];
        Facility__c f = new Facility__c(Name = 'Halifax', Type__c = 'Foreign Port', RecordTypeId = rtp.Id);
        insert f;
        Facility__c port = new Facility__c(Name = 'Test', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port; 
        Facility__c port1 = new Facility__c(Name = 'Test1', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port1; 
        Facility__c port2 = new Facility__c(Name = 'Test2', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port2; 
        Facility__c port3 = new Facility__c(Name = 'Test3', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port3; 
        Facility__c port4 = new Facility__c(Name = 'Test4', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port4; 
        Facility__c port5 = new Facility__c(Name = 'Test5', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port5; 
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        Account acc = testData.newAccount(AccountRecordTypeId); 
        Contact c = testData.newcontact();
        Application__c app = testData.newapplication();
        
        RecordType authorization  = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Authorizations__c'];
        Authorizations__c auth = testData.newAuth(app.Id);
        AC__c ac = testData.newLineItem('Resale/Adoption',app);   
        ac.Authorization__c = auth.id;
        update ac;    
        Authorization_Junction__c aj = new Authorization_Junction__c(Authorization__c = auth.Id, Port__c = port.Id);
        insert aj;
        Authorization_Junction__c aj1 = new Authorization_Junction__c(Authorization__c = auth.Id, Port__c = port1.Id);
        insert aj1;
        Authorization_Junction__c aj2 = new Authorization_Junction__c(Authorization__c = auth.Id, Port__c = port2.Id);
        insert aj2;
        Authorization_Junction__c aj3 = new Authorization_Junction__c(Authorization__c = auth.Id, Port__c = port3.Id);
        insert aj3;
        List<Authorization_Junction__c> slctdports = [select Port__c, Authorization__c from Authorization_Junction__c where Authorization__c = :auth.Id];
        List<SelectOption> slctp = new List<SelectOption>();
        List<Facility__c> lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :auth.Id)];
        
        System.assertEquals(lstselectedPorts.size() > 0, true);
            
        CARPOL_AC_Ports_extension sport = new CARPOL_AC_Ports_extension(new ApexPages.StandardController(auth));
        sport.selectedOptions = slctp;
        System.debug('<<<<<<<<<<<< Selected Options' + sport.selectedOptions.size() + '>>>>>>>>>>>>>>>>>>>');
        PageReference p = sport.savePorts();
       
        System.debug('<<<<<<<<<<<<<<<< PageReference p ' + p + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.assertEquals(p, null);
        System.debug('<<<<<<<<<<<<<<<<<< Expiration Date ' + auth.Expiration_Date__c + '>>>>>>>>>>>>>>>>>>>>>');
        //System.debug('<<<<<<<<<<<<<<<<<< Expiration1 Date ' + auth1.Expiration_Date__c + '>>>>>>>>>>>>>>>>>>>>>');
    }
}