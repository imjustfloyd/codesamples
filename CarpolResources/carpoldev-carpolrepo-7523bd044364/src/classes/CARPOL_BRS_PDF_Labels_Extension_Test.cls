/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class CARPOL_BRS_PDF_Labels_Extension_Test {

	@isTest
    static void test_PDF_Labels_Extension()
    {
        // TO DO: implement unit test
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);              
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          update ac1;
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
    
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
           Level_2_Region__c = l2.id, 
           GPS_1__c = 'GPS_1__c',
           GPS_2__c = 'GPS_2__c',
           GPS_3__c = 'GPS_3__c',
           GPS_4__c = 'GPS_4__c',
           GPS_5__c = 'GPS_5__c',
           GPS_6__c = 'GPS_6__c',
           Contact_Name1__c = 'Test', 
           Day_Phone__c = '555-555-1212'
           );
           insert objloc;          
          
          Label__c lbl = new Label__c();
          lbl.Name = 'Test Label';
          lbl.Authorization__c = objauth.id;
          lbl.Notification_Expiration_Date__c = date.today()+5;
          lbl.Origin_Address__c = objloc.id;
          insert lbl;

          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_PDF_Labels;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('Id',objauth.id);
          CARPOL_BRS_PDF_Labels_Extension extclass = new CARPOL_BRS_PDF_Labels_Extension(sc);          
          system.assert(extclass != null);
          Test.stopTest();     
    }
}