@isTest(seealldata=false)
public class CARPOL_VS_AppFeeDetail_Test {
      @IsTest static void CARPOL_VS_AppFeeDetail_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          objapp.Payment_Type__c = 'Charge No Fee';
          update objapp;

          Application__c objapp1 = testData.newapplication();
          objapp1.Payment_Type__c = 'Money Order';
          update objapp1;          

          Application__c objapp2 = testData.newapplication();
          objapp2.Payment_Type__c = 'Mail-in Check';
          update objapp2;          
          
          Application__c objapp3 = testData.newapplication();
          objapp3.Payment_Type__c = 'APHIS User Account';
          update objapp3;          
          
                    
          /*AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          VS__c objvs = testData.newVS(objcont.Id, objapp.Id);*/
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_VS_AddIngredients;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.currentPage().getParameters().put('id',objapp.id);

              CARPOL_VS_AppFeeDetail_extension extclass = new CARPOL_VS_AppFeeDetail_extension(sc);
              extclass.goBackApplication();
              system.assert(extclass != null);
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objapp1);              
              ApexPages.currentPage().getParameters().put('id',objapp1.id);
              CARPOL_VS_AppFeeDetail_extension extclass1 = new CARPOL_VS_AppFeeDetail_extension(sc1);              
              system.assert(extclass1 != null);              
              ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objapp2);              
              ApexPages.currentPage().getParameters().put('id',objapp2.id);
              CARPOL_VS_AppFeeDetail_extension extclass2 = new CARPOL_VS_AppFeeDetail_extension(sc2);                            
              system.assert(extclass2 != null);              
              ApexPages.Standardcontroller sc3 = new ApexPages.Standardcontroller(objapp3);              
              ApexPages.currentPage().getParameters().put('id',objapp3.id);
              CARPOL_VS_AppFeeDetail_extension extclass3 = new CARPOL_VS_AppFeeDetail_extension(sc3);              
              system.assert(extclass3 != null);              
          Test.stopTest();    

      }

}