@isTest(seealldata=false)
private class GenericHistoryClassTest {
    static testMethod void testGenericHistoryClass() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      map<id,SObject> oldmap = new map<id,SObject>();
      Account objacct = new Account();
      objacct.Name = 'AC account';
      //objacct.RecordTypeId = '012r000000007C4'; 
      objacct.RecordTypeId = AccountRecordTypeId;    
      insert objacct;
      Contact objcont = new Contact();
      objcont.FirstName = 'FirstName';
      objcont.LastName = 'LastName';
      objcont.Email = 'test@email.com';
      objcont.AccountId = objacct.id;
      objcont.MailingStreet = 'Mailing Street';
      objcont.MailingCity = 'Mailing City';
      objcont.MailingState = 'Ohio';
      objcont.MailingCountry = 'United States';
      objcont.MailingPostalCode = 'Mailing Postal Code';
      insert objcont;
      
      Breed__c brd = new Breed__c();
      brd.Name='New Breed';
      insert brd;
      Applicant_Contact__c apcont = new Applicant_Contact__c();
      apcont.First_Name__c = 'apcont';
      apcont.Email_Address__c = 'apcont@test.com';
      apcont.Name = 'Associated Contacts';
      //apcont.RecordTypeId = '012r000000007Iq';
      apcont.RecordTypeId = ImpapcontRecordTypeId;
      //apcont.USDA_License_Expiration_Date__c = date.today()+30;
      //apcont.USDA_Registration_Expiration_Date__c = date.today()+40;
      insert apcont;
      Applicant_Contact__c apcont2 = new Applicant_Contact__c();
      apcont2.First_Name__c = 'apcont2';
      apcont2.Email_Address__c = 'apcont2@test.com';
      apcont2.Name = 'Associated Contacts2';
      //apcont.USDA_License_Expiration_Date__c = date.today()+30;
      //apcont.USDA_Registration_Expiration_Date__c = date.today()+40;
      insert apcont2;
      Facility__c fac = new Facility__c();
      //fac.RecordTypeId = '012r0000000065q';
      fac.RecordTypeId = PortsFacRecordTypeId;
      fac.Name = 'entryport';
      insert fac;
      Facility__c fac2 = new Facility__c();
      //fac2.RecordTypeId = '012r0000000065q';
      fac2.RecordTypeId = PortsFacRecordTypeId;
      fac2.Name = 'Embarkationport';
      fac2.Type__c = 'Foreign Port';
      insert fac2;
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      
      Application__c objapp = new Application__c();
      objapp.Applicant_Name__c = objcont.id;
      //objapp.Recordtypeid = '012r0000000068G';
      objapp.Recordtypeid = ACAppRecordTypeId;
      objapp.Application_Status__c = 'Open';
      insert objapp;
      
      AC__c ac = new AC__c();
      ac.Application_Number__c = objapp.id;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.id;
      ac.Port_of_Embarkation__c = fac2.id;
      ac.Date_of_Birth__c = date.today()-30;
      ac.Breed__c = brd.id;
      ac.Importer_Last_Name__c = apcont.id;
      ac.Exporter_Last_Name__c = apcont2.id;
      ac.Status__c = 'Submitted';
      ac.Purpose_of_the_Importation__c = 'Personal Use';
      ac.Proposed_date_of_arrival__c = date.today()+15;
      ac.Transporter_Type__c = 'Ground';
      ac.Color__c = 'Brown';
      ac.Sex__c = 'Male';
      ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
      ac.Status__c = 'Saved';
      ac.Program_Line_Item_Pathway__c = plip.id;
      insert ac;
      
          
      AC__c ac3 = new AC__c();
      ac3.Application_Number__c = objapp.id;
      ac3.Departure_Time__c = date.today()+11;
      ac3.Arrival_Time__c = date.today()+15;
      ac3.Port_of_Entry__c = fac.id;
      ac3.Port_of_Embarkation__c = fac2.id;
      ac3.Date_of_Birth__c = date.today()-30;
      ac3.Breed__c = brd.id;
      ac3.Importer_Last_Name__c = apcont.id;
      ac3.Exporter_Last_Name__c = apcont2.id;
      ac3.Status__c = 'Submitted';
      ac3.Purpose_of_the_Importation__c = 'Veterinary Treatment';
      ac3.Treatment_available_in_Country_of_Origin__c = 'No';
      ac3.Proposed_date_of_arrival__c = date.today()+15;
      ac3.Transporter_Type__c = 'Ground';
      ac3.Color__c = 'Brown';
      ac3.Sex__c = 'Male';
      ac3.Treatment_available_in_Country_of_Origin__c  = 'No';    
      ac3.Status__c = 'Saved';
      ac3.Program_Line_Item_Pathway__c = plip.id;    
      insert ac3;
            
      
      Attachment attach = new Attachment();
      attach.Body = blob.valueof('Test doc');
      attach.Name = 'Test doc';
      attach.ParentId = ac.id;
      insert attach;
      
      Regulation__c objreg1 = new Regulation__c();
      objreg1.Status__c = 'Active';
      objreg1.Type__c = 'Import Requirements';
      objreg1.Custom_Name__c = 'testcustname1';
      objreg1.Short_Name__c = 't1';
      objreg1.Title__c = 'title1';
      objreg1.Regulation_Description__c = 'descp1';
      objreg1.Sub_Type__c = 'Import Permit Requirements';
      objreg1.RecordTypeID = ACRegRecordTypeId;
      insert objreg1;
      Regulation__c objreg2 = new Regulation__c();
      objreg2.Status__c = 'Active';
      objreg2.Type__c = 'Additional Information';
      objreg2.Custom_Name__c = 'testcustname2';
      objreg2.Short_Name__c = 't2';
      objreg2.Title__c = 'title2';
      objreg2.Regulation_Description__c = 'descp2';
      objreg2.Sub_Type__c = 'Commercial Consignment Requirements';
      objreg2.RecordTypeID = ACRegRecordTypeId;
      insert objreg2;
      Regulation__c objreg3 = new Regulation__c();
      objreg3.Status__c = 'Active';
      objreg3.Type__c = 'Instruction for CBP Officers';
      objreg3.Custom_Name__c = 'testcustname3';
      objreg3.Short_Name__c = 't3';
      objreg3.Title__c = 'title3';
      objreg3.Regulation_Description__c = 'descp2';
      objreg3.Sub_Type__c = 'Pre-Clearance Requirements';
      objreg3.RecordTypeID = ACRegRecordTypeId;
      insert objreg3;
      Authorizations__c objauth = new Authorizations__c();
      objauth.Application__c = objapp.id;
      objauth.RecordTypeID = ACauthRecordTypeId;
      insert objauth;
      oldmap.put(objauth.id,objauth);
      //objauth.Applicant_Alternate_Email__c='test2@email.com';
      //update objauth;
      list<Authorizations__c> newauth = new list<Authorizations__c>();
      newauth.add(objauth);
      GenericHistoryClass.CreateHistoryRecord(newauth,oldmap,'Authorizations__c');
      oldmap = new map<id,SObject>();
      list<Location__c> locList = new list<Location__c>();
        Trade_Agreement__c ta = new Trade_Agreement__c();
        ta.name='test';
        ta.Trade_Agreement_code__c='12';
        insert ta;

        country__c c = new country__c();
        c.Name='Test';
        c.country_code__c='12';
        c.Trade_Agreement__c=ta.Id;
        insert c;

        Level_1_Region__c lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=c.Id;
        insert lr;

        level_2_Region__c l2 = new level_2_Region__c();
        l2.Name='Test';
        l2.level_1_Region__c=lr.Id;
    
            Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212'
             );
            insert objloc;      
      
      
      locList.add(objloc);
      oldmap.put(objloc.id,objloc);      
      GenericHistoryClass.CreateHistoryRecord(locList,oldmap,'Location__c');      
      oldmap = new map<id,SObject>();      
      list<Signature__c> sigList = new list<Signature__c>();
      Signature__c thumb = testData.newThumbPrint();
      sigList.add(thumb);
      oldmap.put(thumb.id,thumb);
      GenericHistoryClass.CreateHistoryRecord(sigList,oldmap,'Signature__c');            
      oldmap = new map<id,SObject>();      
      list<Design_Protocol_Record__c> dprList = new list<Design_Protocol_Record__c>();  
      Design_Protocol_Record__c dpr = new Design_Protocol_Record__c();
      dpr.Status__c = 'Waiting on Customer';
      dpr.Associate_Application__c = objapp.id;
      dpr.Line_Item__c = ac3.id;
      insert dpr;           
      dprList.add(dpr);
      oldmap.put(dpr.id,dpr);           
      GenericHistoryClass.CreateHistoryRecord(dprList,oldmap,'Design_Protocol_Record__c');         
      oldmap = new map<id,SObject>();      
      list<Construct__c> cstList = new list<Construct__c>();
      Construct__c objcaj = new Construct__c();
      objcaj.Construct_s__c = 'Test Construct 1';
      objcaj.Line_Item__c = ac3.id;
      insert objcaj; 
      cstList.add(objcaj);
      oldmap.put(objcaj.id,objcaj);           
      GenericHistoryClass.CreateHistoryRecord(cstList,oldmap,'Construct__c');                  
      oldmap = new map<id,SObject>();      
      list<Genotype__c> genoList = new list<Genotype__c>();
      Genotype__c geno = new Genotype__c();
      geno.Related_Construct_Record_Number__c = objcaj.id;
      geno.Construct_Component_Name__c = 'test';
      geno.Donor__c = 'test';
      insert geno;      
      genoList.add(geno);
      oldmap.put(geno.id,geno);                 
      GenericHistoryClass.CreateHistoryRecord(genoList,oldmap,'Genotype__c');                        
      oldmap = new map<id,SObject>();      
      list<Self_Reporting__c> srList = new list<Self_Reporting__c>();
      Self_Reporting__c objlra = new Self_Reporting__c();
      objlra.Line_Events__c = objcaj.id;
      objlra.Monitoring_Period_Start__c = date.today();
      objlra.Monitoring_Period_End__c = date.today() + 60;
      objlra.Observation_Date__c = date.today() + 61;
      objlra.Number_of_Volunteers__c = 5;
      objlra.Action_Taken__c = 'Test description';
      objlra.Authorization__c = objauth.id;          
      insert objlra;      
      srList.add(objlra);
      oldmap.put(objlra.id,objlra);
      GenericHistoryClass.CreateHistoryRecord(srList,oldmap,'Self_Reporting__c');            
      oldmap = new map<id,SObject>();                                 
      list<Reviewer__c> revList = new list<Reviewer__c>();
      Reviewer__c objrev = new Reviewer__c();
      objrev.Status__c= 'Open';
      objrev.Authorization__c = objauth.id;
      objrev.State_Regulatory_Official__c = objcont.id;
      objrev.BRS_State_Reviewer_Email__c = objcont.Email;          
      insert objrev;
      revList.add(objrev);
      oldmap.put(objrev.id,objrev);
      GenericHistoryClass.CreateHistoryRecord(revList,oldmap,'Reviewer__c');  
      system.assert(oldmap != null);
    }
   }