public class CARPOL_ApplicationSharing 
    {       
        public CARPOL_ApplicationSharing(Map<Id, AC__c> newTriggerLines) 
                    {
                        newline = newTriggerLines.values()[0];
                    }
             
        AC__C newline = new AC__c();
        public void applicationSharing()  
                {
                    try
                        {
                            if (newline.Status__c == 'Saved')
                                {
                                   Program_Line_Item_Pathway__c Pathway = [SELECT Program__c, Program__r.Name FROM Program_Line_Item_Pathway__c where id =: newline.Program_Line_Item_Pathway__c LIMIT 1];
                                   String ProgramName = Pathway.Program__r.Name;
                                   Application__c app = [SELECT Id, Share_with_roles__c  FROM Application__c WHERE Id =: newline.Application_Number__c and Application_Status__c != 'Submitted'  and (NOT Share_with_roles__c like :ProgramName) ];
                                   If ((app.Share_with_roles__c != null))
                                        {
                                            if(!app.Share_with_roles__c.contains(ProgramName))
                                                app.Share_with_roles__c += ','+ ProgramName;
                                         }
                                     else
                                         {      
                                               app.Share_with_roles__c = ProgramName;
                                         }
                                    
                                      update app;
                                 }
                        }
                     catch(Exception e)
                        {
                            system.debug('Error from CARPOL_ApplicationSharing class and applicationSharing method is '+e);
                                         
                        }
                }
                        
                                
        public void applicationsharing_delete()
             {
                 try
                    {     
                          Program_Line_Item_Pathway__c Pathway = [SELECT Program__c, Program__r.Name FROM Program_Line_Item_Pathway__c where id =: newline.Program_Line_Item_Pathway__c LIMIT 1];
                          String ProgramName = Pathway.Program__r.Name;
                          Application__c app = [SELECT Id, Share_with_roles__c,Application_Status__c  FROM Application__c WHERE Id =: newline.Application_Number__c and Application_Status__c != 'Submitted' ];
                          List<AC__C> linetitemlist = [SELECT ID, Program_Line_Item_Pathway__r.Program__r.Name FROM AC__c WHERE  Application_Number__c =: app.ID AND Program_Line_Item_Pathway__r.Program__r.Name=:ProgramName];
                         
                          if(linetitemlist.size()==0)
                              {
                                  If (app.Share_with_roles__c != null)
                                    {
                                       if(app.Share_with_roles__c.contains(ProgramName))
                                           {
                                                app.Share_with_roles__c = app.Share_with_roles__c.replace(ProgramName,'');
                                           }  
                                        update app;
                                      }
                                }

                       }
                catch(Exception e)
                     {
                        system.debug('Error from CARPOL_ApplicationSharing class and applicationsharing_delete method is '+e);
                     }
                 }
    }