public class CARPOL_BRS_Createreviewer_email {

   public Authorizations__c Authorization;
   private ID AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
   boolean iscreaterev =  false;

   public Authorizations__c auth = [SELECT Name,BRS_Create_State_Review_Records__c FROM Authorizations__c WHERE Id =: AuthorizationID];
   public CARPOL_BRS_Createreviewer_email(ApexPages.StandardController controller) {
    this.AuthorizationID= ApexPages.currentPage().getParameters().get('ID');
    }
    
    


public void SendEmailToReviewer(){

system.debug('inside send email to reviewer');
system.debug('authid:'+AuthorizationID);

             Authorizations__c auth1 = [SELECT Application_CBI__c, Recordtype.Name FROM Authorizations__c WHERE Id =: AuthorizationID];
             List<Reviewer__c> reviewer = [SELECT ID, Name, LastModifiedDate FROM Reviewer__c WHERE Authorization__c = :AuthorizationID];
             if(reviewer.size()>0){
            attachment attlab = new attachment();
            attachment attlab3 = new attachment();
            Blob attbody = null;
            Blob attbody2 = null;
            Blob attbody3 = null;
            PageReference PDFPage = new PageReference('/apex/CARPOL_BRS_AuthorizationPDF?ID='+AuthorizationID+'&Ack=True&CBI='+auth1.Application_CBI__c);
            if(!test.isRunningTest()){
                attbody = PDFPage.getContent();
                attbody2 = PDFPage.getContent();
            }else{
                attbody = Blob.valueOf('Some Text');
                attbody2 = Blob.valueOf('Some Text');
             }

            attlab.Body = attbody;
            attlab.Name = 'State_Review_Copy_'+ System.TODAY().format()+ '.pdf'; 
            attlab.ParentId = AuthorizationID;
            attlab.contentType = 'application/pdf';
            insert attlab;

             // Querying for Static Resource which stores the State Letter template document
              //List<StaticResource> sr = [Select body, name from StaticResource where Name = 'Carpol_Brs_StateLetterTemplate'];
              List<Attachment> attach = new List<Attachment>();

                          for(Reviewer__c review : reviewer){
                            attachment attlab2 = new attachment();
                            attlab2.Body = attbody;
                            attlab2.Name = 'State_Review_Copy_'+ System.TODAY().format()+ '.pdf'; //'Acknowledgement_Letter.pdf';
                            attlab2.contentType = 'application/pdf';
                            attlab2.ParentId = review.Id;
                            insert attlab2;
                        }
            }

            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'State Review Copy attachment saved successfully '));



string sBody='' ;
string sSubject='' ;
string templateName='';
list<EmailTemplate> emailTemplate = [Select e.body, e.subject, e.Name, e.Id,
e.DeveloperName From EmailTemplate e where e.DeveloperName = 'BRS_Notification_State_Review'];
 Authorizations__c auth = [SELECT Name,BRS_Create_State_Review_Records__c FROM Authorizations__c WHERE Id =:AuthorizationID];
                    sBody = emailTemplate[0].body;
                    sSubject = emailTemplate[0].subject;

    if(sSubject.contains('{!Authorizations__c.Name}'))
{ sSubject = sSubject.replace('{!Authorizations__c.Name}',auth.name);
    }
if(sBody.contains('{!Authorizations__c.Id}'))
{                    sBody = sBody.replace('{!Authorizations__c.Id}', auth.id);
}

templateName = emailTemplate[0].DeveloperName;
system.debug('Template Name'+templateName);


    list<Reviewer__c> rev =[SELECT State_Regulatory_Official__c,BRS_State_Reviewer_Email__c FROM Reviewer__c where   Authorization__c  =:AuthorizationID ];
    //list<string> toAddress =new list<string>();
    System.debug('Reviewer list to send emails :' + rev);
     List<Id> Reviewer_Ids = new List<Id>();


for(Reviewer__c rev1  : rev ) {
/*
Reviewer_Ids.add(rev1.State_Regulatory_Official__c);

*/
    //toaddress.add(rev[0].BRS_State_Reviewer_Email__c);

    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //mail.setToAddresses(toAddress)
                 mail.settargetObjectId(rev1.State_Regulatory_Official__c);
                //mail.settargetObjectId('003r00000023pQQ');
                // system.debug('object id email:'+rev[0].BRS_State_Reviewer_Email__c);
                mail.setWhatId(AuthorizationID);


   mail.setTemplateId(emailTemplate[0].Id);

                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});


 // List of attachments handler
 /*  List<Messaging.EmailFileAttachment> attachmentList = new List<Messaging.EmailFileAttachment>();
   Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();

   {
     // Create the email attachment
     efa.setFileName(attlab.Name);
     efa.setBody(attlab.body);
     efa.setContentType(attlab.ContentType);
     efa.setInline(false);
    attachmentList.add(efa);

}
    // Attach files to email instance
    mail.setFileAttachments(attachmentList);*/
}
 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'reviewer email sent successfully '));

   /* return null; */

}

// This method is used to create a new Applicant Attachment record with name State Package, when state records are created.
public static void createApplicantAttachment(List<Reviewer__c> rev){
        //Authorizations__c auth1 = [SELECT Id,Name FROM Authorizations__c WHERE Id =: AuthorizationID];
        //list<Reviewer__c> rev =[SELECT Id,Authorization__c FROM Reviewer__c where   Authorization__c  =:AuthorizationID ];
        List<Reviewer__c> rev1 = rev;
        list<Recordtype> rec = [SELECT Name, Id FROM Recordtype WHERE DeveloperName = 'State_Package' AND SObjectType = 'Applicant_Attachments__c'];
        if(rec.size()>0){
            for(Reviewer__c revi : rev1){
                Applicant_Attachments__c aa = new Applicant_Attachments__c();
                aa.Attachment_Type__c = 'State Package';
                aa.Recordtypeid = rec[0].id;
                aa.Document_Types__c = 'Original Scanned Paper Application; State Letter;Draft Environmental Assessment';
                aa.Authorization__c = rev[0].Authorization__c;
                insert aa;
            }
        }
}// End of createApplicantAttachment method.


public  string createReviewerrecords()
{

try
        {
         // AuthorizationID = ApexPages.currentPage().getParameters().get('ID');

          if(AuthorizationID != null)
            {
                list<Reviewer__c> lstrev2 = new list<Reviewer__c>();
list<AC__c> lstlineitem = new list<AC__c>();
list<Location__c> loctlist = new list<Location__c>();

 Authorizations__c auth = [SELECT Id,Status__c,Name,BRS_Create_State_Review_Records__c FROM Authorizations__c WHERE Id =: AuthorizationID];
if(auth.BRS_Create_State_Review_Records__c == false)
{
Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
    Schema.SObjectType s = sObjectMap.get('Contact') ; // getting Sobject Type
    Schema.DescribeSObjectResult resSchema = s.getDescribe();

Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
Id contrtId = recordTypeInfo.get('State SPRO').getRecordTypeId(); // particular RecordId by Name
integer countlineitem = [select count()  from AC__c where Authorization__c=:AuthorizationID];
if(countlineitem>0){
lstlineitem = [select id,Name,Authorization__c,Introduction_Type__c,Purpose_of_the_Importation__c,Hand_Carry__c,Number_of_Labels__c from AC__c where Authorization__c=:AuthorizationID] ;
}


integer loctcount = [select count() from Location__c  where Line_Item__c =:lstlineitem[0].Id];
                        if(loctcount>0){
                            list<string> stateids = new list<string>();
                            list<Reviewer__c> lstrev = new list<Reviewer__c>();
                            Id recTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
                            list<Location__c> loctlst = [select id,Name,State__c,State__r.Name,Authorization__c from Location__c where Line_Item__c =:lstlineitem[0].Id AND RecordTypeID != :recTypeId];
                            if(loctlst.size()>0){
                            for(Location__c l:loctlst){
                                if(l.State__c!=null){
                                    stateids.add(l.State__c);
                                }
                            }
                            
                            if(stateids.size()>0){

                                integer sprocount = [select count() from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId ];
                                if(sprocount>0){
                                   list<contact>   sprolst = [select id,Name,Contact_checkbox__c,State__c,Email from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId and State__c in:stateids ];
                                     for(Contact c:sprolst )
                                     {
                                         Reviewer__c objrev = new Reviewer__c();
                                         objrev.Status__c= 'Open';
                                         objrev.Authorization__c = AuthorizationID;
                                         objrev.State_Regulatory_Official__c = c.id;
                                         objrev.BRS_State_Reviewer_Email__c = c.Email;
                                         lstrev.add(objrev);
                                     }
                                     if(lstrev.size()>0){
                                         insert lstrev;
                                     }

                                 }
                            }
                        
  ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'successfully created reviewer records'));
    SendEmailToReviewer();
    auth.BRS_Create_State_Review_Records__c = true;
    auth.Status__c='State Review';
    update auth;
   } 
   else{
   ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning, 'No State Review Records created'));
   }
  }  //createApplicantAttachment();
}
else{
 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,'State Review Records are already Created.'));

}                     

       
  }





            }

           catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
            ApexPages.addMessages(e);

        }


        return null;
        }

        

}