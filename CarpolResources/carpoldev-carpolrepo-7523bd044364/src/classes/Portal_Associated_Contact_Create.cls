public class Portal_Associated_Contact_Create{

public String accountName{get;set;}
//public String emailAddress{get;set;}
//public String firstname{get;set;}
public Applicant_Contact__c appCon{get;set;}
public User u{get;set;}
public string recordTypeName{get;set;}
public Portal_Associated_Contact_Create(ApexPages.StandardController stdController){

    u=[SELECT id,contactId,accountId,account.Name FROM USER where id=:userInfo.getUserId() limit 1] ;
    accountName =u.account.Name ;
    //this.appCon =(Applicant_Contact__c)stdController.getRecord();
    //firstname = appCon.First_Name__c;
    //emailAddress =appCon.Email_Address__c;
    string recordtypeid = ApexPages.currentPage().getParameters().get('recordtype');
    if(recordtypeid !=null && recordtypeid !=''){
        recordTypeName = [Select Id,Name From RecordType Where Id=:recordtypeid].Name;
    }       
    
}

/*public pageReference save(){
    
   appCon.Account__c =u.AccountId;
   
   appCon.First_Name__c =firstname;
   
   appCon.Email_Address__c =emailAddress;
   upsert appCon;
  PageReference pg =new PageReference('/apex/Portal_Associate_Contact_Detail?id='+appCon.id);
  pg.setRedirect(true);
   return pg;
   
}*/

}