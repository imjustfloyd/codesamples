@isTest(seealldata=false)
public class CARPOL_AC_AttachLineItemPDF_Test {

  static Application__c app;
  static Contact c;
  static Account acc;
  static AC__c ac;
  static Applicant_Contact__c imp;
  static Applicant_Contact__c exp;
  static Applicant_Contact__c dd;
  static Breed__c b;
  static Facility__c f;
  static Facility__c f1;
  static {
    CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
    testData.insertcustomsettings();
    String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

    acc = testData.newAccount(AccountRecordTypeId); 
    imp = testData.newappcontact(); 
    exp = testData.newappcontact();
    dd = testData.newappcontact();
    c = testData.newcontact();
    app = testData.newapplication();
    b = testData.newbreed(); 
    f = testData.newfacility('Domestic Port');  
    f1 = testData.newfacility('Foreign Port');
    ac = testData.newLineItem('Personal Use',app);    
  }

  static testMethod void attachPDF() {

    CARPOL_AC_AttachLineItemPDF page = new CARPOL_AC_AttachLineItemPDF(new ApexPages.StandardController(app));
    page.SavePDF();
        
    List<Attachment> attachments=[select id, name from Attachment where parent.id=:app.id];
    System.assertEquals(1, attachments.size());

  }
  
  static testMethod void DogList(){
    CARPOL_AC_AttachLineItemPDF page = new CARPOL_AC_AttachLineItemPDF(new ApexPages.StandardController(app));
    List<AC__c> dogs = page.getACRecords();

    System.assertEquals(1, dogs.size());  
  }
    
    static testMethod void testACRecords(){
        List<AC__c> aclist = [select ID from AC__c where Application_Number__c =:app.Id];
        System.assert(!aclist.isEmpty()); 
    }
    
}