public with sharing class portal_application_edit_control {
    public Id applicationID{get;set;}
    public Application__c application{get;set;}
    public ApexPages.StandardController controller{get;set;}
    public boolean messageVisible{get;set;}
    public portal_application_edit_control(ApexPages.StandardController controller) {
        
        applicationID=ApexPages.currentPage().getParameters().get('id');
        application=(Application__c)controller.getRecord();
       
        messageVisible=false;

    }
    
    public PageReference saveAndCongrat() {
    
           try {
            //update(application);
            controller.Save();
            PageReference congratsPage = Page.Portal_Application_Detail;
          congratsPage.setRedirect(true);
          messageVisible=false;
          return congratsPage;
        } catch(System.DMLException e) {
             messageVisible=true;
            ApexPages.addMessages(e);
           
            return null;
        }  
         
          
    }



}