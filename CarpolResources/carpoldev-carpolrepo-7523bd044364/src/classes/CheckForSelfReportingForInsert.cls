public class CheckForSelfReportingForInsert {
    public static void ThrowErrorWhenConditionsMet(List<Self_Reporting__c> SelfReportingList) {
        Set<Id> AuthorizationIdSet = new Set<Id>();
        for(Self_Reporting__c SR : SelfReportingList) {
            AuthorizationIdSet.add(SR.Authorization__c);
        }
        
        List<AC__c> LineItemList = [SELECT Id, Purpose_of_the_Importation__c, Authorization__c FROM AC__c WHERE Authorization__c IN : AuthorizationIdSet];
        Map<Id, List<AC__c>> AuthorizationToLineItemListMap = new Map<Id, List<AC__c>>();
        for(AC__c AC : LineItemList) {
            if(AuthorizationToLineItemListMap.containsKey(AC.Authorization__c)) {
                AuthorizationToLineItemListMap.get(AC.Authorization__c).add(AC);
            } else {
                List<AC__c> TempLineItemList = new List<AC__c>();
                TempLineItemList.add(AC);
                AuthorizationToLineItemListMap.put(AC.Authorization__c, TempLineItemList);
            }
        }
        
        Map<Id, List<Self_Reporting__c>> AuthorizationIdSelfReportingListMap = new Map<Id, List<Self_Reporting__c>>();
        for(Self_Reporting__c SR : SelfReportingList) {
            if(AuthorizationIdSelfReportingListMap.containsKey(SR.Authorization__c)) {
                AuthorizationIdSelfReportingListMap.get(SR.Authorization__c).add(SR);
            } else {
                List<Self_Reporting__c> TempSelfReportingList = new List<Self_Reporting__c>();
                TempSelfReportingList.add(SR);
                AuthorizationIdSelfReportingListMap.put(SR.Authorization__c, TempSelfReportingList);
            }
        }
        
        List<Authorizations__c> AuthorizationList = [SELECT Id, Status__c FROM Authorizations__c WHERE Id IN : AuthorizationIdSet];
        Map<Id, Authorizations__c> AuthorizationsMap = new Map<Id, Authorizations__c>();
        for(Authorizations__c AR : AuthorizationList) {
            if(AR.Status__c == 'Approved') {
                AuthorizationsMap.put(AR.Id, AR);
            }
        }
        
        for(Id AuthId : AuthorizationsMap.keySet()) {
            if(ContainsAtleastOneLineItem(AuthorizationToLineItemListMap.get(AuthId))) {
                for(Self_Reporting__c SR : AuthorizationIdSelfReportingListMap.get(AuthId)) {
                    SR.addError('Authorization is Approved and have all line items with status');
                }
            }
        }
    }
    
    private static boolean ContainsAtleastOneLineItem(List<AC__c> LineItemList) {
        boolean atleastonecontains = true;
        for(AC__c LineItem : LineItemList) {
            if(LineItem.Purpose_of_the_Importation__c != 'Interstate Movement and Release' && LineItem.Purpose_of_the_Importation__c != 'Release') {
                atleastonecontains = false;
                break;
            }
        }
        return atleastonecontains;
    } 
}