@isTest(seealldata=false)
private class portal_LinkRegArt_detail_Test {
      @IsTest static void test_portal_LinkRegArt_detail() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);          
          Link_Regulated_Articles__c objlra = new Link_Regulated_Articles__c();

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              PageReference pageRef = Page.portal_LinkRegArt_detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objlra);
              ApexPages.currentPage().getParameters().put('id',objlra.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);              
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);

              portal_LinkRegArt_detail extclass = new portal_LinkRegArt_detail(sc);
              extclass.getattach(); 
              extclass.edit();   
              extclass.ReturnToLineItem();      
              System.assert(extclass != null);                     
          Test.stopTest();   
      }
}