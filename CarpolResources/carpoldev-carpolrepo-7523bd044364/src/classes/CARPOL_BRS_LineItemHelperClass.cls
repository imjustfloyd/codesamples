public class CARPOL_BRS_LineItemHelperClass {
 
 public void updateAuthFields(Map<Id, AC__c> oldTriggerLines,Map<Id, AC__c> newTriggerLines){
     
     
 Map<ID,Authorizations__c> parentAuths = new Map<ID, Authorizations__c>();
 List<Id> authIds = new List<Id>();
 list<Authorizations__c> authList = new list<Authorizations__c>();
 
 for(AC__c lineItem : newTriggerLines.values()) {
     if((lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Notification')) ||
        (lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Standard Permit')) ||
        (lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Courtesy Permit'))){
      authIds.add(lineItem.Authorization__c);
    }
 }
 parentAuths = new Map<Id,Authorizations__c>([SELECT id, BRS_Introduction_Type__c,BRS_Hand_Carry_For_Importation_Only__c,BRS_Number_of_Labels__c,Application_CBI__c,(SELECT ID,Purpose_of_the_Importation__c,Hand_Carry__c,Number_of_Labels__c,Does_This_Application_Contain_CBI__c FROM Animal_Care_AC_Authorization__r ) FROM Authorizations__c  WHERE ID IN :authIds]);  
  if(parentAuths.size()>0){        
  for (AC__c lineItem: newTriggerLines.values()){
       if(((lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Notification')) ||
        (lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Standard Permit')) ||
        (lineItem.recordtypeId == CARPOL_UNI_RecordType.getObjectRecordTypeId(AC__c.SObjectType, 'Biotechnology Regulatory Services - Courtesy Permit'))) &&
        ((oldTriggerLines.get(lineItem.Id).Purpose_of_the_Importation__c != lineItem.Purpose_of_the_Importation__c )
        || (oldTriggerLines.get(lineItem.Id).Number_of_Labels__c != lineItem.Number_of_Labels__c )
        || (oldTriggerLines.get(lineItem.Id).Hand_Carry__c != lineItem.Hand_Carry__c )
        || (oldTriggerLines.get(lineItem.Id).Does_This_Application_Contain_CBI__c != lineItem.Does_This_Application_Contain_CBI__c )))
        {
       Authorizations__c  auth = parentAuths.get(lineItem.Authorization__c);
       auth.BRS_Introduction_Type__c =  lineItem.Purpose_of_the_Importation__c;
       auth.BRS_Hand_Carry_For_Importation_Only__c = lineItem.Hand_Carry__c;
       auth.BRS_Number_of_Labels__c = lineItem.Number_of_Labels__c;
       auth.Application_CBI__c = lineItem.Does_This_Application_Contain_CBI__c;
       authList.add(auth);
     }
   }
   
  if(authList.size()>0)
    update authList;  
 }

}

}