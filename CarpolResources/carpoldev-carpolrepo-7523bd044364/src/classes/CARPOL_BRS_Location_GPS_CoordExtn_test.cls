/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRS_Location_GPS_CoordExtn_test   
@CreateDate: 9/30/15
@Description: Test class for CARPOL_BRS_Location_GPS_CoordExtension
@Version: 1.0
@reference: CARPOL_BRS_Location_GPS_CoordExtension (apex class), CARPOL_BRS_ADD_Location_GPS_Coordinates(VF Page)    
*/
@istest(SeeAllData=True)
public class CARPOL_BRS_Location_GPS_CoordExtn_test{

    public static testmethod void test(){
        Trade_Agreement__c ta = new Trade_Agreement__c();
        ta.name='test';
        ta.Trade_Agreement_code__c='12';
        insert ta;

        country__c c = new country__c();
        c.Name='Test';
        c.country_code__c='12';
        c.Trade_Agreement__c=ta.Id;
        insert c;

        Level_1_Region__c lr = new Level_1_Region__c();
        lr.Name='Test';
        lr.country__c=c.Id;
        insert lr;

        level_2_Region__c l2 = new level_2_Region__c();
        l2.Name='Test';
        l2.level_1_Region__c=lr.Id;
    
            Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212'
             );
            insert objloc;
            
            PageReference pageRef = Page.CARPOL_BRS_ADD_Location_GPS_Coordinates;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('Id',objloc.id);
        
            Apexpages.StandardController stdController = new Apexpages.StandardController(objloc ); 
 
        CARPOL_BRS_Location_GPS_CoordExtension  controller = new CARPOL_BRS_Location_GPS_CoordExtension (StdController);
        
        controller.gpsno = '1';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();
        
        controller.gpsno = '2';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();        
    
        controller.gpsno = '3';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();
    
        controller.gpsno = '4';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();    
    
        controller.gpsno = '5';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();    

        controller.gpsno = '6';
        controller.saveloc();
        controller.removegps();
        controller.saveIngredients();
        controller.addRow();
        controller.removeRow();
        controller.addgps();
        system.assert(controller != null);

    }
    
    
    
}