public with sharing class CARPOL_UNI_WorkflowButtonsController {
    public Authorizations__c au;
    public Workflow_Task__c wf;
   // Private String authId;
    transient public Boolean renderBRSApplicationNonCBI {get; set;}
    transient public Boolean renderBRSApplicationCBI {get; set;}
    transient public Boolean renderPermitconditions {get; set;}
    transient public Boolean renderPermitPackage {get; set;}
    transient public Boolean renderGeneratePermit {get; set;}
    transient public Boolean renderPermit {get; set;}
    transient public Boolean renderCollaboration {get; set;}
    transient public String renderNEPA {get; set;}
    transient public String renderNoticeOffindings {get; set;}
    transient public String renderLetterofWarning {get; set;}
    //transient public String renderCoverLetter {get; set;}
    transient public Boolean renderCoverLetter {get; set;}
    transient public Boolean renderEmail {get; set;}
    transient public String renderStatereview {get; set;}
    public String authid {get; set;} 
     public String incidentid {get; set;} 
    public String wfID {get; set;}   
    public String Rtname {get; set;}  
    public boolean showEditBtn{get;set;}
 
    public CARPOL_UNI_WorkflowButtonsController(ApexPages.StandardController Controller) {
      wfID = ApexPages.currentPage().getParameters().get('id');
      //renderPermit = 'False';
      renderPermit = False;
      renderNEPA = 'False';
      renderNoticeOffindings = 'False';
      renderLetterofWarning = 'False';
      //renderCoverLetter = 'False';
      renderCoverLetter = False;
      renderEmail = False;
      renderStatereview  = 'False';
      renderPermitPackage = false;
      showEditBtn=true;
     // wf = [Select Id, Authorization__r.Authorization_Type__c,Authorization__c,Authorization__r.Response_Type__c,buttons__c,Program__c  FROM Workflow_Task__c Where Id =:wfId];     
        wf = [Select Id, Authorization__r.Authorization_Type__c,Incident_Number__c,Recordtype.Name,Authorization__c,buttons__c,Program__c,status__c,Authorization__r.Application_CBI__c FROM Workflow_Task__c Where Id =:wfId]; 
        incidentid = wf.Incident_Number__c;
        Rtname = wf.Recordtype.Name;
        if(wf.status__c=='Complete')  showEditBtn=False;
         IF((wf.Authorization__r.Authorization_Type__c == 'Permit') && (wf.buttons__c !=null) && (wf.buttons__c.contains('Edit Permit')))
        {
          renderPermit = True;  
          authid=wf.Authorization__c;
       } else{
          authid=wf.Authorization__c;
       }


       if((wf.Authorization__r.Authorization_Type__c == 'Permit') && (wf.buttons__c !=null) && (wf.buttons__c.contains('Edit Permit Conditions'))){
              renderPermit = false;
              renderPermitconditions = true;  
              authid=wf.Authorization__c;
           }
           
           
           
         if((wf.buttons__c !=null) && (wf.buttons__c.contains('View Application'))){
             if(wf.Authorization__r.Application_CBI__c == 'Yes'){
                 renderBRSApplicationCBI = true;
                 renderBRSApplicationNonCBI = true;
             }else if(wf.Authorization__r.Application_CBI__c == 'No') {
                      renderBRSApplicationNonCBI = true; }
           }
           
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Conditions Collaboration'))){
           renderCollaboration = true;
           }           
           
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('NEPA Form'))){
           renderNEPA = 'True';
           }
       
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Send Letter of Warning'))){
           renderLetterofWarning = 'True';
           }
        
        if((wf.buttons__c !=null) && (wf.buttons__c.contains('Send Notice of findings'))){
           renderNoticeOffindings = 'True';
           }
          if((wf.buttons__c !=null) && (wf.buttons__c.contains('Edit Cover Letter'))){
                renderCoverLetter = True;
           }
        if((wf.buttons__c !=null) && (wf.buttons__c.contains('Create Permit Package'))){
                renderPermitPackage = True;
           }
        if((wf.buttons__c !=null) && (wf.buttons__c.contains('Generate Permit'))){
                renderGeneratePermit = True;
           }
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Send Email'))){
           renderEmail = True;
       }
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Create State Review Records'))){
           renderStatereview = 'True';
       }
      // return renderPermit;
  }
}


/*public with sharing class CARPOL_UNI_WorkflowButtonsController {
    public Authorizations__c au;
    public Workflow_Task__c wf;
   // Private String authId;
    transient public String renderPermit {get; set;}
    transient public String renderNEPA {get; set;}
    transient public String renderCoverLetter {get; set;}
    transient public String renderEmail {get; set;}
    transient public String renderStatereview {get; set;}
    public String authid {get; set;} 
    public String wfID {get; set;}   
    public CARPOL_UNI_WorkflowButtonsController(ApexPages.StandardController Controller) {
      wfID = ApexPages.currentPage().getParameters().get('id');
      renderPermit = 'False';
      renderNEPA = 'False';
      renderCoverLetter = 'False';
      renderEmail = 'False';
      renderStatereview  = 'False';
      wf = [Select Id, Authorization__r.Authorization_Type__c,Authorization__c,buttons__c,Program__c  FROM Workflow_Task__c Where Id =:wfId];     
        IF(wf.Authorization__r.Authorization_Type__c == 'Permit')
        {
          renderPermit = 'True';  
          authid=wf.Authorization__c;
       }
       else{
       authid=wf.Authorization__c;
       }
       
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('NEPA Form'))){
           renderNEPA = 'True';
           }
       
         if(((wf.buttons__c !=null) && (wf.buttons__c.contains('Edit Cover Letter'))) || (wf.Program__c == 'AC')){
               renderCoverLetter = 'True';
           }
       
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Send Email'))){
           renderEmail = 'True';
       }
       if((wf.buttons__c !=null) && (wf.buttons__c.contains('Create State Review Records'))){
           renderStatereview = 'True';
       }
      // return renderPermit;
  }
} */