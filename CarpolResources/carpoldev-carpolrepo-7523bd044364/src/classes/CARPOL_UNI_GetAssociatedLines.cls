public class CARPOL_UNI_GetAssociatedLines{
public static MAP<ID, authorizations__c> parentAuth = new Map<ID, authorizations__C>();
public static List<Id> listIds = new List<Id>();
public static Authorizations__c authorizationlst;
Public static void getAssociatedLines (string appid){
List<Authorizations__c> allACAuthorizations = [SELECT Id, Name FROM Authorizations__c WHERE Application__c =:appid];
                        for(Authorizations__c allAuths : allACAuthorizations)
                        {
                            System.Debug('<<<<<<< Updating : ' + allAuths.Name + ' >>>>>>>');
                            String strAssociatedLineItems = '';
                            List<AC__c> acLineItems = [SELECT ID, Name, Authorization__c FROM AC__c WHERE Authorization__c = :allAuths.ID];
                            if(acLineItems.size() > 0)
                            {
                                for(AC__c ac : acLineItems)
                                {
                                    strAssociatedLineItems += '<a href="/' + ac.Id + '">' + ac.Name + '</a>, ';
                                }
                                strAssociatedLineItems = strAssociatedLineItems.substring(0, strAssociatedLineItems.length() - 2);
                            }
                            else
                            {
                                strAssociatedLineItems = '';
                            }
                            //allAuths.Associated_Line_Items__c = strAssociatedLineItems;
                            System.Debug('<<<<<<< Associated Line Items : ' + strAssociatedLineItems + ' >>>>>>>');
                        }
                        update(allACAuthorizations);
                        }
                        }