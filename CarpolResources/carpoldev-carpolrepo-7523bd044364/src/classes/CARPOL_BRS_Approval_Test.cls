@isTest(seealldata=true)
public class CARPOL_BRS_Approval_Test {
      @IsTest static void CARPOL_BRS_Approval_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;    
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();    
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');

          
              Test.startTest(); 
              CARPOL_BRS_Approval extclass = new CARPOL_BRS_Approval();
              CARPOL_BRS_Approval.updateapp(objapp.id,'Pending');
              CARPOL_BRS_Approval.brslabelspdf(objauth.id);
              CARPOL_BRS_Approval.BRSdenailatt(new List<ID>(),objauth.id);
              system.assert(extclass != null);
          Test.stopTest();    

      }

}