public class CreateAuthorizationJunction {

 
    public void createAuthorizationJunctionRecord(list<Authorizations__c> authList){
        /*list<Authorizations__c> AuthListTemp =new list<Authorizations__c>();
        AuthListTemp.addAll(authList);
        
        for(Authorizations__c auth1: AuthListTemp){
         authList=new list<Authorizations__c>();
         authList.add(auth1);
    system.debug('----------------<<<<CreateAuthorizationJunction');
    //if(checkRecursive.runOnce1())
    //{
               system.debug('------------ifloop');
    
        list<AC__c> lineItemList = [SELECT ID, Port_of_Entry__c, Authorization__c,Program_Line_Item_Pathway__c FROM AC__c WHERE Authorization__c IN:authList ];
        list<Related_Line_Decisions__c> relatedLineDecisionsList =[SELECT ID,Line_Item__c, Decision_Matrix__c FROM Related_Line_Decisions__c WHERE Line_Item__c IN: lineItemList ];
        /*Map<ID,list<Related_Line_Decisions__c>> LIRelatedLineDeciMap = new Map<ID,list<Related_Line_Decisions__c>>();
        for(Related_Line_Decisions__c RLD: relatedLineDecisionsList){
            
            if(LIRelatedLineDeciMap.containsKey(RLD.Line_Item__c))
                        {
                            List<Related_Line_Decisions__c> lstoi = maporderitems.get(RLD.Line_Item__c);
                            lstoi.add(RLD);
                        }
                        else
                        {
                            List<orderitem> lstoi = new List<orderitem>();
                            lstoi.add(ois);
                            maporderitems.put(ois.orderid,lstoi);
                        }
        }*/
        /*list <Regulations_Association_Matrix__c>  decisionsList = new list <Regulations_Association_Matrix__c>();
        //Query Decision matrix ---
        set<id> lineItemPathwayIds = new Set <id>();                                            
        for (AC__c lineitem: lineItemList){
            lineItemPathwayIds.add(lineitem.Program_Line_Item_Pathway__c);
        }
        system.debug('------------lineItemPathwayIds'+lineItemPathwayIds);
        system.debug('------------lineItemPathwayIds size'+lineItemPathwayIds.size());
        
        
        list <Signature_Regulation_Junction__c> Regulation_Junction_List = new list<Signature_Regulation_Junction__c>();
        if(lineItemPathwayIds.size() >0){
            decisionsList =[SELECT Id, Program_Line_Item_Pathway__c, Country__c,Thumbprint__c,Criteria_Group__c FROM Regulations_Association_Matrix__c where Program_Line_Item_Pathway__c IN: lineItemPathwayIds ORDER BY CreatedDate DESC  ];
            // Query regulation Junction-(Condition Junction)
            system.debug('-----decisionsList'+decisionsList);
             Regulation_Junction_List =[SELECT ID, Decision_Matrix__c, Regulation__c, Regulation__r.Id, Regulation__r.Regulation_Description__c, Signature__c FROM Signature_Regulation_Junction__c WHERE Decision_Matrix__c IN:decisionsList];
        }
        list<Authorization_Junction__c> Authorization_Junction_List = new list<Authorization_Junction__c>();
        Authorization_Junction__c Authorization_Junction; 
        //Insert Record in Authorization Junctions
        ID recordTypeid =[SELECT ID,Name from RecordTYpe WHERE name=:'Regulation Junction'].ID;
      system.debug('----authList'+authList);  
      system.debug('------Regulation_Junction_List size'+Regulation_Junction_List.size());
      system.debug('------Regulation_Junction_List'+Regulation_Junction_List);
      
      for(Authorizations__c auth: authList)  {
           for(Signature_Regulation_Junction__c Signature_Regulation_Junction: Regulation_Junction_List){
               Authorization_Junction = new Authorization_Junction__c();
               Authorization_Junction.recordTypeid = recordTypeid;
               Authorization_Junction.is_Active__c = 'Yes';
              // Authorization_Junction.Thumbprint__c=
              // Authorization_Junction.Regulation__c=
              Authorization_Junction.Authorization__c=auth.ID;
               Authorization_Junction.Signature_Regulation_Junction_Name__c=Signature_Regulation_Junction.ID;
               //Authorization_Junction.Regulation__c = Signature_Regulation_Junction.Regulation__r.ID;
               Authorization_Junction.Regulation__c = Signature_Regulation_Junction.Regulation__c;
               Authorization_Junction.Decision_Matrix__c = Signature_Regulation_Junction.Decision_Matrix__c;
               Authorization_Junction.Regulation_Description__c= Signature_Regulation_Junction.Regulation__r.Regulation_Description__c;
               Authorization_Junction_List.add(Authorization_Junction);
           }
      }
      
      system.debug('@@@Authorization_Junction_List' + Authorization_Junction_List);
        set<Authorization_Junction__c> finalset = new set<Authorization_Junction__c>();
        List<Authorization_Junction__c> final_list = new List<Authorization_Junction__c>();
        map<id,Authorization_Junction__c> authjunctionMap = new map<id,Authorization_Junction__c> () ; 
              
        for(Authorization_Junction__c aj: Authorization_Junction_List ){
        system.debug('###------aj' + aj);
                 authjunctionMap.put(aj.Regulation__c, aj);       
         }       
        
         system.debug('###authjunctionMap------' + authjunctionMap);        
        //finalset.addAll(Authorization_Junction_List);
        
        system.debug('###finalset' + finalset);
        
        //final_list=new list<>();
        final_list.addAll(authjunctionMap.values());
        
          system.debug('!!!final_list-------' + final_list);
             system.debug('!!!final_list size-------' + final_list.size());
        
        
      if (final_list.size()>0)
      Insert final_list;
      //list<Authorizations__c> authList 
      list<Authorizations__c> authListUpdated = new list<Authorizations__c>();
      list<Workflow_Task__c> lsttask = new list<Workflow_Task__c>();
      list<Program_Workflow__c> lstCond = new list<Program_Workflow__c>();

       Authorizations__c authRec;
       //if(Program_Line_Item_Pathway__c=='Aqua')
       decisionsList =[SELECT Id, Program_Line_Item_Pathway__c, Country__c, Thumbprint__c, Thumbprint__r.Response_Type__c, Criteria_Group__c FROM Regulations_Association_Matrix__c where Program_Line_Item_Pathway__c IN: lineItemPathwayIds  AND Thumbprint__c !=null AND Criteria_Group__c != null ORDER BY CreatedDate DESC limit 1 ];
       system.debug('<<<<<<<<<<-------decisionsList 1:'+decisionsList);
       if( decisionsList.size()==0)
        decisionsList =[SELECT Id, Program_Line_Item_Pathway__c, Country__c, Thumbprint__r.Response_Type__c, Thumbprint__c,Criteria_Group__c FROM Regulations_Association_Matrix__c where Program_Line_Item_Pathway__c IN: lineItemPathwayIds  AND Thumbprint__c !=null  ORDER BY CreatedDate DESC limit 1 ];
        system.debug('<<<<<<<<<<-------decisionsList 2:'+decisionsList);
                if(decisionsList.size()>0)
                 for(Authorizations__c auth:  authList){
                   authRec=new  Authorizations__c();
                   authRec.id=auth.id;
                   authRec.Final_Decision_Matrix_Id__c= decisionsList[0].ID;
                   authRec.Thumbprint__c = decisionsList[0].Thumbprint__c;
                   authRec.Response_Type__c = decisionsList[0].Thumbprint__r.Response_Type__c;
                   authRec.Port_of_Entry__c = lineItemList[0].Port_of_Entry__c;
                   update authRec;

                   /*
                        Update by Subbu - Deactivate task generator trigger and move the functionality here
                   */
                  /* if(decisionsList[0].Thumbprint__c != null){
                      Signature__c thumbPrint = [select ID,Program_Prefix__c,Name,Program_Prefix__r.Name From Signature__c where ID =: decisionsList[0].Thumbprint__c];
                      if(thumbPrint.Program_Prefix__r.Name!=''){
                        lstCond = [select id,Name,Stop_Status__c,Owner_Approver_is_Signatory__c, Requires_Approval__c,Only_for_this_Role__c, Update_Record_to_Specific_Value_Pending__c,
                                  Update_Record_to_Specific_Value_Denied__c,Update_Record_to_Specific_Value__c,Assign_to_Queue__c,Authorization_Status__c,Active__c,
                                  Object_to_Associate_this_Task_to__c,Comments_Destination_Field__c,Copy_Comments_to_Authorization__c,Default_Priority__c,Default_Status__c,
                                  Dependency_Field__c,Dependent_On_Workflow__c,Description__c,Generate_as_Task__c,How_Executed__c,Internal_Only__c,Program__c,Program_Prefix__c,
                                  Program_Prefix__r.Name,Program__r.name,Required_if_Prior_Task_Equals__c,Task_Record_Type__c,Update_Authorization_Status__c,Update_Record_Field_Name__c,
                                  Task_Source_Field_Name__c,Workflow_Order__c,buttons__c,Email_Template__c,Update_Record_to_Specific_Val_InProgress__c,Update_Related_Record_to_Specific_Value__c,
                                  In_Progress_Status__c from Program_Workflow__c where Program_Prefix__r.Name=:thumbPrint.Program_Prefix__r.Name];
                      
                     // Replaced Soql Queries from FOR Loop  Start of changes Kishore 
                       list<RecordType> rTypeList = [select id,name from RecordType where SobjectType = 'Workflow_Task__c'];
                        map<string,Id> rMap = new map<string,Id>();
                        for(RecordType r : rTypeList){
                          rMap.put(r.Name,r.Id);
                        }
                       list<Group> gList = [select id,name from Group];
                        map<string,Id> gMap = new map<string,Id>();
                        for(Group g:gList){
                          gMap.put(g.Name,g.Id);
                        } 
                       list<EmailTemplate> etList = [select id,name,developername from EmailTemplate];
                        map<string,Id> etMap = new map<string,Id>();
                        for(EmailTemplate et:etList){
                          etMap.put(et.Name,et.Id);
                        }                        
                        
                     // End of Changes  
                        for(Program_Workflow__c programWorkFlowVar:lstCond ){
                            Workflow_Task__c t = new Workflow_Task__c();
                            if(programWorkFlowVar.Active__c=='Yes'){
                              if(programWorkFlowVar.Email_Template__c !=null){
                               Id etId = etMap.get(programWorkFlowVar.Email_Template__c); 
                               t.Email_Template__c = etId;
                              //  Id EmailTemplateId  = [Select id,name,developername from EmailTemplate where developername =: programWorkFlowVar.Email_Template__c].id;
                              //  t.Email_Template__c = EmailTemplateId;
                              }
                             // RecordType RecId = [select Id from RecordType where Name=:programWorkFlowVar.Task_Record_Type__c and SobjectType = 'Workflow_Task__c'];
                              Id  recTypeId =  rMap.get(programWorkFlowVar.Task_Record_Type__c);
                              Id gId = gMap.get(programWorkFlowVar.Assign_to_Queue__c); 
                              
                              //Group groupId = [select Id from Group where Name=:programWorkFlowVar.Assign_to_Queue__c];
                             // system.debug('#$#$ groupId  = '+groupId );
                              //t.RecordTypeId = RecId.Id;
                              t.RecordTypeId = recTypeId;
                              t.name = programWorkFlowVar.Name;
                              //t.OwnerId = groupId.Id;
                              t.OwnerId = gId;
                              t.authorization__c = auth.id;
                              t.Priority__c = programWorkFlowVar.Default_Priority__c;
                              t.Status__c = programWorkFlowVar.Default_Status__c;
                              t.Owner_Approver_is_Signatory__c = programWorkFlowVar.Owner_Approver_is_Signatory__c;
                              t.Internal_Only__c = programWorkFlowVar.Internal_Only__c;
                              t.Comments_Destination_Field__c = programWorkFlowVar.Comments_Destination_Field__c;
                              t.Copy_Comments_to_Authorization__c = programWorkFlowVar.Copy_Comments_to_Authorization__c;
                              t.Workflow_Order__c = programWorkFlowVar.Workflow_Order__c;
                              t.Update_Record_Status__c = programWorkFlowVar.Update_Authorization_Status__c;
                              t.Update_Record_Field_Name__c = programWorkFlowVar.Update_Record_Field_Name__c;
                              t.Task_Source_Field_Name__c = programWorkFlowVar.Task_Source_Field_Name__c;
                              t.Dependent_On_Workflow__c = programWorkFlowVar.Dependent_On_Workflow__c;
                              t.Dependency_Field__c = programWorkFlowVar.Dependency_Field__c;
                              t.Required_if_Prior_Task_Equals__c = programWorkFlowVar.Required_if_Prior_Task_Equals__c;
                              t.Stop_Status__c = programWorkFlowVar.Stop_Status__c;
                              t.Record_Status__c = programWorkFlowVar.Authorization_Status__c;
                              t.Update_Record_to_Specific_Value__c = programWorkFlowVar.Update_Record_to_Specific_Value__c;
                              t.Update_Record_to_Specific_Value_Denied__c = programWorkFlowVar.Update_Record_to_Specific_Value_Denied__c;
                              t.Update_Record_to_Specific_Value_Pending__c = programWorkFlowVar.Update_Record_to_Specific_Value_Pending__c;//added by Kishore
                              t.Update_Record_to_Specific_Val_InProgress__c = programWorkFlowVar.Update_Record_to_Specific_Val_InProgress__c;//added by Kishore 10/19/2015
                              t.Update_Related_Record_to_Specific_Value__c = programWorkFlowVar.Update_Related_Record_to_Specific_Value__c;//added by Kishore 10/20/2015                      
                              t.Program_Prefix__c = programWorkFlowVar.Program_Prefix__r.Name;// added by Kishore
                              t.Program__c = programWorkFlowVar.Program__r.name; //added by kishore
                              t.Approval_Roles__c = programWorkFlowVar.Only_for_this_Role__c;
                              t.Requires_Approval__c = programWorkFlowVar.Requires_Approval__c;
                              t.buttons__c = programWorkFlowVar.buttons__c; // added by kishore
                              t.Instructions__c = programWorkFlowVar.Description__c; // added by Kishore 10/10/2015
                              t.In_Progress_Status__c = programWorkFlowVar.In_Progress_Status__c; // added by kishore 10/26/2015
                              lsttask.add(t);
                            }
                        }
                        if(lsttask.size()>0){
                          Database.SaveResult[] savelst = Database.insert(lsttask);
                        }
                      } else{
                        System.debug(' --- No Prefix assigned -- Cannot load tasks ');
                      }
                    }else{
                      System.debug(' --- Thumbprint null - Workflow tasks cannot be inserted --- ');
                    }
       } 
}

           
  */  //}
}
}