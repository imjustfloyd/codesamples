@isTest(seeAlldata = true)
public class CARPOL_ApplicationProcessing_Test{
    
    static testMethod void testProcessApplication(){
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Applicant_Contact__c imp = testData.newappcontact(); 
        Applicant_Contact__c exp = testData.newappcontact(); 
        Applicant_Contact__c dd = testData.newappcontact(); 
        RecordType portrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Ports' AND SobjectType = 'Facility__c'];
        Facility__c f = new Facility__c(Name = 'Halifax', Type__c = 'Foreign Port', RecordTypeId = portrt.Id);
        insert f;
        Facility__c f1 = new Facility__c(Name = 'ABERDEEN-HOQUIAM, WA', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f1;
        Facility__c f2 = new Facility__c(Name = 'Test1', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f2;
        Facility__c f3 = new Facility__c(Name = 'Test2', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f3;
        RecordType rtp = [select ID from RecordType where sObjectType = 'Facility__c' AND Name = 'Ports'];
        Facility__c port = new Facility__c(Name = 'Test', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port; 
        Account acc = testData.newAccount(AccountRecordTypeId); 
        Contact c = testData.newcontact();
        Application__c app = testData.newapplication();
        Breed__c b = testData.newbreed(); 
        Authorizations__c auth = testData.newAuth(app.Id);
        
        AC__c ac = testData.newLineItem('Veterinary Treatment',app);
        System.debug ('<<<<<<<<<<<<<<<<<<<< Animal Care ' + ac + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
        Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = ac.Id, Total_Files__c = 1);
        insert att;
        
        Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
        insert n;
        
        System.debug ('<<<<<<<<<<<<<<<<<<<< ATT ' + att + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug ('<<<<<<<<<<<<<<<<<<<< Documents ' + n + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug ('<<<<<<<<<<<<<<<<<<<< Application (PocessApplication) ' + app + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        CARPOL_ApplicationProcessing approc = new CARPOL_ApplicationProcessing(new ApexPages.StandardController(app));
        System.debug('<<<<<<<<<<<<<<<<<<< Approc ' + approc + ' >>>>>>>>>>>>>>>>>>>>>>');
        PageReference p = approc.processApplication();
        Application__c appStatus = [select Application_Status__c from Application__c where ID = :app.Id];
        System.debug ('<<<<<<<<<<<<<<<<<<<< Status ' + appStatus.Application_Status__c + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:app.id];
        System.debug ('<<<<<<<<<<<<<<<<<<<< Attachments ' + attachments.size() + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        //System.assertEquals(1, attachments.size());
        System.debug('<<<<<<<<<<<<<<<<<<< P ' + p + ' >>>>>>>>>>>>>>>>>>>>>>');
        //System.assertEquals(p, null);
        ApexPages.Message[] msgs = ApexPages.getMessages();
        Boolean found=false;
        for (ApexPages.Message msg : msgs)
        {
            if (msg.getSummary()=='Application submitted successfully.')
            {
              found=true;
            }
        }
        System.debug('<<<<<<<<<<<<<<<< Messages ' + msgs + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
        //System.assert(found);
        
    }
    
    static testMethod void testNoApplication(){
        //Application__c app = new Application__c();
        //insert app;
        CARPOL_ApplicationProcessing approc = new CARPOL_ApplicationProcessing(new ApexPages.StandardController(new Application__c()));
        System.debug('<<<<<<<<<<<<<<<<<<< Approc ' + approc + ' >>>>>>>>>>>>>>>>>>>>>>');
        PageReference p = approc.processApplication();
        //System.assertEquals(p, null);
        ApexPages.Message[] msgs = ApexPages.getMessages();
        Boolean found=false;
        for (ApexPages.Message msg : msgs)
        {
            if (msg.getSummary()=='No valid Application ID found.')
            {
              found=true;
            }
        }
        System.debug('<<<<<<<<<<<<<<<< Messages ' + msgs + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.assert(found);
    }
    
    static testMethod void testGoBackApplication(){
        List<CARPOL_UNI_DisableTrigger__c> dtlist = new List<CARPOL_UNI_DisableTrigger__c>();
        Set<String> dtstrings = new Set<String>{'CARPOL_BRS_AuthorizationTrigger','CARPOL_BRS_ConstructTrigger','CARPOL_BRS_GenotypeTrigger','CARPOL_BRS_Link_RegulatedTrigger','CARPOL_BRS_LocationsTrigger Active','CARPOL_BRS_Reviewer_DuplicateTrigger','CARPOL_BRS_Self_ReportingTrigger','CARPOL_BRS_SOPTrigger','CARPOL_BRS_State_ReviewerTrigger'};
        for(String x : dtstrings){
           CARPOL_UNI_DisableTrigger__c dt = CARPOL_UNI_DisableTrigger__c.getInstance(x);
               if(dt == null){
                   dt = new CARPOL_UNI_DisableTrigger__c(Name=x,Disable__c = false);  
                   dtlist.add(dt);
               }
        }
        insert dtlist;
        RecordType application  = [SELECT ID, Name FROM RecordType WHERE Name = 'Standard Application' AND SobjectType = 'Application__c'];
        Account acc = new Account(Name = 'Test1');
        insert acc;  
        Contact c = new Contact(FirstName = 'Test', LastName = 'John', Account = acc, Phone = '2323232312', Email = 'john@test.com', MailingState='Alabama' , MailingCity='null', MailingStreet = 'null', MailingCountry='United States' , MailingPostalCode='null');
        insert c;
        Application__c app = new Application__c(Applicant_Name__c = c.Id, RecordTypeId = application.Id);
        insert app;
        System.debug ('<<<<<<<<<<<<<<<<<<<< Application (GoBackApplication) ' + app + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        CARPOL_ApplicationProcessing approc = new CARPOL_ApplicationProcessing(new ApexPages.StandardController (app));
        String p = approc.goBackApplication().getUrl();
        System.debug ('<<<<<<<<<<<<<<<<<<<< P ' + p + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        //System.assertEquals(p, '/'+app.Id);
    }
}