@isTest(seealldata=true)
public class CARPOL_PPQ_Docs_Test {
      @IsTest static void CARPOL_PPQ_Docs_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_PPQ_TestDataManager testData = new CARPOL_PPQ_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          insert objdm;
          Application__c objapp = testData.newapplication();
          AC__c li = testData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Final_Decision_Matrix_Id__c = objdm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
          Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = li.Id, Total_Files__c = 1);
          //Applicant_Attachments__c att = testData.newAttach(li.Id);
          insert att;
        
          //Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
          //insert n;
          Attachment n = testData.newAttachment(li.Id);
          
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_Add_PPQ_Documents;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(li);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<AC__c>());

              ApexPages.currentPage().getParameters().put('id',li.id);

              CARPOL_PPQ_Docs extclass = new CARPOL_PPQ_Docs(sc);
              extclass.getSelectedDocuments();
              extclass.getRequiredDocument();
              extclass.getExistingDocuments();
              Attachment ex = extclass.uploadedFile;
              extclass.uploadedFile.Name = 'Test';
              extclass.uploadedFile.Body = Blob.valueOf('Some Text');
              extclass.uploadedFile.parentId = li.id;
              extclass.strSelectedDocuments = 'Test';
              extclass.uploadFile();
              system.assert(extclass != null);
          Test.stopTest();    

      }

}