public  class CARPOL_BRS_StateReviewEditRecords {
    public Id reviewerId{get;set;}
    public Reviewer__c Reviewer{get;set;}
    public Reviewer__c obj {get;set;} 
    public boolean messageVisible{get;set;}
    public Id AuthId;
    public ApexPages.StandardController stdController;
    
    public CARPOL_BRS_StateReviewEditRecords(ApexPages.StandardController controller) {
        
        reviewerId = ApexPages.currentPage().getParameters().get('id');
        obj = (Reviewer__c)controller.getRecord();
        system.debug('Reviewer  ###'+Reviewer );
        stdController = controller;
        messageVisible = false;
    Reviewer = [Select Id, Name,State_Regulatory_Official__c,BRS_State_Reviewer_Email__c,State__c,Authorization__r.name,Authorization__c,
                Status__c,State_Comments__c,Requirement_Desc__c,Review_Complete__c,State_has_comments__c,No_Requirements__c,State_not_Responded__c,
                Notes_to_State__c,CreatedByID,LastModifiedDate,CreatedDate,LastModifiedByID
                from Reviewer__c where ID=:reviewerId ]; 
         system.debug('Reviewer  ###'+Reviewer );       

    }
    
    public PageReference saveAndCongrat() {
    
           //try {
              system.debug('<<<<<!!!!Reviewer ###'+Reviewer);
              system.debug('Reviewer state comments###'+Reviewer.State_Comments__c);
              reviewerId = ApexPages.currentPage().getParameters().get('id');
                obj.id = reviewerId;
                obj.State_Comments__c = Reviewer.State_Comments__c;
                obj.Review_Complete__c = Reviewer.Review_Complete__c;
                obj.Requirement_Desc__c = Reviewer.Requirement_Desc__c;
                obj.No_Requirements__c = Reviewer.No_Requirements__c;
                obj.State_has_comments__c = Reviewer.State_has_comments__c;
                
                
                system.debug('<<<<<!!!!obj.id '+obj.id);
                system.debug('<<<<<!!!!obj before #'+obj);
                
                update obj;
              
             system.debug('<<<<<!!!!obj After #'+obj);
            //PageReference congratsPage = new PageReference('/apex/CARPOL_BRS_StateReviewRecords?id=' + reviewerId);
           
            
            PageReference congratsPage = Page.CARPOL_BRS_StateReviewRecords;
          
           congratsPage.getParameters().put('id',obj.id);
        system.debug('<<<<<!!!! pg is'+congratsPage);
          congratsPage.setRedirect(true);
          messageVisible=false;
          return congratsPage;
        /*} catch(System.DMLException e) {
             messageVisible=true;
            ApexPages.addMessages(e);
           
            return null;
        }  */
         
          
    }
    
  



}