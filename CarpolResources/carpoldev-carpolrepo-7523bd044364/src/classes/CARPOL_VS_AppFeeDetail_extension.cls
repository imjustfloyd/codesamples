public class CARPOL_VS_AppFeeDetail_extension {

public List <Authorizations__c> auList {get;set;}

public String paymentType{get;set;}
public Boolean showUserFeeAccountNumber { get; set; }
public Boolean showMailInCheck {get;set;}
public Boolean showMoneyOrder {get;set;}

public Application__c app;
public Application__c application { get; set; }

    public CARPOL_VS_AppFeeDetail_extension(ApexPages.StandardController controller) {
    
    this.app = (Application__c)controller.getRecord();
    load();

    ID applicationID = ApexPages.CurrentPage().getparameters().get('id');
    if(applicationID != null)
        {
            application = new Application__c();
            Application = [SELECT Id, Payment_Type__c, APHIS_User_Fee_Account_Number__c, Application_Fee_Amount__c FROM Application__c WHERE Id = :applicationID LIMIT 1];
            paymentType = Application.Payment_Type__c;
            paymentTypeChangeAction();
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No valid Application ID found.'));
        }
    }
    
    public void load()
    {
      auList = new List<Authorizations__c>([SELECT
      Id,
      Name,
      Application__c,
      Fee_Amount__c,
      Authorization_Type__c,
      (SELECT Id,
      Name,
      Quantity__c,
      Product_Category__c,
      Intended_Use__c
      FROM Veterinary_Services_VS__r)
      FROM Authorizations__c
      WHERE Application__c = :app.Id
      AND Fee_Amount__c <> null
      ]);
    System.Debug('<<<<<<< Total VS Products : ' + auList.size() + ' >>>>>>>');
    
    }
    
        public PageReference paymentTypeChangeAction()
    {
        if(paymentType == 'APHIS User Account')
        {
            showUserFeeAccountNumber = true;
        }
        else if(paymentType != 'APHIS User Account')
        {
            showUserFeeAccountNumber = false;
        }
            
        if(paymentType == 'Mail-in Check')
        {
            showMailInCheck = true;
        }
        else if (paymentType != 'Mail-in Check')
        {
            showMailInCheck = false;
        }
        if(paymentType == 'Money Order')
        {
            showMoneyOrder= true;
        }
        else if (paymentType != 'Money Order')
        {
            showMoneyOrder = false;
        }
        
        return null;
    }
    
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + app.Id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }

}