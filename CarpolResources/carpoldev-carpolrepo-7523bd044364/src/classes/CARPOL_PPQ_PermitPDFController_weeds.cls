public class CARPOL_PPQ_PermitPDFController_weeds {

    public ID appId {get; set;}
    public ID authId {get;set;}
    public Authorizations__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineItem{get;set;}
    public String htmlText {get;set;}
    public String tdSpan = 'font-size:9.0pt;color:#221F1F';
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
    public String defaultPart {get;set;}
    Private List<TableCol> tblColumns;
    public string destination {get;set;}
    public string portsSelected {get;set;}    
    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public string HandCarry {get;set;}
    public Decimal  facnum {get;set;}
    public string facaddr1 {get; set;}
    public string facaddr2 {get; set;} 
    public string facbldg  {get; set;}
    public string facCity  {get; set;}   
    public string facstate  {get; set;} 
    public string faccountry  {get; set;} 
    public string faczip  {get; set;} 
    public String articleCategoryName {get; set;}
    public String articleGroupName {get; set;}
    public String pathwayName {get; set;}

    public CARPOL_PPQ_PermitPDFController_weeds(ApexPages.StandardController controller){
        app = (Authorizations__c)controller.getRecord();
        authId = ApexPages.currentPage().getParameters().get('id');
        app = [Select Id, Application__c, Program_Pathway__c from Authorizations__c where Id=: authId];
        appId = app.Application__c;
        //strPathway = ApexPages.currentPage().getParameters().get('strPathway');
        System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
        defaultPart = 'Seed';
        getPDFFields();

        system.debug('&&&Start&&&');
            portsSelected = ''; //5/16/16 VV 
        list<Authorization_Junction__c> lstAuthjunct = new list<Authorization_Junction__c>();
        for(Authorization_Junction__c authjunct: [select id,Name,Port__c,Port__r.Name,Authorization__c from Authorization_Junction__c where Authorization__c=:authId  and Port__c!=null])
        {
            lstAuthjunct.add(authjunct);
        }
        if(lstAuthjunct.size()>0){
            for(integer i=0;i<lstAuthjunct.size();i++){
                if(i<lstAuthjunct.size()-1){
                    portsSelected += lstAuthjunct[i].Port__r.Name+'; ';
                }
                else{
                    portsSelected += lstAuthjunct[i].Port__r.Name;
                }
            }
        }
        system.debug('----portsSelected---'+portsSelected);   //5/16/16 END VV 
        
        //Dinesh
        if(!pathwayName.containsIgnoreCase('PESTS')){
            articleCategoryName = pathwayName;
        }else{
            articleCategoryName = articleGroupName;
        }

    }
    public CARPOL_PPQ_PermitPDFController_weeds()  //VV added
    {
    }

    public List<AC__c> getACRecords(){
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            //String soql = 'select ' + String.join(fields, ', ') + ',RA_Scientific_Name__r.name, Country_Of_Origin__r.Name,Country__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name,Group__r.Name,Component__r.Name ,Country_of_Export__r.Name,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Regulated_Article__r.Plant_Part__r.Name,State_Territory_of_Destination__r.Name from AC__c where Application_Number__c = \'' + appId + '\'';
            String soql = 'select ' + String.join(fields, ', ') + ',RA_Scientific_Name__r.name, Country_Of_Origin__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name,Scientific_Name__r.Category_Group_Ref__r.Name, Group__r.Name,Component__r.Name ,Country_of_Export__r.Name,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Regulated_Article__r.Plant_Part__r.Name,State_Territory_of_Destination__r.Name from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            
            lineItem = aclst[0]; 
        }
        HandCarry = aclst[0].Hand_Carry__c;
        articleGroupName = aclst[0].Scientific_Name__r.Category_Group_Ref__r.Name; //Dinesh
        
        if (aclst[0].Approved_Facility__c!=null){
        Facility__c facility = [select id, Facility_ID__c, Address_1__c,Address_2__c,Building__c,City__c,State_LV1__r.name,Zip__c,Country__r.name from facility__c where id =: aclst[0].Approved_Facility__c limit 1];
        FacNum = facility.Facility_ID__c ;
        facaddr1 = facility.Address_1__c;
        facaddr2 = facility.Address_2__c;
        facbldg = facility.Building__c;   
        facCity = facility.City__c;
        //facstate= facility.State_LV1__r.name;         
        faczip = facility.Zip__c;        
        facCountry = facility.Country__r.name;
        
        destination =  facaddr1+', '+facaddr2 +', '+ facbldg +', '+ facCity +', '+ facstate +', '+ faczip +', '+ facCountry ;
        }
        return aclst;

    }
    
     public void getPDFFields()
    {

         //Regulations_Association_Matrix__c raMatrix = [Select ID, Name, Program_Line_Item_Pathway__c from Regulations_Association_Matrix__c where Id =:app.Final_Decision_Matrix_Id__c];
         pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                    Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                    PDF_Heading_1__c, PDF_Heading_2__c, PDF_Permit_Conditions_Static__c, Table_Section_Heading__c, Permit_PDF_Sub_Heading_1__c, Permit_PDF_Heading_1__c, Permit_PDF_Heading_2__c,
                    PDF_Forwarded_Completed_App_Address__c from Program_Line_Item_Pathway__c where Id =: app.Program_Pathway__c];

         pathwayName = pdfLnItmPway.Name;
         tblColumns = new List<TableCol>();           
         
        if(pdfLnItmPway.Column_1_API_Name__c!=NULL)        
            tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
        if(pdfLnItmPway.Column_2_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
        if(pdfLnItmPway.Column_3_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
        if(pdfLnItmPway.Column_4_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
        if(pdfLnItmPway.Column_5_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
        if(pdfLnItmPway.Column_6_API_Name__c!=NULL)
             tblColumns.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c));      
                
    }
    
    public String getTable()
    {
        htmlText ='';
        //List<CARPOL_UNI_ApplicationPDF_Table__c> tableSettings = CARPOL_UNI_ApplicationPDF_Table__c.getall().values();
        //tableSettings.sort();
        

        htmlText += '<tr>';
            //loop over column heads from tablecol object
            for(TableCol tbl: tblColumns){
                
                
                    htmlText += '<th align="center"><span style="' + tdSpan + '">';
                    htmlText +=  tbl.Heading;
                    htmlText += '</span></th>';
                    counter++;
                
            }
            
            htmlText += '</tr>';
        
        for(AC__c a: aclst)
        {
            counter =0;
            htmlText += '<tr>';
            for(TableCol tbl: tblColumns){
                htmlText += '<td align="center"><span style="' + tdSpan + '">';
                htmlText +=  (getFieldValue(a,tbl.APIName)!=null) ? getFieldValue(a,tbl.APIName): '';
                
                htmlText += '</span></td>';
                counter++;
            }
            htmlText += '</tr>';
            
            
        }
        
        return htmlText;
    }
    
    public static Object getFieldValue(SObject o,String field){
 
     if(o == null){
        return null;
     }
     if(field.contains('.')){
    String nextField = field.substringAfter('.');
    String relation = field.substringBefore('.');
    return CARPOL_UNI_ApplicationPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
     }else{
    return o.get(field);
     }
    

    }
    
    //Added by Tharun 04/27/2016
    
    public List<Authorization_Junction__c> getConditions(){
        
        try{
    
scj= new List<Authorization_Junction__c>();
    
    scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Regulation__r.Type__c='Import Requirements' and Regulation__r.Sub_Type__c='Permit Requirement' and Authorization__c = :authId ORDER BY Order_Number__c ASC];
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
    scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' && SCJ.Is_Active__c=='Yes' ){scjA.add(SCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(SCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
            
            }
        }
        
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }
     return scj;
        
    }
  //End of Tharun changes --04/27/2016  
   
       
    public Class TableCol
    {
        public String Heading;
        public String APIName;
        public String sortOrder;

        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }
}