@isTest(seealldata=true)
private class CARPOL_VS_AttachLineItemPDF_Test {
      @IsTest static void CARPOL_VS_AttachLineItemPDF_Test() {
          String Description = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp); 
          ac1.Authorization_Group_String__c='test';
          ac2.Authorization_Group_String__c='test';
          update ac1;
          update ac2;
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          objreg1.Regulation_Description__c = Description;
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');            
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          objauthjun1.Is_Active__c = 'Yes';
          update objauthjun1;
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);  
           String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();   
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Facility_Vet_Services_Junction__c objFSV=new Facility_Vet_Services_Junction__c();
          Facility__c fac = testData.newfacility('Domestic Port');  
          objFSV.Vet_Services__c= objacct.id;
          objFSV.Authorization__c=objauth.id;
          objFSV.Facility__c=fac.id;
          insert objFSV;
          
          Test.startTest();
          PageReference pageRef = Page.CARPOL_VS_AuthorizationPDF;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('id',objauth.id);
          
          CARPOL_VS_AttachLineItemPDF extclass = new CARPOL_VS_AttachLineItemPDF(sc);
          extclass.savePDF();
          extclass.getPDFFields();
          extclass.getACRecords1();
          extclass.getACRecords2();
          extclass.getLineItemString();
          system.assert(extclass != null); 
          Facility_Vet_Services_Junction__c objFSV1=new Facility_Vet_Services_Junction__c(); 
          objFSV1.Vet_Services__c= objacct.id;
          objFSV1.Authorization__c=objauth.id;
          objFSV1.Facility__c=fac.id;
          insert objFSV1;
          extclass.getLineItemString();         
          Test.stopTest();     
      }
      @IsTest static void CARPOL_VS_AttachLineItemPDF_Test1() {
          String Description = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.';
          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp); 
          ac1.Authorization_Group_String__c='test';
          ac2.Authorization_Group_String__c='test';
          update ac1;
          update ac2;
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          objreg1.Regulation_Description__c = Description;
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');            
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          objauthjun1.Is_Active__c = 'Yes';
          update objauthjun1;
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);  
           String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();   
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Facility_Vet_Services_Junction__c objFSV=new Facility_Vet_Services_Junction__c();
          Facility__c fac = testData.newfacility('Domestic Port');  
          objFSV.Vet_Services__c= objacct.id;
          objFSV.Authorization__c=objauth.id;
          objFSV.Facility__c=fac.id;
          insert objFSV;
          Facility_Vet_Services_Junction__c objFSV1=new Facility_Vet_Services_Junction__c(); 
          objFSV1.Vet_Services__c= objacct.id;
          objFSV1.Authorization__c=objauth.id;
          objFSV1.Facility__c=fac.id;
          insert objFSV1;
          
          Test.startTest();
          PageReference pageRef = Page.CARPOL_VS_AuthorizationPDF;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('id',objauth.id);
          
          CARPOL_VS_AttachLineItemPDF extclass = new CARPOL_VS_AttachLineItemPDF(sc);
          extclass.savePDF();
          extclass.getPDFFields();
          extclass.getACRecords1();
          extclass.getACRecords2();
          extclass.getLineItemString();
          system.assert(extclass != null); 
          
          extclass.getLineItemString();         
          Test.stopTest();     
      }
}