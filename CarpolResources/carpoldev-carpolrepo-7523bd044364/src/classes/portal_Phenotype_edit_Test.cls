@isTest(seealldata=false)
private class portal_Phenotype_edit_Test {
      @IsTest static void portal_Phenotype_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Phenotype__c pheno = new Phenotype__c();
          pheno.Construct__c = objcaj.id;
          pheno.Construct_Name__c = 'test';
          pheno.Phenotypic_Category__c = 'OO-Other';
          pheno.Phenotypic_Description__c = 'test description';
          insert pheno;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.portal_Phenotype_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(new PhenoType__c());
              ApexPages.currentPage().getParameters().put('phenoid',pheno.id);
              ApexPages.currentPage().getParameters().put('constid',objcaj.id);                            
              portal_Phenotype_edit extclass = new portal_Phenotype_edit(sc);
              extclass.updatephenotype();
              extclass.cancelphenotype();
              extclass.ret();  
              System.assert(extclass != null);                       
          Test.stopTest();   
      }
}