@isTest
private class MasterQuestionnaireTrigger_test {

	private static testMethod void test() {
	    List<EFL_Inspection_Questions_Template__c> templatelist = new  List<EFL_Inspection_Questions_Template__c>();
	     List<EFL_Inspection_Questions__c> questionlist = new List<EFL_Inspection_Questions__c>();
	     List<EFL_INS_Template_Question_Junction__c> junctionlist = new List<EFL_INS_Template_Question_Junction__c>();
	     List < EFL_Inspection_Responses__c > OptionsToCreate = new List < EFL_Inspection_Responses__c > ();
         
         Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];
         
         system.runAs(u){
         
         Facility__c fac = new Facility__c();
         fac.Name = 'Test Facility';
         insert fac;
         
         Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
         Inspection__c Ins = new Inspection__c();
         Ins.Facility__c = facc.id;
         Ins.Program__c = 'BRS';
         insert Ins;
         
         
         EFL_Inspection_Questions_Template__c IQT = new  EFL_Inspection_Questions_Template__c();
         IQT.Name = 'Live Plants';
         IQT.Description__c = 'Testing';
         templatelist.add(IQT);
         
         EFL_Inspection_Questions_Template__c IQT2 = new  EFL_Inspection_Questions_Template__c();
         IQT2.Name = 'Live Plants';
         IQT2.Description__c = 'Testing';
         templatelist.add(IQT2);
         
         insert templatelist;
       
         EFL_Inspection_Questions__c IQ = new EFL_Inspection_Questions__c();
         IQ.question__c = 'What is the date today';
         insert IQ;
         
         EFL_Inspection_Questions__c IQQ = [SELECT ID,Name,question__c FROM EFL_Inspection_Questions__c WHERE ID=:IQ.id];
         EFL_Inspection_Questions_Template__c IQTQ = [SELECT ID,Name FROM EFL_Inspection_Questions_Template__c WHERE ID=:IQT.Id];
         
         EFL_Inspection_Responses__c IR = new EFL_Inspection_Responses__c();
         IR.Response__c = 'Response 1';
         IR.EFL_Inspection_Questions__c = IQQ.Id;
         insert IR;
         
         
         EFL_INS_Template_Question_Junction__c IQJ = new EFL_INS_Template_Question_Junction__c();
         IQJ.Inspection_Template_Questions__c = IQQ.Id;
         IQJ.EFL_Inspection_Questions_Template__c = IQT.Id;
         insert IQJ;
         
         Inspection__c Inss = [SELECT ID,Name FROM Inspection__C WHERE Id=:Ins.Id];
         EFL_Inspection_Questionnaire__c IQR = new EFL_Inspection_Questionnaire__c();
         ID IQRRecordTypeId = Schema.SObjectType.EFL_Inspection_Questionnaire__c.getRecordTypeInfosByName().get('Compliance officer/staff').getRecordTypeId();
         IQR.RecordTypeId = IQRRecordTypeId;
         IQR.Inspection__c = Inss.id;
             system.debug('IQR IS '+IQR);
         insert IQR;
         
         IQR.Description__c = 'Test Description';
         IQR.EFL_Inspection_Questions_Template__c = IQTQ.id;
         IQR.Status__c = 'Open';
             system.debug('IQR update is '+IQR);
         update IQR;
          
         IQR.Description__c = 'Test Description2';
         IQR.EFL_Inspection_Questions_Template__c = IQTQ.id;
         IQR.Status__c = 'Finalize Questionnaire';
         update IQR;
             
         IQR.Description__c = 'Test Description3';
         IQR.EFL_Inspection_Questions_Template__c = IQTQ.id;
         IQR.Status__c = 'Approved';
         update IQR;

	}

}
    
}