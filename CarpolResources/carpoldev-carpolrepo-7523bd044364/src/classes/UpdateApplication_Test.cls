@isTest
private class UpdateApplication_Test {

	private static testMethod void UpdateAppTest() 
	{
        CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
        testData.insertcustomsettings();
        Application__c objapp = testData.newapplication();
        AC__c lineItem1 = testData.newLineItem('Resale',objapp);
        AC__c lineItem2 = testData.newLineItem('Adoption',objapp);
          
        Test.startTest();
        Transaction__c trans = new Transaction__c();
        trans.Application__c = objapp.id;
        trans.Transaction_Amount__c = 150;
        trans.Transaction_Date_Time__c = System.now();
        insert trans;
        PageReference pageRef = Page.UpdateApplication;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        UpdateApplication updateClass = new UpdateApplication(sc);
        updateClass.appUpdate();
        updateClass.goBackApplication();
        
	}

}