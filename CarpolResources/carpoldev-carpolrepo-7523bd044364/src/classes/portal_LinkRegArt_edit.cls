public class portal_LinkRegArt_edit {
    
//public string lineItemID {get;set;}
public String Id = ApexPages.currentPage().getParameters().get('ID');
public List<AC__c> lineitemlst {get;set;}
public string redirect = ApexPages.currentPage().getParameters().get('redirect');
public Id LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
public Id strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
public Id Lineitem {get;set;}
public List<Link_Regulated_Articles__c> larlst {get;set;}   
public Link_Regulated_Articles__c obj {get; set;}
public List<Regulated_Article__c> listRA {get;set;} 
public List<Regulated_Article__c> listRSA{get;set;} 
public string sSelCat {get;set;}
public String status_active ='Active';
public string sSelArticle {get;set;}
public list<string> RAIdList {get;set;}
public boolean bSearchClicked                       { get; set; }
 Map<String, String> allAnswersMap = new Map<String, String>();
  public String selectedCategoryName {get;set;}
public boolean bShowArticle                         { get; set; }
 public boolean isradio {get;set;} 
 public boolean showError {get;set;} 
 public map<Id,String> mapCatName                    { get; set; }
 public string sSrhArticle                           { get; set; }
 public string SelectedPathwayId                             { get; set; }
    public portal_LinkRegArt_edit (ApexPages.StandardController controller) {
        mapCatName = new map<Id,String>();
        bShowArticle = false;
        isradio = false;
        showError = false;
        bSearchClicked = false;
        obj = (Link_Regulated_Articles__c)controller.getRecord();
        obj.Line_Item__c = ApexPages.currentPage().getParameters().get('LineItemId');
        //Lineitem = [SELECT Line_Item__c FROM Link_Regulated_Articles__c 
                                                   //WHERE ID = :Id LIMIT 1].Id;
        //Lineitem = larlst[0].Line_Item__c;
        listRA=new List<Regulated_Article__c>(); // VV added
        listRSA=new  list<Regulated_Article__c >();
        sSelCat = '';
        sSelArticle = '';
        RAIdList = new list<String>();
        system.debug('strPathwayId****'+strPathwayId);
        strPathwayId = [select id,Program_Line_Item_Pathway__c from ac__c where id =: obj.Line_Item__c].Program_Line_Item_Pathway__c;
        list<RA_Scientific_Name_Junction__c> listRAPW = [Select Regulated_Article__c from RA_Scientific_Name_Junction__c where (Program_Pathway__c=:strPathwayId or Program_Pathway__r.Name =:strPathwayId ) and Status__c =: 'Active'];
                         for(RA_Scientific_Name_Junction__c r:listRAPW ){
                             RAIdList.add(r.Regulated_Article__c);
                         }
                         system.debug('!!! List of Regulated Article IDs'+RAIdList);
                         listRA=[Select Name,Category_Group_Ref__r.Name,Category_Group_Ref__c from Regulated_Article__c where Id in:RAIdList];
                         system.debug('---'+listRA);
    }
    public PageReference cancelLAR(){
        if(redirect == 'yes'){
            PageReference pg = Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }else if(obj.id != null){
        PageReference pg = new PageReference('/'+obj.id);
        pg.setRedirect(true);
        return pg;
        }
        return null;
    }
    public PageReference SaveLAR(){
        system.debug('^^^^ sSelArticle = '+sSelArticle);
        system.debug('^^^^ obj.Regulated_Article__c = '+obj.Regulated_Article__c);
        system.debug('^^^^ String.isBlank(obj.Regulated_Article__c) = '+String.isBlank(obj.Regulated_Article__c));
       // if ((obj.Regulated_Article__c != null && obj.Regulated_Article__c != '') || (sSelArticle != null && sSelArticle != ''))
       if ((!(String.isBlank(obj.Regulated_Article__c))) || (sSelArticle != null && sSelArticle != ''))
        {
            if(sSelArticle != null && sSelArticle != ''){    
               obj.Regulated_Article__c = sSelArticle;}
        try{
            if(obj.id == null){
               insert obj;
            }else {
                update obj;
            }   
            PageReference pg = Page.portal_LinkRegArt_detail;
            pg.getParameters().put('id',obj.id);
            pg.setRedirect(true);
            return pg;
        }
        catch (DmlException ex){
            ApexPages.addMessages(ex);
            return null;
        }
       }else{
             apexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Regulated Article is a required field. Please select a value'));
             return null;  
       }        
    }
     // VV added         
            
    // Get Category Options
      public List<SelectOption> getCategory() { 
        set<String> setNames=new set<String>();        
        List<SelectOption> options = new List<SelectOption>();
        if(listRA!= null  && listRA.size()>0){
            
            if(listRA.size()>10){
                options.add(new SelectOption('','--All--'));
                
            }else{
                options.add(new SelectOption('','--All--')); 
            }
             map<string,string> catMap = new map<String,string>(); //Noharika 3/9
            for(Regulated_Article__c recRA: listRA){
                if(setNames.add(recRA.Category_Group_Ref__r.Name))
                {
                    //options.add(new SelectOption(recRA.Category_Group_Ref__c,recRA.Category_Group_Ref__r.Name)); //VV commented 3-3-16
                   // options.add(new SelectOption(recRA.Category_Group_Ref__r.Name,recRA.Category_Group_Ref__r.Name)); //VV 3-3-16
                   system.debug('===>'+recRA);
                     mapCatName.put(recRA.Category_Group_Ref__c,recRA.Category_Group_Ref__r.Name);
                     catMap.put(recRA.Category_Group_Ref__r.Name,recRA.Category_Group_Ref__c); //Niharika 3/9
                    
                }
            }
            //added niharika 3/9 to sort categories map alphabetically
             List<String> CategoriesList = new List<String>();
            CategoriesList.addAll(catMap.keySet());
            CategoriesList.sort();
            system.debug('*=== catMap==='+catMap);
            for(String st: CategoriesList){
              if (catMap.get(st) != null){
               options.add(new SelectOption(catMap.get(st),st)); 
               }
            }
            
            
            //options.sort(); //VV 3-3-16
            system.debug('$%$% categ options = '+options);
            return options;
        }
        else
            return null;
    }
    /*-------------------------------------Regulated Article display---------------------------------------------*/       
   public void displayRegArticles()
    {
        getArticles();
     }
     
   public List<SelectOption> getArticles() {    
         system.debug(' ===> getArticles');
         system.debug(' ===> sSelCat---'+sSelCat);
        List<SelectOption> options = new List<SelectOption>();
        string strquery = 'select Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c from Regulated_Article__c where ID in:RAIdList and status__c =:status_active ';
      system.debug('strquery1@@@@'+strquery);
      system.debug('sSelCat@@@@'+sSelCat);
        if(sSelCat!=null && sSelCat!='')
            {
                strquery += 'and Category_Group_Ref__c=:sSelCat ';
                bShowArticle = true;
                if(sSrhArticle!=null && sSrhArticle!=''){
                strquery += ' and ( Name like \'%'+sSrhArticle+'%\'OR Additional_Common_Name__c like \'%'+sSrhArticle+'%\'OR Family__c like \'%'+sSrhArticle+'%\'OR Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Additional_Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Common_Disease_Name__c like \'%'+sSrhArticle+'%\'OR Common_Name__c like \'%'+sSrhArticle+'%\')';
                } 
                system.debug('strquery2@@@@'+strquery);
                listRSA=database.query(strquery);
                listRSA.sort();
            }
            else{
              
               //  strquery += 'and Category_Group_Ref__c=:sSelCat ';
                bShowArticle = true;
                if(sSrhArticle!=null && sSrhArticle!=''){
                strquery += ' and ( Name like \'%'+sSrhArticle+'%\'OR Additional_Common_Name__c like \'%'+sSrhArticle+'%\'OR Family__c like \'%'+sSrhArticle+'%\'OR Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Additional_Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Common_Disease_Name__c like \'%'+sSrhArticle+'%\'OR Common_Name__c like \'%'+sSrhArticle+'%\')';
                } 
                system.debug('strquery3@@@@'+strquery);
                listRSA=database.query(strquery); 
                 listRSA.sort();
                 //  bShowArticle = false;
            }
            
            
              system.debug('---strquery---'+strquery);
              system.debug('---listRSA---'+listRSA);
              system.debug('---sSrhArticle---'+sSrhArticle);
              system.debug('---showError1---'+showError);  
            if(listRSA.isEmpty()){
               // if((sSelCat!=null && sSelCat!='') ||  (sSrhArticle!=null && sSrhArticle!=''))
                showError = true;
                system.debug('---showError+listRSA---'+showError); 
            }
          /*  else if((sSrhArticle!=null && sSrhArticle!='')){
                showError = true;
                system.debug('---showError+sSrhArticle---'+showError); 
            }*/
            else{
                showError = false;
                system.debug('---showError + else---'+showError); 
            }
            
            for(Regulated_Article__c ra:listRSA){
                options.add(new SelectOption(ra.id,ra.Name)); 
            }
         system.debug('---showError2---'+showError);   
        system.debug('---'+options);
        if(options.size()<7){
            isradio = true;
        }else{
            isradio = false;
        }
        
       // options.sort();  
        if(bShowArticle){
          return options;
        }
        return null;
        
    }
 /*-------------------------------------Regulated Article Search Functionality----------------------------------------------*/
    public void searchArticle()
    {
        getArticles();
    }

}