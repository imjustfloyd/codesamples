@isTest(seealldata=true)
private class CARPOL_BRS_NEPA_Decision_Document_Test {
		 
      @IsTest static void NEPA_Decision_Document_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newAuth(objapp.Id);           
          objauth.BRS_Introduction_Type__c = 'Release';
          update objauth;
          AC__c ac1 = testData.newLineItem('Personal Use',objapp); 
          ac1.Authorization__c = objauth.id;
          update ac1;       
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          ac2.Authorization__c = objauth.id;
          update ac2;                 
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);              
          ac3.Authorization__c = objauth.id;
          update ac3; 
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           

          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Complete');
          objwft.Workflow_Order__c = 2;
          update objwft;
          Auth_Related_Records__c objarr = testData.newauthrelatedrecord(objwft.id,objauth.id);
		  testData.setARRecords(objarr);
		  
          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_NEPA_Decision_Document;
          Test.setCurrentPage(pageRef);
          ApexPages.currentPage().getParameters().put('authid',objauth.id);
          ApexPages.currentPage().getParameters().put('wfid',objwft.id);
          ApexPages.currentPage().getParameters().put('id',objarr.id);          
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          CARPOL_BRS_NEPA_Decision_Document extclassempty = new CARPOL_BRS_NEPA_Decision_Document(sc);          
          extclassempty.arronload();
          extclassempty.doupdate();
          CARPOL_BRS_NEPA_Decision_Document extclass = new CARPOL_BRS_NEPA_Decision_Document(sc);
          extclass.arronload();
          extclass.doupdate();
          extclass.cancel();
          //extclass.save();
          extclass.potentialcheckbox2();          

          extclass.checkboxtab2 = true;
          extclass.checkboxtab3 = true;
          extclass.getseleoptions('X1_Does_this_Document_contain_CBI__c');
          extclass.getX1types();
          extclass.getX2types();
          extclass.getx3Types();
          extclass.getx4Types();
          extclass.getx5Types();
          extclass.getx6Types();
          extclass.getx7Types();
          extclass.getx8Types();
          extclass.getx9Types();
          extclass.getx10Types();
          extclass.getx11Types();
          extclass.getx12Types();
          extclass.getx13Types();
          extclass.getx14Types();
          extclass.getx15Types();
          extclass.getx16Types();
          extclass.getx17Types();
          extclass.getx18Types();
          extclass.getM1Types();
          extclass.getM2Types();
          extclass.getM3Types();
          extclass.getM4Types();
          extclass.getM5Types();
          extclass.getM6Types();
          extclass.getM7Types();
          extclass.getM8Types();
          extclass.getR1Types();
          extclass.getR2Types();
          extclass.getR3Types();          
          extclass.getR4Types();
          extclass.getR5Types();
          extclass.getR6Types();
          extclass.getR7Types();
          extclass.getR8Types();
          extclass.getR9Types();
          extclass.getR10Types();
          extclass.getR11Types();
          extclass.getR12Types();
          extclass.getR13Types();
          extclass.getR14Types();

          
          
          extclass.potentialcheckbox();
          extclass.submit();
         // extclass.GeneratePermit();
         //extclass.AttachPermit();
          objauth.BRS_Introduction_Type__c = 'Import';
          update objauth;
          ApexPages.currentPage().getParameters().put('authid',objauth.id);
          ApexPages.currentPage().getParameters().put('wfid',objwft.id);
          ApexPages.currentPage().getParameters().put('id',objarr.id);
          CARPOL_BRS_NEPA_Decision_Document extclass1 = new CARPOL_BRS_NEPA_Decision_Document(sc);
          extclass1.arronload();
          extclass1.doupdate();
          extclass1.potentialcheckbox();
          extclass1.potentialcheckbox2();
          extclass1.submit();
          extclass1.cancel();         
          //extclass1.save();
          system.assert(extclass1 != null);
          Test.stopTest();     
      }
      
}