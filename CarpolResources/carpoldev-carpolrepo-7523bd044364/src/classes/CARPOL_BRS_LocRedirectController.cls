public class CARPOL_BRS_LocRedirectController{
transient public String optyURL2 {get; set;}
   public pagereference Redirect()
   {
        RecordType RecType = [Select Id,Name From RecordType  Where SobjectType = 'Location__c' and DeveloperName = 'Release_Location'];

        If(ApexPages.currentPage().getParameters().get('RecordType') != RecType.Id || (ApexPages.currentPage().getParameters().get('RecordType') == RecType.Id && (ApexPages.currentPage().getParameters().get('Int')!='Interstate Movement' && ApexPages.currentPage().getParameters().get('Int')!='Importation' )) )
        {
              String hostname = ApexPages.currentPage().getHeaders().get('Host'); 
              String optyURL2 = ApexPages.currentPage().getUrl();
              system.debug('**************' + optyURL2);
              system.debug('**************' + ApexPages.currentPage().getParameters().get('RecordType'));
              system.debug('**************' + ApexPages.currentPage().getParameters().get('Int'));
              optyURL2= optyURL2.replace('/apex/carpol_brs_location','/a0W/e');
              system.debug('**************' + optyURL2);
              pagereference pageref = new pagereference(optyURL2);
              pageref.setredirect(true);
              return pageref;
        }
        else
            
             //String optyURL2 = ApexPages.currentPage().getUrl();
            // optyURL2= optyURL2.replace('/apex/carpol_brs_location','/setup/ui/recordtypeselect.jsp?ent=01Ir000000009H5');
            // system.debug('**************' + optyURL2);
             return null;
   }
  
}