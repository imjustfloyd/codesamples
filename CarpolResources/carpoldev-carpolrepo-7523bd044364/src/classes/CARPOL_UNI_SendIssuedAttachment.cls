Public Class CARPOL_UNI_SendIssuedAttachment {


 // These variables store Trigger.oldMap and Trigger.newMap
 // Map<Id, AC__c> oldLine;
  Map<Id, Authorizations__c> newLine;
  Set<ID> setLineIds = new Set<ID>();
String URLL;
// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_UNI_SendIssuedAttachment(
   // Map<Id, AC__c> oldTriggerLines, 
    Map<Id, Authorizations__c> newTriggerLines) {
        
        List<ConnectApi.Community> communities = ConnectApi.Communities.getCommunities().communities;
            
            String hostVal = '';
            
            for(ConnectApi.Community community:communities){
                if(community.name == 'APHIS Customer Community')
                    
                    hostVal = community.siteUrl; 
            }
            
            String urlVal = '/portal_application_detail?id=';
           
            URLL =  hostVal + urlVal;
            
    //  oldLine = oldTriggerLines;
      newLine = newTriggerLines;
      system.debug('#######################      ' + newTriggerLines + '     #############################');
  }
 
 
 public void sendIssuedAttachment( )  {

   for(Authorizations__c au : newLine.values())
     {
    if (au.Status__c == 'Issued' && au.Documents_Sent_to_Applicant__c==False  && au.Authorization_Type__c != 'Letter of No Jurisdiction' && au.Authorization_Type__c != 'Letter of No Permit Required' && au.Authorization_Type__c != 'Letter of Denial' )
    {
        Authorizations__c authUp = [select Id,Documents_Sent_to_Applicant__c,application__r.Applicant_Email__c from authorizations__c where Id=:au.id];
        List<Workflow_Task__c> wtask = [select Status__c from Workflow_Task__c where Authorization__c = :au.Id AND Owner_Approver_Is_Signatory__c=True and status__c = 'Complete'];
        System.debug('<<<<<<<<<<<<<<< Workflow Task' + wtask + ' >>>>>>>>>>>>>>>>>>>>');
        String st = au.Status__c;
        System.debug('<<<<<<<<<<<<<<<<< Old Status ' + st + '>>>>>>>>>>>>>>>>>>>>>>>>');
       // System.debug('<<<<<<<<<<<<<<<<<< Size: ' + wtask.size() + ' Auth Status: ' + au.Status__c + ' WT Status: ' + wtask[0].Status__c + ' >>>>>>>>>>>>>>>>>>>');
     if( au.Status__c == 'Issued'){
    // if(wtask.size() > 0 && au.Status__c == 'Issued'){
            System.debug('<<<<<<<<<<< Entering Conditions >>>>>>>>>>>>>>>>>');
            List<Attachment> att = [SELECT ID, Name, Body FROM Attachment WHERE (Name like 'LetterOf%' OR Name like 'AuthorizationLetter%') AND ParentID = : au.ID ORDER BY CreatedDate DESC LIMIT 1];
            List<Application__c> app = [SELECT ID, Applicant_Name__r.Name, Name FROM Application__c WHERE ID = : au.Application__c ORDER BY CreatedDate DESC LIMIT 1];
            List<Attachment> att_permit = [SELECT ID, Name, Body FROM Attachment WHERE Name like 'Permit%' AND ParentID = : au.ID ORDER BY CreatedDate DESC LIMIT 1];
            //List<Application__c> applicantEmail = [SELECT applicant_email__c from Application__c WHERE ID = : au.application_number__c  LIMIT 1];
            System.debug('<<<<<<<<<<<<<<<<< Attachment size' + att.size() + ' >>>>>>>>>>>>>>>>>>>>');
            System.debug('<<<<<<<<<<<<<<<<< Permit size' + att_permit.size() + ' >>>>>>>>>>>>>>>>>>>>');
           // if(att.size() > 0)
            Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();
                                system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ authUp.application__r.Applicant_Email__c ==== ' + authUp.application__r.Applicant_Email__c + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
                // Strings to hold the email addresses to which you are sending the email.
                String[] toAddressesAppEmail = new String[] {authUp.application__r.Applicant_Email__c}; 
                String[] ccAddressesAppEmail = new String[] {'acarr@phaseonecg.com'};
            
                // Assign the addresses for the To and CC lists to the mail object.
                appEmail.setToAddresses(toAddressesAppEmail);
                appEmail.setCcAddresses(ccAddressesAppEmail);

                //Email Activity History
                appEmail.setSaveAsActivity(true);
                appEmail.setTargetObjectId(au.Applicant__c);
                appEmail.setWhatId(au.id);
                
                // Specify the address used when the recipients reply to the email. 
                appEmail.setReplyTo('support@efile.com');
                                
                // Specify the name used as the display name.
                appEmail.setSenderDisplayName('eFile Support');
            
               
                                
                // Specify the subject line for your email.
                if(au.Authorization_Type__c == 'Letter of No Jurisdiction'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of No Jurisdiction Response for Application : ' + app[0].Name);
                } 
                if(au.Authorization_Type__c == 'Letter of No Permit Required'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of No Permit Required Response for Application : ' + app[0].Name);
                } 
                if(au.Authorization_Type__c == 'Letter of Denial'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of Denial Response for Application : ' + app[0].Name);
                }
                 appEmail.setSubject('USDA APHIS eFile - Permit Response for Application : ' + app[0].Name);         
                // Set to True if you want to BCC yourself on the email.
                appEmail.setBccSender(false);
                                
                // Optionally append the salesforce.com email signature to the email.
                // The email address of the user executing the Apex Code will be used.
                appEmail.setUseSignature(false);
                                
                String appEmailBody = 'Dear ' + app[0].Applicant_Name__r.Name;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app[0].Name;
                //appEmailBody += ' Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/> <a href="' +  URLL  + app[0].ID + '">click here</a>  to check the status of this application online <br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
            
                appEmail.setHtmlBody(appEmailBody);
                                
                //Blob body = att[0].Body;
                 /* removing attachment for  as instructed in W-006826
                // Create the email attachment
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setFileName(au.Authorization_Type__c + '_' +  au.Name + '.pdf');
                emailAttachment.setBody(body);
                appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
                */
         
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                authUp.Documents_Sent_to_Applicant__c = True;
                update authUp; 
                System.debug('<<<<<<<<<<<<<<<<< Email Cover Letter Sent >>>>>>>>>>>>>>>>>>');
            
            
            /* No auth should do both permit and denial */
           /* else if(att_permit.size() > 0 && att.size() >= 0){

                // Specify the subject line for your email.
                appEmail.setSubject('USDA APHIS eFile - Permit Response for Application : ' + app[0].Name);
                        
                // Set to True if you want to BCC yourself on the email.
                appEmail.setBccSender(false);
                        
                // Optionally append the salesforce.com email signature to the email.
                // The email address of the user executing the Apex Code will be used.
                appEmail.setUseSignature(false);
                        
               String appEmailBody = 'Dear ' + app[0].Applicant_Name__r.Name;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app[0].Name;
                //appEmailBody += ' Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/> <a href="' +  URLL  + app[0].ID + '">click here</a>  to check the status of this application online <br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
    
                appEmail.setHtmlBody(appEmailBody);
                System.debug('<<<<<<<<<<<<<<<<< ATT ' + att.size() + ', Permit ' + att_permit.size() + '>>>>>>>>>>>>>>>>>>');
              
          
               if(att.size() > 0 && att_permit.size() > 0){
                        
                    Blob body = att_permit[0].Body;
                  
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                    authUp.Documents_Sent_to_Applicant__c = True;
                    update authUp; 
                   
                }
                else
                
                 
                
                // Send the email you have created.
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });//added by ram 4/14/2016 for bug : W-006726
                System.debug('<<<<<<<<<<<<<<<<< Email Permit Sent >>>>>>>>>>>>>>>>>>');
            }
            else
            {
                System.debug('<<<<<<<<<<<<<<<<< No attachment >>>>>>>>>>>>>>>>>>');
            }
              */
        }

    }  

}
    }
 
public void sendIssuedAttachmentWithoutWFT()  {
   for(Authorizations__c au : newLine.values())
     {
    if (au.Status__c == 'Issued')
    {
        Authorizations__c authUp = [select Id,application__r.Applicant_Email__c ,Documents_Sent_to_Applicant__c from authorizations__c where Id=:au.id];
        //List<Workflow_Task__c> wtask = [select Status__c from Workflow_Task__c where Authorization__c = :au.Id AND Owner_Approver_Is_Signatory__c=True and status__c = 'Complete'];
        //System.debug('<<<<<<<<<<<<<<< Workflow Task' + wtask + ' >>>>>>>>>>>>>>>>>>>>');
        String st = au.Status__c;
        System.debug('<<<<<<<<<<<<<<<<< Old Status ' + st + '>>>>>>>>>>>>>>>>>>>>>>>>');
       // System.debug('<<<<<<<<<<<<<<<<<< Size: ' + wtask.size() + ' Auth Status: ' + au.Status__c + ' WT Status: ' + wtask[0].Status__c + ' >>>>>>>>>>>>>>>>>>>');
     if(au.Status__c == 'Issued'){
    // if(wtask.size() > 0 && au.Status__c == 'Issued'){
            System.debug('<<<<<<<<<<< Entering Conditions >>>>>>>>>>>>>>>>>');
            List<Attachment> att = [SELECT ID, Name, Body FROM Attachment WHERE  ParentID = : au.ID ORDER BY CreatedDate DESC LIMIT 1];
            List<Application__c> app = [SELECT ID, Applicant_Name__r.Name, Name FROM Application__c WHERE ID = : au.Application__c ORDER BY CreatedDate DESC LIMIT 1];
            List<Attachment> att_permit = [SELECT ID, Name, Body FROM Attachment WHERE ParentID = : au.ID ORDER BY CreatedDate DESC LIMIT 1];
            //List<Application__c> applicantEmail = [SELECT applicant_email__c from Application__c WHERE ID = : au.application_number__c  LIMIT 1];
            System.debug('<<<<<<<<<<<<<<<<< Attachment size' + att.size() + ' >>>>>>>>>>>>>>>>>>>>');
            System.debug('<<<<<<<<<<<<<<<<< Permit size' + att_permit.size() + ' >>>>>>>>>>>>>>>>>>>>');
           // if(att.size() > 0)
            Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();
//                                system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ authUp.application__r.Applicant_Email__c ==== ' + authUp.application__r.Applicant_Email__c + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
                // Strings to hold the email addresses to which you are sending the email.
                String[] toAddressesAppEmail = new String[] {authUp.application__r.Applicant_Email__c}; 
                String[] ccAddressesAppEmail = new String[] {'acarr@phaseonecg.com'};
            
                // Assign the addresses for the To and CC lists to the mail object.
                appEmail.setToAddresses(toAddressesAppEmail);
                appEmail.setCcAddresses(ccAddressesAppEmail);
                
             /*   //Email Activity History
                appEmail.setSaveAsActivity(true);
                appEmail.setTargetObjectId(au.Applicant__c);
                appEmail.setWhatId(au.id); */
                
                // Specify the address used when the recipients reply to the email. 
                appEmail.setReplyTo('support@efile.com');
                                
                // Specify the name used as the display name.
                appEmail.setSenderDisplayName('eFile Support');
            if(att.size() > 0 && att_permit.size() == 0)
            {
               
                                
                // Specify the subject line for your email.
                if(au.Authorization_Type__c == 'Letter of No Jurisdiction'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of No Jurisdiction Response for Application : ' + app[0].Name);
                } 
                if(au.Authorization_Type__c == 'Letter of No Permit Required'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of No Permit Required Response for Application : ' + app[0].Name);
                } 
                if(au.Authorization_Type__c == 'Letter of Denial'){
                    appEmail.setSubject('USDA APHIS eFile - Letter of Denial Response for Application : ' + app[0].Name);
                }
                          
                // Set to True if you want to BCC yourself on the email.
                appEmail.setBccSender(false);
                                
                // Optionally append the salesforce.com email signature to the email.
                // The email address of the user executing the Apex Code will be used.
                appEmail.setUseSignature(false);
                                
                String appEmailBody = 'Dear ' + app[0].Applicant_Name__r.Name;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app[0].Name;
                //appEmailBody += ' Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                  appEmailBody += '<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                 appEmailBody += '<br/> <a href="' +  URLL  + app[0].ID + '">click here</a>  to check the status of this application online<br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
            
                appEmail.setHtmlBody(appEmailBody);
                                
                //Blob body = att[0].Body;
                 /* removing attachment   as instructed in W-006826
                // Create the email attachment
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setFileName(au.Authorization_Type__c + '_' +  au.Name + '.pdf');
                emailAttachment.setBody(body);
                appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
                */
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                authUp.Documents_Sent_to_Applicant__c = True;
                update authUp; 
                System.debug('<<<<<<<<<<<<<<<<< Email Cover Letter Sent >>>>>>>>>>>>>>>>>>');
            }
            /* No auth should do both permit and denial */
            else if(att_permit.size() > 0 && att.size() >= 0){

                // Specify the subject line for your email.
                appEmail.setSubject('USDA APHIS eFile - Permit Response for Application : ' + app[0].Name);
                        
                // Set to True if you want to BCC yourself on the email.
                appEmail.setBccSender(false);
                        
                // Optionally append the salesforce.com email signature to the email.
                // The email address of the user executing the Apex Code will be used.
                appEmail.setUseSignature(false);
                        
                String appEmailBody = 'Dear ' + app[0].Applicant_Name__r.Name;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app[0].Name + '.';
                appEmailBody += '<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                 appEmailBody += '<br/> <a href="' +  URLL  + app[0].ID + '">click here</a>  to check the status of this application online <br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
    
                appEmail.setHtmlBody(appEmailBody);
                System.debug('<<<<<<<<<<<<<<<<< ATT ' + att.size() + ', Permit ' + att_permit.size() + '>>>>>>>>>>>>>>>>>>');
              
          
               if(att.size() > 0 && att_permit.size() > 0){
                        
                   // Blob body = att_permit[0].Body;
                     /* removing attachment  as instructed in W-006826
                    // Create the email attachment
                    Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                    emailAttachment.setFileName(au.Authorization_Type__c + '_' +  au.Name + '.pdf');
                    emailAttachment.setBody(body);
                
                    Blob body_cover = att[0].Body;
                    Messaging.EmailFileAttachment emailAttachment1 = new Messaging.EmailFileAttachment();
                    emailAttachment1.setFileName(au.Authorization_Type__c + '_' + au.Name + '_Cover_Letter.pdf');
                    emailAttachment1.setBody(body_cover);
                    appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment, emailAttachment1});
                    */
                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                    //authUp.Documents_Sent_to_Applicant__c = True;
                    //update authUp; 
                }
                else
                // Send permit if not an LOD 
                 
                
                // Send the email you have created.
               
                System.debug('<<<<<<<<<<<<<<<<< Email Permit Sent >>>>>>>>>>>>>>>>>>');
            }
            else
            {
                System.debug('<<<<<<<<<<<<<<<<< No attachment >>>>>>>>>>>>>>>>>>');
            }
              /*
              system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ PERMIT ==== ' + att_permit.size() + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
              if(att_permit.size() > 0){   
              system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Applicant Name ==== ' + au.Applicant_Name__c + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
              String appEmailBody = 'Dear ' + au.Applicant_Name__c;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + au.Application__c + '.';
                appEmailBody += '<br/><br/><br/> Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/>You may check the status of this application online at <a href= "https://aphis--carpoldev.cs32.my.salesforce.com">https://aphis--carpoldev.cs32.my.salesforce.com</a> .<br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/><br/><br/>APHIS Animal Care<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
            system.debug('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ appEmailBody ==== ' + appEmailBody + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
             
                appEmail.setHtmlBody(appEmailBody);     
                    Blob body = att_permit[0].Body;
                    // Create the email attachment
                    Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                    emailAttachment.setFileName('Permit.pdf');
                    emailAttachment.setBody(body);
                    appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
                  
         
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                System.debug('<<<<<<<<<<<<<<<<< Email Permit Sent >>>>>>>>>>>>>>>>>>');
                }*/
        }

    }      
    
    }
}
}