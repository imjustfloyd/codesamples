public class portal_Phenotype_edit {
    public Phenotype__c obj {get;set;}
    public id id {get;set;}
    public id id1 {get;set;}
    public id phenotypeid {get;set;}
    public id phenid {get;set;}
    public id conId;
    public id lineitemid {get;set;}
    public String usertype {get;set;}
    
    public portal_Phenotype_edit(ApexPages.StandardController controller) {
                userType = UserInfo.getUserType();
                obj = (Phenotype__c)controller.getRecord();
                obj.Construct__c = ApexPages.currentPage().getParameters().get('constid');
                //system.debug('obj.Construct__c'+obj.Construct__c);
                //ConstID = ApexPages.currentPage().getParameters().get('constructID');
                phenotypeid = ApexPages.currentPage().getParameters().get('phenoid');
                //conId = obj.Construct__c;
                System.debug('Phenotype ID :' + phenotypeid);
                List<Phenotype__c> phenolst = [SELECT Construct__c,Phenotypic_Category__c,Phenotypic_Description__c,construct__r.Line_Item__c
                                                                         FROM Phenotype__c WHERE ID = :phenotypeid];
                if(phenolst.size()>0){
                        for(Phenotype__c gen : phenolst){
                                obj.Construct__c = gen.Construct__c;
                                obj.Phenotypic_Category__c = gen.Phenotypic_Category__c;
                                obj.Phenotypic_Description__c = gen.Phenotypic_Description__c;
                        }
                }
                conId = obj.Construct__c;
                if(obj.Construct__c != null)
                   lineitemid = [select id, Line_Item__c from construct__c where id =: obj.Construct__c].Line_Item__c;

    }
    public pagereference updatephenotype(){
        try{
                id1 = ApexPages.currentPage().getParameters().get('phenoid');
                obj.id = id1;
                upsert obj;

                PageReference pg = Page.portal_phenotype_detail;
                pg.getParameters().put('id',obj.id);
                pg.setRedirect(true);
                return pg;
        }catch(Exception ex){
                ApexPages.addMessages(ex);
                return null;
        }        
}

 public pageReference ret(){
          
       PageReference pg = Page.portal_construct_detail;
       //pg.getParameters().put('strPathwayId',strPathwayId); 
       pg.getParameters().put('id',conId);
       pg.setRedirect(true);
       return pg;
}
public pagereference cancelphenotype(){
        id = ApexPages.currentPage().getParameters().get('constid');
        phenid = ApexPages.currentPage().getParameters().get('phenoid');
        if(phenid != null){
                        obj.id = phenid;
                        PageReference pg = Page.portal_phenotype_detail;
            pg.getParameters().put('id',obj.id);
            pg.setRedirect(true);
            return pg;
        }else{
                PageReference pg = Page.portal_construct_detail;
            pg.getParameters().put('id',id);
            pg.setRedirect(true);
                return pg;
        }
    }

}