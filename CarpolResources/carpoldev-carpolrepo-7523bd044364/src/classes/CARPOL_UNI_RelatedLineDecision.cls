public class CARPOL_UNI_RelatedLineDecision 

    {
        public static List<AC__C> insertRLD(List <AC__c> LineList)
            {
            
                
                
                List<AC__c> returnList = new List<AC__c>();
                AC__c lineitem = linelist[0];
                AC__c testlinelist = new AC__c();
                testlinelist = [SELECT ID, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Name, 
                                   Program_Line_Item_Pathway__r.Program__r.Name, Program_Line_Item_Pathway__r.All_Parent_Pathways__c, Authorization_Group_String__c, AC_Unique_Id__c,Arrival_Time__c,
                                   Status__c, Breed__c, Country_Of_Origin__c, Country_of_Export__c, Date_of_Birth__c, DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__c,
                                   Departure_Time__c, Dog_Out_of_Hawaii_for_Resale__c, Exporter_First_Name__c, Exporter_Last_Name__c, Final_destination_Hawaii__c, Hand_Carry__c, Will_the_Seed_be_processed_in_any_way_th__c,
                                   Has_the_seed_been_genetically_modified__c, Importer_First_Name__c, Importer_Last_Name__c, Intended_Use__c, Intended_Use_for_Seeds__c, Introduction_Type__c,
                                   Is_this_bird_US_originated__c, Live_Dogs_Intended_Use__c, Means_of_Importation__c,Method_of_Transport__c,Scientific_Name__c, Regulated_Article_PackageId__c, 
                                   Movement_Type__c, Number_of_Labels__c, Number_of_New_Design_Protocols__c, Number_of_Release_Sites__c,
                                   Parent_Facility_for_New_Facility__c,Regulated_Article_Status__c,SOP_Status__c,Construct_Status__c,Location_Status__c,
                                   Port_of_Embarkation__c, Port_of_Entry__c, Port_of_Entry_State__c, Processed_anyway_that_change_its_nature__c,
                                   Proposed_date_of_arrival__c, Proposed_End_Date__c, Proposed_Start_Date__c, Purpose_of_the_Importation__c,
                                   Quarantine_Facility__c, RA_Component_Junction__c, Move_out_Hawaii__c, Seed_be_planted_or_propagated__c,
                                    Thumbprint__c, State_Territory_of_Destination__c, Time_Zone_of_Arriving_Place__c, Time_Zone_of_Arriving__c,
                                   Time_Zone_of_Departing_Place__c, Time_Zone_of_Departing__c, Total_of_New_Constructs__c, Will_the_seed_s_be_planted__c, Will_the_seed_s_be_processed__c,
                                   Cloned_Line_Item__c
                                FROM AC__c 
                                WHERE  Id =: lineitem.Id ];
             returnList.add(testlinelist);                   
             
             if(testlinelist.status__c != 'Submitted')
              {
                String res = testlinelist.Program_Line_Item_Pathway__r.All_Parent_Pathways__c;
                string excp = testlinelist.Program_Line_Item_Pathway__r.Name;
                List<AC__C> LineItemsToReturn = new List<AC__C>();
                    
                String[] results = res.split(',');
                List<Country_Junction__c> CountryJunctions = [SELECT Name, Country__c, Group__c FROM Country_Junction__c WHERE Country__c=: lineItem.Country_Of_Origin__c];
                Set <ID> CountryGroupIds = new Set<ID>();
                for(Country_Junction__c country: CountryJunctions)
                  {
                      CountryGroupIds.add(country.Group__c);
                  }
                
                List<Related_Line_Decisions__c>  newRLDList = new List<Related_Line_Decisions__c>();
                List<Regulations_Association_Matrix__c> DecisionMatrixList = new List<Regulations_Association_Matrix__c>();
                List <Regulations_Association_Matrix__c>  dmQuery = new List <Regulations_Association_Matrix__c>();
            
                for(String pathway: results)
                {
                  
                     
                      
                         dmQuery = [SELECT Id,
                                                Number_of_Req_Docs__c, 
                                                Excluded_City_Township__c,
                                                Excluded_City_Township__r.Name ,
                                                Excluded_Countries__c,
                                                Excluded_State_Province__c
                                                From Regulations_Association_Matrix__c 
                                                where Program_Line_Item_Pathway__c = :pathway
                                                AND (Movement_Type__c =: lineItem.Movement_Type__c OR Movement_Type__c = '') 
                                                AND (Country__c =: lineItem.Country_Of_Origin__c OR Country__c = '')
                                                AND (State_Territory_of_Destination__c =: lineItem.State_Territory_of_Destination__c OR State_Territory_of_Destination__c = '')
                                                AND (Export_City_Township__c =:lineitem.Exporter_Mailing_City__c OR Export_City_Township__c ='')
                                                AND  (Export_Country__c =:lineItem.Exporter_Mailing_CountryLU__c OR Export_Country__c = '')
                                                AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
                                                AND (Country_of_Export__c =:lineItem.Country_of_Export__c OR Country_of_Export__c = '')
                                                AND  (Export_State_Province__c =:lineItem.Exporter_Mailing_State_ProvinceLU__c OR Export_State_Province__c = '')
                                                AND  (Level_2_Region__r.Name =:lineItem.Exporter_Mailing_City__c OR Level_2_Region__r.Name = '')
                                                AND  (Method_of_Transportation__c =:lineItem.Transporter_Type__c OR Method_of_Transportation__c = '') 
                                                AND  (Country_Group__c IN: CountryGroupIds OR Country_Group__c='')
                                                AND  (Regulated_Article__c=:lineItem.Scientific_Name__c OR Regulated_Article__c = '') 
                                                AND  (Breed__c =:lineItem.Breed__c OR Breed__c = '')
                                                AND  (Thumbprint__c =:lineItem.Thumbprint__c OR Thumbprint__c = '')
                                                AND  (Intended_Use__c =:lineItem.Intended_Use__c OR Intended_Use__c = '')
                                               ];
                      
                       
                       DecisionMatrixList.addall(dmQuery);
                      
                 }
                 
                         
                 Set<id> DMIDs = new Set <id>();
                 integer j=0;
                 
                 for(Regulations_Association_Matrix__c r : DecisionMatrixList)
                     {
                        DMIDs.add(r.Id);
                        if((r.Excluded_City_Township__r.Name!=null) && (r.Excluded_City_Township__r.Name == lineItem.Exporter_Mailing_City__c))
                            {
                                DMIDs.remove(r.Id);
                            }
                        
                        
                        if((r.Excluded_State_Province__c!=null) && (r.Excluded_State_Province__c == lineItem.State_Territory_of_Destination__c))
                            {
                                    DMIDs.remove(r.Id);
                            }
                             
                       }
                         
                  
                           for(Regulations_Association_Matrix__c r : DecisionMatrixList)
                               {
                                   if(r.Number_of_Req_Docs__c!=0)
                                       {
                                           j= j+r.Number_of_Req_Docs__c.intValue();
                                       }
                                   
                               }
                               
                               
                               
          //*********************************************************************Required docs logic****************************************************************************************************************************************************                     
                               
                               //----------------------Line attachments
                           List<Applicant_Attachments__c> Lineattachments =  [SELECT ID, Name,RecordTypeID, Document_Types__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c=:lineitem.id]; 
                           String RecName;
                          
                           if(Lineattachments!=null)
                           {        
                                  List<String> lineside = new List<String>();
                                  for(Applicant_Attachments__c a: Lineattachments)
                                  {
                                        RecName = Applicant_Attachments__c.SObjectType.getDescribe().getRecordTypeInfosById().get(a.RecordTypeId).getName();
                                        
                                        system.debug('RecName.contains*****'+RecName);
                                        
                                        system.debug('a.Document_Types__c*****'+a.Document_Types__c);
                                        
                                        if(!RecName.contains('BRS')){
                                            if(a.Document_Types__c.contains(';'))
                                                lineside = a.Document_Types__c.split(';');
                                            else
                                                lineside.add(a.Document_Types__c);        
                                            
                                        }
                                      
                                  }
                                    
                                   system.debug('<! lineside '+lineside);
                                   
                                    Set<String> linecomp = new Set<String>();
                                    linecomp.addall(lineside);
                                   
                                   system.debug('<! linecomp '+lineside);
                                   
                                   
                                   //--------------------------------------DM req docs list 
                                   List<Required_Documents__c> Docslist = [SELECT ID, Decision_Matrix__c, Required_Docs__c FROM Required_Documents__c WHERE Decision_Matrix__c IN: DMIDs];
                                   List<String> dmside = new List<String>();
                                   List<String> strlist1 = new List<String>();
                                   for(Required_Documents__c req : Docslist)
                                       {
                                            if(req.Required_Docs__c.contains(';')){
                                                strlist1 = req.Required_Docs__c.split(';');
                                                }
                                            else{
                                                dmside.add(req.Required_Docs__c);
                                                }
                                            
                                        }
                                    
                                    system.debug('<! dmside '+dmside);
                                       dmside.addall(strlist1);
                                       Set<String> dmcomp = new Set<String>();
                                       dmcomp.addall(dmside);
                                       system.debug('<! dmcomp '+dmcomp);
                                       
                                    if(linecomp.containsall(dmcomp))
                                       testlinelist.Status__c = 'Ready to Submit'; 
                                    else
                                        testlinelist.Status__c = 'Saved';  
                                       
                                   
                                  
                                 
                               
                           }   
                              
     //*************************************************************************************************************************************************************************************************************************                                                   
                         
                         
                           if(testlinelist.Cloned_Line_Item__c == true)
                               {
                                    if(j>0 && Lineattachments.size()==0)
                                        {
                                            testlinelist.Status__c = 'Saved';
                                        }
                                    else if (j==0) 
                                        {
                                            testlinelist.Status__c = 'Ready to Submit';
                            
                                        }
                                
                              }
                          else 
                              {
                                   if(j>0 && Lineattachments.size()==0)
                                 {
                                     testlinelist.Status__c = 'Saved';
                                 }
                                else if (j==0)
                                {
                                    testlinelist.Status__c = 'Ready to Submit';
                                }
                              }
                  
                          
                          
                          
                          //*******************************************************BRS CASE *********************************
                         system.debug('<!!!! testlinelist.Construct_Status__c'+testlinelist.Construct_Status__c);
                         system.debug('<!!!! testlinelist.SOP_Status__c'+testlinelist.SOP_Status__c);
                         system.debug('<!!!! testlinelist.Regulated_Article_Status__c'+testlinelist.Regulated_Article_Status__c);
                         system.debug('<!!!! testlinelist.Location_Status__c'+testlinelist.Location_Status__c);
                         
                        if(testlinelist.Program_Line_Item_Pathway__r.Program__r.Name == 'BRS')
                            { 
                                if( testlinelist.Construct_Status__c == 'Ready to Submit' && testlinelist.SOP_Status__c== 'Ready to Submit' &&
                                    testlinelist.Regulated_Article_Status__c == 'Ready to Submit' && testlinelist.Location_Status__c== 'Ready to Submit')
                                  {
                                    testlinelist.Status__c = 'Ready to Submit';
                    
                                  }
                                else 
                                {
                                    testlinelist.Status__c = 'Saved';  
                               
                                } 
                            }
                       
                          
                   for (ID dm: DMIDs)
                     {
                         newRLDList.add(new Related_Line_Decisions__c(Line_Item__c = lineItem.Id, Decision_Matrix__c = dm));
                     }
                   
                   
                   
                     
                  
                     LineItemsToReturn.add(testlinelist);
                     upsert newRLDList;
                     return LineItemsToReturn;
                }
                else{
                    return returnList;
                }
             } 
    }