@IsTest(SeeAllData=true)
public with sharing class CARPOL_PPQ_sendlabels_Test {

    @IsTest static void ppqSendLabelsTest(){
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        plip.Column_1_API_Name__c = 'Name';
        plip.Column_2_API_Name__c = 'Application__c';
        plip.Column_1_Display_Name__c = 'Name';
        plip.Column_2_Display_Name__c = 'application';
        Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
        objdm.Program_Line_Item_Pathway__c = plip.id;
        insert objdm;
        Application__c objapp = testData.newapplication();
        AC__c li = TestData.newlineitem('Personal Use', objapp);
        Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
        List<RecordType> recrdTypeId = [Select Id from RecordType where name =: 'Plant Protection & Quarantine (PPQ)' and SObjectType = 'Authorizations__c' limit 1];
        Authorizations__c objauth = testData.newAuth(objapp.id);
        li.Authorization__c = objauth.id;
        update li;
        
        List<RecordType> locnRecrdTypeId = [Select Id from RecordType where name =: 'Destination Location' and SObjectType = 'Location__c' limit 1];

        Trade_Agreement__c testAgreement = new Trade_Agreement__c();
        testAgreement.Trade_Agreement_Code__c = 'TA';
        testAgreement.Name = 'Test Agreement';
        testAgreement.Trade_Agreement_Status__c = 'Active';
        insert testAgreement;

        Country__c testCountry = new Country__c();
        testCountry.Name = 'Test country';
        testCountry.Country_Code__c = 'TC';
        testCountry.Trade_Agreement__c = testAgreement.Id;
        insert testCountry;

        Location__c testLocation = new Location__c();
        testLocation.Authorization__c = objauth.ID;
        testLocation.Name = 'Test Location';
        if(locnRecrdTypeId.size() != 0)
            testLocation.RecordTypeID = locnRecrdTypeId[0].Id;
        testLocation.Country__c = testCountry.Id;
        testLocation.Contact_Name1__c = 'Test Contact';
        testLocation.Day_Phone_2__c = '1234567890';
        testLocation.Day_Phone__c = '1234567890';
        testLocation.Quantity__c = 2;
        insert testLocation;

        objauth.Authorization_Type__c = 'Permit';
        objauth.Template__c = objcm.id;
        objauth.Final_Decision_Matrix_Id__c = objdm.id;
        objauth.Date_Issued__c = Date.today();          
        objauth.Program_Pathway__c = plip.id;
        objauth.BRS_Create_Labels__c = true;
        objauth.Status__c = 'Approved';
        objauth.RecordTypeID = recrdTypeId[0].Id;
        update objauth;

        //update objauth;
        // Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
        /*Regulations_Association_Matrix__c  objAssmtr = new Regulations_Association_Matrix__c();
        objAssmtr.Program_Line_Item_Pathway__c = plip.id;
        insert objAssmtr;
        objauth.Final_Decision_Matrix_Id__c = objAssmtr.id;
        objauth.BRS_Create_Labels__c = true;
        objauth.BRS_Number_of_Labels__c = 2.0;
        update objauth;
        Facility__c objport = new Facility__c();
        objport.Name = 'test port';
        objport.Type__c = 'Domestic Port';
        insert objport ;
        Authorization_Junction__c objauthjun = new Authorization_Junction__c();
        objauthjun.Authorization__c = objauth.id;
        objauthjun.Port__c = objport.id;
        insert objauthjun;
        Regulation__c  objreg = new Regulation__c();
        objreg.Regulation_Description__c ='test description';
        objreg.Custom_Name__c = 'test class54444'; 
        objreg.Short_Name__c = 'test';
        insert objreg;
        Signature_Regulation_Junction__c objregjun = new Signature_Regulation_Junction__c();
        objregjun.Regulation__c = objreg.id;
        objregjun.Decision_Matrix__c = objAssmtr.id;
         
        insert objregjun;*/
        Test.startTest(); 

        CARPOL_PPQ_sendlabels.brslabelspdf(objauth.Id);
}

    public PageReference genlabels() {
    string appemail = 'kishore.kumar@creativesyscon.com';
     attachment attlab = new attachment();
            Blob attbody = null;
            PageReference PDFPage = new PageReference('/apex/CARPOL_Soil_PDF_Labels?authid='+authid);  
           // attbody = PDFPage.getContent();
             if(!test.isRunningTest()){
                        attbody = PDFPage.getContent(); 
                        } 
                     else {
                            attbody = Blob.valueOf('Some Text');
                            } 
                attlab.Body = attbody;
                attlab.Name = 'CARPOL_PPQ_Labels.pdf';
                //attlab.contentType = 'pdf';
                attlab.ParentId = authid;
                
            insert attlab;
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            String[] toaddress = new string[]{appemail};
            email.setSubject('PPQ Soil Labels');
            email.setToAddresses(toaddress);
            email.setPlainTextBody( 'PPQ Soil Labels' );
             Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
             efa.setFileName('CARPOL_PPQ_Labels.pdf');
             efa.setBody(attbody);
             email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        return null;
    }

public string authid {get;set;}
    public list<Label__c> labelslst {get;set;}
    public CARPOL_PPQ_sendlabels_Test()
    {
        authid = ApexPages.currentPage().getParameters().get('authid');
    }
}