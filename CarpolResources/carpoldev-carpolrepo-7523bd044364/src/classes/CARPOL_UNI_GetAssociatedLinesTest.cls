@isTest(seealldata=true)
private class CARPOL_UNI_GetAssociatedLinesTest {
    static testMethod void testCARPOL_UNI_GetAssociatedLines() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);      
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      Attachment attach = testData.newattachment(ac.Id);           
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac.Authorization__c = objauth.Id;
      update ac;
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);  
      
      
      /*PageReference pageRef = Page.CARPOL_AC_EditPermit_Regulations;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      */
      CARPOL_UNI_GetAssociatedLines acepreg = new CARPOL_UNI_GetAssociatedLines();
      CARPOL_UNI_GetAssociatedLines.getAssociatedLines(objapp.id);
      System.assert(acepreg != null);
    }
}