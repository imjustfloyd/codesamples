public class portal_construct_edit {
    public Construct__c obj {get;set;} 
    public Id genid {get;set;}
    public Id consId {get;set;}
    public string redirect {get;set;}
    public Id strPathwayId {get;set;}
    public Id LineItemId{get;set;}
    public String usertype{get;set;}

    public portal_construct_edit(ApexPages.StandardController controller) {
    
    system.debug('<<<!!!!redirect value 1' + redirect);
                userType = UserInfo.getUserType();
                obj = (construct__c)controller.getRecord();
                obj.Line_Item__c = ApexPages.currentPage().getParameters().get('LineItemId');
                LineItemId= ApexPages.currentPage().getParameters().get('LineItemId');
                consId = ApexPages.currentPage().getParameters().get('genid');
                redirect = ApexPages.currentPage().getParameters().get('redirect');
                strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
                List<Construct__c> conslst = [SELECT Construct_s__c,Line_Item__c,Status__c,Identifying_Line_s__c,
                                              Mode_of_Transformation__c,Line_Events__c, Corrections_Required__c FROM Construct__c WHERE ID = :consId];
                                              
                                               
                if(conslst.size()>0){
                    for(Construct__c gen : conslst){
                        obj.Construct_s__c = gen.Construct_s__c;
                        obj.Line_Item__c = gen.Line_Item__c;
                        obj.Status__c = gen.Status__c;
                        obj.Identifying_Line_s__c = gen.Identifying_Line_s__c;
                        obj.Line_Events__c = gen.Line_Events__c;
                        obj.Mode_of_Transformation__c = gen.Mode_of_Transformation__c;
                        obj.Corrections_Required__c = gen.Corrections_Required__c;
                    }
                }
             system.debug('<<<!!!!redirect value 3' + redirect);
    }
    
  
    
     public pagereference updateConstruct(){
    try{
            
            
                consId = ApexPages.currentPage().getParameters().get('genid');
                obj.id = consId;
                upsert obj;
             
        PageReference pg = Page.portal_construct_detail;
          
           pg.getParameters().put('id',obj.id);
        
        if(redirect == 'yes'){
           
            pg.getParameters().put('redirect',redirect);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.getParameters().put('LineItemId',LineItemId);
        }
        
        system.debug('<<<<<<<<!!!!! page construct' + pg);
            pg.setRedirect(true);
        return pg;
           
      }catch(Exception ex){
            ApexPages.addMessages(ex);
            return null;
      }
        
    }
    


public pagereference cancelconstruct(){
    /*
        If landed on the portal_construct_edit page from CARPOL_LineItemPageForApplicant
        the below logic works. Added by RP
    */
    
  
    if(redirect == 'yes'){
    PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
    pg.getParameters().put('LineId',LineItemId);
    pg.getParameters().put('strPathwayId',strPathwayId);
    pg.setRedirect(true);
    
    system.debug('<<<!!!!pg'+pg);
    return pg;
    
    }else{
        consId = ApexPages.currentPage().getParameters().get('genid');
        obj.id = consId;
        PageReference pg = Page.portal_construct_detail;
            pg.getParameters().put('id',obj.id);
            pg.setRedirect(true);
        return pg;
        }
    }
}