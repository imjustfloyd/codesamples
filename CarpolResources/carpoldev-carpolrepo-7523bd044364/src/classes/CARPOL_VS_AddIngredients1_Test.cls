@isTest(seealldata=true)
public class CARPOL_VS_AddIngredients1_Test {
      @IsTest static void CARPOL_VS_AddIngredients1_Test() {
          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          Account objacct = new Account();
          objacct.Name = 'Global Account1';
          objacct.RecordTypeId = AccountRecordTypeId;    

          Account objacct1 = new Account();
          objacct1.Name = 'Global Account2';
          objacct1.RecordTypeId = AccountRecordTypeId;  
           
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          VS__c objvs = testData.newVS(objcont.Id, objapp.Id);
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_VS_AddIngredients;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objvs);
              ApexPages.currentPage().getParameters().put('id',objvs.id);
              //ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              //ApexPages.currentPage().getParameters().put('LineId',li.id);                                                                                                                                                      
              //ApexPages.currentPage().getParameters().put('genid',objcaj.id);                                   
              //ApexPages.currentPage().getParameters().put('retPage','Test');                                                                                                                                                                                  

              CARPOL_VS_AddIngredients1 extclass = new CARPOL_VS_AddIngredients1(sc);
              extclass.Add();
              extclass.Add();
              extclass.Add();  
              extclass.selectedRowIndex = '1';
              extclass.Del();
              //extclass.lstAcct.add(objacct);
              //extclass.lstAcct.add(objacct1);                                        
              for (Integer x = 0; x < extclass.lstInner.size(); x++){
                  extclass.lstInner[x].acct.Name = 'Global ' + EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0, 5);
              }
              extclass.Save();
              system.assert(extclass != null);

              CARPOL_VS_AddIngredients1 extclass2 = new CARPOL_VS_AddIngredients1(sc);              
          Test.stopTest();    

      }

}