public without sharing class csstest_control {

    public Id applicationID{get;set;}
    public String Renewal {get;set;}
    public List<AC__c> AC;
    public String lineItemIdToBeDeleted{get;set;}

    public csstest_control(ApexPages.StandardController controller) { 
        applicationID=ApexPages.currentPage().getParameters().get('id');
        // Start RP 
        Renewal = ApexPages.currentPage().getParameters().get('Renewal');
        List<Application__c> lst = [SELECT Id,Renewal_Application__c,Renewal_Proposed_Start_Date__c,Renewal_Proposed_End_Date__c FROM Application__c WHERE ID = :applicationID];
        
        if(lst.size()>0){
            for(Application__c a : lst){
                if(a.Renewal_Application__c == true){
                    Renewal = 'Renewal';
                }
            }
        }
        
        // End RP 
    }
    
    public List<Attachment> getattach(){
        return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }
          
    public List<AC__c> getAC(){
        return [SELECT ID,Name,Purpose_of_the_Importation__c,Status__c, Color__c, Sex__c, Authorization__c,Thumbprint__c,Scientific_name__c,Scientific_name__r.name FROM AC__c WHERE Application_Number__c =:applicationID and (Line_Item_Type_Hidden_Flag__c = 'product' or Line_Item_Type_Hidden_Flag__c = '' )];
        
    }
    
    public List<AC__c> getProductIngredients(){
        return [SELECT ID,Name,Purpose_of_the_Importation__c,Status__c, Color__c, Sex__c, Authorization__c,Thumbprint__c,Scientific_name__c,Scientific_name__r.name,Regulated_Article_PackageId__c,Regulated_Article_PackageId__r.Product_Name__c FROM AC__c WHERE Application_Number__c =:applicationID and Line_Item_Type_Hidden_Flag__c = 'Ingredient'];
        
    }

    public List<Authorizations__c> getAuth(){
        return [SELECT ID, Name, Authorization_Type__c, Status__c, Fee_Amount__c FROM Authorizations__c WHERE Application__c =:applicationID];
    }
    
    //Receipt
    public List<Transaction__c> getTrx(){
        return [SELECT ID, Name, Pay_Gov_Token__c, Pay_gov_Tracking_ID__c, Payment_Type__c, Transaction_Amount__c, Transaction_Date_Time__c
        FROM Transaction__c WHERE Application__c =:applicationID];
        
    }

    public List<Change_History__c > gethistory(){
        return [SELECT CreatedDate , CreatedById,Id,Name,New_Value__c,Old_Value__c FROM Change_History__c WHERE Application__c =:applicationID];
    }
    //VV 10/11/15
    public PageReference editAssocContact() {
        String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Associated_Contact_Create?id='+applicationID);
        dirpage.setRedirect(true);
        return dirpage;
            
    }

    //Added by Subbu -3/23
    public PageReference deleteLineItem(){
        System.debug('Line item to be deleted '+lineItemIdToBeDeleted);
        if(lineItemIdToBeDeleted != null && lineItemIdToBeDeleted != ''){
            AC__c lineItemObj = new AC__c();
            lineItemObj.Id = lineItemIdToBeDeleted;
            delete lineItemObj;
        }
        PageReference dirpage= new PageReference('/' + applicationID);
        dirpage.setRedirect(true);
        return dirpage;
    }

    //END VV 10/11/15
    public PageReference dir() {
            
            PageReference dirpage= new PageReference('/' + applicationID);
            dirpage.setRedirect(true);
            return dirpage;
    }
    public PageReference redirectPage() {return null;} //Vijay added 1-10-16
    }