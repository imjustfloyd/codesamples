@isTest(seealldata=true)
private class Portal_Auth_Detail2_controller_Test {
      @IsTest static void Portal_Authorization_Detail2_controller_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newAuth(objapp.id);
          Date dtExpiry = Date.newInstance(2016,9,7);
          dtExpiry = dtExpiry.addDays(365);
          objauth.Expiry_Date__c = dtExpiry;
          objauth.Application__c = objapp.Id;
          update objauth;
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          objac.Authorization__c = objauth.id;
          objac.Application_Number__c = objapp.Id;
          update objac;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'In Progress');
          objwft.Workflow_Order__c = 3;
          update objwft;
          Attachment objattach = testData.newAttachment(objauth.id);
          Applicant_Attachments__c objappattach = testData.newAttach(objac.id);
          objappattach.Authorization__c = objauth.id;
          update objappattach;
          
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          
          Program_Line_Item_Section__c mSection1 = new Program_Line_Item_Section__c();
          mSection1.Section_Order__c = 1;
          mSection1.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection1.Destination_Object__c = 'Authorization';
         
          insert mSection1;
          Program_Line_Item_Section__c mSection2 = new Program_Line_Item_Section__c();
          mSection2.Section_Order__c = 1;
          mSection2.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection2.Destination_Object__c = 'Line Item';
          insert mSection2;
          
          Program_Line_Item_Field__c mField1 = new Program_Line_Item_Field__c();
          mField1.isActive__c = 'Yes';
          mField1.Field_API_Name__c = 'ID';
          mField1.Name = 'Test';
          mField1.Program_Line_Item_Section__c = mSection1.id;
          insert mField1;
        
          Program_Line_Item_Field__c mField2 = new Program_Line_Item_Field__c();
          mField2.isActive__c = 'Yes';
          mField2.Field_API_Name__c = 'ID';
          mField2.Name = 'Test';
          mField2.Program_Line_Item_Section__c = mSection2.id;
          insert mField2;
        
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.Portal_Authorization_Detail2;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.currentPage().getParameters().put('id',objauth.id);                                                                                                   
              Portal_Authorization_Detail2_controller extclass = new Portal_Authorization_Detail2_controller(sc);
              extclass.getattach();
              extclass.getList();
              extclass.getappattach();
              extclass.getlinkRegulations();
              extclass.agreeCompliance();
              extclass.rejectCompliance();
              extclass.Renew();
              extclass.AddingSRP();
              extclass.getreturnURL();
              System.assert(extclass != null);                     
          Test.stopTest();   
      }
}