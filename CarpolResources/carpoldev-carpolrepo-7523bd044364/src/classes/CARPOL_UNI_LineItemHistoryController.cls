public with sharing class CARPOL_UNI_LineItemHistoryController {

// External variables
public SObject myObject {get; set;}
public Integer recordLimit {get; set;}
public static String objectLabel {get;}

// Internal Variables
public objectHistoryLine[] objectHistory;

public static final Map<String, Schema.SObjectType> mySObjectTypeMap = Schema.getGlobalDescribe();
public static Map<String, Schema.SObjectField> myObjectFieldMap;
public static List<Schema.PicklistEntry> historyFieldPicklistValues;

public List<objectHistoryLine> getObjectHistory(){

Id myObjectId = String.valueOf(myObject.get('Id'));
Schema.DescribeSObjectResult objectDescription = myObject.getSObjectType().getDescribe();

myObjectFieldMap = objectDescription.fields.getMap();
objectLabel = String.valueOf(objectDescription.getLabel());

//Get the name of thew history table
String objectHistoryTableName = objectDescription.getName();
//if we have a custom object we need to drop the 'c' off the end before adding 'History' to get the history tables name
if (objectDescription.isCustom()){
objectHistoryTableName = objectHistoryTableName.substring(0, objectHistoryTableName.length()-1);
}
objectHistoryTableName = objectHistoryTableName + 'History';

Schema.DescribeFieldResult objectHistoryFieldField = mySObjectTypeMap.get(objectHistoryTableName).getDescribe().fields.getMap().get('Field').getDescribe();
historyFieldPicklistValues = objectHistoryFieldField.getPickListValues();

list<objectHistoryLine> objectHistory = new list<objectHistoryLine>();

String prevDate = '';

if (recordLimit== null){
    recordLimit = 100;
}

list<sObject> historyList = Database.query( 'SELECT Id, Name, CreatedDate, LastModifiedDate,'+
'CreatedById,'+
'Field__c,'+
'New_Value__c,'+
'Old_Value__c ' +
'FROM ' + 'Change_History__c' + ' ' +
'WHERE Line_Item__c =\'' + myObjectId + '\' ' +
'ORDER BY CreatedDate DESC '+
'LIMIT ' + String.valueOf(recordLimit));

for(Integer i = 0; i < historyList.size(); i++){
    sObject historyLine = historyList.get(i);
/*if ((historyLine.get('New_Value__c') == null && historyLine.get('Old_Value__c') == null)
|| (historyLine.get('New_Value__c') != null && !(string.valueOf(historyLine.get('New_Value__c')).startsWith('005') || string.valueOf(historyLine.get('New_Value__c')).startsWith('00G')))
|| (historyLine.get('Old_Value__c') != null && !(string.valueOf(historyLine.get('Old_Value__c')).startsWith('005') || string.valueOf(historyLine.get('oldValue')).startsWith('00G'))))
{*/
    objectHistoryLine tempHistory = new objectHistoryLine();
    // Set the Date and who performed the action
    //if (String.valueOf(historyLine.get('CreatedDate')) != prevDate){
        tempHistory.theDate = String.valueOf(historyLine.get('CreatedDate'));
        tempHistory.userId = String.valueOf(historyLine.get('CreatedById'));
        tempHistory.who = String.valueOf(historyLine.get('CreatedById'));
        tempHistory.fieldId = String.valueOf(historyLine.get('Id'));
        tempHistory.fieldName = String.valueOf(historyLine.get('Name'));
        tempHistory.oldvalue = String.valueOf(historyLine.get('Old_Value__c'));
        tempHistory.newvalue = String.valueOf(historyLine.get('New_Value__c'));        
        DateTime formattedDate = (DateTime) historyLine.get('LastModifiedDate'); 
        tempHistory.lastmodified = formattedDate.format();           

    //}
    //else{
    /*    tempHistory.theDate = '';
        tempHistory.who = '';
        tempHistory.userId = String.valueOf(historyLine.get('CreatedById'));
        tempHistory.fieldId = String.valueOf(historyLine.get('Id'));
        tempHistory.fieldName = String.valueOf(historyLine.get('Name'));
        tempHistory.oldvalue = String.valueOf(historyLine.get('Old_Value__c'));        
        tempHistory.newvalue = String.valueOf(historyLine.get('New_Value__c'));                
        tempHistory.lastmodified = String.valueOf(historyLine.get('LastModifiedDate'));                        
    }*/
    //prevDate = String.valueOf(historyLine.get('CreatedDate'));

    // Get the field label
    //String fieldLabel = CARPOL_UNI_LineItemHistoryController.returnFieldLabel(String.valueOf(historyLine.get('Name')));

    // Set the Action value
    /*if (String.valueOf(historyLine.get('Name')) == 'created') { // on Creation
        tempHistory.action = 'Created.';
    }
    else if (historyLine.get('Old_Value__c') != null && historyLine.get('New_Value__c') == null){ // when deleting a value from a field
    // Format the Date and if there's an error, catch it and re
        try {
            tempHistory.action = 'Deleted ' + Date.valueOf(historyLine.get('Old_Value__c')).format() + ' in <b>' + fieldLabel + '</b>.';
        } catch (Exception e){
            tempHistory.action = 'Deleted ' + String.valueOf(historyLine.get('Old_Value__c')) + ' in <b>' + fieldLabel + '</b>.';
        }
    }
    else{ // all other scenarios
        String fromText = '';
        if (historyLine.get('Old_Value__c') != null) {
            try {
                fromText = ' from ' + Date.valueOf(historyLine.get('Old_Value__c')).format();
            } catch (Exception e) {
                fromText = ' from ' + String.valueOf(historyLine.get('Old_Value__c'));
            }
        }

        String toText = '';
        if (historyLine.get('Old_Value__c') != null) {
            try {
                toText = Date.valueOf(historyLine.get('New_Value__c')).format();
            } catch (Exception e) {
                toText = String.valueOf(historyLine.get('New_Value__c'));
            }
        }
        if (toText != ''){
            tempHistory.action = 'Changed <b>' + fieldLabel + '</b>' + fromText + ' to <b>' + toText + '</b>.';
        }
        else {
            tempHistory.action = 'Changed <b>' + fieldLabel;
        }
    }
*/
    // Add to the list
    objectHistory.add(tempHistory);
//}
}

List<Id> userIdList = new List<Id>();
for (objectHistoryLine myHistory : objectHistory){
userIdList.add(myHistory.userId);
}
Map<Id, User> userIdMap = new Map<ID, User>([SELECT Name FROM User WHERE Id IN : userIdList]);

for (objectHistoryLine myHistory : objectHistory){
if (userIdMap.containsKey(myHistory.userId) & (myHistory.who != '') ){
myHistory.who = userIdMap.get(myHistory.who).Name;
}
}

return objectHistory;
}

// Function to return Field Label of a object field given a Field API name
public Static String returnFieldLabel(String fieldName){

if (CARPOL_UNI_LineItemHistoryController.myObjectFieldMap.containsKey(fieldName)){
return CARPOL_UNI_LineItemHistoryController.myObjectFieldMap.get(fieldName).getDescribe().getLabel();
}
else {
for(Schema.PicklistEntry pickList : historyFieldPicklistValues){
if (pickList.getValue() == fieldName){
if (pickList.getLabel() != null){
return pickList.getLabel();
}
else {
return pickList.getValue();
}
}
}
}
return '';
}

// Inner Class to store the detail of the object history lines
public class objectHistoryLine {

public String theDate {get; set;}
public String who {get; set;}
public Id userId {get; set;}
public Id fieldId{get; set;}
public String fieldname {get; set;}
public String action {get; set;}
public String oldvalue {get;set;}
public String newvalue {get;set;}
public String lastmodified {get;set;}
}
}