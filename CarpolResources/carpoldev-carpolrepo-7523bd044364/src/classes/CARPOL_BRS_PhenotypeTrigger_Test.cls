/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CARPOL_BRS_PhenotypeTrigger_Test {

	@isTest
    public static void test_CARPOL_BRS_PhenotypeTrigger() {
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);            
      Authorizations__c objauth = testData.newAuth(objapp.Id);      
      ac.Authorization__c = objauth.Id;
      update ac;
      
      Construct__c con = new Construct__c();
      con.Line_Item__c = ac.Id;
      con.Construct_s__c = 'Description text goes here..';
      con.Mode_of_Transformation__c = 'Direct Injection';
      insert con;
      
      Phenotype__c pheno = new Phenotype__c();
      pheno.Construct__c = con.Id;
      pheno.Construct_Name__c = 'Test name for pheno type';
      pheno.Phenotypic_Category__c =  'OO-Other';
      pheno.Phenotypic_Description__c = 'Description text goes here..';      
      insert pheno;      
      
      Test.startTest();
      pheno.Construct_Name__c = 'New Test name for pheno type';
      update pheno;
      System.assert(pheno != null);
      Test.stopTest();
        
    }
}