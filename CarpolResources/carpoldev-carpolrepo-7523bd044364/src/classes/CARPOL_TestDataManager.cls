@isTest
public class CARPOL_TestDataManager { 
/**
* CARPOL_TestDataManager.class
*
* @author  Kishore Kumar
* @date   07/5/2015
* @desc   Creates test data for the CARPOL code-base.
*/    
  
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String SRJRecordTypeId = Schema.SObjectType.Signature_Regulation_Junction__c.getRecordTypeInfosByName().get('Singature_Regulation').getRecordTypeId();
       String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
    // Need to add a method for applicant attachment record & AC Attachment parent id = applicant attachment record
    // doc types = Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement
    
    
    public CARPOL_TestDataManager(){
        
    }
    
    public account newAccount(string AccRecordTypeId ){
        Account objacct = new Account();
        objacct.Name = 'Global Account'+randomString5();
        //objacct.RecordTypeId = '012r000000007C4'; 
        objacct.RecordTypeId = AccRecordTypeId;    
        insert objacct; 
        System.assert(objacct != null);
        return objacct;
    }

    public Contact newContact(){
        Contact objCont = new Contact();
        objCont.FirstName = 'Global Contact'+randomString5();
        objcont.LastName = 'LastName'+randomString5();
        objcont.Email = randomString5()+'test@email.com';        
        //objacct.RecordTypeId = '012r000000007C4'; 
       // objacct.RecordTypeId = AccRecordTypeId; 
       objcont.AccountId = newAccount(AccountRecordTypeId).id;
       objcont.MailingStreet = 'Mailing Street'+randomString5();
       objcont.MailingCity = 'Mailing City'+randomString5();
       objcont.MailingState = 'Ohio';
       objcont.MailingCountry = 'United States';
       objcont.MailingPostalCode = '32092';    
        insert objcont; 
        System.assert(objcont != null);        
        return objcont;
    } 
    
    public Level_1_Region__c newState(String Statecode, String StateName, Id CountryId) {
        Level_1_Region__c State = new Level_1_Region__c();
        State.Name = StateName;
        State.Country__c = CountryId;
        State.Level_1_Region_Code__c = StateCode;
        State.Level_1_Name__c = 'State';
        State.Level_1_Region_Status__c = 'Active';
        insert State;
        System.assert(State != null);        
        return State;
    }
    
    public Country__c newCountry(String CountryCode, String CountryName, Id TradeAgreementId) {
        Country__c Country = new Country__c();
        Country.Country_Code__c = CountryCode;
        Country.Name = CountryName;
        Country.Trade_Agreement__c = TradeAgreementId;
        insert Country;
        System.assert(Country != null);        
        return Country;
    }
    
    public Trade_Agreement__c newTradeAgreement(String TradeAgreementCode, String TradeAgreementName) {
        Trade_Agreement__c TradeAgreement = new Trade_Agreement__c();
        TradeAgreement.Trade_Agreement_Code__c = TradeAgreementCode;
        TradeAgreement.Trade_Agreement_Status__c = 'Active';
        TradeAgreement.Name = TradeAgreementName;
        insert TradeAgreement;
        System.assert(TradeAgreement != null);
        return TradeAgreement;
    }
    
    Public breed__c newbreed(){
      Breed__c brd = new Breed__c();
      brd.Name='New Breed'+randomString5();
      insert brd; 
      System.assert(brd != null);      
      return brd;  
    }
    
    Public Applicant_Attachments__c newAttach(id ACLinelid){
      Applicant_Attachments__c appAttach = new Applicant_Attachments__c();
      appAttach.Document_Types__c='Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement';
      appAttach.Animal_Care_AC__c=ACLinelid;
      appAttach.RecordTypeId= AppAttRecordTypeId;
      insert appAttach;
      System.assert(appAttach != null);       
      return appAttach;  
    }
    
    public Applicant_Contact__c newappcontact() {
     Applicant_Contact__c apcont = new Applicant_Contact__c();
      apcont.First_Name__c = 'apcont'+randomString5();
      apcont.Email_Address__c = 'apcont@test.com';
      apcont.Name = 'Associated Contacts'+randomString5();
      //apcont.RecordTypeId = '012r000000007Iq';
      apcont.RecordTypeId = ImpapcontRecordTypeId;
      //apcont.USDA_License_Expiration_Date__c = date.today()+30;
      //apcont.USDA_Registration_Expiration_Date__c = date.today()+40;
      insert apcont;
      System.assert(apcont != null);      
      return apcont;        
    }
    
    Public Facility__c newfacility(){
      Facility__c fac = new Facility__c();
      //fac.RecordTypeId = '012r0000000065q';
      fac.RecordTypeId = PortsFacRecordTypeId;
      fac.Name = 'entryport';
      fac.Type__c = 'Foreign Port';  
      insert fac; 
      System.assert(fac != null);             
      return fac;  
    }  
    
    public Application__c newapplication(){
      Application__c objapp = new Application__c();
      objapp.Applicant_Name__c = newContact().Id;
      //objapp.Recordtypeid = '012r0000000068G';
      objapp.Recordtypeid = ACAppRecordTypeId;
      objapp.Application_Status__c = 'Open';
      insert objapp;
      System.assert(objapp != null);      
      return objapp;        
    }
    
    public Application__c newapp(id AppRecordTypeId){
      Application__c objapp = new Application__c();
      objapp.Applicant_Name__c = newContact().Id;
      objapp.Recordtypeid = AppRecordTypeId;
      objapp.Application_Status__c = 'Open';
      objapp.Locked__c ='No';
      insert objapp;
      System.assert(objapp != null);      
      return objapp;        
    }    

    public AC__c newAC(string poi){
      Facility__c  fac = newfacility();
      fac.Type__c = 'Domestic Port';
      update fac;  
      AC__c ac = new AC__c();
      Applicant_Contact__c apcont2 = newappcontact();
      ac.Application_Number__c = newapplication().Id;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.Id;
      ac.Port_of_Embarkation__c = newfacility().id;
      ac.Date_of_Birth__c = date.today()-400;
      ac.Breed__c = newbreed().id;
      ac.Importer_Last_Name__c = newappcontact().id;
      ac.Exporter_Last_Name__c = newappcontact().id;
      ac.Status__c = 'Test';
      ac.Purpose_of_the_Importation__c = poi;//'Personal Use';
      ac.DeliveryRecipient_Last_Name__c = newappcontact().id;
      insert ac;
      System.assert(ac != null);      
      return ac;  
    }
    
        public AC__c newLine(string poi, id applid){
      Facility__c  fac = newfacility();
      fac.Type__c = 'Domestic Port';
      update fac;  
      AC__c ac = new AC__c();
      Applicant_Contact__c apcont2 = newappcontact();
      ac.Application_Number__c = applid;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.Id;
      ac.Port_of_Embarkation__c = newfacility().id;
      ac.Date_of_Birth__c = date.today()-210;
      ac.Breed__c = newbreed().id;
      ac.Importer_Last_Name__c = newappcontact().id;
      ac.Exporter_Last_Name__c = newappcontact().id;
      ac.Status__c = 'Test';
      ac.Purpose_of_the_Importation__c = poi;//'Personal Use';
      ac.DeliveryRecipient_Last_Name__c = newappcontact().id;
      insert ac;
      System.assert(ac != null);      
      return ac;  
    }
    
    public VS__c newVS(Id ImportedFullId, Id ApplicationId) {
        VS__c VS = new VS__C();
        VS.Application_Number__c = ApplicationId;
        VS.Importer_Full_Name__c = ImportedFullId;
        VS.Hand_Carry__c = 'No';
        VS.Quantity__c = '5';
        VS.Purpose_of_Importation__c = 'Commercial/Retail sale directly to the consumer';
        VS.Intended_Use__c = 'Humar Consumption';
        VS.Product_Category__c = 'birds nest';
        VS.Method_of_Final_Disposition__c = 'Autoclaving';
        insert VS;
        System.assert(VS != null);        
        return VS;
    }
     
    public Regulation__c newRegulation(string type,string subtype){
      Regulation__c objreg = new Regulation__c();
      objreg.Status__c = 'Active';
      objreg.Type__c =  type; //'Import Requirements';
      objreg.Custom_Name__c = 'testcustname'+randomString5();
      objreg.Short_Name__c = 't'+randomString5();
      objreg.Title__c = 'title'+randomString5();
      objreg.Regulation_Description__c = 'descp1'+randomString5();
      objreg.Sub_Type__c = subtype;//'Import Permit Requirements';
      objreg.RecordTypeID = ACRegRecordTypeId;
      insert objreg; 
      System.assert(objreg != null);      
      return objreg;  
    }
    
        public Authorizations__c newAuth(id appid, id authRecordTypeId){
          Authorizations__c objauth = new Authorizations__c();
          objauth.Application__c = appid;//objapp.id;
          objauth.RecordTypeID = authRecordTypeId;
            objauth.Status__c = 'Submitted';
            objauth.Date_Issued__c = date.today();
            objauth.Expiry_Date__c = date.today()+60;
            objauth.Applicant_Alternate_Email__c = 'test@test.com';
            objauth.UNI_Zip__c = '32092';
            objauth.UNI_Country__c = 'United States';
            objauth.UNI_County_Province__c = 'Duval';
            objauth.BRS_Proposed_Start_Date__c = date.today()+30;
            objauth.BRS_Proposed_End_Date__c = date.today()+40;
            objauth.CBI__c = 'CBI Text';
            objauth.Application_CBI__c = 'No';
            objauth.Applicant_Alternate_Email__c = 'email2@test.com';
            objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
    
            objauth.AC_Applicant_Email__c = 'email2@test.com';
            objauth.AC_Applicant_Fax__c = '(904) 123-2345';
            objauth.AC_Applicant_Phone__c = '(904) 123-2345';
            objauth.AC_Organization__c = 'ABC Corp';
            objauth.Means_of_Movement__c = 'Hand Carry';
            objauth.Biological_Material_present_in_Article__c = 'No';
            objauth.If_Yes_Please_Describe__c = 'Text';
            //objauth.Country_and_Locality_Information__c = a.Country_Locality_Information__c;
            //objauth.Additional_Information__c = 'Additional Info';
            objauth.Applicant_Instructions__c = 'Make corrections';
            //objauth.Application_Status__c = 'Submitted';
           // objauth.BRS_Hand_Carry_For_Importation_Only__c = a.Hand_Carry__c;
            objauth.BRS_Number_of_Labels__c = 10;
           // objauth.BRS_Introduction_Type__c = 'Research';
            objauth.BRS_Purpose_of_Permit__c = 'Importation';
          
          insert objauth;
          System.assert(objauth != null);          
          return objauth;  
    }
    
    public Authorizations__c newAuthorization(id appid){
      Authorizations__c objauth = new Authorizations__c();
      objauth.Application__c = appid;//objapp.id;
      objauth.RecordTypeID = ACauthRecordTypeId;
       
      
      insert objauth;
      System.assert(objauth != null);      
      return objauth;  
    }
    
    /*Kishore
    
     public Construct__c newConstruct(id appid){
          Construct__c objcons = new Construct__c();
          objcons.Application__c = appid;//objapp.id;
          objcons.Status__c = 'Submitted';
          objcons.Construct_s__c = 'xgrdbio4jntnTest';
          objcons.Identifying_Line_s__c = 'Line text goes here';     
          insert objcons;
          return objcons;  
    }  Kishore*/
    
    
    public Attachment newAttachment(id ACId){ 
      Attachment attach = new Attachment();
      attach.Body = blob.valueof('Test doc');
      attach.Name = 'Test doc'+randomString5();
      attach.ParentId = ACId;
      insert attach; 
      System.assert(attach != null);       
      return attach;  
    }
    
    public Authorization_Junction__c newAuthorizationJunction(id authId,id RegId){ 
      Authorization_Junction__c objauthjun = new Authorization_Junction__c();
      objauthjun.Authorization__c = authId;
      objauthjun.Regulation__c = RegId;//objreg.id;
      insert objauthjun; 
      System.assert(objauthjun != null);      
      return objauthjun;   
    }
    
    public Communication_Manager__c newCommunicationmanager(string type){
      Communication_Manager__c communmang = new Communication_Manager__c();        
      communmang.Content__c = type;
      communmang.Description__c = 'test description'+randomString5();
      communmang.Type__c = type;
      insert communmang;
      System.assert(communmang != null);              
      return communmang;  
    }
    
      public String randomString5() {
       return randomString().substring(0, 5);
     }

     public String randomString() {
      //return random string of 32 chars
      return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
    }
    
    public Domain__c newProgram(){
      Domain__c objPro = new Domain__c();
      objPro.Name = 'Test program AC';
  
      insert objPro;   
      System.assert(objPro != null);           
      return objPro;  
    }
    
    public Program_Prefix__c newPrefix(){
      Program_Prefix__c objPrefix = new Program_Prefix__c();
      objPrefix.Program__c = newProgram().Id;
      objPrefix.Name = '555';
  
      insert objPrefix;     
      System.assert(objPrefix != null);         
      return objPrefix;  
    }
    
    public Signature__c newThumbprint(){
      Signature__c objTP = new Signature__c();
      objTP.Name = 'Test AC TP Jialin';
      objTP.Recordtypeid = ACTPRecordTypeId;
      objTP.Program_Prefix__c = newPrefix().Id;
      
  
      insert objTP;  
      System.assert(objTP != null);            
      return objTP;  
    }
    
    public Signature_Regulation_Junction__c newSRJ(id tpId,id RegId){ 
      Signature_Regulation_Junction__c objSRJ = new Signature_Regulation_Junction__c();
      objSRJ.Recordtypeid = SRJRecordTypeId;
      objSRJ.Signature__c = tpId;
      objSRJ.Regulation__c = RegId;//objreg.id;
      insert objSRJ;
      System.assert(objSRJ != null);       
      return objSRJ;   
    }
    

}