@isTest(seealldata=false)
private class CARPOL_UNI_ApplicantAttachment_Test {
      @IsTest static void CARPOL_UNI_ApplicantAttachment_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          //Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();
          //breed__c objbrd = testData.newbreed(); 
          //Applicant_Contact__c apcont = testData.newappcontact(); 
          //Applicant_Contact__c apcont2 = testData.newappcontact();
          //Facility__c fac = testData.newfacility('Domestic Port');  
          //Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          objac.status__c = 'Saved';
          //objac.Intended_Use__r.Name = 'Research';
          update objac;
          String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
          
          //Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Applicant_Attachments__c objaa = new Applicant_Attachments__c();
          objaa.Document_Types__c='Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement';
          objaa.RecordTypeId= AppAttRecordTypeId;
          objaa.Total_Files__c = 1;
          objaa.Animal_Care_AC__c = objac.id;          
          insert objaa; 

          objaa.Total_Files__c = 1;
          objaa.Animal_Care_AC__c = objac.id;
          update objaa;

          Map<Id,Applicant_Attachments__c> oldmap = new Map<Id,Applicant_Attachments__c>();
          Map<Id,Applicant_Attachments__c> newmap = new Map<Id,Applicant_Attachments__c>(); 
          oldmap.put(objaa.id,objaa);
          newmap.put(objaa.id,objaa);         
          List<RecordType> rtypes = [SELECT ID FROM RecordType WHERE Name = 'Biotechnology Regulatory Services - Notification'];

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_AddAppAttach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objaa);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              if(!rtypes.isempty()){
                  ApexPages.currentPage().getParameters().put('RecordType',rtypes[0].id);
              }
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');     
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                                                                                             
              CARPOL_UNI_ApplicantAttachment extclass = new CARPOL_UNI_ApplicantAttachment(oldmap,newmap);
              extclass.ApplicantAttachments();  
              system.assert(extclass != null);                      
          Test.stopTest();   
      }
}