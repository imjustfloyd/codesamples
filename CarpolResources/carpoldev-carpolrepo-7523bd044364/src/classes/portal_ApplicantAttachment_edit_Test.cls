@isTest(seealldata=false)
private class portal_ApplicantAttachment_edit_Test {
      @IsTest static void portal_ApplicantAttachment_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);


          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.portal_applicantattachment_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);  
              ApexPages.currentPage().getParameters().put('genid',objaa.id);                                          
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','no');
              ApexPages.currentPage().getParameters().put('SOPid','Test');
              ApexPages.currentPage().getParameters().put('RecordType',objaa.RecordTypeId);
              ApexPages.currentPage().getParameters().put('id',objaa.id);                                                                                                                 
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objaa);
              portal_ApplicantAttachment_edit extclassno = new portal_ApplicantAttachment_edit(sc1);              
              extclassno.saveappattach();
              extclassno.cancelappattach();  
              extclassno.upload();
              extclassno.getattach();
              extclassno.ReturnToLineItem();
              extclassno.save();
              List<SelectOption> lstOp = extclassno.getRequiredDocument();
              String strURL = extclassno.getreturnURL();
              extclassno.uploadFile();
              
              
              pageRef = Page.portal_applicantattachment_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objaa);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);  
              ApexPages.currentPage().getParameters().put('genid',objaa.id);                                          
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','yes');
              ApexPages.currentPage().getParameters().put('SOPid','Test');
              ApexPages.currentPage().getParameters().put('RecordType',objaa.RecordTypeId);
              ApexPages.currentPage().getParameters().put('id',objaa.id);                                                                                                   
              portal_ApplicantAttachment_edit extclass = new portal_ApplicantAttachment_edit(sc);
              extclass.saveappattach();     
              extclass.cancel();          
              extclass.cancelappattach();
              extclass.upload();
              extclass.getattach();
              extclass.ReturnToLineItem();
              extclass.save();
              List<SelectOption> lstOp1 = extclass.getRequiredDocument();
              String strURL1 = extclass.getreturnURL();
              extclass.uploadFile();   
              System.assert(extclass != null);        
          Test.stopTest();   
      }
}