@isTest
global class AddressVerifyMockImpl implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
        //System.assertEquals('https://maps.googleapis.com/maps/api/geocode/xml?address=2120+West+End+Avenue&key=AIzaSyAAWygMsr763OpAOyrYqhKXlwB3_Yz1h4k', req.getEndpoint());

        HttpResponse res = new HttpResponse();
        String MessageBody = '{"address":[{"city":"TestCity", "state":"TestState"}]}';        
        /*String MessageBody = '<AddressValidateRequest USERID="' + '403SDE006350' + '"><Address ID="0">';    
        MessageBody += '<Address1>'+'Test Street'+'</Address1>';
        MessageBody += '<Address2>'+'Test Street 2'+'</Address2>';
        MessageBody += '<City>'+'Test City'+'</City>';
        MessageBody += '<State>'+'TestState'+'</State>';
        MessageBody += '<Zip5>'+'12345'+'</Zip5>';
        MessageBody += '<Zip4>'+'1234'+'</Zip4>';    
        MessageBody += '</Address></AddressValidateRequest>';        
        */
        res.setBody(MessageBody);
        res.setStatusCode(200);
        System.assert(res != null);
        return res;
    }
   }