/* 
// Author : Vinar Amrutia
// Date Created : 06/19/2015
// Purpose : Ability to View all available Pathways for a particular APHIS Program
*/

public with sharing class CARPOL_WizardQuestionnaire {

    public List<Signature__c> thumbprints { get; set;}
    public Boolean showPathDetails { get; set;}
    public String thumbprintID { get; set;}
    
    public CARPOL_WizardQuestionnaire() {
        System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        showPathDetails = false;
        getAllThumbprints();
    }

    public PageReference getPathDetails() {
        System.Debug('<<<<<<< getPathDetails Begins >>>>>>>');
        showPathDetails = true;
        return null;
    }

    public PageReference getAllThumbprints()
    {
        thumbprints = new List<Signature__c>([SELECT ID, Name FROM Signature__c WHERE RecordType.Name = 'Animal Care (AC)' LIMIT 10]);
        System.Debug('<<<<<<< Total Signature(s) : ' + thumbprints.size() + ' >>>>>>>');
        return null;
    }

}