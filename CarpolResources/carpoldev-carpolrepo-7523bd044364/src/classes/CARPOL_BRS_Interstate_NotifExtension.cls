/* Project      : Biotechnolory Regulatory Service
 * Description  : This class supports the CARPOL_BRS_Interstate_Movement_and_Release_Notif
 *                VF Page
 * Authors: Kishore Kumar
 * ======================================================================
 *          Date        Author    Purpose
 * Changes: 07/06/2015  Kishore   Initial Version
 * ======================================================================
 */
public with sharing class CARPOL_BRS_Interstate_NotifExtension { 
    public string strregart {get;set;}
    public string strdest {get;set;}
    public CARPOL_BRS_Interstate_NotifExtension(ApexPages.StandardController controller) {
        
        string authid = apexpages.currentpage().getparameters().get('id');
        //lstregart.add(te);
        strregart = '';
        strdest = '';
        for(Link_Regulated_Articles__c lnkart:[select id,Name,Regulated_Article__r.Name from Link_Regulated_Articles__c where Authorization__c=:authid order by Regulated_Article__r.Name])
        {
            strregart += lnkart.Regulated_Article__r.Name+', ';
        }
        strregart = strregart.removeEnd(', ');
        // Record types for Locations
            Map<String, Schema.SObjectType> locObjectMap = Schema.getGlobalDescribe() ;
            Schema.SObjectType locs = locObjectMap.get('Location__c') ; // getting Sobject Type
            Schema.DescribeSObjectResult locSchema = locs.getDescribe() ;
            Map<String,Schema.RecordTypeInfo> locrectype = locSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
            list<string> desrectypeid = new list<string>();
            desrectypeid.add(locrectype.get('Destination Location').getRecordTypeId());
            desrectypeid.add(locrectype.get('Origin and Destination Location').getRecordTypeId());
            for(Location__c l:[select id,State__r.Name from Location__c where Authorization__c=:authid and RecordTypeid in:desrectypeid order by State__r.Name]){
                strdest += l.State__r.Name+', ';
            }
            strdest = strdest.removeEnd(', ');       
    }
}