Public class CARPOL_UNI_LevelOneRegionOptions{
public static map<string,string> allAnswersMap = new map<string,string>();
public static List<SelectOption> Level1Regions(List<Level_1_Region__c> level1RegRecs , string ctry){
             //CARPOL_WelcomePageController c = new  CARPOL_WelcomePageController();
                if(ctry!= null && ctry!= ''){
            List<SelectOption> options = new List<SelectOption>();
            for(Level_1_Region__c recLR: level1RegRecs){
                options.add(new SelectOption(recLR.Id,recLR.Name));
               allAnswersMap.put(recLR.Id,recLR.Name);
            } 
            options.add(0,new SelectOption('','--Select--'));
            return options;
        }
        else
        return null;
       }
        
    }