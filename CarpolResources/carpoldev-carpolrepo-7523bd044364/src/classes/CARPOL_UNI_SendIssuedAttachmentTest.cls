@isTest(seealldata=true)
private class CARPOL_UNI_SendIssuedAttachmentTest {
      @IsTest static void CARPOL_UNI_SendIssuedAttachmentTest() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);   
          AC__c li1 = TestData.newlineitem('Personal Use', objapp);             
                 
          Authorizations__c objauth = testData.newauth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Documents_Sent_to_Applicant__c = false;
          objauth.Status__c = 'Issued';
          objauth.Authorization_Type__c = 'Letter of No Jurisdiction';
          objauth.Date_Issued__c = Date.today();          
          update objauth;
          
          Authorizations__c objauth1 = testData.newauth(objapp.id);
          li1.Authorization__c = objauth1.id;
          update li1;
          objauth1.Documents_Sent_to_Applicant__c = false;
          objauth1.Status__c = 'Issued';
          objauth1.Authorization_Type__c = 'Letter of No Permit Required';
          objauth1.Date_Issued__c = Date.today();
          update objauth1;

          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Link_Authorization_Regulation__c lar = new Link_Authorization_Regulation__c();
          lar.Authorization__c = objauth.id;
          lar.Regulation__c = objreg1.id;
          insert lar;
          
          Application_Condition__c ac = new Application_Condition__c();
        //ac.Status__c = 'Waiting on Customer';
        //ac.Associate_Application__c = objapp.id;
        //ac.Line_Item__c = objLineItem.id;
          ac.Condition_Number__c = 1;
          ac.Agree__c = 'Agree';
          ac.Link_Authorization_Regulation__c = lar.id;
          insert ac;
          
          Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Complete');
          wft.Owner_Approver_Is_Signatory__c=True;
          update wft;
          
          Workflow_Task__c wft1 = testData.newworkflowtask('Test', objauth1, 'Complete');
          wft1.Owner_Approver_Is_Signatory__c=True;
          update wft1;          
          
          Attachment att = new Attachment();
          att.Name = 'AuthorizationLetter';
          att.Body = blob.valueof('Test doc');
          att.ParentId = objauth.id;
          insert att;
          
          Attachment att1 = new Attachment();
          att1.Name = 'AuthorizationLetter';
          att1.Body = blob.valueof('Test doc');
          att1.ParentId = objauth1.id;
          insert att1;
          
          Attachment att2 = new Attachment();
          att2.Name = 'Permit';
          att2.Body = blob.valueof('Test doc');
          att2.ParentId = objauth1.id;
          insert att2;
          
          
          Map<Id,Authorizations__c> newmap = new Map<Id,Authorizations__c>();
          newmap.put(objauth.id,objauth);
          newmap.put(objauth1.id,objauth1);
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 

              CARPOL_UNI_SendIssuedAttachment extclass = new CARPOL_UNI_SendIssuedAttachment(newmap);
              extclass.sendIssuedAttachment();
              extclass.sendIssuedAttachmentWithoutWFT();
              system.assert(extclass != null);             
          Test.stopTest();   
      }
}