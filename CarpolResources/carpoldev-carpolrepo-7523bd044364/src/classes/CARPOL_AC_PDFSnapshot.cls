public with sharing class CARPOL_AC_PDFSnapshot{
     @future(callout=true)
    public static void savePDF(ID appId, ID rt)
    { 
        /*Application__c app = [select Name, Application_Status__c from Application__c where ID = :appId];
        //Change the hard coded value to a query result
        System.debug('<<<<<<<<<<<<<<<<<< Application ID is ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<<< Record Type is ' + rt + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        if(app.Application_Status__c == 'Submitted' && rt == '012r0000000068GAAQ'){
            String attachmentName = app.Name;
          //  PageReference pdf = new PageReference('/apex/CARPOL_AC_ApplicationPDF?id='+appId);

            PageReference pdf = Page.CARPOL_AC_ApplicationPDF;
            pdf.getParameters().put('id', appId);
    
            Attachment att = new Attachment();
            Blob fileBody;
            try {
                 fileBody = pdf.getContent();

                System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
            } 
            catch (VisualforceException e) {
                fileBody = Blob.valueOf('Some Text');
                System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
            }
            //att.Body = fileBody;
            System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ PDF CONTENT=' + pdf.getContentAsPDF() + '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
            att.body = pdf.getContentAsPDF();
            att.parentId = appId;
            att.Name = attachmentName + '.pdf';
            att.IsPrivate = false;
            //att.ContentType = 'application/pdf';
            //att.ContentType = 'pdf';
            System.debug('<<<<<<<<<<<<<<<<<< Attachment is ' + att + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            insert att; */
        System.debug('<<<<<<<<<<<<<<<<< ID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        PageReference pdf = Page.CARPOL_AC_ApplicationPDF;
        pdf.getParameters().put('id', appId);
        Attachment att = new Attachment();
        Blob fileBody;
            try {
                 fileBody = pdf.getContentAsPDF();

                System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
            } 
            catch (VisualforceException e) {
                fileBody = Blob.valueOf('Some Text');
                System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
            }
        att.Name = 'AppName.pdf';
        att.ParentId = appId;
        att.body = fileBody;
        insert att;
        
       /*                 
        String[] EmailIds = new String[] {'zabseitov@phaseonecg.com', 'vinar.amrutia@creativesyscon.com'}; 
        String strFromEmailAddress = 'usdaAdmin@usda.com';
        String strHtmlBody = 'This is plain Body Content';
        String[] contactEmail = new String[]{'zabseitov@phaseonecg.com', 'vinar.amrutia@creativesyscon.com'};
        String strEmailSubject = 'Test Attachment';
        String strFilename = 'ApplicationAttachment';
        String strContentType = 'application/pdf';
        
        List<Attachment> retrivedAtts = [SELECT ID, Name, Body FROM Attachment WHERE Name = :attachmentName AND ParentID = : appID LIMIT 1];
        if(retrivedAtts.size() == 0)
        {
            //addError('No Approved Permit (Authorization Document) found. Please attach a Permit and then approve.');
        }
        else
        {
        
            Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();
            
            // Strings to hold the email addresses to which you are sending the email.
            //String[] toAddressesAppEmail = new String[] {currentACLineItem.Application_Number__r.Applicant_Name__r.Email}; 
            String[] toAddressesAppEmail = new String[] {'zabseitov@phaseonecg.com'}; 
            String[] ccAddressesAppEmail = new String[] {'vinar.amrutia@creativesyscon.com'};
        
            // Assign the addresses for the To and CC lists to the mail object.
            appEmail.setToAddresses(toAddressesAppEmail);
            appEmail.setCcAddresses(ccAddressesAppEmail);
            
            // Specify the address used when the recipients reply to the email. 
            appEmail.setReplyTo('support@efile.com');
            
            // Specify the name used as the display name.
            appEmail.setSenderDisplayName('eFile Support');
            
            // Specify the subject line for your email.
            appEmail.setSubject('USDA APHIS eFile - Application PDF : ' + appName);
            
            // Set to True if you want to BCC yourself on the email.
            appEmail.setBccSender(false);
            
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            appEmail.setUseSignature(false);
            
            //String appEmailBody = 'Dear ' + currentACLineItem.Application_Number__r.Applicant_Name__r.Name;
            String appEmailBody = 'Dear APplicantName';
            appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + appName + '. ';
            appEmailBody += 'Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online <a href=' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + appID + '>click here.</a><br/><br/>';
            appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
            appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/><br/><br/>APHIS Animal Care<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
        
            appEmail.setHtmlBody(appEmailBody);
            
            Blob body = retrivedAtts[0].Body;
            
            // Create the email attachment
            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
            emailAttachment.setFileName('Application.pdf');
            emailAttachment.setContentType('');
            emailAttachment.setBody(fileBody);
            appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
            
            
            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
       
        */
        /*
        String strFromEmailAddress = 'usdaAdmin@usda.com';
        String strHtmlBody = '<h1>This is HTML Body Content<h1>';
        String[] contactEmail = new String[]{'zabseitov@phaseonecg.com', 'vinar.amrutia@creativesyscon.com'};
        String strEmailSubject = 'Test Attachment';
        String strFilename = 'ApplicationAttachment.pdf';
        String strContentType = 'application/pdf';
        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        System.debug(strHtmlBody);
        mail.setToAddresses(contactEmail);
        mail.setSubject(strEmailSubject);
        mail.setHtmlBody(strHtmlBody);
        
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setBody(fileBody);
        //attachment.setContentType(strContentType);
        attachment.setFileName(strFilename);
        attachment.setinline(false);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment } );
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        }*/
        
    }
}