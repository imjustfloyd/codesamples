@isTest(seealldata=false)
private class portal_construct_detail_Test {
      @IsTest static void portal_construct_detail_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;


          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.portal_construct_detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcaj);
              ApexPages.currentPage().getParameters().put('id',objcaj.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);                                                                                                                                                      
              ApexPages.currentPage().getParameters().put('genid','Test');                                   
              ApexPages.currentPage().getParameters().put('genid','Test');                                                                                                                                                                    
              ApexPages.currentPage().getParameters().put('retPage','Test');                                                                                                                                                                                  
              portal_construct_detail extclass = new portal_construct_detail(sc);
              extclass.getphenotypes(); 
              extclass.getgenotypes();   
              extclass.getattach();   
              extclass.gethistory();
              extclass.EditConstruct();
              extclass.NewPhenotype();
              extclass.NewGenotype();
              extclass.upload();
              extclass.ret();
              System.assert(extclass != null);                      
          Test.stopTest();   
      }
}