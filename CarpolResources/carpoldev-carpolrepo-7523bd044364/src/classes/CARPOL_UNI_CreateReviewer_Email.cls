/* The purpose of this code is to create official review records for BRS and PPQ (prefix 203, 205 and 206).                             */
/* official review records are created, where needed permits are attached, emails are sent out and apex managed sharing is applied */
public class CARPOL_UNI_CreateReviewer_Email {

    public Authorizations__c Authorization;
    private ID AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
   // private String officialRole = ApexPages.currentPage().getParameters().get('rtype'); 
    public string officialRole {get;set;}
    public string wfname {get;set;}
    public string sproid  {get;set;}
    public string sphdid  {get;set;}
    Id BRSsproreviewerRecordTypeId;
    Id PPQsproreviewerRecordTypeId;
    Id sphdreviewerRecordTypeId;
    Reviewer__c objrev = new Reviewer__c();
    public list<Reviewer__c> listReviewRec {get;set;}
    public list<Link_Regulated_Articles__c> listRegArt {get; set;} 
    public list<Reviewer__c> lstReviewRec {get;set;}
    public list<Reviewer__c> lstsphdReviewRec {get;set;}
    public id wfid {get;set;}
    public boolean showAttachDraftBtn{get;set;}     
    public boolean showAttachBtn{get;set;}      
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    public String templateURL { get; set; }
    public String baseURL { get; set; }
    public Boolean brsview {get;set;}
    public string delrecid {get;set;}
    public Authorizations__c auth {get;set;}
   boolean iscreaterev =  false;
   

   public CARPOL_UNI_CreateReviewer_Email(ApexPages.StandardController controller) {
        this.AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
        if(AuthorizationID !=null){
          auth = [SELECT Name,Program__c,BRS_Create_State_Review_Records__c,Create_SPHD_Review_Records__c,Prefix__c,Date_Issued__c,
                         State_letter_Template__c,Authorization_State_Letter_Content__c FROM Authorizations__c WHERE Id =: AuthorizationID]; }

        //this.officialRole = ApexPages.currentPage().getParameters().get('rtype');
        if(ApexPages.currentPage().getParameters().get('wfid') != null){
            wfid = ApexPages.currentPage().getParameters().get('wfid');
            wfname = [select name from Workflow_Task__c where id =:wfid ].name;
         } 
         this.sproid = 'spro';
         this.sphdid = 'sphd';
         brsview = false;
          BRSsproreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('BRS State Review Record').getRecordTypeId();
          PPQsproreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('PPQ State Review Record').getRecordTypeId();
          sphdreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('State Plant Health Director Record').getRecordTypeId();  
          system.debug('sphdreviewerRecordTypeId###'+sphdreviewerRecordTypeId);
         if(auth.Program__c == 'BRS'){ 
             brsview = true;}
         system.debug('this.officialRole***'+this.officialRole);
         getstatepackage();
         getstatesphd();
   }
   
   public void SendEmailToReviewer(String progprefix, Authorizations__c auth, List<Reviewer__c> reviewer){

    if(reviewer.size()>0){
        attachment attlab = new attachment();
        attachment attlab3 = new attachment();
        Blob attbody = null;
        Blob attbody2 = null;
        Blob attbody3 = null;
        
        //brs only - add the attachment to the authorization, PPQ does this differently
        System.debug('authprefix: '+auth.Prefix__c);
        if(progprefix == 'BRS'){
             PageReference PDFPage = new PageReference('/apex/CARPOL_BRS_AuthorizationPDFCBI?ID='+AuthorizationID+'&Ack=True&CBI='+auth.Application_CBI__c);
            if(!test.isRunningTest()){
                attbody = PDFPage.getContent();
                attbody2 = PDFPage.getContent();
            }else{
                attbody = Blob.valueOf('Some Text');
                attbody2 = Blob.valueOf('Some Text');
            }

            attlab.Body = attbody;
            attlab.Name = 'CBI_Deleted_Application_Copy_'+ System.TODAY().format()+ '.pdf'; 
            attlab.ParentId = AuthorizationID;
            attlab.contentType = 'application/pdf';
            insert attlab;
        } else if(progprefix == 'PPQ'){          
//      } else if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206'){
            //get the existing state review copy from the authorization
            String searchPermit = '%' + 'Permit'+ '%';
            String searchCompliance = '%' + 'Compliance'+ '%';            
            List<Attachment> files = [Select Id, Body, Name, ContentType, ParentId From Attachment Where ParentId =: auth.Id AND name <> 'Draft Permit.pdf' and (name Like :searchPermit OR name Like :searchCompliance) order by createddate desc]; // 8-1-16 VV updated to skip Draft Permit that gets generated in case of PEQ
            if(files.size() > 0 ){ 
                attbody = files[0].body;
            }
        }
        //need to get the attachment for ppq differently to pass below
        List<Attachment> attach = new List<Attachment>();
        
        //attach the permit to the official review record
        for(Reviewer__c review : reviewer){
            if(attbody != null){
                attachment attlab2 = new attachment();
                attlab2.Body = attbody;
                attlab2.Name = 'State_Review_Copy_'+ System.TODAY().format()+ '.pdf'; //'Acknowledgement_Letter.pdf';
                attlab2.contentType = 'application/pdf';
                attlab2.ParentId = review.Id;
                attach.add(attlab2);
            }
        }
        if(attach.size() > 0){
            insert attach;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'CBI-Deleted version PDF successfully attached for each State Review record'));       
        }
    }

    //prepare email message
    string sBody='' ;
    string sSubject='' ;
    string templateName='';
    List<EmailTemplate> emailTemplate = new List<EmailTemplate>();
    //BRS template, different template for PPQ SPRO and SPHD
    if(progprefix == 'BRS'){
//    if(auth.Prefix__c != '203' && auth.Prefix__c != '205' && auth.Prefix__c != '206'){
        emailTemplate = [Select e.body, e.subject, e.Name, e.Id,e.DeveloperName From EmailTemplate e where e.DeveloperName = 'BRS_Notification_State_Review'];
    } else if (progprefix == 'PPQ' && officialRole == 'spro'){
//  } else if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206' && officialRole == 'spro'){
        emailTemplate = [Select e.body, e.subject, e.Name, e.Id,e.DeveloperName From EmailTemplate e where e.DeveloperName = 'PPQ_Notification_State_Review'];    
    } else if (progprefix == 'PPQ' && officialRole == 'sphd'){
//  } else if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206' && officialRole == 'sphd'){
        emailTemplate = [Select e.body, e.subject, e.Name, e.Id,e.DeveloperName From EmailTemplate e where e.DeveloperName = 'PPQ_Notification_State_Plant_Health_Director'];        
    }

        for(Reviewer__c rev1  : reviewer ) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(officialRole == 'spro'){
                mail.settargetObjectId(rev1.State_Regulatory_Official__c);
            } else if(officialRole == 'sphd') {
                mail.settargetObjectId(rev1.State_Plant_Health_Director__c);            
            }
            mail.setWhatId(rev1.id);
            mail.setTemplateId(emailTemplate[0].Id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Reviewer email sent successfully '));
    }

    // This method is used to create a new Applicant Attachment record with name State Package, when state records are created.
   /* public static void createApplicantAttachment(List<Reviewer__c> rev){
        //Authorizations__c auth1 = [SELECT Id,Name FROM Authorizations__c WHERE Id =: AuthorizationID];
        //list<Reviewer__c> rev =[SELECT Id,Authorization__c FROM Reviewer__c where   Authorization__c  =:AuthorizationID ];
        List<Reviewer__c> rev1 = rev;
        list<Recordtype> rec = [SELECT Name, Id FROM Recordtype WHERE DeveloperName = 'State_Package' AND SObjectType = 'Applicant_Attachments__c'];
        if(rec.size()>0){
            for(Reviewer__c revi : rev1){
                Applicant_Attachments__c aa = new Applicant_Attachments__c();
                aa.Attachment_Type__c = 'State Package';
                aa.Recordtypeid = rec[0].id;
                aa.Document_Types__c = 'Original Scanned Paper Application; State Letter;Draft Environmental Assessment';
                aa.Authorization__c = rev[0].Authorization__c;
                insert aa;
            }
        }
    }*/// End of createApplicantAttachment method.


    public  string createReviewerrecords()
    {
       // try
        //{   
            //we get this when the class is instantiated
            // AuthorizationID = ApexPages.currentPage().getParameters().get('ID');

            if(AuthorizationID != null)
            {   //instantiate lists
                list<Reviewer__c> lstrev2 = new list<Reviewer__c>();
                list<AC__c> lstlineitem = new list<AC__c>();
                list<Location__c> loctlist = new list<Location__c>();
                boolean chkCreated;
                //get the authorization
                Authorizations__c auth = [SELECT Id,Status__c,Name,BRS_Create_State_Review_Records__c,Create_SPHD_Review_Records__c, Prefix__c, Application_CBI__c, Recordtype.Name,State_letter_Template__c FROM Authorizations__c WHERE Id =: AuthorizationID];
                //TODO:replace all the prefix numbers with progPrefix.Program__r.Name
                Program_Prefix__c progPrefix = [SELECT Id,Program__r.Name From Program_Prefix__c where Name = :auth.Prefix__c Limit 1];
                //if rtype is spro
                system.debug('auth.BRS_Create_State_Review_Records__c*****'+auth.BRS_Create_State_Review_Records__c);
                 system.debug('officialRole*****'+officialRole);
                  
                if(officialRole == 'spro'){
                    chkCreated = auth.BRS_Create_State_Review_Records__c;
                } else if (officialRole == 'sphd'){
                    chkCreated = auth.Create_SPHD_Review_Records__c;
                }
                system.debug('chkCreated*****'+chkCreated);
                if(chkCreated == false)
                {
                    //gets contact schema to get 'State SPRO' record type
                    Map<String, Schema.SObjectType> sObjectMap = Schema.getGlobalDescribe() ;
                    Schema.SObjectType s = sObjectMap.get('Contact') ; // getting Sobject Type
                    Schema.DescribeSObjectResult resSchema = s.getDescribe();
                    Map<String,Schema.RecordTypeInfo> recordTypeInfo = resSchema.getRecordTypeInfosByName(); //getting all Recordtype for the Sobject
                    Id contrtId;
                    if(officialRole == 'spro'){
                        contrtId = recordTypeInfo.get('State SPRO').getRecordTypeId(); // particular RecordId by Name
                    } else if(officialRole == 'sphd'){
                        contrtId = recordTypeInfo.get('State SPHD').getRecordTypeId(); // particular RecordId by Name
                    }
                    
                    //get line item, for PPQ will need State/Territory of Destination
                    integer countlineitem = [select count() from AC__c where Authorization__c=:AuthorizationID];
                    if(countlineitem>0){
                        lstlineitem = [select id,Name,Authorization__c,Introduction_Type__c,Purpose_of_the_Importation__c,Hand_Carry__c,Number_of_Labels__c,State_Territory_of_Destination__c, Delivery_Recipient_State_ProvinceLU__c,Movement_Type__c from AC__c where Authorization__c=:AuthorizationID] ;
                    }
                    list<string> stateids = new list<string>();
                    list<Reviewer__c> lstrev = new list<Reviewer__c>();
                    if(progPrefix.Program__r.Name == 'PPQ' && lstlineitem[0].movement_type__c == 'Import'){ //VV 8-24-16 added Movement type filter
                    //if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206'){
                        //set the state ids to retrieve from the State/Territory of Destination field on the line item
                        if(lstlineitem.size() > 0){
                            if(lstlineitem[0].State_Territory_of_Destination__c != null){
                                stateids.add(lstlineitem[0].State_Territory_of_Destination__c);
                            } else if(lstlineitem[0].Delivery_Recipient_State_ProvinceLU__c != null){ 
                                stateids.add(lstlineitem[0].Delivery_Recipient_State_ProvinceLU__c);                            
                            }
                        }
                    }
                            system.debug('---stateids before = '+stateids);
                    //VV Added 8-23-16 for transit movement type to get all the states of ports of entry 
                    if(lstlineitem[0].movement_type__c == 'Transit'){ 
                       set  <id> TLId = new set<id>();
                       for (transit_locale__c tl: [select id, Port_of_Entry__c,Port_of_Exit__c from transit_locale__c where Line_Item__c =:lstlineitem[0].Id]){
                              TLId.add(tl.Port_of_Entry__c);
                              TLId.add(tl.Port_of_Exit__c);
                           }
                        system.debug('---TLId = '+TLId);                          
                       for (facility__c f: [select id,State_LV1__c from facility__c where Id in :TLId]){
                                 if(f.State_LV1__c!=null){
                                     stateids.add(f.State_LV1__c);
                                 } 
                              } 
                            system.debug('---stateids after = '+stateids);     
                        }                     
                    //if BRS, we need to get the locations to get state ids
                    if(progPrefix.Program__r.Name == 'BRS'){
                    //if(auth.Prefix__c != '203' && auth.Prefix__c != '205' && auth.Prefix__c != '206'){
                       integer loctcount = [select count() from Location__c  where Line_Item__c =:lstlineitem[0].Id];
                       if(loctcount>0)
                       {
                          Id recTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
                          list<Location__c> loctlst = [select id,Name,State__c,State__r.Name,Authorization__c from Location__c where Line_Item__c =:lstlineitem[0].Id AND RecordTypeID != :recTypeId];
                          if(loctlst.size()>0){
                             for(Location__c l:loctlst){
                                 if(l.State__c!=null){
                                     stateids.add(l.State__c);
                                 }
                             }
                          }
                       }
                    }
                    system.debug('stateids***'+stateids);
                    //we got our state, create the review records
                    if(stateids.size()>0){
                       //get the state SPRO or SPHD contacts depending on record type
                       integer contactcount = [select count() from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId ];
                       system.debug('----count contact--'+contactcount);
                        system.debug('----contrtId --'+contrtId);
                        system.debug('----stateids --'+stateids);
                       if(contactcount>0){
                          list<contact> contactlst = [select id,Name,Contact_checkbox__c,State__c,Email from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId and State__c in:stateids ];
                         system.debug('----contactlst --'+contactlst);
                          for(Contact c:contactlst)
                          {   
                              objrev = new Reviewer__c();
                              //get the correct record type
                              if(progPrefix.Program__r.Name == 'BRS'){
                              //if(auth.Prefix__c != '203' && auth.Prefix__c != '205' && auth.Prefix__c != '206'){
                                  objrev.RecordTypeId = BRSsproreviewerRecordTypeId;
                               } else if(progPrefix.Program__r.Name == 'PPQ' && officialRole == 'spro'){
//                            } else if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206' && officialRole == 'spro') {
                                 // sproreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('PPQ State Review Record').getRecordTypeId();                              
                                  objrev.RecordTypeId = PPQsproreviewerRecordTypeId;
                               } else if(progPrefix.Program__r.Name == 'PPQ' && officialRole == 'sphd'){                             
//                            } else if(auth.Prefix__c == '203' || auth.Prefix__c == '205' || auth.Prefix__c == '206' && officialRole == 'sphd') {
                                //  sphdreviewerRecordTypeId = Schema.SObjectType.Reviewer__c.getRecordTypeInfosByName().get('State Plant Health Director Record').getRecordTypeId();                                                            
                                  objrev.RecordTypeId = sphdreviewerRecordTypeId;
                               }
                              
                             // objrev.RecordTypeId = reviewerRecordTypeId;
                              objrev.Status__c= 'Open';
                              objrev.Authorization__c = AuthorizationID;
                              //This field changes for sphd
                              if(officialRole == 'spro'){
                                  objrev.State_Regulatory_Official__c = c.id;
                              } else if(officialRole == 'sphd'){
                                  objrev.State_Plant_Health_Director__c = c.id;                              
                              }
                              objrev.BRS_State_Reviewer_Email__c = c.Email;
                              system.debug('objrev@@@@'+objrev);
                              
                              lstrev.add(objrev);
                              system.debug('lstrev1@@@@'+lstrev);
                          }
                          system.debug('lstrev2@@@@'+lstrev);

                          if(lstrev.size()>0){
                              insert lstrev;
                              
                              //add page message
                              ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Successfully created Official State Review records'));
 
                              //add sharing for the correct state group
                              List<Reviewer__Share> revShare = new List<Reviewer__Share>();
 
                              for(Reviewer__c objrev : lstrev){
                                 Reviewer__Share reviewerShr = new Reviewer__Share();
                                 // Set the ID of record being shared.
                                 reviewerShr.ParentId = objrev.Id;
                                 
                                 // Set the access level.
                                 reviewerShr.AccessLevel = 'Edit';
                                                
                                 String statecode = '';
                                 //set row cause based on who is sharing State_Plant_Health_Director__c or State_Reviewer__c 
                                 if(officialRole == 'spro'){
                                     Contact c = [Select ID,Name,State__c,Mailing_State_Province_LR__c,State__r.Level_1_Region_Code__c From Contact where Id = :objrev.State_Regulatory_Official__c and Email != null];                                 
                                     reviewerShr.RowCause = Schema.Reviewer__Share.RowCause.State_Reviewer__c;
                                     statecode = c.State__r.Level_1_Region_Code__c;
                                     statecode = statecode + ' SPRO';
                                 } else if(officialRole == 'sphd'){
                                     Contact c = [Select ID,Name,State__c,Mailing_State_Province_LR__c,State__r.Level_1_Region_Code__c From Contact where Id = :objrev.State_Plant_Health_Director__c];                                                                  
                                     reviewerShr.RowCause = Schema.Reviewer__Share.RowCause.State_Plant_Health_Director__c;
                                     statecode = c.State__r.Level_1_Region_Code__c;
                                     statecode = statecode + ' SPHD';                                     
                                 }

                                 List<Group> shareGroup = [Select ID from Group where Name = :statecode Limit 1];
                                 if(shareGroup.size() > 0){
                                     reviewerShr.UserOrGroupId = shareGroup[0].Id; 
                                     revShare.add(reviewerShr);
                                 }
                                 
                                 //TODO: if sphd, grant them access to existing state reviewer records too?
                              }
                              if(revShare.size() > 0){
                                  insert revShare; 
                              }
                              SendEmailToReviewer(progPrefix.Program__r.Name,auth,lstrev);                                     
                              
                              //save the flag to the authorization
                              //this field changes if sphd
                              if(officialRole == 'spro'){
                                  auth.BRS_Create_State_Review_Records__c = true;
                                  auth.Status__c='State Review';
                              } else if(officialRole == 'sphd'){
                                  auth.Create_SPHD_Review_Records__c = true;                          
                                  auth.Status__c = 'State Plant Health Director Review';
                              }                              
                              update auth;                                     
                          }
                       }
                       else
                       {
                          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning, 'No Offical State Review Records created'));
                       }                            
                    } //end stateids size
                 //createApplicantAttachment();
                } 
                else{
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,'Official State Review Records are already Created.'));
                }
            }

        /*}
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
            ApexPages.addMessages(e);
        }*/
        return null;
    }
    


    public pageReference createstatepackage(){
         pageReference pg = Page.CARPOL_UNI_CreateOfficialReviewerRecords;
         pg.getParameters().put('rtype','spro');
         pg.getParameters().put('ID',AuthorizationID);
         pg.setRedirect(true);
        return pg;
      }    
    
        public pageReference Attachstateletter(){
         pageReference pg = Page.CARPOL_BRS_StateLetterAttachment;
          pg.getParameters().put('ID',AuthorizationID);
         pg.setRedirect(true);
        return pg;
       } 
    
        public void getstatepackage(){

            // load Official Review Records
                lstReviewRec = [SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,
                                State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,
                                Status__c,Reviewer_Unique_Id__c,State__c,State_Review_Duration__c,(SELECT ID, Name FROM Attachments where name like 'CBI%' LIMIT 1)  FROM Reviewer__c WHERE Authorization__c =:AuthorizationID and (RecordTypeId =: BRSsproreviewerRecordTypeId or RecordTypeId =: PPQsproreviewerRecordTypeId)]; 
        }  
     
        public void getstatesphd(){

            // load Official Review Records
             lstsphdReviewRec = [SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,State_Plant_Health_Director__c,
                                State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,
                                Status__c,Reviewer_Unique_Id__c,State__c,State_Review_Duration__c,(SELECT ID, Name FROM Attachments LIMIT 1)  FROM Reviewer__c WHERE Authorization__c =:AuthorizationID and RecordTypeId =: sphdreviewerRecordTypeId]; 
                                
            system.debug('lstsphdReviewRec####'+lstsphdReviewRec);                    
        }      

    
    public PageReference populateTemplate()
    {
          system.debug('auth*****'+auth);
        System.Debug('<<<<<<< Template : ' + auth.State_letter_Template__c + ' >>>>>>>');
        try
        {
            if(auth.State_letter_Template__c != null)
            {
                auth.Authorization_State_Letter_Content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:auth.State_letter_Template__c LIMIT 1].Content__c;
                if(auth.Date_Issued__c != null){
                auth.Authorization_State_Letter_Content__c = auth.Authorization_State_Letter_Content__c.replace('{!IssuedDate}', String.ValueOf(auth.Date_Issued__c.format()));  }
  
                update auth; 
                previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Template populated successfully.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a template to be populated.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid template to be populated.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference cancel()
    {
        System.Debug('<<<<<<< Cancel >>>>>>>');
        PageReference return2Auth = new PageReference('/' + AuthorizationID);
        return2Auth.setRedirect(true);
        return return2Auth;
    } 

    public PageReference attachLetter()
    {
        try{
            List<Reviewer__c> revlst1 = [SELECT ID,State_Letter_Attached__c,(SELECT ID, Name FROM Attachments) FROM Reviewer__c WHERE Authorization__c = :AuthorizationID];
            
             if(revlst1.size()>0){
                  for(Reviewer__c r : revlst1){
                      system.debug('r.attachments.size()***'+r.attachments.size());
                        if(r.attachments.size()<2){
                         String attachmentName;
                         PageReference pdfAuthorizationLetter = Page.CARPOL_AuthorizationLetter;
             
                        pdfAuthorizationLetter.getParameters().put('id', AuthorizationID);
                        pdfAuthorizationLetter.getParameters().put('tId', wfid);
                        pdfAuthorizationLetter.getParameters().put('type', 'true');
                        Blob pdfAuthorizationLetterBlob;
                        
                       if (Test.IsRunningTest())
                       {
                          pdfAuthorizationLetterBlob =Blob.valueOf('UNIT.TEST');
                       }
                       else
                       {
                           pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent();
                        
                        }
 
                        attachmentName = 'State_Letter_'+ System.TODAY().format()+ '.pdf';  
                        Attachment attachment = new Attachment(parentId = r.ID, name = attachmentName, body = pdfAuthorizationLetterBlob);
                        insert attachment;
                         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'State Letter successfully attached for each state review record.'));
             
                 }else{
                          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'State Letters are already attached.'));
                  }                  
                      
                }
              } else {// End of IF revlst1 statement       
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'State Review Records are not yet created, Please use Create State Review Records button and try to attach state letter.'));
             }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
     public PageReference attachDraftLetter()        
    {       
        showLinks=true;
        try{        
            System.Debug('<<<<<<< Authorization : ' + authorization + ' >>>>>>>');      
            String draftattachmentName;     
            PageReference pdfAuthorizationLetter = Page.CARPOL_AuthorizationLetter;     
            draftattachmentName = 'Draft Authorization Letter_' + System.TODAY().format();      
                    
                    
            pdfAuthorizationLetter.getParameters().put('id', authorizationID);     
            pdfAuthorizationLetter.getParameters().put('tId', wfId);        
            Blob pdfAuthorizationLetterBlob;        
                    
          //  pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent(); 
           if (Test.IsRunningTest())
           {
            pdfAuthorizationLetterBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfAuthorizationLetter + ' >>>>>>');
            pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent();
            
            }            
                    
            String tempAttachmentName = draftattachmentName.substring(0, draftattachmentName.length()) + '%';       
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :wfId AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];     
            if(prevAttachments.size() > 0)      
            {       
                if(prevAttachments.size() == 1){draftattachmentName = draftattachmentName + '_v2';}     
                else        
                {       
                    for(Integer i = 0; i < prevAttachments.size(); i++ )        
                    {       
                        if(i == prevAttachments.size() - 1)     
                        {       
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);       
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);      
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;       
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');      
                            draftattachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;        
                            System.Debug('<<<<<<< New Attachment Name : ' + draftattachmentName + ' >>>>>>');       
                        }       
                    }       
                }       
            }       
                    
            draftattachmentName = draftattachmentName + '.pdf';     
            Attachment attachment = new Attachment(parentId = wfId, name = draftattachmentName, body = pdfAuthorizationLetterBlob);     
            insert attachment;      
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Authorization Draft Letter attached successfully.'));
        }       
        catch(Exception e)      
        {       
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');      
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));     
        }       
        return null;        
    }
    
    public PageReference saveDraft()
    {
        try{
            if(auth.Date_Issued__c < System.Today())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Date Issued cannot be in the past.'));
            }
            else
            {
                update auth;
                //previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference previewTemplate()
    {
        try{
             templateURL = '/apex/CARPOL_AuthorizationLetter?id=' + AuthorizationID+ '&tId='+ wfid +'&Type='+true;
  
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }  
    
     public PageReference deleteRecord(){
 
                  string  strqurey = 'select id from Attachments where id=:delrecid';
                    list<sobject> lst = database.query(strqurey);
                   delete lst;
                    
                    PageReference dirpage= new PageReference('/apex/CARPOL_UNI_CreateOfficialReviewerRecords?id=' + authorizationID+'&wfid='+wfid);
                    dirpage.setRedirect(true);
                    return dirpage;
                }  
                
     public PageReference redirect(){
                  PageReference dirpage;
                   if(wfid!=null){
                        dirpage= new PageReference('/'+wfid);
                   }else {
                        dirpage= new PageReference('/'+authorizationID); 
                   }
                    dirpage.setRedirect(true);
                    return dirpage;
                }   
    
    
}