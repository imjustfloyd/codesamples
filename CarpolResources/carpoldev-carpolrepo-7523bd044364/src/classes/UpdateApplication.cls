public class UpdateApplication {
    
    public Application__c application = new Application__c();
    public ID applicationID;
    
    public UpdateApplication(ApexPages.StandardController controller)
    {
        applicationID = ApexPages.CurrentPage().getparameters().get('id');
        application = (Application__c)controller.getRecord();
    }
    
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + application.Id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }
    
    public PageReference appUpdate()
    {
        try
        {
            List<Transaction__c> lstTrans = [Select Id, Name, Pay_Gov_Token__c From Transaction__c where Application__c =: application.Id LIMIT 1];
            if(lstTrans.size() > 0)
            {   Transaction__c trans = lstTrans[0];
                CARPOL_ProcessPaymentPayGov temp = new CARPOL_ProcessPaymentPayGov();
                trans.Pay_gov_Tracking_ID__c = temp.completeOnlineCollection(trans.Pay_Gov_Token__c, trans.id);
                update trans;
            }
            application.Application_Status__c = 'Submitted';
            update application; //This generates the authorization.
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Application submitted successfully.'));
        }
        catch(exception e)
        {
            application.Application_Status__c = 'Submitted';
            update application; //This generates the authorization.
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Application submitted successfully.'));
        }
        
        return null;
    }

}