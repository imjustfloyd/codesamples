@isTest(seealldata=true)
private class CARPOL_AC_AddLettersTest {
/**
* CARPOL_AC_AddLettersTest.class
*
* @author  Kishore Kumar
* @date   07/5/2015
* @desc   Creates test data for the CARPOL_AC_AddLetters.
*/      
    static testMethod void testCARPOL_AC_AddLetters() {
    String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      system.debug('GetApplicantRecordType'+ImpapcontRecordTypeId );
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);
      Attachment attach = testData.newattachment(ac.Id);  
      Communication_Manager__c communmang = testData.newCommunicationManager('Letter of No Jurisdiction');

        PageReference pageRef = Page.CARPOL_AC_AddLetters;
        Test.setCurrentPage(pageRef);
        AC__c objac = new AC__c();
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
        CARPOL_AC_AddLetters acaddcls = new CARPOL_AC_AddLetters(sc);
        acaddcls.selectedTempalteType = 'Letter of No Jurisdiction';
        acaddcls.selectedTempalteID = communmang.id;
        acaddcls.attachmentName = 'attachmentName.name';
        acaddcls.getTemplates();
        acaddcls.populateTemplateContent();
        acaddcls.saveLetter();
        acaddcls.selectedTempalteID ='--None--';
        acaddcls.populateTemplateContent();
        System.assert(acaddcls != null);
    }
   }