public class CARPOL_UNI_ApplicationPDF {
    
    public ID appId {get; set;}
    public Application__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public String htmlText {get;set;}
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
   
    private List<TableCol> tblColumns;
    
    public CARPOL_UNI_ApplicationPDF(ApexPages.StandardController controller){
        app = (Application__c)controller.getRecord();
        appId = ApexPages.currentPage().getParameters().get('id');
        strPathway = ApexPages.currentPage().getParameters().get('strPathway');
        System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<< strPathway ' + strPathway  + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
        if(strPathway!=NULL && strPathway!='')
            getPDFFields();


    }

    public void getPDFFields()
    {

         
         pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                    Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                    PDF_Heading_1__c, PDF_Heading_2__c, Table_Section_Heading__c, PDF_Forwarded_Completed_App_Address__c from Program_Line_Item_Pathway__c where Id =: strPathway];

         tblColumns = new List<TableCol>();           

         tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
         tblColumns.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
         tblColumns.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
         tblColumns.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
         tblColumns.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
         if(pdfLnItmPway.Column_6_API_Name__c!=NULL)
         tblColumns.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c)); 
         
                        
    }

    public String getTable()
    {
        htmlText ='';
        //List<CARPOL_UNI_ApplicationPDF_Table__c> tableSettings = CARPOL_UNI_ApplicationPDF_Table__c.getall().values();
        //tableSettings.sort();
        

        htmlText += '<tr>';
            //loop over column heads from tablecol object
            for(TableCol tbl: tblColumns){
                
                if(Counter==5)
                    htmlText += '<td style="' + tdStyleEnd + '" width="20%">';
                else
                    htmlText += '<td style="' + tdStyle + '" width="20%">';
                htmlText +=  tbl.Heading;
                htmlText += '</td>';
                counter++;
                
            }
            
            htmlText += '</tr>';
        
        for(AC__c a: aclst)
        {
            counter =0;
            htmlText += '<tr>';
            for(TableCol tbl: tblColumns){
                
                //values
                if(counter==5)
                    htmlText+= '<td style="' + tdStyleEnd + '" width="20%">';
                else
                    htmlText += '<td style="' + tdStyle + '" width="20%">';
                htmlText +=  getFieldValue(a,tbl.APIName);
                
                htmlText += '</td>';
                counter++;
            }
            htmlText += '</tr>';
            
            
        }
        if(aclst.size()<10)
        {
            for(Integer i=0; i<10; i++)
                htmlText += '<tr><td style="' + tdStyle + '" width="20%">&nbsp;</td><td style="' + tdStyle + '" width="20%">&nbsp;</td><td style="' + tdStyle + '" width="20%">&nbsp;</td><td style="' + tdStyle + '" width="20%">&nbsp;</td><td style="' + tdStyle + '" width="20%">&nbsp;</td><td style="' + tdStyleEnd + '" width="20%">&nbsp;</td></tr>';   
        }
        return htmlText;
    }
    public List<AC__c> getACRecords(){
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ',Country_Of_Origin__r.Name, Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Regulated_Article__r.Name, Component__r.Name ,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name,Regulated_Article__r.Additional_Scientific_Name__c, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name from AC__c where Application_Number__c = \'' + app.Id + '\'';
            aclst = Database.query(soql);
        }
        
        return aclst;
    }
    
    public static Object getFieldValue(SObject o,String field){
 
     if(o == null){
        return null;
     }
     if(field.contains('.')){
    String nextField = field.substringAfter('.');
    String relation = field.substringBefore('.');
    return CARPOL_UNI_ApplicationPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
     }else{
    return o.get(field);
     }
    

    }

    public void savePDF(string pathID){
        if(app.Id != null){
            System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
            PageReference pdf = Page.CARPOL_UNI_ApplicationPDF;
            pdf.getParameters().put('id', app.Id);
            pdf.getParameters().put('strPathway', pathID);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                }
            att.Name = app.Name+'.pdf';
            att.ParentId = app.Id;
            att.body = fileBody;
            insert att;
        }
    }
    
    public Class TableCol
    {
        public String Heading;
        public String APIName;
        public String sortOrder;

        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }

}