@isTest(seealldata=true)
private class FindContactsTest{
    static testMethod void testFindContacts() {
    
    String BrokerContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Broker').getRecordTypeId();
    String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
    Account objacct = new Account();
      objacct.Name = 'AC account';
      objacct.RecordTypeId = AccountRecordTypeId;
      insert objacct;
    Contact objcont = new Contact();
      objcont.FirstName = 'FirstName';
      objcont.LastName = 'LastName';
      objcont.Email = 'test@email.com';
      objcont.MailingStreet = 'Mailing Street';
      objcont.MailingCity = 'Mailing City';
      objcont.MailingState = 'Ohio';
      objcont.MailingCountry = 'United States';
      objcont.MailingPostalCode = 'Mailing Postal Code';
      objcont.RecordTypeId = BrokerContRecordTypeId;
      insert objcont;
      Preparer_Request__c objprereq = new Preparer_Request__c();
      objprereq.Contact_Email__c = 'test@email.com';
      objprereq.Last_Name__c = 'LastName';
      objprereq.Contact_Phone__c = '1234567890';
      insert objprereq;
      Preparer_Request__c objprereq2 = new Preparer_Request__c();
      objprereq2.Contact_Email__c = 'objprereq2@email.com';
      objprereq2.Last_Name__c = 'LastName';
      objprereq2.Contact_Phone__c = '1234507890';
      insert objprereq2;
      system.assert(objprereq2 != null);                                   
    }
   }