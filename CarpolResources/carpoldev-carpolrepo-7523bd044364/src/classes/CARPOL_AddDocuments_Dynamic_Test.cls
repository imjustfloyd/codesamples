@isTest(seealldata=false)
private class CARPOL_AddDocuments_Dynamic_Test {
    static testMethod void testCARPOL_AddDocuments_Dynamic() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());     
      AC__c ac = testData.newLineItem('Personal Use',objapp);        
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);       

      Attachment att1 = testData.newattachment(ac.Id);  
      Attachment att2 = testData.newattachment(ac.Id);  
      Attachment att3 = testData.newattachment(ac.Id);                     

      PageReference pageRef = Page.CARPOL_AddDocuments_Dynamic;

      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
      ApexPages.currentPage().getParameters().put('Id',ac.id);
      CARPOL_AddDocuments_Dynamic cls = new CARPOL_AddDocuments_Dynamic();
      cls.newAttachments[0].documenttypes = new List<String>();
      cls.newAttachments[0].documenttypes.add('testdocumenttype');
      cls.newAttachments[0].attachment.Body = blob.valueof('******');
      cls.newAttachments[0].attachment.Name = 'test';
      cls.newAttachments[0].appAttach.File_Name__c = '';
      cls.newAttachments[0].appAttach.File_Description__c = '';
      cls.save();
      system.assert(cls != null);
        
    }
   }