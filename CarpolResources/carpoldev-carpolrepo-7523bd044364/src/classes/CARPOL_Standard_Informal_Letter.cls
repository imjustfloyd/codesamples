public with sharing class CARPOL_Standard_Informal_Letter {
    
    public ID TPId {get; set;}
    public Signature__c tp {get; set;}
    //Added by niharika 3/8
      public String parm1 {get;set;}
     public String strPathway {get;set;}
     public String Scientific_Name {get;set;}
     public String Country_Of_Origin {get;set;}
   public String Intended_Use {get;set;}
   public String sRAScName{get;set;}
    public Regulated_Article__c regulatedArticle{get;set;}
    public Country__c CountryOfOrigin{get;set;}
      public Program_Line_Item_Pathway__c ProgramPathway{get;set;}
      public Intended_Use__c IntendedUse{get;set;}
     public String State_Territory_of_Destination {get;set;}
     public Level_1_Region__c StateTerritoryofDestination{get;set;} // VV 5-4-16
     public RA_Scientific_Names__c RAScienticName{get;set;}
     public List<Wizard_Questionnaire__c> WQ{get; set;}  // VV added 3-30-16  
     public wizard_selections__c ws{get;set;}  // VV added 3-30-16     
     
    public CARPOL_Standard_Informal_Letter(ApexPages.StandardController controller) {
        TPId = ApexPages.currentPage().getParameters().get('TPId');// niharika 3/8
         System.Debug('<<<<<<< TPId   : ' + TPId  + ' >>>>>>>');
          //Niharika 3/8
         parm1  = apexpages.currentpage().getparameters().get('parm1');
         strPathway = apexpages.currentpage().getparameters().get('strPathway');
         System.Debug('<<<<<<< parm1   : ' + parm1  + ' >>>>>>>');
         System.Debug('<<<<<<< Pathway   : ' + strPathway  + ' >>>>>>>');
          Scientific_Name  = apexpages.currentpage().getparameters().get('Scientific_Name__c');
        Country_Of_Origin  = apexpages.currentpage().getparameters().get('Country_Of_Origin__c');
        Intended_Use  = apexpages.currentpage().getparameters().get('Intended_Use__c');
         sRAScName=  apexpages.currentpage().getparameters().get('RA_Scientific_Name__c');
        State_Territory_of_Destination  = apexpages.currentpage().getparameters().get('State_Territory_of_Destination__c');
        if(strPathway  != null ){
            ProgramPathway= [select name,Letter_Expiration_Date__c from Program_Line_Item_Pathway__c where id=:strPathway  ];
            System.Debug('<<<<<<< ProgramPathway Name: ' + ProgramPathway.name+ ' >>>>>>>');
        }
        if(Scientific_Name  != null ){
            regulatedArticle= [select name, Common_Name__c, Scientific_Name__c from Regulated_Article__c where id=:Scientific_Name  ];
            System.Debug('<<<<<<< regulatedArticle Name: ' + regulatedArticle.name+ ' >>>>>>>');
        }
         if(Country_Of_Origin  != null ){
            CountryOfOrigin= [select name from Country__c where id=:Country_Of_Origin  ];
            System.Debug('<<<<<<< CountryOfOrigin Name: ' + CountryOfOrigin.name+ ' >>>>>>>');
        }
         if(Intended_Use  != null ){
            IntendedUse= [select name from Intended_Use__c where id=:Intended_Use  ];
            System.Debug('<<<<<<< IntendedUse.Name: ' + IntendedUse.name+ ' >>>>>>>');
        }
        if(sRAScName!= null ){
            RAScienticName= [select Id,name from RA_Scientific_Names__c where id=:sRAScName];
        }
        if(State_Territory_of_Destination  != null && State_Territory_of_Destination  != 'null' ){
            StateTerritoryofDestination= [select name from Level_1_Region__c where id=:State_Territory_of_Destination  ];
            System.Debug('<<<<<<< State_Territory_of_Destination.Name: ' + StateTerritoryofDestination.name+ ' >>>>>>>');
        }
        
        //
        if (TPId == null){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No thumbprint found. Details cannot be filled.'));  
        }
        else {
        
        tp = [SELECT ID, Name,  
        Communication_Manager__c, 
        Communication_Manager__r.Name,
        Line_Record_Type__c,
        Intended_Use__r.Name,
        To_Level_1_Region__r.Name,
        From_Level_1_Region__r.Name,
        REF_Program_Name__c,
        Port__r.Name,
        Communication_Manager__r.Content__c,
        Regulated_Article__r.name,
        From_Country__r.name  
        FROM Signature__c WHERE ID = : TPId LIMIT 1];
        
         if(tp.id != null){
                if(tp.Line_Record_Type__c!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLineRecordType}',tp.Line_Record_Type__c);
                //VV 4-12-16
            string strlineitem = tp.Line_Record_Type__c;
            if (strlineitem.contains('PPQ')){
            boolean isgroupcountry = false;
            for(Country_Junction__c cj: [select id,Name from Country_Junction__c where Group__r.Name='Khapra Beetle Countries' and Country__c= :tp.From_Country__r.id])
            {
                isgroupcountry = true;
            }
            // integer groupCountry = [select count() from Country_Junction__c where Group__r.Name='Khapra Beetle Countries' and Country__c= :tp.From_Country__r.id ]; 
              // if (groupCountry >0){
              if(isgroupcountry){
               tp.communication_manager__r.content__c = tp.communication_manager__r.content__c.replace('{!tpCountrygroup}','Do not use jute or burlap bags as packing material. Using packing materials that can harbor Khapra Beetle could result in the rejection of your shipment');
               }else{
               tp.communication_manager__r.content__c = tp.communication_manager__r.content__c.replace('{!tpCountrygroup}','');
               }
             } //VV 4-12-16 
                }
                if(tp.To_Level_1_Region__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', tp.To_Level_1_Region__r.Name);
                }
                if(tp.From_Level_1_Region__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpFromLevel1Region}', tp.From_Level_1_Region__r.Name);
                }
                if(tp.REF_Program_Name__c!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpProgramName}', tp.REF_Program_Name__c);
                }
                if(tp.Port__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPorts}', tp.Port__r.Name);
                }
                //Added by Niharika 3/8
                if(Scientific_Name !=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRegulatedArticle}', regulatedArticle.name);
                if(regulatedArticle.Common_Name__c != null && regulatedArticle.Common_Name__c != '') //Added by Subbu
                        tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpCommonName}', regulatedArticle.Common_Name__c);
                }
                 if(Country_Of_Origin !=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpFromCountry}', CountryOfOrigin.Name );
                }
                 if(strPathway !=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpProgramPathway}', ProgramPathway.name );
                }
                if(strPathway !=null && ProgramPathway.Letter_Expiration_Date__c != null){
                 tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpExpirationDate}', String.valueof(ProgramPathway.Letter_Expiration_Date__c) );
                }
                if(strPathway !=null && ProgramPathway.Letter_Expiration_Date__c == null){
                 tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpExpirationDate}', ' ' );
                }
                if(Intended_Use!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPurposeOfImportation}', IntendedUse.Name);
                }
                else
                {
                    system.debug('************'+tp.Communication_Manager__r.Content__c);
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('for </span>{!tpPurposeOfImportation}', '');
                }
                if(State_Territory_of_Destination  != null && State_Territory_of_Destination  != 'null' ){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', StateTerritoryofDestination.name);
                }

                if(sRAScName !=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', RAScienticName.name );
                }
                else
                {
     
                   // sRAScName = regulatedArticle.Scientific_Name__c; // VV added 4-15-16
                  // if(sRAScName != null)
                    if(Scientific_Name !=null && regulatedArticle.Scientific_Name__c != null) //Niharika added Scientific_Name
                      tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', regulatedArticle.Scientific_Name__c); // VV added sRAScName   4-15-16
                       
                }
            // VV 3-31-16 //Modified By Subbu    
            wq = new list<wizard_questionnaire__c>();
            for(wizard_questionnaire__c w: [select Id, signature__c from wizard_questionnaire__c where signature__c  = :tpid limit 1])
            {
                wq.add(w);
            }
           // wq = [select Id, signature__c from wizard_questionnaire__c where signature__c  = :tpid limit 1]; //vv
            if(wq != null && wq.size()!=0){
                if (wq[0].id != null){
                    for(wizard_selections__c s: [select id, value__c from wizard_selections__c where Wizard_Next_Question__c = :wq[0].id limit 1])
                    {
                        ws = s;
                    }
                   // ws = [select id, value__c from wizard_selections__c where Wizard_Next_Question__c = :wq[0].id limit 1]; //vv
                }
                if (ws !=null && ws.value__c != null){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!ProcessName}', ws.value__c );
                    system.debug('&&& WS = '+ws);
                }
            }               
                    // VV 3-31-16
        
        }
   
    }
}
}