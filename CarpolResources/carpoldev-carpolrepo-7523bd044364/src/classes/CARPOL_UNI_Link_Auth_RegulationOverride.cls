public class CARPOL_UNI_Link_Auth_RegulationOverride { //Portal_Link_Auth_Regulation_Detail?id=a0U35000000D4vbEAC
  public CARPOL_UNI_Link_Auth_RegulationOverride() {

  }


 String recordId;

public CARPOL_UNI_Link_Auth_RegulationOverride(ApexPages.StandardController
     controller) {recordId = controller.getId();}

public PageReference redirect() {
Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
if ('APHIS Applicant'.equals(p.name)
    || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name)||'State Reviewer'.equals(p.name))
    {
    
     PageReference customPage = Page.Portal_Link_Auth_Regulation_Detail;
     
     //PageReference customPage = new  Pagereference ('apex/Portal_Authorization_Detail1?id='+recordId);
     customPage.setRedirect(true);
     customPage.getParameters().put('id', recordId);
     return customPage;
    } else {
    String hostname = ApexPages.currentPage().getHeaders().get('Host');
           String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
           pagereference pageref = new pagereference(optyURL2);
           pageref.setredirect(true);
           return pageref;

       return null; //otherwise stay on the same page
    }
 }
}