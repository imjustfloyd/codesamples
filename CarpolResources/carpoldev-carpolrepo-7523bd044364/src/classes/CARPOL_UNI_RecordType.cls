public class CARPOL_UNI_RecordType 
{
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName)
    {
        //Generate a map of tokens for all the Record Types for the desired object
        Map<string, schema.recordtypeinfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosByName();

        if(!recordTypeInfo.containsKey(recordTypeName))
            throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');

        //Retrieve the record type id by name
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }
    
    public static String getObjectRecordTypeName(SObjectType sObjectType, Id recordTypeId)
    {

    //Generate a map of tokens for all the Record Types for the desired object
      Map<id, schema.recordtypeinfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();

    if(!recordTypeInfo.containsKey(recordTypeId))
    throw new RecordTypeException('Record type "'+ recordTypeId +'" does not exist.');

    //Retrieve the record type id by name

     return recordTypeInfo.get(recordTypeId).getName();
   }  

    public class RecordTypeException extends Exception{}
}