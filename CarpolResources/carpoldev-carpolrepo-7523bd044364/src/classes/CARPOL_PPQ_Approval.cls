@RestResource(urlMapping='/carpolppqattach/*')
global class CARPOL_PPQ_Approval{

   /* @HttpPost
    global static void PPQattchments(string appid,string authid,string appstatus,Integer permitno,string sessionId)
    {
        list<Attachment> lstattach = new list<Attachment>();
        Blob PDFPageblob = null;
        system.debug('--authid---'+authid);
        system.debug('--appid---'+appid);
        system.debug('--appstatus---'+appstatus);
        if(appstatus=='Approved')
        {
            system.debug('--appstatus---'+appstatus);
                PageReference PDFPage = new PageReference('/apex/CARPOL_PermitPDF?id='+authid);
                
                if (!Test.isRunningTest()){
                  PDFPageblob = PDFPage.getContent();            
                    }
                else {
                    PDFPageblob = Blob.valueOf('Some Text');
                } 
                Attachment objatt = new Attachment();
                objatt.Body = PDFPageblob ;
                objatt.Name = 'ePermits'+ permitno +'.pdf';             
                objatt.ParentId = appid;
                lstattach.add(objatt);
                insert objatt;               
                Attachment objatt2 = new Attachment();
                objatt2.Body = PDFPageblob ;
                objatt2.Name = 'ePermits'+ permitno +'.pdf';
                objatt2.ParentId = authid;
                insert objatt2;
               lstattach.add(objatt);
                //insert lstattach;
            //}
        }
    } */
  /*   @HttpPost
    global static void sendEmail(string appid,string authid,string appstatus,string permitno) {
        System.debug('Hello Service');
        system.debug('--authid---'+authid);
        system.debug('--appid---'+appid);
        system.debug('--appstatus---'+appstatus);
        // Reference the attachment page, pass in the account ID
        PageReference pdf = Page.CARPOL_PPQ_PermitPDF;
        pdf.getParameters().put('id',authid);
        pdf.setRedirect(true);
        
        // Take the PDF content
        Blob b = (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
        Attachment atch = new Attachment();
        atch.ParentId = appid;
        atch.body = b;
        atch.name='MyAttachment2.pdf';
        insert atch;
    } */
    
     @HttpPost
    global static void sendEmail(string appid,string authid, string decisiontype) {
    
    try{
        
        PageReference pdf = new PageReference('');
        Boolean attachPermit= false;
        Authorizations__c auth = [SELECT Id, Name, Thumbprint__r.Program_Prefix__r.name, CITES_Needed__c, Regular_Permit_Needed__c, Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c, Application__c, Program_Pathway__r.Permit_PDF_Template__c, Status__c, Authorization_Type__c, Thumbprint__c,RecordType.Name FROM Authorizations__c WHERE Id =:authid];
        Attachment atch = new Attachment();       
        
        
     //  system.debug('--appstatus---'+appstatus);
        List<Workflow_Task__c> wrkFlowTaskSTP = New List<Workflow_Task__c>();
        wrkFlowTaskSTP = [Select Id, Name,  Status__c from Workflow_Task__c where Authorization__c =:auth.id and Name = 'Issue Standard Permit' and Status__c = 'Deferred'];    
      
        if((decisiontype == 'Permit' && wrkFlowTaskSTP.size()==0) || (auth.Regular_Permit_Needed__c && auth.CITES_Needed__c))   
        { 
          if(auth.Id!=null)
          {
             if (auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null){ // VV added on 7-12-16 for PEQ VF for template
                 //VV added 7/14/16 to auto attach a draft permit if the applicant is a violator for PEQ only
                  if (auth.status__c == 'Issued'){
                     pdf= new PageReference('/apex/'+auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c);   
                     atch.name='Permit.pdf';    
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                       
                     }else{
                     //VV end additions on 7/14/16
                    pdf= new PageReference('/apex/'+auth.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c+'_DRAFT'); 
                    system.debug('---pdf---'+pdf);  
                    atch.name='Draft Permit.pdf';         
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                                            
                   } 
                }else if(auth.Program_Pathway__r.Permit_PDF_Template__c!=null && !auth.CITES_Needed__c){
                   pdf = new PageReference('/apex/'+auth.Program_Pathway__r.Permit_PDF_Template__c);
                    atch.name='Permit.pdf';            
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                             
                  
                }else if(auth.Program_Pathway__r.Permit_PDF_Template__c!=null && auth.CITES_Needed__c && auth.Thumbprint__r.Program_Prefix__r.name == '211'){
                   pdf = new PageReference('/apex/'+auth.Program_Pathway__r.Permit_PDF_Template__c);
                    atch.name='Permit.pdf';            
                     pdf.getParameters().put('id',authid);
                     attachPermit = true;                                             
                  }                  
              else if (auth.Program_Pathway__r.Permit_PDF_Template__c == null && !auth.CITES_Needed__c)
                   {
                    pdf = Page.CARPOL_PPQ_PermitPDF;
                    atch.name='Permit.pdf'; 
                    pdf.getParameters().put('id',authid);
                    attachPermit = true;                                                        
                   }

           }
          

          // pdf.getParameters().put('id',authid);
           //attachPermit = true;
           
        } 
        else if(decisiontype == 'Letter of Denial')   
        { 
          
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter Of Denial.pdf';
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          pdf.getParameters().put('id',appid);
          attachPermit=true;
                    
        } 
        else if(decisiontype == 'Letter of No Permit Required')   
        { 
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter Of No Permit Required.pdf';
          pdf.getParameters().put('id',appid);
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          attachPermit=true;
        } 
        else if(decisiontype == 'Letter of No Jurisdiction')   
        { 
          pdf = Page.CARPOL_Standard_Formal_Letter;
          atch.name='Letter of No Jurisdiction.pdf';
          pdf.getParameters().put('id',appid);
          pdf.getParameters().put('authid',authid); // VV 3-20-16
          attachPermit=true;
        }  
        

        
      //  pdf.getParameters().put('id',authid);
        if(attachPermit)
        {
            pdf.setRedirect(true);
            
         // Take the PDF content
            //Blob b= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
            Blob b= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContentAsPDF());
            //Blob b= pdf.getContent();
            atch.ParentId = authid;//appid;
            atch.body = b;
            //atch.name='Permit.pdf';
            insert atch;   
         }
        
        //CITES permit
        //check if workflow task issue ppp is set to complete - do not process if deffered or anything else
        // for Scenario 3: lookup CITES Auth
        system.debug('--ccc-decisiontype--'+decisiontype);
        List<Authorizations__c> CitiesAuth = [Select Id from Authorizations__c where Application__c =: appId and prefix__c =: '204' limit 1];
        system.debug('--ccc-CitiesAuth --'+CitiesAuth );
        if(decisiontype == 'Permit')
        {
            List<Workflow_Task__c> wrkFlowTaskPPP = new List<Workflow_Task__c>();
            wrkFlowTaskPPP = [Select Id, Name, Status__c from Workflow_Task__c  
                                                                  where Authorization__c =:auth.id
                                                                   AND Name = 'Issue Protected Plant Permit' 
                                                                   and Status__c = 'Complete'];
             system.debug('--ccc-wrkFlowTaskPPP--'+wrkFlowTaskPPP);                                                      
            if(wrkFlowTaskPPP.size()!=0 || CitiesAuth.size()!=0)
                {
                    pdf = Page.CARPOL_PPQ_CITEPermitPDF;
                    system.debug('--ccc-pdf-'+pdf); 
                    pdf.getParameters().put('id',authid);
                    // VV Added for Transit CITES 9-2-16
                    system.debug('---prefix = '+auth.Thumbprint__r.Program_Prefix__r.name); 
                    if (auth.Thumbprint__r.Program_Prefix__r.name != '204'){
                    pdf.getParameters().put('id',CitiesAuth[0].Id);
                    } //End VV updates
                    
                    pdf.setRedirect(true);
                    blob bb= (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
                    system.debug('--ccc-get content-');
                    atch = new attachment();
                    atch.ParentId = (CitiesAuth.size()!=0)? CitiesAuth[0].Id : authid;//appid;
                    atch.name='CITES Permit.pdf';
                    atch.body = bb;
                
                    insert atch;   
                }
            
             //fire update on CITES auth that came over with regular permit
            //so that premit number is generated
            if(CitiesAuth.size()!=0)
            {
                CitiesAuth[0].Authorization_Type__c = 'Permit';
                CitiesAuth[0].Response_Type__c = 'Permit';
                update CitiesAuth[0];
            }
            
        }      
        
   /*
          // Reference the attachment page, pass in the account ID
        PageReference pdf = Page.CARPOL_PPQ_PermitPDF;
        pdf.getParameters().put('id',authid);
        pdf.setRedirect(true);      
        Blob b = (test.isrunningtest()?Blob.valueOf('UNIT.TEST'):pdf.getContent());
        Attachment atch = new Attachment();
        atch.ParentId = authid;
        atch.body = b;
        atch.name='Permit.pdf';
        insert atch;     */ 
     }
     catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
   
        
    }    
}