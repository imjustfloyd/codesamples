public with sharing class CARPOL_UNI_getIntendedUse {
    
    public List<SelectOption> getIntendedUse(string selectedAnswerID, Map<string, string> allAnswersMap){
        List<SelectOption> Options = new List<SelectOption>();
        List<Program_Line_Item_Pathway__c> ProgramLineItems = [SELECT Id, Parent_Id__c FROM Program_Line_Item_Pathway__c WHERE Name =: selectedAnswerID or Id =: selectedAnswerID];
        if(ProgramLineItems.size() > 0) {
             List<Intended_Use__c> IntendedUseList = [SELECT Department__c, Name, Id, Program_Line_Item_Pathway__c,Intended_Use_Description__c FROM Intended_Use__c WHERE Program_Line_Item_Pathway__r.Id =:ProgramLineItems[0].Id AND Status__c =: 'Active' Order By Name];
            //List<Intended_Use__c> IntendedUseList = [SELECT Department__c, Name, Id, Program_Line_Item_Pathway__c FROM Intended_Use__c WHERE Program_Line_Item_Pathway__r.Id =:selectedAnswerID Order By Name];
            if(IntendedUseList.size() > 0) {
                for(Intended_Use__c IU : IntendedUseList) {
                  
                    //Options.add(new SelectOption(IU.Id, IU.Name));
                     SelectOption so = new SelectOption(IU.Id,'<span title="'+ (IU.Intended_Use_Description__c!=null?IU.Intended_Use_Description__c:'')+ '">'+IU.Name+'</span>');
                    so.setEscapeItem(false);
                     Options.add(so);
                    allAnswersMap.put(IU.Id, IU.Name);
                }
               return Options;
            }
        }
        
        return null;    
    }
    
    public List<SelectOption> getIntendedUse(string selectedAnswerID){
        List<SelectOption> Options = new List<SelectOption>();
        List<Program_Line_Item_Pathway__c> ProgramLineItems = [SELECT Id, Parent_Id__c FROM Program_Line_Item_Pathway__c WHERE Name =: selectedAnswerID or Id =: selectedAnswerID];
        if(ProgramLineItems.size() > 0) {
             List<Intended_Use__c> IntendedUseList = [SELECT Department__c, Name, Id, Program_Line_Item_Pathway__c FROM Intended_Use__c WHERE Program_Line_Item_Pathway__r.Id =:ProgramLineItems[0].Id AND Status__c =: 'Active' Order By Name];
            //List<Intended_Use__c> IntendedUseList = [SELECT Department__c, Name, Id, Program_Line_Item_Pathway__c FROM Intended_Use__c WHERE Program_Line_Item_Pathway__r.Id =:selectedAnswerID Order By Name];
            if(IntendedUseList.size() > 0) {
                for(Intended_Use__c IU : IntendedUseList) {
                    Options.add(new SelectOption(IU.Id, IU.Name));
                  //  allAnswersMap.put(IU.Id, IU.Name);
                }
                return Options;
            }
        }
        
        return null;    
    }
    

}