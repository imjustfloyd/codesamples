public class CARPOL_CheckLineAttachments {

 // These variables store Trigger.oldMap and Trigger.newMap
 // Map<Id, AC__c> oldLine;
  Map<Id, AC__c> newLine;
  Set<ID> setLineIds = new Set<ID>();

// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_CheckLineAttachments(
 //   Map<Id, AC__c> oldTriggerLines, 
    Map<Id, AC__c> newTriggerLines) {
  //   oldLine = oldTriggerLines;
      newLine = newTriggerLines;
      system.debug('#######################      ' + newTriggerLines + '     #############################');
  }
 
 
 public void checkAttachments()  {
 String ACAttachmentsRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
   for(AC__c ac : newLine.values())
     {
     if (ac.Status__c == 'Saved'){
     Ac__c lineAtt = newLine.get(ac.Id);
          System.Debug('<<<<<< Line Status : ' +  ac.Status__c + ' >>>>>>');
      // check validation for Resale/Adoption
      List<Applicant_Attachments__c> att = new List<Applicant_Attachments__c>();
          system.debug('ACID=' + ac.ID + ' rECORDTYPEID = ' + ACAttachmentsRecordTypeId + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^ att.size');
      att = [SELECT ID, RecordTypeId, Animal_Care_AC__c, Document_Types__c, Total_Files__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c = :ac.ID AND RecordTypeId =: ACAttachmentsRecordTypeId];
      
          
           
          
          
            if(att.size()!=0) { 
                List<Attachment> appatt = [select ID, Body from Attachment where ParentID = :att[0].Id ORDER BY CreatedDate DESC LIMIT 1];
                if(appatt.size()!=0) {
                system.debug('SIZE:' + att.size() + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^ att.size');
                 system.debug('Total Files:' + att[0].Total_Files__c + ' ^^^^^^^^^^^^^^^^^^^^^^^^^^ att[0].Total_Files__c');
                 //setup new list for document types
                 List<Applicant_Attachments__c> attHealthCertificate = new List<Applicant_Attachments__c>();
                 List<Applicant_Attachments__c> attRabiesVaccination = new List<Applicant_Attachments__c>();
                 List<Applicant_Attachments__c> attIACUCResearchProposal = new List<Applicant_Attachments__c>();
                 List<Applicant_Attachments__c> attResearchJustification = new List<Applicant_Attachments__c>();
                 List<Applicant_Attachments__c> attVetVeterinaryTreatment = new List<Applicant_Attachments__c>();
                 List<Applicant_Attachments__c> attTotalFiles = new List<Applicant_Attachments__c>();
         //  if(att.size()<>0 && ac.Purpose_of_the_Importation__c != 'Personal Use'){
            //       System.Debug('<<<<<< No attachments and not personal use: ' +  att.size() + ' >>>>>>');
            
                      for (Applicant_Attachments__c aAtt: att)
                      {
                          if(aAtt.Document_Types__c.contains('Health Certificate (AC7041)')){
                              attHealthCertificate.add(aAtt);
                             //   lineAtt.Health_Certificate_Attached__c = True;
                              
                              }
                          System.Debug('<<<<<< Attachment Health Certificate Size : ' +  attHealthCertificate.size() + ' >>>>>>');
                            
                          if(aAtt.Document_Types__c.contains('Rabies Vaccination Certificate (AC7042)')){
                              attRabiesVaccination.add(aAtt);
                              //  lineAtt.Rabies_Vaccination_Certificate_Attached__c = True;
                              
                              }
                          System.Debug('<<<<<< Attachment Rabies Vaccination Size : ' +  attRabiesVaccination.size() + ' >>>>>>');
                          
                          if(aAtt.Document_Types__c.contains('IACUC Approved Research Proposal')){
                              attIACUCResearchProposal.add(aAtt);
                              }
                          System.Debug('<<<<<< Attachment IACUC Research Proposal Size : ' +  attIACUCResearchProposal.size() + ' >>>>>>');
                          
                          if(aAtt.Document_Types__c.contains('Research Justification')){
                              attResearchJustification.add(aAtt);
                              }
                          System.Debug('<<<<<< Attachment Research Justification Size : ' +  attResearchJustification.size() + ' >>>>>>');
                          
                          if(aAtt.Document_Types__c.contains('Veterinary Treatment Agreement')){
                              attVetVeterinaryTreatment.add(aAtt);
                             
                              //  lineAtt.Vet_Agreement_Attached__c = True;
                                
                              }
                          System.Debug('<<<<<< Attachment Vet Veterinary Treatment Size : ' +  attVetVeterinaryTreatment.size() + ' >>>>>>');
                      
                          if (aAtt.Total_Files__c < 1){
                          attTotalFiles.add(aAtt);
                          }
                                           // }
                      
                      //Check Attachment total file number
                     // if (ac.Status__c == 'Saved' && attTotalFiles.size() !=0){
                       //    lineAtt.Applicant_Instructions__c='Please attach required attachments on Applicant Attachment before you submit the application line item for approval. Click on New Applicant Attachment below';
                    //  }
                      
                      //Resale: alert error message on line item
                      system.debug('*********************************** Status = ' + ac.Status__c + '   and POI = ' + ac.Purpose_of_the_Importation__c + '***********************************');
                      if(ac.Status__c == 'Saved' && ac.Purpose_of_the_Importation__c == 'Resale/Adoption'){
                          if(attHealthCertificate.size() == 0 && attRabiesVaccination.size() != 0){
                              lineAtt.Applicant_Instructions__c='Please attach Health Certificate documents by clicking on New Applicant Attachment below.';
                              //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if(attHealthCertificate.size() != 0 && attRabiesVaccination.size() == 0){
                              lineAtt.Applicant_Instructions__c='Please attach relevant Rabies Vaccination documents by clicking on New Applicant Attachment below.';
                               //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if(attHealthCertificate.size() != 0 && attRabiesVaccination.size() != 0){
                            lineAtt.Status__c = 'Ready to Submit';
                            //lineAtt.Contains_Applicant_Attachments__c=True;
                            lineAtt.Applicant_Instructions__c='';
                          }
                      } 
                     //Research: alert error message on line item
                      else if(ac.Status__c == 'Saved' && ac.Purpose_of_the_Importation__c == 'Research'){
                          if((attHealthCertificate.size() == 0 && attRabiesVaccination.size() > 0) &&(attIACUCResearchProposal.size() ==0 && attResearchJustification.size() ==0 )){
                             lineAtt.Applicant_Instructions__c='Please attach Health Certificate or IACUC Approved Research Proposal and Research Justification documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() == 0 && attRabiesVaccination.size() > 0) && (attIACUCResearchProposal.size()==0 && attResearchJustification.size() >0 )){
                             lineAtt.Applicant_Instructions__c='Please attach Health Certificate or IACUC Approved Research Proposal documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() == 0 && attRabiesVaccination.size() > 0) && (attIACUCResearchProposal.size()>0 && attResearchJustification.size() ==0 )){
                             lineAtt.Applicant_Instructions__c='Please attach Health Certificate or Research Justification documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() > 0 && attRabiesVaccination.size() == 0) && (attIACUCResearchProposal.size()==0 && attResearchJustification.size() == 0)){
                             lineAtt.Applicant_Instructions__c='Please attach Rabies Vaccination or IACUC Approved Research Proposal and Research Justification documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() > 0 && attRabiesVaccination.size() == 0) && (attIACUCResearchProposal.size()==0 && attResearchJustification.size()>0)){
                             lineAtt.Applicant_Instructions__c='Please attach Rabies Vaccination or IACUC Approved Research Proposal documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() > 0 && attRabiesVaccination.size() == 0) && (attIACUCResearchProposal.size()>0 && attResearchJustification.size() == 0)){
                               lineAtt.Applicant_Instructions__c='Please attach Rabies Vaccination or Research Justification documents by clicking on New Applicant Attachment below.';
                               //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() == 0 && attRabiesVaccination.size() == 0) && (attIACUCResearchProposal.size()>0 && attResearchJustification.size() == 0)){
                              lineAtt.Applicant_Instructions__c='Please attach Health Certificate and Rabies Vaccination or Research Justification documents by clicking on New Applicant Attachment below.';
                              //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                          else if((attHealthCertificate.size() == 0 && attRabiesVaccination.size() == 0) && (attIACUCResearchProposal.size()==0 && attResearchJustification.size() >0 )){
                              lineAtt.Applicant_Instructions__c='Please attach Health Certificate and Rabies Vaccination or IACUC Approved Research Proposal documents  by clicking on New Applicant Attachment below.';
                              //lineAtt.Contains_Applicant_Attachments__c=False;
                          }
                          else if((attHealthCertificate.size() > 0 && attRabiesVaccination.size() >0) || (attIACUCResearchProposal.size()>0 && attResearchJustification.size()>0)){
                            lineAtt.Status__c = 'Ready to Submit';
                            //lineAtt.Contains_Applicant_Attachments__c=True;
                            lineAtt.Applicant_Instructions__c='';
                          }
    
                      }
                      //Veterinary Treatment: alert error message on line item
                      else if(ac.Status__c == 'Saved' && ac.Purpose_of_the_Importation__c == 'Veterinary Treatment'){
                           if(attHealthCertificate.size() == 0 &&  attVetVeterinaryTreatment.size()!=0){
                             lineAtt.Applicant_Instructions__c='Please attach Health Certificate documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                         
                          else if(attHealthCertificate.size() == 0 && attVetVeterinaryTreatment.size()==0){
                             lineAtt.Applicant_Instructions__c='Please attach Health Certificate and Veterinary Treatment documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                         
                          else if(attHealthCertificate.size() != 0  && attVetVeterinaryTreatment.size()==0){
                             lineAtt.Applicant_Instructions__c='Please attach  Veterinary Treatment documents by clicking on New Applicant Attachment below.';
                             //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                         
                           else if((attHealthCertificate.size() != 0 && attVetVeterinaryTreatment.size()!=0 ) || (attHealthCertificate.size() != 0 && attRabiesVaccination.size()!=0 )){
                           lineAtt.Status__c = 'Ready to Submit';
                           lineAtt.Applicant_Instructions__c='';
                           //lineAtt.Contains_Applicant_Attachments__c=True;
                          }
                      }
                  }
                 }
                  
    
        // }
        // update att;
         //continue;

}

  
    System.Debug('<<<<<< Trigger to check Attachments Ends >>>>>>'); 
    }
                    }
              } 
         }