public with sharing class CARPOL_UNI_Get_Required_LI_Docs {
    public static List<String> getRequiredDocs(ID lineItemId){
        List<String> returnList = new List<String>();
        List <Related_Line_Decisions__c> RLDList = [SELECT ID, Decision_Matrix__c, Line_Item__c FROM Related_Line_Decisions__c WHERE Line_Item__c=:lineItemId]; 
    
        Set<ID> DMIDs = new Set<ID>();
        for(Related_Line_Decisions__c r : RLDList){
            DMIDs.add(r.Decision_Matrix__c);
        }    
        List<Required_Documents__c> docsList = [SELECT ID, Decision_Matrix__c, Required_Docs__c FROM Required_Documents__c WHERE Decision_Matrix__c IN: DMIDs];        

        //gather all of the entries from all the required documents and de-duplicate

        Map<String, String> reqDocs = new Map<String,String>();
        
        for(Required_Documents__c d : docsList){
            if(d.Required_Docs__c != null){
                if(d.Required_Docs__c.contains(';')){ 
                    for(String req : d.Required_Docs__c.split(';')){
                        if(!reqDocs.containsKey(req))
                            reqDocs.put(req, req);
                    }
                } else {
                    if(!reqDocs.containsKey(d.Required_Docs__c))
                        reqDocs.put(d.Required_Docs__c, d.Required_Docs__c);                    
                }
            }
        }
        
        if(reqDocs.size() > 0){
            returnList.addall(reqDocs.keyset());
        } 
        return returnList;
  }
}