public with sharing class CARPOL_BRSLinkConstructController {
/*  ====================================================  */
/*  Name: CARPOL_BRSLinkConstruct                         */  
/*  Related VF Page: CARPOL_BRSLinkConstruct              */
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */  
/*  ====================================================  */ 
/*  ====================================================  */
/*  History:                                              */    
/*  ----------------------------------------------------  */
/*  VERSION AUTHOR  DATE    DETAIL  RELEASE/CSR           */
/*  1.0 - D12/Kishore 05/08/2015  INITIAL DEVELOPMENT     */ 
/*  ====================================================  */  
    public Id LineItemId{get;set;}
    public Construct_Application_Junction__c obj {get;set;} 
    public Id strPathwayId{get;set;}
    public String redirect{get;set;}
    public string applid {get;set;}
    public string contructs {get;set;}
    public String[] ps= new String[]{};
    public Construct_Application_Junction__c objconstjun {get;set;}
    public CARPOL_BRSLinkConstructController()
    {
        
        LineItemId = ApexPages.currentPage().getParameters().get('appid');   
        strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
        redirect = ApexPages.currentPage().getParameters().get('redirect');
        
        applid =  ApexPages.CurrentPage().getParameters().get('appid');
         objconstjun = new Construct_Application_Junction__c();
         objconstjun.Line_Item__c = applid;
        

       // List<Construct_Application_Junction__c> LstConstJun = [select id, Construct__c, Line_Item__c,Design_Protocol_Record_SOP__c  from Construct_Application_Junction__c  where id=: applid ];
      //  objconstjun = ( LstConstJun != null && LstConstJun.size()>0) ? LstConstJun [0]: null;
        
     }
    
    public String[] getContructs() {
    return ps;
  }

 

  public void setContructs(String[] ps) {

    this.ps = ps;

  }


 public List<SelectOption> getItems() {
    List<SelectOption> op = new List<SelectOption>();
     op.add(new SelectOption('--None--','--None--'));
    for(Construct__c c : [SELECT Id, Name, Construct_s__c FROM Construct__c where Status__c =: 'Review Complete' and Decision__c =: 'Authorized']) op.add(new SelectOption(c.Id, c.Construct_s__c));
    return op;
 }
    
    public PageReference cancel() {
        if(redirect == 'yes'){
            PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pagref = new PageReference('/'+applid);
        return pagref ;
    }


    public PageReference save() {
        objconstjun.Construct__c = contructs;
        insert objconstjun ;
        if(redirect == 'yes'){
            PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pagref = new PageReference('/'+applid);
        return pagref ;
        }    
        
}