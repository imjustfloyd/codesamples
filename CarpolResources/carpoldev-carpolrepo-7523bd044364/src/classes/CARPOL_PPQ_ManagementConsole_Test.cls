/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - AC Management Console Test Class
*/

@isTest(seealldata=false)
private class CARPOL_PPQ_ManagementConsole_Test {
    
    static testMethod void ppqManagementConsole() {
      
        PageReference pageRef = Page.CARPOL_PPQ_ManagementConsole;
        Test.setCurrentPage(pageRef);
        CARPOL_PPQ_ManagementConsole acMC = new CARPOL_PPQ_ManagementConsole();
        acMC.getResults(); 
        List<Apexpages.Message> pageMessages1 = ApexPages.getMessages();
        
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        String ACgroupRecordTypeId = Schema.SObjectType.Group__c.getRecordTypeInfosByName().get('Signature').getRecordTypeId();   
          
        CARPOL_PPQ_TestDataManager testData = new CARPOL_PPQ_TestDataManager();
        testData.insertcustomsettings();
        
        //Account objacct = testData.newAccount(AccountRecordTypeId); 
        //Contact objcont = testData.newcontact();
        //breed__c objbrd = testData.newbreed(); 
        //Applicant_Contact__c apcont = testData.newappcontact(); 
        //Applicant_Contact__c apcont2 = testData.newappcontact();
        //Facility__c fac = testData.newfacility('Domestic Port');  
        //Facility__c fac2 = testData.newfacility('Foreign Port');
        //Application__c objapp = testData.newapplication();
        //AC__c ac = testData.newLineItem('Personal Use',objapp);      
        Program_Prefix__c pp = testData.newPrefix();
        Country__c cntry = testData.newcountryAF();

        Group__c grp = new Group__c();
        grp.Name = 'Test';
        grp.RecordTypeId = ACgroupRecordTypeId;
        grp.Group_Custom_Name__c = 'Test';
        grp.Status__c = 'Active';
        insert grp;      
        
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        Regulated_Article__c regart = testdata.newRegulatedArticle(plip.id);

        Signature__c tp = testData.newThumbprint();
        tp.From_Country__c = cntry.id;
        tp.Regulated_Article__c = regart.id;
        tp.Group__c = grp.id;
        update tp;
        
        Regulation__c newreg1 = testData.newRegulation('Import Requirements','Test');
        Signature_Regulation_Junction__c srj = testData.newSRJ(tp.id,newreg1.id);
        
        acMC.signature = tp;         
        acMC.getResults();    
        
        system.assert(acMc != null);   
        
        List<Apexpages.Message> pageMessages2 = ApexPages.getMessages();  
        acMC.Save();  
            
    }
}