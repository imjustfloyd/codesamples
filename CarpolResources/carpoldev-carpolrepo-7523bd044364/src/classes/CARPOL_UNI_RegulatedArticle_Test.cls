@isTest(seealldata=false)
private class CARPOL_UNI_RegulatedArticle_Test {
      @IsTest static void CARPOL_UNI_RegulatedArticle_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulated_Article__c regart1 = testData.newRegulatedArticle(plip.id);
          Regulated_Article__c regart2 = testData.newRegulatedArticle(plip.id);
          List<Regulated_Article__c> regList = new List<Regulated_Article__c>();
          regList.add(regart1);
          regList.add(regart2);
          Map<String,String> answers = new Map<String,String>();
          Test.startTest(); 

              CARPOL_UNI_RegulatedArticle extclass = new CARPOL_UNI_RegulatedArticle();
              extclass.getRegulatedArticles(regList, answers);
              extclass.searchRegulatedArticles(regList,'Test article',regart1.Category_Group_Ref__c,plip.id);
              extclass.searchRegulatedArticles(regList,'Test article','',plip.id);              
              extclass.showArticle(true, regList);
              extclass.showArticle(true, new List<Regulated_Article__c>());              
              system.assert(extclass != null);              
          Test.stopTest();   
      }
}