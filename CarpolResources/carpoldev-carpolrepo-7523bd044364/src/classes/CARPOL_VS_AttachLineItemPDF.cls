public class CARPOL_VS_AttachLineItemPDF {

    public ID appId {get; set;}
     public ID authId {get; set;}
   // public Application__c app {get; set;}
   
   public Authorizations__c auth{get;set;}
    public list<Authorizations__c> authList{get;set;}
    public boolean displayValidRecordType{get;set;}
    public boolean displayValidAuth{get;set;}
     public boolean displayValid {get;set;}
     public list<AC__c> aclst{get;set;}
     public list<AC__c> oneLineItem{get;set;}
     public Application__c appln {get;set;}
     public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public List<Authorization_Junction__c> scjC {get;set;}
    public Set<String> groupList{get;set;}
    public map<String,list<ac__c>> groupLineItemMap{get;set;}
    public map<String,list<ac__c>> groupLineItemMapfirst4{get;set;}
    public map<String,list<ac__c>> groupLineItemMapnext{get;set;}
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    public List<TableCol> tblColumns{get;set;}
    public List<TableCol> tblColumns1{get;set;}
    public map<string,string> fieldValueMap{get;set;}
    public string strPathway;
    public string PDF_Heading_2{get;set;}
    public string PDF_Heading_1{get;set;}
    public string Permit_PDF_Heading_1{get;set;}
    public string Permit_PDF_Heading_2{get;set;}

    //Added by Subbu
    public Map<String, Decimal> totalNumberThisPermitAuthorizes{get;set;}
    public String regulationDescriptionToBePrinted{get;set;}
    public String regulationDescriptionToBePrintedOV{get;set;}
    public String regDescriptionFirst100Chars{get;set;}
    public String regDescriptionFirst100CharsOV{get;set;}
    public String htmlText {get;set;} 
    public String htmlText_vet {get;set;} 
    public String htmlText2 {get;set;}       
    public Program_Line_Item_Pathway__c lineItemPathwayFields{get;set;}
    public String tdStyleEnd ='font-size:11.5px;text-align:left';
    public String tdStyle = 'font-size:11.5px;text-align:left';
    public String lineItemString;
    public Integer counter = 0;
    public Integer lineItemSize;
    public Boolean displayStringFlag{get;set;}
    public String permitSummaryString{get;set;}
    public List<Facility_Vet_Services_Junction__c> facVetJnObj{get;set;}
    
    public CARPOL_VS_AttachLineItemPDF (ApexPages.StandardController controller){
    displayValidRecordType= true;
     displayValidAuth= false;
    //app = (Application__c)controller.getRecord();
    auth = (Authorizations__c)controller.getRecord();
    
    authId = ApexPages.currentPage().getParameters().get('id');
    auth= [select id from Authorizations__c where id=:authId ];
    //System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
    //System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
    authList = new list<Authorizations__c>();
    //authList  = [SELECT Permit_Number__c,Date_Issued__c,Authorization_Letter_Content__c FROM Authorizations__c WHERE Application__c =:appId and Authorization_Type__c='Permit' ];
    authList  = [SELECT Permit_Number__c,Date_Issued__c,Authorization_Letter_Content__c,Application__c, Application__r.Applicant_Name__r.name,Application__r.Applicant_Name__r.title, Issuer_Name__c,  
                 Minimum_No_of_days__c, Minimum_charge_each_importation__c, Charge_per_Animal_per_day__c, Issuer_Title__c,Expiration_Date__c    FROM Authorizations__c WHERE id=:authId and Authorization_Type__c='Permit' ];
    id VSRecTypeID = [SELECT Id FROM RecordType WHERE DeveloperName = 'VS_RT' AND SobjectType = 'Application__c'].ID;
    
    appln = [SELECT id,name,RecordTypeId FROM Application__c WHERE id=:authList[0].Application__c];
   
    displayStringFlag = false;
    if (authList.size()>0)  displayValidAuth=true;
    // if(appln.RecordTypeId != VSRecTypeID ) // displayValidRecordType=false;    
     getRegulation();  
     getACRecords(); 
     getACRecords2();
       //getACRecords();  

    lineItemPathwayFields = [select id,Name, Section_6_Column_1_Information__c, Section_6_Column_2_Information__c, Section_6_Column_3_Information__c,
                                Section_6_Column_4_Information__c, Section_6_Column_5_Information_ID__c, Section_6_Column_6_Information_ID__c,
                                Section_6_Column_7_Information_ID__c, Permit_Summary_Header__c
                                from Program_Line_Item_Pathway__c where Id =: strPathway];
         
    facVetJnObj = new List<Facility_Vet_Services_Junction__c>();
    facVetJnObj = [Select Facility__c, Vet_Services__r.Name, Vet_Services__r.ShippingStreet,
                    Vet_Services__r.ShippingCity, Vet_Services__r.ShippingState, 
                    Vet_Services__r.ShippingPostalCode, Vet_Services__r.ShippingCountry,
                    Vet_Services__r.Phone, Vet_Services__r.Fax from Facility_Vet_Services_Junction__c where Authorization__c =:  authId ];
    }
     public list<TableCol> getPDFFields()
    {
     pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                    Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                    PDF_Heading_1__c, PDF_Heading_2__c, PDF_Forwarded_Completed_App_Address__c ,Permit_PDF_Heading_1__c,Permit_PDF_Heading_2__c
                    from Program_Line_Item_Pathway__c where Id =: strPathway];
         PDF_Heading_1=pdfLnItmPway.PDF_Heading_1__c ;           
         PDF_Heading_2=pdfLnItmPway.PDF_Heading_2__c ;
         Permit_PDF_Heading_1=pdfLnItmPway.Permit_PDF_Heading_1__c;
         Permit_PDF_Heading_2=pdfLnItmPway.Permit_PDF_Heading_2__c;
         tblColumns = new List<TableCol>();        

         tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c,pdfLnItmPway.Column_2_Display_Name__c,pdfLnItmPway.Column_3_Display_Name__c,pdfLnItmPway.Column_4_Display_Name__c)); 
         fieldValueMap = new map<string,String>();
         if(pdfLnItmPway.Column_1_Display_Name__c!=null)
         fieldValueMap.put(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c);
          if(pdfLnItmPway.Column_2_Display_Name__c!=null)
         fieldValueMap.put(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c);
          if(pdfLnItmPway.Column_3_Display_Name__c!=null)
         fieldValueMap.put(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c);
          if(pdfLnItmPway.Column_4_Display_Name__c!=null)
         fieldValueMap.put(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c);
          if(pdfLnItmPway.Column_5_Display_Name__c!=null)
         fieldValueMap.put(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c);
         system.debug('-----Map'+fieldValueMap);
         
         
         tblColumns1 = new List<TableCol>(); 
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
         if(pdfLnItmPway.Column_6_Display_Name__c!=null)
         tblColumns1.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c)); 
         system.debug('tblColumns = '+tblColumns);
         
         return tblColumns;     
                               
    }
    
    public map<string,list<AC__c>> getACRecords1(){
     ///groupLineItemMap = new Map<String,list<ac__c>>();
        list<AC__C> acNewlist = new list<ac__C>() ;
         Map<String,List<ac__C>> groupLineItemMap  = new Map<String,List<ac__C>>();
         totalNumberThisPermitAuthorizes = new Map<String, Decimal>();
         Decimal totalQty = 0.0;
         Decimal lineItmQty = 0.0;
         if(groupList.size()>0){         
           
            For(ac__C ois : aclst){ 
                system.debug('----outside'+ois );
                if(ois.Quantity__c != null)
                    lineItmQty = ois.Quantity__c;
                if(groupLineItemMap.containsKey(ois.Authorization_Group_String__c))
                {   system.debug('----ifside'+ois ); 
                    //Calculating total number of line item quantity for permit pdf - Subbu
                    totalQty = lineItmQty + totalNumberThisPermitAuthorizes.get(ois.Authorization_Group_String__c);
                    totalNumberThisPermitAuthorizes.remove(ois.Authorization_Group_String__c);
                    totalNumberThisPermitAuthorizes.put(ois.Authorization_Group_String__c, totalQty);

                    List<ac__C> lstoi = groupLineItemMap.get(ois.Authorization_Group_String__c);
                    lstoi.add(ois);

                    /*groupLineItemMap.remove(ois.Authorization_Group_String__c);
                    groupLineItemMap.put(ois.Authorization_Group_String__c,lstoi);*/
                }
                else
                {
                    system.debug('----elseside'+ois );
                    List<ac__C> lstoi = new List<ac__C>();
                    lstoi.add(ois);
                    groupLineItemMap.put(ois.Authorization_Group_String__c,lstoi);
                    system.debug('--------InsideElsegroupLineItemMap'+groupLineItemMap);
                    
                    //Calculating total number of line item quantity for permit pdf - Subbu
                    totalNumberThisPermitAuthorizes.put(ois.Authorization_Group_String__c, lineItmQty);

                }
                system.debug('--------groupLineItemMap'+groupLineItemMap);                    
            }  
             
         system.debug('--------groupLineItemMap'+groupLineItemMap);
         }
        
        system.debug('--------groupLineItemMapfinal'+groupLineItemMap);
        system.debug('--------totalNumberThisPermitAuthorizes'+totalNumberThisPermitAuthorizes);
       return groupLineItemMap;
    }

    public String getLineItemString(){
        
        try{
        
        htmlText_vet='';
         htmlText_vet+= '<td colspan = "3" style = "font-size:11.5px;border-bottom: thick solid black ">  ';
             htmlText_vet+='13. Address of Port Veterinarian at';    
                htmlText_vet+=  ' <br/><br/>';
        
        if(facVetJnObj.size()>0)
        {
              htmlText_vet+=facVetJnObj[0].Vet_Services__r.Name         +' <br/>' ;
              htmlText_vet+=  facVetJnObj[0].Vet_Services__r.ShippingStreet          +' <br/>' ;
              htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingCity+',' +facVetJnObj[0].Vet_Services__r.ShippingState+' <br/>' ;
              htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingPostalCode +' <br/>' ;
              htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingCountry +' <br/>' ;
              htmlText_vet+=facVetJnObj[0].Vet_Services__r.Phone+'/'+facVetJnObj[0].Vet_Services__r.Fax  +' <br/>' ;
              
        }
              htmlText_vet+=                '</td>';
              htmlText_vet+=           '<td colspan = "2" style = "font-size:11.5px;border-bottom: thick solid black;border-right: thin solid black">';
              htmlText_vet+=              '<br/><br/>';
              
         if(facVetJnObj.size()>0)
         {
              htmlText_vet+=facVetJnObj[1].Vet_Services__r.Name       +' <br/>' ;
              htmlText_vet+=              facVetJnObj[1].Vet_Services__r.ShippingStreet+          +' <br/>' ;
              htmlText_vet+=                facVetJnObj[1].Vet_Services__r.ShippingCity +',' 
             +facVetJnObj[1].Vet_Services__r.ShippingState+' <br/>' ;
              htmlText_vet+=                facVetJnObj[1].Vet_Services__r.ShippingPostalCode  +' <br/>' ;
              htmlText_vet+=                facVetJnObj[1].Vet_Services__r.ShippingCountry  +' <br/>' ;
              htmlText_vet+=facVetJnObj[1].Vet_Services__r.Phone+'/'+facVetJnObj[0].Vet_Services__r.Fax  +' <br/>' ;
        }
         
 }
catch (exception e)
{
     htmlText_vet='';
         htmlText_vet+= '<td colspan = "3" style = "font-size:11.5px;border-bottom: thick solid black ">  ';
             htmlText_vet+='13. Address of Port Veterinarian at'; 
                htmlText_vet+=  ' <br/><br/>';
        htmlText_vet+=facVetJnObj[0].Vet_Services__r.Name         +' <br/>' ;
              htmlText_vet+=  facVetJnObj[0].Vet_Services__r.ShippingStreet          +' <br/>' ;
           htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingCity+',' +facVetJnObj[0].Vet_Services__r.ShippingState+' <br/>' ;
           htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingPostalCode +' <br/>' ;
           htmlText_vet+=     facVetJnObj[0].Vet_Services__r.ShippingCountry +' <br/>' ;
                htmlText_vet+=facVetJnObj[0].Vet_Services__r.Phone+'/'+facVetJnObj[0].Vet_Services__r.Fax  +' <br/>' ;
    htmlText_vet+=                '</td>';
        htmlText_vet+=           '<td colspan = "2" style = "font-size:11.5px;border-bottom: thick solid black;border-right: thin solid black">';
         htmlText_vet+=                '</td>';
}

        permitSummaryString = lineItemPathwayFields.Permit_Summary_Header__c;
        system.debug('$$$$$$$$$$$$$'+aclst);
        system.debug('$$$$$$$$$$$$$^^^^'+permitSummaryString);
        if(aclst != null){
            if(regDescriptionFirst100CharsOV != null && regDescriptionFirst100CharsOV .contains('{!listItem.Importer_First_Name__c}') )
               regDescriptionFirst100CharsOV = regDescriptionFirst100CharsOV.replace('{!listItem.Importer_First_Name__c}',aclst[0].Importer_First_Name__c+' '+aclst[0].Importer_Last_Name__r.Name);
               if(regDescriptionFirst100CharsOV != null && regDescriptionFirst100CharsOV .contains('{!listItem.Organization__c} ') )
              regDescriptionFirst100CharsOV = regDescriptionFirst100CharsOV.replace('{!listItem.Organization__c}',aclst[0].Company_Name__c);
            if(permitSummaryString != null && permitSummaryString.contains('{!listItem.Importer_First_Name__c}') )
                permitSummaryString = permitSummaryString.replace('{!listItem.Importer_First_Name__c}',aclst[0].Importer_First_Name__c+' '+aclst[0].Importer_Last_Name__r.Name);
            if(permitSummaryString != null && permitSummaryString.contains('{!category}') && aclst[0].Regulated_Article__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!category}',aclst[0].Regulated_Article__r.name);
            if(permitSummaryString != null && permitSummaryString.contains('{!countryOfOrigin}') && aclst[0].Country_Of_Origin__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!countryOfOrigin}',aclst[0].Country_Of_Origin__r.Name);
            if(permitSummaryString != null && permitSummaryString.contains('{!portOfEntry}') && aclst[0].Port_of_Entry__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!portOfEntry}',aclst[0].Port_of_Entry__r.Name);
            if(permitSummaryString != null && permitSummaryString.contains('{!destinationPort}') && aclst[0].Port_of_Entry__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!destinationPort}',aclst[0].Port_of_Entry__r.Name);
        }
        htmlText = '';
        htmlText2 = '';
        System.debug(' Line Item Size : '+aclst.size());
        for(AC__c sObj: aclst)
        {
            //counter++;
            //if(counter <= 2){
                htmlText += '<tr>';
                htmlText += '<td style="' + tdStyleEnd + '">';
                htmlText +=  getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_1_Information__c);
                if(lineItemPathwayFields.Section_6_Column_1_Information__c != null && lineItemPathwayFields.Section_6_Column_1_Information__c.contains('Quantity'))
                    htmlText += ' Number';
                htmlText += '</td>';
                htmlText += '<td style="' + tdStyleEnd + '">';
                htmlText +=  getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_2_Information__c);
                if(lineItemPathwayFields.Section_6_Column_3_Information__c != null)
                    htmlText += '/' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_3_Information__c);
                htmlText += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_4_Information__c);
                if(lineItemPathwayFields.Section_6_Column_5_Information_ID__c != null || lineItemPathwayFields.Section_6_Column_6_Information_ID__c != null ||
                    lineItemPathwayFields.Section_6_Column_7_Information_ID__c != null){
                    htmlText += ' - ID : ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_5_Information_ID__c);
                    htmlText += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_6_Information_ID__c);
                    htmlText += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_7_Information_ID__c);
                }                htmlText += '</td>';
                htmlText += '</tr>';
            //}
            /*if(counter > 2){
                htmlText2 += '<tr>';
                htmlText2 += '<td style="' + tdStyleEnd + '">';
                htmlText2 +=  getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_1_Information__c);
                if(lineItemPathwayFields.Section_6_Column_1_Information__c != null && lineItemPathwayFields.Section_6_Column_1_Information__c.contains('Quantity'))
                    htmlText2 += ' Number';
                htmlText2 += '</td>';
                htmlText2 += '<td style="' + tdStyleEnd + '">';
                htmlText2 +=  getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_2_Information__c);
                if(lineItemPathwayFields.Section_6_Column_3_Information__c != null)
                    htmlText2 += '/' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_3_Information__c);
                htmlText2 += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_4_Information__c);
                if(lineItemPathwayFields.Section_6_Column_5_Information_ID__c != null || lineItemPathwayFields.Section_6_Column_6_Information_ID__c != null ||
                    lineItemPathwayFields.Section_6_Column_7_Information_ID__c != null){
                    htmlText2 += ' - ID : ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_5_Information_ID__c);
                    htmlText2 += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_6_Information_ID__c);
                    htmlText2 += ' ' + getFieldValue(sObj,lineItemPathwayFields.Section_6_Column_7_Information_ID__c);
                }                
                htmlText2 += '</td>';
                htmlText2 += '</tr>';
            }*/
        }
        return htmlText;
    }

    public static Object getFieldValue(SObject o,String field){
        if(o == null){
            return null;
        }
        if(field != null){
            if(field == 'N/A'){
                return 'N/A';
            }
            if(field.contains('.')){
                String nextField = field.substringAfter('.');
                String relation = field.substringBefore('.');
                return CARPOL_VS_AttachLineItemPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
            }else{
                return o.get(field);
            }
        } else {
            return '';
        }
    }
    
     public map<string,list<AC__c>> getACRecords2(){
     ///groupLineItemMap = new Map<String,list<ac__c>>();
        list<AC__C> acNewlist = new list<ac__C>() ;
         Map<String,List<ac__C>> groupLineItemMap  = new Map<String,List<ac__C>>();
         
         if(groupList.size()>0){    
           
            For(ac__C ois : aclst){ 
            system.debug('----outside'+ois );
                if(groupLineItemMap.containsKey(ois.Authorization_Group_String__c))
                {   system.debug('----ifside'+ois ); 
                    List<ac__C> lstoi = groupLineItemMap.get(ois.Authorization_Group_String__c);
                    lstoi.add(ois);
                }
                else
                {
                    system.debug('----elseside'+ois );
                    List<ac__C> lstoi = new List<ac__C>();
                    lstoi.add(ois);
                    groupLineItemMap.put(ois.Authorization_Group_String__c,lstoi);
                    system.debug('--------InsideElsegroupLineItemMap'+groupLineItemMap);
                    
                }
                system.debug('--------groupLineItemMap'+groupLineItemMap);                    
            }  
             
         system.debug('--------groupLineItemMap'+groupLineItemMap);
         }        
        system.debug('--------groupLineItemMapfinal'+groupLineItemMap);
       //find out list contains more than 4 items
       groupLineItemMapfirst4=new map<String,list<ac__c>>();
       groupLineItemMapnext=new map<String,list<ac__c>>();
       set<string> groupset = new set<string>();
       groupset = groupLineItemMap.keySet();
       List<ac__C> lineitemsList1 ;
       List<ac__C> lineitemsList2;
       ac__C ac1;
       
       for(String st: groupset){
       integer i=0; 
       lineitemsList1 =  new List<ac__C>();
       lineitemsList2 = new List<ac__C>();
       system.debug('--------groupLineItemMap.get(st).size()'+groupLineItemMap.get(st).size());
      // if (groupLineItemMap.get(st).size() <= 3){
         // groupLineItemMapfirst4.put(st,groupLineItemMap.get(st)) ; 
       //}
       //else
           for(ac__C ac: groupLineItemMap.get(st)){
               i=i+1;
               if(i<=4){
                  lineitemsList1.add(ac);
               }
               if(i>4){
                   lineitemsList2.add(ac); 
               }
           }
          
            groupLineItemMapfirst4.put(st,lineitemsList1);
            if(i>4) groupLineItemMapnext.put(st,lineitemsList2);
            
             system.debug('------groupLineItemMapfirst4'+groupLineItemMapfirst4);
             system.debug('------groupLineItemMapnext'+groupLineItemMapnext);
       }
    
       
       //return groupLineItemMapfirst4;
       return groupLineItemMap;
       
       
    }
    
    public Boolean isInMap=false;
    /*(string key){
        return groupLineItemMapnext.containskey(key);
        
    }*/
    
    
    
    
    public List<AC__c> getACRecords(){
    
    
    
        if(auth.Id != null){
            aclst = [select ID,Program_Line_Item_Pathway__c, Exporter_First_Name__c,Exporter_Last_Name__r.Name, 
            Exporter_Mailing_CountryLU__r.Name, Exporter_Mailing_Street__c, Exporter_Mailing_City__c, 
            Exporter_Mailing_State_ProvinceLU__r.Name, Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,
            Exporter_Fax__c, Exporter_Email__c, Importer_First_Name__c, Importer_Last_Name__r.Name,Company_Name__c,
            Importer_Mailing_CountryLU__r.Name, Importer_Mailing_Street__c, Importer_Mailing_City__c,
            Importer_Mailing_State_ProvinceLU__r.Name, Importer_Phone__c, Importer_Mailing_ZIP_code__c,
            Importer_Fax__c, Importer_Email__c, Importer_USDA_Registration_Number__c,
            Importer_USDA_Registration_Exp_Date__c, Importer_USDA_License_Number__c, 
            Importer_USDA_License_Expiration_Date__c, Purpose_of_the_Importation__c,
            Breed_Name__c, Sex__c, Age_in_months__c, Color__c, Other_identifying_information__c, 
            Transporter_Type__c, Port_of_Embarkation__r.Name, Transporter_Name__c,
            Air_Transporter_Flight_Number__c, Arrival_Time__c, Departure_Time__c,Intended_Use__c,Intended_Use__r.Name,
            USDA_Registration_Number__c, USDA_Expiration_Date__c, USDA_License_Number__c,
            USDA_License_Expiration_Date__c, Port_of_Entry__r.Name, Port_of_Entry__r.Address_1__c, Port_of_Entry__r.Address_2__c,
            Port_of_Entry__r.City__c, Port_of_Entry__r.State_Code__c, Port_of_Entry__r.Zip__c, Port_of_Entry__r.Country__r.Name,
            Port_of_Entry__r.Office_Phone__c, Proposed_date_of_arrival__c,
            DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__r.Name, RA_Scientific_Name__r.Name, Common_Name__c,
            DeliveryRecipient_Mailing_CountryLU__r.Name, DeliveryRecipient_Mailing_Street__c, 
            DeliveryRecipient_Mailing_City__c, Delivery_Recipient_State_ProvinceLU__r.Name, DeliveryRecipient_Mailing_Zip__c,
            DeliveryRecipient_Phone__c, DeliveryRecipient_Fax__c, DeliveryRecipient_Email__c,
            DeliveryRecipient_USDA_Registration_Num__c, DeliveryRec_USDA_Regist_Expiration_Date__c,
            DeliveryRecipient_USDA_License_Number__c, DeliveryRec_USDA_License_Expiration_Date__c,
            Program_Line_Item_Pathway__r.name,Registration_Number__c,Regulated_Article__r.name,name,Authorization_Group_String__c,Quantity__c, 
            Permit_Expiration_Date__c, Proposed_Shipping_Date__c, Gender__c, Age_yr_months__c, Age__c, 
            Microchip_Number__c, Tattoo_Number__c, Ear_Tag__c, Brand__c, Country_Of_Origin__r.Name,
            Scientific_Name__c, Scientific_Name__r.Name ,Component__c, Component__r.Name
            from AC__C  where Application_Number__c = :authList[0].Application__c];
            if(aclst.size()>0)
            strPathway=aclst[0].Program_Line_Item_Pathway__c;
            if(aclst.size() > 2)
                displayStringFlag = true;
        }
        groupList= new set<String>();
        for(AC__C  gpSt: aclst ){
            groupList.add(gpSt.Authorization_Group_String__c);
        }
        system.debug('--------groupList'+groupList);
        if(groupList.size()==1){
            oneLineItem = new list<AC__C>();
            oneLineItem = [select ID, Exporter_First_Name__c,Exporter_Last_Name__r.Name, 
            Exporter_Mailing_CountryLU__r.Name, Exporter_Mailing_Street__c, Exporter_Mailing_City__c, 
            Exporter_Mailing_State_ProvinceLU__r.Name, Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,
            Exporter_Fax__c, Exporter_Email__c, Importer_First_Name__c, Importer_Last_Name__r.Name,Company_Name__c,
            Importer_Mailing_CountryLU__r.Name, Importer_Mailing_Street__c, Importer_Mailing_City__c,
            Importer_Mailing_State_ProvinceLU__r.Name, Importer_Phone__c, Importer_Mailing_ZIP_code__c,
            Importer_Fax__c, Importer_Email__c, Importer_USDA_Registration_Number__c,
            Importer_USDA_Registration_Exp_Date__c, Importer_USDA_License_Number__c, 
            Importer_USDA_License_Expiration_Date__c, Purpose_of_the_Importation__c,
            Breed_Name__c, Sex__c, Age_in_months__c, Color__c, Other_identifying_information__c, 
            Transporter_Type__c, Port_of_Embarkation__r.Name, Transporter_Name__c,
            Air_Transporter_Flight_Number__c, Arrival_Time__c, Departure_Time__c,Quantity__c,
            USDA_Registration_Number__c, USDA_Expiration_Date__c, USDA_License_Number__c, Intended_Use__c,Intended_Use__r.Name,
            USDA_License_Expiration_Date__c, Port_of_Entry__r.Name, Proposed_date_of_arrival__c,
            DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__r.Name, Quarantine_Facility__c,
            DeliveryRecipient_Mailing_CountryLU__r.Name, DeliveryRecipient_Mailing_Street__c, 
            DeliveryRecipient_Mailing_City__c, Delivery_Recipient_State_ProvinceLU__r.Name, DeliveryRecipient_Mailing_Zip__c,
            DeliveryRecipient_Phone__c, DeliveryRecipient_Fax__c, DeliveryRecipient_Email__c,
            DeliveryRecipient_USDA_Registration_Num__c, DeliveryRec_USDA_Regist_Expiration_Date__c,
            DeliveryRecipient_USDA_License_Number__c, DeliveryRec_USDA_License_Expiration_Date__c,Method_of_Transport__c,
            Program_Line_Item_Pathway__r.name,Registration_Number__c,Regulated_Article__r.name,name,Authorization_Group_String__c, 
            Permit_Expiration_Date__c, Proposed_Shipping_Date__c, Gender__c,Age_yr_months__c,
            Microchip_Number__c, Tattoo_Number__c, Ear_Tag__c, Brand__c, Country_Of_Origin__r.Name ,
            Scientific_Name__c, Scientific_Name__r.Name,Component__c, Component__r.Name
            from AC__C  where Application_Number__c = :authList[0].Application__c LIMIT 1];        
        
        }
        
        ///groupLineItemMap = new Map<String,list<ac__c>>();
        list<AC__C> acNewlist = new list<ac__C>() ;
         Map<String,List<ac__C>> groupLineItemMap  = new Map<String,List<ac__C>>();
         if(groupList.size()>0){         
           
            For(ac__C ois : aclst){ 
            system.debug('----outside'+ois );
                if(groupLineItemMap .containsKey(ois.Authorization_Group_String__c))
                {   system.debug('----ifside'+ois ); 
                    List<ac__C> lstoi = groupLineItemMap.get(ois.Authorization_Group_String__c);
                    lstoi.add(ois);
                }
                else
                {
                    system.debug('----elseside'+ois );
                    List<ac__C> lstoi = new List<ac__C>();
                    lstoi.add(ois);
                    groupLineItemMap.put(ois.Authorization_Group_String__c,lstoi);
                    system.debug('--------InsideElsegroupLineItemMap'+groupLineItemMap);
                    
                }
                system.debug('--------groupLineItemMap'+groupLineItemMap);                    
            }  
             
         system.debug('--------groupLineItemMap'+groupLineItemMap);
         }
        
        system.debug('--------groupLineItemMapfinal'+groupLineItemMap);
        
        return aclst;
    }

    public void savePDF(){
        if(auth.Id != null){
            //System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
            PageReference pdf = Page.CARPOL_VS_AuthorizationPDF;
            pdf.getParameters().put('id', auth.Id);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                }
            att.Name = 'Permit_'+appln.Name+'.pdf'; 
            att.ParentId = auth.Id;
            att.body = fileBody;
            insert att;
        }
    }
    
     public void getRegulation()
    {
        regulationDescriptionToBePrinted = '';
        regDescriptionFirst100Chars = '';
        regDescriptionFirst100CharsOV = '';
        regulationDescriptionToBePrinted = '';

    try{
    scj = new List<Authorization_Junction__c>([Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :auth.Id ORDER BY Order_Number__c ASC]);
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
    scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        scjC = new List<Authorization_Junction__c>();
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' && SCJ.Is_Active__c=='Yes' ){
                    scjA.add(SCJ);
                    //{!listItem.Importer_First_Name__c}
                    //tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', tp.To_Level_1_Region__r.Name);
                    /*
                    
                    if(regulationDescriptionToBePrinted != null && regulationDescriptionToBePrinted.contains('{!listItem.Importer_First_Name__c}'))
                       regulationDescriptionToBePrinted = regulationDescriptionToBePrinted.replace('{!listItem.Importer_First_Name__c}',authList[0].Application__r.Applicant_Name__r.name);
                    
                    if(permitSummaryString != null && permitSummaryString.contains('{!category}') && aclst[0].Regulated_Article__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!category}',aclst[0].Regulated_Article__r.name);
            if(permitSummaryString != null && permitSummaryString.contains('{!countryOfOrigin}') && aclst[0].Country__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!countryOfOrigin}',aclst[0].Country__r.Name);
            if(permitSummaryString != null && permitSummaryString.contains('{!portOfEntry}') && aclst[0].Port_of_Entry__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!portOfEntry}',aclst[0].Port_of_Entry__r.Name);
            if(permitSummaryString != null && permitSummaryString.contains('{!destinationPort}') && aclst[0].Port_of_Entry__r.Name != null)
                permitSummaryString = permitSummaryString.replace('{!destinationPort}',aclst[0].Port_of_Entry__r.Name);
        
                    */
                    
                  aclst=[select id,name,Company_Name__c,Application_Number__c from AC__c where Application_Number__c=:authList[0].Application__c limit 1];  
                    
                    regulationDescriptionToBePrinted = regulationDescriptionToBePrinted + SCJ.Regulation_Description__c;
                    if(regulationDescriptionToBePrinted != null && regulationDescriptionToBePrinted.contains('{!listItem.Importer_First_Name__c}'))
                       regulationDescriptionToBePrinted = regulationDescriptionToBePrinted.replace('{!listItem.Importer_First_Name__c}',authList[0].Application__r.Applicant_Name__r.name);
                    if(regulationDescriptionToBePrinted != null && regulationDescriptionToBePrinted.contains('{!listItem.Organization__c}'))
                       regulationDescriptionToBePrinted = regulationDescriptionToBePrinted.replace('{!listItem.Organization__c}',aclst[0].Company_Name__c);
                    
                    
                }
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){
                    scjB.add(SCJ);
                    regulationDescriptionToBePrinted = regulationDescriptionToBePrinted + SCJ.Regulation__r.Regulation_Description__c;
                }
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Instruction for CBP Officers'){
                    scjC.add(SCJ);
                    regulationDescriptionToBePrinted = regulationDescriptionToBePrinted + SCJ.Regulation__r.Regulation_Description__c;
                }
                System.Debug('<<<<<<< Total IO : ' + scjc.size() + ' >>>>>>>');
            }
        }
            Integer midPt;
            Boolean breakFlag = true;
 System.Debug('<<<<<<< regulationDescriptionToBePrinted size : ' + regulationDescriptionToBePrinted.length() + ' >>>>>>>');
             if(regulationDescriptionToBePrinted.length() != 0){
                if(regulationDescriptionToBePrinted.length() < 600){
                    midPt = regulationDescriptionToBePrinted.length();
                }else{
                    midPt = 600;
                }
                System.Debug('<<<<<<< breakFlag' + breakFlag + ' >>>>>>>');
                 while(breakFlag){
                 System.Debug('<<<<<<< regulationDescriptionToBePrinted.subString(midPt-1,midPt) >>>>' + regulationDescriptionToBePrinted.subString(midPt-1,midPt).equals(' ') );
                    if(regulationDescriptionToBePrinted.subString(midPt-1,midPt).equals(' ')){
                       breakFlag = false; 
                    } else {
                        midPt = midPt + 1;
                    }
                }
                regDescriptionFirst100Chars = regulationDescriptionToBePrinted.mid(0,midPt);
                regDescriptionFirst100CharsOV = regulationDescriptionToBePrinted.mid(0,3750);
                system.debug('&&&&&&&&&&&&'+regDescriptionFirst100CharsOV);
                system.debug('&&&&&&&&&&&&'+aclst);
                if(aclst != null){
                    if(regDescriptionFirst100CharsOV != null && regDescriptionFirst100CharsOV .contains('{!listItem.Importer_First_Name__c}') )
                        regDescriptionFirst100CharsOV = regDescriptionFirst100CharsOV.replace('{!listItem.Importer_First_Name__c}',aclst[0].Importer_First_Name__c+' '+aclst[0].Importer_Last_Name__r.Name);
                        if(regDescriptionFirst100CharsOV != null && regDescriptionFirst100CharsOV .contains('{!listItem.Organization__c}') )
                        regDescriptionFirst100CharsOV = regDescriptionFirst100CharsOV.replace('{!listItem.Organization__c}', aclst[0].Company_Name__c);
              
               } 
                regulationDescriptionToBePrinted = regulationDescriptionToBePrinted.mid(midPt, regulationDescriptionToBePrinted.length());
                regulationDescriptionToBePrintedOV = regulationDescriptionToBePrinted.mid(2750, regulationDescriptionToBePrinted.length());
            }
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }

    }
    
     public Class TableCol
    {
        public String Heading1{get;set;}       
        public String Heading2{get;set;}
        public String Heading3{get;set;}
        public String Heading4{get;set;}
        
        public String Heading{get;set;} 
        public String APIName{get;set;}
        public String sortOrder{get;set;}
        

        public TableCol(String h1, String h2,String h3, String h4)
        {
            Heading1 = h1;
            Heading2= h2;
            Heading3 = h3;
            Heading4 = h4; 
            
        }
        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }
    
}