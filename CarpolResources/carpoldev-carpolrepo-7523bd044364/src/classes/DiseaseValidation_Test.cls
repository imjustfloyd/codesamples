@isTest(seealldata=false)
private class DiseaseValidation_Test {
      @IsTest static void DiseaseValidation_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
         
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Is_Country_Required__c = true;
          plip.Is_State_Required__c = true;
          update plip;
          
          Intended_Use__c iu = testData.newIntendedUse(plip.id);
          Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
          Group__c grp = testData.newgroup();
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          insert l2;
          
          Disease__c dis = new Disease__c();        
          dis.Condition_Group__c = grp.id;
          dis.IsActive__c = true;
          dis.RecordTypeId = Schema.SObjectType.Disease__c.getRecordTypeInfosByName().get('Disease').getRecordTypeId();
          dis.Abbreviation__c = 'EX';
          insert dis;
          
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.Country__c = c.id;
          objdm.Level_1_Region__c = lr.id;
          objdm.Level_2_Region__c = l2.id;
          objdm.Condition__c = dis.id;
          objdm.Disease_Statys__c = 'Affected'; 
          objdm.Intended_Use__c = iu.id;
          objdm.Regulated_Article__c = regart.id;
          insert objdm;


          Test.startTest();

          DiseaseValidation extclass = new DiseaseValidation();
          extclass.checkForDisease(c.id,lr.id,l2.id,plip.id,'Personal Use','Test article');

          DiseaseValidation extclass1 = new DiseaseValidation();
          extclass1.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use','Test article');          

          
          DiseaseValidation extclass2 = new DiseaseValidation();
          extclass2.checkForDisease(c.id,lr.id,null,plip.id,null,'Test article');          

                      
          DiseaseValidation extclass3 = new DiseaseValidation();
          extclass3.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use',null);          
          system.assert(extclass != null);                                       
          Test.stopTest();                                   
      }
      @IsTest static void DiseaseValidation_Test2() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
         
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Is_Country_Required__c = false;
          plip.Is_State_Required__c = false;
          update plip;
          
          Intended_Use__c iu = testData.newIntendedUse(plip.id);
          Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
          Group__c grp = testData.newgroup();
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          insert l2;
          
          Disease__c dis = new Disease__c();        
          dis.Condition_Group__c = grp.id;
          dis.IsActive__c = true;
          dis.RecordTypeId = Schema.SObjectType.Disease__c.getRecordTypeInfosByName().get('Disease').getRecordTypeId();
          dis.Abbreviation__c = 'EX';
          insert dis;
          
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.Country__c = c.id;
          objdm.Level_1_Region__c = lr.id;
          objdm.Level_2_Region__c = l2.id;
          objdm.Condition__c = dis.id;
          objdm.Disease_Statys__c = 'Affected'; 
          objdm.Intended_Use__c = iu.id;
          objdm.Regulated_Article__c = regart.id;
          insert objdm;


          Test.startTest();

          DiseaseValidation extclass = new DiseaseValidation();
          extclass.checkForDisease(c.id,lr.id,l2.id,plip.id,'Personal Use','Test article');

          DiseaseValidation extclass1 = new DiseaseValidation();
          extclass1.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use','Test article');          

          
          DiseaseValidation extclass2 = new DiseaseValidation();
          extclass2.checkForDisease(c.id,lr.id,null,plip.id,null,'Test article');          

          DiseaseValidation extclass3 = new DiseaseValidation();
          extclass3.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use',null);          
          system.assert(extclass != null);                                       
          Test.stopTest();                         
      }  
      @IsTest static void DiseaseValidation_Test3() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
         
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Is_Country_Required__c = true;
          plip.Is_State_Required__c = false;
          update plip;
          
          Intended_Use__c iu = testData.newIntendedUse(plip.id);
          Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
          Group__c grp = testData.newgroup();
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          insert l2;
          
          Disease__c dis = new Disease__c();        
          dis.Condition_Group__c = grp.id;
          dis.IsActive__c = true;
          dis.RecordTypeId = Schema.SObjectType.Disease__c.getRecordTypeInfosByName().get('Disease').getRecordTypeId();
          dis.Abbreviation__c = 'EX';
          insert dis;
          
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.Country__c = c.id;
          objdm.Level_1_Region__c = lr.id;
          objdm.Level_2_Region__c = l2.id;
          objdm.Condition__c = dis.id;
          objdm.Disease_Statys__c = 'Affected'; 
          objdm.Intended_Use__c = iu.id;
          objdm.Regulated_Article__c = regart.id;
          insert objdm;


          Test.startTest();

          DiseaseValidation extclass = new DiseaseValidation();
          extclass.checkForDisease(c.id,lr.id,l2.id,plip.id,'Personal Use','Test article');

          DiseaseValidation extclass1 = new DiseaseValidation();
          extclass1.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use','Test article');          

          
          DiseaseValidation extclass2 = new DiseaseValidation();
          extclass2.checkForDisease(c.id,lr.id,null,plip.id,null,'Test article');          

          DiseaseValidation extclass3 = new DiseaseValidation();
          extclass3.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use',null);  
          system.assert(extclass != null);                                               
          Test.stopTest();                         
      }  
      @IsTest static void DiseaseValidation_Test4() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
         
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Is_Country_Required__c = false;
          plip.Is_State_Required__c = true;
          update plip;
          
          Intended_Use__c iu = testData.newIntendedUse(plip.id);
          Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
          Group__c grp = testData.newgroup();
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          insert l2;
          
          Disease__c dis = new Disease__c();        
          dis.Condition_Group__c = grp.id;
          dis.IsActive__c = true;
          dis.RecordTypeId = Schema.SObjectType.Disease__c.getRecordTypeInfosByName().get('Disease').getRecordTypeId();
          dis.Abbreviation__c = 'EX';
          insert dis;
          
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          objdm.Country__c = c.id;
          objdm.Level_1_Region__c = lr.id;
          objdm.Level_2_Region__c = l2.id;
          objdm.Condition__c = dis.id;
          objdm.Disease_Statys__c = 'Affected'; 
          objdm.Intended_Use__c = iu.id;
          objdm.Regulated_Article__c = regart.id;
          insert objdm;


          Test.startTest();

          DiseaseValidation extclass = new DiseaseValidation();
          extclass.checkForDisease(c.id,lr.id,l2.id,plip.id,'Personal Use','Test article');

          DiseaseValidation extclass1 = new DiseaseValidation();
          extclass1.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use','Test article');          

          
          DiseaseValidation extclass2 = new DiseaseValidation();
          extclass2.checkForDisease(c.id,lr.id,null,plip.id,null,'Test article');          

          DiseaseValidation extclass3 = new DiseaseValidation();
          extclass3.checkForDisease(c.id,lr.id,null,plip.id,'Personal Use',null);  
          system.assert(extclass != null);                                               
          Test.stopTest();                         
      }                  
}