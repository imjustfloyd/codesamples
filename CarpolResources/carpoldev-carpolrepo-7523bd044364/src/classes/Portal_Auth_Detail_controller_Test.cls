@isTest(seealldata=false)
private class Portal_Auth_Detail_controller_Test {
      @IsTest static void Portal_Authorization_Detail_controller_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Authorizations__c objauth = testData.newAuth(objapp.id);
          Program_Line_Item_Section__c mSection1 = new Program_Line_Item_Section__c();
          mSection1.Section_Order__c = 1;
          mSection1.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection1.Destination_Object__c = 'Authorization';
         
          insert mSection1;
          Program_Line_Item_Section__c mSection2 = new Program_Line_Item_Section__c();
          mSection2.Section_Order__c = 1;
          mSection2.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection2.Destination_Object__c = 'Line Item';
          insert mSection2;
          
          Program_Line_Item_Field__c mField1 = new Program_Line_Item_Field__c();
          mField1.isActive__c = 'Yes';
          mField1.Field_API_Name__c = 'ID';
          mField1.Name = 'Test';
          mField1.Program_Line_Item_Section__c = mSection1.id;
          insert mField1;
        
          Program_Line_Item_Field__c mField2 = new Program_Line_Item_Field__c();
          mField2.isActive__c = 'Yes';
          mField2.Field_API_Name__c = 'ID';
          mField2.Name = 'Test';
          mField2.Program_Line_Item_Section__c = mSection2.id;
          insert mField2;
        
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.Portal_Authorization_Detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.currentPage().getParameters().put('id',objauth.id);                                                                                                   
              Portal_Authorization_Detail_controller extclass = new Portal_Authorization_Detail_controller(sc);
              extclass.getDynamicPageBlock();
              extclass.objlineitem = objac;
              System.assert(extclass != null);        

              
          Test.stopTest();   
      }
}