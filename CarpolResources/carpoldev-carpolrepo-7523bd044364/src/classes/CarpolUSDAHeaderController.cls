public class CarpolUSDAHeaderController {

    public PageReference logout() {
        // Get the base URL for community salesforce instance
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('<<<<<<< Base URL: ' + baseURL + ' >>>>>>>');
        String communityURL = baseURL;
        
        /* Commented by Dinesh - 7/29
        if(test.isrunningtest()){
            baseURL=apexpages.currentpage().getparameters().get('baseURLTest');
        }
        
        //String communityURL;
        
       
       //PRODUCTION
        if(baseURL.contains('aphis-efile.force'))
        {communityURL = 'https://aphis-efile.force.com';}
// 7-19-16 VV added        
        //efiledev1
        if(baseURL.contains('efiledev1'))
        {communityURL = 'https://efiledev1-aphis-efile.cs32.force.com';}  

        //eFilMerge
        if(baseURL.contains('efilmerge'))
        {communityURL = 'https://efilMerge-aphis-efile.cs32.force.com';}  

        //eFileUAT
        if(baseURL.contains('eFileUAT'))
        {communityURL = 'https://efileuat-aphis-efile.cs32.force.com';}  
// 7-19-16 VV code End
        //efiledev1
        if(baseURL.contains('efiledev1'))
        {communityURL = 'https://efiledev1-aphis-efile.cs32.force.com';}  


        //T4 Dev
        if(baseURL.contains('t4dev'))
        {communityURL = 'https://t4dev-aphis-efile.cs33.force.com';}  

        //T3 Dev
        if(baseURL.contains('t3dev'))
        {communityURL = 'https://t3dev-aphis-efile.cs33.force.com';}        
        
        //UAT
        if(baseURL.contains('efileuat'))
        {communityURL = 'https://efileuat-aphis-efile.cs32.force.com';}
        
        //Dev
        if(baseURL.contains('carpoldev'))
        {communityURL = 'https://carpoldev-aphis--efilesystem.cs32.force.com';}
        
        //QA
        if(baseURL.contains('efileqa'))
        {communityURL = 'https://efileqa-aphis-efile.cs32.force.com';}
        
        //Demo
        if(baseURL.contains('efiledemo'))
        {communityURL = 'https://efiledemo-aphis-efile.cs32.force.com';}
        
        //T5
        if(baseURL.contains('t5dev'))
        {communityURL = 'https://t5dev-aphis-efile.cs33.force.com';}

        //District12 
        if(baseURL.contains('district12'))
        {communityURL = 'https://district12-aphis-efile.cs33.force.com';}          
        
        //efilemerge
        if(baseURL.contains('efilemerge'))
        {communityURL = 'https://efilemerge-aphis-efile.cs33.force.com';}
        //jabberjay
        if(baseURL.contains('jabberjay'))
        {communityURL = 'https://jabberjay-aphis-efile.cs33.force.com';}   
        //efilestage
        if(baseURL.contains('efilestage'))
        {communityURL = 'https://efilestage-aphis-efile.cs32.force.com';}        
        //Production
        //if(baseURL.contains('eFileUAT')) 
        //{communityURL = 'https://carpoldev-aphis--efilesystem.cs32.force.com/';}*/ //End Dinesh
        
        pagereference pagref=new pagereference(communityURL+'/secur/logout.jsp');
        system.debug('--->'+pagref);
        pagref.setredirect(true);
        return pagref;
    }

}