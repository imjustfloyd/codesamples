public class Portal_genotype_edit {
    public Genotype__c obj {get; set;}
    public ID genid {get;set;}
    public ID gentypeid {get;set;} 
    public ID constructid {get;set;}
    public ID conID;
    public ID recordTypeId {get;set;}
    public string recTypeName{get;set;}  
    public ID lineitemid {get; set;}
    public String usertype {get;set;}
    
    public Portal_genotype_edit(ApexPages.StandardController controller) {
                userType = UserInfo.getUserType();
                obj = (Genotype__c)controller.getRecord();
                obj.Related_Construct_Record_Number__c = ApexPages.currentPage().getParameters().get('constid');
                system.debug('obj.Construct__c'+obj.Related_Construct_Record_Number__c);
                gentypeid = ApexPages.currentPage().getParameters().get('genid');
                obj.RecordTypeId = ApexPages.CurrentPage().getParameters().get('rectype');
                if(obj.RecordTypeId == null)
                   obj.RecordTypeId = [select recordtypeid from genotype__c where id=:gentypeid].recordtypeid;
               
                  if(obj.Related_Construct_Record_Number__c != null)  
                    lineitemid = [select id, Line_Item__c from construct__c where id =: obj.Related_Construct_Record_Number__c].Line_Item__c;
                   
               recTypeName = CARPOL_UNI_RecordType.getObjectRecordTypeName(Genotype__c.SObjectType,obj.RecordTypeId);
                //obj.ID = gentypeid;
                List<Genotype__c> genlst = [SELECT Related_Construct_Record_Number__c,Construct_Component__c,Genotype__c,
                                             Recordtypeid, Construct_Component_Name__c, Description__c, Donor1__c, Donor4__c,
                                             Donor2__c, Record_Type_Text__c,Donor5__c, Donor3__c,donor_list__c FROM Genotype__c WHERE ID = :gentypeid];
                if(genlst.size()>0){
                    for(Genotype__c gen : genlst){
                        obj.Related_Construct_Record_Number__c = gen.Related_Construct_Record_Number__c;
                        obj.Construct_Component__c = gen.Construct_Component__c;
                        obj.Genotype__c = gen.Genotype__c;
                        obj.Recordtypeid = gen.Recordtypeid;
                        obj.Construct_Component_Name__c = gen.Construct_Component_Name__c;
                        obj.Description__c = gen.Description__c;
                        obj.Donor1__c = gen.Donor1__c;
                        obj.Donor4__c = gen.Donor4__c;
                        obj.Donor2__c = gen.Donor2__c;
                        obj.Donor5__c = gen.Donor5__c;
                        obj.Donor3__c = gen.Donor3__c;
                        obj.donor_list__c = gen.donor_list__c;
                    }
                }
                conID = obj.Related_Construct_Record_Number__c;
    }

    public pagereference updategentype(){
            gentypeid = ApexPages.currentPage().getParameters().get('genid');
            obj.RecordTypeId = ApexPages.currentPage().getParameters().get('rectype');
            obj.id = gentypeid;
            upsert obj;
    
            PageReference pg = Page.portal_genotype_detail;
            pg.getParameters().put('id',obj.id);
            pg.getParameters().put('rectype',obj.RecordTypeId);
            pg.setRedirect(true);
        return pg;
    }


    public pageReference ret(){
          
       PageReference pg = Page.portal_construct_detail;
       //pg.getParameters().put('strPathwayId',strPathwayId); 
       pg.getParameters().put('id',conId);
       pg.setRedirect(true);
       return pg;
    }
    public pagereference cancelgenotype(){
        gentypeid = ApexPages.currentPage().getParameters().get('genid');
        constructid = ApexPages.currentPage().getParameters().get('constid');
        obj.RecordTypeId = ApexPages.currentPage().getParameters().get('rectype');
        if(gentypeid != null){
            obj.id = gentypeid;
            PageReference pg = Page.portal_genotype_detail;
            pg.getParameters().put('id',obj.id);
            pg.getParameters().put('rectype',obj.RecordTypeId);
            pg.setRedirect(true);
            return pg;
        } else{
            PageReference pg = Page.portal_construct_detail;
            pg.getParameters().put('id',constructid);
            pg.setRedirect(true);
            return pg;
        }
    }
    
public List<Genotype__c> getgenotypes(){
return [SELECT ID,Name,Genotype__c,Construct_Component__c, Construct_Component_Name__c, Donor_list__c, Description__c, Related_Construct_Record_Number__c FROM Genotype__c WHERE Related_Construct_Record_Number__c =:conId and recordtypeid =: obj.RecordTypeId];
}     

}