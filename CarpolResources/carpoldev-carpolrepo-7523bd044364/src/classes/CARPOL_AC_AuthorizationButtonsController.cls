public with sharing class CARPOL_AC_AuthorizationButtonsController {
    public Authorizations__c au;
   // Private String authId;
    transient public String renderPermit {get; set;} 
    public String authID = ApexPages.currentPage().getParameters().get('id');
    public CARPOL_AC_AuthorizationButtonsController(ApexPages.StandardController Controller) {
      renderPermit = 'False';      
      au = [Select Id, Authorization_Type__c FROM Authorizations__c Where Id =:authId];
        IF(au.Authorization_Type__c == 'Permit')
        {
          renderPermit = 'True';  
       }
      // return renderPermit;
  }

       
    


}