@isTest(seealldata=true)
public class CARPOL_UNI_CreateReviewer_Email_Test{
    static testMethod void updateauthcreaterecords(){
        RecordType aRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Biotechnology Regulatory Services-Acknowledgement' LIMIT 1];
        system.debug(aRT);
        Authorizations__c auth = [select Id from Authorizations__c where recordtypeid  =: aRT.Id AND BRS_Create_State_Review_Records__c = false LIMIT 1];
        
        Account a = new Account();
        a.Name = 'Test Account';
        insert a;
        Test.startTest();
        RecordType cRT = [Select Id,Name, DeveloperName From RecordType Where DeveloperName = 'Contact' LIMIT 1];
        RecordType sproRT = [Select Id,Name, DeveloperName From RecordType WHERE Name = 'State SPRO' LIMIT 1];        
        Contact c = new Contact();
        //c.Name = 'Test Contact';
        c.FirstName = 'Global Contact'+'123';
        c.LastName = 'LastName'+'123';
        c.Email = '123test@email.com';        
        c.MailingStreet = 'Mailing Street'+'123';
        c.MailingCity = 'Mailing City'+'123';
        c.MailingState = 'Ohio';
        c.MailingCountry = 'United States';
        c.MailingPostalCode = '32092';    
        c.Phone = '12345';
        c.RecordTypeId = cRT.Id;
        c.AccountId = a.Id;
        insert c;

        Contact spro = new Contact();
        //c.Name = 'Test Contact';
        spro.FirstName = 'Global Contact'+'123';
        spro.LastName = 'LastName'+'123';
        spro.Email = '123test@email.com';        
        spro.MailingStreet = 'Mailing Street'+'123';
        spro.MailingCity = 'Mailing City'+'123';
        spro.MailingState = 'Ohio';
        spro.MailingCountry = 'United States';
        spro.MailingPostalCode = '32092';    
        spro.Phone = '12345';
        spro.RecordTypeId = sproRT.Id;
        spro.AccountId = a.Id;
        insert spro;        
        
        /*Reviewer__c r = new Reviewer__c();
        r.State_Regulatory_Official__c = c.Id;
        r.BRS_State_Reviewer_Email__c = 'brs@gmail.com';
        r.Authorization__c = auth.id;
        r.State_Regulatory_Official__c = spro.Id;
        insert r;
        
        List<Reviewer__c> rList = new List<Reviewer__c>();
        rList.add(r);
        */
 
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c ct = new country__c();
          ct.Name='Test';
          ct.country_code__c='12';
          ct.Trade_Agreement__c=ta.Id;
          insert ct;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=ct.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newauth(objapp.id);
          AC__c li = testData.newlineitem('Personal Use', objapp);
          li.Authorization__c = objauth.id;
          update li;
          
            Location__c objloc = new Location__c(Name='testloc', Country__c=ct.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = li.id
             );
            insert objloc;
            /*
            Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
            Regulated_Article__c regart = testData.newRegulatedArticle(plip.id);
            
            Link_Regulated_Articles__c objlra = new Link_Regulated_Articles__c();
            //objlra.Regulated_Article__c = regart.id;  
            objlra.Line_Item__c = ac1.id;
            objlra.Authorization__c = objauth.id;
            objlra.Application__c = objapp.id;
            objlra.Cultivar_and_or_Breeding_Line__c = 'Test';         
            insert objlra;
           */             
        
        PageReference pg = Page.CARPOL_UNI_CreateOfficialReviewerRecords;
        system.currentPageReference().getParameters().put('Id', auth.Id);
        system.currentPageReference().getParameters().put('rtype', 'spro');        
        //Test.setCurrentPage(pg);
                
        ApexPages.StandardController std=new ApexPages.StandardController(auth);
        CARPOL_UNI_Createreviewer_email core=new CARPOL_UNI_Createreviewer_email(std); 
        core.createReviewerrecords();
        //CARPOL_BRS_Createreviewer_email.createApplicantAttachment(rList);
        //core.SendEmailToReviewer();
        
        PageReference pg1 = Page.CARPOL_UNI_CreateOfficialReviewerRecords;
        system.currentPageReference().getParameters().put('Id', objauth.Id);
        system.currentPageReference().getParameters().put('rtype', 'spro');                
        //Test.setCurrentPage(pg);
                
        ApexPages.StandardController std1=new ApexPages.StandardController(objauth);
        CARPOL_UNI_Createreviewer_email core1=new CARPOL_UNI_Createreviewer_email(std1);         
        core.createReviewerrecords();
        //cannot call following method.  Trigger breaks because animal_care_ac__c is not being set
        //CARPOL_BRS_Createreviewer_email.createApplicantAttachment(rList);
        //core.SendEmailToReviewer();
        system.assert(core1 != null);
        Test.stopTest();
    }
}