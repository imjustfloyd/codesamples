@isTest(seealldata=false)
private class CARPOL_BRS_SelfReportingRecordType_Test {
      @IsTest static void CARPOL_BRS_SelfReportingRecordType_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Authorizations__c objauth = TestData.newauth(objapp.id);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Self_Reporting__c sr= new Self_Reporting__c();
          sr.Line_Events__c = objcaj.id;
          sr.Monitoring_Period_Start__c = date.today();
          sr.Monitoring_Period_End__c = date.today() + 60;
          sr.Observation_Date__c = date.today() + 61;
          sr.Number_of_Volunteers__c = 5;
          sr.Action_Taken__c = 'Test description';
          insert sr;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          System.runAs(usershare){
          //run as portal user
              PageReference pageRef = Page.portal_SelfReport_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
              ApexPages.currentPage().getParameters().put('AuthorizationId',objauth.id);
              ApexPages.currentPage().getParameters().put('Host','test');
              CARPOL_BRS_SelfReportingRecordType extclass = new CARPOL_BRS_SelfReportingRecordType(sc);                              
              extclass.getRecordTypes();
              extclass.continueAssociate();
          }
          //run as salesforce user
              PageReference pageRef = Page.portal_SelfReport_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
              ApexPages.currentPage().getParameters().put('AuthorizationId',objauth.id);
              ApexPages.currentPage().getParameters().put('Host','test');
              CARPOL_BRS_SelfReportingRecordType extclass = new CARPOL_BRS_SelfReportingRecordType(sc);                              
              extclass.getRecordTypes();
              extclass.continueAssociate();
              system.assert(extclass != null);
          Test.stopTest();   
      }
}