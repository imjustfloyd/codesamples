/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CARPOL_UNI_ADD_LOCALE_TRANSIT_Test {

    static testMethod void myUnitTest() { 
        
            String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
            CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
            Application__c  newApp =dataObj.newapplication();
            AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
            test.startTest();
            Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_ADD_LOCALE_TRANSIT')); 
            System.currentPageReference().getParameters().put('LineItemId',newLine.Id);
            CARPOL_UNI_ADD_LOCALE_TRANSIT obj = new CARPOL_UNI_ADD_LOCALE_TRANSIT();
            obj.CancelTransitLocal();
            obj.SaveTransitLocal();
            obj.Add();
            obj.addMore();
            obj.deleteTransit();
            obj.removeTransit();
            obj.removeMore();
            system.assert(obj != null);
            //obj.addMore();
        test.stopTest();
    }   
}