public with sharing class FacilityTriggerHandler {

	public void updateTimeZone(Map<Id, Facility__c> mapFacility){
//		CARPOL_AC_GooglePopulateTimeZone apiCall = new CARPOL_AC_GooglePopulateTimeZone(mapFacility);
	
	if (CARPOL_AC_GooglePopulateTimeZone.canIRun() == true){
		List<Id> lstFacility = new List<id>();
    List<Facility__c> lstFacToUpdate = new List<Facility__c>();
      
      for(Facility__c facility : mapFacility.values()){
      	
      	system.debug('map facility in trigger handler ' + mapFacility.values());
      	system.debug('facility in trigger handler ' + facility);
      	lstFacility.add(facility.id);
      	
      
      }
       
      CARPOL_AC_GooglePopulateTimeZone.CARPOL_AC_CallGoogleGeoAddress(lstFacility);
      //update lstFacToUpdate;
	}
	}

}