public with sharing class CARPOL_AC_UploadImage {

    public List < Attachment > att = new List < Attachment > ();
    public list < Attachment > attachmentList = new list < Attachment > ();
    public list < attWrapper > attWrapperList {
        get;
        set;
    }
    private AC__c acLineItem = new AC__c();
    public String picURL {
        get;
        set;
    }
    public Integer iterator;
    public String imageid {
        get;
        set;
    }
    public Boolean selected {
        get;
        set;
    }
    public Id pID {
        get;
        set;
    }


    public Attachment uploadedFile {
        get {
            if (uploadedFile == null) uploadedFile = new Attachment();
            return uploadedFile;
        }
        set;
    }

    // Constructor    
    public CARPOL_AC_UploadImage(ApexPages.StandardController controller) {

        if (ApexPages.currentPage().getParameters().get('ID') != null) {
            pID = ApexPages.currentPage().getParameters().get('ID');
            acLineItem = [SELECT ID, Name FROM AC__c WHERE ID = : ApexPages.currentPage().getParameters().get('ID') LIMIT 1];
            System.Debug('<<<<<<< AC Line Item : ' + acLineItem + ' >>>>>>>');
            getExisting();
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select an AC Line Item application to upload an image.'));
        }

    }



    public void getExisting() {
        System.Debug('<<<<<<< AC Line Item Name : ' + acLineItem.Name + ' >>>>>>>');
        att = [SELECT ID, Name FROM Attachment WHERE Description = 'Uploaded Image'
        AND ParentId = : acLineItem.ID ORDER BY CreatedDate DESC LIMIT 1];
        if (att.size() > 0) {
            picURL = '/servlet/servlet.FileDownload?file=' + att[0].ID;
        }

        //New Code Oct 02

        attWrapperList = new list < attWrapper > ();
        attachmentList = [SELECT ContentType, CreatedById, Id, ParentId FROM Attachment WHERE ParentId = : pID]; //'a00r00000012hqO'];

        for (Attachment at: attachmentList) {

            //attWrapperList.add(at);
            attWrapperList.add(new attWrapper(at, false));

        }

    }

    public PageReference uploadFile() {

        try {
            String fileExtension = uploadedFile.Name;
            System.Debug('<<<<<<< Uploaded File Name : ' + fileExtension + ' >>>>>>>');

            if (uploadedFile.Name != null) {
                fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.') + 1, fileExtension.length());
                fileExtension = fileExtension.toUpperCase();
                System.Debug('<<<<<<< File Size : ' + uploadedFile.BodyLength + ' >>>>>>>');
                System.Debug('<<<<<<< File Extension : ' + fileExtension + ' >>>>>>>');
                if (fileExtension == 'PNG' || fileExtension == 'JPG' || fileExtension == 'JPEG') {
                    uploadedFile.OwnerId = UserInfo.getUserId();
                    uploadedFile.ParentId = acLineItem.ID;
                    uploadedFile.Description = 'Uploaded Image';
                    insert uploadedFile;
                    picURL = '/servlet/servlet.FileDownload?file=' + uploadedFile.ID;
                    imageid = uploadedfile.id;
                    uploadedFile = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'File uploaded successfully.'));
                } else {
                    uploadedFile = new Attachment();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Only .png, .jpeg, .jpg files are allowed.'));
                }
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select an image to upload.'));
            }

            PageReference secondPage = Page.CARPOL_UNI_UploadImage;
            secondPage.setRedirect(true);
            secondPage.getParameters().put('id', pID);
            return secondPage;


            //return null;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading file. Please re-try.'));
            return null;
        }
    }




    public PageReference removeFile() {

        try {
            /* system.debug ('$$$$$$$$$$ ');
        Attachment[] attachs = [SELECT ID, Name FROM Attachment WHERE ParentId = :acLineItem.ID ];
   delete attachs;
   */
            list < Attachment > attachListDeleted = new list < Attachment > ();
            for (attWrapper atWraper: attWrapperList) {

                if (atWraper.isChecked) attachListDeleted.add(atWraper.att);

            }
            if (attachListDeleted.size() > 0) {
                delete attachListDeleted;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attachment deleted successfully'));
            }
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment deleted successfully'));
            // return null;
            PageReference secondPage = Page.CARPOL_UNI_UploadImage;
            secondPage.setRedirect(true);
            secondPage.getParameters().put('id', pID);
            return secondPage;

        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error deleting file. Please make sure you have atleast 1 file'));
            return null;
        }



    }

    //Wrapper


    public class attWrapper {
        public Attachment att {
            get;
            set;
        }
        public Boolean isChecked {
            get;
            set;
        }
        public attWrapper(Attachment att, boolean ischeck) {
            this.att = att;
            this.isChecked = ischeck;

        }
    }



}