Public without sharing class CARPOL_UNI_eAuthExternalProfileRequest{
    List<SelectOption> profiles = new List<SelectOption>();
    
    public String profileList { get; set; }
    public static List<SelectOption> profiles { get; set; }
    public String selectedValue { get; set; }
    
    
    public List<SelectOption> getProfileOptions() {
        system.debug('inside get profiles function');
          //List<SelectOption> options = new List<SelectOption>();
          
           profiles.add(new SelectOption('None','None'));
           profiles.add(new SelectOption('Broker Preparer','Broker Preparer'));
           //profiles.add(new SelectOption('Reviewer','Reviewer'));
            system.debug('options '+ profiles);
           // return options;
        return profiles;
    }
    
    public PageReference submitRequest() {
        system.debug('selected profile ' + selectedValue);
        markAccountFlag(selectedValue);
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your request for broker/preparer access has been submitted.');
        return null;
    }


    
    public CARPOL_UNI_eAuthExternalProfileRequest(){
    
    }
    
    public void markAccountFlag(string profileRequest) {
        Case caseCreate = new Case();
       // Id contactId = [select id from Contact where Name =: UserInfo.getName()].Id;
        
           
        /*Id userId = UserInfo.getUserID();
        user currentUser = [select id, contactid from user where id =: userid];
        contact con = [select id, accountid from contact where id =: currentuser.contactid];
        account accnt = [select id, broker__c from account where id =: con.accountid];
       // accnt.broker__c = true;
       // accnt.IsPartner = false;
       // accnt.IsCustomerPortal = false;
        update accnt;*/
        
    }

  
public static void sendEmail(ID caseId) {

  //New instance of a single email message
   /*Messaging.SingleEmailMessage mail = 
            new Messaging.SingleEmailMessage();
            
   string queueEmail = [Select g.Id, g.Name, g.Email from Group g where g.Type = 'Queue' and g.Name = 'eAuth External Reviewer Profile Approval' ].Email;
    string[] toaddress = New String[] {queueEmail};
    //query on template object
   // EmailTemplate et=[Select id from EmailTemplate where name=:'New Profile Request'];


   // The email template ID used for the email
   mail.setTemplateId(et.id);
   //set object Id
   mail.setTargetObjectId(UserInfo.getUserId());
   mail.setToAddresses(toaddress);

   mail.setWhatId(caseId);    
   mail.setBccSender(false);
   mail.setUseSignature(false);
   mail.setReplyTo(queueEmail);
   mail.setSenderDisplayName('Profile Request');
   mail.setSaveAsActivity(false);  
 
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }  */

  }
 }