@isTest(seealldata=false)
private class CheckForSelfReportingForInsert_Test {
      @IsTest static void CheckForSelfReportingForInsert_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
          AC__c li = testData.newlineitem('Personal Use', objapp);

          Authorizations__c objauth = new Authorizations__c();
          objauth.Application__c = objapp.id;
          objauth.RecordTypeID = ACauthRecordTypeId;
          objauth.Status__c = 'Submitted';
          objauth.Date_Issued__c = date.today();
          objauth.Applicant_Alternate_Email__c = 'test@test.com';
          objauth.UNI_Zip__c = '32092';
          objauth.UNI_Country__c = 'United States';
          objauth.UNI_County_Province__c = 'Duval';
          objauth.BRS_Proposed_Start_Date__c = date.today()+30;
          objauth.BRS_Proposed_End_Date__c = date.today()+40;
          objauth.CBI__c = 'CBI Text';
          objauth.Application_CBI__c = 'No';
          objauth.Applicant_Alternate_Email__c = 'email2@test.com';
          objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
          objauth.AC_Applicant_Email__c = 'email2@test.com';
          objauth.AC_Applicant_Fax__c = '(904) 123-2345';
          objauth.AC_Applicant_Phone__c = '(904) 123-2345';
          objauth.AC_Organization__c = 'ABC Corp';
          objauth.Means_of_Movement__c = 'Hand Carry';
          objauth.Biological_Material_present_in_Article__c = 'No';
          objauth.If_Yes_Please_Describe__c = 'Text';
          objauth.Applicant_Instructions__c = 'Make corrections';
          objauth.BRS_Number_of_Labels__c = 10;
          objauth.BRS_Purpose_of_Permit__c = 'Importation';
          objauth.Documents_Sent_to_Applicant__c = false;
          objauth.Status__c = 'Approved';
          insert objauth;          

          li.Authorization__c = objauth.id;
          update li;
          Self_Reporting__c objlra = new Self_Reporting__c();
          objlra.Authorization__c = objauth.id;
          insert objlra;
          List<Self_Reporting__c> newlist = new List<Self_Reporting__c>();
          newlist.add(objlra);
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_ViewSelfReport;
              Test.setCurrentPage(pageRef);

              ApexPages.currentPage().getParameters().put('id','Test');                                                    

              CheckForSelfReportingForInsert extclass = new CheckForSelfReportingForInsert();
              
              CheckForSelfReportingForInsert.ThrowErrorWhenConditionsMet(newlist);  
              system.assert(extclass != null);                                                   
          Test.stopTest();   
      }
}