public class portal_location_edit {
    
    public location__c obj {get;set;}
    public String DestLoc {get;set;}
    public String OrigLoc {get;set;}
    public String OrigDestLoc {get;set;}
    public String RelLoc {get;set;}
    public String recordTypename {get;set;}
    public ID recordTypeId {get;set;}
    public ID rectype {get;set;}
    public ID cancelid {get;set;}
    public Id LineId{get;set;}
    public string redirect {get;set;}
    public Id strPathwayId {get;set;}
    public Id LineItemId{get;set;}
    public Id genid {get;set;}
    public Id locId {get;set;}
    //private String stCurrentPageURL;
    // VV Added for GPS coordinates
    public boolean gps1 {get;set;}
    public boolean gps2 {get;set;}
    public boolean gps3 {get;set;}
    public boolean gps4 {get;set;}
    public boolean gps5 {get;set;}
    public boolean gps6 {get;set;}
    public string gpsno {get;set;}
    public string errmsg {get;set;}
    public String usertype {get;set;}


    public portal_location_edit(ApexPages.StandardController controller) {
                     userType = UserInfo.getUserType();
                     obj = (location__c)controller.getRecord();
                     obj.Line_Item__c = ApexPages.currentPage().getParameters().get('LineItemId');
                     obj.id = ApexPages.currentPage().getParameters().get('id');
                     LineItemId= ApexPages.currentPage().getParameters().get('LineItemId');
                     locId = ApexPages.currentPage().getParameters().get('genid');
                     obj.RecordTypeId = ApexPages.currentPage().getParameters().get('rectype');
                     recordTypeId = ApexPages.currentPage().getParameters().get('rectype');
                     cancelid = ApexPages.currentPage().getParameters().get('LineItemId');
                     strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
                     redirect = ApexPages.currentPage().getParameters().get('redirect');
                     //stCurrentPageURL = ApexPages.currentPage().getURL();
                     
                    system.debug('-----------LineItemId'+LineItemId);

                String USCountryID = [Select Id, Name from country__c where Name='United States of America' limit 1].ID;
                list<RecordType> rts = [SELECT ID,name, DeveloperName FROM RecordType WHERE ID = :recordTypeId];

                if(rts.size()>0){
                    for(RecordType r : rts){
                        if(r.DeveloperName == 'Destination_Location' || r.DeveloperName == 'Origin_and_Destination'){
                            DestLoc = 'Yes';
                            OrigDestLoc = 'Yes';
                            obj.Country__c = USCountryID;
                            recordTypename = r.name;
                        } else if(r.DeveloperName == 'Origin_Location'){
                            OrigLoc = 'Yes';
                        } else if(r.DeveloperName == 'Release_Location'){
                            RelLoc = 'Yes';
                            obj.Country__c = USCountryID;
                        }
                    }
                }
             gpsno = '';
             gps1 = false;
             gps2 = false;
             gps3 = false;
             gps4 = false;
             gps5 = false;
             gps6 = false;
             errmsg = ''; 
            if(obj.id!=null){
            gps1 = true;           
            if(obj.GPS_Coordinates_2__Latitude__s!=null)
            gps2 = true;
            if(obj.GPS_Coordinates_3__Latitude__s!=null)
            gps3 = true;
            if(obj.GPS_Coordinates_4__Latitude__s!=null)
            gps4 = true;
            if(obj.GPS_Coordinates_5__Latitude__s!=null)
            gps5 = true;
            if(obj.GPS_Coordinates_6__Latitude__s!=null)
            gps6 = true;
        }
    }

    public pagereference saveloc(){
            recordType rtid =[SELECT ID,name, DeveloperName FROM RecordType WHERE ID =:recordTypeId limit 1];
             country__c countryname =[SELECT ID,name from country__c where id=:obj.Country__c limit 1];
           AC__c lineitem =[SELECT ID,name,Movement_Type__c from AC__c where id=:LineItemId];
           system.debug('------lineitem'+lineitem.Movement_Type__c);
             if(rtid.name =='Origin Location' && countryname.name  == 'United States of America' && lineitem.Movement_Type__c == 'Import'){
                   ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Origin country should be other than United States of America'));  
                   //PageReference currPage = new PageReference(stCurrentPageURL); 
                   return null;
             }else{
        try{
            
             locId = ApexPages.currentPage().getParameters().get('id');
             system.debug('------locId'+locId);
                        obj.id = locId;
                        upsert obj;
            
            PageReference pg = Page.portal_location_detail;
            
             pg.getParameters().put('id',obj.id);
                    
                    if(redirect == 'yes'){
                   
                        pg.getParameters().put('redirect',redirect);
                        pg.getParameters().put('strPathwayId',strPathwayId);
                        pg.getParameters().put('LineItemId',LineItemId);
                        pg.getParameters().put('recordTypeId',recordTypeId);
                    }
                        pg.setRedirect(true);
                    return pg;
             }catch(Exception ex){
                ApexPages.addMessages(ex);
                return null;
                }
             }
            }
 
    public pageReference cancel(){
        if(redirect == 'yes'){
            PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',cancelid);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }else if(obj.id != null){
        PageReference pg = new PageReference('/'+obj.id);
        pg.setRedirect(true);
        return pg;
        }else{
        PageReference pg = new PageReference('/'+cancelid);
        pg.setRedirect(true);
        return pg;
        }
       
    }
    
    public void prepopulateAC(){
        List<Applicant_contact__c> LstPrimaryAC = [Select Account__r.name,Contact_Organization__c,Mailing_County__c,Alternate_Phone__c,Mailing_Street__c, Mailing_City__c,Mailing_Country_LR__c, Mailing_State_LR__c, Mailing_Zip_Postal_Code__c,Phone__c,Email_Address__c, Fax__c from Applicant_Contact__c where id = :obj.Primary_AssociatedContact__c];
        Applicant_contact__c objPrimaryAC = ( LstPrimaryAC != null && LstPrimaryAC.size()>0) ? LstPrimaryAC  [0]: null;                   
            system.debug('===> objPrimaryAC org  = '+objPrimaryAC );
            If(objPrimaryAC != null){
            obj.Org_L1__c = objPrimaryAC.Account__r.name;
            obj.Contact_Address1__c = objPrimaryAC.Mailing_Street__c;
            obj.Contact_City__c= objPrimaryAC.Mailing_City__c ;
            obj.Primary_County__c= objPrimaryAC.Mailing_County__c;
            obj.Primary_Country__c = objPrimaryAC.Mailing_Country_LR__c;
            obj.Primary_State__c = objPrimaryAC.Mailing_State_LR__c;
            obj.Contact_Zip__c = objPrimaryAC.Mailing_Zip_Postal_Code__c;
            obj.Day_Phone__c = objPrimaryAC.Phone__c;
            obj.Email_1__c = objPrimaryAC.Email_Address__c;
            obj.Fax__c = objPrimaryAC.Fax__c;
            }             
          }    

    public void prepopulateSecondAC(){
        List<Applicant_contact__c> LstSecondaryAC = [Select Account__r.name,Contact_Organization__c,Mailing_County__c,Alternate_Phone__c,Mailing_Street__c, Mailing_City__c,Mailing_Country_LR__c, Mailing_State_LR__c, Mailing_Zip_Postal_Code__c,Phone__c,Email_Address__c, Fax__c from Applicant_Contact__c where id = :obj.Secondary_AssociatedContact__c];
        Applicant_contact__c objSecondaryAC = ( LstSecondaryAC != null && LstSecondaryAC .size()>0) ? LstSecondaryAC [0]: null;                   
            system.debug('===> objSecondaryAC   = '+objSecondaryAC  );
            If(objSecondaryAC != null){
            obj.Org_L2__c= objSecondaryAC.Account__r.name;
            obj.Contact_Address2__c= objSecondaryAC.Mailing_Street__c;
            obj.Secondary_Contact_City__c= objSecondaryAC.Mailing_City__c ;
            obj.Secondary_County__c= objSecondaryAC.Mailing_County__c;
            obj.Secondary_Country__c= objSecondaryAC.Mailing_Country_LR__c;
            obj.Secondary_State__c= objSecondaryAC.Mailing_State_LR__c;
            obj.Secondary_Contact_Zip__c= objSecondaryAC.Mailing_Zip_Postal_Code__c;
            obj.Day_Phone_2__c= objSecondaryAC.Phone__c;
            obj.Email_2__c= objSecondaryAC.Email_Address__c;
            obj.Fax_2__c= objSecondaryAC.Fax__c;
            }             
          }    

        // VV Added for GPS coordinates
         public void addgps()
    {
      errmsg = '';
        if(gps1==false){
       gps1=true;
       }
       else if(gps2==false){
       gps2=true;
       }
       else if(gps3==false){
       gps3=true;
       }
       else if(gps4==false){
       gps4=true;
       }
       else if(gps5==false){
       gps5=true;
       }
       else if(gps6==false){
       gps6=true;
       }
       else{
           errmsg = 'You can only add 6 location GPS coordinates';
       }
     // return null; 
    }
    
     public void removegps()
    {
      errmsg = '';
      system.debug('-----gpsno-----'+gpsno);
      system.debug('-----gps3 -----'+gps3);
      if(gpsno=='1'){
          gps1 = false;
          obj.GPS_Coordinates_1__Latitude__s = null;
          obj.GPS_Coordinates_1__Longitude__s = null;
          
      }
      if(gpsno=='2'){
          gps2 = false;
          obj.GPS_Coordinates_2__Latitude__s = null;
          obj.GPS_Coordinates_2__Longitude__s = null;
      }
      if(gpsno=='3'){
          gps3 = false;
          obj.GPS_Coordinates_3__Latitude__s = null;
          obj.GPS_Coordinates_3__Longitude__s = null;
      }
      if(gpsno=='4'){
          gps4 = false;
          obj.GPS_Coordinates_4__Latitude__s = null;
          obj.GPS_Coordinates_4__Longitude__s = null;
      }
      if(gpsno=='5'){
          gps5 = false;
         obj.GPS_Coordinates_5__Latitude__s = null;
          obj.GPS_Coordinates_5__Longitude__s = null;
      }
      if(gpsno=='6'){
          gps6 = false;
          obj.GPS_Coordinates_6__Latitude__s = null;
          obj.GPS_Coordinates_6__Longitude__s = null;
      }
      //update objloc;
      system.debug('-----gps3 -----'+gps3);
        
    }
    
}