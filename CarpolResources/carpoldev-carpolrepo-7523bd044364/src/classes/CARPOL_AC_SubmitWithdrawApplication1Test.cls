@isTest(seealldata=true)
private class CARPOL_AC_SubmitWithdrawApplication1Test {
    static testMethod void ACSubmit() {
     
     List<AC__c> acLineItems = new List<AC__c>();
     integer StopQuery = 0;

      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      Application__c objapp = testData.newapplication();
     

    
    breed__c objbrd = testData.newbreed(); 
    Applicant_Contact__c apcont = testData.newappcontact(); 
    Applicant_Contact__c apcont2 = testData.newappcontact();
    Facility__c fac = testData.newfacility('Domestic Port');  
    Facility__c fac2 = testData.newfacility('Foreign Port'); 
    
      
      AC__c ac1 = testData.newLineItem('Resale/Adoption',objapp);       
      Applicant_Attachments__c appAttach1 = testData.NewAttach(ac1.Id);
      
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');                   
  
      Attachment att1 = testData.newattachment(appAttach1.Id);  
      Attachment att2 = testData.newattachment(appAttach1.Id);  
      Attachment att3 = testData.newattachment(appAttach1.Id);    
      
     ac1.Application_Number__c = objapp.Id;
     ac1.Date_of_Birth__c = date.today()-220; 
     ac1.status__c = 'Ready to Submit';

     update ac1;  

     objapp.Application_Status__c = 'Submitted';

     update objapp;
      
 
       // Code for creating new authorization
       Map<String,String> authorizationsGrouping = new Map<String,String>();
       Map<String,Integer> acLineItemsMap = new Map<String,Integer>();
       acLineItems.clear();
       acLineItems = [SELECT ID, Name, Signature__c, Signature__r.Response_Type__c,  Port_of_Entry__r.ID, Authorization__c,Exporter_Last_Name__r.Id,Importer_Last_Name__r.Id,Proposed_date_of_arrival__c,Signature__r.Program_Prefix__r.Program__r.Name FROM AC__c WHERE Application_Number__c =: objapp.id];
       String strACLineItemsAuthorizations;
       String strShare;
       system.assert(objapp != null);
        /* commenting auth creating causing SOQL errors w/ task_generator trigger on auth insert   
      
      //  Authorizations__c objauth = testData.newAuth(objapp.Id, ACauthRecordTypeId); 
   
       //objauth.Thumbprint__c = signature.Id;

     //  objauth.Authorization_Type__c = 'Permit';
      // objauth.port_of_Entry__c = 'a07r0000000CfEl';
       //objauth.Status__c = 'Submitted';
       //objauth.Authorized_User__c = objapp.Applicant_Name__c;
       //objauth.AC_Applicant_Email__c = objapp.Applicant_Email__c;
       //objauth.AC_Applicant_Fax__c = objapp.Applicant_Fax__c;
       //objauth.AC_Applicant_Address__c = objapp.Applicant_Address__c;
       //objauth.AC_Applicant_Phone__c = objapp.Applicant_Phone__c;
       //objauth.AC_Applicant_U_S_Address__c = objapp.US_Address__c;
       //objauth.AC_Organization__c = objapp.Organization__c;
      // objauth.Locked__c = 'No';
       //Group queueid = [select Id from Group where Name = 'AC Reviewer' and Type = 'Queue'];                            
       //objauth.OwnerId=queueid.Id;



       //objauth.Locked__c = 'Yes';
      // update objauth;
      */
     }
}