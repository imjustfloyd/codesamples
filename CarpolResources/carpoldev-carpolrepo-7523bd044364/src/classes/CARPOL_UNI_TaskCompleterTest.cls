@isTest(seealldata=true)
private class CARPOL_UNI_TaskCompleterTest{
    static testMethod void testCARPOL_UNI_TaskCompleter() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String WFTasksRecordTypeId = Schema.SObjectType.Workflow_Task__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      
      String UpdateField = '';
      String SourceField = '';
      String DependentField = '';
      String stopLoop='False';
      Integer FinalWorkFlowOrder = 1;
      
      // Retrieve two profiles, for the standard user and the system   
    
      // administrator, then populate a map with them.  
    

      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name = 
         'AC Analyst' or name = 'System Administrator'];

      for(Profile p : ps){
         profiles.put(p.name, p.id);
      }

      // Create the users to be used in this test.  
    
      // First make a new user.  
      
      
       // Insert 20 Users
   // List<User> users = new List<User>();
    //for (Integer i = 0; i < 2; i++) {
      User u = new User();
      u.FirstName         = 'Elsa';
      u.LastName          = 'of Arendelle';
      u.Email             = 'elsa@disney.com';
      u.Alias             = 'elsa';
      u.Username          = 'elsa@disney.com';
      u.LocaleSidKey      = 'en_US';
      u.TimeZoneSidKey    = 'GMT';
      u.ProfileID         = profiles.get('AC Analyst');
      u.LanguageLocaleKey = 'en_US';
      u.EmailEncodingKey  = 'UTF-8';
     // users.add(u);
    //}
    //insert users;
      insert u;
      
      Account objacct = new Account();
      objacct.Name = 'AC account';
      //objacct.RecordTypeId = '012r000000007C4'; 
      objacct.RecordTypeId = AccountRecordTypeId;    
      insert objacct;
      Contact objcont = new Contact();
      objcont.FirstName = 'FirstName';
      objcont.LastName = 'LastName';
      objcont.Email = 'test@email.com';
      objcont.AccountId = objacct.id;
      objcont.MailingStreet = 'Mailing Street';
      objcont.MailingCity = 'Mailing City';
      objcont.MailingState = 'Ohio';
      objcont.MailingCountry = 'United States';
      objcont.MailingPostalCode = 'Mailing Postal Code';
      insert objcont;
      
      Breed__c brd = new Breed__c();
      brd.Name='New Breed';
      insert brd;
      Applicant_Contact__c apcont = new Applicant_Contact__c();
      apcont.First_Name__c = 'apcont';
      apcont.Email_Address__c = 'apcont@test.com';
      apcont.Name = 'Associated Contacts';
      //apcont.RecordTypeId = '012r000000007Iq';
      apcont.RecordTypeId = ImpapcontRecordTypeId;
      //apcont.USDA_License_Expiration_Date__c = date.today()+30;
      //apcont.USDA_Registration_Expiration_Date__c = date.today()+40;
      insert apcont;
      Applicant_Contact__c apcont2 = new Applicant_Contact__c();
      apcont2.First_Name__c = 'apcont2';
      apcont2.Email_Address__c = 'apcont2@test.com';
      apcont2.Name = 'Associated Contacts2';
      //apcont.USDA_License_Expiration_Date__c = date.today()+30;
      //apcont.USDA_Registration_Expiration_Date__c = date.today()+40;
      insert apcont2;
      Facility__c fac = new Facility__c();
      //fac.RecordTypeId = '012r0000000065q';
      fac.RecordTypeId = PortsFacRecordTypeId;
      fac.Name = 'entryport';
      insert fac;
      Facility__c fac2 = new Facility__c();
      //fac2.RecordTypeId = '012r0000000065q';
      fac2.RecordTypeId = PortsFacRecordTypeId;
      fac2.Name = 'Embarkationport';
      fac2.Type__c = 'Foreign Port';
      insert fac2;
      Application__c objapp = new Application__c();
      objapp.Applicant_Name__c = objcont.id;
      //objapp.Recordtypeid = '012r0000000068G';
      objapp.Recordtypeid = ACAppRecordTypeId;
      objapp.Application_Status__c = 'Open';
      insert objapp;
      
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      
      AC__c ac = new AC__c();
      ac.Application_Number__c = objapp.id;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.id;
      ac.Port_of_Embarkation__c = fac2.id;
      ac.Date_of_Birth__c = date.today()-30;
      ac.Breed__c = brd.id;
      ac.Importer_Last_Name__c = apcont.id;
      ac.Exporter_Last_Name__c = apcont2.id;
      ac.Status__c = 'Submitted';
      ac.Purpose_of_the_Importation__c = 'Personal Use';
      ac.Program_Line_Item_Pathway__c = plip.id;
      insert ac;
      
          
      AC__c ac3 = new AC__c();
      ac3.Application_Number__c = objapp.id;
      ac3.Departure_Time__c = date.today()+11;
      ac3.Arrival_Time__c = date.today()+15;
      ac3.Port_of_Entry__c = fac.id;
      ac3.Port_of_Embarkation__c = fac2.id;
      ac3.Date_of_Birth__c = date.today()-30;
      ac3.Breed__c = brd.id;
      ac3.Importer_Last_Name__c = apcont.id;
      ac3.Exporter_Last_Name__c = apcont2.id;
      ac3.Status__c = 'Submitted';
      ac3.Purpose_of_the_Importation__c = 'Veterinary Treatment';
      ac3.Treatment_available_in_Country_of_Origin__c = 'No';
      ac.Program_Line_Item_Pathway__c = plip.id;      
      insert ac3;
           
       Authorizations__c objauth = new Authorizations__c();
       objauth.RecordTypeId =  ACauthRecordTypeId;
       objauth.Application__c = objapp.id;
       objauth.Status__c = 'Issued';
       objauth.Issuer__c = '424423423424';
       objauth.Date_Issued__c = Date.today();
       insert objauth;
       ac.Authorization__c = objauth.id;
       update ac;
       
        Workflow_Task__c  objwrkflwtsd = new Workflow_Task__c();
        objwrkflwtsd.Name = 'Task 1: Process Authorization';
        objwrkflwtsd.OwnerId = u.Id;
        objwrkflwtsd.Authorization__c = objauth.id;

        objwrkflwtsd.Workflow_Order__c = 23;
        objwrkflwtsd.RecordTypeId = WFTasksRecordTypeId;
       objwrkflwtsd.Stop_Status__c = 'Denied';
       objwrkflwtsd.Status__c = 'Waiting';
        insert objwrkflwtsd;
        

        
        DependentField = 'Status__c';
        Workflow_Task__c  objwrkflwtsd2 = new Workflow_Task__c();
        objwrkflwtsd2.Name = 'Ac wrokflow task2';
        objwrkflwtsd2.Authorization__c = objauth.id;
        objwrkflwtsd2.OwnerId = u.Id;
        objwrkflwtsd2.Status__c='Waiting';
        objwrkflwtsd2.Dependent_On_Workflow__c = objwrkflwtsd.Workflow_Order__c;

        objwrkflwtsd2.Required_if_Prior_Task_Equals__c = 'True';
        insert objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=null;
        objwrkflwtsd2.Status__c = 'In Progress';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        objwrkflwtsd2.Task_Source_Field_Name__c='Status__c';
        update objwrkflwtsd2;
        
        objwrkflwtsd.Status__c = 'Not Started';
        update objwrkflwtsd;
        objwrkflwtsd2.Incident_Number__c=null;
        objwrkflwtsd2.Status_Categories__c = 'Customer Feedback';
        objwrkflwtsd2.Status__c = 'Pending';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=null;
        objwrkflwtsd2.Update_Record_Field_Name__c='test';
        objwrkflwtsd2.Status__c = 'Complete';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=null;
        objwrkflwtsd2.Status__c = 'In Progress';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        
        test.starttest();
        Incident__c incident = new Incident__c();
          incident.Requester_Name__c='testReqName';
         
          incident.Authorization__c= objauth.id;
          incident.EFL_Issued_Date__c = Date.today();
          insert incident;
        objwrkflwtsd2.Incident_Number__c=incident.id;
         
        objwrkflwtsd2.Status__c = 'In Progress';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        objwrkflwtsd2.In_Progress_Status__c='test';
        objwrkflwtsd2.Update_Record_Status__c=True;
        update objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=incident.id;
        objwrkflwtsd2.Status__c = 'Pending';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=incident.id;
        objwrkflwtsd2.Update_Record_Field_Name__c='test';
        objwrkflwtsd2.Update_Record_to_Specific_Value__c='test';
        objwrkflwtsd2.Status__c = 'Complete';
        objwrkflwtsd2.Record_Status__c ='test';
        objwrkflwtsd2.Update_Record_Status__c=True;
        objwrkflwtsd2.Status_Categories__c = 'Inspection Initiated';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        objwrkflwtsd2.Incident_Number__c=incident.id;
        objwrkflwtsd2.Status__c = 'In Progress';
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
        update objwrkflwtsd2;
        test.stoptest();
        system.runas(u)
        {
        
             Workflow_Task__c getTask1 = [select Id,Dependent_On_Workflow__c,Workflow_Order__c,Dependency_Field__c,Required_if_Prior_Task_Equals__c from Workflow_Task__C where id = :objwrkflwtsd.id];
             If(getTask1.Dependency_Field__c == objwrkflwtsd2.Required_if_Prior_Task_Equals__c)
                   {
                    objwrkflwtsd2.Status__c = 'Not Started';
                    CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
                    update objwrkflwtsd2;
                    FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                    stopLoop = 'True';
                   }
                   else if(stopLoop=='False'){
                    objwrkflwtsd2.Status__c = 'Deferred';
                    FinalWorkFlowOrder = FinalWorkFlowOrder + 1;
                    CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;
                    update objwrkflwtsd2;
                    }
               
        
        getTask1.Status__c='Complete';     
        CARPOL_UNI_TriggerMonitor.runCARPOL_UNI_TaskCompleter=true;   
        update getTask1;
        }
        Map<Id, Authorizations__c> newTriggerLines = new Map<Id, Authorizations__c> ();
        Authorizations__c auth = new Authorizations__c();
        auth.Status__c = 'Issued';
        auth.Documents_Sent_to_Applicant__c = false;
        auth.Date_Issued__c = Date.today();
        //auth.id='a03r0000000l9UiAAI';
        //auth.Application__c ='a01r0000000cLVYAA2';
        List<Attachment> test1 = [SELECT ID, Name,parentID, Body FROM Attachment WHERE Name like 'Permit%' ORDER BY CreatedDate DESC LIMIT 1 ];
        newTriggerLines.put(test1[0].parentID, objauth);
        CARPOL_UNI_SendIssuedAttachment attch = new CARPOL_UNI_SendIssuedAttachment(newTriggerLines);
        attch.sendIssuedAttachment();
        system.assert(attch != null);
    }
  }