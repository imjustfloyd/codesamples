@isTest(seealldata=false) 
private class CARPOL_AC_AuthButtonsControllerTest {
    static testMethod void CARPOL_AC_AuthButtonsControllerTest() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Application__c objapp = testData.newapplication();
      AC__c ac1 = testData.newlineitem('Personal Use',objapp);
      Authorizations__c objauth = testData.newAuth(objapp.Id); 

      
      Test.startTest();
      PageReference pageRef = Page.CARPOL_AC_LiveDogPermit_Auth;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
      ApexPages.currentPage().getParameters().put('Id',objauth.id);
      CARPOL_AC_AuthorizationButtonsController acepreg = new CARPOL_AC_AuthorizationButtonsController(sc);
      System.assert(acepreg != null);
      Test.stopTest();
      }
}