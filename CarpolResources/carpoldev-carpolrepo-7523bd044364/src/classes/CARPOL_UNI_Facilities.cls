public class CARPOL_UNI_Facilities    {
    
      
        public void CreateFacilities(Authorizations__c Auth)
            {    
                //Authorizations__c Auth = AuthList[0];
                system.debug('auth id '+ auth.id);
                ID RecordType = Schema.Sobjecttype.AC__c.getRecordTypeInfosByName().get('Facilities').getRecordTypeId();
                system.debug('RecordType '+ RecordType);
                AC__c LineItem = [SELECT     Facility_Name__c ,Facility_Address_1__c,Facility_Building_Type__c,Facility_City__c,Facility_Contact_Email_Address__c,Facility_Contact_Fax__c,Facility_Contact_First_Name__c,
                                            Importer_Last_Name__c,Facility_Contact_Phone__c,Facility_Country__c,Facility_Description__c,Facility_Disposition__c,Facility_IATA_Code__c,Facility_Id__c,DeliveryRecipient_Mailing_Street__c,
                                            DeliveryRecipient_Mailing_City__c,
                                            DeliveryRecipient_Mailing_CountryLU__c,
                                            Delivery_Recipient_State_ProvinceLU__c,
                                            DeliveryRecipient_Mailing_Zip__c,Facility_Inspection_Expiry_Date__c,Facility_Inspection_Validity_Period__c,Facility_ISO_Code__c,
                                            Facility_License_Number__c,Facility_Room_Number__c,
                                            Facility_State__c,
                                            Facility_Type__c,Facility_Zip__c,Country_Of_Origin__c, State_Province_of_Origin__c,Status__c, Regulated_Article__c, Disease_Pest__c FROM AC__c WHERE (Authorization__c=:Auth.ID AND RecordTypeID=:RecordType) limit 1];
                // Id stateId = [SELECT  Id from Level_1_Region__c where Name =:  LineItem.Facility_info_State__c].Id;
                //Id countryId = [SELECT  Id from Country__c where Name =: LineItem.Facility_Info_Country__c].Id;
                Facility__c newfacility = new Facility__c();
                    newfacility.Name = LineItem.Facility_Name__c;
                    system.debug('name '+ LineItem.Facility_Name__c);
                    newfacility.Description__c = LineItem.Facility_Description__c;
                    newfacility.IATA_Code__c = LineItem.Facility_IATA_Code__c;
                    system.debug('building record type ' + LineItem.Facility_Building_Type__c);
                    newfacility.RecordTypeID = Schema.Sobjecttype.Facility__c.getRecordTypeInfosByName().get(LineItem.Facility_Building_Type__c).getRecordTypeId();
                    newfacility.Address_1__c = LineItem.DeliveryRecipient_Mailing_Street__c;
                    newfacility.City__c = LineItem.DeliveryRecipient_Mailing_City__c;
                    newfacility.State__c = LineItem.Delivery_Recipient_State_ProvinceLU__c;
                    newfacility.Zip__c = LineItem.DeliveryRecipient_Mailing_Zip__c;
                    newfacility.Country__c = LineItem.DeliveryRecipient_Mailing_CountryLU__c;
                    newfacility.Room_Number__c = LineItem.Facility_Room_Number__c;
                    //newfacility.Inspection_Expiry_Date__c = LineItem.Facility_Inspection_Expiry_Date__c;
                    //newfacility.Inspection_Required__c = LineItem.Facility_Inspection_Required__c;
                  //  newfacility.Inspection_Validity_Period__c = LineItem.Facility_Inspection_Validity_Period__c;
                   // newfacility.isActive__c = true
                    newfacility.Type__c = LineItem.Facility_Type__c;
                    newfacility.Status__c = 'Active';
                    system.debug('This is creating it');
                    insert newfacility;
                    
                Facility_Capability__c facCapability = new Facility_Capability__c();
                
                facCapability.Disease_Pest__c = lineitem.Disease_Pest__c;
                facCapability.Regulated_Article__c = lineitem.Regulated_Article__c;
                facCapability.Facility__c = newfacility.id;
                insert facCapability;
                    
            }
   }