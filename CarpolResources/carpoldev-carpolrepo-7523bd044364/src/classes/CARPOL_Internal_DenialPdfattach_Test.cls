@isTest(seealldata=true)
public class CARPOL_Internal_DenialPdfattach_Test {
      @IsTest static void CARPOL_Internal_DenialPdfattach_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Date_Issued__c = Date.today();
          objauth.Template__c = objcm.id;
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');

          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_Internal_DenialPdfattach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objapp.id);
              CARPOL_Internal_DenialPdfattach extclassempty = new CARPOL_Internal_DenialPdfattach();              
              CARPOL_Internal_DenialPdfattach extclass = new CARPOL_Internal_DenialPdfattach(sc);
              extclass.intdenattach();
              system.assert(extclass != null);
          Test.stopTest();    

      }

}