public class portal_LocationRecordType {
public String LineItemId {get; set;}
public String rectype {get;set;}
public id recordTypeId{get;set;}
public id strPathwayId {get;set;}
public id LineId {get;set;}
public string redirect {get;set;}
public string movementtype {get;set;}
public integer olocrectype;
 public integer dlocrectype;
 string locrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();
string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId(); 
public Location__c location = new Location__c();

     public portal_LocationRecordType(ApexPages.StandardController stdController) {
        
        this.location = (Location__c)stdController.getRecord(); 
        LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
        strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
        LineId = ApexPages.currentPage().getParameters().get('LineId');
        redirect = ApexPages.currentPage().getParameters().get('redirect');
        //rectype = ApexPages.currentPage().getParameters().get('recordTypeId');
        if(LineId !=null)
          movementtype = [select Movement_Type__c from ac__c where id =:LineId ].Movement_Type__c;
 
    }
    public list<SelectOption> getRecordTypes() {
        list<SelectOption> options = new list<SelectOption>();
        for (list<RecordType> rts : [SELECT ID, name FROM RecordType WHERE SObjectType = 'Location__c' ORDER BY name]) {
             if(movementtype != null  || movementtype != ''){
                 if(movementtype == 'Import'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                   // options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                  }else if(movementtype == 'Interstate Movement'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                    options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                  }else if(movementtype == 'Interstate Movement and Release'){
                    options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                    options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                    options.add(new SelectOption(locrectypeid, 'Release Sites Location'));
                 }else if(movementtype == 'Release'){
                  //  options.add(new SelectOption(olocrectypeid, 'Origin Location'));
                 //    options.add(new SelectOption(dlocrectypeid, 'Destination Location'));
                  //  options.add(new SelectOption(oanddlocrectypeid, 'Origin and Destination Location'));
                    options.add(new SelectOption(locrectypeid, 'Release Sites Location'));
                 }
                
           }else{
                for (RecordType rt : rts) {
                    options.add(new SelectOption(rt.ID, rt.Name));
                }
           }
        }
        options.add(0,new SelectOption('','--Select--'));
        return options;
    }
    
    
    public pageReference continueAssociate(){
        Profile p = [select name from Profile where id =
                     :UserInfo.getProfileId()];
        olocrectype = 0;
        dlocrectype = 0;
        list<Location__c> listloc = [select id, recordTypeId from Location__c where Line_Item__c =: LineId];
        if(movementtype !=null || movementtype != '' ){
            
           if(movementtype == 'Import' && listloc.size()>0){
  
    	     	    for(Location__c lc:listloc){
     	     	        if(lc.recordtypeid == olocrectypeid)
    	     	            olocrectype =  olocrectype+1;
    	     	         if(lc.recordtypeid == dlocrectypeId)
    	     	            dlocrectype = dlocrectype+1;
     	     	    } 
  
     	          If(olocrectype >= 1 && olocrectypeid == recordTypeId ){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 origin location'));
                     return null;
     	          }else  If(dlocrectype >= 1 && dlocrectypeId == recordTypeId ){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You can add only 1 destination location'));
                     return null;
     	          }
               
              }
               
           }
 
        
                     
        if ('APHIS Applicant'.equals(p.name)
            || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name))
            {
                PageReference customPage = Page.portal_Location_edit;
                customPage.getParameters().put('LineItemId', LineId);
                customPage.getParameters().put('strPathwayId',strPathwayId);
                customPage.getParameters().put('rectype',recordTypeId);
                customPage.getParameters().put('redirect',redirect);
                customPage.setRedirect(true);
                return customPage;
            }else{
                String hostname = ApexPages.currentPage().getHeaders().get('Host');
                String optyURL2 = 'https://'+hostname+'/'+ SObjectType.Location__c.keyPrefix +'/e?&RecordType='+recordTypeId+'&nooverride=1?';
                pagereference pageref = new pagereference(optyURL2);
                pageref.setredirect(true);
                return pageref;
            }

    }

    public pageReference cancel(){
        if(redirect == 'yes'){
            PageReference pg = Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }else{
        pageReference pg = new PageReference('/'+LineItemId);
        pg.setredirect(true);
        return pg;
        }
    }
}