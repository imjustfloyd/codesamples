global class CARPOL_BRS_ShareRecords{

   
    webservice static void processSharing(String wfId) // you can pass parameters
    { 
          system.debug('WF ### '+ wfId);
        Workflow_Task__c wf = [select id,Authorization__c from Workflow_Task__c where Id =: wfId];
        system.debug('wf ###'+wf);
        List <ProcessInstance> pi = [SELECT Id, CreatedDate from ProcessInstance where TargetObjectId = :wfId ORDER BY CreatedDate DESC limit 1];
        if(pi.size()>0){
        system.debug('pi ### '+ pi[0].Id);
        list<ProcessInstanceWorkitem> ApproverItem = [Select p.Id, p.ProcessInstanceId, p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId, p.ActorId, 
               p.OriginalActorId, p.CreatedById, p.CreatedDate from ProcessInstanceWorkitem p  where p.ProcessInstanceid = : pi[0].Id];
           system.debug('ApproverItem ### '+ ApproverItem);
           system.debug('Size ### '+ ApproverItem.size());    
           if(ApproverItem.size()>0){       
           // Create new sharing object for the custom object Job.
             Authorizations__Share AuthorizationShr  = new Authorizations__Share();
             Workflow_Task__Share Workflow_TaskShr  = new Workflow_Task__Share();
             
         
             // Set the ID of record being shared.
             AuthorizationShr.ParentId = wf.Authorization__c;
             Workflow_TaskShr.ParentId = wf.ID; 
             system.debug('AuthorizationShr.ParentId ###'+AuthorizationShr.ParentId);            
               
             // Set the ID of user or group being granted access.
             AuthorizationShr.UserOrGroupId = ApproverItem[0].ActorId;//userOrGroupId;
             Workflow_TaskShr.UserOrGroupId = ApproverItem[0].ActorId;//userOrGroupId;
             system.debug('AuthorizationShr.UserOrGroupId ###'+AuthorizationShr.UserOrGroupId);
               
             // Set the access level.
             AuthorizationShr.AccessLevel = 'Edit';
             Workflow_TaskShr.AccessLevel = 'Edit';
             system.debug('AuthorizationShr.AccessLevel ###'+AuthorizationShr.AccessLevel); 
              
             AuthorizationShr.RowCause = Schema.Authorizations__Share.RowCause.BRS_ROP_Sharing__c;
             Workflow_TaskShr.RowCause = Schema.Workflow_Task__Share.RowCause.BRS_ROP_Sharing__c;
             
               
             // Insert the sharing record and capture the save result.
             // The false parameter allows for partial processing if multiple records passed
             // into the operation.
             Database.SaveResult sr = Database.insert(AuthorizationShr,false); 
             system.debug('sr ###'+sr); 
             Database.SaveResult wr = Database.insert(Workflow_TaskShr,false);                
             

           }
        } 
     }

}