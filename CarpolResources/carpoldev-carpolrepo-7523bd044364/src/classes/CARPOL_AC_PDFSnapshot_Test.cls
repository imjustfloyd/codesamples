@isTest(seealldata=false)
private class CARPOL_AC_PDFSnapshot_Test {
      @IsTest static void CARPOL_AC_PDFSnapshot_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
          AC__c li = testData.newlineitem('Personal Use', objapp);
          Authorizations__c objauth = testData.newAuth(objapp.id);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Self_Reporting__c objlra = new Self_Reporting__c();
          objlra.Line_Events__c = objcaj.id;
          objlra.Monitoring_Period_Start__c = date.today();
          objlra.Monitoring_Period_End__c = date.today() + 60;
          objlra.Observation_Date__c = date.today() + 61;
          objlra.Number_of_Volunteers__c = 5;
          objlra.Action_Taken__c = 'Test description';
          objlra.Authorization__c = objauth.id;          
          insert objlra;

          List<Self_Reporting__c> newlist = new List<Self_Reporting__c>();
          newlist.add(objlra);
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_AC_ApplicationPDF;
              Test.setCurrentPage(pageRef);
              CARPOL_AC_PDFSnapshot.savePDF(objapp.Id,objapp.Id);
              System.assert(objapp != null);

                      
          Test.stopTest();   
      }
}