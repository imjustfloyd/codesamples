/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - AC Management Console Test Class
*/

@isTest(seealldata=false)
private class CARPOL_WizardQuestionnaire_Test {
    
    static testMethod void CARPOL_WizardQuestionnaire_Test() {
      
        PageReference pageRef = Page.CARPOL_WizardQuestionnaire;
        Test.setCurrentPage(pageRef);
        CARPOL_WizardQuestionnaire acMC = new CARPOL_WizardQuestionnaire();
        acMC.getPathDetails(); 
        List<Apexpages.Message> pageMessages1 = ApexPages.getMessages();
        
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);      
        Program_Prefix__c pp = testData.newPrefix();
        Signature__c tp = testData.newThumbprint();
        
                 
        acMC.getAllThumbprints();
        acMC.thumbprintID = '';
        List<Apexpages.Message> pageMessages2 = ApexPages.getMessages();    
        system.assert(acMC != null);                                         
    }
}