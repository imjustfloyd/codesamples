public class CARPOL_BRS_PDF_Labels_Extension {
  
 private ApexPages.StandardController stdCtrl {get; set;}
  
 public List<Label__c> labelList {get; set;}
    
 public CARPOL_BRS_PDF_Labels_Extension(ApexPages.StandardController std){
     stdCtrl=std;
     setupLabels();
 }
  
 private void setupLabels()
 {
     system.debug('LAbel query before is ');
    labelList = [select id,Name,QRcode_Barcode__c,Notification_Expiration_Date__c from Label__c where Authorization__c =:stdCtrl.getId()];
         system.debug('LAbel query after is '+labellist);    
 }

}