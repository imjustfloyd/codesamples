@isTest(seeAlldata=true)
public class CARPOL_UNI_MasterIncidentTrigger_test{
    
    @isTest
    public static void testCARPOL_UNI_MasterIncidentTrigger() {
       Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];
         
        system.runAs(u){
        Domain__c domain = new Domain__c(Name = 'BRS');
        insert domain;
        
        Program_Prefix__c pp = new Program_Prefix__c(Name = 'test', Prefix_Status__c = 'Active', Incident_Related__c=TRUE, Program__c = domain.Id);
        insert pp;
            
        Program_Workflow__c pw = new Program_Workflow__c();
          pw.Name = 'Testing';
          pw.Program_Prefix__c = pp .id;
          pw.Assign_to_Queue__c = 'BRS Compliance Officer Queue';
          pw.Program__c = domain.id;
          pw.Process_Type__c = 'Manual';
          pw.Active__c = 'Yes';
          pw.Task_Record_Type__c = 'Incident';
          pw.Email_Template__c = 'PCIR Acknowledgement';
          pw.Generate_as_Task__c = TRUE;
          insert pw;    
            
          String FacilityRecordTypeID = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
          Facility__c fac = new Facility__c();
          fac.Name = 'Port ABC';
          fac.Status__c = 'Active';
          fac.RecordTypeId = FacilityRecordTypeID;
          insert fac;
       
       Facility__c facc = [SELECT Id,Name,Status__c FROM Facility__c WHERE Id=:fac.id]; 
       
        Incident__c objInc=new Incident__c();
        objInc.status__c='Potential Incident';
        objInc.EFL_WFTasks_created__c=FALSE;
        objInc.Facility__c = facc.id;
        objInc.EFL_Origin__c= 'Inspection';
        objInc.EFL_Type_of_Activity__c = 'Facility';
        objInc.Related_Program__c='BRS';
        insert objInc;
        
            
                System.debug('<<<[Nitish] after insert incident record >>>');
        Test.startTest();
        objInc.Related_Program__c='BRS';
        objInc.status__c='Formal Incident' ;
        update objInc;  
        Test.stopTest(); 
       
        System.debug('<<<[Nitish] after update incident record >>>');
        Incident__c inci = [SELECT ID,Status__c FROM Incident__c WHERE ID=:objInc.id];
        //check if the incident is updated
        system.assertEquals('Formal Incident',objInc.Status__c);
        
        System.debug('<<<[Nitish] updated record value>>> '+ objInc.Status__c);
      
            
            
        objInc.status__c ='Not in Compliance';
        update objInc;
        Facility__c faccc = [SELECT Id,Name,Status__c FROM Facility__c WHERE Id=:objInc.Facility__c];
        faccc.Status__c = 'Inactive';
        update faccc;
       
        objInc.status__c ='Compliant';
        update objInc;
        Facility__c facility = [SELECT Id,Name,Status__c FROM Facility__c WHERE Id=:objInc.Facility__c];
        facility.Status__c = 'Active';
        update facility;
        
    }
    }
}