@isTest(seealldata=false)
private class CARPOL_UNI_ViewContactOverride_Test {
      @IsTest static void CARPOL_UNI_ViewContactOverride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();


          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_ViewContact;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcont);
              ApexPages.currentPage().getParameters().put('id',objcont.id);
              
              CARPOL_UNI_ViewContactOverride extclass = new CARPOL_UNI_ViewContactOverride(sc);
              extclass.redirect();
              system.assert(extclass != null);              
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_ViewContact;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcont);
              ApexPages.currentPage().getParameters().put('id',objcont.id);

              CARPOL_UNI_ViewContactOverride extclassempty = new CARPOL_UNI_ViewContactOverride();
              CARPOL_UNI_ViewContactOverride extclass = new CARPOL_UNI_ViewContactOverride(sc);
              extclass.redirect(); 
              system.assert(extclass != null);                       
          Test.stopTest();   
      }
}