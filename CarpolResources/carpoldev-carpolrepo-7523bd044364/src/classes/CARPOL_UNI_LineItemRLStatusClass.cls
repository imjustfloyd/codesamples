public class CARPOL_UNI_LineItemRLStatusClass{
    
   public void updateStatus(Workflow_Task__c wf){
    
     List<SObject> sObjectUpdates = new List<SObject>();
     //Workflow_Task__c wf = [select Id,Authorization__c,Update_Record_to_Specific_Value__c,Update_Record_Field_Name__c from Workflow_Task__c where Id =:wfId];
     
     //only seem to need the id for this
     AC__c LineItem = [select ID,Authorization__c,Applicant_Instructions__c,Status__c,RecordType.DeveloperName from AC__c where Authorization__c =: wf.Authorization__c limit 1 ];

     // Load Design Protocols
     list<Design_Protocol_Record__c> lstDesign = [select id,Name,Attaching_or_Entering_Design_Protocols__c,Corrections_Required__c,Design_Protocol_Name__c,Status__c  From Design_Protocol_Record__c where   Line_Item__c =:LineItem.Id];
     
     //SOP/Design Protocol ( Applicant Attachment )
     list<Applicant_Attachments__c> lstSOP = [select id,Name,Animal_Care_AC__c,Status__c  From Applicant_Attachments__c where Animal_Care_AC__c =:LineItem.Id];

     // Load Constructs,Genotypes and Phenotypes   
     list<Construct__c> lstConstruct = [select id,Construct_s__c,Corrections_Required__c,Status__c From Construct__C where Line_Item__c =:LineItem.Id]; 

     //Instantiate Location Lists
     List<String> locationTypes = new List<String>{'Origin_Location','Destination_Location','Origin_and_Destination','Release_Location'};  
     Map<String,List<Location__c>> locationMap = new Map<String,List<Location__c>>();

     //Load Locations
     List<Location__c> lstAll = [select ID,Name,City__c,Applicant_Instructions__c, Status__c,Description__c, Location_Id__c, RecordType.DeveloperName from Location__c where RecordType.DeveloperName IN :locationTypes and Line_Item__c =:LineItem.Id];
     //sort out the records     
     for(Location__c loc : lstAll){
         //match against types
         for(String type : locationTypes){
            if(loc.RecordType.DeveloperName == type){
                //put it in the map under the right key
                if(locationMap.containsKey(type)){
                    locationMap.get(type).add(loc);
                } else {
                    List<Location__c> locList = new List<Location__c>();
                    locList.add(loc);
                    locationMap.put(type,locList);
                }            
                //we found a match so move on to the next record
                break;
            }
         }
     }
    if(locationMap.get('Origin_Location') != null){
        for(Location__c location : locationMap.get('Origin_Location'))
        {
          if(wf.Update_Record_to_Specific_Value__c !=null){
              location.status__C = wf.Update_Record_to_Specific_Value__c;
              sObjectUpdates.add(location);
          }
        }
    }
    
    if(locationMap.get('Release_Location') != null){
        for(Location__c location : locationMap.get('Release_Location'))    
        {
          location.status__C = wf.Update_Record_to_Specific_Value__c;
          sObjectUpdates.add(location);      
        }
    }
    
    if(locationMap.get('Origin_and_Destination') != null){
        for(Location__c location : locationMap.get('Origin_and_Destination'))        
        {
          if(wf.Update_Record_to_Specific_Value__c !=null){
              location.status__C = wf.Update_Record_to_Specific_Value__c;
              sObjectUpdates.add(location);
          }
        }
    }

    if(locationMap.get('Destination_Location') != null){
        for(Location__c location : locationMap.get('Destination_Location'))            
        {
          if(wf.Update_Record_to_Specific_Value__c !=null){
              location.status__C = wf.Update_Record_to_Specific_Value__c;
              sObjectUpdates.add(location);
          }
        }
    }

    for(Construct__c construct : lstConstruct)
    {
      if(wf.Update_Record_to_Specific_Value__c !=null){
          construct.status__C = wf.Update_Record_to_Specific_Value__c;
          sObjectUpdates.add(construct);          
      }
    }

    for(Design_Protocol_Record__c designProtocol : lstDesign)
    {
      if(wf.Update_Record_to_Specific_Value__c !=null){
          designProtocol.status__C = wf.Update_Record_to_Specific_Value__c;
          sObjectUpdates.add(designProtocol);      
      }
    }
    
    for(Applicant_Attachments__c SOP : lstSOP)
    {
      if(wf.Update_Record_to_Specific_Value__c !=null){
          SOP.status__C = wf.Update_Record_to_Specific_Value__c;
          sObjectUpdates.add(SOP);      
      }
    }    
    update sObjectUpdates;            
   }
    
}