@isTest(seealldata=false)
public class CARPOL_AC_LettersTest {
/**
* CARPOL_AC_LettersTest.class
*
* @author  Kishore Kumar
* @date   07/5/2015
* @desc   Creates test data for the CARPOL_AC_Letters.
*/     
    static testMethod void AC_Letters() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      Test.startTest();
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Applicant_Contact__c apcont3 = testData.newappcontact();       
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      Country__c objaf = testData.newcountryaf();
      Level_1_Region__c objl1r = testData.newlevel1regionAL();
      Intended_Use__c objiu = testData.newintendeduse(plip.id);
      Application__c objapp = testData.newapplication();
      Regulated_Article__c objra = testData.newregulatedarticle(plip.id);  
      RA_Scientific_Names__c objrscn = testData.newScientificName();
      Signature__c objthumb = testData.newThumbprint();
      Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
      Wizard_Questionnaire__c objwiz = testData.newWizardQuestionnaire(plip.id, objiu.id);
      Authorizations__c objAuth = testData.newAuth(objapp.id);
      objthumb.Intended_Use__c = objiu.id;
      objthumb.Line_Record_Type__c = 'Live Dogs';
      objthumb.Regulated_Article__c = objra.id;
      objthumb.From_Country__c = objaf.id;
      objthumb.Communication_Manager__c = objcm.id;
      objthumb.Response_Type__c = 'Permit';
      update objthumb;
      objwiz.Signature__c = objthumb.id;
      update objwiz;
      AC__c ac = new AC__c();

      ac.Application_Number__c = objapp.id;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.id;
      ac.Port_of_Embarkation__c = fac2.id;
      ac.Date_of_Birth__c = date.today()-200;
      ac.Breed__c = objbrd.id;
      ac.Importer_Last_Name__c = apcont.id;
      ac.Exporter_Last_Name__c = apcont2.id;
      //ac.Status__c = 'Ready to Submit';
      ac.Purpose_of_the_Importation__c = 'Veterinary Treatment';
      ac.DeliveryRecipient_Last_Name__c = apcont3.id;
      ac.Program_Line_Item_Pathway__c = plip.Id;    
      insert ac;
      PageReference pageRef = Page.CARPOL_AC_LiveDogs_LOD_Formal;
        Test.setCurrentPage(pageRef);
        Cookie criteria = new Cookie('criteria', 'test data', null, 300, true);
        ApexPages.currentPage().setCookies(new Cookie[]{criteria});                
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
        ApexPages.currentPage().getParameters().put('Id',ac.id);
        ApexPages.currentPage().getParameters().put('TPId',objthumb.id);        
        ApexPages.currentPage().getParameters().put('applicationID',objapp.id);
        ApexPages.currentPage().getParameters().put('strPathway',plip.id);        
        ApexPages.currentPage().getParameters().put('Scientific_Name__c',objra.id);
        ApexPages.currentPage().getParameters().put('Country_Of_Origin__c',objaf.id);
        ApexPages.currentPage().getParameters().put('Intended_Use__c',objiu.id);
        ApexPages.currentPage().getParameters().put('State_Territory_of_Destination__c',objl1r.id);
        ApexPages.currentPage().getParameters().put('RA_Scientific_Name__c',objrscn.id);                                
        
        CARPOL_AC_Letters cls = new CARPOL_AC_Letters(sc);
        cls.getCriteria();
        cls.saveAttachment();
        CARPOL_AC_Processing prcs = new CARPOL_AC_Processing();
        prcs.processID= '23';
        prcs.pageLoadFunc(); 
        prcs.attachLetter();
        System.assert(prcs != null);
      Test.stopTest();        
    }
    
     static testMethod void AC_Letters1() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      Test.startTest();
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Applicant_Contact__c apcont3 = testData.newappcontact();       
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      Country__c objaf = testData.newcountryaf();
      Level_1_Region__c objl1r = testData.newlevel1regionAL();
      Intended_Use__c objiu = testData.newintendeduse(plip.id);
      Regulated_Article__c objra = testData.newregulatedarticle(plip.id);  
      RA_Scientific_Names__c objrscn = testData.newScientificName();
      Signature__c objthumb = testData.newThumbprint();
      Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
      Wizard_Questionnaire__c objwiz = testData.newWizardQuestionnaire(plip.id, objiu.id);
      Authorizations__c objAuth = testData.newAuth(objapp.id);
      objthumb.Intended_Use__c = objiu.id;
      objthumb.Line_Record_Type__c = 'Live Dogs';
      objthumb.Regulated_Article__c = objra.id;
      objthumb.From_Country__c = objaf.id;
      objthumb.Communication_Manager__c = objcm.id;
      objthumb.Response_Type__c = 'Permit';
      update objthumb;
      objwiz.Signature__c = objthumb.id;
      update objwiz;
        
      AC__c ac = new AC__c();

      ac.Application_Number__c = objapp.id;
      ac.Departure_Time__c = date.today()+11;
      ac.Arrival_Time__c = date.today()+15;
      ac.Port_of_Entry__c = fac.id;
      ac.Port_of_Embarkation__c = fac2.id;
      ac.Date_of_Birth__c = date.today()-200;
      ac.Breed__c = objbrd.id;
      ac.Importer_Last_Name__c = apcont.id;
      ac.Exporter_Last_Name__c = apcont2.id;
      //ac.Status__c = 'Ready to Submit';
      ac.Purpose_of_the_Importation__c = 'Veterinary Treatment';
      ac.DeliveryRecipient_Last_Name__c = apcont3.id;
      ac.Program_Line_Item_Pathway__c = plip.Id;    
      insert ac;

            
      PageReference pageRef = Page.CARPOL_AC_LiveDogs_LOD_Formal;
        Test.setCurrentPage(pageRef);
        //need to set the criteria cookie because the class is bypassing false using isRunningTest method (refactor later)
        Cookie criteria = new Cookie('criteria', 'test data', null, 300, true);
        ApexPages.currentPage().setCookies(new Cookie[]{criteria}); 
        ApexPages.currentPage().getParameters().put('Id',ac.id);
        ApexPages.currentPage().getParameters().put('TPId',objthumb.id);        
        ApexPages.currentPage().getParameters().put('applicationID',objapp.id);
        ApexPages.currentPage().getParameters().put('strPathway',plip.id);        
        ApexPages.currentPage().getParameters().put('Scientific_Name__c',objra.id);
        ApexPages.currentPage().getParameters().put('Country_Of_Origin__c',objaf.id);
        ApexPages.currentPage().getParameters().put('Intended_Use__c',objiu.id);
        ApexPages.currentPage().getParameters().put('State_Territory_of_Destination__c',objl1r.id);
        ApexPages.currentPage().getParameters().put('RA_Scientific_Name__c',objrscn.id);                             
           
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ac);
        CARPOL_AC_Letters cls = new CARPOL_AC_Letters(sc);
        cls.getCriteria();
        cls.saveAttachment();
        
        system.assert(sc != null);
        Test.stopTest();
     
     }

}