public with sharing class CARPOL_IncidentLetter {
    
    public Incident__c incident { get; set; }
    
    public CARPOL_IncidentLetter(ApexPages.StandardController controller) {
        
        incident = [SELECT Id,Name, Action__c, EFL_APHIS_Incident_Id__c,EFL_Compliance_letter_content__c,Application__c, EFL_Associated_Incidents__c, Authorization__c, EFL_Compliance_officer_Queue__c,  Facility__c, Facility__r.Name, Inspection__c, Inspection__r.Scheduled_Date_of_Inspection__c, Inspection__r.Inspector__r.Name ,Program_Compliance_officer__c, Related_Program__c, Requester_email__c, Requester_Name__c FROM Incident__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
    }
}