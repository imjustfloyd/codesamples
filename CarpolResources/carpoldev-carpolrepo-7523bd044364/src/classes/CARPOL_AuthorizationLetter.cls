public with sharing class CARPOL_AuthorizationLetter {
    
    public Authorizations__c authorization { get; set; }
    public boolean statepackage {get; set;}
    public Map<String, String> urlParms{get;set;}

    public CARPOL_AuthorizationLetter(ApexPages.StandardController controller) {
         statepackage = false;
         urlParms = ApexPages.currentPage().getParameters();
        if(urlParms.containsKey('Type'))
          {  statepackage = true;   } 
        authorization = [SELECT ID, Name, Authorization_Letter_Content__c, Authorization_Type__c, AC_Applicant_Name__c, Application__r.Name, Authorized_User__r.FirstName, 
        Authorized_User__r.LastName, AC_Applicant_Address__c, Template__c,Authorization_State_Letter_Content__c, 
        Date_Issued__c FROM Authorizations__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
    }
}