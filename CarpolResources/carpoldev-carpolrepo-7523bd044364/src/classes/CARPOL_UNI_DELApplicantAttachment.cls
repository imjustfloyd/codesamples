public class CARPOL_UNI_DELApplicantAttachment {
  
     List <Applicant_Attachments__c> oldAtt;
     List<AC__c> Linetoupdate;
     list<string> lstappids = new list<string>();
     list<AC__c> listlineitemToUpdate = new list<AC__c>();
     Map<ID, Schema.RecordTypeInfo> attrtMap = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosById();

// This is the constructor
  public CARPOL_UNI_DELApplicantAttachment(List <Applicant_Attachments__c> oldTriggerAtts ) {
      oldAtt = oldTriggerAtts;
      Linetoupdate = [SELECT ID, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Program__r.Name, Status__c FROM AC__c WHERE ID=:oldAtt[0].Animal_Care_AC__c LIMIT 1];
  }
 
 
 public void ApplicantAttachments()  {
if(Linetoupdate.size()>0){
 if(Linetoupdate[0].Program_Line_Item_Pathway__r.Program__r.Name == 'BRS'){
       for(Applicant_Attachments__c aa : oldAtt)
       {
           string attrt = attrtMap.get(aa.RecordTypeId).getName();
           If(aa.Animal_Care_AC__c != null && (attrt == 'BRS Notification SOP' || attrt == 'BRS Standard Permit SOP')){
              lstappids.add(aa.Animal_Care_AC__c);
            }         
         } 
        List<AC__c> LineItemList = [select id, (select id from Applicant_Attachments__r) from AC__c where id in : lstappids];
        for(AC__c a : LineItemList) {
           	if(a.Applicant_Attachments__r.size() == 0){
            	     	    a.SOP_Status__c = 'Yet to Add';
            	     	    listlineitemToUpdate.add(a); 
                 }
               }
      if(listlineitemToUpdate.size()>0){
              update listlineitemToUpdate;
         }       
      }
    }
 }
    
    }