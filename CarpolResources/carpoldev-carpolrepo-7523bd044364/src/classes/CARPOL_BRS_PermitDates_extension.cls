public class CARPOL_BRS_PermitDates_extension {

    public Authorizations__c auth {get; set;}
    public String AuthorizationID = ApexPages.currentPage().getParameters().get('id');
     public CARPOL_BRS_PermitDates_extension(ApexPages.StandardController stdController) {
        this.auth = (Authorizations__c)stdController.getRecord();
        
        if(auth.ID != NULL){
            auth = [SELECT Effective_Date__c, pathway_exp_days__c, Expiration_Date__c,Expiry_Date__c FROM Authorizations__c WHERE ID =: auth.ID];
        }

        if(auth.pathway_exp_days__c!=null && auth.Expiry_Date__c ==null){
            auth.Expiry_Date__c = System.Today().AddDays(integer.valueOf(auth.pathway_exp_days__c));
        }else if(auth.Expiry_Date__c == null) {
                 auth.Expiry_Date__c = System.Today().AddDays(30); }
              
        if(auth.Effective_Date__c == null) {       
            auth.Effective_Date__c = System.Today();}
     }

    public PageReference save()
    {
          
         try{

            upsert auth;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
             return null;
        }
        catch(Exception e)
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }    
}