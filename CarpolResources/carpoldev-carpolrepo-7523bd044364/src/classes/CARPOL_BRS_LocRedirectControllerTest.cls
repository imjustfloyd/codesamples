/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRS_LocRedirectControllerTest
@CreateDate: 
@Description: Test class for CARPOL_BRS_LocRedirectController
@Version: 1.0
@reference: CARPOL_BRS_LocRedirectController(apex class)
*/
@isTest
public class CARPOL_BRS_LocRedirectControllerTest
{
    public static testmethod void testm()
    {
            RecordType RecType = [Select Id,Name From RecordType  Where SobjectType = 'Location__c' and DeveloperName = 'Release_Location'];
            ApexPages.currentPage().getParameters().put('RecordType','01pr00000004OdC');
            ApexPages.currentPage().getHeaders().put('Host', 'test');
            PageReference pg = new pagereference('/apex/carpol_brs_location');
            test.setcurrentpage(pg);
            CARPOL_BRS_LocRedirectController c = new CARPOL_BRS_LocRedirectController();
            c.Redirect();            
            system.assert(c != null);
    }
}