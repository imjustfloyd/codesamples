public class CARPOL_UNI_ViewAssocContactsOverride {
  public CARPOL_UNI_ViewAssocContactsOverride() {

  }


 String recordId;

public CARPOL_UNI_ViewAssocContactsOverride(ApexPages.StandardController
     controller) {recordId = controller.getId();}

public PageReference redirect() {
Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
if ('APHIS Applicant'.equals(p.name)
    || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name)||'State Reviewer'.equals(p.name))
    {
     PageReference customPage =
Page.Portal_Associate_Contact_Detail;
     customPage.setRedirect(true);
     customPage.getParameters().put('id', recordId);
     return customPage;
    } else {
    String hostname = ApexPages.currentPage().getHeaders().get('Host');
           String sendURL = 'https://'+hostname+'/'+recordID +'?nooverride=1';
           pagereference pageref = new pagereference(sendURL);
           pageref.setredirect(true);
           return pageref;

       return null; //otherwise stay on the same page
    }
 }
}