public with sharing class CARPOL_BRS_NEPA_Decision_Document {

ApexPages.StandardController stdController;

public id recordid {get;set;}
public Auth_Related_Records__c objauthrel {get;set;}
public string authid {get;set;}
public string arrid {get;set;}
public boolean checkboxna {get;set;}
public boolean checkboxtab2 {get;set;}
public boolean checkboxtab3 {get;set;}
public list<Authorizations__c> authList {get;set;}
public list<Workflow_Task__c> wtList {get;set;}
 public string wfid{get; set;} 

    public CARPOL_BRS_NEPA_Decision_Document(ApexPages.StandardController con)
    {
        stdController = con;
     } 
     
    public void potentialcheckbox2() {
     potentialcheckbox(); 
     }
   
   public void arronload() {
    tab1val = false;
    tab2val = false;
    tab3val = false;
    tab4val = false;
    checkboxna = false;
    checkboxtab2 = false;
    checkboxtab3 = false;
    arrid = '';
    authid = ApexPages.CurrentPage().getParameters().get('authid');
    arrid = ApexPages.CurrentPage().getParameters().get('id');
    
    if(arrid!='' && arrid!=null){
        list<Auth_Related_Records__c> arrlst = [select id,Name,Authorization__c,Authorization__r.name,X1_Does_this_Document_contain_CBI__c,X2_Is_the_recipient_a_plant__c,X3_Is_duration_greater_than_one_year__c,X4_Is_plant_a_noxious_weed_species__c,X5_Derived_from_an_animal_or_human_virus__c,X5a_REQUIRED_COMMENTS_Q5__c,Is_introduced_genetic_material_known__c,X7_Is_likely_to_cause_disease__c,X7a_REQUIRED_COMMENTS_Q7__c,X8_Does_Gen_mat_result_in_plant_disease__c,
                                                X9_Can_result_in_any_infectious_entity__c,X10_Can_be_toxic_to_non_target_organisms__c,X10a_REQUIRED_COMMENTS_Q10__c,X11_Intended_for_phar_or_industrial_use__c,X12_Non_coding_regulatory_sequences__c,X12a_REQUIRED_COMMENTS_Q12__c,X13_Sequences_from_coding_regions__c,X13a_REQUIRED_COMMENTS_Q13__c,X14_Cell_to_cell_movement_of_the_virus__c,X14a_REQUIRED_COMMENTS_Q14__c,X15_Foreign_DNA_in_the_plant_genome__c,X15a_Comment_if_a_novel_method__c,
                                                X16_Article_qualify_s_for_Notification__c,X16a_REQUIRED_COMMENTS_Q16__c,M1_Potential_for_enviornmental_Impact__c,M2_Unsafe_licensed_approved_biologic__c,M3_Contains_live_microorganisms__c,M4_New_species_or_organisms_new_issues__c,M5_Effect_quality_of_human_environment__c,M6_Between_contained_facilities__c,M7_Measures_used_to_avoid_impact__c,M8_ESA_Assessment_Movement__c,M8a_REQUIRED_COMMENTS_QM8__c,R1_Impact_of_proposed_release__c,R1a_REQUIRED_COMMENTS_QR1__c,R2_Involve_approved_vet_biologic__c,
                                                R2a_REQUIRED_COMMENTS_QR2__c,R3_Is_unlicensed_vet_biological_product__c,R3a_REQUIRED_COMMENTS_QR3__c,R4_Release_a_genetically_eng_organism__c,R4a_REQUIRED_COMMENTS_QR4__c,R5_Involve_new_species_organisms__c,R5a_REQUIRED_COMMENTS_QR5__c,R6_Modifications_that_raise_new_issues__c,R6a_REQUIRED_COMMENTS_QR6__c,R7_Significant_impact_to_Human_Environ__c,R7a_REQUIRED_COMMENTS_QR7__c,R8_Within_Reservation_Lands__c,R9_Is_plant_sexually_compatible__c,R10_Release_in_critical_habitat__c,R10a_REQUIRED_COMMENTS_QR10__c,
                                                R11_Can_cause_disease_in_humans__c,R11a_REQUIRED_COMMENTS_QR11__c,R12_Is_toxic_to_non_target_organisms__c,R12a_REQUIRED_COMMENTS_QR12__c,R13_Effects_to_TES_or_critical_habitat__c,R14_Has_NO_EFFECT_on_Critical_Habitat__c,R14a_REQUIRED_COMMENTS_QR14__c,X17_Categorical_exclusions_under_NEPA__c,X17a_REQUIRED_COMMENTS_Q17__c,X18_Do_exceptions_to_exclusions_apply__c,X18a_REQUIRED_COMMENTS_Q18__c,
                                                M4a_REQUIRED_COMMENTS_QM4__c,M5a_REQUIRED_COMMENTS_QM5__c,M6a_REQUIRED_COMMENTS_QM6__c,M7a_REQUIRED_COMMENTS_QM7__c,R8a_REQUIRED_COMMENTS_QR8__c,R9a_REQUIRED_COMMENTS_QR9__c,R13a_REQUIRED_COMMENTS_QR13__c,M1a_REQUIRED_COMMENTS_QM1__c,M2a_REQUIRED_COMMENTS_QM2__c,M3a_REQUIRED_COMMENTS_QM3__c,X11a_REQUIRED_COMMENTS_Q11__c,X1a_REQUIRED_COMMENTS_Q1__c,X2a_REQUIRED_COMMENTS_Q2__c,X3a_REQUIRED_COMMENTS_Q3__c,X4a_REQUIRED_COMMENTS_Q4__c,X6a_REQUIRED_COMMENTS_Q6__c,
                                                X8a_REQUIRED_COMMENTS_Q8__c,X9a_REQUIRED_COMMENTS_Q9__c from Auth_Related_Records__c where id=:arrid];
          objauthrel = arrlst[0];                                          
    }
    else{
         objauthrel = new Auth_Related_Records__c();    
         objauthrel.Authorization__c = authid;
    } 
     
      if(authid =='' || authid == null){
        authid = [select id, Authorization__c from Auth_Related_Records__c where id=:arrid ].Authorization__c;  }
        
       if(authid !='' && authid != null){ 
        wfid = [select id,name,Workflow_Order__c,Authorization__c from Workflow_Task__c where Workflow_Order__c = 2 and Authorization__c =:authid].id; 
        authList = [select id,name,BRS_Introduction_Type__c from Authorizations__c where id =:authid]; }
          
    }

    public PageReference doupdate() {  
              potentialcheckbox(); 
               upsert objauthrel;
              PageReference redirectPage = Page.CARPOL_BRS_NEPA_Decision_Document;
               redirectPage.getParameters().put('id',objauthrel.id); 
              redirectPage.getParameters().put('authid',authid );
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved Successfully!'));
              redirectPage.setRedirect(true); 
              return redirectPage;
         
        If(arrid != null){
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved Successfully!'));
        }
        
       return null;    
    }
    

    public void potentialcheckbox()
    {
       Auth_Related_Records__c objauthre2 = new Auth_Related_Records__c(); 
          
        // tab 2
        if(checkboxtab2==true){
        objauthrel.M1_Potential_for_enviornmental_Impact__c = 'N/A';
        objauthrel.M1a_REQUIRED_COMMENTS_QM1__c= 'N/A';
        objauthrel.M2_Unsafe_licensed_approved_biologic__c= 'N/A';
        objauthrel.M2a_REQUIRED_COMMENTS_QM2__c= 'N/A';
        objauthrel.M3_Contains_live_microorganisms__c= 'N/A';
        objauthrel.M3a_REQUIRED_COMMENTS_QM3__c= 'N/A';
        objauthrel.M4_New_species_or_organisms_new_issues__c= 'N/A';
        objauthrel.M4a_REQUIRED_COMMENTS_QM4__c= 'N/A';
        objauthrel.M5_Effect_quality_of_human_environment__c= 'N/A';
        objauthrel.M5a_REQUIRED_COMMENTS_QM5__c= 'N/A';
        objauthrel.M6_Between_contained_facilities__c= 'N/A';
        objauthrel.M6a_REQUIRED_COMMENTS_QM6__c= 'N/A';
        objauthrel.M7_Measures_used_to_avoid_impact__c= 'N/A';
        objauthrel.M7a_REQUIRED_COMMENTS_QM7__c= 'N/A';
        objauthrel.M8_ESA_Assessment_Movement__c= 'N/A';
        objauthrel.M8a_REQUIRED_COMMENTS_QM8__c= 'N/A';    
        }    
        // tab 3
        if(checkboxtab3==true){
        objauthrel.R1_Impact_of_proposed_release__c= 'N/A';
        objauthrel.R1a_REQUIRED_COMMENTS_QR1__c= 'N/A';
        objauthrel.R2_Involve_approved_vet_biologic__c= 'N/A';
        objauthrel.R2a_REQUIRED_COMMENTS_QR2__c= 'N/A';
        
        objauthrel.R3_Is_unlicensed_vet_biological_product__c= 'N/A';
        objauthrel.R3a_REQUIRED_COMMENTS_QR3__c= 'N/A';
        
        objauthrel.R4_Release_a_genetically_eng_organism__c= 'N/A';
        objauthrel.R4a_REQUIRED_COMMENTS_QR4__c= 'N/A';
        
        objauthrel.R5_Involve_new_species_organisms__c= 'N/A';
        objauthrel.R5a_REQUIRED_COMMENTS_QR5__c= 'N/A';
        
        objauthrel.R6_Modifications_that_raise_new_issues__c= 'N/A';
        objauthrel.R6a_REQUIRED_COMMENTS_QR6__c= 'N/A';
        
        objauthrel.R7_Significant_impact_to_Human_Environ__c= 'N/A';
        objauthrel.R7a_REQUIRED_COMMENTS_QR7__c= 'N/A';
        
        objauthrel.R8_Within_Reservation_Lands__c= 'N/A';
        objauthrel.R8a_REQUIRED_COMMENTS_QR8__c= 'N/A';
        
        objauthrel.R9_Is_plant_sexually_compatible__c= 'N/A';
        objauthrel.R9a_REQUIRED_COMMENTS_QR9__c= 'N/A';
        
        objauthrel.R10_Release_in_critical_habitat__c= 'N/A';
        objauthrel.R10a_REQUIRED_COMMENTS_QR10__c= 'N/A';
        
        objauthrel.R11_Can_cause_disease_in_humans__c= 'N/A';
        objauthrel.R11a_REQUIRED_COMMENTS_QR11__c= 'N/A';
        
        objauthrel.R12_Is_toxic_to_non_target_organisms__c= 'N/A';
        objauthrel.R12a_REQUIRED_COMMENTS_QR12__c= 'N/A';
        objauthrel.R13_Effects_to_TES_or_critical_habitat__c= 'N/A';
        objauthrel.R13a_REQUIRED_COMMENTS_QR13__c= 'N/A';
        objauthrel.R14_Has_NO_EFFECT_on_Critical_Habitat__c= 'N/A';
        objauthrel.R14a_REQUIRED_COMMENTS_QR14__c= 'N/A';
        }
     }

   public boolean tab1val {get;set;} 
   public boolean tab2val {get;set;}
   public boolean tab3val {get;set;}
   public boolean tab4val {get;set;}
   public List<SelectOption> getseleoptions(string fieldapiname) {
        Schema.sObjectType sobject_type = Auth_Related_Records__c.getSObjectType();

            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();

            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap();
          
            List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldapiname).getDescribe().getPickListValues();

            List<selectOption> options = new List<selectOption>();

           for (Schema.PicklistEntry a : pick_list_values) {
                      options.add(new selectOption(a.getLabel(), a.getValue()));
          }
      return options;
    }
   public List<SelectOption> getX1types() {
      return getseleoptions('X1_Does_this_Document_contain_CBI__c');
    }
    
     public List<SelectOption> getX2types() {
       
      return getseleoptions('X2_Is_the_recipient_a_plant__c');
    }

    public List<SelectOption> getx3Types(){
      return getseleoptions('X3_Is_duration_greater_than_one_year__c');
      }
      
      public List<SelectOption> getx4Types(){
      return getseleoptions('X4_Is_plant_a_noxious_weed_species__c');
      }
      
      public List<SelectOption> getx5Types(){
      return getseleoptions('X5_Derived_from_an_animal_or_human_virus__c');
      }

      public List<SelectOption> getx6Types(){
          return getseleoptions('Is_introduced_genetic_material_known__c');
      }
      
      public List<SelectOption> getx7Types(){
          return getseleoptions('X7_Is_likely_to_cause_disease__c');
      }
      
      public List<SelectOption> getx8Types(){
          return getseleoptions('X8_Does_Gen_mat_result_in_plant_disease__c');
      }
       public List<SelectOption> getx9Types(){
          return getseleoptions('X9_Can_result_in_any_infectious_entity__c');
      }
      public List<SelectOption> getx10Types(){
          return getseleoptions('X10_Can_be_toxic_to_non_target_organisms__c');
      }
      public List<SelectOption> getx11Types(){
          return getseleoptions('X11_Intended_for_phar_or_industrial_use__c');
      }
      public List<SelectOption> getx12Types(){
          return getseleoptions('X12_Non_coding_regulatory_sequences__c');
      }
       public List<SelectOption> getx13Types(){
          return getseleoptions('X13_Sequences_from_coding_regions__c');
      }
       public List<SelectOption> getx14Types(){
          return getseleoptions('X14_Cell_to_cell_movement_of_the_virus__c');
      }
       public List<SelectOption> getx15Types(){
          return getseleoptions('X15_Foreign_DNA_in_the_plant_genome__c');
      }
      public List<SelectOption> getx16Types(){
          return getseleoptions('X16_Article_qualify_s_for_Notification__c');
      }
      public List<SelectOption> getx17Types(){
          return getseleoptions('X17_Categorical_exclusions_under_NEPA__c');
      }
      public List<SelectOption> getx18Types(){
          return getseleoptions('X18_Do_exceptions_to_exclusions_apply__c');
      } 
      public List<SelectOption> getM1Types(){
          return getseleoptions('M1_Potential_for_enviornmental_Impact__c');
      } 
       public List<SelectOption> getM2Types(){
          return getseleoptions('M2_Unsafe_licensed_approved_biologic__c');
      } 
      public List<SelectOption> getM3Types(){
          return getseleoptions('M3_Contains_live_microorganisms__c');
      } 
       public List<SelectOption> getM4Types(){
          return getseleoptions('M4_New_species_or_organisms_new_issues__c');
      }
     public List<SelectOption> getM5Types(){
          return getseleoptions('M5_Effect_quality_of_human_environment__c');
      } 
       public List<SelectOption> getM6Types(){
          return getseleoptions('M6_Between_contained_facilities__c');
      } 
       public List<SelectOption> getM7Types(){
          return getseleoptions('M7_Measures_used_to_avoid_impact__c');
      } 
       public List<SelectOption> getM8Types(){
          return getseleoptions('M8_ESA_Assessment_Movement__c');
      } 
       public List<SelectOption> getR1Types(){
          return getseleoptions('R1_Impact_of_proposed_release__c');
      } 
       public List<SelectOption> getR2Types(){
          return getseleoptions('R2_Involve_approved_vet_biologic__c');
      } 
       public List<SelectOption> getR3Types(){
          return getseleoptions('R3_Is_unlicensed_vet_biological_product__c');
      } 
        public List<SelectOption> getR4Types(){
          return getseleoptions('R4_Release_a_genetically_eng_organism__c');
      } 
       public List<SelectOption> getR5Types(){
          return getseleoptions('R5_Involve_new_species_organisms__c');
      } 
       public List<SelectOption> getR6Types(){
          return getseleoptions('R6_Modifications_that_raise_new_issues__c');
      } 
       public List<SelectOption> getR7Types(){
          return getseleoptions('R7_Significant_impact_to_Human_Environ__c');
      } 
       public List<SelectOption> getR8Types(){
          return getseleoptions('R8_Within_Reservation_Lands__c');
      } 
       public List<SelectOption> getR9Types(){
          return getseleoptions('R9_Is_plant_sexually_compatible__c');
      }
       public List<SelectOption> getR10Types(){
          return getseleoptions('R10_Release_in_critical_habitat__c');
      } 
       public List<SelectOption> getR11Types(){
          return getseleoptions('R11_Can_cause_disease_in_humans__c');
      } 
       public List<SelectOption> getR12Types(){
          return getseleoptions('R12_Is_toxic_to_non_target_organisms__c');
      } 
       public List<SelectOption> getR13Types(){
          return getseleoptions('R13_Effects_to_TES_or_critical_habitat__c');
      } 
       public List<SelectOption> getR14Types(){
          return getseleoptions('R14_Has_NO_EFFECT_on_Critical_Habitat__c');
      } 
 
    public PageReference submit() { 
        try{
                system.debug('Inside Save Method');
                tab1val = false;
                tab2val = false;
                tab3val = false;
                tab4val = false;
                integer errcount = 0;
                // if checkbox is true
                
                   potentialcheckbox();
                
                // tab1 validation
                if(objauthrel.X1_Does_this_Document_contain_CBI__c==null){
                    tab1val = true;
                    errcount ++;
                } 
                //if(objauthrel.X1a_REQUIRED_COMMENTS_Q1__c==null){
               //     tab1val = true;
                 //   errcount ++;
                //} 
                if(objauthrel.X2_Is_the_recipient_a_plant__c==null){
                    tab1val = true;
                    errcount ++;
                } 
             /*   if(objauthrel.X2a_REQUIRED_COMMENTS_Q2__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X3_Is_duration_greater_than_one_year__c==null){
                    tab1val = true;
                    errcount ++;
                }                
            /*     if(objauthrel.X3a_REQUIRED_COMMENTS_Q3__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X4_Is_plant_a_noxious_weed_species__c==null){
                    tab1val = true;
                    errcount ++;
                }                
              /*   if(objauthrel.X4a_REQUIRED_COMMENTS_Q4__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X5_Derived_from_an_animal_or_human_virus__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X5a_REQUIRED_COMMENTS_Q5__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.Is_introduced_genetic_material_known__c==null){
                    tab1val = true;
                    errcount ++;
                }
             /*    if(objauthrel.X6a_REQUIRED_COMMENTS_Q6__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X7_Is_likely_to_cause_disease__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X7a_REQUIRED_COMMENTS_Q7__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X8_Does_Gen_mat_result_in_plant_disease__c==null){
                    tab1val = true;
                    errcount ++;
                }
             /*    if(objauthrel.X8a_REQUIRED_COMMENTS_Q8__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X9_Can_result_in_any_infectious_entity__c==null){
                    tab1val = true;
                    errcount ++;
                }
             /*    if(objauthrel.X9a_REQUIRED_COMMENTS_Q9__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X10_Can_be_toxic_to_non_target_organisms__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X10a_REQUIRED_COMMENTS_Q10__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X11_Intended_for_phar_or_industrial_use__c==null){
                    tab1val = true;
                    errcount ++;
                }
            /*    if(objauthrel.X11a_REQUIRED_COMMENTS_Q11__c==null){
                    tab1val = true;
                    errcount ++;
                } */ 
                if(objauthrel.X12_Non_coding_regulatory_sequences__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X12a_REQUIRED_COMMENTS_Q12__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X13_Sequences_from_coding_regions__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X13a_REQUIRED_COMMENTS_Q13__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X14_Cell_to_cell_movement_of_the_virus__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X14a_REQUIRED_COMMENTS_Q14__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X15_Foreign_DNA_in_the_plant_genome__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X15a_Comment_if_a_novel_method__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X16_Article_qualify_s_for_Notification__c==null){
                    tab1val = true;
                    errcount ++;
                }
                if(objauthrel.X16a_REQUIRED_COMMENTS_Q16__c==null){
                    tab1val = true;
                    errcount ++;
                }
                // tab2 validation
        IF((authList[0].BRS_Introduction_Type__c != 'Release')&&(authList[0].BRS_Introduction_Type__c != 'Interstate Movement and Release')) {         
                if(objauthrel.M1_Potential_for_enviornmental_Impact__c==null){
                    tab2val = true;
                    errcount ++;
                }
             /*   if(objauthrel.M1a_REQUIRED_COMMENTS_QM1__c==null){
                    tab2val = true;
                    errcount ++;
                } */  
                if(objauthrel.M2_Unsafe_licensed_approved_biologic__c==null){
                    tab2val = true;
                    errcount ++;
                }
              /*  if(objauthrel.M2a_REQUIRED_COMMENTS_QM2__c==null){
                    tab2val = true;
                    errcount ++;
                } */  
                if(objauthrel.M3_Contains_live_microorganisms__c==null){
                    tab2val = true;
                    errcount ++;
                }
              /*   if(objauthrel.M3a_REQUIRED_COMMENTS_QM3__c==null){
                    tab2val = true;
                    errcount ++;
                } */  
                if(objauthrel.M4_New_species_or_organisms_new_issues__c==null){
                    tab2val = true;
                    errcount ++;
                }
             /*    if(objauthrel.M4a_REQUIRED_COMMENTS_QM4__c==null){
                    tab2val = true;
                    errcount ++;
                } */ 
                if(objauthrel.M5_Effect_quality_of_human_environment__c==null){
                    tab2val = true;
                    errcount ++;
                }
              /*   if(objauthrel.M5a_REQUIRED_COMMENTS_QM5__c==null){
                    tab2val = true;
                    errcount ++;
                } */ 
                if(objauthrel.M6_Between_contained_facilities__c==null){
                    tab2val = true;
                    errcount ++;
                }
              /*  if(objauthrel.M6a_REQUIRED_COMMENTS_QM6__c==null){
                    tab2val = true;
                    errcount ++;
                } */  
                if(objauthrel.M7_Measures_used_to_avoid_impact__c==null){
                    tab2val = true;
                    errcount ++;
                }
             /*   if(objauthrel.M7a_REQUIRED_COMMENTS_QM7__c==null){
                    tab2val = true;
                    errcount ++;
                } */
                if(objauthrel.M8_ESA_Assessment_Movement__c==null){
                    tab2val = true;
                    errcount ++;
                }
                if(objauthrel.M8a_REQUIRED_COMMENTS_QM8__c==null){
                    tab2val = true;
                    errcount ++;
                }
        }        
                // tab3 validation
             IF(authList[0].BRS_Introduction_Type__c == 'Release') {   
                if(objauthrel.R1_Impact_of_proposed_release__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R1a_REQUIRED_COMMENTS_QR1__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R2_Involve_approved_vet_biologic__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R2a_REQUIRED_COMMENTS_QR2__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R3_Is_unlicensed_vet_biological_product__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R3a_REQUIRED_COMMENTS_QR3__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R4_Release_a_genetically_eng_organism__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R4a_REQUIRED_COMMENTS_QR4__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R5_Involve_new_species_organisms__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R5a_REQUIRED_COMMENTS_QR5__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R6_Modifications_that_raise_new_issues__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R6a_REQUIRED_COMMENTS_QR6__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R7_Significant_impact_to_Human_Environ__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R7a_REQUIRED_COMMENTS_QR7__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R8_Within_Reservation_Lands__c==null){
                    tab3val = true;
                    errcount ++;
                }
             /*   if(objauthrel.R8a_REQUIRED_COMMENTS_QR8__c==null){
                    tab3val = true;
                    errcount ++;
                } */  
                if(objauthrel.R9_Is_plant_sexually_compatible__c==null){
                    tab3val = true;
                    errcount ++;
                }
             /*   if(objauthrel.R9a_REQUIRED_COMMENTS_QR9__c==null){
                    tab3val = true;
                    errcount ++;
                } */   
                if(objauthrel.R10_Release_in_critical_habitat__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R10a_REQUIRED_COMMENTS_QR10__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R11_Can_cause_disease_in_humans__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R11a_REQUIRED_COMMENTS_QR11__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R12_Is_toxic_to_non_target_organisms__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R12a_REQUIRED_COMMENTS_QR12__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R13_Effects_to_TES_or_critical_habitat__c==null){
                    tab3val = true;
                    errcount ++;
                }
              /*  if(objauthrel.R13a_REQUIRED_COMMENTS_QR13__c==null){
                    tab3val = true;
                    errcount ++;
                } */ 
                if(objauthrel.R14_Has_NO_EFFECT_on_Critical_Habitat__c==null){
                    tab3val = true;
                    errcount ++;
                }
                if(objauthrel.R14a_REQUIRED_COMMENTS_QR14__c==null){
                    tab3val = true;
                    errcount ++;
                }
                
             }
                // tab4 validation
                if(objauthrel.X17_Categorical_exclusions_under_NEPA__c==null){
                    tab4val = true;
                    errcount ++;
                }
                if(objauthrel.X17a_REQUIRED_COMMENTS_Q17__c==null){
                    tab4val = true;
                    errcount ++;
                }
                if(objauthrel.X18_Do_exceptions_to_exclusions_apply__c==null){
                    tab4val = true;
                    errcount ++;
                }
                if(objauthrel.X18a_REQUIRED_COMMENTS_Q18__c==null){
                    tab4val = true;
                    errcount ++;
                } 
                system.debug('errcount ####'+errcount);
                if(errcount>0){
                     ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill in all required fields');
                     ApexPages.addMessage(myMsg);
                     return null;
                } 
                else{
                    upsert objauthrel;
                    PageReference pagref = new PageReference('/'+authid);  //added to line 609
                    return pagref;
                }
        } 
          catch(Exception e){
            system.debug('---Exception---'+e);
            return null;
          }
       
    }
 
    public PageReference cancel() {
                    PageReference pagref = new PageReference('/'+authid);  //added to line 609
                    return pagref;
     }
     
}