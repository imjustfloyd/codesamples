@isTest(seealldata=false)
private class CARPOL_AC_CopyContactsTest {
    static testMethod void AC_CopyCont() {
    
    String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Applicant_Contact__c apcont3 = testData.newappcontact();        
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port'); 
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newlineitem('Personal Use',objapp);
      System.assert(ac != null);
      }
}