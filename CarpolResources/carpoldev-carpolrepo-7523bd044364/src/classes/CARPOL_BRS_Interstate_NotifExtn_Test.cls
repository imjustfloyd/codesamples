/* Project      : Biotechnolory Regulatory Service
 * Description  : This is the test class for CARPOL_BRS_Interstate_NotifExtension
 * Authors: Siddarth Asokan
 * ======================================================================
 *          Date        Author    Purpose
 * Changes: 10/01/2015  Sid       Initial Version
 * ======================================================================
 */
@istest
public with sharing class CARPOL_BRS_Interstate_NotifExtn_Test{
    Static Authorizations__c objAuth;
        static{
            objAuth = new Authorizations__c();
            objAuth.Authorization_Type__c='Acknowledgement';
            insert objAuth;
        }
    public static testmethod void tstMethodPdf(){
        PageReference pageRef = Page.CARPOL_BRS_Interstate_Move_and_Rel_Notif;
        pageRef.getParameters().put('id', objAuth.id);
        test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(new Authorizations__c());
        CARPOL_BRS_Interstate_NotifExtension objExt = new CARPOL_BRS_Interstate_NotifExtension(con);
        system.assert(objExt != null);
    }
}