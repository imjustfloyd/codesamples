public with sharing class CARPOL_UNI_Contact_Lookup_Controller {
    public string searchString{get;set;} // search keyword, TODO: need to put some error checking on the get method for web safety
    public string newasscont {get;set;}
    public Boolean showMessage {get;set;}
    public Applicant_Contact__c objImpContactNew {get;set;}    
    public List<Applicant_Contact__c> newContacts {get;set;}
    public Contact contactDetails { get; set; }
       
    public CARPOL_UNI_Contact_Lookup_Controller() {
        searchString = '';
        newasscont = '';
        showMessage = false;
        objImpContactNew = new Applicant_Contact__c();
        newContacts = new List<Applicant_Contact__c>();
        //contactDetails = [SELECT ID, FirstName, LastName, Account.Name, Phone, Fax, Email, Additional_Email__c, Mailing_Street_LR__c, Mailing_City_LR__c, Mailing_State_Province_LR__c, Mailing_Country_LR__c, Mailing_Zip_Postalcode_LR__c, Name FROM Contact WHERE ID = : appDetails.Applicant_Name__c LIMIT 1];        
    }
    
  public PageReference search() {
    getperformSearch();
    return null;
  } 
  
  // This method is used to get associated contact records in page block table in pop-up 
     public List<Applicant_Contact__c> getperformSearch() { 
        List<Applicant_Contact__c> contactsearch = new List<Applicant_Contact__c>();
        String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String soql = 'select id, name, First_Name__c ,Email_Address__c,USDA_Registration_Number__c,USDA_License_Number__c,USDA_License_Expiration_Date__c,USDA_Registration_Expiration_Date__c,Phone__c,Fax__c,Contact_Organization__c,Mailing_City__c,Mailing_Country_LR__c,Mailing_Country_LR__r.Name,Mailing_State_LR__c,Mailing_State_LR__r.Name,Mailing_Street__c,Mailing_Zip_Postal_Code__c from Applicant_Contact__c where Recordtypeid=:apcontRecordTypeId ';
        soql = soql +  ' and (name LIKE \'%' + searchString +'%\'' + 'or First_Name__c LIKE \'%' + searchString +'%\')' ;
            //soql = soql + ' limit 25';
        if(searchString != '' && searchString != null){
            contactsearch = database.query(soql); 
        } else if(newContacts.size() > 0){
            contactsearch = newContacts;
        }
        return contactsearch;
  }  
  
  // This method is used to create new associated contact record
  public PageReference saveAppcont() {
    try {

    String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
    objImpContactNew.Recordtypeid = apcontRecordTypeId ;    
    insert objImpContactNew;
    
    //return list of new contacts to search tab
    newContacts.add(objImpContactNew);
    
    objImpContactNew = new Applicant_Contact__c();
    showMessage = true;
    
    } catch(DmlException e) {
    System.debug('The following exception has occurred: ' + e.getMessage());
    }
    return null;
  }  
  
// used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }  
      
}