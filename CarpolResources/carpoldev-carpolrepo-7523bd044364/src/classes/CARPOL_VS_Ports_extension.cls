public class CARPOL_VS_Ports_extension {

    public List<Facility__c> lstselectedPorts = new List<Facility__c>();
    public Map<String, String> mapSelectedPorts = new Map<String, String>();
    public SelectOption[] allOptions {get; set;}
     public SelectOption[] allportsete{get; set;}
    public SelectOption[] selectedOptions {get; set;}
    public String rightLabel {get; set;}
    public String leftLabel {get; set;}
    public Facility__c domesticPort;
    public List<AC__c> aclst ;
    public List<Program_Line_Item_Pathway__c> plip; //niha 7/29
    public Date arrival;
    public Authorizations__c auth {get; set;}
    public String AuthorizationID = ApexPages.currentPage().getParameters().get('id');
    public String leftVetLabel{get;set;}
    public String rightVetLabel{get;set;}
    public String Val{get;set;}
    public String portsofarrival{get;set;}
   public String Portsofdelivery{get;set;}
    public SelectOption[] allVetOptions {get; set;}
    public SelectOption[] selectedVetOptions {get; set;}
    // The extension constructor initializes the private member
    // variable vsApp by using the getRecord method from the standard
    // controller.
    public CARPOL_VS_Ports_extension(ApexPages.StandardController stdController) {
        this.auth = (Authorizations__c)stdController.getRecord();
        
        if(auth.ID != NULL){
            auth = [SELECT Expiration_Timeframe__c, Effective_Date__c, Expiration_Date__c,Program_Pathway__c FROM Authorizations__c WHERE ID =: auth.ID LIMIT 1];
        }
        
        /*aclst = [select Proposed_date_of_arrival__c from AC__c where Authorization__c = :auth.ID AND RecordType.Name != 'PPQ-Seeds Not For Planting']; //Added RT filter condition-Niharika
        aclst.sort();
        System.debug('<<<<<<<<<<<<<<<<<<<<< List of Sorted Dates of Arrival' + aclst + ' >>>>>>>>>>>>>>>>>');
        if(aclst.size() > 0  && aclst[aclst.size()-1].Proposed_date_of_arrival__c!=null){
            arrival = aclst[aclst.size()-1].Proposed_date_of_arrival__c;
            auth.Expiration_Date__c = arrival.AddDays(30);
        }
        else
        {
            auth.Expiration_Date__c = System.Today().AddDays(30);
        }*/
        
        //Modified by Tharun-08/11/2016
        if((auth.Expiration_Timeframe__c!=null || auth.Expiration_Timeframe__c!='') && auth.Expiration_Date__c==null){
            Integer timeframe = integer.valueof(auth.Expiration_Timeframe__c);
            auth.Expiration_Date__c = System.Today().AddDays(timeframe);
        }
        
        auth.Effective_Date__c = System.Today();
        rightLabel = 'Selected Ports';
        leftLabel = 'All Ports';
        System.debug('<<<<<<<<<<<<<<<<<<<<< Test Point >>>>>>>>>>>>>>>>>>>>>>>');

        getPorts();
        getVets();
    }

    public PageReference getPorts(){
       try
       {
            allOptions = new List<SelectOption>();
            allportsete = new List<SelectOption>();
            AC__c line_item = [select id,name,Port_of_Embarkation__c from AC__c where  Authorization__c =:auth.Id limit 1];
            
            
            List<Facility__c> allports = [select id, Name from Facility__c where ID in (select Transit_Facility__c from Transit_Locale__c where Line_Item__c =: line_item.ID )];
            
            Facility__c portofentry = [select id, Name from Facility__c where ID in ( select Port_of_Entry__c from  AC__c where id =: line_item.ID )];
              portsofarrival = string.valueOf(portofentry.name);
           if(line_item.Port_of_Embarkation__c !=null) {
             Facility__c PortofEmbarkation = [select id, Name from Facility__c where ID in ( select Port_of_Embarkation__c from  AC__c where id =: line_item.ID )];
            Portsofdelivery = string.valueOf(PortofEmbarkation.name);
             allportsete.add(new SelectOption(PortofEmbarkation.id, PortofEmbarkation.Name));
          }
            for(Facility__c allp : allports){
                allOptions.add(new SelectOption(allp.id, allp.Name));
                allportsete.add(new SelectOption(allp.id, allp.Name));
            }
            
            
            
         
            allportsete.add(new SelectOption(portofentry.id, portofentry.Name));
           system.debug('!!!!!!Ports list in allportsete:' +allportsete);
         }
       
         catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        
        }
           
            return null;
        
    }
    
    public PageReference getVets(){
        
        try 
        {
        System.debug(' in Get Vets : '+selectedOptions);
        List<String> portsList = new List<String>();
        for (SelectOption selPorts : allportsete ) {
            portsList.add(selPorts.getValue());
        }
        List<Facility_Vet_Services_Junction__c> facVetJn = [Select Id, Name, Facility__c,facility__r.name, Vet_Services__c, Vet_Services__r.Name, Vet_Services__r.ShippingCity, Vet_Services__r.ShippingState from Facility_Vet_Services_Junction__c where Facility__c in: portsList and Authorization__c = :auth.ID];
        list<Facility__c> facvet =[Select Id, Name,account__C,Account__r.Name,  Account__r.ShippingCity, Account__r.ShippingState from Facility__c where id  in: portsList and account__C != null ];
        allVetOptions = new List<SelectOption>();
        //Facility_Vet_Services_Junction__c facVetJn ;
        
       // Map<Id, id> m = new Map<Id, id>();
          selectedVetOptions = new List<SelectOption>();        
        
        for(Facility__c facilities : facVet){
            
            Facility_Vet_Services_Junction__c[] fvj =[ Select Id,Facility__c,Vet_Services__c from Facility_Vet_Services_Junction__c where Facility__c =: facilities.id and Vet_Services__c =: facilities.account__C and Authorization__c = :auth.ID];
            if(fvj.size() == 0){
            
            allVetOptions.add(new SelectOption(facilities.id,(facilities.Account__r.Name+','+facilities.Account__r.ShippingCity)));
            }
            else
            {
            
            selectedVetOptions.add(new SelectOption(facilities.id,(facilities.Account__r.Name+','+facilities.Account__r.ShippingCity)));
            }
            
           
           // m.put(facilities.id,facilities.account__C );
        }
      
        /*for(Facility_Vet_Services_Junction__c facVetJn1 : facVetJn)
        {
             if( (facVetJn.Vet_Services__c == m.get(facVetJn.id)) && (facVetJn.Facility__c == m) )
             {
                 
             }
            
        }
        */
       // selectedVetOptions = new List<SelectOption>();
        
       //  Facility_Vet_Services_Junction__c facVetJn1 = [Select Id, Name,Authorization__c,Facility__c,Vet_Services__c
        // from Facility_Vet_Services_Junction__c where Authorization__c = auth.ID ];
       // selectedvetoptions 
        
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        
        }
        
        return null;
    }
    public void savevets()
    {
          

         try {
     
       List<Facility_Vet_Services_Junction__c> facVetJn2 = [Select Id from Facility_Vet_Services_Junction__c  where Authorization__c = :auth.ID];
       
       delete facVetJn2; 
     
        for(SelectOption s : selectedVetOptions) {
          system.debug('--getvalue ' +s);
         id st = string.valueof(s.getvalue());
         system.debug('--getvalue ' +st);
         
         
         
         //Facility_Vet_Services_Junction__c facVetJn1 = [Select Id, Name,Authorization__c,Facility__c,Vet_Services__c
        // from Facility_Vet_Services_Junction__c where id =:st];
         //if( facVetJn1.Authorization__c== null)
         //{
           //  facVetJn1.Authorization__c = auth.id;
            // update facVetJn1;
        // }
        
        
         //system.debug('--facVetJn1.name ' +facVetJn1.name);
        }
         if(selectedVetOptions.size() == 0)
            {
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least 1 vet services'));
            }
            else 
            {
                list<Facility_Vet_Services_Junction__c> listfac = new list<Facility_Vet_Services_Junction__c>();
                
                 for(SelectOption s : selectedVetOptions) {
                      id st = string.valueof(s.getvalue());
                         system.debug('--getvalue ' +st);
                
                      Facility_Vet_Services_Junction__c fvsjc = new Facility_Vet_Services_Junction__c();
                      
                         fvsjc.Facility__c = st;
                         Facility__c fc =[ select account__C from Facility__c where id = : st]; 
                         
                          fvsjc.Vet_Services__c  =  fc.account__C;
                          fvsjc.Authorization__c =  auth.ID;
                          listfac.add(fvsjc);
                          
                         
                 }
                    Database.SaveResult[] savelst = Database.insert(listfac);
                    upsert auth;
                    
                    
                     
               // Facility_Vet_Services_Junction__c
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
            }
        //selectedVetOptions;
    }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        
        }
    }    
   
}