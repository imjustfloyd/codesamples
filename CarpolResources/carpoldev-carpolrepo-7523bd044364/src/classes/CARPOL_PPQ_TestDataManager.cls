@isTest
public class CARPOL_PPQ_TestDataManager {
/*  ====================================================  */
/*  Name: CARPOL_BRS_TestDataManager                       */  
/*  Related Components:                                   */
/*  ====================================================  */ 
/*  ====================================================  */
/*  Purpose: BRS Specific Activities                      */  
/*  ====================================================  */ 
/*  ====================================================  */
//Object record type information

      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACgroupRecordTypeId = Schema.SObjectType.Group__c.getRecordTypeInfosByName().get('Regulated Article').getRecordTypeId();      
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String SRJRecordTypeId = Schema.SObjectType.Signature_Regulation_Junction__c.getRecordTypeInfosByName().get('Singature_Regulation').getRecordTypeId();
      String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
      String ACWFTRecordTypeId = Schema.SObjectType.Workflow_Task__c.getRecordTypeInfosByName().get('Process Authorization').getRecordTypeId();      
      String ACARRRecordTypeId = Schema.SObjectType.Auth_Related_Records__c.getRecordTypeInfosByName().get('NEPA Notifications').getRecordTypeId();            
      
      
//=====Utility functions=====//
public String randomString() {
  //return random string of 32 chars
  return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
}

public String randomString5() {
    return randomString().substring(0, 5);
}

public void insertcustomsettings(){
    List<CARPOL_UNI_DisableTrigger__c> dtlist = new List<CARPOL_UNI_DisableTrigger__c>();
    Set<String> dtstrings = new Set<String>{'CARPOL_BRS_AuthorizationTrigger','CARPOL_BRS_ConstructTrigger','CARPOL_BRS_GenotypeTrigger','CARPOL_BRS_Link_RegulatedTrigger','CARPOL_BRS_LocationsTrigger','CARPOL_BRS_LocationsTrigger Active','CARPOL_BRS_Reviewer_DuplicateTrigger','CARPOL_BRS_Self_ReportingTrigger','CARPOL_BRS_SOPTrigger','CARPOL_BRS_State_ReviewerTrigger'};
    for(String x : dtstrings){
       CARPOL_UNI_DisableTrigger__c dt = CARPOL_UNI_DisableTrigger__c.getInstance(x);
       if(dt == null){
           dt = new CARPOL_UNI_DisableTrigger__c(Name=x,Disable__c = false);  
           dtlist.add(dt);
       }
    }
    insert dtlist;
} 
 
//=====Supporting/Configuration records=====//
     
//newAccount - Must have an account to create a contact
 public account newAccount(string AccRecordTypeId ){
    Account objacct = new Account();
    objacct.Name = 'Global Account'+randomString5();
    objacct.RecordTypeId = AccRecordTypeId;    
    insert objacct; 
    System.assert(objacct != null);    
    return objacct;
 }
 
 public Attachment newAttachment(id ACId){ 
    Attachment attach = new Attachment();
    attach.Body = blob.valueof('Test doc');
    attach.Name = 'Test doc'+randomString5();
    attach.ParentId = ACId;
    insert attach;  
    System.assert(attach != null);        
    return attach;  
 } 
 
 public breed__c newbreed(){
      Breed__c brd = new Breed__c();
      brd.Name='New Breed'+randomString5();
      insert brd; 
      System.assert(brd != null);              
      return brd;  
 }
 
 public Applicant_Contact__c newappcontact() {
     //creating this calls the validateAddress trigger
     Level_1_Region__c appL1R = newlevel1regionAL();
     
     Applicant_Contact__c apcont = new Applicant_Contact__c();
     apcont.First_Name__c = 'apcont'+randomString5();
     apcont.Email_Address__c = 'apcont@test.com';
     apcont.Name = 'Associated Contacts'+randomString5();
     //if the field below isn't assigned the AddressVerify class breaks //- DMS 4/4/2016
     apcont.Mailing_Country__c = 'United States';
     apcont.Mailing_Country_LR__c = appL1R.Country__c;
     apcont.Mailing_State_LR__c = appL1R.Id;
     apcont.Mailing_State__c = 'Alabama';
     apcont.RecordTypeId = apcontRecordTypeId;
     Test.setMock(HttpCalloutMock.class, new AddressVerifyMockImpl());
     insert apcont;
     System.assert(apcont != null);             
     return apcont;
 } 
 
 public Communication_Manager__c newCommunicationmanager(string type){
     Communication_Manager__c communmang = new Communication_Manager__c();        
     communmang.Content__c = type;
     communmang.Description__c = 'test description'+randomString5();
     communmang.Type__c = type;
     insert communmang;
     System.assert(communmang != null);                     
     return communmang;  
 } 
 
//new Contact - Must have a contact to create an application
 public Contact newContact(){
    Contact objCont = new Contact();
    objCont.FirstName = 'Global Contact'+randomString5();
    objcont.LastName = 'LastName'+randomString5();
    objcont.Email = randomString5()+'test@email.com';        
    objcont.AccountId = newAccount(AccountRecordTypeId).id;
    objcont.MailingStreet = 'Mailing Street'+randomString5();
    objcont.MailingCity = 'Mailing City'+randomString5();
    objcont.MailingState = 'Ohio';
    objcont.MailingCountry = 'United States';
    objcont.MailingPostalCode = '32092';    
    insert objcont; 
    System.assert(objcont != null);            
    return objcont;
 }
  
 public Country__c newcountryaf(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'Afghanistan';
    objcountry.Country_Code__c = 'AF';
    objcountry.Country_Status__c = 'Active';
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    System.assert(objcountry != null);            
    return objcountry;
 }
 
 public Country__c newcountrywithassoc(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'Pakistan';
    objcountry.Country_Code__c = 'PK';
    objcountry.Country_Status__c = 'Active';
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    //create the country junction    
    newcountryjunction(objcountry, newgroup());
    System.assert(objcountry != null);            
    return objcountry;
 } 
 
 public Country__c newcountryus(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'United States of America';
    objcountry.Country_Code__c = 'US';
    objcountry.Country_Status__c = 'Active';    
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    System.assert(objcountry != null);            
    return objcountry;
 }
 
  public Country_Junction__c newcountryjunction(Country__c cntry, Group__c grp){
    Country_Junction__c objcountryjn = new Country_Junction__c();
    objcountryjn.Country__c = cntry.Id;
    objcountryjn.Group__c = grp.Id;
    insert objcountryjn;
    System.assert(objcountryjn != null);            
    return objcountryjn;
 }
 
 //New facility - calling this method fires the CARPOL_AC_MasterFacilityTrigger
 //Note GooglePopulateTimeZone will not be called in this method because mock implementation in test not set
 public Facility__c newfacility(String type){
    Facility__c fac = new Facility__c();
    fac.RecordTypeId = PortsFacRecordTypeId;
    fac.Name = 'entryport';
    fac.Type__c = type;  
    insert fac;    
    System.assert(fac != null);                
    return fac;  
 }
 
  public Group__c newgroup(){
    Group__c grp = new Group__c();
    grp.Name = 'Test';
    grp.RecordTypeId = ACgroupRecordTypeId;
    grp.Group_Custom_Name__c = 'Test';
    grp.Status__c = 'Active';
    insert grp;  
    System.assert(grp != null);                  
    return grp;  
 }
 
 public Level_1_Region__c newlevel1regionAL(){
     Level_1_Region__c objL1R = new Level_1_Region__c();
     objL1r.Country__c = newcountryus().Id;
     objL1r.Name = 'Alabama';
     objL1r.Level_1_Name__c = 'Alabama';
     objL1r.Level_1_Region_Code__c = 'AL';
     objL1r.Level_1_Region_Status__c = 'Active';
     insert objL1r;
     System.assert(objL1r != null);             
     return objL1R;
 }
 
 public Level_1_Region__c newlevel1regionMD(){
     Level_1_Region__c objL1R = new Level_1_Region__c();
     objL1r.Country__c = newcountryus().Id;
     objL1r.Name = 'Maryland';
     objL1r.Level_1_Name__c = 'Maryland';
     objL1r.Level_1_Region_Code__c = 'MD';
     objL1r.Level_1_Region_Status__c = 'Active';
     insert objL1r;
     System.assert(objL1r != null);             
     return objL1R;
 } 
 
 public Program_Prefix__c newPrefix(){
     Program_Prefix__c objPrefix = new Program_Prefix__c();
     objPrefix.Program__c = newProgram('AC').Id;
     objPrefix.Name = '555';
  
     insert objPrefix; 
     System.assert(objPrefix != null);                    
     return objPrefix;  
 }
 
 public Domain__c newProgram(String prog){
     Domain__c objProg = new Domain__c();
     objProg.Name = prog;
     objProg.Active__c = true;
     insert objProg;
     System.assert(objProg != null);             
     return objProg;
 }
  
 public Program_Line_Item_Pathway__c newCaninePathway(){
    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = newProgram('VS').Id;
    objParentPathway.Name = 'Live Animals';   
    insert objParentPathway;
    System.assert(objParentPathway != null);            

    Program_Line_Item_Pathway__c objPathway= new Program_Line_Item_Pathway__c();
    objPathway.Program__c = newProgram('Canines').Id;
    objPathway.Name = 'Canines';       
    objPathway.Parent_Id__c = objParentPathway.Id;
    objPathway.Is_Country_Required__c = true;
    objPathway.Is_State_of_Destination_Required__c = true;
    objPathway.Show_Intended_Use__c = true;
    objPathway.Validate_for_Disease__c = 'Rabies';
    objPathway.Show_Required_Documents_Button__c = true;
    objPathway.Show_Photos_Button__c = true;
    //Live Dogs record type may be needed
    //objPathway.Required_Documents__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement';
    insert objPathway;

    return objPathway;
 }
 
    public Regulation__c newRegulation(string type,string subtype){
      Regulation__c objreg = new Regulation__c();
      objreg.Status__c = 'Active';
      objreg.Type__c =  type; //'Import Requirements';
      objreg.Custom_Name__c = 'testcustname'+randomString5();
      objreg.Short_Name__c = 't'+randomString5();
      objreg.Title__c = 'title'+randomString5();
      objreg.Regulation_Description__c = 'descp1'+randomString5();
      objreg.Sub_Type__c = subtype;//'Import Permit Requirements';
      objreg.RecordTypeID = ACRegRecordTypeId;
      insert objreg; 
      System.assert(objreg != null);              
      return objreg;  
    } 
 
    public Signature__c newThumbprint(){
      Signature__c objTP = new Signature__c();
      objTP.Name = 'Test AC TP Jialin';
      objTP.Recordtypeid = ACTPRecordTypeId;
      objTP.Program_Prefix__c = newPrefix().Id;
  
      insert objTP;  
      System.assert(objTP != null);                    
      return objTP;  
    }
    
    public Signature_Regulation_Junction__c newSRJ(id tpId,id RegId){ 
      Signature_Regulation_Junction__c objSRJ = new Signature_Regulation_Junction__c();
      objSRJ.Recordtypeid = SRJRecordTypeId;
      objSRJ.Signature__c = tpId;
      objSRJ.Regulation__c = RegId;//objreg.id;
      insert objSRJ; 
      System.assert(objSRJ != null);              
      return objSRJ;   
    } 
    
    public Regulated_Article__c newRegulatedArticle(ID plip){
        Regulated_Article__c regArt = new Regulated_Article__c();
        regArt.Name = 'Test article';
        regArt.RecordTypeID = Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regArt.Status__c = 'Active';
        regArt.Category_Group_Ref__c = newgroup().Id;
        regArt.Program_Pathway__c = plip;
               
        insert regArt;
        //create a junction too
        RA_Scientific_Name_Junction__c raJunc = new RA_Scientific_Name_Junction__c();
        raJunc.Program_Pathway__c = plip;
        raJunc.Regulated_Article__c = regArt.id;
        insert raJunc;
        System.assert(regArt != null);                
        return regArt;
    }    
 
 public Trade_Agreement__c newta(){ 
    Trade_Agreement__c ta = new Trade_Agreement__c();
    ta.Name = 'test ta';
    ta.Trade_Agreement_Code__c = 'ta';
    ta.Trade_Agreement_Status__c ='Active';
    insert ta;
    System.assert(ta != null);            
    return ta;
 }
 
 public VS__c newVS(Id ImportedFullId, Id ApplicationId) {
        VS__c VS = new VS__C();
        VS.Application_Number__c = ApplicationId;
        VS.Importer_Full_Name__c = ImportedFullId;
        VS.Hand_Carry__c = 'No';
        VS.Quantity__c = '5';
        VS.Purpose_of_Importation__c = 'Commercial/Retail sale directly to the consumer';
        VS.Intended_Use__c = 'Humar Consumption';
        VS.Product_Category__c = 'birds nest';
        VS.Method_of_Final_Disposition__c = 'Autoclaving';
        insert VS;
        System.assert(VS != null);                
        return VS;
    }
 
 //======Primary records=====//

//Application - this insert triggers the CARPOL_UNI_MasterApplicationTrigger

 public Application__c newapplication(){
    Application__c objapp = new Application__c();
    objapp.Applicant_Name__c = newContact().Id;
    objapp.Recordtypeid = ACAppRecordTypeId;
    objapp.Application_Status__c = 'Open';

    insert objapp; 
    System.assert(objapp != null);             
    return objapp;        
 }
 
 //Authorization - - this insert triggers the CARPOL_UNI_MasterAuthorizationTrigger
 public Authorizations__c newAuth(id appid){
    Authorizations__c objauth = new Authorizations__c();
    if(appid == null){
        objauth.Application__c = newapplication().Id;
    } else {
        objauth.Application__c = appid;
    }    
//    objauth.Application__c = appid;
    objauth.RecordTypeID = ACauthRecordTypeId;
    objauth.Status__c = 'Submitted';
    objauth.Date_Issued__c = date.today();
    objauth.Applicant_Alternate_Email__c = 'test@test.com';
    objauth.UNI_Zip__c = '32092';
    objauth.UNI_Country__c = 'United States';
    objauth.UNI_County_Province__c = 'Duval';
    objauth.BRS_Proposed_Start_Date__c = date.today()+30;
    objauth.BRS_Proposed_End_Date__c = date.today()+40;
    objauth.CBI__c = 'CBI Text';
    objauth.Application_CBI__c = 'No';
    objauth.Applicant_Alternate_Email__c = 'email2@test.com';
    objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
    objauth.AC_Applicant_Email__c = 'email2@test.com';
    objauth.AC_Applicant_Fax__c = '(904) 123-2345';
    objauth.AC_Applicant_Phone__c = '(904) 123-2345';
    objauth.AC_Organization__c = 'ABC Corp';
    objauth.Means_of_Movement__c = 'Hand Carry';
    objauth.Biological_Material_present_in_Article__c = 'No';
    objauth.If_Yes_Please_Describe__c = 'Text';
    objauth.Applicant_Instructions__c = 'Make corrections';
    objauth.BRS_Number_of_Labels__c = 10;
    objauth.BRS_Purpose_of_Permit__c = 'Importation';
    objauth.Documents_Sent_to_Applicant__c = false;
    insert objauth;
    System.assert(objauth != null);            
    return objauth;  
 } 
 
 public Authorization_Junction__c newAuthorizationJunction(id authId,id RegId){ 
   Authorization_Junction__c objauthjun = new Authorization_Junction__c();
   objauthjun.Authorization__c = authId;
   objauthjun.Regulation__c = RegId;//objreg.id;
   insert objauthjun; 
   System.assert(objauthjun != null);           
   return objauthjun;   
 } 
    
 public Auth_Related_Records__c newauthrelatedrecord(Id wft, Id auth){
     Auth_Related_Records__c arr = new Auth_Related_Records__c();
     arr.RecordTypeId = ACARRRecordTypeId;  
     arr.Authorization__c = auth;    
     arr.X1_Does_this_Document_contain_CBI__c = 'Yes';
     arr.X2_Is_the_recipient_a_plant__c = 'No';
     arr.X3_Is_duration_greater_than_one_year__c = 'No';
     arr.X4_Is_plant_a_noxious_weed_species__c = 'No';
     arr.X5_Derived_from_an_animal_or_human_virus__c = 'No';
     arr.X5a_REQUIRED_COMMENTS_Q5__c = 'No';
     arr.Is_introduced_genetic_material_known__c = 'No';
     arr.X7_Is_likely_to_cause_disease__c = 'No';
     arr.X7a_REQUIRED_COMMENTS_Q7__c = 'Test';
     arr.X8_Does_Gen_mat_result_in_plant_disease__c = 'No';
     arr.X9_Can_result_in_any_infectious_entity__c = 'No';
     arr.X10_Can_be_toxic_to_non_target_organisms__c = 'No';
     arr.X10a_REQUIRED_COMMENTS_Q10__c = 'Test';
     arr.X11_Intended_for_phar_or_industrial_use__c = 'No';
     arr.X12_Non_coding_regulatory_sequences__c = 'No';
     arr.X12a_REQUIRED_COMMENTS_Q12__c = 'Test';
     arr.X13_Sequences_from_coding_regions__c = 'No';
     arr.X13a_REQUIRED_COMMENTS_Q13__c = 'Test';
     arr.X14_Cell_to_cell_movement_of_the_virus__c = 'No';
     arr.X14a_REQUIRED_COMMENTS_Q14__c = 'Test';
     arr.X15_Foreign_DNA_in_the_plant_genome__c = 'No';
     arr.X15a_Comment_if_a_novel_method__c = 'No';
     arr.X16_Article_qualify_s_for_Notification__c = 'No';
     arr.X16a_REQUIRED_COMMENTS_Q16__c = 'Test';
     arr.M1_Potential_for_enviornmental_Impact__c = 'No';
     arr.M2_Unsafe_licensed_approved_biologic__c = 'No';
     arr.M3_Contains_live_microorganisms__c = 'No';
     arr.M4_New_species_or_organisms_new_issues__c = 'No';
     arr.M5_Effect_quality_of_human_environment__c = 'No';
     arr.M6_Between_contained_facilities__c = 'No';
     arr.M7_Measures_used_to_avoid_impact__c = 'No';
     arr.M8_ESA_Assessment_Movement__c = 'No';
     arr.M8a_REQUIRED_COMMENTS_QM8__c = 'Test comment';
     arr.R1_Impact_of_proposed_release__c = 'No';
     arr.R1a_REQUIRED_COMMENTS_QR1__c = 'Test comment';
     arr.R2_Involve_approved_vet_biologic__c = 'No';
     arr.R2a_REQUIRED_COMMENTS_QR2__c = 'Test comment';
     arr.R3_Is_unlicensed_vet_biological_product__c = 'No';
     arr.R3a_REQUIRED_COMMENTS_QR3__c = 'Test comment';
     arr.R4_Release_a_genetically_eng_organism__c = 'No';
     arr.R4a_REQUIRED_COMMENTS_QR4__c = 'Test comment';
     arr.R5_Involve_new_species_organisms__c = 'No';
     arr.R5a_REQUIRED_COMMENTS_QR5__c = 'Test comment';
     arr.R6_Modifications_that_raise_new_issues__c = 'No';
     arr.R6a_REQUIRED_COMMENTS_QR6__c = 'Test comment';
     arr.R7_Significant_impact_to_Human_Environ__c = 'No';
     arr.R7a_REQUIRED_COMMENTS_QR7__c = 'Test comment';
     arr.R8_Within_Reservation_Lands__c = 'No';
     arr.R9_Is_plant_sexually_compatible__c = 'No';
     arr.R10_Release_in_critical_habitat__c = 'No';
     arr.R10a_REQUIRED_COMMENTS_QR10__c = 'Test comment';
     arr.R11_Can_cause_disease_in_humans__c = 'No';
     arr.R11a_REQUIRED_COMMENTS_QR11__c = 'Test comment';
     arr.R12_Is_toxic_to_non_target_organisms__c = 'No';
     arr.R12a_REQUIRED_COMMENTS_QR12__c = 'Test comment';
     arr.R13_Effects_to_TES_or_critical_habitat__c = 'No';
     arr.R14_Has_NO_EFFECT_on_Critical_Habitat__c = 'No';
     arr.R14a_REQUIRED_COMMENTS_QR14__c = 'Test comment';
     arr.X17_Categorical_exclusions_under_NEPA__c = 'No';
     arr.X17a_REQUIRED_COMMENTS_Q17__c = 'Test comment';
     arr.X18_Do_exceptions_to_exclusions_apply__c = 'No';
     arr.X18a_REQUIRED_COMMENTS_Q18__c = 'Test comment';
     arr.M4a_REQUIRED_COMMENTS_QM4__c = 'Test comment';
     arr.M5a_REQUIRED_COMMENTS_QM5__c = 'Test comment';
     arr.M6a_REQUIRED_COMMENTS_QM6__c = 'Test comment';
     arr.M7a_REQUIRED_COMMENTS_QM7__c = 'Test comment';
     arr.R8a_REQUIRED_COMMENTS_QR8__c = 'Test comment';
     arr.R9a_REQUIRED_COMMENTS_QR9__c = 'Test comment';
     arr.R13a_REQUIRED_COMMENTS_QR13__c = 'Test comment';
     arr.M1a_REQUIRED_COMMENTS_QM1__c = 'Test comment';
     arr.M2a_REQUIRED_COMMENTS_QM2__c = 'Test comment';
     arr.M3a_REQUIRED_COMMENTS_QM3__c = 'Test comment';
     arr.X11a_REQUIRED_COMMENTS_Q11__c = 'Test comment';
     arr.X1a_REQUIRED_COMMENTS_Q1__c = 'Test comment';
     arr.X2a_REQUIRED_COMMENTS_Q2__c = 'Test comment';
     arr.X3a_REQUIRED_COMMENTS_Q3__c = 'Test comment';
     arr.X4a_REQUIRED_COMMENTS_Q4__c = 'Test comment';
     arr.X6a_REQUIRED_COMMENTS_Q6__c = 'Test comment';
     arr.X8a_REQUIRED_COMMENTS_Q8__c = 'Test comment';
     arr.X9a_REQUIRED_COMMENTS_Q9__c = 'Test comment';
     insert arr;
     System.assert(arr != null);             
     return arr;
 }
  
//Line Item(s) - Insertion triggers the CARPOL_UNI_MasterLineItemTrigger
public AC__c newLineItem(string poi, Application__c application){
    //default record type 'Live Dogs'
    Facility__c  entry = newfacility('Domestic Port');
    Facility__c  embarkation = newfacility('Foreign Port');
      
    AC__c ac = new AC__c();
    Applicant_Contact__c appcont = newappcontact();
    if(application == null){
        ac.Application_Number__c = newapplication().Id;
    } else {
        ac.Application_Number__c = application.Id;
    }
    ac.Departure_Time__c = date.today()+11;
    ac.Arrival_Time__c = date.today()+15;
    ac.Proposed_date_of_arrival__c = date.today()+15;
    ac.Port_of_Entry__c = entry.Id;
    ac.Port_of_Embarkation__c = embarkation.id;
    ac.Transporter_Type__c = 'Ground';
    
    ac.Date_of_Birth__c = date.today()-400;
    ac.Breed__c = newbreed().id;
    ac.Color__c = 'Brown';
    ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
    ac.Sex__c = 'Male';
    ac.Country_Of_Origin__c = newcountryus().Id;
    ac.Program_Line_Item_Pathway__c = newCaninePathway().Id;    
    ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
    ac.Importer_Last_Name__c = appcont.id;
    ac.Exporter_Last_Name__c = appcont.id;
    ac.Status__c = 'Saved';
    ac.Purpose_of_the_Importation__c = poi; //'Resale/Adoption'
    ac.DeliveryRecipient_Last_Name__c = appcont.id;
    insert ac;
    System.assert(ac != null);            
    return ac;  
}

//History records should be generated by GenericHistoryClass

//Applicant Attachmentsw


public Workflow_Task__c newworkflowtask(string wfname, Authorizations__c auth, string status){
     Workflow_Task__c wtask = new Workflow_Task__c();
     wtask.RecordTypeId = ACWFTRecordTypeId;
     wtask.Name = wfname; //Process Authorization;
     if(auth == null){
         wtask.Authorization__c = newAuth(null).Id;
     } else {
         wtask.Authorization__c = auth.Id;
     }
     wtask.Status__c = status; //'Complete'
     wtask.Buttons__c = '';
     insert wtask;
     System.assert(wtask != null);             
     return wtask;
}

//Agreements

//Inspections

//Documents


}