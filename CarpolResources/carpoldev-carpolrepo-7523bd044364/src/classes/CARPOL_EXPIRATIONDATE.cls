public class CARPOL_EXPIRATIONDATE {

      Map<Id, AC__c> newLine;
      Set<ID> setLineIds = new Set<ID>();
      
      public  CARPOL_EXPIRATIONDATE(Map<Id, AC__c> newTriggerLines) 
        {
              newLine = newTriggerLines;
        }
    
     public void EXPIRATIONDATE_INSERT()
        {        
               
                AC__c newLineItem = new AC__c();
                newLineItem = newLine.values()[0];
                system.debug(' <!!!!!!!!!newLineItem'+newLineItem);
                 try
                    {
                           if (newLineItem.Status__c == 'Saved' || newLineItem.Status__c == 'Ready to Submit' )
                            {
                                 Program_Line_Item_Pathway__c PLIP = [SELECT ID, Expiration_Timeframe__c 
                                                                      FROM Program_Line_Item_Pathway__c 
                                                                      WHERE id =: newLineItem.Program_Line_Item_Pathway__c LIMIT 1];
                                      system.debug('<!!!! PLIP'+PLIP);
                                 newLineItem.Permit_Expiration_Date__C =  newLineItem.Proposed_date_of_arrival__c.addDays(integer.valueof(PLIP.Expiration_Timeframe__c));
                            }
                            system.debug('<!!!!! Permit_Expiration_Date__C'+newLineItem.Permit_Expiration_Date__C);
                    }
                 catch(Exception e)
                    {
                        system.debug('Exception in CARPOL_EXPIRATIONDATE is '+e);
                    }
         }
}