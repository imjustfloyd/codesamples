/*
// Author : Vinar Amrutia
// Created Date : 3/2/2015
// Purpose : This controller is used to process applications pertaining to Animal Care (AC)
*/
public with sharing class CARPOL_AC_Processing {

    
    public Application__c application;
    public AC__c acLineItem;
    public Authorizations__c authorization;
    public Boolean loadPage { get; set; }
    public Boolean success;
    public String processID;
    public string wqTPId;
    private Map<String, String> paramMap;
    public String parm1{get;set;}
     public String strPathway{get;set;}
    public String Scientific_Name{get;set;}
    public String Country_Of_Origin{get;set;}
    public String Intended_Use{get;set;}
    public String State_Territory_of_Destination{get;set;}
    public String RA_Scientific_Name{get;set;}
     
    //Constructor
    public CARPOL_AC_Processing()
    {
        loadPage = true;
        success = false;
        processID = apexpages.currentpage().getparameters().get('processID');
        wqTPId = apexpages.currentpage().getparameters().get('TPId');
        //Added by Niharika 3/8
        parm1 = apexpages.currentpage().getparameters().get('parm1');
         strPathway = apexpages.currentpage().getparameters().get('strPathway');
        Scientific_Name  = apexpages.currentpage().getparameters().get('Scientific_Name__c');
        Country_Of_Origin  = apexpages.currentpage().getparameters().get('Country_Of_Origin__c');
       Intended_Use  = apexpages.currentpage().getparameters().get('Intended_Use__c');
        State_Territory_of_Destination = apexpages.currentpage().getparameters().get('State_Territory_of_Destination__c');
        RA_Scientific_Name=apexpages.currentpage().getparameters().get('RA_Scientific_Name__c');
        parm1='&Scientific_Name__c='+Scientific_Name+'&Country_Of_Origin__c='+Country_Of_Origin +'&Intended_Use__c='+Intended_Use+'&State_Territory_of_Destination__c='+State_Territory_of_Destination+'&RA_Scientific_Name__c='+RA_Scientific_Name;
        System.Debug('<<<<<<< parm1  : ' + parm1 + ' >>>>>>');
        System.Debug('<<<<<<< Constructor Process ID : ' + processID + ' >>>>>>');
        
    }
    
    public PageReference pageLoadFunc()
    {
        try
        {
            if(processID == '21' || processID == '22' || processID == '23' || processID == '24')
            {
                Cookie criteria = ApexPages.currentPage().getCookies().get('criteria');
                String finalCriteriaString = '';
                System.Debug('<<<<<<< Cookie : ' + criteria + ' >>>>>>>');     
                
                if(criteria != null || test.isRunningTest())
                {
                    Integer questionNo = 0;
                    String tempString = criteria.getValue();
                    String questionsTemp = tempString.substring(0,tempString.lastIndexOf('][') + 1);
                    String answersTemp = tempString.substring(tempString.lastIndexOf('][') + 1, tempString.length());
                    System.Debug('<<<<<<< tempString : ' + tempString + ' >>>>>>>');  
                    String answers = '';
                    
                    while(questionsTemp.indexOf(']') > 0)
                    {
                        questionNo += 1; 
                        finalCriteriaString += questionNo + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        finalCriteriaString += questionsTemp.substring(1, questionsTemp.IndexOf(']')) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        finalCriteriaString += answersTemp.substring(1, answersTemp.IndexOf(']')) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>';
                        System.Debug('<<<<<<< finalCriteriaString : ' + finalCriteriaString + ' >>>>>>>');
                        if(questionsTemp.IndexOf(']_[') >= 0)
                        {
                            questionsTemp = questionsTemp.substring(questionsTemp.IndexOf(']_[')+2, questionsTemp.length());
                            answersTemp = answersTemp.substring(answersTemp.IndexOf(']_[')+2, answersTemp.length());
                        }else{break;}
                    }
                }
                
                application = new Application__c();
                application.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Application__c' AND Name = 'Standard Application' LIMIT 1].ID;
                Id contactID = [SELECT ID, Name, ContactID FROM User WHERE ID = :UserInfo.getUserID() LIMIT 1].ContactID;
                application.Applicant_Name__c = contactID;
                if(contactID == null)
                {
                    //application.Applicant_Name__c = '003r0000001xtSN';
                    //application.Applicant_Name__c = '00335000001iPKf';
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, +'Please login as Applicant and Access the External Wizard Page.'));                    
                }
                if(processID == '21'){application.Application_Status__c = 'Open';}
                else{application.Application_Status__c = 'Auto Submitted';}
                //application.Locked__c = 'Yes';
                application.Share_with_Roles__c = 'AC';
                application.Comments__c = finalCriteriaString;
                insert(application);
                System.Debug('<<<<<<< Application Inserted : ' + application.ID + ' >>>>>>');
                List<Program_Line_Item_Pathway__c> listPTW;
                if(strPathway!='')
                    listPTW=[select id,Letter_Expiration_Timeframe__c from Program_Line_Item_Pathway__c where id=:strPathway];
                if(application.ID != null && processID != '21')
                {
                    authorization = new Authorizations__c();
                    Signature__c thumbprint; 
                    //figure out if this is PPQ autho or AC - set recordtype accordingly
                    system.debug('<<<<<< ThumbPrint ID'+wqTPId);
                    if(wqTPId!=null)
                    {
                        thumbprint = [select Id, name, REF_Program_Name__c from signature__c where Id =:wqTPId limit 1];
                        if(thumbprint.REF_Program_Name__c == 'PPQ')
                            authorization.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Authorizations__c' AND Name = 'Plant Protection & Quarantine (PPQ)' LIMIT 1].ID;
                        else if(thumbprint.REF_Program_Name__c == 'VS')
                            authorization.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Authorizations__c' AND Name = 'Veterinary Services (VS)' LIMIT 1].ID;                            
                        else
                            authorization.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Authorizations__c' AND Name = 'Animal Care (AC)' LIMIT 1].ID;
                            
                    }
                    else
                        authorization.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Authorizations__c' AND Name = 'Animal Care (AC)' LIMIT 1].ID;
                    
                    authorization.Application__c = application.ID;
                    if(listPTW!=null && listPTW.size()>0 && listPTW[0].Letter_Expiration_Timeframe__c!=null)
                        authorization.Expiration_Date__c=System.Today()+Integer.valueof(listPTW[0].Letter_Expiration_Timeframe__c);
                    if(processID == '22'){authorization.Authorization_Type__c = 'Letter of Denial';}
                    if(processID == '23'){authorization.Authorization_Type__c = 'Letter of No Jurisdiction';}
                    if(processID == '24'){authorization.Authorization_Type__c = 'Letter of No Permit Required';}                    
                    authorization.Status__c = 'Issued';
                    authorization.Program_Pathway__c = strPathway; // Dinesh - W-006365 - 8/16
                  //  authorization.Locked__c = 'Yes';
                    insert(authorization);
                    System.Debug('<<<<<<< Authorization Inserted : ' + authorization.ID + ' >>>>>>');
                    
                /*
                acLineItem = new AC__c();
                acLineItem.RecordTypeID = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'AC__c' AND Name = 'Live Dogs' LIMIT 1].ID;
                acLineItem.Application_Number__c = application.ID;
                acLineItem.Breed__c = [SELECT ID, Name FROM Breed__c WHERE Name = 'Other' LIMIT 1].ID;
                acLineItem.Breed_Description__c = 'System LOD';
                acLineItem.Status__c = 'Saved';
                acLineItem.Sex__c = 'Female';
                acLineItem.Color__c = 'White';
                acLineItem.Port_of_Embarkation__c = 'a07r0000000CeVZ';
                acLineItem.Port_of_Entry__c = 'a07r0000000CeQN';
                acLineItem.Date_of_Birth__c = System.Today().AddMonths(-12);
                acLineItem.Purpose_of_the_Importation__c = 'Veterinary Treatment';
                acLineItem.Transporter_Type__c = 'Ground';
                acLineItem.Rabies_Vaccination_Date__c = System.Today().AddDays(10);
                acLineItem.Proposed_date_of_arrival__c = System.Today().AddDays(45);
                insert(acLineItem);
                String acLnItmID = acLineItem.ID;
                System.Debug('<<<<<<< AC Line Item Inserted : ' + acLineItem.ID + ' >>>>>>');
                System.Debug('<<<<<<< AC Line Item Inserted : ' + acLnItmID + ' >>>>>>');
                */
                loadPage = false;
                success = true;                
                System.Debug('<<<<<<< Redirecting to URL : /apex/CARPOL_AC_Processing2?applicationID=' + application.ID + ' >>>>>>');
                PageReference appProcess2;
                if(parm1==null){
                 appProcess2 = new PageReference('/apex/CARPOL_AC_Processing2?applicationID=' + application.ID + '&processID=' + processID+'&TPId='+wqTPId +'&strPathway='+strPathway  ); //niharika 3/8
                }else{
                 appProcess2 = new PageReference('/apex/CARPOL_AC_Processing2?applicationID=' + application.ID + '&processID=' + processID+'&TPId='+wqTPId +'&strPathway='+strPathway +'&'+parm1 ); //niharika 3/8
                }
                appProcess2.setRedirect(true);
                return appProcess2;
                }                
                else
                {
                    //get the paramaters that were passed from the wizard
                    String paramStr;
                    paramMap = ApexPages.currentPage().getParameters();
                    for(String paramId: paramMap.keySet())
                    {
                        if(paramStr==null)paramStr = '&' + paramId + '=' + paramMap.get(paramId);
                        paramStr = paramStr + '&' + paramId + '=' + paramMap.get(paramId);
                    }
                    PageReference appDetails = new PageReference('/apex/CARPOL_UniversalLineItem?appid=' + application.ID + paramStr);
                    appDetails.setRedirect(true);
                    return appDetails;
                } 
            }
            else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Process ID found.')); return null;}
        }
        catch(Exception e)
        {
            success = false;
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
            return null;
        }
        //return success;
    }
    
    public PageReference attachLetter() {
        
        ID applicationID = apexpages.currentpage().getparameters().get('applicationID');
        System.Debug('<<<<<<< Application ID : ' + applicationID + ' >>>>>>>');
        String processID = apexpages.currentpage().getparameters().get('processID');
        System.Debug('<<<<<<< Process ID : ' + processID + ' >>>>>>>');
        //ID acLineItemID = apexpages.currentpage().getparameters().get('acLineItemID');
        //System.Debug('<<<<<<< AC Line Item ID : ' + acLineItemID + ' >>>>>>>');   
        Cookie criteria = ApexPages.currentPage().getCookies().get('criteria');
        System.Debug('<<<<<<< Cookie : ' + criteria + ' >>>>>>>');
         //Email and attachment variables
        String attachmentName;
        String letterType;
        PageReference pdfACFormalPage;        
        try
        {
            if((applicationID != null && (processID == '21' || processID == '22' || processID == '23' || processID == '24')) || test.isRunningTest())
            {
                if(processID == '21')
                {
                    PageReference appDetails = new PageReference('/' + applicationID);
                    appDetails.setRedirect(true);
                    return appDetails;
                }
                else
                {
                    if(processID == '22')
                    {
                        attachmentName = 'Letter of Denial.pdf';
                        letterType = 'Letter of Denial';
                        pdfACFormalPage = Page.CARPOL_AC_LiveDogs_LOD_Formal;
                        pdfACFormalPage.getParameters().put('TPId', wqTPId); // Added KK on 03/04/2016
                    }
                    if(processID == '23')
                    {
                        attachmentName = 'Letter of No Jurisdication.pdf';
                        letterType = 'Letter of No Jurisdication';
                        pdfACFormalPage = Page.CARPOL_AC_LiveDogs_NJLetter_Formal;
                        pdfACFormalPage.getParameters().put('TPId', wqTPId); // Added KK on 03/04/2016
                    }
                    if(processID == '24')
                    {
                        attachmentName = 'Letter of No Permit Required.pdf';
                        letterType = 'Letter of No Permit Required';
                        pdfACFormalPage = Page.CARPOL_AC_LiveDogs_NJLetter_Formal;
                        pdfACFormalPage.getParameters().put('TPId', wqTPId);
                        
                    }                    
                    Application__c currentApp = [SELECT Id, Name, Comments__c, Applicant_Name__r.Name, Applicant_Name__r.Email FROM Application__c WHERE ID = :applicationID LIMIT 1];
                    Authorizations__c associatedAuth = [SELECT ID, Name FROM Authorizations__c WHERE Application__c = :applicationID LIMIT 1];
                    System.Debug('<<<<<<< parm1 : ' + parm1 + ' >>>>>>');                  
                    pdfACFormalPage.getParameters().put('applicationID', applicationID);
                    //Niharika 3/8
                    pdfACFormalPage.getParameters().put('parm1', parm1 );
                    pdfACFormalPage.getParameters().put('strPathway', strPathway );
                    pdfACFormalPage.getParameters().put('Scientific_Name__c', Scientific_Name );
                    pdfACFormalPage.getParameters().put('Country_Of_Origin__c', Country_Of_Origin );
                    pdfACFormalPage.getParameters().put('State_Territory_of_Destination__c', State_Territory_of_Destination);
                    pdfACFormalPage.getParameters().put('Intended_Use__c', Intended_Use );
                    pdfACFormalPage.getParameters().put('RA_Scientific_Name__c', RA_Scientific_Name);
                    System.Debug('<<<<<<< Current Cookie value : ' + criteria + ' >>>>>>>'); 
                    pdfACFormalPage.setCookies(new Cookie[]{criteria});
                    Blob pdfACFormalBlob ;                    
                    System.Debug('<<<<<<< Page about to be loaded : ' + pdfACFormalPage + ' >>>>>>');
                    pdfACFormalBlob = pdfACFormalPage.getContent();                    
                    Attachment attachment = new Attachment(parentId = associatedAuth.ID, name = attachmentName, body = pdfACFormalBlob);
                    insert attachment;
                    System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');                    
                    Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();                        
                    // Strings to hold the email addresses to which you are sending the email.
                    String[] toAddressesAppEmail = new String[] {currentApp.Applicant_Name__r.Email}; 
                    String[] ccAddressesAppEmail = new String[] {'acarr@phaseonecg.com'};            
                    // Assign the addresses for the To and CC lists to the mail object.
                    appEmail.setToAddresses(toAddressesAppEmail);
                    appEmail.setCcAddresses(ccAddressesAppEmail);                        
                    // Specify the address used when the recipients reply to the email. 
                    appEmail.setReplyTo('support@efile.com');                    
                    // Specify the name used as the display name.
                    appEmail.setSenderDisplayName('eFile Support');                    
                    // Specify the subject line for your email.
                    appEmail.setSubject('USDA APHIS eFile - ' + letterType + ' (Response for Application : ' + currentApp.Name + ')');                    
                    // Set to True if you want to BCC yourself on the email.
                    appEmail.setBccSender(false);                        
                    // Optionally append the salesforce.com email signature to the email.
                    // The email address of the user executing the Apex Code will be used.
                    appEmail.setUseSignature(false);                    
                    String appEmailBody = 'Dear ' + currentApp.Applicant_Name__r.Name;
                    appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + currentApp.Name + '. ';
                    appEmailBody += 'Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online <a href=' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentApp.ID + '>click here.</a><br/><br/>';
                    appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                    appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/><br/><br/>APHIS Animal Care<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
            
                    appEmail.setHtmlBody(appEmailBody);                    
                    // Create the email attachment
                    /*Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                    emailAttachment.setFileName(attachmentName);
                    emailAttachment.setBody(pdfACFormalBlob);
                    appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment}); */                    
                    // Send the email you have created.
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });                    
                    //PageReference appDetails = new PageReference('/servlet/servlet.FileDownload?file=' + attachment.ID);
                      PageReference appDetails = new PageReference('/' + applicationID);
                    //PageReference appDetails = new PageReference('/' + associatedAuth.ID);
                    appDetails.setRedirect(true);
                    return appDetails;
                }
            }
            else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Application ID found.')); return null;}
        }
        catch(Exception e)
        {
            success = false;
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
}