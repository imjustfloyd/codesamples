public class CARPOL_BRS_Location_GPS_Coordinates {

    
    public Location__c objloc {get;set;}
    public boolean gps1 {get;set;}
    public boolean gps2 {get;set;}
    public boolean gps3 {get;set;}
    public boolean gps4 {get;set;}
    public boolean gps5 {get;set;}
    public boolean gps6 {get;set;}
    public string errmsg {get;set;}
    public string gpsno {get;set;}
    public CARPOL_BRS_Location_GPS_Coordinates()
    {
       
        // locId = ApexPages.currentPage().getParameters().get('id');
        system.debug('----locId----'+locId);
        
    }
    public String selectedRowIndex { get; set; }
    public Integer totalIngredients { get; set; }
    public Map<String, Integer> ingredientsMap = new Map<String, Integer>();
    public String mapKeyValue = '';
    public ID locId { get; set; }
    public Integer ingredientCount = 0;
    public List<Location__c > allLocations { get; set; }

        
    public CARPOL_BRS_Location_GPS_Coordinates(ApexPages.StandardController controller) {    
        errmsg = '';
        gpsno = '';
         objloc = new Location__c();
         gps1 = false;
         gps2 = false;
         gps3 = false;
         gps4 = false;
         gps5 = false;
         gps6 = false;    
        System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        locId = ApexPages.currentPage().getParameters().get('id');
         system.debug('----locId----'+locId);
          if(locId!=null){
            objloc = [select id,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c from Location__c where id =:locId];
            if(objloc.GPS_1__c!=null)
            gps1 = true;           
            if(objloc.GPS_2__c!=null)
            gps2 = true;
            if(objloc.GPS_3__c!=null)
            gps3 = true;
            if(objloc.GPS_4__c!=null)
            gps4 = true;
            if(objloc.GPS_5__c!=null)
            gps5 = true;
            if(objloc.GPS_6__c!=null)
            gps6 = true;
        }
        selectedRowIndex = '0';
       // allLocations = [select id,GPS_1__c,GPS_2__c,GPS_3__c,GPS_4__c,GPS_5__c,GPS_6__c from Location__c where id =:locId];
       
        System.Debug('<<<<<<< Total Ingredients : ' + totalIngredients + ' >>>>>>>');
    }
    
   public void saveloc()
    {
        errmsg = 'Location GPS coordinates have been saved';
        update objloc;
      //return null; 
    }
    public void addgps()
    {
      errmsg = '';
        if(gps1==false){
       gps1=true;
       }
       else if(gps2==false){
       gps2=true;
       }
       else if(gps3==false){
       gps3=true;
       }
       else if(gps4==false){
       gps4=true;
       }
       else if(gps5==false){
       gps5=true;
       }
       else if(gps6==false){
       gps6=true;
       }
       else{
           errmsg = 'You can only add 6 location GPS coordinates';
       }
     // return null; 
    }
    public void removegps()
    {
      errmsg = '';
      system.debug('-----gpsno-----'+gpsno);
      system.debug('-----gps3 -----'+gps3);
      if(gpsno=='1'){
          gps1 = false;
          objloc.GPS_1__c = '';
      }
      if(gpsno=='2'){
          gps2 = false;
          objloc.GPS_2__c = '';
      }
      if(gpsno=='3'){
          gps3 = false;
          objloc.GPS_3__c = '';
      }
      if(gpsno=='4'){
          gps4 = false;
          objloc.GPS_4__c = '';
      }
      if(gpsno=='5'){
          gps5 = false;
          objloc.GPS_5__c = '';
      }
      if(gpsno=='6'){
          gps6 = false;
          objloc.GPS_6__c = '';
      }
      update objloc;
      system.debug('-----gps3 -----'+gps3);
        
    }
    public PageReference saveIngredients()
    {
      return null; 
    }
    
    public PageReference addRow()
    {
       
       return null;
    }
    
    public PageReference removeRow()
    {
       return null;
    }
}