@isTest
public class CARPOL_VS_TestDataManager {
/*  ====================================================   */
/*  Name: CARPOL_VS_TestDataManager                        */  
/*  Related Components:                                    */
/*  ====================================================   */ 
/*  ====================================================   */
/*  Purpose: VS Specific Activities                        */  
/*  The creation of test records specific to VS functions  */  
/*  ====================================================   */ 
/*  ====================================================   */

//Object record type information

      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String apcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Live Dogs').getRecordTypeId();      
      String ACgroupRecordTypeId = Schema.SObjectType.Group__c.getRecordTypeInfosByName().get('Regulated Article').getRecordTypeId();      
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String SRJRecordTypeId = Schema.SObjectType.Signature_Regulation_Junction__c.getRecordTypeInfosByName().get('Singature_Regulation').getRecordTypeId();
      String AppAttRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
      String ACWFTRecordTypeId = Schema.SObjectType.Workflow_Task__c.getRecordTypeInfosByName().get('Process Authorization').getRecordTypeId();      
      
                              
      
//=====Utility functions=====//
public String randomString() {
  //return random string of 32 chars
  return EncodingUtil.convertToHex(Crypto.generateAesKey(128));
}

public String randomString5() {
    return randomString().substring(0, 5);
}

public void insertcustomsettings(){
    List<CARPOL_UNI_DisableTrigger__c> dtlist = new List<CARPOL_UNI_DisableTrigger__c>();
    Set<String> dtstrings = new Set<String>{'CARPOL_BRS_AuthorizationTrigger','CARPOL_BRS_ConstructTrigger','CARPOL_BRS_GenotypeTrigger','CARPOL_BRS_Link_RegulatedTrigger','CARPOL_BRS_LocationsTrigger Active','CARPOL_BRS_Reviewer_DuplicateTrigger','CARPOL_BRS_Self_ReportingTrigger','CARPOL_BRS_SOPTrigger','CARPOL_BRS_State_ReviewerTrigger'};
    for(String x : dtstrings){
       CARPOL_UNI_DisableTrigger__c dt = CARPOL_UNI_DisableTrigger__c.getInstance(x);
       if(dt == null){
           dt = new CARPOL_UNI_DisableTrigger__c(Name=x,Disable__c = false);  
           dtlist.add(dt);
       }
    }
    insert dtlist;
} 
 
//=====Supporting/Configuration records=====//
     
//newAccount - Must have an account to create a contact
 public account newAccount(string AccRecordTypeId ){
    Account objacct = new Account();
    objacct.Name = 'Global Account'+randomString5();
    objacct.RecordTypeId = AccRecordTypeId;    
    insert objacct; 
    system.assert(objacct != null);
    return objacct;
 }
 
 public Attachment newAttachment(id ACId){ 
    Attachment attach = new Attachment();
    attach.Body = blob.valueof('Test doc');
    attach.Name = 'Test doc'+randomString5();
    attach.ParentId = ACId;
    insert attach;  
    system.assert(attach != null);    
    return attach;  
 } 
 
 public Applicant_Attachments__c newAttach(id ACLinelid){
      Applicant_Attachments__c appAttach = new Applicant_Attachments__c();
      appAttach.Document_Types__c='Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement';
      appAttach.Animal_Care_AC__c=ACLinelid;
      appAttach.RecordTypeId= AppAttRecordTypeId;
      insert appAttach;
      system.assert(appAttach != null);       
      return appAttach;  
    }
 
 public breed__c newbreed(){
      Breed__c brd = new Breed__c();
      brd.Name='New Breed'+randomString5();
      insert brd; 
      system.assert(brd != null);      
      return brd;  
 }
 
 public Applicant_Contact__c newappcontact() {
     //creating this calls the validateAddress trigger
     Level_1_Region__c appL1R = newlevel1regionAL();
     
     Applicant_Contact__c apcont = new Applicant_Contact__c();
     apcont.First_Name__c = 'apcont'+randomString5();
     apcont.Email_Address__c = 'apcont@test.com';
     apcont.Name = 'Associated Contacts'+randomString5();
     //if the field below isn't assigned the AddressVerify class breaks //- DMS 4/4/2016
     apcont.Mailing_Country__c = 'United States';
     apcont.Mailing_Country_LR__c = appL1R.Country__c;
     apcont.Mailing_State_LR__c = appL1R.Id;
     apcont.Mailing_State__c = 'Alabama';
     apcont.RecordTypeId = apcontRecordTypeId;
     Test.setMock(HttpCalloutMock.class, new AddressVerifyMockImpl());
     insert apcont;
     system.assert(apcont != null);     
     return apcont;
 } 
 
 public Communication_Manager__c newCommunicationmanager(string type){
     Communication_Manager__c communmang = new Communication_Manager__c();        
     communmang.Content__c = type;
     communmang.Description__c = 'test description'+randomString5();
     communmang.Type__c = type;
     insert communmang;
     system.assert(communmang != null);             
     return communmang;  
 } 
 
//new Contact - Must have a contact to create an application
 public Contact newContact(){
    Contact objCont = new Contact();
    objCont.FirstName = 'Global Contact'+randomString5();
    objcont.LastName = 'LastName'+randomString5();
    objcont.Email = randomString5()+'test@email.com';        
    objcont.AccountId = newAccount(AccountRecordTypeId).id;
    objcont.MailingStreet = 'Mailing Street'+randomString5();
    objcont.MailingCity = 'Mailing City'+randomString5();
    objcont.MailingState = 'Ohio';
    objcont.MailingCountry = 'United States';
    objcont.MailingPostalCode = '32092';    
    insert objcont; 
    system.assert(objcont != null);    
    return objcont;
 }
  
 public Country__c newcountryaf(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'Afghanistan';
    objcountry.Country_Code__c = 'AF';
    objcountry.Country_Status__c = 'Active';
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    system.assert(objcountry != null);    
    return objcountry;
 }
 
 public Country__c newcountrywithassoc(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'Pakistan';
    objcountry.Country_Code__c = 'PK';
    objcountry.Country_Status__c = 'Active';
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    //create the country junction    
    newcountryjunction(objcountry, newgroup());
    system.assert(objcountry != null);    
    return objcountry;
 } 
 
 public Country__c newcountryus(){
    Country__c objcountry = new Country__c();
    objcountry.Name = 'United States of America';
    objcountry.Country_Code__c = 'US';
    objcountry.Country_Status__c = 'Active';    
    objcountry.Trade_Agreement__c = newta().Id;
    insert objcountry;
    system.assert(objcountry != null);    
    return objcountry;
 }
 
  public Country_Junction__c newcountryjunction(Country__c cntry, Group__c grp){
    Country_Junction__c objcountryjn = new Country_Junction__c();
    objcountryjn.Country__c = cntry.Id;
    objcountryjn.Group__c = grp.Id;
    insert objcountryjn;
    system.assert(objcountryjn != null);    
    return objcountryjn;
 }
 //Custom setting
 public CARPOL_External_Landing__c newExternalLanding(String extName, Integer seqNumb){
     CARPOL_External_Landing__c extLand = new CARPOL_External_Landing__c();
     extLand.Name = extName;
     extLand.Is_Active__c = true;
     extLand.Sequence__c = seqNumb;
     insert extLand;
     system.assert(extLand != null);     
     return extLand;
 }
 
 //New facility - calling this method fires the CARPOL_AC_MasterFacilityTrigger
 //Note GooglePopulateTimeZone will not be called in this method because mock implementation in test not set
 public Facility__c newfacility(String type){
    Facility__c fac = new Facility__c();
    fac.RecordTypeId = PortsFacRecordTypeId;
    fac.Name = 'entryport';
    fac.Type__c = type;  
    insert fac;
    system.assert(fac != null);            
    return fac;  
 }
 
  public Group__c newgroup(){
    Group__c grp = new Group__c();
    grp.Name = 'Test';
    grp.RecordTypeId = ACgroupRecordTypeId;
    grp.Group_Custom_Name__c = 'Test';
    grp.Status__c = 'Active';
    insert grp;  
    system.assert(grp != null);          
    return grp;  
 }
 
 public Intended_Use__c newIntendedUse(Id plip){
     Intended_Use__c objIU = new Intended_Use__c();
     objIU.Name = 'Personal Use';
     objIU.RecordTypeID = Schema.SObjectType.Intended_Use__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
     objIU.Department__c = 'Live Dogs';
     objIU.Intended_Use_Description__c = 'Live Dogs';
     objIU.Intended_Use_Id__c = 1;
     //lookup
     if(plip == null){
         objIU.Program_Line_Item_Pathway__c = newCaninePathway().Id;
     } else {
         objIU.Program_Line_Item_Pathway__c = plip;
     }
     insert objIU;
     system.assert(objIU != null);     
     return objIU;
 }
 
 public Level_1_Region__c newlevel1regionAL(){
     Level_1_Region__c objL1R = new Level_1_Region__c();
     objL1r.Country__c = newcountryus().Id;
     objL1r.Name = 'Alabama';
     objL1r.Level_1_Name__c = 'Alabama';
     objL1r.Level_1_Region_Code__c = 'AL';
     objL1r.Level_1_Region_Status__c = 'Active';
     insert objL1r;
     system.assert(objL1r != null);     
     return objL1R;
 }
 
 public Level_1_Region__c newlevel1regionMD(){
     Level_1_Region__c objL1R = new Level_1_Region__c();
     objL1r.Country__c = newcountryus().Id;
     objL1r.Name = 'Maryland';
     objL1r.Level_1_Name__c = 'Maryland';
     objL1r.Level_1_Region_Code__c = 'MD';
     objL1r.Level_1_Region_Status__c = 'Active';
     insert objL1r;
     system.assert(objL1r != null);     
     return objL1R;
 } 
 
 public Program_Prefix__c newPrefix(){
     Program_Prefix__c objPrefix = new Program_Prefix__c();
     objPrefix.Program__c = newProgram('AC').Id;
     objPrefix.Name = '555';
  
     insert objPrefix; 
     system.assert(objPrefix != null);            
     return objPrefix;  
 }
 
 public Domain__c newProgram(String prog){
     Domain__c objProg = new Domain__c();
     objProg.Name = prog;
     objProg.Active__c = true;
     insert objProg;
     system.assert(objProg != null);     
     return objProg;
 }
  
 public Program_Line_Item_Pathway__c newCaninePathway(){
    Program_Line_Item_Pathway__c objParentPathway = new Program_Line_Item_Pathway__c();
    objParentPathway.Program__c = newProgram('VS').Id;
    objParentPathway.Name = 'Live Animals';   
    insert objParentPathway;
    system.assert(objParentPathway != null);    

    Program_Line_Item_Pathway__c objPathway= new Program_Line_Item_Pathway__c();
    objPathway.Program__c = newProgram('Canines').Id;
    objPathway.Name = 'Canines';       
    objPathway.Parent_Id__c = objParentPathway.Id;
    objPathway.Is_Country_Required__c = true;
    objPathway.Is_State_of_Destination_Required__c = true;
    objPathway.Show_Intended_Use__c = true;
    objPathway.Validate_for_Disease__c = 'Rabies';
    objPathway.Show_Required_Documents_Button__c = true;
    objPathway.Show_Photos_Button__c = true;
    //Live Dogs record type may be needed
    //objPathway.Required_Documents__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement';
    insert objPathway;

    return objPathway;
 }
 
    public Regulated_Article__c newRegulatedArticle(ID plip){
        Regulated_Article__c regArt = new Regulated_Article__c();
        regArt.Name = 'Test article';
        regArt.RecordTypeID = Schema.SObjectType.Regulated_Article__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regArt.Status__c = 'Active';
        regArt.Category_Group_Ref__c = newgroup().Id;
        regArt.Program_Pathway__c = plip;
               
        insert regArt;
        //create a junction too
        RA_Scientific_Name_Junction__c raJunc = new RA_Scientific_Name_Junction__c();
        raJunc.Program_Pathway__c = plip;
        raJunc.Regulated_Article__c = regArt.id;
        insert raJunc;
        system.assert(regArt != null);        
        return regArt;
    }
 
    public Regulation__c newRegulation(string type,string subtype){
      Regulation__c objreg = new Regulation__c();
      objreg.Status__c = 'Active';
      objreg.Type__c =  type; //'Import Requirements';
      objreg.Custom_Name__c = 'testcustname'+randomString5();
      objreg.Short_Name__c = 't'+randomString5();
      objreg.Title__c = 'title'+randomString5();
      objreg.Regulation_Description__c = 'descp1'+randomString5();
      objreg.Sub_Type__c = subtype;//'Import Permit Requirements';
      objreg.RecordTypeID = ACRegRecordTypeId;
      insert objreg;
      system.assert(objreg != null);       
      return objreg;  
    } 
 
    public Signature__c newThumbprint(){
      Signature__c objTP = new Signature__c();
      objTP.Name = 'Test AC TP Jialin';
      objTP.Recordtypeid = ACTPRecordTypeId;
      objTP.Program_Prefix__c = newPrefix().Id;
      
  
      insert objTP;  
      system.assert(objTP != null);            
      return objTP;  
    }
    
    public Signature_Regulation_Junction__c newSRJ(id tpId,id RegId){ 
      Signature_Regulation_Junction__c objSRJ = new Signature_Regulation_Junction__c();
      objSRJ.Recordtypeid = SRJRecordTypeId;
      objSRJ.Signature__c = tpId;
      objSRJ.Regulation__c = RegId;//objreg.id;
      insert objSRJ; 
      system.assert(objSRJ != null);      
      return objSRJ;   
    } 
 
 public Trade_Agreement__c newta(){ 
    Trade_Agreement__c ta = new Trade_Agreement__c();
    ta.Name = 'test ta';
    ta.Trade_Agreement_Code__c = 'ta';
    ta.Trade_Agreement_Status__c ='Active';
    insert ta;
    system.assert(ta != null);    
    return ta;
 }
 
 public Wizard_Questionnaire__c newWizardQuestionnaire(ID plip, ID intendeduse){
     //Auto number
     Wizard_Questionnaire__c wq = new Wizard_Questionnaire__c();
     wq.RecordTypeID = Schema.SObjectType.Wizard_Questionnaire__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
     wq.Question__c = 'Test question description';
     wq.Intended_Use__c = intendeduse;
     wq.Program_Line_Item_Pathway__c = plip;
     wq.Type__c = 'START';
     insert wq;
     system.assert(wq != null);     
     return wq;
 }
 
 public VS__c newVS(Id ImportedFullId, Id ApplicationId) {
        VS__c VS = new VS__C();
        VS.Application_Number__c = ApplicationId;
        VS.Importer_Full_Name__c = ImportedFullId;
        VS.Hand_Carry__c = 'No';
        VS.Quantity__c = '5';
        VS.Purpose_of_Importation__c = 'Commercial/Retail sale directly to the consumer';
        VS.Intended_Use__c = 'Humar Consumption';
        VS.Product_Category__c = 'birds nest';
        VS.Method_of_Final_Disposition__c = 'Autoclaving';
        insert VS;
        system.assert(VS != null);        
        return VS;
    }
 
 //======Primary records=====//

//Application - this insert triggers the CARPOL_UNI_MasterApplicationTrigger

 public Application__c newapplication(){
    Application__c objapp = new Application__c();
    objapp.Applicant_Name__c = newContact().Id;
    objapp.Recordtypeid = ACAppRecordTypeId;
    objapp.Application_Status__c = 'Open';

    insert objapp;  
    system.assert(objapp != null);    
    return objapp;        
 }
 
 //Authorization - - this insert triggers the CARPOL_UNI_MasterAuthorizationTrigger
 public Authorizations__c newAuth(id appid){
    Authorizations__c objauth = new Authorizations__c();
    if(appid == null){
        objauth.Application__c = newapplication().Id;
    } else {
        objauth.Application__c = appid;
    }    
//    objauth.Application__c = appid;
    objauth.RecordTypeID = ACauthRecordTypeId;
    objauth.Status__c = 'Submitted';
    objauth.Authorization_Type__c = 'Permit';
    objauth.Date_Issued__c = date.today();
    objauth.Applicant_Alternate_Email__c = 'test@test.com';
    objauth.UNI_Zip__c = '32092';
    objauth.UNI_Country__c = 'United States';
    objauth.UNI_County_Province__c = 'Duval';
    objauth.BRS_Proposed_Start_Date__c = date.today()+30;
    objauth.BRS_Proposed_End_Date__c = date.today()+40;
    objauth.CBI__c = 'CBI Text';
    objauth.Application_CBI__c = 'No';
    objauth.Applicant_Alternate_Email__c = 'email2@test.com';
    objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
    objauth.AC_Applicant_Email__c = 'email2@test.com';
    objauth.AC_Applicant_Fax__c = '(904) 123-2345';
    objauth.AC_Applicant_Phone__c = '(904) 123-2345';
    objauth.AC_Organization__c = 'ABC Corp';
    objauth.Means_of_Movement__c = 'Hand Carry';
    objauth.Biological_Material_present_in_Article__c = 'No';
    objauth.If_Yes_Please_Describe__c = 'Text';
    objauth.Applicant_Instructions__c = 'Make corrections';
    objauth.BRS_Number_of_Labels__c = 10;
    objauth.BRS_Purpose_of_Permit__c = 'Importation';
    objauth.Documents_Sent_to_Applicant__c = false;
    insert objauth;
    system.assert(objauth != null);    
    return objauth;  
 } 
 
    public Authorization_Junction__c newAuthorizationJunction(id authId,id RegId){ 
      Authorization_Junction__c objauthjun = new Authorization_Junction__c();
      objauthjun.Authorization__c = authId;
      objauthjun.Regulation__c = RegId;//objreg.id;
      insert objauthjun; 
      system.assert(objauthjun != null);      
      return objauthjun;   
    } 

//Line Item(s) - Insertion triggers the CARPOL_UNI_MasterLineItemTrigger
public AC__c newLineItem(string poi, Application__c application){
    //default record type 'Live Dogs'
    
    Facility__c  entry = newfacility('Domestic Port');
    Facility__c  embarkation = newfacility('Foreign Port');
      
    AC__c ac = new AC__c();
    Applicant_Contact__c appcont = newappcontact();
    if(application == null){
        ac.Application_Number__c = newapplication().Id;
    } else {
        ac.Application_Number__c = application.Id;
    }
    ac.Departure_Time__c = date.today()+11;
    ac.Arrival_Time__c = date.today()+15;
    ac.Proposed_date_of_arrival__c = date.today()+15;
    ac.Port_of_Entry__c = entry.Id;
    ac.Port_of_Embarkation__c = embarkation.id;
    ac.Transporter_Type__c = 'Ground';
    ac.RecordTypeId = ACLIRecordTypeId;
    ac.Date_of_Birth__c = date.today()-400;
    ac.Breed__c = newbreed().id;
    ac.Color__c = 'Brown';
    ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
    ac.Sex__c = 'Male';
    ac.Country_Of_Origin__c = newcountryus().Id;
    ac.Program_Line_Item_Pathway__c = newCaninePathway().Id;    
    ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
    ac.Importer_Last_Name__c = appcont.id;
    ac.Exporter_Last_Name__c = appcont.id;
    ac.Status__c = 'Saved';
    ac.Purpose_of_the_Importation__c = poi; //'Resale/Adoption'
    ac.DeliveryRecipient_Last_Name__c = appcont.id;
    insert ac;
    system.assert(ac != null);    
    return ac;  
}
  
//Line Item(s) - Insertion triggers the CARPOL_UNI_MasterLineItemTrigger
public AC__c newLineItem(string poi, Application__c application, Authorizations__c authorization){
    //default record type 'Live Dogs'
    
    Facility__c  entry = newfacility('Domestic Port');
    Facility__c  embarkation = newfacility('Foreign Port');
      
    AC__c ac = new AC__c();
    Applicant_Contact__c appcont = newappcontact();
    if(application == null){
        ac.Application_Number__c = newapplication().Id;
    } else {
        ac.Application_Number__c = application.Id;
    }
    if(authorization != null){
        ac.Authorization__c = authorization.id;
    }
    ac.Departure_Time__c = date.today()+11;
    ac.Arrival_Time__c = date.today()+15;
    ac.Proposed_date_of_arrival__c = date.today()+15;
    ac.Port_of_Entry__c = entry.Id;
    ac.Port_of_Embarkation__c = embarkation.id;
    ac.Transporter_Type__c = 'Ground';
    ac.RecordTypeId = ACLIRecordTypeId;
    ac.Date_of_Birth__c = date.today()-400;
    ac.Breed__c = newbreed().id;
    ac.Color__c = 'Brown';
    ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
    ac.Sex__c = 'Male';
    ac.Country_Of_Origin__c = newcountryus().Id;
    ac.Program_Line_Item_Pathway__c = newCaninePathway().Id;    
    ac.Treatment_available_in_Country_of_Origin__c  = 'No';    
    ac.Importer_Last_Name__c = appcont.id;
    ac.Exporter_Last_Name__c = appcont.id;
    ac.Status__c = 'Saved';
    ac.Purpose_of_the_Importation__c = poi; //'Resale/Adoption'
    ac.DeliveryRecipient_Last_Name__c = appcont.id;
    insert ac;
    system.assert(ac != null);    
    return ac;  
}

//History records should be generated by GenericHistoryClass

//Applicant Attachmentsw


public Workflow_Task__c newworkflowtask(string wfname, Authorizations__c auth, string status){
     Workflow_Task__c wtask = new Workflow_Task__c();
     wtask.RecordTypeId = ACWFTRecordTypeId;
     wtask.Name = wfname; //Process Authorization;
     if(auth == null){
         wtask.Authorization__c = newAuth(null).Id;
     } else {
         wtask.Authorization__c = auth.Id;
     }
     wtask.Status__c = status; //'Complete'
     wtask.Buttons__c = '';
     insert wtask;
     system.assert(wtask != null);     
     return wtask;
}

//Agreements

//Inspections

//Documents


}