/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRSLinkConstructController_Test
@CreateDate: 9/23/15
@Description: Test class for CARPOL_BRSLinkConstructController
@Version: 1.0
@reference: CARPOL_BRSLinkConstructController(apex class)
*/

@isTest
public class CARPOL_BRSLinkConstruct_Test{

    public static testmethod void test(){
          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();

          Application__c objapp = testData.newapplication();

          ApexPages.currentPage().getParameters().put('appid',objapp.id);       
          CARPOL_BRSLinkConstruct cc = new CARPOL_BRSLinkConstruct();
          cc.save();
          cc.cancel();
          system.assert(cc != null);
    }

}