public class BatchExternalMigrationIdCreation {
//global class BatchExternalMigrationIdCreation implements Schedulable{

    //get the config objects
    List<Config_Objects__c> configobjs = new List<Config_Objects__c>();
    List<Config_Email__c> configemail = new List<Config_Email__c>();
    String dbquery_prefix = 'Select ID, External_Migration_Id__c, CreatedDate, IsDeleted FROM ';
    String dbquery_suffix = ' Order By External_Migration_Id__c, CreatedDate ALL ROWS';
    public List<String> counts{get;set;}
    
    List<SObject> wasdeleted{get;set;}
    List<SObject> generateid{get;set;}
    Map<String, SObject> existing{get;set;}
    List<Sobject> updatelist{get;set;}
    public BatchExternalMigrationIdCreation () {
    }
    public void doStuff(){

        counts = new List<String>();
        updatelist = new List<SObject>();
        //retrieve config objects
        configobjs = Config_Objects__c.getall().values();
        configemail = Config_Email__c.getall().values();        
        
        for (Config_Objects__c config: configobjs){
            //set these up for each object
            wasdeleted = new List<SObject>();
            generateid = new List<SObject>();    
            existing = new Map<String, SObject>();    
            String fullstring = dbquery_prefix + config.APIName__c + dbquery_suffix;
            List<SObject> records = Database.query(fullstring);
            if(!records.IsEmpty()){
                //sift the record
                for(Sobject s : records){
                    //capture deleted records
                    if((Boolean)s.get('IsDeleted') == true){
                        wasdeleted.add(s);
                        continue;
                    }
                    //get the external migration id field value
                    String ext_id = (String)s.get('External_Migration_Id__c');
                    //capture null field records
                    if(ext_id == null){
                        generateid.add(s);
                        continue;
                    }
                    if(ext_id.tolowercase() == 'dummy'){
                        //skip ones marked dummy
                        continue;
                    } 
                    //prepare to deduplicate, compare creation dates
                    if(!existing.containsKey(ext_id)){
                        existing.put(ext_id, s);
                    } else {
                        //the earliest one should keep the external id
                        DateTime currentrecord = (DateTime)existing.get(ext_id).get('CreatedDate');
                        DateTime newrecord = (DateTime)s.get('CreatedDate');
                        //if current date is before new date put the new one in the generate list
                        if(currentrecord < newrecord){
                            generateid.add(s);
                        } else {
                            //get the SObject in the list
                            Sobject rec = existing.get(ext_id);
                            //put that record in the list for new generation
                            generateid.add(rec);
                            //remove the existing record
                            existing.remove(ext_id);
                            //put the new one in the map
                            existing.put(ext_id, s);
                        }
                    }
                    //should have all the records sorted now
                }

            }
            //Get today's date
            Date rundate = Date.Today();
            Date lastrun;
            Integer startnumber = 0;
            //Get last date run from config record
            if(config.LastRunDate__c != null){
                lastrun = config.LastRunDate__c;
            }
            //if today's date = last date run get the last number assigned, increment by 1
            if(lastrun == rundate){
                if(config.LastRunInteger__c != null){
                    startnumber = Integer.valueof(config.LastRunInteger__c);
                }
            }
            //assemble string <first letter of api name> - date - incrementnumber
            String datestring = string.valueof(rundate.year()) + '-' + string.valueof(rundate.month()) + '-' + string.valueof(rundate.day());
            String newexternalid; 
            if(!generateid.isempty()){        
                //for loop generate id assign external key incrementing number as loop progresses
                for(Sobject assignee : generateid){
                    startnumber++;
                    newexternalid = config.Name.left(1) + '-' + datestring + '-' + startnumber;
                    assignee.put('External_Migration_Id__c',newexternalid);
                    //add all records to update Sobject list
                    updatelist.add(assignee);
                }
            }
            //some logging
            String report = config.Name + ': ' + 'Del: ' + wasdeleted.size() + ', Gen: ' + generateid.size() + ', Ext: ' + existing.size() + ' - ';
            counts.add(config.Name + ':');
            counts.add('  Number Deleted: ' + wasdeleted.size());
            counts.add('  Number Generated: ' + generateid.size());            
            counts.add('  Number Existed: ' + existing.size());                        
            counts.add('========================================');
            counts.add('  Generated Ids' + ':');
            for(Sobject x : generateid){
                counts.add('    Id: ' + x.get('Id') + ' - external id: ' + x.get('External_Migration_Id__c'));
            }
            counts.add('========================================');            
            
            counts.add('  Deleted Ids' + ':');
            report = report + 'Del Ids: ';
            for(Sobject x : wasdeleted){
                report = report + x.get('Id') + ':' + x.get('External_Migration_Id__c') + ',';
                counts.add('    Id: ' + x.get('Id') + ' - external id: ' + x.get('External_Migration_Id__c'));
            }            
            counts.add('========================================'); 
            
            if(!configemail.isEmpty()){
                for(Config_Email__c ce : configemail){
                  //      ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), ce.User_Id__c, ConnectApi.FeedElementType.FeedItem, report);                                       
                }
            }            
            if(!updatelist.isempty()){
                update updatelist;
            }
            updatelist.clear();    
            
            //update the config record
            config.LastRunDate__c = rundate;
            config.LastRunInteger__c = startnumber;
            
            //report out deletions for this object
        } // end config loop
        //if sobject list not empty update list
        update configobjs;
        
        //send email of deleted items and counts report
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        List<String> toAddresses = new List<String>();
        if(!configemail.isEmpty()){
            for(Config_Email__c ce : configemail){
                toAddresses.add(ce.User_Id__c);
            }
        }
        message.setToAddresses(toAddresses);
        message.subject = 'External Id Processing Report - ' + Date.today();
        message.setHtmlBody(String.join(counts,'<br/>'));
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
        
                
   } //end main method
}