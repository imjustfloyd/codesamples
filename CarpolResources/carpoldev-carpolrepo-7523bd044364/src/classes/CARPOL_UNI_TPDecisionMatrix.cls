//Obtain Thumbprint from Decision Matrix when Crieteria Group is NULL

public class CARPOL_UNI_TPDecisionMatrix {
    
    public Program_Line_Item_Pathway__c programlineItemPathway;
     Public list<Regulations_Association_Matrix__c> decisionMatrixList;
    Public list<ThumbPrint> ThumbPrintList;

    public list<ThumbPrint> checkForDecisionMatrix(String country,string regulatedarticle , String ProgPathID,string countryName){ //VV added country selected by applicant
        system.debug('ProgPathID##'+ProgPathID);
        system.debug('country ###'+country);
        system.debug('regulatedarticle ###'+regulatedarticle);
        programlineItemPathway = [SELECT ID,Is_Country_Required__c,Is_State_Required__c,Program__c,Parent_Id__c
                                  FROM Program_Line_Item_Pathway__c WHERE Id =: ProgPathID ];
       system.debug('programlineItemPathway ###'+programlineItemPathway);
      decisionMatrixList = new  list<Regulations_Association_Matrix__c>();
      system.debug('programlineItemPathway.Is_Country_Required__c ###'+programlineItemPathway.Is_Country_Required__c);
      if(programlineItemPathway.Is_Country_Required__c )
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,Condition__r.Name,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Warning_Message__c,Criteria_Group__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c=:country 
                            AND Regulated_Article__c=:regulatedarticle         // VV added
                           /* AND Excluded_Countries__c EXCLUDES(:countryName) */];  // VV added to skip if selcted country is in exclusion list
                            
                            
        system.debug('-----decisionMatrixList:--'+decisionMatrixList)  ;                                          
                                    
      ThumbPrintList = new list<ThumbPrint>();   
      
      for(Regulations_Association_Matrix__c ds: decisionMatrixList){
          ThumbPrint tp = new ThumbPrint(ds.Country__r.Name,ds.Level_1_Region__r.Name,ds.Level_2_Region__r.Name,ds.Criteria_Group__c,ds.Thumbprint__r.Response_Type__c,ds.Thumbprint__c,ds.Thumbprint__r.Communication_Manager__c,ds.Warning_Message__c);
          ThumbPrintList.add(tp);
      }
      
    system.debug('---------------ThumbPrintList'+ThumbPrintList);                                
    if(ThumbPrintList.size()>0) 
        return ThumbPrintList;
     else
        return null;
    }
    
   // /*niharika
    public list<ThumbPrint> checkForDecisionMatrixWithGroup(String country,string regulatedarticle , String ProgPathID,String groupID, string countryName){
        system.debug('ProgPathID##'+ProgPathID);
        system.debug('country ###'+country);
        system.debug('regulatedarticle ###'+regulatedarticle);
        programlineItemPathway = [SELECT ID,Is_Country_Required__c,Is_State_Required__c,Program__c,Parent_Id__c
                                  FROM Program_Line_Item_Pathway__c WHERE Id =: ProgPathID ];
       system.debug('programlineItemPathway ###'+programlineItemPathway);
      decisionMatrixList = new  list<Regulations_Association_Matrix__c>();
      system.debug('programlineItemPathway.Is_Country_Required__c ###'+programlineItemPathway.Is_Country_Required__c);
      if(programlineItemPathway.Is_Country_Required__c )
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,Condition__r.Name,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Warning_Message__c,Criteria_Group__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND (Country__c=:country OR Country__c=null ) 
                            AND (Regulated_Article__c=:regulatedarticle OR Regulated_Article__c=null ) 
                            AND Criteria_Group__c=:groupID 
                           /* AND Excluded_Countries__c EXCLUDES(:countryName)*/ ];  // VV added to skip if selcted country is in exclusion list
                            
                            
        system.debug('-----decisionMatrixList:--'+decisionMatrixList)  ;                                          
                                    
      ThumbPrintList = new list<ThumbPrint>();   
      
      for(Regulations_Association_Matrix__c ds: decisionMatrixList){
          ThumbPrint tp = new ThumbPrint(ds.Country__r.Name,ds.Level_1_Region__r.Name,ds.Level_2_Region__r.Name,ds.Criteria_Group__c,ds.Thumbprint__r.Response_Type__c,ds.Thumbprint__c,ds.Thumbprint__r.Communication_Manager__c,ds.Warning_Message__c);
          ThumbPrintList.add(tp);
      }
      
    system.debug('---------------ThumbPrintList'+ThumbPrintList);                                
    if(ThumbPrintList.size()>0) 
        return ThumbPrintList;
     else
        return null;
    }
    
    
    
    
     
    public class ThumbPrint{
        
        public String country;
        String state;
        String county;
        public String CriteriaGroup;
        public String thumbPrintResponseType;
        public String thumbPrintID;
        String communicationManagerID;
        public String Warning_Message;
        
        public ThumbPrint(String con,String st,String county1,String cg,String responseType,String thumbid, String communicationManager,String Warning_Message){  //Boolean intendeduse,
            
            this.country = con;
            this.state = st;
            this.county = county1;
            this.CriteriaGroup = cg;
            this.thumbPrintResponseType=responseType;
            this.thumbPrintID=thumbid;
            this.communicationManagerID=communicationManager;
            this.Warning_Message=Warning_Message;
            
            
        }
    }

}