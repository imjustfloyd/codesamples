/*
   Author       : Raghu Parupalli    
   Created Date : 01/05/2016
   Purpose      : As an applicant, after adding a lineitem, they are directed to this controller
                  which displays all the related records of the line item and also has the 
                  buttons to create related records to the lineitem based on the program 
                  line item pathways.  
*/
public class CARPOL_LineItemPageForApplicant{

public id LineId {get;set;}
public id LineItemId;
public id AuthId {get;set;}
public Id strPathwayId{get;set;}
public boolean aabutton{get;set;}
public boolean constructbutton {get;set;}
public boolean sopbutton {get;set;}  
public boolean locationbutton {get;set;}  
public boolean RAbutton {get;set;}  
public boolean BRSPermitInfo {get;set;}
public boolean BRSPermitReleaseInfo {get;set;}    
public boolean BRSNotificationInfo {get;set;}
public boolean BRSCourtesyInfo {get;set;}
public boolean prevReviewConst {get;set;} 
public List<AC__c> Linelst {get;set;}
public id AppId;
public string redirect {get;set;}
public string recTypeName{get;set;}
public string releaselocationinfo{get;set;}
public Id recordTypeId {get;set;}
public boolean SOPDesignProtocol {get;set;}
public String wfStatus {get;set;}
public boolean displayCompAgreementButton {get;set;}
public Map<String,String> sobjectkeys {get;set;}
public string delrecid {get;set;}
public List<Workflow_Task__c> wfLst {get;set;}
public string errmsg{get;set;}
// vv added
public List<Regulated_Article__c> listRA {get;set;}    
        public string sSelCat {get;set;}

    public CARPOL_LineItemPageForApplicant(ApexPages.StandardSetController controller){        
        Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
        sobjectkeys = new Map<String,String>();
            for(String s:describe.keyset()){
            sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
            }
        BRSPermitReleaseInfo = false;
        SOPDesignProtocol = false;
        BRSNotificationInfo = false; 
        BRSPermitInfo = false;
        BRSCourtesyInfo = false;
        LineId = ApexPages.currentPage().getParameters().get('LineId');  
        LineItemId = LineId; 
        strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');  
        Linelst = [SELECT AC_Manager_Comments__c,AC_Unique_Id__c,Additional_Information__c,
                                Age_in_months__c,Age_yr_months__c,Air_Transporter_Flight_Number__c,
                                Applicant_Instructions__c,Application_Duration__c,Application_Number__c,
                                Arrival_Date_and_Time__c,Arrival_Time__c,Authorization_Content__c,Authorization__c,
                                Biological_Material_present_in_Article__c,Breed_Description__c,Breed_Name__c,Breed__c,
                                CBI_Justification__c,Clerk_Comments__c,Color__c,Communication_Letter__c,
                                Contains_Applicant_Attachments__c,Country_and_Locality_Information__c,
                                Country_Of_Origin__c,Courtesy_Justification__c,CreatedById,CreatedDate,
                                Date_of_Birth__c,DeliveryRecipient_Email__c,DeliveryRecipient_Fax__c,
                                DeliveryRecipient_First_Name__c,DeliveryRecipient_Last_Name__c,
                                DeliveryRecipient_Mailing_City__c,DeliveryRecipient_Mailing_CountryLU__c,
                                DeliveryRecipient_Mailing_Street__c,DeliveryRecipient_Mailing_Zip__c,
                                DeliveryRecipient_Phone__c,DeliveryRecipient_USDA_License_Number__c,
                                DeliveryRecipient_USDA_Registration_Num__c,DeliveryRec_USDA_License_Expiration_Date__c,
                                DeliveryRec_USDA_Regist_Expiration_Date__c,Delivery_Recipient_State_ProvinceLU__c,
                                Deny_Resubmit__c,Departure_Date_and_Time__c,Departure_Time__c,
                                Does_This_Application_Contain_CBI__c,Domain__c,Enforcement_Officer_Comments__c,
                                Exporter_Email__c,Exporter_Fax__c,Exporter_First_Name__c,Exporter_Last_Name__c,
                                Exporter_Mailing_City__c,Exporter_Mailing_CountryLU__c,
                                Exporter_Mailing_State_ProvinceLU__c,movement_type__c,
                                Exporter_Mailing_Street__c,Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,
                                F_Importer_First_Name__c,Hand_Carry__c,Health_Certificate_Attached__c,Id,
                                If_yes_Please_Describe__c,Importer_Email__c,Importer_Fax__c,Importer_First_Name__c,
                                Importer_Last_Name__c,Importer_Mailing_City__c,Importer_Mailing_CountryLU__c,
                                Importer_Mailing_State_ProvinceLU__c,
                                Importer_Mailing_Street__c,Importer_Mailing_ZIP_code__c,
                                Importer_Phone__c,Importer_USDA_License_Expiration_Date__c,
                                Importer_USDA_License_Number__c,Importer_USDA_Registration_Exp_Date__c,
                                Importer_USDA_Registration_Number__c,Intended_Use__c,Introduction_Type__c,IsDeleted,
                                Is_violator__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,
                                LastViewedDate,Locked__c,Means_of_Movement__c,Microchip_Number__c,Move_out_Hawaii__c,
                                Name,Number_of_Labels__c,Number_of_New_Design_Protocols__c,Number_of_Release_Sites__c,
                                Other_identifying_information__c,OwnerId,Picture__c,Port_of_Embarkation__c,
                                Port_of_Entry_State__c,Port_of_Entry__c,Processed_anyway_that_change_its_nature__c,
                                Program_Line_Item_Pathway__c,Proposed_date_of_arrival__c,Proposed_End_Date__c,
                                Proposed_Start_Date__c,Purpose_of_Permit__c,Purpose_of_the_Importation__c,
                                Quarantine_Facility__c,Rabies_Vaccination_Certificate_Attached__c,RecordTypeId,
                                RecordType.Name,Rejected_Date__c,Renew_Authorization__c,Reviewer_Comments__c,
                                Seed_be_planted_or_propagated__c,Sex__c,Status__c,Street_Address__c,
                                SystemModstamp,Tattoo_Number__c,Test_Field__c,Thumbprint__c,
                                Time_Zone_of_Arriving_Place__c,Time_Zone_of_Departing_Place__c,
                                Total_of_New_Constructs__c,Transporter_Name__c,Transporter_Type__c,
                                Treatment_available_in_Country_of_Origin__c,USDA_Expiration_Date__c,
                                USDA_License_Expiration_Date__c,USDA_License_Number_Format__c,USDA_License_Number__c,
                                USDA_Registration_Number_Format__c,USDA_Registration_Number__c,
                                Vet_Agreement_Attached__c,Will_the_Seed_be_processed_in_any_way_th__c
                                FROM AC__c WHERE ID = :LineItemId ];  
       if(Linelst.size() > 0) {                         
        AppId = Linelst[0].Application_Number__c; 
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Notification' || Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit'){
            SOPDesignProtocol = true;
        }
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Notification'){
            BRSNotificationInfo = true;
        }  
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit'){
              BRSPermitInfo = true; 
           } 
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit'){
              BRSCourtesyInfo = true; 
           }           
        if(Linelst[0].movement_type__c == 'Interstate Movement and Release' || Linelst[0].movement_type__c == 'Release' || Linelst[0].movement_type__c == 'Interstate Movement'){
              BRSPermitReleaseInfo = true; BRSPermitInfo = false; BRSNotificationInfo=false; BRSCourtesyInfo=false;
            
              if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit'){
              BRSCourtesyInfo = true; BRSPermitReleaseInfo = false;
           }
           } 
         if(Linelst[0].movement_type__c == 'Interstate Movement and Release')
             releaselocationinfo = 'At least 1 or more Origin Locations, 1 or more Destination Locations, and 1 or more Release Locations must be added.';
         if(Linelst[0].movement_type__c == 'Interstate Movement')
             releaselocationinfo = 'At least 1 or more Origin Locations and 1 or more Destination Locations must be added.';
         if(Linelst[0].movement_type__c == 'Import')
             releaselocationinfo = '1 Origin Locations and 1 Destination Locations must be added.';            
         if(Linelst[0].movement_type__c == 'Release')
             releaselocationinfo = 'At least 1 or more Release Locations must be added.';
                 
       }
        
        List<Program_Line_Item_Pathway__c> linepthwaylst = [SELECT Id, Display_Applicant_Attachment_Button__c,
                                                            Display_Construct_Button__c, Display_Design_Protocol_SOP_Button__c,
                                                            Display_Location_Button__c, Display_Regulated_Articles_Button__c, 
                                                            Previously_approved_Constructs_SOPs__c
                                                            FROM Program_Line_Item_Pathway__c WHERE
                                                            Id = :strPathwayId];
        if(linepthwaylst.size()>0){
            for(Program_Line_Item_Pathway__c p : linepthwaylst){
                if(p.Display_Applicant_Attachment_Button__c == true){
                    aabutton = true;
                }
                if(p.Display_Construct_Button__c == true){
                    constructbutton = true;
                }
                if(p.Display_Design_Protocol_SOP_Button__c == true){
                    sopbutton = true;
                }
                if(p.Display_Location_Button__c == true){
                    locationbutton = true;
                }
                if(p.Display_Regulated_Articles_Button__c == true){
                    RAbutton = true;
                }
                if(p.Previously_approved_Constructs_SOPs__c == true){
                    prevReviewConst = true;
                }
            }
        }
        
        listRA=new List<Regulated_Article__c>(); // VV added
        list<string> RAIdList = new list<String>();
        sSelCat = '';
        list<RA_Scientific_Name_Junction__c> listRAPW = [Select Regulated_Article__c from RA_Scientific_Name_Junction__c where (Program_Pathway__c=:strPathwayId or Program_Pathway__r.Name =:strPathwayId ) and Status__c =: 'Active'];
                         for(RA_Scientific_Name_Junction__c r:listRAPW ){
                             RAIdList.add(r.Regulated_Article__c);
                         }
                         system.debug('!!! List of Regulated Article IDs'+RAIdList);
                         listRA=[Select Name,Category_Group_Ref__r.Name,Category_Group_Ref__c from Regulated_Article__c where Id in:RAIdList];
    }
    public pageReference EditLineItem(){
        return null;
    }
    public pageReference AddConstruct(){
        redirect = 'yes';
        pageReference pg = Page.portal_construct_edit;
        pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        /* The below parameter "redirect" is passed on to construct edit page and 
         from there to construct detail page to render a button on construct detail
         page - "return to Line item details" to get back to the CARPOL_LineItemPageForApplicant. 
         This particular button should render only when the user is landed from the page
         CARPOL_LineItemPageForApplicant */
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
    public pageReference AddSOP(){
        redirect = 'yes';
        pageReference pg = Page.portal_applicantAttachment_edit;
        pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('appId',AppId);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
    
    public pageReference AddingSOP(){
        redirect = 'yes';
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Notification'){
            recTypeName = 'BRS Notification SOP';
            recordTypeId = [SELECT ID FROM Recordtype WHERE Name=:'BRS Notification SOP'].ID;
            
            system.debug('<<<<<!!!!!record type id'+recordTypeId);
            //recordTypeId = '012350000000826';
        }
        
        if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit'){
            recTypeName = 'BRS Standard Permit SOP';
            recordTypeId = [SELECT ID FROM Recordtype WHERE Name=:recTypeName].ID;
            
            system.debug('<<<<<!!!!!record type id'+recordTypeId);
            //recordTypeId = '012350000000826';
        }
        
        pageReference pg = Page.portal_applicantAttachment_edit;
        pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('RecordType',recordTypeId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('appId',AppId);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
    
    
    public pageReference AddLocations(){
        redirect = 'yes';
        pageReference pg = Page.portal_locationrecordtype;
        pg.getParameters().put('LineId',LineId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
     
    public pageReference AddRegArt(){
        redirect = 'yes';
        pageReference pg = Page.portal_LinkRegArt_edit;
        pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
    public pageReference RetApp(){
        pageReference pg = Page.portal_application_detail;
        pg.getParameters().put('id',AppId);
        pg.setRedirect(true);
        return pg;
    }
    public pageReference Retlineitem(){
        pageReference pg = Page.carpol_uni_lineitem;
        pg.getParameters().put('appId',AppId);
        pg.getParameters().put('Id',LineItemId);
        pg.setRedirect(true);
        return pg;
    }    
    public pageReference ReviewedConstructs(){
        redirect = 'yes';
        pageReference pg = Page.CARPOL_BRSLinkConstruct;
        pg.getParameters().put('appid',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('redirect',redirect);
        //pg.getParameters().put('appId',AppId);
        pg.setRedirect(true);
        return pg;
    }
    public pageReference ReviewedSOPDesignProtocol(){
        redirect = 'yes';
        pageReference pg = Page.CARPOL_BRS_ReviewedDesignProtocol_SOP;
        pg.getParameters().put('appId',LineItemId);
        pg.getParameters().put('strPathwayId',strPathwayId);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
    public List<Construct__c> getConstruct(){
                return [SELECT ID,Name,Line_Item__c, Name__c, Construct_s__c, CreatedDate,Mode_of_Transformation__c,Status__c, Status_Graphical__c,Total_No_of_PhenoTypes__c,Total_No_of_GenoTypes__c  FROM Construct__c WHERE Line_Item__c =:LineItemId];
        }
    public List<Location__c> getLocation(){
                return [SELECT ID,Name,Line_Item__c, Description__c, RecordType.Name,Country__r.Name,Level_2_Region__r.name, Status__c, State__r.Name, Status_Graphical__c,CreatedDate  FROM Location__c  WHERE Line_Item__c =:LineItemId];
        }
    public List<Applicant_Attachments__c > getSOP(){
        return[SELECT ID,Name, Animal_Care_AC__c ,Attachment_Type__c , RecordType.Name, Document_Types__c,File_Name__c, Status__c ,Standard_Operating_Procedure__r.Name,Status_Graphical__c,Total_Files__c,(SELECT ID, Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Animal_Care_AC__c =:LineItemId];
    }
    public List<Link_Regulated_Articles__c> getRA(){
        return[SELECT Id,Name,Line_Item__c,Regulated_Article__c ,Common_Name__c, Cultivar_and_or_Breeding_Line__c ,CreatedDate  FROM Link_Regulated_Articles__c WHERE Line_Item__c=:LineItemId];
    }
    public List<Construct_Application_Junction__c> getRevCons(){
        return[SELECT Id,Name,Line_Item__c,Construct__c,Construct__r.Construct_s__c,Construct_Status__c,Status_Graphical__c  FROM Construct_Application_Junction__c WHERE Line_Item__c=:LineItemId and Design_Protocol_Record_SOP__c = NULL];
    }   
    public List<Construct_Application_Junction__c> getRevSOP(){
        return[SELECT Id,Name,Line_Item__c,Design_Protocol_Record_SOP__c,Design_Protocol_Record_SOP__r.File_Name__c,Design_Protocol_Record_Status__c,Design_Protocol_SOP_Graphical_Status__c FROM Construct_Application_Junction__c WHERE Line_Item__c=:LineItemId and Construct__c = NULL];
    } 
        /* The below method is added by Raghu to display the "Agree For Compliance Agreement" button when the LineItem is PPQ F+V Irradiated and the Authorization workflow task requires the applicant
           to agree for the compliance agreement.
        */
        
        // VV added below method to delete records
             public PageReference deleteRecord(){
                 string sobjkey = delrecid.substring(0,3);
                 string sobjname = sobjectkeys.get(sobjkey);
                  string  strqurey = 'select id from '+sobjname +' where id=:delrecid';
                  list<sobject> lst = database.query(strqurey);
                  delete lst;
                    
                    PageReference dirpage= new PageReference('/apex/CARPOL_LineItemPageForApplicant?LineId=' + LineItemId+'&strPathwayId='+strPathwayId);
                    dirpage.setRedirect(true);
                    return dirpage;
                }  
                // VV added          
                // Get Category Options
      public List<SelectOption> getCategory() { 
        set<String> setNames=new set<String>();        
        if(listRA!= null  && listRA.size()>0){
            List<SelectOption> options = new List<SelectOption>();
            
            if(listRA.size()>10){
                options.add(new SelectOption('','--All--'));
                
            }else{
                options.add(new SelectOption('','--All--')); 
            }
             map<string,string> catMap = new map<String,string>(); //Noharika 3/9
            for(Regulated_Article__c recRA: listRA){
                if(setNames.add(recRA.Category_Group_Ref__r.Name))
                {
                    //options.add(new SelectOption(recRA.Category_Group_Ref__c,recRA.Category_Group_Ref__r.Name)); //VV commented 3-3-16
                   // options.add(new SelectOption(recRA.Category_Group_Ref__r.Name,recRA.Category_Group_Ref__r.Name)); //VV 3-3-16
                   // mapCatName.put(recRA.Category_Group_Ref__c,recRA.Category_Group_Ref__r.Name);
                     catMap.put(recRA.Category_Group_Ref__r.Name,recRA.Category_Group_Ref__c); //Niharika 3/9
                    
                }
            }
            //added niharika 3/9 to sort categories map alphabetically
             List<String> CategoriesList = new List<String>();
            CategoriesList.addAll(catMap.keySet());
            CategoriesList.sort();
            
            for(String st: CategoriesList){
               options.add(new SelectOption(catMap.get(st),st)); 
                
            }
            
            
            //options.sort(); //VV 3-3-16
            system.debug('$%$% categ options = '+options);
            return options;
        }
        else
            return null;
    } 
}