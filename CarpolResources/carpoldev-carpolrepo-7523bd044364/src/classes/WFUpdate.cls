//7-12-16 VV created for PEQ requirement
//For the Auth that has a review record that was created 15 days ago, and SPRO has not updated
//then set the WF task to complete and add comments
global class WFUpdate implements Database.Batchable<sObject>
{
    global  database.querylocator start(database.batchablecontext bc)
    {       
        string query = 'select id, Authorization__c, createddate  from Reviewer__c';
        return database.getquerylocator(query);
    }
    global void execute (database.batchablecontext bc, list<Reviewer__c> revRec)
    {
       for (Reviewer__c  r: revRec)
         {
         if (system.today().day() - r.createddate.day() > 15 && r.status__c != 'Completed')
            {
              Workflow_Task__c WFtoUpd = [select id from Workflow_Task__c where Authorization__c = : r.Authorization__c AND status__c <> 'Complete'];
              WFtoUpd.status__c = 'Complete';
              WFtoUpd.comments__c = 'No response from the State Reviewer';
              update WFtoUpd;
            }
         }
    }
    global void finish(database.batchablecontext bc){}
}