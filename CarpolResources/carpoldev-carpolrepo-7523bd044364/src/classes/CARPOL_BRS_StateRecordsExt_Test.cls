@isTest(seealldata=true)
private class CARPOL_BRS_StateRecordsExt_Test {
      @IsTest static void CARPOL_BRS_StateRecordsExt_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          //String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();        
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
          String ACLocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();          
          String ACContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('State SPRO').getRecordTypeId();                    

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
         
          Contact objcontu = testData.newcontact();
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          //usershare.ContactId = objcontu.id;
          usershare.IsActive = true;
          insert usershare;              
          
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);   

          //Authorizations__c objauth = testData.newAuth(objapp.Id); 
          Authorizations__c objauth = new Authorizations__c();
          objauth.Application__c = objapp.id;
          objauth.RecordTypeID = ACauthRecordTypeId;
          objauth.Status__c = 'Submitted';
          objauth.Date_Issued__c = date.today();
          objauth.Applicant_Alternate_Email__c = 'test@test.com';
          objauth.UNI_Zip__c = '32092';
          objauth.UNI_Country__c = 'United States';
          objauth.UNI_County_Province__c = 'Duval';
          objauth.BRS_Proposed_Start_Date__c = date.today()+30;
          objauth.BRS_Proposed_End_Date__c = date.today()+40;
          objauth.CBI__c = 'CBI Text';
          objauth.Application_CBI__c = 'No';
          objauth.Applicant_Alternate_Email__c = 'email2@test.com';
          objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
          objauth.AC_Applicant_Email__c = 'email2@test.com';
          objauth.AC_Applicant_Fax__c = '(904) 123-2345';
          objauth.AC_Applicant_Phone__c = '(904) 123-2345';
          objauth.AC_Organization__c = 'ABC Corp';
          objauth.Means_of_Movement__c = 'Hand Carry';
          objauth.Biological_Material_present_in_Article__c = 'No';
          objauth.If_Yes_Please_Describe__c = 'Text';
          objauth.Applicant_Instructions__c = 'Make corrections';
          objauth.BRS_Number_of_Labels__c = 10;
          objauth.BRS_Purpose_of_Permit__c = 'Importation';
          objauth.Documents_Sent_to_Applicant__c = false;
          objauth.BRS_Create_State_Review_Records__c = false;
          objauth.Bio_Tech_Reviewer__c = usershare.id;
          insert objauth;          

          ac1.Authorization__c = objauth.id;
          ac1.status__c = 'Waiting on Customer';
          //ac1.RecordTypeId = ACLIRecordTypeId;
          update ac1;
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          lr.Level_1_Name__c = 'State';
          lr.State_Name__c = 'Test';
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
    
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLocRecordTypeId
             );
            insert objloc;  
            
          Contact objCont = new Contact();
          objCont.FirstName = 'Global Contact';
          objcont.LastName = 'LastName';
          objcont.Email = 'test@email.com';        
          objcont.AccountId = objacct.id;
          objcont.MailingStreet = 'Mailing Street';
          objcont.MailingCity = 'Mailing City';
          objcont.MailingState = 'Ohio';
          objcont.MailingCountry = 'United States';
          objcont.MailingPostalCode = '32092';   
          objcont.RecordTypeId = ACContRecordTypeId;
          objcont.Contact_checkbox__c = true;  
          objcont.State__c = lr.id;
          insert objcont;                
              
          Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
          wft.Status_Categories__c = 'Customer Feedback';
          update wft;
          Map<Id,Workflow_Task__c> mapId = new Map<Id,Workflow_Task__c>();
          mapId.put(wft.Id,wft);

          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_WaitingPage;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('ID',objauth.id);
          
          CARPOL_BRS_StateRecordsExt extclass = new CARPOL_BRS_StateRecordsExt(sc);
          //extclass.CreateStateRecords();
          extclass.createStateLetterAttachment();
          //extclass.sendNotification();
          extclass.outputpanel = '';
          system.assert(extclass != null);         
          Test.stopTest();     
      }
}