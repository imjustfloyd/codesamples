@isTest(seealldata=true)
private class CARPOL_GetFieldAPINamesTest {
    
          static testMethod void testCARPOL_GetFieldAPINames(){
            
            Domain__c d = new Domain__c();
            d.Name = 'AC'; 
            d.Active__c = true;
            insert d;
            
            Program_Line_Item_Pathway__c pl = new Program_Line_Item_Pathway__c();
            pl.Name = 'Animal Care';
            pl.Program__c = d.Id;
            insert pl;
            
            Program_Line_Item_Section__c ps = new Program_Line_Item_Section__c();
            ps.Name = 'Information';
            ps.Program_Line_Item_Pathway__c = pl.Id;
            ps.Section_Order__c = 1;
            ps.Destination_Object__c = 'Line Item';
            insert ps;
            
            Program_Line_Item_Field__c pi = new Program_Line_Item_Field__c();
            pi.Name = 'Test Field';
            pi.Program_Line_Item_Section__c = ps.Id;
            insert pi;
            system.assert(pi != null);
          }
          
}