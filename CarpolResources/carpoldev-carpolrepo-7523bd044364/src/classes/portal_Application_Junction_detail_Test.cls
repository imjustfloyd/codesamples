@isTest(seealldata=false)
private class portal_Application_Junction_detail_Test {
      @IsTest static void portal_Application_Junction_detail_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               

          Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();
          objcaj.Line_Item__c = objac.id;
          objcaj.Application__c = objapp.id;
          insert objcaj;
          Attachment objattach = testData.newAttachment(objcaj.id);
          
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.portal_Application_Junction_detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcaj);
              ApexPages.currentPage().getParameters().put('id',objcaj.id);                                                                                                   
              portal_Application_Junction_detail extclass = new portal_Application_Junction_detail(sc);
              extclass.getattach();  
              extclass.ReturnToLineitem();   
              System.assert(extclass != null);                      
          Test.stopTest();   
      }
}