global class ProcessWorkFlow implements Messaging.InboundEmailHandler {
 
 global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
        // declare the result variable
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        // declare a string variable to catch our custom debugging messages
        String myErr;
        result.success = true;
        
        try
        {          
            // extract the email body : either the htmlBody or the plainTextBody
            String msgObject = '';
            String messageBody = '';  
            String messageContent = '';     

            messageBody = email.plainTextBody;
            messageBody = messageBody.trim();

            //Extract just the XML portion of the message
            //messageBody = messageBody.substring(messageBody.indexOf('<?xml version="1.0"?>'),messageBody.indexOf('</emailData>')+12);

                        
            msgObject = readXMLelement(messageBody,'objectName');
            msgObject = msgObject.replaceAll('\\D','');        
            
            messageContent = readXMLelement(messageBody,'message');
            
            if(messageContent.length() > 155)
            {
                messageContent = messageContent.substring(0,155);
            }
                                             
           
          MessageServices__c message = new MessageServices__c(Object__c = msgObject, Message_Content__c=messageContent, Original_Message__c =email.plainTextBody);
          insert message;
            
        }
        
        catch(exception e)
        {     
            Error_Logging__c log = new Error_Logging__c();           
            log.trace__c = e.getTypeName() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getLineNumber();
            insert log;
        }

        return result;
    }

    public static String readXMLelement(String xml, String element)
    {
        String elementValue = 'NOT FOUND'; 
        
        try
        {
            Xmlstreamreader reader = new Xmlstreamreader(xml);
            while (reader.hasNext()) 
            {
                if (reader.getEventType() == XmlTag.START_ELEMENT && reader.getLocalName() == element)
                {
                    System.debug('Found');
                    reader.next();
                    //elementValue = getDecodedString(reader);
                }         
                reader.next();
            }
            return elementValue;
        }
        catch(exception e)
        {
            Error_Logging__c log = new Error_Logging__c();
            log.trace__c = e.getTypeName() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + xml + '\n' + e.getLineNumber();
            insert log;
                        
            string err;
            err = e.getTypeName() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + xml + '\n' + e.getLineNumber();
            return err;
        }
    }
    }