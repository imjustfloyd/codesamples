public class Portal_UploadAttachment {
    public ID attId {get;set;}
    public string returl {get;set;}
    public string parentname {get;set;}
 

    public Attachment attachment {
      get {
          if (attachment == null)
            attachment = new Attachment();
          return attachment; }
      set;}
//---------------------------------------------------------------------------------------------------------------------//
      
    public Portal_UploadAttachment() { 
   
        
    returl = ApexPages.currentPage().getParameters().get('retPage');      
    if(returl == 'portal_construct_detail'){
        attId = ApexPages.currentPage().getParameters().get('Id');
         parentname = (String) Database.query( 'select Construct_s__c from ' + attId.getSobjectType() + ' where id = :attId')[0].get('Construct_s__c');
        
    }else if(returl == 'portal_applicantattachment_detail'){ 
        attId = ApexPages.currentPage().getParameters().get('Id');
        parentname = (String) Database.query( 'select File_Name__c from ' + attId.getSobjectType() + ' where id = :attId')[0].get('File_Name__c');        
   
    }else if(returl == 'portal_phenotype_detail'){
        attId = ApexPages.currentPage().getParameters().get('phenoid');
        parentname = (String) Database.query( 'select Phenotypic_Category__c from ' + attId.getSobjectType() + ' where id = :attId')[0].get('Phenotypic_Category__c');          
  
    }else if(returl == 'portal_genotype_detail'){
        attId = ApexPages.currentPage().getParameters().get('genid');
        parentname = (String) Database.query( 'select Construct_Component__c from ' + attId.getSobjectType() + ' where id = :attId')[0].get('Construct_Component__c');          
   
    }else if(returl == 'portal_application_junction_detail'){ 
      //  attId = ApexPages.currentPage().getParameters().get('Id');
     //   parentname = (String) Database.query( 'select File_Name__c from ' + attId.getSobjectType() + ' where id = :attId')[0].get('File_Name__c');          
        
    }
      if(parentname != null)
         attachment.name = parentname;
        
    }

//---------------------------------------------------------------------------------------------------------------------//
  public PageReference upload() {

       attId = ApexPages.currentPage().getParameters().get('phenoid');
    if(attid == null )
       attId = ApexPages.currentPage().getParameters().get('genid');
    if(attId == null )
       attId = ApexPages.currentPage().getParameters().get('Id');
    
    returl = ApexPages.currentPage().getParameters().get('retPage');
    
    system.debug('***attId**'+attId);
    system.debug('***returl**'+returl);  
    
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = attId;
   // attachment.name = attId;
   
    system.debug('***attachment.OwnerId**'+attachment.OwnerId);
    system.debug('***attachment.ParentId**'+attachment.ParentId);
    
    
    
   try {
      insert attachment;
    } catch (DMLException e) {
      //  if( attachment.body.size() < 0 ){ ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'Please upload a file')); return null; }
    //  system.debug('attachment.body.size()@@@@'+attachment.body.size());
      if( attachment.body == null ){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please upload your file'));
              return ApexPages.CurrentPage(); 
      }
      // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please upload a file'));
     //  return null;
    } finally {
      attachment = new Attachment(); 
    }

  // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    
    PageReference pg;
    if(returl == 'portal_construct_detail'){
        pg = Page.portal_construct_detail;
        pg.getParameters().put('id',attId);
        pg.setRedirect(true);
    }
    if(returl == 'portal_applicantattachment_detail'){
       pg = Page.portal_applicantattachment_detail;
       pg.getParameters().put('id',attId);
       pg.setRedirect(true);    
    }
     if(returl == 'portal_phenotype_detail'){
       pg = Page.portal_phenotype_detail ;
       pg.getParameters().put('id',attId);
       pg.setRedirect(true);    
    }
     if(returl == 'portal_genotype_detail'){
       pg = Page.portal_genotype_detail ;
       pg.getParameters().put('id',attId);
       pg.setRedirect(true);    
    }
    
    if(returl == 'portal_application_junction_detail'){
       pg = Page.portal_application_junction_detail ;
       pg.getParameters().put('id',attId);
       pg.setRedirect(true);    
    }
    
    return pg;  
  }
  
  public PageReference  cancel(){
   attId = ApexPages.currentPage().getParameters().get('phenoid');
    if(attid == null )
       attId = ApexPages.currentPage().getParameters().get('genid');
    if(attId == null )
       attId = ApexPages.currentPage().getParameters().get('Id');
    returl = ApexPages.currentPage().getParameters().get('retPage');
    system.debug('inside cancel ###'+returl);
    PageReference pg;
    if(returl == 'portal_construct_detail'){
         pg = Page.portal_construct_detail;
         pg.getParameters().put('id',attId);
         pg.setRedirect(true);
    }
    if(returl == 'portal_applicantattachment_detail'){
           pg = Page.portal_applicantattachment_detail;
           pg.getParameters().put('id',attId);
           pg.setRedirect(true);    
    }
     if(returl == 'portal_genotype_detail'){
           pg = Page.portal_genotype_detail ;
           pg.getParameters().put('id',attId);
           pg.setRedirect(true);   
       }
    if(returl == 'portal_phenotype_detail'){
           pg = Page.portal_phenotype_detail ;
           pg.getParameters().put('id',attId);
           pg.setRedirect(true);    
    }
    
     if(returl == 'portal_application_junction_detail'){
        pg = Page.portal_application_junction_detail ;
        pg.getParameters().put('id',attId);
        pg.setRedirect(true);    
    }
    
    return pg; 
  }

}