public  with sharing class Portal_Contact_Edit_Controller {
    public Id contactID{get;set;}
    public Contact cont{get;set;}
   
    public Contact obj {get;set;} 
    public ApexPages.StandardController controller{get;set;}
    
    public Portal_Contact_Edit_Controller(ApexPages.StandardController controller)
    {
        contactID=ApexPages.currentPage().getParameters().get('id');
        if(contactID!=null){
        obj=(Contact)controller.getRecord();
        cont = [Select FirstName, LastName, AccountId, Contact_checkbox__c, State__c,
               Recordtypeid, Phone, MobilePhone, Email, MailingCountryCode, MailingStreet, MailingCity,
                MailingStateCode, MailingPostalCode, OtherCountryCode, OtherStreet, OtherCity,
                OtherStateCode, OtherPostalCode, Name, MailingAddress, OtherAddress
                from Contact where ID=:contactID];
        controller = controller;
        }
        else
        {
            cont = new Contact();
        }
    }
    
    
    public PageReference Save()
    {   
    if(contactID!=null){
             contactID = ApexPages.currentPage().getParameters().get('id');
                obj.id = contactID;
                obj.Phone = cont.Phone;
                //obj.Review_Complete__c = Reviewer.Review_Complete__c;
                //obj.Requirement_Desc__c = Reviewer.Requirement_Desc__c;
                //obj.No_Requirements__c = Reviewer.No_Requirements__c;
                //obj.State_has_comments__c = Reviewer.State_has_comments__c;
                update obj;
                PageReference pg= new PageReference ('/'+obj.Id);
                pg.setRedirect(true);
        return pg;
                }else{
                upsert cont;
                PageReference pg= new PageReference ('/'+cont.Id);
                pg.setRedirect(true);
        return pg;
                }
        
       // system.debug('<<<<<!!!!!!! page '+pg);
        
        
    }
    public PageReference getEditPage()
    {
        /*PageReference pg= new PageReference ('/'+ContactID + '/e?retURL=/' + ContactId);
        pg.setRedirect(true);
        return pg;*/
        
        //String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        //PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Contacts_Edit?id='+contactID);
        PageReference dirpage= new PageReference('/apex/Portal_Contacts_Edit?id='+contactID);
        dirpage.setRedirect(true);
        return dirpage;
    }
    
   

}