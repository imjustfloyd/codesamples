global class CARPOL_BRS_VoidLabelsScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        CARPOL_BRS_VoidLabelsBatch BRSVoidlabels = new CARPOL_BRS_VoidLabelsBatch();
        Database.executeBatch(BRSVoidlabels); 
    }
}