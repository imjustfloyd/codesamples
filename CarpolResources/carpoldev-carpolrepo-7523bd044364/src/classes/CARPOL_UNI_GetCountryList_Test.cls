@isTest(seealldata=false)
private class CARPOL_UNI_GetCountryList_Test { 
      @IsTest static void CARPOL_UNI_GetCountryList_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Country__c objcountry = testData.newcountryus();
          
          
          Test.startTest(); 

              CARPOL_UNI_GetCountryList extclass = new CARPOL_UNI_GetCountryList();
              extclass.getCountryList(new Map<String, String>());   

              System.assert(extclass != null);              
          Test.stopTest();   
      }
}