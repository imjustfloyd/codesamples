public with sharing class CARPOL_BRSLinkConstruct {

    public PageReference cancel() {
        PageReference pagref = new PageReference('/'+applid);
        return pagref ;
    }

    public string applid {get;set;}
    public Construct_Application_Junction__c objconstjun {get;set;}
    public CARPOL_BRSLinkConstruct()
    {
        applid =  ApexPages.CurrentPage().getParameters().get('appid');
         objconstjun = new Construct_Application_Junction__c();
         objconstjun.Application__c = applid;
    }
    public PageReference save() {
        insert objconstjun ;
        PageReference pagref = new PageReference('/'+applid);
        return pagref ;
        }
}