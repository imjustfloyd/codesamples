/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRS_Interstate_Mvmt_Notif_test 
@CreateDate: 9/24/15
@Description: Test class for CARPOL_BRS_Interstate_Movement_Notif
@Version: 1.0
@reference: CARPOL_BRS_Interstate_Movement_Notif(apex class)
*/

@isTest
public class CARPOL_BRS_Interstate_Mvmt_Notif_test{
    public static testmethod void Test(){
    
        Apexpages.StandardController stdController = new Apexpages.StandardController(new Application__c()); 
 
        CARPOL_BRS_Interstate_Movement_Notif controller = new CARPOL_BRS_Interstate_Movement_Notif(StdController);
        system.assert(controller != null);
    }


}