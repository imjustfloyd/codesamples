public class CARPOL_UNI_LineAttachment {

 // These variables store Trigger.oldMap and Trigger.newMap
  Map<Id, Attachment> oldAtt;
  Map<Id, Attachment> newAtt;
  Set<ID> setLineIds = new Set<ID>();

  string attrtid = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();

// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_UNI_LineAttachment(
    Map<Id, Attachment> oldTriggerAtts, 
    Map<Id, Attachment> newTriggerAtts) {
      oldAtt = oldTriggerAtts;
      newAtt = newTriggerAtts;
      system.debug('#######################      ' + newTriggerAtts + '     #############################');
  }
 
 
 public void LineAttachment()  {

    if(Trigger.isDelete)
    {
          for(Attachment att : oldAtt.values())
             {
                 
              List<Applicant_Attachments__c> app=[select Id,RecordTypeId, Animal_Care_AC__c,Status__c, Total_Files__c from Applicant_Attachments__c where Id=:att.ParentId];

           if (app.size()>0) {
            If(app[0].Animal_Care_AC__c != null && app[0].RecordTypeId == attrtid){
                    AC__c getLine = [select ID from AC__c where id=:app[0].Animal_Care_AC__c];
                    getLine.Status__c = 'Saved';
                    getLine.Contains_Applicant_Attachments__c=False;
                    update getLine;
                    system.debug('#######################   Updated after delete of attachment  ' + app[0].Animal_Care_AC__c + '     #############################');
                      }
          }    
                }
          }  
  
   
   else{
       for(Attachment att : newAtt.values())
         {
     
         List<Applicant_Attachments__c> app=[select Id ,RecordTypeId, Animal_Care_AC__c,Status__c, Total_Files__c from Applicant_Attachments__c where Id=:att.ParentId];
    
           if (app.size()>0) {
            If(app[0].Animal_Care_AC__c != null && app[0].RecordTypeId == attrtid){
                if(Trigger.isInsert || Trigger.isUndelete)
                    {
                        AC__c getLine = [select ID from AC__c where id=:app[0].Animal_Care_AC__c];
                        getLine.Status__c = 'Saved';
                        getLine.Contains_Applicant_Attachments__c=True;
                        update getLine;
                         system.debug('#######################   Updated after insert of attachment  ' + app[0].Animal_Care_AC__c + '     #############################');
                        
                    }
              }  
          

            }
        }
    }
     }
         }