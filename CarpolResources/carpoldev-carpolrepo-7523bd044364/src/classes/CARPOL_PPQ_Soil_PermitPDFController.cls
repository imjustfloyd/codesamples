public class CARPOL_PPQ_Soil_PermitPDFController {

    public ID appId {get; set;}
    public ID authId {get;set;}
    public Authorizations__c app {get; set;}
    List<AC__c> aclst {get; set;}
    public AC__c lineItem{get;set;}
    public String htmlText {get;set;}
    public String tdSpan = 'font-size:9.0pt;color:#221F1F';
    public String tdStyle = 'font-size:13px;border-right: thin solid black;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    public String tdStyleEnd ='font-size:13px;border-top: thin solid black;border-bottom: thin solid black;text-align:center;border-collapse: collapse;';
    Integer counter =0;
    public Program_Line_Item_Pathway__c pdfLnItmPway{get;set;}
    String strPathway;
    public String defaultPart {get;set;}
    Private List<TableCol> tblColumns;
    public string portsSelected {get;set;} // VV 4-20-2016


    public CARPOL_PPQ_Soil_PermitPDFController(ApexPages.StandardController controller){
        app = (Authorizations__c)controller.getRecord();
        authId = ApexPages.currentPage().getParameters().get('id');
        app = [Select Id, Application__c, Final_Decision_Matrix_Id__c, Program_Pathway__c from Authorizations__c where Id=: authId];
        appId = app.Application__c;
        // VV 4-20-2016 starting
        portsSelected = '';
        list<Authorization_Junction__c> lstAuthjunct = new list<Authorization_Junction__c>();
        for(Authorization_Junction__c authjunct: [select id,Name,Port__c,Port__r.Name,Authorization__c from Authorization_Junction__c where Authorization__c=:authId  and Port__c!=null])
        {
            lstAuthjunct.add(authjunct);
        }
        if(lstAuthjunct.size()>0){
            for(integer i=0;i<lstAuthjunct.size();i++){
                if(i<lstAuthjunct.size()-1){
                    portsSelected += lstAuthjunct[i].Port__r.Name+'; ';
                }
                else{
                    portsSelected += lstAuthjunct[i].Port__r.Name;
                }
            }
        }
        system.debug('----portsSelected---'+portsSelected);
        // VV 4-20-2016 ending
        //strPathway = ApexPages.currentPage().getParameters().get('strPathway');
        System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
        htmlText = '<p style="color:red;">Sample HTML Input</p>';
        getACRecords();
        defaultPart = 'Seed';
        getPDFFields();


    }
    public CARPOL_PPQ_Soil_PermitPDFController()  //VV added
    {}


    public List<AC__c> getACRecords(){
        if(app.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ',Hand_Carrier_Last_Name__r.Name,Country_Of_Origin__r.Name,Country__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name, Component__r.Name ,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Regulated_Article__r.Plant_Part__r.Name from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            
            lineItem = aclst[0]; 
        }
        
        return aclst;
    }
    
     public void getPDFFields()
    {

         //Regulations_Association_Matrix__c raMatrix = [Select ID, Name, Program_Line_Item_Pathway__c from Regulations_Association_Matrix__c where Id =:app.Final_Decision_Matrix_Id__c];
         pdfLnItmPway = [select id,Name, Column_1_API_Name__c, Column_2_API_Name__c, Column_3_API_Name__c, Column_4_API_Name__c, Column_5_API_Name__c, Column_6_API_Name__c, 
                    Column_1_Display_Name__c, Column_2_Display_Name__c, Column_3_Display_Name__c, Column_4_Display_Name__c, Column_5_Display_Name__c, Column_6_Display_Name__c ,   
                    PDF_Heading_1__c, PDF_Heading_2__c, PDF_Permit_Conditions_Static__c, Table_Section_Heading__c, Permit_PDF_Sub_Heading_1__c, Permit_PDF_Heading_1__c, Permit_PDF_Heading_2__c, 
                    PDF_Forwarded_Completed_App_Address__c from Program_Line_Item_Pathway__c where Id =: app.Program_Pathway__c];

         tblColumns = new List<TableCol>();           

        if(pdfLnItmPway.Column_1_API_Name__c!=NULL)        
            tblColumns.add(new TableCol(pdfLnItmPway.Column_1_Display_Name__c, pdfLnItmPway.Column_1_API_Name__c)); 
        if(pdfLnItmPway.Column_2_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_2_Display_Name__c, pdfLnItmPway.Column_2_API_Name__c)); 
        if(pdfLnItmPway.Column_3_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_3_Display_Name__c, pdfLnItmPway.Column_3_API_Name__c)); 
        if(pdfLnItmPway.Column_4_API_Name__c!=NULL) 
            tblColumns.add(new TableCol(pdfLnItmPway.Column_4_Display_Name__c, pdfLnItmPway.Column_4_API_Name__c)); 
        if(pdfLnItmPway.Column_5_API_Name__c!=NULL)
            tblColumns.add(new TableCol(pdfLnItmPway.Column_5_Display_Name__c, pdfLnItmPway.Column_5_API_Name__c)); 
        if(pdfLnItmPway.Column_6_API_Name__c!=NULL)
             tblColumns.add(new TableCol(pdfLnItmPway.Column_6_Display_Name__c, pdfLnItmPway.Column_6_API_Name__c)); 
                
    }
    
    public String getTable()
    {
        htmlText ='';
        //List<CARPOL_UNI_ApplicationPDF_Table__c> tableSettings = CARPOL_UNI_ApplicationPDF_Table__c.getall().values();
        //tableSettings.sort();
        

        htmlText += '<tr>';
            //loop over column heads from tablecol object
            for(TableCol tbl: tblColumns){
                
                
                    htmlText += '<th align="center"><span style="' + tdSpan + '">';
                    htmlText +=  tbl.Heading;
                    htmlText += '</span></th>';
                    counter++;
                
            }
            
            htmlText += '</tr>';
        
        for(AC__c a: aclst)
        {
            counter =0;
            htmlText += '<tr>';
            for(TableCol tbl: tblColumns){
                htmlText += '<td align="center"><span style="' + tdSpan + '">';
                htmlText +=  (getFieldValue(a,tbl.APIName)!=null) ? getFieldValue(a,tbl.APIName): '';
                
                htmlText += '</span></td>';
                counter++;
            }
            htmlText += '</tr>';
            
            
        }
        
        return htmlText;
    }
    
    public static Object getFieldValue(SObject o,String field){
 
     if(o == null){
        return null;
     }
     if(field.contains('.')){
    String nextField = field.substringAfter('.');
    String relation = field.substringBefore('.');
    return CARPOL_UNI_ApplicationPDF.getFieldValue((SObject)o.getSObject(relation),nextField);
     }else{
    return o.get(field);
     }
    

    }
    
    public List<Authorization_Junction__c> getConditions(){
        List<Authorization_Junction__c> scj= new List<Authorization_Junction__c>();
         try{
    

    
    scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Regulation__r.Type__c='Import Requirements' and Regulation__r.Sub_Type__c='Permit Requirement' and Authorization__c = :authId ORDER BY Order_Number__c ASC];
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
  /*  scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        
        
        if(scj.size()>0){
            for (Authorization_Junction__c objSCJ: scj)
            {
                if(objSCJ.Regulation__r.Type__c=='Import Requirements' && objSCJ.Is_Active__c=='Yes' ){scjA.add(objSCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(objSCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(objSCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
            
            }
        } */
        
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }
     return scj;
            
        
    }

    public Class TableCol
    {
        public String Heading;
        public String APIName;
        public String sortOrder;

        public TableCol(String n, String a)
        {
            Heading = n; APIName = a;
        }
    }
    
}