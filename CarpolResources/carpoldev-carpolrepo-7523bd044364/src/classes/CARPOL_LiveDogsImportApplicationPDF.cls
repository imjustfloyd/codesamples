public with sharing class CARPOL_LiveDogsImportApplicationPDF {

    public List<AC__c> allLineItems { get; set; }
    public Integer liveDogCount { get; set; }
    public Application__c application = new Application__c();
    
    public CARPOL_LiveDogsImportApplicationPDF(ApexPages.StandardController controller) {
        this.application = (Application__c)controller.getRecord();
        allLineItems = new List<AC__c>();
        allLineItems = [SELECT ID, Name, Purpose_of_the_Importation__c, Sex__c, Breed_Name__c, Age_in_months__c, Color__c, Other_identifying_information__c FROM AC__c WHERE Application_Number__c = :application.ID ORDER BY CreatedDate];

    }
}