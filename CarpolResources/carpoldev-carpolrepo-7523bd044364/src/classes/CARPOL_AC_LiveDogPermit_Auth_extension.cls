public with sharing class CARPOL_AC_LiveDogPermit_Auth_extension {

    public List<AC__c> allLineItems { get; set; }
    public List<AC__c> oneLineItem { get; set; }
    public List<Workflow_Task__c> task3 {get;set;}
    
    public Integer liveDogCount { get; set; }
    public Authorizations__c auth = new Authorizations__c();
    
    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public List<Authorization_Junction__c> scjC {get;set;}
    
    public CARPOL_AC_LiveDogPermit_Auth_extension(ApexPages.StandardController controller) {
        this.auth = (Authorizations__c)controller.getRecord();
        if(auth.Id != null)
        {
            Integer result = getLineItems();
            if(result == 1)
            {
                getRegulation();
            }
        }
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Authorization Id found. Details cannot be filled.'));}
    }
    
    public Integer getLineItems()
    {
        try
        {
            auth = [SELECT Id, Name, Application__c, Status__c, Authorization_Type__c, Expiration_Date__c , Response_Type__c, Thumbprint__c FROM Authorizations__c WHERE Id = :auth.Id];
            allLineItems = new List<AC__c>();
            allLineItems = [SELECT ID, Name, Age_yr_months__c,USDA_License_Number_Format__c,USDA_Registration_Number_Format__c,Intended_Use__r.Name, Purpose_of_the_Importation__c, Sex__c, Breed_Name__c, Age_in_months__c, Color__c, Microchip_Number__c, Tattoo_Number__c, Other_identifying_information__c FROM AC__c WHERE Authorization__c = :auth.ID ORDER BY CreatedDate];
            if (allLineItems.size() == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No line item(s) found. Details cannot be filled.'));
                return -1;
            }
            
            oneLineItem = [SELECT ID, 
            Name, 
            Purpose_of_the_Importation__c, 
            Sex__c, 
            Breed_Name__c, 
            Age_in_months__c, 
            Color__c,
            Other_identifying_information__c,
            Permit_Expiration_Date__c,
            Exporter_Last_Name__c,
            Exporter_First_Name__c,
            Exporter_Last_Name__r.Name,
            Exporter_Email__c,
            Exporter_Fax__c,
            Exporter_Mailing_City__c,
            Exporter_Mailing_CountryLU__r.Name,
            Exporter_Mailing_State_ProvinceLU__r.Name,
            Exporter_Mailing_Street__c,
            Exporter_Mailing_Zip_Postal_Code__c,
            Exporter_Phone__c,
            Importer_Email__c,
            Importer_Fax__c,
            Importer_First_Name__c,
            Importer_Last_Name__c,
            Importer_Last_Name__r.Name,
            Importer_USDA_License_Number__c,
            Importer_USDA_License_Expiration_Date__c,
            Importer_Mailing_City__c,
            Importer_Mailing_CountryLU__r.Name,
            Importer_Mailing_State_ProvinceLU__r.Name,
            Importer_Mailing_Street__c,
            Importer_Mailing_ZIP_code__c,
            Importer_Phone__c,
            Importer_USDA_Registration_Number__c,
            Importer_USDA_Registration_Exp_Date__c,
            Port_of_Embarkation__c,
            Port_of_Embarkation__r.Name,
            Port_of_Entry__c,
            Port_of_Entry__r.Name,
            Transporter_Type__c,
            Air_Transporter_Flight_Number__c,
            Transporter_Name__c,
            Departure_Time__c,
            Arrival_Time__c,
            Age_yr_months__c,
            USDA_License_Number_Format__c,
            USDA_Registration_Number_Format__c, 
            USDA_License_Number__c, 
            USDA_License_Expiration_Date__c,
            USDA_Registration_Number__c,
            USDA_Expiration_Date__c,
            Proposed_date_of_arrival__c,
            DeliveryRecipient_USDA_License_Number__c,
            DeliveryRec_USDA_License_Expiration_Date__c,
            DeliveryRecipient_USDA_Registration_Num__c,
            DeliveryRec_USDA_Regist_Expiration_Date__c,
            DeliveryRecipient_Email__c,
            DeliveryRecipient_Fax__c,
            DeliveryRecipient_First_Name__c,
            DeliveryRecipient_Last_Name__c,
            DeliveryRecipient_Last_Name__r.Name,
            DeliveryRecipient_Mailing_City__c,
            DeliveryRecipient_Mailing_CountryLU__r.Name,
            DeliveryRecipient_Mailing_Street__c,
            DeliveryRecipient_Phone__c,
            Delivery_Recipient_State_ProvinceLU__r.Name,
            DeliveryRecipient_Mailing_Zip__c
            FROM AC__c WHERE Authorization__c = :auth.ID ORDER BY CreatedDate DESC Limit 1];
            
            if (oneLineItem.size() == 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No line item found. Details cannot be filled.'));
                return -1;
            }
            
            task3 = [SELECT Id, Name, Owner_User_Name__c, Authorization__c FROM Workflow_Task__c WHERE Owner_Approver_Is_Signatory__c = True AND Authorization__c =: auth.Id Limit 1];
            if (task3.size() == 0){
                task3.add(new Workflow_Task__c());
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No process authorization taskfound. Details cannot be filled.'));
                return -1;
            }
            return 1;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please contact your System Administrator. Exception Occured : ' + e));
            return -1;
        }
    }
    
    public void getRegulation()
    {
    try{
    scj = new List<Authorization_Junction__c>([Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :auth.Id ORDER BY Order_Number__c ASC]);
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
    scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        scjC = new List<Authorization_Junction__c>();
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' && SCJ.Is_Active__c=='Yes' ){scjA.add(SCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(SCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Instruction for CBP Officers'){scjC.add(SCJ);}
                System.Debug('<<<<<<< Total IO : ' + scjc.size() + ' >>>>>>>');
            }
        }
        
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }

    }

public void saveAttachment(){
    Application__c app = [SELECT Applicant_Email__c, Name, ID, Applicant_Name__r.Name from Application__c where ID = :auth.Application__c];
       if(auth.Id != null && auth.Response_Type__c == 'Permit'){
            System.debug('<<<<<<<<<<<<<<<<< ID ' + auth.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
            PageReference pdf = Page.CARPOL_AC_LiveDogPermit_Auth;
            pdf.getParameters().put('id', auth.Id);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                }
            att.Name = 'Permit.pdf';
            att.ParentId = Auth.Id;
            att.body = fileBody;
            insert att;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Permit Successfully Saved'));
            //Sending Permit Email Code
            
            /*List<Attachment> pg = [SELECT ID, Name, Body FROM Attachment WHERE Name = 'Permit.pdf' AND ParentID = : auth.ID ORDER BY CreatedDate DESC LIMIT 1];
            if(pg.size() > 0){
                Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();

                // Strings to hold the email addresses to which you are sending the email.
                String[] toAddressesAppEmail = new String[] {app.Applicant_Email__c}; 
                //String[] ccAddressesAppEmail = new String[] {'zabseitov@phaseonecg.com'};
    
                // Assign the addresses for the To and CC lists to the mail object.
                appEmail.setToAddresses(toAddressesAppEmail);
                //appEmail.setCcAddresses(ccAddressesAppEmail);
                        
                // Specify the address used when the recipients reply to the email. 
                appEmail.setReplyTo('support@efile.com');
                        
                // Specify the name used as the display name.
                appEmail.setSenderDisplayName('eFile Support');
                        
                // Specify the subject line for your email.
                appEmail.setSubject('USDA APHIS eFile - Permit Response for Application : ' + app.Name);
                        
                // Set to True if you want to BCC yourself on the email.
                appEmail.setBccSender(false);
                        
                // Optionally append the salesforce.com email signature to the email.
                // The email address of the user executing the Apex Code will be used.
                appEmail.setUseSignature(false);
                        
                String appEmailBody = 'Dear ' + app.Applicant_Name__r.Name;
                appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app.Name + '.';
                appEmailBody += '<br/><br/><br/>Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online<br/><br/>';
                appEmailBody += '<br/>You may check the status of this application online at <a href= "https://aphis--carpoldev.cs32.my.salesforce.com">https://aphis--carpoldev.cs32.my.salesforce.com</a> .<br/><br/>';
                appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
                appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/><br/><br/>APHIS Animal Care<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
    
                appEmail.setHtmlBody(appEmailBody);
                        
                Blob body = pg[0].Body;
                // Create the email attachment
                Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
                emailAttachment.setFileName('Permit.pdf');
                emailAttachment.setBody(body);
                appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
 
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The Permit Successfully Saved and Sent to ' + app.Applicant_Name__r.Name));
                
                } */      
        }
        /*else if (auth.Id != null && auth.Status__c == 'Approved' && auth.Authorization_Type__c == 'Letter of Denial'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit is not issued. Please create a Letter of Denial'));
        }
        else if (auth.Id != null && auth.Status__c == 'Approved' && auth.Authorization_Type__c == 'Letter of No Jurisdiction'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit is not issued. Please create a Letter of No Jurisdiction'));
        }
        else if (auth.Id != null && auth.Status__c == 'Approved' && auth.Authorization_Type__c == 'Letter of No Permit Required'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit is not issued. Please create a Letter of No Permit Required'));
        }*/
    }
}