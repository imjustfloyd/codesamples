@isTest
private class MasterTempQJunctionTrigger_test {
   
	private static testMethod void test() {
	     List<EFL_Inspection_Questions_Template__c> templatelist = new  List<EFL_Inspection_Questions_Template__c>();
	     List<EFL_Inspection_Questions__c> questionlist = new List<EFL_Inspection_Questions__c>();
	     List<EFL_INS_Template_Question_Junction__c> junctionlist = new List<EFL_INS_Template_Question_Junction__c>();
         Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];
         system.runAs(u){
         EFL_Inspection_Questions_Template__c IQT = new  EFL_Inspection_Questions_Template__c();
         IQT.Name = 'Live Plants';
         IQT.Description__c = 'Testing';
         insert IQT;
       
        
         EFL_Inspection_Questions__c IQ = new EFL_Inspection_Questions__c();
         IQ.question__c = 'What is the date today';
         insert IQ;
       
        
       
         EFL_Inspection_Questions__c IQQ = [SELECT ID,Name FROM EFL_Inspection_Questions__c WHERE ID=:IQ.id];
         EFL_Inspection_Questions_Template__c IQTQ = [SELECT ID,Name FROM EFL_Inspection_Questions_Template__c WHERE ID=:IQT.Id];
         EFL_INS_Template_Question_Junction__c junction1 = new EFL_INS_Template_Question_Junction__c();
         junction1.Inspection_Template_Questions__c = IQQ.ID;
         junction1.EFL_Inspection_Questions_Template__c = IQTQ.Id;
         insert junction1;
        
    }
	}

}