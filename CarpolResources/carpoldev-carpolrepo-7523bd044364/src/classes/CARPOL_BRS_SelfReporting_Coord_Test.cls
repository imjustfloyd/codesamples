@isTest(seealldata=false)
private class CARPOL_BRS_SelfReporting_Coord_Test {
      @IsTest static void CARPOL_BRS_SelfReporting_Coord_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Authorizations__c objauth = TestData.newauth(objapp.id);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Self_Reporting__c sr= new Self_Reporting__c();
          sr.Line_Events__c = objcaj.id;
          sr.Monitoring_Period_Start__c = date.today();
          sr.Monitoring_Period_End__c = date.today() + 60;
          sr.Observation_Date__c = date.today() + 61;
          sr.Number_of_Volunteers__c = 5;
          sr.Action_Taken__c = 'Test description';
          sr.Latitude_1__c = 'test';
          sr.Latitude_2__c = 'test';
          sr.Latitude_3__c = 'test';
          sr.Latitude_4__c = 'test';
          sr.Latitude_5__c = 'test';
          sr.Latitude_6__c = 'test';
          sr.Longitude_1__c = 'test';           
          insert sr;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
          PageReference pageRef = Page.portal_SelfReport_edit;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
          ApexPages.currentPage().getParameters().put('id',sr.id);
          ApexPages.currentPage().getParameters().put('AuthoId',objauth.id);
          RecordType aRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Hand Carry Information Form' LIMIT 1];
          RecordType cRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Planting Report' LIMIT 1];          
          RecordType dRT = [Select Id,Name, DeveloperName From RecordType Where Name = 'Volunteer Monitoring Report' LIMIT 1];                    
          CARPOL_BRS_SelfReporting_Coordinates extclassempty = new CARPOL_BRS_SelfReporting_Coordinates();        
          CARPOL_BRS_SelfReporting_Coordinates extclass = new CARPOL_BRS_SelfReporting_Coordinates(sc);                              
          extclass.saveloc();
          extclass.addgps();
          extclass.gpsno = '1';
          extclass.removegps();
          extclass.gpsno = '2';
          extclass.removegps();          
          extclass.gpsno = '3';
          extclass.removegps();                    
          extclass.gpsno = '4';
          extclass.removegps();                    
          extclass.gpsno = '5';
          extclass.removegps();                    
          extclass.gpsno = '6';          
          extclass.removegps();
          extclass.saveIngredients();
          extclass.addRow();
          extclass.removeRow();
          extclass.gps1 = false;
          extclass.addgps();
          extclass.gps2 = false;
          extclass.addgps();          
          extclass.gps3 = false;
          extclass.addgps();                    
          extclass.gps4 = false;
          extclass.addgps();                    
          extclass.gps5 = false;
          extclass.addgps();                              
          extclass.gps6 = false;
          extclass.addgps();                              
          extclass.allLocations = new List<Location__c>();  
          system.assert(extclass != null);        
          Test.stopTest();   
      }
}