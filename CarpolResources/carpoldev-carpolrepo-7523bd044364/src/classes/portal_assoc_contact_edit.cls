public  class portal_assoc_contact_edit {
    public Id assocconid{get;set;}
    public portal_assoc_contact_edit(ApexPages.StandardController controller) {
    
    assocconid=ApexPages.currentPage().getParameters().get('id');
    
    }
    
   
    
 
       


//VV 10/11/15
public PageReference editAssocContact() {
        String BaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        PageReference dirpage= new PageReference(BaseUrl+'/apex/Portal_Associated_Contact_Create?id='+assocconid);
        dirpage.setRedirect(true);
        return dirpage;
        
    }
//END VV 10/11/15
public PageReference dir() {
        
        PageReference dirpage= new PageReference('/' + assocconid);
        dirpage.setRedirect(true);
        return dirpage;
    }
}