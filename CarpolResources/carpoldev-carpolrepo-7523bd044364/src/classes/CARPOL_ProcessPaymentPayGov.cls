/*
// Purpose : This class is used to process payment using Pay.gov
*/
public with sharing class CARPOL_ProcessPaymentPayGov {

    //Standard Values
    public String strTCSAppID = 'TCSAPHISEFILE';
    public String strAgencyTrackingID;
    public String strtransactionType = 'Sale';
    public String certificateValue = '';
    public fmsTreasGovServicesTcsonline.TCSOnlineServicePort tcsOnlineServicePort = new fmsTreasGovServicesTcsonline.TCSOnlineServicePort();
    private Application__c application;
    
    //Constructor
    public CARPOL_ProcessPaymentPayGov()
    {
        System.Debug('<<<<<<< ProcessPaymentPayGov Constructor Begins >>>>>>>');
      
        //Getting the certificate
        //tcsOnlineServicePort.clientCertName_x = 'le_f114d7b3_5b65_4f72_952f_2ea4c1c644bb';
        
        certificateValue = getResourceBody('QAPayGovCertificate');
        // certificateValue = getResourceBody('efile_pay');
        System.Debug('<<<<<<< Certificate Value : ' + certificateValue + ' >>>>>>>');
        tcsOnlineServicePort.clientCert_x = certificateValue;
        tcsOnlineServicePort.clientCertPasswd_x = 'efil3Ju!y15';
        
    }
    
    public String completeOnlineCollection(String strToken, ID transactionID)
    {
        System.Debug('<<<<<<< completeOnlineCollection Token : ' + strToken + ' >>>>>>>');
        System.Debug('<<<<<<< completeOnlineCollection transactionID : ' + transactionID + ' >>>>>>>');
        String strPayGovTrackingId = '';
        
        //Request Instance
        fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest tcsCompleteOnlineCollectionRequest = new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionRequest();
        tcsCompleteOnlineCollectionRequest.tcs_app_id = strTCSAppID;
        tcsCompleteOnlineCollectionRequest.token = strToken;
        
        //Response Instance
            fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponse  tcsCompleteOnlineCollectionResponse  = new fmsTreasGovServicesTcsonline.CompleteOnlineCollectionResponse();
            
        try
        {
            //Calling the completeOnlineCollection method
            tcsCompleteOnlineCollectionResponse = tcsOnlineServicePort.completeOnlineCollection(tcsCompleteOnlineCollectionRequest);
            strPayGovTrackingId = tcsCompleteOnlineCollectionResponse.paygov_tracking_id;
            System.Debug('<<<<<<< completeOnlineCollection paygovTrackingId : ' + strPayGovTrackingId + ' >>>>>>>');
            if(strPayGovTrackingId != null)
            {
                System.Debug('<<<<<<< About to redirect >>>>>>>');
                return strPayGovTrackingId;
            }
            //if token is not generated`
            else
            {
                return 'NoPayGovTrackingId';
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception in Pay.gov : ' + e));
            return 'Error';
        }
    }
    
    public String startOnlineCollection(String tranAmount, ID transactionID)
    {
        
        System.Debug('<<<<<<< Transaction Amount : ' + tranAmount + ' >>>>>>>');
        System.Debug('<<<<<<< Transaction ID : ' + transactionID + ' >>>>>>>');
        String strToken = '';
        //if(transactionID != null)
        
        application = [Select Id, Name, Name_Applicant__c, Applicant_Billing_Street__c, Applicant_Billing_City__c, Applicant_Billing_State__c, 
                      Applicant_Postal_Code__c, Applicant_Billing_Country__c, Applicant_Email__c from Application__c where id =: transactionID];
        
        if(application == null)
            application = new Application__c();
        
        if(true)
        {
         
            DateTime dt = DateTime.now();
            System.debug(dt.format('yyyyMMddhhmmss'));
            strAgencyTrackingID = dt.format('yyyyMMddhhmmss'); //'86416248'; //Date.today().format() + System.now(); //This creates a unique ID every time. 
            
            //Request Instance
            fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest tcsStartOnlineCollectionRequest = new fmsTreasGovServicesTcsonline.StartOnlineCollectionRequest();
            tcsStartOnlineCollectionRequest.tcs_app_id = strTCSAppID;
            tcsStartOnlineCollectionRequest.agency_tracking_id = strAgencyTrackingID;
            tcsStartOnlineCollectionRequest.transaction_type = strtransactionType;
            tcsStartOnlineCollectionRequest.transaction_amount = tranAmount;
            tcsStartOnlineCollectionRequest.language = 'en';
            tcsStartOnlineCollectionRequest.url_success = URL.getSalesforceBaseUrl().toExternalForm() + '/apex/UpdateApplication?id=' + transactionID;
            tcsStartOnlineCollectionRequest.url_cancel = URL.getSalesforceBaseUrl().toExternalForm()+ '/apex/CARPOL_SomethingWentWrong?id=' + transactionID;
            tcsStartOnlineCollectionRequest.account_holder_name = application.Name_Applicant__c;
            //get Applicant Information
            tcsStartOnlineCollectionRequest.billing_address = application.Applicant_Billing_Street__c;
            tcsStartOnlineCollectionRequest.billing_address2 = ' ';
            System.debug(application.Applicant_Billing_City__c + '\n' + application.Applicant_Billing_State__c + '\n' + application.Applicant_Billing_Country__c
            + '\n' + application.Applicant_Postal_Code__c);
            tcsStartOnlineCollectionRequest.billing_city = application.Applicant_Billing_City__c;
            tcsStartOnlineCollectionRequest.billing_zip = application.Applicant_Postal_Code__c;
            tcsStartOnlineCollectionRequest.billing_country = '840';//application.Applicant_Billing_Country__c;
            tcsStartOnlineCollectionRequest.email_address = application.Applicant_Email__c;
            
            System.debug('Right before the try block');
            try
            {   
                //Response Instance
                fmsTreasGovServicesTcsonline.StartOnlineCollectionResponse tcsStartOnlineCollectionResponse = new fmsTreasGovServicesTcsonline.StartOnlineCollectionResponse();
                System.debug('step 1');
                //Calling the startOnlineCollection method
                tcsStartOnlineCollectionResponse = tcsOnlineServicePort.startOnlineCollection(tcsStartOnlineCollectionRequest);
                System.debug('step 2');
                System.Debug('<<<<<<< tcsStartOnlineCollectionResponse : ' + tcsStartOnlineCollectionResponse + ' >>>>>>>');
                System.debug('step 3');
                strToken = tcsStartOnlineCollectionResponse.token;
                System.debug('step 4');
                System.Debug('<<<<<<< Token Value : ' + strToken + ' >>>>>>>');
                //Redirect to Pay.gov Agency 
                //Agency Testing Environment URL: https://qa.pay.gov/tcsonline/payment.do?token={InputValue}&tcsAppID={InputValue}
                //Production Environment URL: https://www.pay.gov/tcsonline/payment.do?token={InputValue}&tcsAppID={InputValue}
                if(strToken != null)
                {
                    System.Debug('<<<<<<< ProcessPayGov Token : ' + strToken + ' >>>>>>>');
                }
                //if token is not generated
                else
                {
                    strToken = 'NoToken';
                } 
            }
            catch(Exception e)
            {
                System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception : ' + e));
                strToken = 'Error';
                return strToken;
            }
        }
        else
        {
            strToken = 'NoToken';
        }
        return strToken;
    }
    
    public static String getResourceBody(String resourceName) {
        //Fetching the resource
        List<StaticResource> resourceList = [SELECT ID, Body
                                               FROM StaticResource
                                              WHERE Name = :resourceName];
                           
        //Checking if the result is returned or not
        if(resourceList.size() == 1) {
            return EncodingUtil.base64Encode(resourceList[0].Body);
        } else {
            return '';
        }
    }
}