@isTest(seealldata=false)
private class CARPOL_UNI_ViewConstAndSopOvrride_Test {
      @IsTest static void CARPOL_UNI_ViewConstAndSopOvrride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Construct_Application_Junction__c objcaj = new Construct_Application_Junction__c();

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_ViewContructAndSop;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcaj);
              ApexPages.currentPage().getParameters().put('id','test');             
              CARPOL_UNI_ViewConstructAndSopOverride extclass = new CARPOL_UNI_ViewConstructAndSopOverride(sc);
              extclass.redirect();
              system.assert(extclass != null);              
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_ViewContructAndSop;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcaj);
              ApexPages.currentPage().getParameters().put('id','test');
              CARPOL_UNI_ViewConstructAndSopOverride extclassempty = new CARPOL_UNI_ViewConstructAndSopOverride();
              CARPOL_UNI_ViewConstructAndSopOverride extclass = new CARPOL_UNI_ViewConstructAndSopOverride(sc);
              extclass.redirect(); 
              system.assert(extclass != null);                       
          Test.stopTest();   
      }
}