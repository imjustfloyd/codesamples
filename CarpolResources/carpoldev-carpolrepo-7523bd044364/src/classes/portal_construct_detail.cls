public class portal_construct_detail {

public Id constructID{get;set;}
public String redirect {get;set;}
public boolean ret {get;set;}
public Id strPathwayId {get;set;}
public Id LineItemId {get;set;}
public Construct__c obj{get;set;}
public Map<String,String> sobjectkeys {get;set;}
public string delrecid {get;set;}
public string recTypeId{get;set;}

    public portal_construct_detail(ApexPages.StandardController controller) {
        Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
        sobjectkeys = new Map<String,String>();
            for(String s:describe.keyset()){
            sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
            }

    obj = (Construct__c)Controller.getRecord();
     constructID=ApexPages.currentPage().getParameters().get('id');
     redirect = ApexPages.currentPage().getParameters().get('redirect');
     strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
     LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
     
   //  if(LineItemId == NULL || LineItemId == ''){
   if(String.isEmpty(LineItemId)){
         
        // LineItemId = obj.Line_Item__c;
       // string lineitem;
       //  lineitemId = [SELECT Line_Item__c FROM Construct__c WHERE ID=: obj.Id].Line_Item__c;
        list<Construct__c> conList = [SELECT Line_Item__c,Line_Item__r.Status__c  FROM Construct__c WHERE ID=: obj.Id];
        lineitemId = conList[0]. Line_Item__c;        
        // lineitemId = lineitem.id;
         system.debug('LineItemId ###'+LineItemId);
         if(LineItemId != null || LineItemId != '')
             strPathwayId=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId].Program_Line_Item_Pathway__c;
     }

     
         
        if(redirect == 'yes'){
            ret = true;
        } 
        
     // genotypes.sort();        
    }


public List<Phenotype__c> getphenotypes(){
return [SELECT ID,Name,Applicant_Instructions__c,Phenotypic_Category__c, Phenotype_Category_Text__c,Phenotypic_Description__c, Construct__c FROM Phenotype__c WHERE Construct__c =:constructID];// 'a01r0000000dB4H'];
}


public List<Genotype__c> getgenotypes(){
return [SELECT ID,Name,recordtype.name,recordtype.Id,Genotype__c,Construct_Component__c,Construct_Component_Text__c,Construct_Component_Name__c, Donor_list__c, Description__c, Related_Construct_Record_Number__c FROM Genotype__c WHERE Related_Construct_Record_Number__c =:constructID order by recordtype.name];// 'a01r0000000dB4H'];
}

public list<Id> getRecordtypeIds(){
  list<Id> recIdList = new list<Id>(); 
  for(Genotype__c g: getgenotypes()){
      recIdList.add(g.recordtype.Id);
  }    
    return recIdList;    
}

public List<RecordType> getGenoRecordtypes(){
return [SELECT DeveloperName,Name,Id FROM RecordType WHERE SObjectType = 'Genotype__c' and Id in:getRecordtypeIds()];
}

public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }


public List<Change_History__c > gethistory(){
return [SELECT CreatedDate , CreatedById,Id,Name,New_Value__c,Old_Value__c FROM Change_History__c WHERE Construct__c =:constructID];
}

public PageReference EditConstruct(){
    PageReference pg = Page.portal_construct_edit;
    pg.getParameters().put('genid',constructID);
    pg.setRedirect(true);
    return pg;
}
public PageReference NewPhenotype(){
    PageReference pg = Page.portal_phenotype_edit;
    pg.getParameters().put('constid', constructID);
    system.debug('Construct ID passing to phenotype page : ' + constructID);
    pg.setRedirect(true);
    return pg;
}
 public PageReference NewGenotype(){
    PageReference pg = Page.portal_genotype_edit;
    pg.getParameters().put('constid', constructID);
    system.debug('Construct ID passing to genotype page : ' + constructID);
    pg.setRedirect(true);
    return pg;
} 

public PageReference NewGenotype1(){
    PageReference pg = Page.portal_genotyperecordtype;
    pg.getParameters().put('constid', constructID);
    system.debug('Construct ID passing to genotype page : ' + constructID);
    pg.setRedirect(true);
    return pg;
}

public PageReference NewContructComponent(){
    PageReference pg = Page.portal_genotype_edit;
    pg.getParameters().put('constid', constructID);
    pg.getParameters().put('rectype', recTypeId);
    system.debug('Construct ID passing to genotype page : ' + constructID);
    system.debug('recTypeId  passing to genotype page : ' + recTypeId);
    pg.setRedirect(true);
    return pg;
}

public PageReference upload(){
    PageReference pg = Page.Portal_UploadAttachment;
    pg.getParameters().put('Id', constructID);
    pg.getParameters().put('retPage','portal_construct_detail');
    system.debug('Construct ID passing to genotype page : ' + constructID);
    pg.setRedirect(true);
    return pg;
}

public pageReference ret(){
    
       system.debug('LineItemId ## '+LineItemId);
       system.debug('constructID ## '+constructID);
       system.debug('strPathwayId ## '+strPathwayId);
       
       
       PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
    /*    List<AC__c> pathway = new List<AC__c>();     
    
       LineItemId = obj.Line_Item__c;

       pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
       
      Id dummy;
       for(AC__c i : pathway){
       dummy = i.Program_Line_Item_Pathway__c;} 
       
       if(dummy != null || dummy != '')
          strPathwayId = dummy;  */
      
     //  pg.getParameters().put('strPathwayId',dummy);    
      // pg.getParameters().put('LineId',LineItemId);
      pg.getParameters().put('strPathwayId',strPathwayId); 
      pg.getParameters().put('LineId',LineItemId);
       pg.setRedirect(true);
       return pg;
}
        // VV added below method to delete records
             public PageReference deleteRecord(){
                 string sobjkey = delrecid.substring(0,3);
                 string sobjname = sobjectkeys.get(sobjkey);
                  string  strqurey = 'select id from '+sobjname +' where id=:delrecid';
                  list<sobject> lst = database.query(strqurey);
                  delete lst;
                    
                    PageReference dirpage= new PageReference('/apex/portal_construct_detail?id=' + constructID);
                    dirpage.setRedirect(true);
                    return dirpage;
                }   

}