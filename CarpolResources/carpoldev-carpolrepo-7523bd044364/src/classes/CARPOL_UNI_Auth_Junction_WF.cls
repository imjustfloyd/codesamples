/*
Purpose - Create Authorization Junctions and Workflow Tasks 
Authors : Angela Carr, Kishore Kumar, Nitish Jakkani, Dawn Sangree
Last modified date: 05/16/2016

*/

public class CARPOL_UNI_Auth_Junction_WF {

// VV 3-17-16 START
   /* public Date updateExpDate(Authorizations__c authRec){
        Date AuthDate;
        Date expAuthDate;
        Integer ExpirationTimeframe;
        list<AC__c> lineItemList = [SELECT ID, RecordTypeID, Number_of_Labels__c, Hand_Carry__c, Does_This_Application_Contain_CBI__c,Proposed_Start_Date__c,Proposed_End_Date__c, Purpose_of_the_Importation__c, Port_of_Entry__c, Authorization__c,Program_Line_Item_Pathway__c,Program_Line_Item_Pathway__r.Name,Program_Line_Item_Pathway__r.Program__r.Name, is_violator__c, 
                                   Program_Line_Item_Pathway__r.Expiration_Timeframe__c
                                   FROM AC__c WHERE Authorization__c =:authRec.id];
        system.debug('#$#$ ExpirationTimeframe = '+ lineitemList[0].Program_Line_Item_Pathway__r.Expiration_Timeframe__c);
       
        if (lineitemList[0].Program_Line_Item_Pathway__r.Expiration_Timeframe__c > 0){
                ExpirationTimeframe = lineitemList[0].Program_Line_Item_Pathway__r.Expiration_Timeframe__c.intValue();
                AuthDate =  Date.newInstance(authRec.Date_Issued__c.Year(),authRec.Date_Issued__c.month(), authRec.Date_Issued__c.day());
                expAuthDate = AuthDate.addDays(ExpirationTimeframe);
        }
            
        
        return expAuthDate;
    }*/
// VV 3-17-16 END


    public void createAuthorizationJunctionRecord(Map<Authorizations__c, List<AC__c>> authList) //n number of auths in authlist
        {
            if(authlist!=null && authlist.size()>0)
                {
                    
                    system.debug('<! size is ' + authlist.size());
                    list < Workflow_Task__c > WFtasksinsert = new list < Workflow_Task__c > ();
                    list < AC__c > lineItemList = new list < AC__c > ();
                    List < Authorizations__c > ProductAuths = new List < Authorizations__c > ();
                    Map < String, Authorizations__c > tpauthmap = new Map < String, Authorizations__c > ();
                    Set < ID > TPids = new Set < ID > ();
                    Map < ID, Authorizations__c > authidauth = new Map < ID, Authorizations__c > ();
                    List < Authorizations__c > FacilityAuths = new List < Authorizations__c > ();
                    for(Authorizations__c auth1: authList.keyset()) //3 
                    {
                        if (auth1.Auth_Type_Hidden_Flag__c != 'product' && authlist.get(auth1) != null) {
                            lineItemList.addall(authlist.get(auth1));
                        }
                        
                        if (auth1.Auth_Type_Hidden_Flag__c == 'product' || auth1.Auth_Type_Hidden_Flag__c == 'ingredient') {
                            ProductAuths.add(auth1);
                        }
                        //check for nulls to avoid referencing null instead of string on field
                        if (auth1.Auth_Type_Hidden_Flag__c != null) {
                            if (auth1.Auth_Type_Hidden_Flag__c.contains('Facility')) {
                                FacilityAuths.add(auth1);
                            }
                        }
                        
                        TPids.add(auth1.Thumbprint__c);
                        tpauthmap.put(auth1.prefix2__c, auth1);
                        authidauth.put(auth1.id, auth1);
                            
                    }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                                                      //************************************************************Authorization Junction Creation - Start ************************************************************
                    if(!lineItemList.isEmpty()) 
                    {
                        Set<Related_Line_Decisions__c> relatedLineDecisionsList =new Set<Related_Line_Decisions__c>([SELECT ID,Line_Item__c, Decision_Matrix__c FROM Related_Line_Decisions__c WHERE Line_Item__c IN: lineItemList ]); //#####################################################Q2
                        set<id> DMIDs = new Set <id>();
                        list<Authorization_Junction__c> AuthJunctionList = new list<Authorization_Junction__c>();
                        Map<ID, ID> lineAuthMap = new  Map<ID, ID>();
                        Map<ID, Authorization_Junction__c> insertingAJs = new Map<ID, Authorization_Junction__c>();
                        List<Authorization_Junction__c> Final_AuthJunction_list = new List<Authorization_Junction__c>();
                        Map<id,Authorization_Junction__c> authjunctionMap = new map<id,Authorization_Junction__c> () ; 
                                         
                        for(Related_Line_Decisions__c RLD : relatedLineDecisionsList)
                        { 
                            DMIDs.add(RLD.Decision_Matrix__c);
                        }
                         
                             
                        for(AC__c a: lineItemList){
                            lineAuthMap.put(a.ID,a.Authorization__c);
                        }
                        
                        
                        for(Related_Line_Decisions__c Rel : relatedLineDecisionsList)
                        {
                            for(Signature_Regulation_Junction__c SRJ: [SELECT ID, Decision_Matrix__c, Decision_Matrix__r.Thumbprint__c, Regulation__c, Regulation__r.Id, Regulation__r.Regulation_Description__c, Signature__c FROM Signature_Regulation_Junction__c WHERE Decision_Matrix__c IN:DMIDs] )
                            {
                                if(Rel.Decision_Matrix__c == SRJ.Decision_Matrix__c)
                                {
                                    ID lineid = Rel.line_item__c;
                                    
                                    Authorization_Junction__c Authorization_Junction = new Authorization_Junction__c();
                                    Authorization_Junction.is_Active__c = 'Yes';
                                    Authorization_Junction.Authorization__c=lineAuthMap.get(lineid);
                                    Authorization_Junction.Signature_Regulation_Junction_Name__c=SRJ.ID;
                                    Authorization_Junction.Regulation__c = SRJ.Regulation__c;
                                    Authorization_Junction.Decision_Matrix__c = SRJ.Decision_Matrix__c;
                                    Authorization_Junction.Regulation_Description__c= SRJ.Regulation__r.Regulation_Description__c;
                                    AuthJunctionList.add(Authorization_Junction);
                                              
                                 }
                             }
                         }
                         
                         for(Authorization_Junction__c aj: AuthJunctionList )
                         {
                              authjunctionMap.put(aj.Regulation__c, aj);       
                         }       
    
                         Final_AuthJunction_list.addAll(authjunctionMap.values());
                      
                         try
                         {
                             upsert Final_AuthJunction_list; //---------------------inserting Auth Junctions 
                         }
                         catch(Exception e)
                         {
                             System.debug(e);   
                         }
                    }
                      //************************************************************Authorization Junction Creation - End ************************************************************
                     
                     
                     
                     
                     
                     
                     
                     
                     
                        
                    Set<String>  PrefixIDs = new Set<String>();
                    Map<ID,Signature__c> ThumbprintMap = new Map<ID,Signature__c>();
                         
                    if(!TPids.isEmpty()) 
                    {
                        for(Signature__c sign : [select ID,Program_Prefix__c,Name,Response_Type__c, Program_Prefix__r.Name,Program_Prefix__r.Process_Type__c From Signature__c where ID IN: TPids])
                        {
                          PrefixIDs.add(sign.Program_Prefix__r.Name);
                          ThumbprintMap.put(sign.ID, sign);
                        }
                    }    
                         
                    list<Program_Workflow__c> lstCond = [select id,Name,Stop_Status__c,Owner_Approver_is_Signatory__c, Requires_Approval__c,Only_for_this_Role__c, Update_Record_to_Specific_Value_Pending__c,
                                                         Update_Record_to_Specific_Value_Denied__c,Update_Record_to_Specific_Value__c,Assign_to_Queue__c,Authorization_Status__c,Active__c,
                                                         Object_to_Associate_this_Task_to__c,Comments_Destination_Field__c,Copy_Comments_to_Authorization__c,Default_Priority__c,Default_Status__c,
                                                         Dependency_Field__c,Dependent_On_Workflow__c,Description__c,Generate_as_Task__c,How_Executed__c,Internal_Only__c,Program__c,Program_Prefix__c,
                                                         Program_Prefix__r.Name,Program__r.name,Required_if_Prior_Task_Equals__c,Task_Record_Type__c,Update_Authorization_Status__c,Update_Record_Field_Name__c,
                                                         Task_Source_Field_Name__c,Workflow_Order__c,buttons__c,Email_Template__c,Update_Record_to_Specific_Val_InProgress__c,Update_Related_Record_to_Specific_Value__c,
                                                         In_Progress_Status__c,Defer_all_Subsequent_Tasks__c, process_type__c from Program_Workflow__c where Program_Prefix__r.Name IN :PrefixIDs]; //#####################################################Q6
                    system.debug('lstCond '+lstCond);

                    Map<String, Schema.RecordTypeInfo> RecordTypeMap;
                    RecordTypeMap = Schema.SObjectType.Workflow_Task__c.getRecordTypeInfosByName();
                    Id recTypeID= Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
                    Map<string,Id> GroupMap = new map<string,Id>();
                    for(Group g:[select ID,Name from Group LIMIT 10000]){
                        GroupMap.put(g.Name,g.Id);
                    } 
             
                    Map<string,Id> etMap = new map<string,Id>();
                    for(EmailTemplate et: [select id,name,developername from EmailTemplate LIMIT 10000]){
                        etMap.put(et.developername,et.Id);
                    }                        
                     
                          
                    for(Program_Workflow__c programWorkFlowVar: lstCond )
                    {       
                         Workflow_Task__c WFTask = new Workflow_Task__c();
                         if(programWorkFlowVar.Active__c=='Yes' )
                         {
                             if(programWorkFlowVar.Email_Template__c !=null)
                             {
                                  WFTask.Email_Template__c = etMap.get(programWorkFlowVar.Email_Template__c);   
                             }
                             WFTask.RecordTypeId = RecordTypeMap.get(programWorkFlowVar.Task_Record_Type__c).getRecordTypeId();
                             WFTask.name = programWorkFlowVar.Name;
                             WFTask.OwnerId = GroupMap.get(programWorkFlowVar.Assign_to_Queue__c); 
                             WFTask.Priority__c = programWorkFlowVar.Default_Priority__c;
                             WFTask.Status__c = programWorkFlowVar.Default_Status__c;
                             WFTask.Owner_Approver_is_Signatory__c = programWorkFlowVar.Owner_Approver_is_Signatory__c;
                             WFTask.Comments_Destination_Field__c = programWorkFlowVar.Comments_Destination_Field__c;
                             WFTask.Copy_Comments_to_Authorization__c = programWorkFlowVar.Copy_Comments_to_Authorization__c;
                             WFTask.Workflow_Order__c = programWorkFlowVar.Workflow_Order__c;
                             WFTask.Update_Record_Status__c = programWorkFlowVar.Update_Authorization_Status__c;
                             WFTask.Update_Record_Field_Name__c = programWorkFlowVar.Update_Record_Field_Name__c;
                             WFTask.Task_Source_Field_Name__c = programWorkFlowVar.Task_Source_Field_Name__c;
                             WFTask.Dependent_On_Workflow__c = programWorkFlowVar.Dependent_On_Workflow__c;
                             WFTask.Dependency_Field__c = programWorkFlowVar.Dependency_Field__c;
                             WFTask.Required_if_Prior_Task_Equals__c = programWorkFlowVar.Required_if_Prior_Task_Equals__c;
                             WFTask.Stop_Status__c = programWorkFlowVar.Stop_Status__c;
                             WFTask.Record_Status__c = programWorkFlowVar.Authorization_Status__c;
                             WFTask.Update_Record_to_Specific_Value__c = programWorkFlowVar.Update_Record_to_Specific_Value__c;
                             WFTask.Update_Record_to_Specific_Value_Denied__c = programWorkFlowVar.Update_Record_to_Specific_Value_Denied__c;
                             WFTask.Update_Record_to_Specific_Value_Pending__c = programWorkFlowVar.Update_Record_to_Specific_Value_Pending__c;//added by Kishore
                             WFTask.Update_Record_to_Specific_Val_InProgress__c = programWorkFlowVar.Update_Record_to_Specific_Val_InProgress__c;//added by Kishore 10/19/2015
                             WFTask.Update_Related_Record_to_Specific_Value__c = programWorkFlowVar.Update_Related_Record_to_Specific_Value__c;//added by Kishore 10/20/2015                      
                             WFTask.Program_Prefix__c = programWorkFlowVar.Program_Prefix__r.Name;// added by Kishore
                             WFTask.Program__c = programWorkFlowVar.Program__r.name; //added by kishore
                             WFTask.Approval_Roles__c = programWorkFlowVar.Only_for_this_Role__c;
                             WFTask.Requires_Approval__c = programWorkFlowVar.Requires_Approval__c;
                             WFTask.buttons__c = programWorkFlowVar.buttons__c; // added by kishore
                             WFTask.Instructions__c = programWorkFlowVar.Description__c; // added by Kishore 10/10/2015
                             WFTask.In_Progress_Status__c = programWorkFlowVar.In_Progress_Status__c; // added by kishore 10/26/2015
                             WFTask.Defer_all_Subsequent_Tasks__c = programWorkFlowVar.Defer_all_Subsequent_Tasks__c; // added by kishore
                             WFTask.process_type__C = programWorkFlowVar.process_type__c;
                             WFtasksinsert.add(WFTask);
                         }
                    } // ----------------------end of for loop----------------
                        
                    Map<Authorizations__c,Id> childauthparentidmap = new   Map<Authorizations__c,ID>(); 
                    Map<String,List<Workflow_Task__c>> prefixwflistmap = new  Map<String,List<Workflow_Task__c>>();
                    Set<ID> wfauth = new Set<ID>();
                    Integer IntA = 0;
                    String ResponseType;
                    String appId;
                    String authId;
                    String ResponseId;
                    String ProcessType;
                    String Hiddenflag='';
                    String prefix;
                    String violator;
                    string authstatus;
                    Boolean neeedBothPermits=false;
                    Authorizations__c authtoupdate = new Authorizations__c();
                    List<Authorizations__c> AuthListToUpdate = new List<Authorizations__c>();
                    List<Workflow_Task__c> deletelist = new  List<Workflow_Task__c>();
                    for(Workflow_Task__c wf : WFtasksinsert)
                    {       
                        Authorizations__c auth = tpauthmap.get(wf.Program_Prefix__c);
                        wf.WF_Auth_Hidden_flag__c = auth.Auth_Type_Hidden_Flag__c;
                        if(wf.WF_Auth_Hidden_flag__c=='ingredient' && WF.name.contains('Violator'))
                        {
                            deletelist.add(wf);
                        }
                                  
                        if(wf.WF_Auth_Hidden_flag__c=='ingredient' && WF.name.contains('Issue'))
                        {
                             deletelist.add(wf);
                        }
                        if(wf.Program_Prefix__c==tpauthmap.get(wf.Program_Prefix__c).prefix2__c)
                        {   
                             wf.authorization__c = tpauthmap.get(wf.Program_Prefix__c).ID;
                             WF.Is_Violator__c = tpauthmap.get(wf.Program_Prefix__c).Is_Violator__c;                             
                                                                                                            
                             if(wf.Process_Type__c=='Fast Track')                                                                                   //=========================Fast Track====================== 
                             { 
                                 if( WF.name.contains('Check Violator')) //---------------------WF 1 - Fast Track
                                 {  
                                      WF.Status__c = 'Complete';
                                      WF.Complete_from_trigger__c = TRUE;
                                      wf.Locked__c = 'Yes';
                                 }
                                              
                                 if(WF.Name.contains('Review Potential Violator')) //---------------------WF 2 - Fast Track
                                 {  
                                     WF.Status__c = 'Deferred';
                                     WF.Complete_from_trigger__c = TRUE;
                                     wf.Locked__c = 'Yes';
                                 }
                                                       
                                 if(WF.Name.contains('Process Authorization'))  //---------------------WF 3 - Fast Track
                                 { 
                                     WF.Status__c = 'Complete';
                                     WF.Complete_from_trigger__c = TRUE;
                                     wf.Locked__c = 'Yes';
                                 }
                                                        
                                 if(WF.Name.contains('Issue Authorization') || wf.Name.contains('Issue')) //---------------------WF 4 - Fast Track
                                 {  
                                     WF.Status__c = 'Complete';
                                     WF.Complete_from_trigger__c = TRUE;
                                     wf.Locked__c = 'Yes';
 
                                     auth.Status__c = 'Issued';
                                     auth.Date_Issued__c = System.Today();
                                                             
                                     Date AuthDate;
                                     Date expAuthDate;
                                     Integer ExpirationTimeframe;
                                     ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe__c);
                                     AuthDate =  Date.newInstance(auth.Date_Issued__c.Year(),auth.Date_Issued__c.month(), auth.Date_Issued__c.day());
                                     auth.Expiration_Date__c = AuthDate.addDays(ExpirationTimeframe);
                                   
                                     auth.Authorization_Type__c = ThumbprintMap.get(auth.Thumbprint__c).Response_Type__c;
                                     hiddenflag = auth.Auth_Type_Hidden_Flag__c;
                                     authtoupdate = auth;
                                     AuthListToUpdate.add(authtoupdate);
                                     childauthparentidmap.put(auth,auth.Parent_Authorization__c);
                                 }
                                 //CITES LOGIC
                                 if(!auth.CITES_Needed__c || !auth.Regular_Permit_Needed__c) {
                                     if(WF.Name.contains('Issue Protected Plant Permit'))
                                     {
                                         if(!auth.CITES_Needed__c){
                                             WF.Status__c = 'Deferred';
                                         }
                                     }
                                     if(WF.Name == 'Issue Standard Permit'){
                                         if(auth.CITES_Needed__c && !auth.Regular_Permit_Needed__c)
                                             WF.Status__c = 'Deferred';  
                                     }
                                 }
                                 if(auth.CITES_Needed__c && auth.Regular_Permit_Needed__c)
                                 {
                                     neeedBothPermits =true;
                                 }
                                                       
                             }
                             else                                                                                                               //=========================Manual====================== 
                             {
                                 if (auth.Is_Violator__c == 'No' && WF.name.contains('Check Violator')) //---------------------WF 1 - Manual 
                                 {
                                      if (auth.select_agent__c){ //VV added 9/8/16 or select agent
                                          WF.Status__c = 'Not Started';
                                      }else{
                                          WF.Status__c = 'Complete';
                                      }
                                      WF.Complete_from_trigger__c = TRUE;
                                      wf.Locked__c = 'Yes';
                                 }
                                 if (auth.Is_Violator__c == 'No' /*&& auth.Auth_Type_Hidden_Flag__c !='ingredient'*/ && WF.Name.contains('Review Potential Violator'))//---------------------WF 2 - Manual
                                 { 
                                      WF.Status__c = 'Deferred';
                                      WF.Complete_from_trigger__c = TRUE;
                                      wf.Locked__c = 'Yes';
                                 }
                                                  
                                 if (auth.Is_Violator__c == 'No'  && wf.Name.contains('Process Authorization') && auth.RecordTypeID != recTypeID ) //---------------------WF 3 - Manual
                                 {
                                      WF.Status__c = 'Not Started';
                                 }
                                 
                                  if (auth.Is_Violator__c == 'No'  && wf.Name.contains('Perform Evaluation')) //---------------------WF 3 - Manual
                                 {
                                      WF.Status__c = 'Not Started';
                                 }
                                                  
                                 /*if(auth.Auth_Type_Hidden_Flag__c == 'ingredient')
                                 {
                                      WF.Status__c = 'Not Started';
                                 }*/
                                          
                             }
                                             
                             if(prefixwflistmap.containsKey(wf.Program_Prefix__c))
                             {
                                 List<Workflow_Task__c> MapwfList = prefixwflistmap.get(wf.Program_Prefix__c);
                                 MapwfList.add(wf);
                                 prefixwflistmap.remove(wf.Program_Prefix__c);
                                 prefixwflistmap.put(wf.Program_Prefix__c,MapwfList);
                             } else {
                                 List<Workflow_Task__c> MapwfList = new List<Workflow_Task__c>();
                                 MapwfList.add(wf);
                                 prefixwflistmap.put(wf.Program_Prefix__c,MapwfList);                                        
                             }
                                     
                                             
                        }
                            ResponseType = ThumbprintMap.get(auth.Thumbprint__c).Response_Type__c;
                            appId = string.valueof(auth.Application__c);
                            authId = string.valueof(auth.id);
                            ResponseId = string.valueof(ResponseType);
                            ProcessType = ThumbprintMap.get(auth.Thumbprint__c).Program_Prefix__r.Process_Type__c;
                            prefix = auth.Thumbprint__r.Program_Prefix__r.Name;
                            violator = auth.Is_violator__c;
                            authstatus = auth.status__c;
                            
                            wfauth.add(wf.authorization__c);
                    }          
                    List<Authorizations__c> leftoverauths = new List<Authorizations__c>();
                    if(authList.size()!=wfauth.size())
                    {
                        for(Authorizations__c auth : authList.keyset())
                        {
                            if(!wfauth.contains(auth.id))
                                leftoverauths.add(auth);
                        }
                    }
                    system.debug('<!!! leftoverauths'+leftoverauths);
                      
                   //*****************************************************************************************************************************leftover auths************************
                   if(!leftoverauths.isEmpty()){
                       for(Authorizations__c auth: leftoverauths)
                       {
                           List<Workflow_Task__c> leftoverwfs =prefixwflistmap.get(auth.prefix2__c);
                           System.debug('<!!!!!!!!!!! leftoverwfs '+leftoverwfs);
                           if(leftoverwfs!=null)
                           {
                               List<Workflow_Task__c> programworkflows = new List<Workflow_Task__c>();
                               for(Workflow_Task__c wf : leftoverwfs)
                               {
                                   Workflow_Task__c WFTask = new Workflow_Task__c();
                                   WFTask = wf.clone(false,true);
                                   WFTask.authorization__c = auth.id;
                                   programworkflows.add(WFTask);
                               }
                               for(Workflow_Task__c wf : programworkflows)
                               {   
                                   //===============Fast Track==============                               
                                   if(wf.Process_Type__c=='Fast Track') 
                                   { 
                                       if( WF.name.contains('Check Violator')) //---------------------WF 1
                                       {  
                                           WF.Status__c = 'Complete';
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                       }
                                                          
                                       if(WF.Name.contains('Review Potential Violator')) //---------------------WF 2
                                       {  
                                           WF.Status__c = 'Deferred';
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                       }
                                       if(WF.Name.contains('Process Authorization'))  //---------------------WF 3
                                       { 
                                           WF.Status__c = 'Complete';
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                       }
                                                                   
                                       if(WF.Name.contains('Issue Authorization') || wf.Name.contains('Issue')) //---------------------WF 4
                                       {  
                                           WF.Status__c = 'Complete';
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                                           
                                           auth.Status__c = 'Issued';
                                           auth.Date_Issued__c = System.Today();
                                                                         
                                           Date AuthDate;
                                           Date expAuthDate;
                                           Integer ExpirationTimeframe;
                                           ExpirationTimeframe = integer.valueOf(auth.Expiration_Timeframe__c);
                                           AuthDate =  Date.newInstance(auth.Date_Issued__c.Year(),auth.Date_Issued__c.month(), auth.Date_Issued__c.day());
                                           auth.Expiration_Date__c = AuthDate.addDays(ExpirationTimeframe);
                                           hiddenflag = auth.Auth_Type_Hidden_Flag__c;
                                                                         
                                           AuthListToUpdate.add(auth);
                                           childauthparentidmap.put(auth,auth.Parent_Authorization__c);
                                        }
                                                                
                                        //CITES LOGIC
                                        if(!auth.CITES_Needed__c || !auth.Regular_Permit_Needed__c) {
                                            if(WF.Name.contains('Issue Protected Plant Permit'))
                                            {
                                                if(!auth.CITES_Needed__c){
                                                    WF.Status__c = 'Deferred';
                                                }
                                            }
                                            if(WF.Name == 'Issue Standard Permit'){
                                                if(auth.CITES_Needed__c && !auth.Regular_Permit_Needed__c)
                                                    WF.Status__c = 'Deferred';  
                                            }
                                         }
                                         if(auth.CITES_Needed__c && auth.Regular_Permit_Needed__c)
                                         {
                                             neeedBothPermits =true;
                                         }
                                                                       
                                   }
                                   else
                                   {
                                       if (auth.Is_Violator__c == 'No' && WF.name.contains('Check Violator'))//-----wf1-----Manual
                                       {
                                          if (auth.select_agent__c){ //VV added 9/8/16 or select agent
                                                  WF.Status__c = 'Not Started';
                                              }else{
                                                  WF.Status__c = 'Complete';
                                              }
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                       }
                                                                      
                                       if (auth.Is_Violator__c == 'No' /*&& auth.Auth_Type_Hidden_Flag__c !='ingredient'*/ && WF.Name.contains('Review Potential Violator'))//---wf2 ---- Manual 
                                       { 
                                           WF.Status__c = 'Deferred';
                                           WF.Complete_from_trigger__c = TRUE;
                                           wf.Locked__c = 'Yes';
                                       }
                                                                     
                                       if (auth.Is_Violator__c == 'No'  && (wf.Name.contains('Process Authorization') || wf.Name.contains('Issue'))) //wf 3 ---manual
                                       {
                                           WF.Status__c = 'Not Started';
                                       }
                                                                      
                                       /*if(auth.Auth_Type_Hidden_Flag__c == 'ingredient')
                                       {
                                           WF.Status__c = 'Not Started';
                                       }*/
                                                              
                                   }
                               }
                               WFtasksinsert.addAll(programworkflows);
                           }
                            
                       }
                   }
                   //***********************************************************************************************************************************leftover auths*************************************************************************************
                   
                   
                   Database.SaveResult[] savelst = Database.insert(WFtasksinsert); //********************inserting WF tasks *********************
                         
                   Set<Authorizations__c> Dedupeset = new Set<Authorizations__c>();
                   List<Authorizations__c> DedupeAuths = new  List<Authorizations__c>();
                   if(ProductAuths.size()!=null)//.isEmpty()
                   {
                       AuthListToUpdate.addall(ProductAuths);
                       Dedupeset.addall(AuthListToUpdate);
                       DedupeAuths.addall(Dedupeset);
                       for(Authorizations__c auth :DedupeAuths){
                           if(auth.Auth_Type_Hidden_Flag__c=='ingredient' && (auth.Status__c=='Issued' || auth.Status__c=='Approved'))
                           {   
                               if(authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c==null){
                                   Integer i=1;
                                   authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c = string.valueOf(i);
                               }
                               else{
                                   Integer j;
                                   j=integer.valueOf(authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c);
                                   j++;
                                    authidauth.get(childauthparentidmap.get(auth)).Approved_child_auth_num__c =string.valueOf(j);
                               }
                            }   
                       }
                       try
                       {
                           update DedupeAuths; //---------------------updating Product +Fasttrack auths 
                       }
                       catch(Exception e)
                       {
                           System.debug(e);   
                       }
                   }
                          
                   else{
                       try
                       {
                           update AuthListToUpdate; //---------------------updating just Fasttrack auths 
                       }
                       catch(Exception e)
                       {
                           System.debug(e);   
                       }
                   }
                   if(FacilityAuths.size() > 0){
                       try
                       {
                           //updating in bulk
                           update FacilityAuths; //---------------------updating facility auths 
                       }
                       catch(Exception e)
                       {
                           System.debug(e);   
                       }
                   }
                   System.debug('Before future : ' + ProcessType + ' ' + hiddenflag + ' ' + ResponseType);
                   if(ProcessType == 'Fast Track' && (hiddenflag==null || hiddenflag=='product')  &&
                   (ResponseType =='Permit' ||
                    ResponseType == 'Letter of Denial' ||
                    ResponseType == 'Letter of No Permit Required' ||
                    ResponseType == 'Letter of No Jurisdiction'))
                    {
                       IntA++;
                       system.debug('<! AuthListToUpdate'+AuthListToUpdate);
                       String Productcore = 'No';
                       //update AuthListToUpdate
                       FutureAttachmentCreator.sendVF(string.valueOf(userinfo.getSessionId()), appId, authId, ResponseId);
                    }
              
                }
        }
}