/*
// Author : Vinar Amrutia
// Created On : Feb 26 2015
// Purpose : This apex class helps fetch the right template to be attached as response
*/
public class CARPOL_AC_AddLetters {

    public String selectedTempalteID { get; set; }
    public String selectedTempalteType { get; set; }
    public String attachmentName { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    private AC__c acLineItem;
    
    public CARPOL_AC_AddLetters(ApexPages.StandardController controller) {
        
        this.acLineItem = (AC__c)controller.getRecord();
        selectedTempalteType = apexpages.currentpage().getParameters().get('type');
        selectedTempalteID = apexpages.currentpage().getParameters().get('template');
        allTemplateOptions = new SelectOption[]{};
        if(selectedTempalteType == null){selectedTempalteType = '--None--';}
        getTemplates();
    }
    
    public PageReference getTemplates() {
        
        System.debug('<<<<<<< Template Type : ' + selectedTempalteType + ' >>>>>>>');
        allTemplateOptions.clear();
        if(selectedTempalteType != '--None--')
        {
            for(Communication_Manager__c commMgr : [SELECT ID, Name FROM Communication_Manager__c WHERE Type__c = :selectedTempalteType]){
                allTemplateOptions.add(new SelectOption(commMgr.Id,commMgr.Name));
            }
        }
        else
        {
           for(Communication_Manager__c commMgr : [SELECT ID, Name FROM Communication_Manager__c]){
                allTemplateOptions.add(new SelectOption(commMgr.Id,commMgr.Name));
            } 
        }
        System.debug('<<<<<<< allTemplateOptions size : ' + allTemplateOptions.size() + ' >>>>>>>');
        return null;
    }
    
    public PageReference populateTemplateContent() {
        
        System.debug('<<<<<<< Selected Template ID : ' + selectedTempalteID + ' >>>>>>>');
        
        if(selectedTempalteID != '--None--')
        {
            Communication_Manager__c commMgr = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID = :selectedTempalteID LIMIT 1];
            System.debug('<<<<<<< Content : ' + commMgr.Content__c + ' >>>>>>>');
            acLineItem.Communication_Letter__c = commMgr.Content__c;
            update(acLineItem);
            PageReference redirect = new PageReference('/apex/CARPOL_AC_AddLetters'); 
            redirect.getParameters().put('id',acLineItem.ID);
            redirect.getParameters().put('type',selectedTempalteType);
            redirect.getParameters().put('template',selectedTempalteID);
            redirect.setRedirect(true); 
            return redirect;
        }
        else{return null;}
    }
    
    public PageReference saveLetter() {
        
        try
        {
            update(acLineItem);
            Blob pdfLetterBlob ;
            
            if(!Test.isRunningTest())
            {
                if(attachmentName == null || attachmentName.contains('.'))
                {
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please type in a name without . for your attachment.'));
                }
                else
                {
                    pdfLetterBlob = EncodingUtil.base64Decode(acLineItem.Communication_Letter__c);
                    Attachment attLetter = new Attachment(parentId = acLineItem.Id, name = attachmentName + '.pdf', body = pdfLetterBlob);
                    insert attLetter;
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Template attached successfully !!'));
                }
            }
        }
        catch(Exception e)
        {
            System.debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong. Please re-try or contact your System Administrator.'));
        }
        return null;
    }
}