public with sharing class CARPOL_AC_WorkflowButtonsController {
    public Authorizations__c au;
    public Workflow_Task__c wf;
   // Private String authId;
    transient public String renderPermit {get; set;} 
    public String authid {get; set;} 
    public String wfID = ApexPages.currentPage().getParameters().get('id');
    public CARPOL_AC_WorkflowButtonsController(ApexPages.StandardController Controller) {
      renderPermit = 'False';      
      wf = [Select Id, Authorization__r.Authorization_Type__c,Authorization__c  FROM Workflow_Task__c Where Id =:wfId];
      
        IF(wf.Authorization__r.Authorization_Type__c == 'Permit')
        {
          renderPermit = 'True';  
          authid=wf.Authorization__c;
        
       }
       else{
       authid=wf.Authorization__c;
       }
      // return renderPermit;
  }

       
    


}