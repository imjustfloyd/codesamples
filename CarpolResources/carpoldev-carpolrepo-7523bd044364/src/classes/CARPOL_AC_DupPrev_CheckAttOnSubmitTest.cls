@isTest(seealldata=true)

// Test class for trigger : CARPOL_AC_DuplicatePreventer_CheckAttachmentsOnSubmit.trigger
Private class CARPOL_AC_DupPrev_CheckAttOnSubmitTest{
    static testMethod void ACDupPrevAtt() {
    
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ACAttachmentsRecordTypeId = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('AC Attachment').getRecordTypeId();
      
      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();      
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      test.starttest();
      Application__c objapp = testData.newapplication();
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
      ac1.Authorization__c = objauth.id;
      update ac1;
      //AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
      //ac2.Authorization__c = objauth.id;
      //update ac2;      
      //AC__c ac3 = testData.newLineItem('Personal Use',objapp);              
      //ac3.Authorization__c = objauth.id;
      //update ac3;      
      Attachment attach = testData.newattachment(ac1.Id);           
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id);       
      
      List<Applicant_Attachments__c> attachs = new List<Applicant_Attachments__c>();
      Applicant_Attachments__c appatt = new Applicant_Attachments__c();
      appatt.RecordTypeid = ACAttachmentsRecordTypeId;
      appatt.Animal_Care_AC__c =ac1.id;
      appatt.Document_Types__c = 'Veterinary Treatment Agreement';
      attachs.add(appatt);
      Applicant_Attachments__c appatt2 = new Applicant_Attachments__c();
      appatt2.RecordTypeid = ACAttachmentsRecordTypeId;
      appatt2.Animal_Care_AC__c =ac1.id;
      appatt2.Document_Types__c = 'Rabies Vaccination Certificate (AC7042)';
      attachs.add(appatt2);
      Applicant_Attachments__c appatt3 = new Applicant_Attachments__c();
      appatt3.RecordTypeid = ACAttachmentsRecordTypeId;
      appatt3.Animal_Care_AC__c =ac1.id;
      appatt3.Document_Types__c = 'Health Certificate (AC7041)';
      attachs.add(appatt3);
      Applicant_Attachments__c appatt4 = new Applicant_Attachments__c();
      appatt4.RecordTypeid = ACAttachmentsRecordTypeId;
      appatt4.Animal_Care_AC__c =ac1.id;
      appatt4.Document_Types__c = 'IACUC Approved Research Proposal';
      attachs.add(appatt4);
      Applicant_Attachments__c appatt5 = new Applicant_Attachments__c();
      appatt5.RecordTypeid = ACAttachmentsRecordTypeId;
      appatt5.Animal_Care_AC__c =ac1.id;
      appatt5.Document_Types__c = 'Research Justification';
      attachs.add(appatt5);
      insert attachs;
      ac1.Status__c = 'Ready to Submit';
      update ac1;      

      ac1.Purpose_of_the_Importation__c = 'Resale/Adoption';
      ac1.Date_of_Birth__c = date.today()-200;
      update ac1;
      ac1.Purpose_of_the_Importation__c = 'Research';
      ac1.Date_of_Birth__c = date.today()-300;
      ac1.USDA_Registration_Number__c = '11H1010';

      ac1.USDA_Expiration_Date__c = date.today()+20;
      update ac1;
      System.assert(ac1 != null);
      test.stoptest();
    
    }
}