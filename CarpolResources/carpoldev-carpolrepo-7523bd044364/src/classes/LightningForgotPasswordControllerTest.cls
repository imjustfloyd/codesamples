/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class LightningForgotPasswordControllerTest {

     @IsTest(SeeAllData=true) public static void testLightningForgotPasswordController() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
          AC__c li = testData.newlineitem('Personal Use', objapp);
          Authorizations__c objauth = testData.newAuth(objapp.id);
     
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
        // Instantiate a new controller with all parameters in the page
        LightningForgotPasswordController controller = new LightningForgotPasswordController();
        //LightningForgotPasswordController.forgotPassowrd(usershare.Username, '/apex/CARPOL_HomePage');      
    
        System.assert(controller != null); 
    }
    
}