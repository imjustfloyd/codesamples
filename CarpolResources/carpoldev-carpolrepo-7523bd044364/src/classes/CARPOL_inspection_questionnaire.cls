public class CARPOL_inspection_questionnaire {
    public Map<String,List<SelectOption>> mapOptions{get;set;}
    public Map<String,String> mapResponses{get;set;}
    public List<EFL_Inspection_Questionnaire_Questions__c> listEIQ{get;set;}
    public EFL_Inspection_Questionnaire__c Questionnaire {get;set;}
    List<EFL_Inspection_Questionnaire_Questions__c> RelatedQuestList{get;set;}
   // String country = null;
    List<EFL_Inspection_Questionnaire_Questions__c> questionnairequestions = new List<EFL_Inspection_Questionnaire_Questions__c>();
    Id questionnaireid;
    public Integer Size;
    public Integer selectedSize{Set;get;}
    public EFL_Inspection_Questionnaire__c objEIQ;
    public boolean bIsDisabled{get;set;}
    public CARPOL_inspection_questionnaire(ApexPages.StandardController controller) {
        Size = 5;
        Questionnaire =  (EFL_Inspection_Questionnaire__c)Controller.getRecord();
        questionnaireid = ApexPages.currentPage().getParameters().get('id');
        objEIQ=[select id,Responses_Submitted__c from EFL_Inspection_Questionnaire__c where id=:questionnaireid ];
        bIsDisabled=objEIQ.Responses_Submitted__c;
        listEIQ=new List<EFL_Inspection_Questionnaire_Questions__c>();
        mapOptions=new Map<String,List<SelectOption>>(); 
        mapResponses=new Map<String,String>();
        Set<Id> setQuesIds=new Set<Id>();
        for(EFL_Inspection_Questionnaire_Questions__c recEIQ:[SELECT ID,NAME,Question__c,Answer__c,Comments__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid])
        {
            listEIQ.add(recEIQ);
            mapResponses.put(recEIQ.Id,'');
            setQuesIds.add(recEIQ.Id);
        }
        for(EFL_Inspection_Questionnaire_Responses__c recEIQR:[SELECT ID,NAme, EFL_Inspection_Questionnaire_Questions__c, Response__c FROM EFL_Inspection_Questionnaire_Responses__c WHERE EFL_Inspection_Questionnaire_Questions__c IN:setQuesIds ])
        {
            List<SelectOption> optionsTemp =mapOptions.get(recEIQR.EFL_Inspection_Questionnaire_Questions__c);
            if(optionsTemp==null)
            {
                 optionsTemp =new List<SelectOption>();
                 mapOptions.put(recEIQR.EFL_Inspection_Questionnaire_Questions__c,optionsTemp);
            }
            optionsTemp.add(new SelectOption(recEIQR.Response__c,recEIQR.Response__c));
        }
        
       
    }
    public pagereference save()
    {
        if(RelatedQuestList!=null && RelatedQuestList.size()>0){
            update RelatedQuestList;
            
            /*Questionnaire.Status__c = 'Responses Submitted';
            update Questionnaire;*/
        }
        
        /*PageReference pg = Page.thank_you;
         pg.setRedirect(true);
                    return pg;*/
                    return null;
    }
    
    public pagereference submit()
    {
      try{
            save();
            Integer iCnt=0;
            for(EFL_Inspection_Questionnaire_Questions__c recEIQ:[SELECT ID,NAME,Question__c,Answer__c,Comments__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid])
                if(recEIQ.Answer__c!=null)
                    iCnt++;
            if(iCnt!=listEIQ.size())
            {
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'All the questionnaire must be completed before the Submition for approval')); 
                 return null;
            }
            objEIQ.Responses_Submitted__c=true;
            update objEIQ;
            bIsDisabled=true;
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setObjectId(questionnaireid);
        Approval.ProcessResult result = Approval.process(req1);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Successfully Submitted'));
     /*   PageReference pg = Page.thank_you
         pg.setRedirect(true);
          return pg;  */
             
             return null;  
                
                    }
      catch(Exception ex){ 
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'This record is currently in an approval process. A record can be in only one approval process at a time'+ex)); 
      return null; }
    }
   
    /*public List<EFL_Inspection_Questionnaire_Questions__c> getqs() {
       questionnairequestions= [SELECT ID,NAME,Question__c,Answer__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid];
        return questionnairequestions;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        
        List< EFL_Inspection_Questionnaire_Responses__c > responseoptions = new List< EFL_Inspection_Questionnaire_Responses__c >();
       
        
        
        responseoptions = [SELECT ID,NAme, EFL_Inspection_Questionnaire_Questions__c, Response__c FROM EFL_Inspection_Questionnaire_Responses__c WHERE EFL_Inspection_Questionnaire_Questions__c IN: questionnairequestions ];
        options.add(new SelectOption('US','US'));
        options.add(new SelectOption('CANADA','Canada'));
        options.add(new SelectOption('MEXICO','Mexico')); 
       
        for(EFL_Inspection_Questionnaire_Responses__c r:responseoptions){
            options.add(new SelectOption(r.Response__c,r.Response__c));
        }
        return options;
    }
    
    public String getCountry() {
        return country;
    }
                     
    public void setCountry(String country) { 
        this.country = country; 
    }
    */
     //To set new Size for the pagination 
    public void setPaginationSize(){
     Size = selectedSize ;
    }
     public ApexPages.StandardSetController setcon {
        get {
            if(setcon == null) {
                setcon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID,NAME,Question__c,Answer__c,Comments__c FROM EFL_Inspection_Questionnaire_Questions__c WHERE EFL_Inspection_Questionnaire__c=:questionnaireid]));
                // sets the number of records in each page set
                setcon.setPageSize(5);
            }
            return setcon;
        }
        set;
    }
    
    
   //To Display Queried records on Visualforce page 
   public List<EFL_Inspection_Questionnaire_Questions__c> getRelatedQuestList(){
   
        RelatedQuestList= new List<EFL_Inspection_Questionnaire_Questions__c>();
        for (EFL_Inspection_Questionnaire_Questions__c objRQ : (List<EFL_Inspection_Questionnaire_Questions__c>)setcon.getRecords())
        {
            EFL_Inspection_Questionnaire_Questions__c recRQ=new EFL_Inspection_Questionnaire_Questions__c(ID=objRQ .Id);
            recRQ.Question__c=objRQ.Question__c;
            recRQ.Answer__c=objRQ.Answer__c;
            recRQ.Comments__c =objRQ.Comments__c ;
            RelatedQuestList.add(recRQ);
        }
            

        return RelatedQuestList;
   }
     // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return setcon.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return setcon.getHasPrevious();
        }
        set;
    }


    // returns the first page of records
     public void first() {
         upsert RelatedQuestList;
         setcon.first();
     }

     // returns the last page of records
     public void last() {
         upsert RelatedQuestList;
         setcon.last();
     }

     // returns the previous page of records
     public void previous() {
         upsert RelatedQuestList;
         setcon.previous();
     }

     // returns the next page of records
     public void next() {
         upsert RelatedQuestList;
         setcon.next();
     }

     // returns the PageReference of the original page, if known, or the home page.
     public void cancel() {
         setcon.cancel();
     }

    
  
    
}