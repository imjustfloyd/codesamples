//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class eAuthJITUserProvisioning implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
    private void handleUser(boolean create, User u, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard) {
        
       
        
        system.debug(attributes);
        if(create) {
        
         u.Username = attributes.get('usdaemail') + '.' +attributes.get('usdalastname');
         system.debug('Username ' + u.Username);
            if(attributes.containsKey('usdaeauthid')) {
                u.FederationIdentifier = attributes.get('usdaeauthid');
                system.debug('fed id' + u.FederationIdentifier);
           
            }
            }
        
        if(attributes.containsKey('usdahomephone')) {
            u.Phone = attributes.get('usdahomephone');
            system.debug('home phone ' + u.Phone);
        }
        if(attributes.containsKey('usdaemail')) {
            u.Email = attributes.get('usdaemail');
            system.debug('email ' + u.Email);
        }
        if(attributes.containsKey('usdafirstname')) {
            u.FirstName = attributes.get('usdafirstname');
            system.debug('firstname ' + u.FirstName);
        }
        if(attributes.containsKey('usdalastname')) {
            u.LastName = attributes.get('usdalastname');
            system.debug('lastname ' + u.LastName);
        }
        if(attributes.containsKey('MailingAddress')) {
            u.Street = attributes.get('MailingAddress');
            system.debug('address ' +u.street);
        }
        if(attributes.containsKey('MailingState')) {
            system.debug('state '+ attributes.get('MailingState'));
            u.State = [select id, Level_1_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('MailingState')].Name;
            system.debug('state ' +u.State);
        }
        if(attributes.containsKey('MailingCity')) {
            u.City = attributes.get('MailingCity');
            system.debug('city ' +u.City);

        }
        if(attributes.containsKey('MailingPostalCode')) {
            u.PostalCode = attributes.get('MailingPostalCode');
            system.debug('Zip ' +u.PostalCode);

        }
        if(attributes.containsKey('usdacountry')) {
            //u.Country = attributes.get('usdacountry');
            //system.debug('country '+ u.Country);
        }
      
        if(attributes.containsKey('usdaagencycode')) {
            u.Division = attributes.get('usdaagencycode');
        }
        
        String uid = UserInfo.getUserId();
         User currentUser = 
            [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        if(create) {
            u.LocaleSidKey = currentUser.LocaleSidKey;
            u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
            u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
            u.EmailEncodingKey = currentUser.EmailEncodingKey;
            String alias = '';
            alias = u.LastName;
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }
         
        u.CommunityNickname = u.LastName;
        

        /*
         * If you are updating Contact or Account object fields, you cannot update the following User fields at the same time.
         * If your identity provider sends these User fields as attributes along with Contact 
         * or Account fields, you must modify the logic in this class to update either these 
         * User fields or the Contact and Account fields.
         */
        
           // String profileId = attributes.get('User.ProfileId');
            if (create){
            Profile p = [SELECT Id FROM Profile WHERE Name= 'APHIS Applicant'];
           // u.ProfileId = p.Id;
            u.ProfileId = p.Id;
            insert u;
            
            string name = u.FirstName + ' ' + u.LastName;
            Id contactId = [select id from Contact where Name =: name].Id;
                if(attributes.containsKey('usdaemployeestatus') && attributes.get('usdaemployeestatus') != null) {
                    system.debug('agency code ' + attributes.get('usdaemployeestatus'));
                    string agencyCode = 'agency code ' + attributes.get('usdaemployeestatus');
                    agencyCode = agencyCode.substring(0, 1);
                    if(agencyCode == 'E'){
                        
                       // Case caseCreate = new Case();
                        
                       // Id queueId = [select Id from Group where Name = '   eAuth_Employee_Registration' and Type = 'Queue'].Id;

                       // create case and set queue as owner
               
                     //  caseCreate.Origin = 'Web';
                      // caseCreate.Reason = 'eAuth Employee User Account Created';
                       //caseCreate.ContactID = contactId;
                       //caseCreate.Description = 'The user, ' + u.Username + ', is an employee, which was setup through eAuth.';
                       //caseCreate.Subject = 'eAuth Employee request for ' + name;
                       //caseCreate.OwnerID = queueId;
                       //insert caseCreate;
                     }
               
                 }
            }
            
           
           
            
        /*if(attributes.containsKey('User.UserRoleId')) {
            String userRole = attributes.get('User.UserRoleId');
            UserRole r = [SELECT Id FROM UserRole WHERE Id=:userRole];
            u.UserRoleId = r.Id;
        }*/

    }

    private void handleContact(boolean create, String accountId, User u, Map<String, String> attributes) {
        Contact c;
        boolean newContact = false;
        if(create) {
            
                c = new Contact();
                newContact = true;
            
        
        }
      

        if(attributes.containsKey('usdaemail')) {
            c.Email = attributes.get('usdaemail');
        }
        if(attributes.containsKey('usdafirstname')) {
            c.FirstName = attributes.get('usdafirstname');
        }
        if(attributes.containsKey('usdalastname')) {
            c.LastName = attributes.get('usdalastname');
        }
        if(attributes.containsKey('usdahomephone')) {
            c.Phone = attributes.get('usdahomephone');
        }
        if(attributes.containsKey('MailingAddress')) {
            c.MailingStreet = attributes.get('MailingAddress');
            c.Mailing_Street_LR__c = c.MailingStreet;
        }
        if(attributes.containsKey('MailingCity')) {
            c.MailingCity = attributes.get('MailingCity');
            c.Mailing_City_LR__c = c.MailingCity;
        }
        if(attributes.containsKey('usdacountry')) {
          //  c.MailingCountry = attributes.get('usdacountry');
           
        }
        if(attributes.containsKey('MailingState')) {
            c.MailingState = [select id, Level_1_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('MailingState')].Name;
           
        }
        
        if(attributes.containsKey('MailingPostalCode')) {
            c.MailingPostalCode = attributes.get('MailingPostalCode');
            c.Mailing_Zip_Postalcode_LR__c = c.MailingPostalCode;
        }
        
        
        
        if(attributes.containsKey('usdahomephone')) {
            c.HomePhone = attributes.get('usdahomephone');
        }
        
        
        //c.Individual_Owner_Account__c = false;
        c.JITUserProvisioning__c = true;
        if(newContact) {
            c.AccountId = accountId;
            insert(c);
            u.ContactId = c.Id;
        } else {
           // update(c);
        }
    }

    private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        Account a;
        boolean newAccount = false;
        if(create) {
            if(attributes.containsKey('User.Account')) {
                String account = attributes.get('User.Account');
                a = [SELECT Id FROM Account WHERE Id=:account];
            } else {
                
                    a = new Account();
                    newAccount = true;
                
            }
        } 
        if (newAccount == true){
        
          a.Name = attributes.get('usdafirstname') + ' ' + attributes.get('usdalastname') + ' Account';
        
        
        if(attributes.containsKey('MailingAddress')) {
            a.BillingStreet = attributes.get('MailingAddress');
        }
        if(attributes.containsKey('MailingCity')) {
            a.BillingCity = attributes.get('MailingCity');
        }
        if(attributes.containsKey('MailingState')) {
            //a.BillingState = attributes.get('usdastate');
            system.debug(attributes.containsKey('MailingState'));
          
                a.BillingState =  [select id, Level_1_Name__c, Name from Level_1_Region__c where Level_1_Region_Code__c =: attributes.get('MailingState')].Name;
            
            
        }
        if(attributes.containsKey('usdacountry')) {
            //a.BillingCountry = attributes.get('usdacountry');
        }
        if(attributes.containsKey('MailingPostalCode')) {
            a.BillingPostalCode = attributes.get('MailingPostalCode');
        }
       
       
            a.Description = 'Community User Account';
        
        
        if(attributes.containsKey('usdaworkphone')) {
            a.Phone = attributes.get('usdaworkphone');
        }
        if(attributes.containsKey('MailingAddress')) {
            a.ShippingStreet = attributes.get('MailingAddress');
        }
        if(attributes.containsKey('MailingCity')) {
            a.ShippingCity = attributes.get('MailingCity');
        }
        if(attributes.containsKey('MailingState')) {
            a.ShippingState = attributes.get('MailingState');
        }
        if(attributes.containsKey('usdacountry')) {
            //a.ShippingCountry = attributes.get('usdacountry');
        }
        if(attributes.containsKey('MailingPostalCode')) {
            a.ShippingPostalCode = attributes.get('MailingPostalCode');
        }
        
        
           insert(a);
           return a.Id;
        }
        return null;
    }

    public void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        if(communityId != null || portalId != null) {
            String account = handleAccount(create, u, attributes);
            system.debug('string account ' + account);
            system.debug('attributes ' +attributes);
            system.debug('assertion '+assertion);
            system.debug('last name ' + attributes.get('usdalastname'));
            system.debug('agency code ' + attributes.get('usdaagencycode'));
            system.debug('fed id '+ attributes.get('usdaeauthid'));
            handleContact(create, account, u, attributes);
            handleUser(create, u, attributes, federationIdentifier, false);
           
           } else {
        system.debug('agency code ' + attributes.get('usdaemployeetypecode'));
            handleUser(create, u, attributes, federationIdentifier, true);
        }
    }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
        return u;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
       // User u = [SELECT Id, FirstName, ContactId FROM User WHERE Id=:userId];
       // handleJit(false, u, samlSsoProviderId, communityId, portalId,
         //   federationIdentifier, attributes, assertion);
    }
}