public class CARPOL_UNI_ApplicantAttachment {
// Class runs if an applicant attachment record is changed but NO new attachments are added.
 // These variables store Trigger.oldMap and Trigger.newMap
    Map<Id, Applicant_Attachments__c> oldAtt;
  Map<Id, Applicant_Attachments__c> newAtt;
  Set<ID> setLineIds = new Set<ID>();
 Map<ID, Schema.RecordTypeInfo> attrtMap = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosById();

// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_UNI_ApplicantAttachment(
   Map<Id, Applicant_Attachments__c> oldTriggerAtts, 
    Map<Id, Applicant_Attachments__c> newTriggerAtts) {
      oldAtt = oldTriggerAtts;
      newAtt = newTriggerAtts;
      system.debug('#######################      ' + newTriggerAtts + '     #############################');
  }
 
 
     public void ApplicantAttachments()  {
           for(Applicant_Attachments__c oldattVar : oldAtt.values())
           {
               decimal oldTotal = oldattVar.Total_Files__c;
                  for(Applicant_Attachments__c att : newAtt.values())
                   {
                                string attrt = attrtMap.get(att.RecordTypeId).getName();
                    system.debug('^^^^^^^^ att.Animal_Care_AC__c= ' + att.Animal_Care_AC__c + '  att.Total_Files__c= ' + att.Total_Files__c+ ' oldtotal=' + oldTotal +'  ^^^^^^^^^^^^^^^^^^^');
                    system.debug('+++attrt++'+attrt);
                          If(att.Animal_Care_AC__c != null && att.Total_Files__c > 0 && oldTotal == att.Total_Files__c && attrt == 'AC_Attachment'){
                              system.debug('^^^^^^^^ att.Animal_Care_AC__c= ' + att.Animal_Care_AC__c + ' ^^^^^^^^^^^^^^^^^^^');
                              AC__c getLine = [select ID from AC__c where id=:att.Animal_Care_AC__c];
                              getLine.Status__c = 'Saved';
                              getLine.Contains_Applicant_Attachments__c=True;
                              update getLine;
                           
                  }
                  else  If(att.Animal_Care_AC__c != null && att.Total_Files__c == 0 && oldTotal > att.Total_Files__c && attrt == 'AC_Attachment'){
                              system.debug('^^^^^^^^ All attachment was deleted:  att.Animal_Care_AC__c= ' + att.Animal_Care_AC__c + ' ^^^^^^^^^^^^^^^^^^^');
                              AC__c getLine = [select ID from AC__c where id=:att.Animal_Care_AC__c];
                              getLine.Status__c = 'Saved';
                              getLine.Contains_Applicant_Attachments__c=False;
                              update getLine;
                           
                  }
                  
               
           }         
    
    
        }
    
    }
    
    }