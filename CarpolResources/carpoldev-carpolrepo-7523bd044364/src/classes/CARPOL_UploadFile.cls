public with sharing class CARPOL_UploadFile {
    
    public List<Attachment> att = new List<Attachment>();
    private AC__c acLineItem = new AC__c();
    public String picURL { get; set;}
    public Integer iterator;
    public Attachment uploadedFile {
        get {
              if (uploadedFile == null)
                  uploadedFile = new Attachment();
              return uploadedFile;
        } set; }
       
    // Constructor    
    public CARPOL_UploadFile(ApexPages.StandardController controller) {
        
        if(ApexPages.currentPage().getParameters().get('ID') != null)
        {
            acLineItem = [SELECT ID, Name FROM AC__c WHERE ID = :ApexPages.currentPage().getParameters().get('ID') LIMIT 1]; 
            System.Debug('<<<<<<< AC Line Item : ' + acLineItem + ' >>>>>>>');
            getExisting();
        }
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an AC Line Item application to upload an image.'));}
        
    }
    
    public void getExisting()
    {
        System.Debug('<<<<<<< AC Line Item Name : ' + acLineItem.Name + ' >>>>>>>');
        att = [SELECT ID, Name FROM Attachment WHERE Description = 'Uploaded Image' AND ParentId = :acLineItem.ID ORDER BY CreatedDate DESC LIMIT 1];
        if(att.size() > 0)
        {
            picURL = '/servlet/servlet.FileDownload?file=' + att[0].ID;
        }
    }
    
    public PageReference uploadFile() {
 
    try 
    {
        String fileExtension = uploadedFile.Name;
        System.Debug('<<<<<<< Uploaded File Name : ' + fileExtension + ' >>>>>>>');
        
        if(uploadedFile.Name != null)
        {
            fileExtension = fileExtension.substring(fileExtension.lastIndexOf('.') + 1, fileExtension.length());
            fileExtension = fileExtension.toUpperCase();
            System.Debug('<<<<<<< File Size : ' + uploadedFile.BodyLength + ' >>>>>>>');
            System.Debug('<<<<<<< File Extension : ' + fileExtension + ' >>>>>>>');
            if(fileExtension == 'PNG' || fileExtension == 'JPG' || fileExtension == 'JPEG')
            {
                uploadedFile.OwnerId = UserInfo.getUserId();
                uploadedFile.ParentId = acLineItem.ID;
                uploadedFile.Description = 'Uploaded Image';
                insert uploadedFile;
                picURL = '/servlet/servlet.FileDownload?file=' + uploadedFile.ID;
                uploadedFile = new Attachment();
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully.'));
            }
            else{uploadedFile = new Attachment();ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Only .png, .jpeg, .jpg files are allowed.'));}
        }
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select an image to upload.'));}
        return null;
    }
    catch (DMLException e) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file. Please re-try.'));
        return null;
    }
  }
}