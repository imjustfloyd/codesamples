public class CARPOL_UNI_Update_RLD {
    
    public static void updateRLD(List <AC__c> LineList){
   
    if(LineList.size() !=0 && LineList[0].Status__c!='Submitted')
    {
        List <Related_Line_Decisions__c>  RLDList = [SELECT ID, Decision_Matrix__c, Line_Item__c FROM Related_Line_Decisions__c WHERE Line_Item__c =: LineList[0].ID];
        if(RLDList.size()!=0)
            {
                system.debug('RLD List'+RLDList);
                  delete RLDList;
            }
      
    }
    }
}