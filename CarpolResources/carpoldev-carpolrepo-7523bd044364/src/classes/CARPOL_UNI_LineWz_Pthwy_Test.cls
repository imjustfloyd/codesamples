@isTest(SeeAllData=true)
public class CARPOL_UNI_LineWz_Pthwy_Test {
     static testMethod void myUnitTest() { 
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
          CARPOL_TestDataManager dataObj = new CARPOL_TestDataManager ();
          Application__c  newApp =dataObj.newapp(ACAppRecordTypeId);
          
          Domain__c program = new Domain__c(name = 'ABCD');
          insert program;
          
          Program_Line_Item_Pathway__c newPathWay = new Program_Line_Item_Pathway__c (Program__c = program.id, name = 'ABCD Pathway');
          insert newPathWay;
          
          test.startTest();
              Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_LineWz_Pthwy')); 
              System.currentPageReference().getParameters().put('AppId',newApp.Id);
              System.currentPageReference().getParameters().put('ipwy',newApp.Id);
              
              CARPOL_UNI_LineWz_Pthwy obj = new CARPOL_UNI_LineWz_Pthwy();
              obj.getprogLineItemPathwayItems();
              obj.getNextChild();
              system.assert(obj != null);              
          test.stopTest();
     } 
     
     static testMethod void myUnitTest1() { 
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
          CARPOL_TestDataManager dataObj = new CARPOL_TestDataManager ();
          Application__c  newApp =dataObj.newapp(ACAppRecordTypeId);
          
          Domain__c program = new Domain__c(name = 'ABCD');
          insert program;
          
          Program_Line_Item_Pathway__c newPathWay = new Program_Line_Item_Pathway__c (Program__c = program.id, name = 'ABCD Pathway');
          insert newPathWay;
          
          test.startTest();
              Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_LineWz_Pthwy')); 
              System.currentPageReference().getParameters().put('AppId',newApp.Id);
              
              CARPOL_UNI_LineWz_Pthwy obj = new CARPOL_UNI_LineWz_Pthwy();
              system.assert(obj != null);              
          test.stopTest();
     }
}