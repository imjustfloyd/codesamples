@isTest(seealldata=true)
private class CARPOL_UNI_LineItemRLStatusClass_Test {
      @IsTest static void CARPOL_UNI_LineItemRLStatusClass_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();
          String ACLocRecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();                    
          String ACLoc1RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();                              
          String ACLoc2RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Release Sites Location').getRecordTypeId();                                        
          String ACLoc3RecordTypeId = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();                                                            

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);

          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          ac1.status__c = 'Waiting on Customer';

          update ac1;
          
          List<Applicant_Attachments__c> attList = new List<Applicant_Attachments__c>();
          RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'BRS Standard Permit SOP' AND SobjectType = 'Applicant_Attachments__c'];
          Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Status__c='Waiting on Customer', Animal_Care_AC__c = ac1.id, Authorization__c = objauth.Id, Total_Files__c = 1);
          attList.add(att);          
          
          RecordType atrt1  = [SELECT ID, Name FROM RecordType WHERE Name = 'BRS Notification SOP' AND SobjectType = 'Applicant_Attachments__c'];
          Applicant_Attachments__c att1 = new Applicant_Attachments__c(RecordTypeId = atrt1.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Status__c='Waiting on Customer', Animal_Care_AC__c = ac1.id, Authorization__c = objauth.Id, Total_Files__c = 1);
          attList.add(att1);
          insert attList;                    
          
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Construct Test';
          objcaj.Line_Item__c = ac1.id;
          objcaj.Status__c = 'Waiting on Customer';
          insert objcaj;   
          
          Design_Protocol_Record__c dpr = new Design_Protocol_Record__c();
          dpr.Status__c = 'Waiting on Customer';
          dpr.Associate_Application__c = objapp.id;
          dpr.Line_Item__c = ac1.id;
          insert dpr;     
                    
          Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
          wft.Status_Categories__c = 'Customer Feedback';
          wft.Update_Related_Record_to_Specific_Value__c = 'Test';
          update wft;
          Map<Id,Workflow_Task__c> mapId = new Map<Id,Workflow_Task__c>();
          mapId.put(wft.Id,wft);

          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;

          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          lr.Level_1_Name__c = 'State';
          lr.State_Name__c = 'Test';
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
          
          List<Location__c> listloc = new List<Location__c>();
          
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLocRecordTypeId
             );
            listloc.add(objloc); 
            
            Location__c objloc1 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc1RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc1);      
            
            Location__c objloc2 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc2RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc2);    
            
           Location__c objloc3 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLoc3RecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc3); 
            
           Location__c objloc4 = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c',
             Contact_Name1__c = 'Test', 
             Day_Phone__c = '555-555-1212',
             Line_Item__c = ac1.id,
             RecordTypeId = ACLocRecordTypeId,
             Status__c = 'Waiting on Customer'
             );
            listloc.add(objloc4);
            insert listloc;  


          Reviewer__c objrev = new Reviewer__c();
          objrev.Status__c= 'Open';
          objrev.Authorization__c = objauth.id;
          objrev.State_Regulatory_Official__c = objcont.id;
          objrev.BRS_State_Reviewer_Email__c = objcont.Email;          
          insert objrev;

          Test.startTest();
          
          CARPOL_UNI_LineItemRLStatusClass extclass = new CARPOL_UNI_LineItemRLStatusClass();
          extclass.updateStatus(wft);
          system.assert(extclass != null);          
                   
          Test.stopTest();     
      }
}