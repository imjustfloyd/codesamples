@isTest
public class CARPOL_UNI_LineAttachment_Test{
    static testMethod void testLineAttachment(){
    
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
        System.debug('<<<<<<<<<<<<<<<<<< UNI Line Attachment Test >>>>>>>>>>>>>>>>>>>>');
        Map<Id, Attachment> mapatt = new Map<Id, Attachment>();
        RecordType imprt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Applicant Contact' AND SobjectType = 'Applicant_Contact__c'];
        RecordType exprt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Applicant Contact' AND SobjectType = 'Applicant_Contact__c'];
        RecordType ddrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Applicant Contact' AND SobjectType = 'Applicant_Contact__c'];
        Applicant_Contact__c imp = new Applicant_Contact__c(First_Name__c = 'Ace', Name = 'Ventura', Email_Address__c = 'atest@test.com', RecordTypeId = imprt.Id);
        insert imp;
        Applicant_Contact__c exp = new Applicant_Contact__c(First_Name__c = 'Ace', Name = 'Ventura', Email_Address__c = 'atest@test.com', RecordTypeId = exprt.Id);
        insert exp;
        Applicant_Contact__c dd = new Applicant_Contact__c(First_Name__c = 'Ace', Name = 'Ventura', Email_Address__c = 'atest@test.com', RecordTypeId = ddrt.Id);
        insert dd;
        RecordType portrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Ports' AND SobjectType = 'Facility__c'];
        Facility__c f = new Facility__c(Name = 'Halifax', Type__c = 'Foreign Port', RecordTypeId = portrt.Id);
        insert f;
        Facility__c f1 = new Facility__c(Name = 'ABERDEEN-HOQUIAM, WA', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f1;
        Facility__c f2 = new Facility__c(Name = 'Test1', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f2;
        Facility__c f3 = new Facility__c(Name = 'Test2', Type__c = 'Domestic Port', RecordTypeId = portrt.Id);
        insert f3;
        RecordType rtp = [select ID from RecordType where sObjectType = 'Facility__c' AND Name = 'Ports'];
        Facility__c port = new Facility__c(Name = 'Test', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port; 
        Account acc = new Account(Name = 'Test1');
        insert acc;  
        Contact c = new Contact(FirstName = 'Test', LastName = 'John', Account = acc, Phone = '2323232312', Email = 'john@test.com', MailingState='Alabama' , MailingCity='null', MailingStreet = 'null', MailingCountry='United States' , MailingPostalCode='null');
        insert c;
        RecordType application  = [SELECT ID, Name FROM RecordType WHERE Name = 'Standard Application' AND SobjectType = 'Application__c'];
        Application__c app = new Application__c(Applicant_Name__c = c.id, RecordTypeId = application.Id, Application_Status__c = 'Open');
        insert app;
        RecordType authorization  = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Authorizations__c'];
        Breed__c b = new Breed__c(Name = 'test');
        insert b;
         Domain__c domain = new Domain__c(Name = 'AC');
        insert domain;
        
        Program_Prefix__c pp = new Program_Prefix__c(Name = 'test', Prefix_Status__c = 'Active', Program__c = domain.Id);
        insert pp;
        
        RecordType usert = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Intended_Use__c'];
        Intended_Use__c use = new Intended_Use__c(Name = 'Veterinary Treatment', RecordTypeId = usert.Id);
        insert use;
        
        RecordType sigrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Signature__c'];
        Signature__c sig = new Signature__c(Name = 'Test Signature', Program_Prefix__c = pp.Id, RecordTypeId = sigrt.Id, Intended_Use__c = use.Id, Treatment_available_in_Country_of_Origin__c = 'Yes');
        insert sig;
        
        RecordType regrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Regulation__c'];
        Regulation__c reg = new Regulation__c(RecordTypeId = regrt.Id, Custom_Name__c = 'Test', Short_Name__c = 'Test', Status__c = 'Active');
        insert reg;
        
        Authorizations__c auth = new Authorizations__c(Application__c = app.Id, RecordTypeId = authorization.Id, Thumbprint__c = sig.Id);
        insert auth;
        
         
        AC__c ac = new AC__c();
        ac.Application_Number__c = app.Id;
        ac.Breed__c = b.id;
        ac.Color__c = 'Brown';
        ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
        ac.Sex__c = 'Male';
        ac.Purpose_of_the_Importation__c = 'Veterinary Treatment';
        ac.Treatment_available_in_Country_of_Origin__c  = 'No';
        ac.Exporter_Last_Name__c = exp.id;
        ac.DeliveryRecipient_Last_Name__c = dd.id;
        ac.Importer_Last_Name__c = imp.id;
        ac.Port_of_Embarkation__c = f.id;
        System.debug('<<<<<<<<<<< Port of Embarkation at AC ' + ac.Port_of_Embarkation__c + '>>>>>>>>>>>>>>>>>>>');
        ac.Port_of_Entry__c = port.id;
        System.debug('<<<<<<<<<<< Port of Entry at AC ' + ac.Port_of_Entry__c + '>>>>>>>>>>>>>>>>>>>');
        ac.Transporter_Type__c = 'Ground';
        ac.Proposed_date_of_arrival__c = Date.newInstance(2015, 10, 17);
        ac.Arrival_Time__c = date.today()+15;
        ac.Authorization__c = auth.ID;
        ac.Status__c = 'Saved';
        ac.RecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Live Dogs').getRecordTypeId();
        //ac.Country_Of_Origin__c = newcountryus().Id;
        ac.Program_Line_Item_Pathway__c = plip.Id;            
        insert ac;
        
        RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
        Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = ac.Id);
        insert att;
        System.debug('<<<<<<<<<<<<<<<< ' + att + ' >>>>>>>>>>>>>>>>>>>>>>>>>>');
        
        Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
        insert n;
        mapatt.put(n.Id, n);
          CARPOL_UNI_LineAttachment ula = new CARPOL_UNI_LineAttachment(mapatt,mapatt);
          system.assert(ula != null);          
      //  ula.LineAttachment();
    }
}