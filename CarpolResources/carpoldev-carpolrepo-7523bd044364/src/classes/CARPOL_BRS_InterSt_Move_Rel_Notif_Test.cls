/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRS_InterSt_Move_Rel_Notif_Test
@CreateDate: 9/29/15
@Description: Test class for CARPOL_BRS_Interstate_Move_and_Rel_Notif 
@Version: 1.0
@reference: CARPOL_BRS_Interstate_Move_and_Rel_Notif (apex class)
*/
@istest
public class CARPOL_BRS_InterSt_Move_Rel_Notif_Test{
    public static testmethod void Test(){
    
     Apexpages.StandardController stdController = new Apexpages.StandardController(new Link_Regulated_Articles__c ()); 
     CARPOL_BRS_Interstate_Move_and_Rel_Notif  controller = new CARPOL_BRS_Interstate_Move_and_Rel_Notif (StdController);
     system.assert(controller != null);
    }
}