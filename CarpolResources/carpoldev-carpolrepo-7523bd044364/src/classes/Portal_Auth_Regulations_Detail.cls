public with sharing class Portal_Auth_Regulations_Detail {

     public Id authID{get;set;}
     public Id Id{get;set;}
     
     public  String AuthJuncRecordTypeId {get;set;}
     
     public List<Authorization_Junction__c> AssociatedRegulations;
     public List<Authorization_Junction__c> Regulations;
     public List<Authorization_Junction__c> AddInformation;
     public List<Authorization_Junction__c> CBPInformation;
     public List<Authorization_Junction__c> OriginalRegulations = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalAddInformation = new List<Authorization_Junction__c>();
     public List<Authorization_Junction__c> OriginalCBPInformation = new List<Authorization_Junction__c>();
     
     //--------------------------------BRS Related-------------------------------------------------------------------------------------------------// 
     public string regulationtype;
     public string program;
     public integer TotalAgree;
     public integer TotalRegs;
     public integer Totalunanswered;

    public Portal_Auth_Regulations_Detail(ApexPages.StandardController controller) {
    
         authID=ApexPages.currentPage().getParameters().get('id');
         Regulations = new List<Authorization_Junction__c>();
         AddInformation = new List<Authorization_Junction__c>();
         CBPInformation = new List<Authorization_Junction__c>();
        
         AuthJuncRecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();
         
         
         program = [select Program_Pathway__c,Program_Pathway__r.program__r.name from Authorizations__c where id =: authID].Program_Pathway__r.program__r.name;

    }
 
    //--------------------------------Standard/Import Requirements-------------------------------------------------------------------------//  
    public List<Authorization_Junction__c> getRegulations(){

        regulationtype = 'Import Requirements';
        
         if(program == 'BRS') { regulationtype = 'Standard'; }
        
         Regulations = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c,Regulation__c 
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: regulationtype
                                                        and Authorization__c =: authID order by Order_Number__c];
         system.debug('Regulations@@@'+Regulations);                                                    
         OriginalRegulations =  Regulations.deepClone();
         return Regulations;
       }
       
    //--------------------------------Additional Information/Supplemental Conditions(BRS)-------------------------------------------------//       
    public List<Authorization_Junction__c> getAddInformation(){
        
         regulationtype = 'Additional Information';
        
         if(program == 'BRS') { regulationtype = 'Supplemental Conditions'; }        
    
         AddInformation = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c 
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: regulationtype
                                                        and Authorization__c =: authID order by Order_Number__c];
         OriginalAddInformation  =  AddInformation.deepClone();
         
               return AddInformation;
          }     
          
    //--------------------------------CBI Regulations list-------------------------------------------------------------------------------------//        
    public List<Authorization_Junction__c> getCBPInformation(){
        
        
    
         CBPInformation = [SELECT ID, Name,Order_Number__c, Regulation_Short_Name__c, Is_Active__c, Regulation_Description__c, Applicant_Agree__c, Applicant_Comments__c 
                       FROM Authorization_Junction__c WHERE RecordTypeID =: AuthJuncRecordTypeId 
                                                        and Type__c =: 'Instruction for CBP Officers'
                                                        and Authorization__c =: authID order by Order_Number__c];
                                                        
              OriginalCBPInformation = CBPInformation.deepClone();
               return CBPInformation;
          }           

     //--------------------------------SAVE Button-------------------------------------------------------------------------------------// 
      public PageReference tosave() { 
 
          
            AssociatedRegulations = new List<Authorization_Junction__c>();
            
            for(integer i=0;i<Regulations.size();i++){
            
              if( OriginalRegulations[i].Applicant_Agree__c != Regulations[i].Applicant_Agree__c ||
                  OriginalRegulations[i].Applicant_Comments__c != Regulations[i].Applicant_Comments__c ){
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = Regulations[i].Id;
                    aj.Applicant_Agree__c = Regulations[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = Regulations[i].Applicant_Comments__c;
                    AssociatedRegulations.add(aj);
                  }
            
              }
              
              for(integer i=0;i<AddInformation.size();i++){
            
              if( OriginalAddInformation  [i].Applicant_Agree__c != AddInformation[i].Applicant_Agree__c ||
                  OriginalAddInformation  [i].Applicant_Comments__c != AddInformation[i].Applicant_Comments__c ){
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = AddInformation[i].Id;
                    aj.Applicant_Agree__c = AddInformation[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = AddInformation[i].Applicant_Comments__c;
                    AssociatedRegulations.add(aj);
                  }
            
              }
              
              
             for(integer i=0;i<CBPInformation.size();i++){
            
              if( OriginalCBPInformation[i].Applicant_Agree__c != CBPInformation[i].Applicant_Agree__c ||
                  OriginalCBPInformation[i].Applicant_Comments__c != CBPInformation[i].Applicant_Comments__c ){
                   Authorization_Junction__c aj = new Authorization_Junction__c();
                    aj.Id = CBPInformation[i].Id;
                    aj.Applicant_Agree__c = CBPInformation[i].Applicant_Agree__c;
                    aj.Applicant_Comments__c = CBPInformation[i].Applicant_Comments__c;
                    AssociatedRegulations.add(aj);
                  }
            
              }
            
            
            
             system.debug('##### AssociatedRegulations ### '+ AssociatedRegulations); 
             if(AssociatedRegulations.size()>0){
             update AssociatedRegulations;
             }
             PageReference RowEditing=new PageReference('/apex/Portal_Auth_Regulations_Detail?id='+authID);
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Saved Successfully!'));
             RowEditing.setRedirect(true); // 5-24-16 VV Uncommented
            return RowEditing;

       }
       
       //--------------------------------Submit Button-------------------------------------// 
        public PageReference submit() { 

            list<Authorizations__c> authToUpdate = [Select Id,Status__c from Authorizations__c where id=:authID limit 1];
            list <Ac__c> lineitem = new list<Ac__c>();
            list <Workflow_Task__c> WFT = new list<Workflow_Task__c>();  
         
             system.debug('authToUpdate@@@0'+authToUpdate);

             //VV added 8-4-16
             TotalAgree  = 0;
             TotalRegs =0;
             Totalunanswered =0;
            //-- Check if any Import Requirements or Standard conditons are not answered --// 
             for (Authorization_Junction__c rj:Regulations){
                 system.debug('rj.Applicant_Agree__c@@@0'+rj.Applicant_Agree__c);
                   if (rj.Applicant_Agree__c == 'Agree'){
                       TotalAgree++ ;
                       }
                    if (rj.Regulation__c != null){
                         TotalRegs++;
                       }
                    if (rj.Applicant_Agree__c == null || rj.Applicant_Agree__c == ''){
                         Totalunanswered++;
                       }  
             }
             
             system.debug('Totalunanswered@@@0'+Totalunanswered);
             
             //-- Check if any IAdditional Information or Supplemental conditons are not answered --//
             if(Totalunanswered == 0){
                 for (Authorization_Junction__c rj:AddInformation){
     
                        if (rj.Applicant_Agree__c == null || rj.Applicant_Agree__c == ''){
                             Totalunanswered++;
                           }  
                   }                 
                 
             }
             //-- Check if any CBI related Information regulations are not answered --//
             if(Totalunanswered == 0){
                 for (Authorization_Junction__c rj:CBPInformation){
     
                        if (rj.Applicant_Agree__c == null || rj.Applicant_Agree__c == ''){
                             Totalunanswered++;
                           }  
                   }                 
                 
             }             
   
            if(Totalunanswered > 0){
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please agree/disagree to all regulations.')); 
               return null;
            } 

               
             system.debug ('++++ TotalAgree = '+ TotalAgree);
             system.debug ('++++ TotalRegs = '+TotalRegs );             
         //    if (TotalRegs == TotalAgree)
          //    {
                 system.debug('++++++++++IN');
                 for(Ac__c li: [select id,status__c from AC__c where Authorization__c=:authToUpdate[0].id])
                         { 
                             li.status__c = 'Submitted';
                             lineitem.add(li);
                         }
                         update lineitem;
                         system.debug('lineitem###'+lineitem); 
                 for(Workflow_Task__c w: [select id, Status__c from Workflow_Task__c where Authorization__c =:authToUpdate[0].id and Status__c = 'Pending' and Status_Categories__c='Customer Feedback'])
                         { 
                             w.Status__c  = 'In Progress';
                             w.Status_Categories__c = ' ';
                             WFT.add(w);
                         }
                         update WFT; 
                         system.debug('WFT###'+WFT); 
                         
                        if(authToUpdate.size() >0){
                             authToUpdate[0].Status__c  = 'In Review';
                             system.debug('authToUpdate@@@1'+authToUpdate);
                             update authToUpdate;
                      }
                      system.debug('authToUpdate###'+authToUpdate);
                       
       //     }
             //   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Submitted Successfully!')); 
              //  return null;
 
                        
            PageReference RowEditing=new PageReference('/apex/Portal_Auth_Regulations_Detail?id='+authID);
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Submitted Successfully!'));
            RowEditing.setRedirect(false); // 5-24-16 VV Uncommented
            return RowEditing; 
 
       
       } 
      
}