/**
 * An apex page controller that exposes the site forgot password functionality
 */
@IsTest public with sharing class MyCustomLookupControllerTest{ 

     @IsTest(SeeAllData=false) public static void MyCustomLookupControllerTest() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
        
         
          
        // Instantiate a new controller with all parameters in the page
        MyCustomLookupController controller = new MyCustomLookupController();
    
        System.assert(controller != null); 
    }
    
}