@isTest(seealldata=true)
private class CreateAccountFromContactTest{
    static testMethod void testCreateAccountFromContact() {
    
    String ContRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Owner').getRecordTypeId();
    Contact objcont = new Contact();
      objcont.FirstName = 'FirstName';
      objcont.LastName = 'LastName';
      objcont.Email = 'test@email.com';
      objcont.MailingStreet = 'Mailing Street';
      objcont.MailingCity = 'Mailing City';
      objcont.MailingState = 'Ohio';
      objcont.MailingCountry = 'United States';
      objcont.MailingPostalCode = 'Mailing Postal Code';
      objcont.RecordTypeId = ContRecordTypeId;
      insert objcont;
      system.assert(objcont != null);                                   
    }
   }