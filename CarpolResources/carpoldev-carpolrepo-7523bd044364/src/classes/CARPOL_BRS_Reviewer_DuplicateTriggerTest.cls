@isTest(seealldata=true)
private class CARPOL_BRS_Reviewer_DuplicateTriggerTest {
    /**
    * CARPOL_BRS_Reviewer_DuplicateTriggerTest.class
    *
    * @author  Rajinikanth
    * @date   09/05/2015
    * @desc   Test method for CARPOL_BRS_Reviewer_DuplicateTrigger
    */ 
    static testmethod void testCARPOL_BRS_Reviewer_DuplicateTrigger() {
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
        String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
        String ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('State SPRO').getRecordTypeId();
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        objcont.RecordTypeId = ContactRecordTypeId;
        objcont.Contact_checkbox__c = true;
        update objcont;
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact();  
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port'); 
        Application__c objapp = testData.newapplication();
        Authorizations__c objauth = testData.newAuth(objapp.Id); 
        AC__c ac = testData.newlineitem('Personal Use',objapp);
        ac.Authorization__c = objauth.id;
        update ac; 
        AC__c ac3 = testData.newlineitem('Veterinary Treatment',objapp);
        ac3.Authorization__c = objauth.id;
        update ac3;               
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
        Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');
        Attachment att1 = testData.newattachment(ac.Id);  
        Attachment att2 = testData.newattachment(ac.Id);  
        Attachment att3 = testData.newattachment(ac.Id);                     

        Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
        Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
        Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
        Trade_Agreement__c TradeAgreement = testData.newTA(); 
        Country__C Country = testData.newCountryus();
        Level_1_Region__c State = testData.newlevel1regionmd();
        //objcont.State__c = State.Id;
        //objcont.Contact_checkbox__c  = true;
        //update objcont;
        VS__c VS = testData.newVS(objcont.Id, objapp.Id);
        Reviewer__c Reviewer = new Reviewer__c();
        Reviewer.Authorization__c = objauth.Id;
        Reviewer.State_Regulatory_Official__c = objcont.Id;
        insert Reviewer;
        system.assert(reviewer != null);
        try{
            Reviewer = new Reviewer__c();
            Reviewer.Authorization__c = objauth.Id;
            Reviewer.State_Regulatory_Official__c = objcont.Id;
            insert Reviewer;
        } catch (DMLException e) {
            
        }
        
    }
}