public class CARPOL_BRS_StateRecordsExt {

    public Authorizations__c Authorization;
    public string outputpanel{get; set;}
    private ID AuthorizationID = ApexPages.currentPage().getParameters().get('ID');
    public Authorizations__c auth = [SELECT ID,Name,BRS_Create_State_Review_Records__c,BRS_State_Review_Notification__c,RecordType.DeveloperName FROM Authorizations__c WHERE Id =: AuthorizationID];

    public CARPOL_BRS_StateRecordsExt(ApexPages.StandardController controller) {
                    this.AuthorizationID= ApexPages.currentPage().getParameters().get('ID');
    }

      /*     public void CreateStateRecords(){
                try{
                        System.debug('Auth Details : ' + auth);
                        Authorizations__c auth1 = [SELECT  Recordtype.Name FROM Authorizations__c WHERE Id =: AuthorizationID];
                        if(auth.RecordType.DeveloperName == 'Biotechnology_Regulatory_Services_Standard_Permit' && auth.BRS_Create_State_Review_Records__c == false ){
                                System.debug('Entered the loop with all conditions good');
                                List<AC__c> lineitem = [SELECT ID, Name FROM AC__c WHERE Authorization__c = :AuthorizationID];
                                integer loctcount = [select count() from Location__c  where Line_Item__c =:lineitem[0].Id AND (Recordtype.DeveloperName ='Destination_Location' OR Recordtype.DeveloperName = 'Origin_and_Destination' OR Recordtype.DeveloperName = 'Release_Location')];
                                if(loctcount>0){
                                        System.debug('Entered loop for locations ');
                                        list<string> stateids = new list<string>();
                                        stateids.clear();
                                        list<Reviewer__c> lstrev = new list<Reviewer__c>();
                                        list<Location__c> loctlst = [select id,Name,State__c,State__r.Name,Authorization__c from Location__c where Line_Item__c =:lineitem[0].Id AND (Recordtype.DeveloperName ='Destination_Location' OR Recordtype.DeveloperName = 'Origin_and_Destination' OR Recordtype.DeveloperName = 'Release_Location')];
                                        for(Location__c l:loctlst){
                                                if(l.State__c!=null){
                                                stateids.add(l.State__c);
                                                System.debug('State IDs : ' + stateids);
                                                } // End of loop if(l.State__c!=null)
                                         } // End of loop for(Location__c l:loctlst)

                                        if(stateids.size()>0){
                                                List<RecordType> contrtId = [SELECT ID FROM RecordType WHERE Name = 'State SPRO' AND sObjectType = 'Contact'];
                                                integer sprocount = [select count() from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId[0].id ];
                                                if(sprocount>0){
                                                    System.debug('Got contacts');
                                                    list<contact> sprolst = [select id,Name,Contact_checkbox__c,State__c,Email from contact where Contact_checkbox__c=:true and Recordtypeid=:contrtId and State__c in:stateids ];
                                                    System.debug('Contact details : ' + sprolst);
                                                    for(Contact c:sprolst )
                                                    {
                                                         Reviewer__c objrev = new Reviewer__c();
                                                         objrev.Status__c= 'Open';
                                                         objrev.Authorization__c = AuthorizationID;
                                                         objrev.State_Regulatory_Official__c = c.id;
                                                         objrev.BRS_State_Reviewer_Email__c = c.Email;
                                                         lstrev.add(objrev);
                                                         System.debug('List of State Records to be inserted : ' + lstrev);

                                                     } // End of loop for(Contact c:sprolst )
                                                     if(lstrev.size()>0){
                                                     System.debug('State records to be inserted is entered.');
                                                     insert lstrev;
                                                     auth.BRS_Create_State_Review_Records__c = true;
                                                     //auth.BRS_State_Review_Notification__c = true;
                                                     update auth;
                                                     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'State records are created.'));
                                                     }// End of loop if(lstrev.size()>0)
                                                }// End of loop if(sprocount>0)
                                        }//End of loop if(stateids.size()>0)
                                    }
                                    } //End of starting loop

                                    else{
                                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'State Review Records are already Created.'));
                                    } // End of loop else


                     }
                catch(Exception e){
                        System.debug(e);
                }
          }  */

          public void createStateLetterAttachment(){
                List<Reviewer__c> revlst1 = [SELECT ID,State_Letter_Attached__c FROM Reviewer__c WHERE Authorization__c = :AuthorizationID];
                //List<Authorizations__c> revlst1 = revlst;
                                                             if(revlst1.size()>0){
                                                                     for(Reviewer__c r : revlst1){
                                                                        if(r.State_Letter_Attached__c == false){
                                                                             System.debug('Entered for loop to attach the state letter');
                                                                             Blob attbody = null;
                                                                             PageReference PdfPage = new PageReference('/apex/CARPOL_BRS_StateLetter?ID='+r.Id);
                                                                             PdfPage.setRedirect(true);
                                                                             if (!Test.isRunningTest()){
                                                                                    attbody = PdfPage.getContent();
                                                                            }
                                                                            else{
                                                                                    attbody = Blob.valueof('Some random String');
                                                                            }
                                                                             attachment att = new attachment();
                                                                             //attbody = PDFPage.getContent();
                                                                             System.debug('State letter content : ' + attbody);
                                                                             att.Body = attbody;
                                                                             att.Name = 'State_Letter_'+ System.TODAY().format()+ '.pdf';
                                                                             att.ParentId = r.ID;
                                                                             att.IsPrivate = false;
                                                                             System.debug('Checking for reviewer record id : ' + r.id);
                                                                             att.contentType = 'application/pdf';
                                                                             insert att;
                                                                             ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'State Letters are generated and attached.'));
                                                                             r.State_Letter_Attached__c = true;
                                                                             update r;
                                                                         }
                                                                         else{
                                                                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'State Letters are already attached.'));
                                                                         }
                                                                    }    // End of loop for(Reviewer__c r : lstrev)
                                                             }
          }
          
       /*   public void sendNotification(){
            try{
                List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                List<Authorizations__c> authlst = [SELECT ID, Bio_Tech_Reviewer__c,Name, RecordType.DeveloperName,BRS_State_Review_Notification__c FROM Authorizations__c WHERE ID = :AuthorizationID];
                Set<ID> userids = new Set<Id>();
                if(auth.BRS_State_Review_Notification__c == false){
                            for(Authorizations__c auth : authlst){
                                    userids.add(auth.Bio_Tech_Reviewer__c);
                            LIst<User> usrlst = [SELECT Id,Email FROM User WHERE ID IN :userids AND IsActive = True];
                            List<String> str = new List<String>();
                            List<Id> usridlst = new List<Id>();
                            for(User U : usrlst){
                                    str.clear();
                                    str.add(U.Email);
                                    System.debug('Str List has the following : ' + str);
                                    usridlst.clear();
                                    usridlst.add(U.Id);
                                    System.debug('The new list with User IDs has the following : ' + usridlst);
                            }
                            for(integer i = 0; i < str.size(); i++){
                                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                                    ID templateId = [select id from EmailTemplate where developername = 'BRS_Standard_Permit_Notify_State_Reviewers'].id;
                                    String [] toAddress = str;
                                    email.setToAddresses(toAddress);
                                    email.setTemplateID(templateId);
                                    email.saveAsActivity = false;
                                    email.setTargetObjectId(usridlst[i]);
                                    System.debug('The following email ids are captured to send email : ' + str);
                                    emails.add(email);
                                }
                                auth.BRS_State_Review_Notification__c = true;
                                update auth;
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Notification is sent.'));
                            }
                            if(emails.size() > 0) {
                                Messaging.sendEmail(emails);
                            }
                        }else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Notification is already sent.'));
                        }

            }

        catch(Exception e){
            System.debug(e);
        }
} */
}