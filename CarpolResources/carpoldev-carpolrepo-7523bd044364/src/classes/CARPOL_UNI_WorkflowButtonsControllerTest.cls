@isTest(seealldata=true)
private class CARPOL_UNI_WorkflowButtonsControllerTest {
    static testMethod void CARPOL_UNI_WorkflowButtonsControllerTest() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Application__c objapp = testData.newapplication();
      AC__c ac1 = testData.newlineitem('Personal Use',objapp);
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac1.Authorization__c = objauth.id;
      update ac1;
      objauth.Authorization_Type__c = 'Permit';
      objauth.Date_Issued__c = Date.today();
      update objauth;
      Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
      wft.buttons__c = 'NEPA Form,Edit Cover Letter,Send Email,Create State Review Records,Edit Permit,Edit Permit Conditions,View Application,Conditions Collaboration';
      update wft;
       Incident__c incident = new Incident__c();
        incident.Requester_Name__c='testReqName';
        insert incident;
        wft.Incident_Number__c=incident.id;
        update wft;
      Test.startTest();
      PageReference pageRef = Page.WFTaskButtons;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(wft);
      
      ApexPages.currentPage().getParameters().put('Id',wft.id);
      CARPOL_UNI_WorkflowButtonsController extclass = new CARPOL_UNI_WorkflowButtonsController(sc);
      system.assert(extclass != null);
    
      Test.stopTest();
      }
}