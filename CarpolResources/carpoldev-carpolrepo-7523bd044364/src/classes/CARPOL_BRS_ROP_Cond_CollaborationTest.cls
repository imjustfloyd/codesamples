@isTest(SeeAllData=true)
private class CARPOL_BRS_ROP_Cond_CollaborationTest {
    static testMethod void testCARPOL_BRS_ROP_Cond_Collaboration() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);
      Regulation__c objreg1 = testData.newRegulation('Standard','');
      Regulation__c objreg2 = testData.newRegulation('Supplemental Conditions','Release SC Plants - Multiyear Permit');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements'); 
      Regulation__c objreg4 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Attachment attach = testData.newattachment(ac.Id);           
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac.Authorization__c = objauth.Id;
      update ac;
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
 	  Workflow_Task__c objWF = testData.newworkflowtask('Process Authorization', objauth, 'Complete');
 	  
      String BRSTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
      String authRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
      String ReguRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
      objreg4.RecordTypeid = ReguRecordTypeId;
      update objreg4;
      Domain__c objprog = testData.newProgram('BRS');
       Program_Prefix__c objPrefix = new Program_Prefix__c();
     objPrefix.Program__c = objprog.Id;
     objPrefix.Name = '101';
     objPrefix.Permit_PDF_Template__c = 'CARPOL_BRS_StandardPermit';
     insert objPrefix;
       Signature__c objTP = new Signature__c();
      objTP.Name = 'Test BRS KK';
      objTP.Recordtypeid = BRSTPRecordTypeId ;
      objTP.Program_Prefix__c = objPrefix.Id;
      insert objTP; 
      
      /*User usershare = new User();
      usershare.Username ='aphistestemail@test.com';
      usershare.LastName = 'APHISTestLastName';
      usershare.Email = 'APHISTestEmail@test.com';
      usershare.alias = 'APHtest';
      usershare.TimeZoneSidKey = 'America/New_York';
      usershare.LocaleSidKey = 'en_US';
      usershare.EmailEncodingKey = 'ISO-8859-1';
      usershare.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;
      usershare.LanguageLocaleKey = 'en_US';
      insert usershare;
      
      User usershare1 = new User();
      usershare1.Username ='aphistestemail@test1.com';
      usershare1.LastName = 'APHISTestLastName1';
      usershare1.Email = 'APHISTestEmail@test1.com';
      usershare1.alias = 'APHtest1';
      usershare1.TimeZoneSidKey = 'America/New_York';
      usershare1.LocaleSidKey = 'en_US';
      usershare1.EmailEncodingKey = 'ISO-8859-1';
      usershare1.ProfileId = [select id from Profile where Name='BRS Enforcement Officer'].Id;
      usershare1.LanguageLocaleKey = 'en_US';
      insert usershare1;    
      
      User usershare2 = new User();
      usershare2.Username ='aphistestemail@test2.com';
      usershare2.LastName = 'APHISTestLastName2';
      usershare2.Email = 'APHISTestEmail@test2.com';
      usershare2.alias = 'APHtest2';
      usershare2.TimeZoneSidKey = 'America/New_York';
      usershare2.LocaleSidKey = 'en_US';
      usershare2.EmailEncodingKey = 'ISO-8859-1';
      usershare2.ProfileId = [select id from Profile where Name='BRS ROP Reviewer'].Id;
      usershare2.LanguageLocaleKey = 'en_US';
      insert usershare2; */   
          
      
      
      Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_ROP_Cond_Collaboration;
	      Test.setCurrentPage(pageRef);
	      ApexPages.StandardSetController sc = new ApexPages.StandardSetController(new List<Authorizations__c>());
	      ApexPages.currentPage().getParameters().put('Id',objauth.id);
	      ApexPages.currentPage().getParameters().put('wfid',objWF.id);
	      CARPOL_BRS_ROP_Cond_Collaboration acepreg = new CARPOL_BRS_ROP_Cond_Collaboration(sc);
	      acepreg.save();
	      acepreg.getFilteredConditions();
	      acepreg.savesuppconditions();
	      acepreg.redirect();
	      acepreg.redirectPermitPage();
	      List<Authorization_Junction__c> lstAJ = acepreg.getAddInformation();
	      List<Authorization_Junction__c> lstAJ1 = acepreg.getRegulations();   
	      System.assert(acepreg != null);
         
      
      /*system.runAs(usershare)
      {
	      PageReference pageRef1 = Page.CARPOL_BRS_ROP_Cond_Collaboration;
	      Test.setCurrentPage(pageRef1);
	      ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(new List<Authorizations__c>());
	      ApexPages.currentPage().getParameters().put('Id',objauth.id);
	      ApexPages.currentPage().getParameters().put('wfid',objWF.id);
	      CARPOL_BRS_ROP_Cond_Collaboration acepreg1 = new CARPOL_BRS_ROP_Cond_Collaboration(sc1);
	      acepreg1.save();
	      acepreg1.savesuppconditions();
	      acepreg1.redirect();
	      acepreg1.redirectPermitPage(); 
	           
	      System.assert(acepreg1 != null);
      }   
      
      system.runAs(usershare2)
      {
	      PageReference pageRef2 = Page.CARPOL_BRS_ROP_Cond_Collaboration;
	      Test.setCurrentPage(pageRef2);
	      ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(new List<Authorizations__c>());
	      ApexPages.currentPage().getParameters().put('Id',objauth.id);
	      ApexPages.currentPage().getParameters().put('wfid',objWF.id);
	      CARPOL_BRS_ROP_Cond_Collaboration acepreg2 = new CARPOL_BRS_ROP_Cond_Collaboration(sc2);
	      acepreg2.save();
	      acepreg2.savesuppconditions();
	      acepreg2.redirect();
	      acepreg2.redirectPermitPage();      
	      System.assert(acepreg2 != null);
      }   */
    Test.stopTest();
    }
}