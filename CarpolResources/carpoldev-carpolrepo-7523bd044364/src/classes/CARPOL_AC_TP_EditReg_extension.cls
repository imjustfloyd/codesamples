public class CARPOL_AC_TP_EditReg_extension {

    public List<Signature_Regulation_Junction__c> scj {get;set;}
    public List<Signature_Regulation_Junction__c> scjA {get;set;}
    public List<Signature_Regulation_Junction__c> scjB {get;set;}
    public List<Signature_Regulation_Junction__c> scjC {get;set;}
    public Integer index { get; set; }
    
    public Regulation__c regulation { get; set; }
    public Group__c regulationGroup { get; set; }
    public Group_Junction__c regulationGroupLookup { get; set; }
    public SelectOption[] allOptions { get; set; }
    public SelectOption[] selectedOptions { get; set; }
    public String selectedInputType { get; set; }
    public String leftLabel { get; set; }
    public String rightLabel { get; set; }
    public String signatureID = ApexPages.currentPage().getParameters().get('id');
    public Map<String, String> mapAllSavedRegulations = new Map<String, String>();
    public Map<String, String> mapAllSavedRegulationsGroup = new Map<String, String>();
    public List<Regulation__c> lstSavedRegulations = new List<Regulation__c>();
    public List<Group__c> lstSavedRegulationsGroup = new List<Group__c>();
    public String selectedType;
    public String selectedSubType;
    public Boolean showPopUpMessage;
    private Signature__c au;
    
    public String regulationType { get; set; }
    
    public String regACRecordTypeId { get; set; }
    
    public CARPOL_AC_TP_EditReg_extension(ApexPages.StandardController controller) {
        
        System.Debug('<<<<<<< Standard Controller Constructor Begins >>>>>>>');
        getRegulation();
        
        //popup add regulations
        regulation = new Regulation__c();
        this.au= (Signature__c)controller.getRecord();
        if (au.Id!=null){
            au = [Select Id, Name, RecordType.Name, Program_Prefix__c FROM Signature__c Where Id =:au.Id];
         }
        showPopUpMessage = false;
        selectedInputType = 'Test';
        leftLabel= 'Available Regulations';
        rightLabel = 'Selected Regulations';        
        regACRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        regulationGroupLookup = new Group_Junction__c();
        allOptions = new List<SelectOption>();
        List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regACRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :au.Id) Order By Custom_Name__c ASC LIMIT 999];
        for (Regulation__c rgltn : regulations ) 
        {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
        
        selectedOptions = new List<SelectOption>();
        List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :au.Id) LIMIT 999];
        lstSavedRegulations = slctdRgltns;
        System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
        for (Regulation__c rgltn : slctdRgltns)
        {
            selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
            mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
        }
    }
    
    // Method for testing a hidden field functionality 
    public PageReference setRegulationType(){
        System.debug('regulationType: ' + regulationType);
        regulation.Type__c = regulationType;
        getResults();
        return null;   
    }
    
    //get regulations
    public void getRegulation()
    {
        ID tpID = ApexPages.currentPage().getParameters().get('id');
        scj = new List<Signature_Regulation_Junction__c>([Select id, Signature__c, Order_Number__c, Regulation__r.Id, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Signature_Regulation_Junction__c where Signature__c = :tpID ORDER BY Order_Number__c ASC LIMIT 999]);
        System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
        
        scjA = new List<Signature_Regulation_Junction__c>();
        scjB = new List<Signature_Regulation_Junction__c>();
        scjC = new List<Signature_Regulation_Junction__c>();
        
        if(scj.size()>0){
            for ( Signature_Regulation_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements'){scjA.add(SCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(SCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Instruction for CBP Officers'){scjC.add(SCJ);}
                System.Debug('<<<<<<< Total IO : ' + scjc.size() + ' >>>>>>>');
            }
        }
    }
    
//add regulations
public PageReference getResults(){
        try
        {
            System.Debug('<<<<<<< Getting Updated List >>>>>>>');
            showPopUpMessage = false;
            selectedType = regulation.Type__c;
            System.Debug('<<<<<<< Selected Type : ' + selectedType + ' >>>>>>>');
            selectedSubType = regulation.Sub_Type__c;
            System.Debug('<<<<<<< Selected Sub Type : ' + selectedSubType + ' >>>>>>>');
            Boolean flagWhereClause = false;
            
            if(selectedInputType != 'Group')
            {
                String allOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId=:regACRecordTypeId AND ';
                String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
                
                if(selectedType != null){
                    allOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    flagWhereClause = true;
                }
                if(selectedSubType != null){
                    if(flagWhereClause == true){
                        allOptionsQuery += 'AND ';
                        selectedOptionsQuery += 'AND ';
                    }
                    allOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    flagWhereClause = true;
                }
                
                if(flagWhereClause == true){
                    allOptionsQuery += 'AND ';
                    selectedOptionsQuery += 'AND ';
                }
                allOptionsQuery += 'ID NOT IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = \'' + signatureID + '\') Order By Custom_Name__c ASC LIMIT 999';
                selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = \'' + signatureID + '\') LIMIT 999';
                
                System.Debug('<<<<<<< Query - All Regulations : ' + allOptionsQuery + ' >>>>>>>');
                System.Debug('<<<<<<< Query - Saved Regulations : ' + selectedOptionsQuery + ' >>>>>>>');
                allOptions = new List<SelectOption>();
                List<Regulation__c> regulations = Database.query(allOptionsQuery);
                for (Regulation__c rgltn : regulations) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
                
                selectedOptions = new List<SelectOption>();
                List<Regulation__c> slctdRgltns = Database.query(selectedOptionsQuery);
                lstSavedRegulations = slctdRgltns;
                System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
                for (Regulation__c rgltn : slctdRgltns )
                {
                    //selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                    selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c));
                    mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
                }
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
    
    public PageReference updateSignature() {
        
        try{
            if(selectedInputType != 'Group')
            {
                updateRegulations();
                //getRegulation();
                //showPopUpMessage = true;
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
                
                PageReference pageRef = Page.CARPOL_AC_Thumbprint_EditRegulations;
                pageRef.getParameters().put('Id',au.Id);
                pageRef.setRedirect(true);
                return pageRef;
                return null;
                 
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
        
    }
    
    public void updateRegulations() {
        
        System.Debug('<<<<<<< Updating Regulations on Signature >>>>>>>');
        try
        {
            System.Debug('<<<<<<< Add - All Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Add - Selected Regulations : ' + selectedOptions.size() + ' >>>>>>>');
            List<Signature_Regulation_Junction__c> regulationsToBeAdded = new List<Signature_Regulation_Junction__c>();
            List<Signature_Regulation_Junction__c> regulationsToBeRemoved = new List<Signature_Regulation_Junction__c>();
            Signature_Regulation_Junction__c sigRegulationMapp;
            
            // Logic to add Regulations and update the list
            for (SelectOption selReg : selectedOptions) {
                if(mapAllSavedRegulations.get(selReg.getValue()) == null)
                {
                    System.Debug('<<<<<<< New Regulation Found >>>>>>>');
                    sigRegulationMapp = new Signature_Regulation_Junction__c();
                    RecordType rt = [SELECT ID FROM RecordType WHERE Name = 'Singature_Regulation' AND SObjectType = 'Signature_Regulation_Junction__c' LIMIT 1];
                    sigRegulationMapp.RecordTypeID = rt.ID;
                    sigRegulationMapp.Signature__c = signatureID;
                    sigRegulationMapp.Regulation__c = selReg.getValue();
                    regulationsToBeAdded.add(sigRegulationMapp);
                }
            }
            System.Debug('<<<<<<< Total New Regulations to be Added : ' + regulationsToBeAdded.size() + ' >>>>>>>');
            insert(regulationsToBeAdded);
            
            // Update the saved result set for regulations
            Boolean flagWhereClause = false;
            String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
            
            if(selectedType != null){
                selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                flagWhereClause = true;
            }
            if(selectedSubType != null){
                if(flagWhereClause == true){
                    selectedOptionsQuery += 'AND ';
                }
                selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                flagWhereClause = true;
            }
            if(flagWhereClause == true){
                selectedOptionsQuery += 'AND ';
            }
            selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = \'' + signatureID + '\') LIMIT 999';
            System.Debug('<<<<<<< Query - Updated Saved Regulations : ' + selectedOptionsQuery + ' >>>>>>>');
            lstSavedRegulations = Database.query(selectedOptionsQuery);
            
            // Logic to remove regulations and update the list
            String strRemoveSignatureRegulationJunctionIds = '(';
            System.Debug('<<<<<<< Remove - Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Remove - Selected Regulations : ' + selectedOptions.size() + ' >>>>>>>');
            if(selectedOptions.size() == 0)
            {    
                for (Regulation__c svdRegulation : lstSavedRegulations) {
                    strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(svdRegulation.ID) + '\',';
                }
            }
            else
            {
                for(Integer i = 0; i < lstSavedRegulations.size(); i++)
                {
                    Integer matchFound = 0;
                    for(Integer j = 0; j < selectedOptions.size(); j++)
                    {
                        if(lstSavedRegulations[i].ID == selectedOptions[j].getValue())
                        {
                            matchFound += 1;
                            break;
                        }
                    }
                    if(matchFound == 0)
                    {
                        strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(lstSavedRegulations[i].ID) + '\',';
                    }
                }
            }
            if(strRemoveSignatureRegulationJunctionIds != '(')
            {
                strRemoveSignatureRegulationJunctionIds = strRemoveSignatureRegulationJunctionIds.substring(0, strRemoveSignatureRegulationJunctionIds.length() - 1);
                strRemoveSignatureRegulationJunctionIds += ')';
                System.Debug('<<<<<<< Regulations to be Removed : ' + strRemoveSignatureRegulationJunctionIds + ' >>>>>>>');
                String queryTemp = 'SELECT ID FROM Signature_Regulation_Junction__c WHERE Regulation__c IN ' + strRemoveSignatureRegulationJunctionIds + ' AND Signature__c = :signatureID LIMIT 999';
                regulationsToBeRemoved = Database.query(queryTemp);
                System.Debug('<<<<<<< Signature_Regulation_Junction__c WHERE Signature__c to be Removed : ' + regulationsToBeRemoved + ' >>>>>>>');
                delete(regulationsToBeRemoved);
            }
            
            if(regulationsToBeAdded.size() != 0 || regulationsToBeRemoved.size() != 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
                //regulationsToBeAdded.clear();      
            }
            //regulationsToBeAdded.clear();
            //System.Debug('<<<<<<< RegulationsToBeAdded Size : ' + regulationsToBeAdded.size() + ' >>>>>>>');    
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
    }
    
    public List<SelectOption> getAddByOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Individually','Individually'));
        options.add(new SelectOption('Group','By Group'));
        return options;
    }
    
    public PageReference selectInput() {
        System.Debug('<<<<<<< Selected Input Type : ' + selectedInputType + ' >>>>>>>');
        if(false){}
        else
        {
            //displayGroupInputs = false;
            leftLabel = 'Available Regulations';
            rightLabel = 'Selected Regulations';
            allOptions = new List<SelectOption>();
            List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regACRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            for (Regulation__c rgltn : regulations ) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
            
            selectedOptions = new List<SelectOption>();
            List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            lstSavedRegulations = slctdRgltns;
            System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            for (Regulation__c rgltn : slctdRgltns)
            {
                selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
            }
        }
        //System.Debug('<<<<<<< Display Group Input Type :  : ' + displayGroupInputs + ' >>>>>>>');
        return null;
    }

/*public PageReference createNewRegulation(){
    PageReference pageRef = Page.CARPOL_AC_Create_RegulationOnAuth;
    pageRef.getParameters().put('Id',au.Id);
    pageRef.setRedirect(true);
    return pageRef; 
}*/

}