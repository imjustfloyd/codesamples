@isTest(seealldata=true)
public class CARPOL_AC_InsertRegulationJunction_Test{
    static testMethod void insertRegJunction(){
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c b = testData.newbreed(); 
          Applicant_Contact__c exp = testData.newappcontact(); 
          Applicant_Contact__c dd = testData.newappcontact();
          Applicant_Contact__c imp = testData.newappcontact();          
          Facility__c port = testData.newfacility('Domestic Port');  
          Facility__c f = testData.newfacility('Foreign Port');
          Application__c app = testData.newapplication();
      
        Domain__c domain = new Domain__c(Name = 'AC');
        insert domain;
        
        Program_Prefix__c pp = new Program_Prefix__c(Name = 'test', Prefix_Status__c = 'Active', Program__c = domain.Id);
        insert pp;
        
        RecordType usert = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Intended_Use__c'];
        Intended_Use__c use = new Intended_Use__c(Name = 'Veterinary Treatment', RecordTypeId = usert.Id);
        insert use;
        
        RecordType sigrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Signature__c'];
        Signature__c sig = new Signature__c(Name = 'Test Signature', Program_Prefix__c = pp.Id, RecordTypeId = sigrt.Id, Intended_Use__c = use.Id, Treatment_available_in_Country_of_Origin__c = 'Yes');
        insert sig;
        
        RecordType regrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Regulation__c'];
        Regulation__c reg = new Regulation__c(RecordTypeId = regrt.Id, Custom_Name__c = 'Test Regulation', Short_Name__c = 'Test', Status__c = 'Active');
        insert reg;
        
        Signature_Regulation_Junction__c srj = new Signature_Regulation_Junction__c(Regulation__c = reg.Id, Signature__c = sig.Id);
        insert srj;
        
        Authorizations__c auth = testData.newAuth(app.Id); 
            
        AC__c ac = testData.newLineItem('Personal Use',app,auth);
        
        System.debug ('<<<<<<<<<<<<<<<<<<<< Animal Care ' + ac + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
        Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = ac.Id, Total_Files__c = 1);
        insert att;
        
        Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
        insert n;
        System.assert(n != null);
    }
    
    static testMethod void updateRegJunction(){
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c b = testData.newbreed(); 
          Applicant_Contact__c exp = testData.newappcontact(); 
          Applicant_Contact__c dd = testData.newappcontact();
          Applicant_Contact__c imp = testData.newappcontact();          
          Facility__c port = testData.newfacility('Domestic Port');  
          Facility__c f = testData.newfacility('Foreign Port');
          Application__c app = testData.newapplication();
        
        RecordType authorization  = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Authorizations__c'];
       
        Domain__c domain = new Domain__c(Name = 'AC');
        insert domain;
        
        Program_Prefix__c pp = new Program_Prefix__c(Name = 'test', Prefix_Status__c = 'Active', Program__c = domain.Id);
        insert pp;
        
        RecordType usert = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Intended_Use__c'];
        Intended_Use__c use = new Intended_Use__c(Name = 'Veterinary Treatment', RecordTypeId = usert.Id);
        insert use;
        
        RecordType sigrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Signature__c'];
        Signature__c sig = new Signature__c(Name = 'Test Signature', Program_Prefix__c = pp.Id, RecordTypeId = sigrt.Id, Intended_Use__c = use.Id, Treatment_available_in_Country_of_Origin__c = 'Yes');
        insert sig;
        
        RecordType regrt = [SELECT ID, Name FROM RecordType WHERE Name = 'Animal Care (AC)' AND SobjectType = 'Regulation__c'];
        Regulation__c reg = new Regulation__c(RecordTypeId = regrt.Id, Custom_Name__c = 'Test Regulation 1', Short_Name__c = 'Test', Status__c = 'Active');
        insert reg;
        
        Signature_Regulation_Junction__c srj = new Signature_Regulation_Junction__c(Regulation__c = reg.Id, Signature__c = sig.Id);
        insert srj;
        
        Authorizations__c auth = new Authorizations__c(Application__c = app.Id, RecordTypeId = authorization.Id, Thumbprint__c = sig.Id);
        insert auth;
        
        AC__c ac = testData.newLineItem('Personal Use',app,auth);
        
        System.debug ('<<<<<<<<<<<<<<<<<<<< Animal Care ' + ac + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
        Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = ac.Id, Total_Files__c = 1);
        insert att;
        
        Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
        insert n;
        
        Authorizations__c au = [select Thumbprint__c from Authorizations__c where ID = :auth.Id];
        
        Domain__c domain1 = new Domain__c(Name = 'Test');
        insert domain1;
        
        Program_Prefix__c pp1 = new Program_Prefix__c(Name = 'test', Prefix_Status__c = 'Active', Program__c = domain.Id);
        insert pp1;
        
        Intended_Use__c use1 = new Intended_Use__c(Name = 'Veterinary Treatment', RecordTypeId = usert.Id);
        insert use1;
        
        Regulation__c reg1 = new Regulation__c(RecordTypeId = regrt.Id, Custom_Name__c = 'Test1', Short_Name__c = 'Test1', Status__c = 'Active');
        insert reg1;
        
        Signature__c sig1 = new Signature__c(Name = 'Test Signature1', Program_Prefix__c = pp.Id, RecordTypeId = sigrt.Id, Intended_Use__c = use.Id, Treatment_available_in_Country_of_Origin__c = 'Yes');
        insert sig1;
        
        Signature_Regulation_Junction__c srj1 = new Signature_Regulation_Junction__c(Regulation__c = reg1.Id, Signature__c = sig1.Id);
        insert srj1;
        
        auth.Thumbprint__c = sig1.Id;
        auth.Date_Issued__c = date.today();
        update auth;
        system.assert(auth != null);
        
    }
}