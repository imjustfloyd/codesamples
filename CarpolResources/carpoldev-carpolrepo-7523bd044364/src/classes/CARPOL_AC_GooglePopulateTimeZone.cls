/*
 * Dinesh - Added exception handler logging try-catch block
 */
public with sharing class CARPOL_AC_GooglePopulateTimeZone {

    Public Static boolean allow = true;

    public static void stopTrigger(){
        allow = false;
    }

    public static boolean canIRun(){
        return allow;
    }



    // inner class
    private class GeoResult {

        public String latitude;
        public String longitude;
        
        public String toDisplayString() {
            return  latitude + ','
                + longitude;
        }
    }

    public class TimeZoneResult {

        private String timeZone;

    }

    @future(callout=true)
    public static void CARPOL_AC_CallGoogleGeoAddress(List<Id> facIds) {
        try{
            string portAddress;
            List<Facility__c> lstFac = [select Id, Name, Address_1__c, Address_2__c, City__c, State_Code__c, Zip__c,  Time_Zone__c from facility__c where Id IN: facids];
            for (Facility__c fac : lstFac) {

                portAddress = fac.Name + '+' + fac.Address_1__c + '+'+ fac.Address_2__c  + '+' + fac.City__c + '+' + fac.State_Code__c + '+' + fac.Zip__c;

                portAddress = portAddress.replaceAll('\\s+','+');
                portAddress = portAddress.replaceAll('null', '');
                string apiKey = 'AIzaSyAAWygMsr763OpAOyrYqhKXlwB3_Yz1h4k';
                //This is the key for server applications. Put this in a custom setting
                String url = 'https://maps.googleapis.com/maps/api/geocode/xml?';
                // string address = 'address=' + portAddress;
                List<GeoResult> results = new List<GeoResult>();
                url += 'address=' + portAddress;
                url += '&key=' + apiKey;
                Http h = new Http(); HttpRequest req = new HttpRequest();
                req.setHeader('Content-type', 'application/x-www-form-urlencoded');
                req.setHeader('Content-length', '0');
                req.setEndpoint(url); req.setMethod('POST');
                String responseBody = '';
                if(!test.isRunningTest()){
                    HttpResponse res = h.send(req);
                    // create the xml doc that will contain the results of the REST operation

                    XMLDom doc = new XMLDom(res.getBody());
                    string latlongitude;

                    // process the results
                    XmlDom.Element[] elements = doc.getElementsByTagName('geometry');
                    if (elements != null) {
                        for (XmlDom.Element element : elements){
                            string latitude = element.getValue('lat');
                            // system.debug('latitude ' + latitude);
                            string longitude = element.getValue('lng');
                            latlongitude = latitude + ','
                                + longitude;
                            //results.add(timeZne.timeZone);
                        }
                    }

                    //  system.debug('results '+ results);

                    //  GeoResult resLatLng = results[0];


                    url = 'https://maps.googleapis.com/maps/api/timezone/xml?';
                    string location = latlongitude;
                    DateTime dateTimeNow = dateTime.now();
                    String timeNow = ''+dateTimeNow.getTime()/1000;
                    System.debug('time-stamp: '+timeNow);


                    url += 'location=' + location;
                    url += '&timestamp=' + timeNow;
                    url += '&key=' + apiKey;
                    url = url.replaceAll('\\s+','+');
                    System.debug(url.replaceAll('\\s+','+'));
                    h = new Http();
                    req = new HttpRequest();
                    req.setHeader('Content-type', 'application/x-www-form-urlencoded');
                    req.setHeader('Content-length', '0');
                    req.setEndpoint(url); req.setMethod('POST');
                    responseBody = '';
                    res = h.send(req);
                    // create the xml doc that will contain the results of the REST operation

                    doc = new XMLDom(res.getBody());


                    // process the results
                    elements = doc.getElementsByTagName('TimeZoneResponse');
                    if (elements != null) {
                        for (XmlDom.Element element : elements){

                            string timeZone = element.getValue('time_zone_name');
                            system.debug('zone ' + timeZone);
                            // timeZoneResults.add(toTimeZoneResult(element));
                            system.debug('time zone '+ timeZone);
                            fac.Time_Zone__c = timeZone;
                        }
                    }
                }
                //return facTimeZone;

            }
            stopTrigger();
            update lstFac;
        }catch(Exception ae){ //exception handler to log the exception
            MessageServices__c objMS = new MessageServices__c();
            objMS.Object__c = 'Facility';
            objMS.Original_Message__c = ae.getMessage();
            objMS.Message_Content__c = ae.getStackTraceString();
            insert objMS;
        }
    }
}