@isTest(seealldata=true)
private class CARPOL_UNI_Update_RLD_Test { 
      @IsTest static void CARPOL_UNI_Update_RLD_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          Authorizations__c objauth = testData.newauth(objapp.id);
          AC__c objli1 = testData.newlineitem('Personal Use', objapp);
          AC__c objli2 = testData.newlineitem('Personal Use', objapp);
          AC__c objli3 = testData.newlineitem('Personal Use', objapp);                    
          List<AC__c> listLI = new List<AC__c>();
          listLI.add(objli1);
          listLI.add(objli2);
          listLI.add(objli3);          
          
          Related_Line_Decisions__c objrld = new Related_Line_Decisions__c();
          objrld.Line_Item__c = objli1.id;
          insert objrld;

          Test.startTest(); 

              CARPOL_UNI_Update_RLD extclass = new CARPOL_UNI_Update_RLD();
              CARPOL_UNI_Update_RLD.updateRLD(listLI);
              System.assert(extclass != null);
          Test.stopTest();   
      }
}