public class CARPOL_AC_Ports_extension {

    public List<Facility__c> lstselectedPorts = new List<Facility__c>();
    public Map<String, String> mapSelectedPorts = new Map<String, String>();
    public SelectOption[] allOptions {get; set;}
    public SelectOption[] selectedOptions {get; set;}
    public String rightLabel {get; set;}
    public String leftLabel {get; set;}
    public Facility__c domesticPort;
    public List<AC__c> aclst ;
    public Date arrival;
    public Authorizations__c auth {get; set;}
    public String AuthorizationID = ApexPages.currentPage().getParameters().get('id');
    // The extension constructor initializes the private member
    // variable vsApp by using the getRecord method from the standard
    // controller.
    public CARPOL_AC_Ports_extension(ApexPages.StandardController stdController) {
        this.auth = (Authorizations__c)stdController.getRecord();
        
        if(auth.ID != NULL){
            auth = [SELECT Effective_Date__c, pathway_exp_days__c, Expiration_Date__c FROM Authorizations__c WHERE ID =: auth.ID];
        }
       
        aclst = [select Proposed_date_of_arrival__c from AC__c where Authorization__c = :auth.ID AND RecordType.Name != 'PPQ-Seeds Not For Planting']; //Added RT filter condition-Niharika
        aclst.sort();
        System.debug('<<<<<<<<<<<<<<<<<<<<< List of Sorted Dates of Arrival' + aclst + ' >>>>>>>>>>>>>>>>>');
        if(aclst.size() > 0 && aclst[aclst.size()-1].Proposed_date_of_arrival__c!=null){
            arrival = aclst[aclst.size()-1].Proposed_date_of_arrival__c;
            auth.Expiration_Date__c = arrival.AddDays(integer.valueOf(auth.pathway_exp_days__c));
        }
        else
        {
            auth.Expiration_Date__c = System.Today().AddDays(30);
        }
        
        auth.Effective_Date__c = System.Today();
        rightLabel = 'Selected Ports';
        leftLabel = 'All Ports';
        System.debug('<<<<<<<<<<<<<<<<<<<<< Test Point >>>>>>>>>>>>>>>>>>>>>>>');

        getPorts();
        
    }

    public PageReference getPorts(){
        //try
        {
            List<Facility__c> allports = [select id, Name from Facility__c where Type__c ='Domestic Port' AND ID NOT IN (select Port__c from Authorization_Junction__c where Authorization__c =:auth.Id) LIMIT 1000];
            allOptions = new List<SelectOption>();
            for(Facility__c allp : allports){
                allOptions.add(new SelectOption(allp.id, allp.Name));
            }
            List<Facility__c> selectedports = [select id, Name from Facility__c where Type__c ='Domestic Port' AND ID IN (select Port__c from Authorization_Junction__c where Authorization__c =:auth.Id)];
            selectedOptions = new List<SelectOption>();
            lstselectedPorts = selectedports;
            System.debug('<<<<<<<<<<<<<<<< Selected Ports: ' + lstselectedPorts.size() + '>>>>>>>>>>>>>>>>>>');
            for (Facility__c slctdp : selectedports){
                selectedOptions.add(new SelectOption(slctdp.id, slctdp.Name));  
                mapSelectedPorts.put(slctdp.id, slctdp.id);
            }
            return null;
        }
        /*catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }*/
    }
    
        
    public PageReference savePorts()
    {
        System.Debug('<<<<<<< Updating Ports in Authorizations >>>>>>>');
        try{
           System.Debug('<<<<<<< Add - All Saved Ports : ' + lstselectedPorts.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Add - Selected Ports : ' + selectedOptions.size() + ' >>>>>>>');
            List<Authorization_Junction__c> portsToBeAdded = new List<Authorization_Junction__c>();
            List<Authorization_Junction__c> portsToBeRemoved = new List<Authorization_Junction__c>();
            Authorization_Junction__c authPortJunction;
            // Create a savepoint while Port List is null
            Savepoint sp = Database.setSavepoint();
            // Logic to add Ports and update the list
            for (SelectOption selPorts : selectedOptions ) {
                System.debug('<<<<<<<<<<<<<<<<<<<<< MAP ' + mapSelectedPorts.size() + ' >>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                if(mapSelectedPorts.get(selPorts.getValue()) == null)
                {
                    System.Debug('<<<<<<< New Port Found >>>>>>>');
                    authPortJunction = new Authorization_Junction__c();
                    authPortJunction.Authorization__c = auth.Id;
                    authPortJunction.Port__c = selPorts.getValue();
                    portsToBeAdded.add(authPortJunction);
                }
            }
            System.Debug('<<<<<<< Total New Ports to be Added : ' + portsToBeAdded.size() + ' >>>>>>>');
            
            insert(portsToBeAdded);
            lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :auth.Id)];
            System.debug('<<<<<<<<<<<<<<<< Authorization: ' + auth.ID + ' >>>>>>>>>>>>>>>>>>>');
            // Logic to remove ports and update the list
            String strRemovePortGroupJunctionIds = '(';
            System.Debug('<<<<<<< Remove - Saved Ports : ' + lstselectedPorts.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Remove - Selected Ports : ' + selectedOptions.size() + ' >>>>>>>');
            if(selectedOptions.size() == 0)
            {    
                for (Facility__c DomPort : lstselectedPorts) {
                    strRemovePortGroupJunctionIds += '\'' + String.ValueOf(DomPort.ID) + '\',';
                }
            }
            else
            {
                for(Integer i = 0; i < lstselectedPorts.size(); i++)
                {
                    Integer matchFound = 0;
                    for(Integer j = 0; j < selectedOptions.size(); j++)
                    {
                        if(lstselectedPorts[i].ID == selectedOptions[j].getValue())
                        {
                            matchFound += 1;
                            break;
                        }
                    }
                    if(matchFound == 0)
                    {
                        strRemovePortGroupJunctionIds += '\'' + String.ValueOf(lstselectedPorts[i].ID) + '\',';
                    }
                }
            }
            if(strRemovePortGroupJunctionIds != '(')
            {
                strRemovePortGroupJunctionIds = strRemovePortGroupJunctionIds.substring(0, strRemovePortGroupJunctionIds.length() - 1);
                strRemovePortGroupJunctionIds += ')';
                System.Debug('<<<<<<< Ids to be Removed : ' + strRemovePortGroupJunctionIds + ' >>>>>>>');
                String queryTemp = 'SELECT ID FROM Authorization_Junction__c WHERE Port__c IN ' + strRemovePortGroupJunctionIds + ' AND Authorization__c =: auth.Id';
                System.Debug('<<<<<<< Final String : ' + queryTemp + ' >>>>>>>');
                portsToBeRemoved = Database.query(queryTemp);
                System.Debug('<<<<<<< Authorization Port Junctions to be Removed : ' + portsToBeRemoved + ' >>>>>>>');
                delete(portsToBeRemoved);
            }
            System.debug('<<<<<<<<<<<< Ports to Be Added ' + portsToBeAdded.size() + ' >>>>>>>>>>>>>>>>>>>');
            System.debug('<<<<<<<<<<<< Ports to Be Removed ' + portsToBeRemoved.size() + ' >>>>>>>>>>>>>>>>>>>');
            upsert auth;
            System.debug('<<<<<<<<<<<<<< Upsert Dates are: ' + auth + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :auth.Id)];
            if(lstselectedPorts.size() == 0)
            {
                // Rollback to the previous null value
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least 1 Port'));
            }
            else 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved'));
            }
            
            getPorts();
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
}