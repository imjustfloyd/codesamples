@isTest(seealldata=true)
private class CARPOL_AC_TP_EditReg_extensionTest {
    static testMethod void testCARPOL_AC_TP_EditReg_extension() {
      
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACTPRecordTypeId = Schema.SObjectType.Signature__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String SRJRecordTypeId = Schema.SObjectType.Signature_Regulation_Junction__c.getRecordTypeInfosByName().get('Singature_Regulation').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();

      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);        
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);            
      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      Attachment attach = testData.newattachment(ac.Id);           
      
      Domain__c objPro = testData.newProgram('AC');
      Program_Prefix__c objPrefix = testData.newPrefix();
      Signature__c objTP = testData.newThumbprint();
      Signature_Regulation_Junction__c objSRJ1 = testData.newSRJ( objTP.Id, objreg1.Id);
      Signature_Regulation_Junction__c objSRJ2 = testData.newSRJ( objTP.Id, objreg2.Id);
      Signature_Regulation_Junction__c objSRJ3 = testData.newSRJ( objTP.Id, objreg3.Id);
      


      PageReference pageRef = Page.carpol_ac_thumbprint_editregulations;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objTP);
      ApexPages.currentPage().getParameters().put('Id',objTP.id);
      CARPOL_AC_TP_EditReg_extension acepreg = new CARPOL_AC_TP_EditReg_extension(sc);
      acepreg.regulation = objreg1;
      acepreg.getResults();
      acepreg.setRegulationType();
      acepreg.getAddByOptions();
      acepreg.selectInput();
      acepreg.getAddByOptions();
      acepreg.updateSignature();
      system.assert(acepreg != null);
    }
}