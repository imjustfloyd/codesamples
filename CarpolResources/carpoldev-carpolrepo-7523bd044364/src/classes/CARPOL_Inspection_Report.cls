public class CARPOL_Inspection_Report {
    public Inspection__c inspection{get;set;}
    public List<Attachment> listInspAtt{get;set;}
    public list<EFL_Inspection_Questionnaire_Questions__c> listEFLQuest{get;set;}
    public CARPOL_Inspection_Report (ApexPages.StandardController controller)
    {
        Id inspectionId = ApexPages.currentPage().getParameters().get('id');
        inspection = [SELECT Id,Status__c,Inspection_Co_ordinates__Latitude__s, Inspection_Co_ordinates__Longitude__s, Inspection_Co_ordinates__c, Inspection_Comments__c,Facility_Features__c, Incident__c, Inspection_Due_Date__c, Scheduled_Date_of_Inspection__c, Inspector_Email__c, Actual_Inspection__c, Time_Spent_Preparing_for_the_Inspection__c, Time_Spent_Conducting_the_Inspection__c, Time_Spent_Creating_and_Revising_Report__c, Time_Spent_Travelling_to_and_from_Site__c, Total_Miles_Driven_to_and_from_Site__c, Authorization__c, Corrective_Actions_Text__c, Inspection_Results_Summary__c, Scheduled_Time_of_Inspection__c,Application__c,Application__r.Name,Application__r.Applicant_Name__c,Application__r.Organization__c,Application__r.Applicant_Address__c,Application__r.Applicant_Fax__c,Facility__r.Name,Facility__r.CBP_Port_Number__c,Facility__r.Address_1__c,Facility__r.Address_2__c,Facility__r.Country__r.Name,Facility__r.Office_Name__c,Facility__r.Office_Phone__c,Facility__r.Office_Email__c,Inspector__r.Name,Inspector__r.Phone FROM Inspection__c WHERE id=:inspectionId];
        listInspAtt=new List<Attachment>();
        listInspAtt=[Select Id,Name,CreatedDate from Attachment where parentid=:inspectionId];
        listEFLQuest=new list<EFL_Inspection_Questionnaire_Questions__c>();
        listEFLQuest=[Select Id,Question__c,Answer__c,Comments__c,Sequence__c from EFL_Inspection_Questionnaire_Questions__c where EFL_Inspection_Questionnaire__r.Inspection__c=:inspectionId];
    }

}