public class portal_Application_Junction_detail {
    public Id recid {get;set;}
    public Id LineItemId {get;set;}
    public ID LineID{get;set;}
    public Id strPathwayId{get;set;}
    public Construct_Application_Junction__c obj{get;set;}

    public portal_Application_Junction_detail(ApexPages.StandardController controller) {
        recid = ApexPages.currentPage().getParameters().get('id');
        obj = (Construct_Application_Junction__c)Controller.getRecord();
        LineID = obj.Line_Item__c;
        system.debug('Record Id : '+ recid);
        LineItemId = [SELECT Line_Item__c  FROM Construct_Application_Junction__c  WHERE ID =:recid].Id;
        system.debug('LineItem Id : '+ LineItemId);
    }

 public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }
 public pageReference ReturnToLineitem(){
    PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
    
       LineItemId = obj.Line_Item__c;
       
       List<AC__c> pathway = new List<AC__c>();
       pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
       
     if(pathway.size()>0){
         for(AC__c i : pathway){ 
       strPathwayId = i.Program_Line_Item_Pathway__c;
         }
     }
       pg.getParameters().put('strPathwayId',strPathwayId);    
       pg.getParameters().put('LineId',LineItemId);
       pg.setRedirect(true);
       return pg;
}
   public PageReference EditPRConstruct(){
   /* List<AC__c> pathway = new List<AC__c>();
       pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
       //strPathwayId = [SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=:LineItemId].id;
       //Id dummy;
     if(pathway.size()>0){
         for(AC__c i : pathway){ 
       strPathwayId = i.Program_Line_Item_Pathway__c;
         }
     }*/
       
   pagereference pg = Page.CARPOL_BRSLinkConstruct;
   pg.getparameters().put('appid',LineID);
   pg.setRedirect(true);
   return pg;
   }
   
   public PageReference upload(){
    PageReference pg = Page.Portal_UploadAttachment;
    pg.getParameters().put('Id', obj.ID);
    pg.getParameters().put('retPage','portal_Application_Junction_detail');
    //system.debug('Construct ID passing to genotype page : ' + constructID);
    pg.setRedirect(true);
    return pg;
}


   public PageReference EditPRSOP(){
   pagereference pg = Page.CARPOL_BRS_ReviewedDesignProtocol_SOP;
   pg.getparameters().put('appid',recid);
   pg.setRedirect(true);
   return pg;
   }

}