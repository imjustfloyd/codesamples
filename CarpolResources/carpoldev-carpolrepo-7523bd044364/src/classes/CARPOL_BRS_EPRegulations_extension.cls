public class CARPOL_BRS_EPRegulations_extension {

    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public List<Authorization_Junction__c> scjC {get;set;}
    public Integer index { get; set; }
    public String regulationType { get; set; }
    
    public Regulation__c regulation { get; set; }
    public Group__c regulationGroup { get; set; }
    public Group_Junction__c regulationGroupLookup { get; set; }
    public SelectOption[] allOptions { get; set; }
    public SelectOption[] selectedOptions { get; set; }
    public String selectedInputType { get; set; }
    public String leftLabel { get; set; }
    public String rightLabel { get; set; }
    public date expirydate{ get; set; }
    public String signatureID = ApexPages.currentPage().getParameters().get('id');
    public Map<String, String> mapAllSavedRegulations = new Map<String, String>();
    public Map<String, String> mapAllSavedRegulationsGroup = new Map<String, String>();
    public List<Regulation__c> lstSavedRegulations = new List<Regulation__c>();
    public List<Group__c> lstSavedRegulationsGroup = new List<Group__c>();
    public String selectedType;
    public String selectedSubType;
    public Boolean showPopUpMessage;
    public String portpage{ get; set; }
    public Authorizations__c au {get;set;}
    public boolean live_animal_nosave_port ;
    public boolean Inspectionstation {get; set;}
    public String regBRSRecordTypeId { get; set; }
    
    //-------------------------------------------------//
    
    //required documents map
    //public Map<String,documentResource> mapRequiredDocuments { get; set;}
    //select option list of documents remaining
    public List<SelectOption> remainingDocuments { get; set;}
    //select option list of documents requried
    public List<SelectOption> requiredDocuments { get; set; }
    //expect this one to go
    public Boolean renderRemainingDocuments { get; set; }
    //remove once save is rewritten - expect this to migrate onto the field
    public String[] selectedDocuments { get; set; }
    //line item id 
    public ID lineItemID{get;set;}
    public AC__c lineItem {get; set;} 

    public List<Applicant_Attachments__c> appAttachments {get;set;}
    //list of new attachments to add
    public List<DocumentWrap> newAttachments {get;set;}

    //used to compare uploaded records with required records
    public Map<String,documentResource> mapUploadedDocuments = new Map<String,documentResource>();
    //should be removed, replace it's use in the map with something else
    public List<documentResource> docResource = new List<documentResource>();    
    
    public string delrecid {get;set;}
    public Map<String,String> sobjectkeys {get;set;}
    transient public string PermitPackage {get; set;}
    public boolean renderPermitPackage {get; set;}
    List<SObject> toInsert = new List<SObject>();
    List<SObject> toUpdate = new List<SObject>();
    ID tmpRecordType = Schema.SObjectType.Applicant_Attachments__c.getRecordTypeInfosByName().get('Permit Package').getRecordTypeId();
    public id wfid {get;set;}
    public string wfname {get;set;}
    public string prefix{get;set;}
    public boolean renderCourtesyPermit {get; set;}
    public boolean renderPermitPackagelabel {get; set;}
     //Labels
    String label = 'Labels';
    public List<Label__c> labelList {get; set;}
    public string packagetitle {get;set;}
    public list<Reviewer__c> lstReviewRec {get;set;}
        public String templateURL { get; set; }
    public String letterType { get; set; }
    public String selectedTemplateType { get; set; }
    public String selectedTemplate { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    public String reviewerComments { get; set; }
    public String baseURL { get; set; }
    public Authorizations__c authorization { get; set; }
    public Workflow_Task__c wtask { get; set; } // Added by Kishore
   //added by niharika  
    public boolean showAttachDraftBtn{get;set;}     
    public boolean showAttachBtn{get;set;}      
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    
     
    public list<Reviewer__c> listReviewRec {get;set;}
    public list<Link_Regulated_Articles__c> listRegArt {get; set;} 

    //-------------------------------------------------//
    
    public CARPOL_BRS_EPRegulations_extension(ApexPages.StandardController controller) {
        
        System.Debug('<<<<<<< Standard Controller Constructor Begins >>>>>>>');
        getRegulation();
        Authorizations__c rrt;
        recordtype artname;
       // Regulation__c rrtname;
         AC__c line_item;
         recordtype liname;
         String     sliname;
         Inspectionstation = true ;
         renderPermitPackagelabel = false;
          lstReviewRec = new list<Reviewer__c>();
         
         if(ApexPages.currentPage().getParameters().get('wfid') != null){
             wfid = ApexPages.currentPage().getParameters().get('wfid');
             wfname = [select name from Workflow_Task__c where id =:wfid ].name;
            // if(wfname.contains('Labels'))
            //   renderPermitPackagelabel = true;
               
              renderPermitPackagelabel = wfname.contains(label); 
               
             }
        //popup add regulations
        regulation = new Regulation__c();
        this.au= (Authorizations__c)controller.getRecord();
        if (au.Id!=null){
            au = [Select Authorization_Type__c, Program_Pathway__r.Permit_PDF_Template__c,Program_Pathway__r.program__r.name, Status__c, Id, Response_Type__c,RecordTypeId, RecordType.Name, Application__c,Template__c, 
                        Thumbprint__c,Effective_Date__c,Expiry_Date__c, pathway_exp_days__c, Expiration_Date__c,Plant_Inspection_Station__c,Prefix__c,(select name from Labels__r) FROM Authorizations__c Where Id =:au.Id];

            string recordTypeName=ApexPages.currentPage().getParameters().get('RecordType');
            artname=[select id, name from recordtype where developername =: 'BRS_RT' and SObjectType =: 'Regulation__c']; 
            line_item = [select recordtypeid,id,name, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Program__r.Name from AC__c where  Authorization__c =:au.Id limit 1]; 
            liname = [select name from recordtype where id =: line_item.RecordTypeId ]; 
            sliname = string.valueOf(liname.name); 
            prefix = au.Prefix__c;
            if(prefix == '100'){
                packagetitle = 'Notification';
            }else if(prefix == '101'){
                packagetitle = 'Permit Package'; 
            }
            labelList = [select id,Name,QRcode_Barcode__c,Notification_Issue_Date__c,Notification_Expiration_Date__c,Status__c from Label__c where Authorization__c =:au.Id];
          }
         
        showPopUpMessage = false;
        selectedInputType = 'Test';
        leftLabel= 'Available Regulations';
        rightLabel = 'Selected Regulations'; 

        if(artname != null)
        {
        regBRSRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get(artname.name).getRecordTypeId();
        }
        else
        {
        regBRSRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        }
        
        regulationGroupLookup = new Group_Junction__c();
        allOptions = new List<SelectOption>();

        List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regBRSRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = :au.Id) Order By Custom_Name__c ASC LIMIT 999];
        for (Regulation__c rgltn : regulations ) 
        {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
        selectedOptions = new List<SelectOption>();
        List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = :au.Id) LIMIT 999];
        lstSavedRegulations = slctdRgltns;
        for (Regulation__c rgltn : slctdRgltns)
        {
            selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
            mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
         
          lineItemID = [SELECT ID,Does_This_Application_Contain_CBI__c FROM AC__c WHERE Authorization__c = :au.Id LIMIT 1].ID;
         
        system.debug('lineItemID@@@@'+lineItemID);
       if(ApexPages.currentPage().getParameters().get('PermitPackage') != null){
          PermitPackage = ApexPages.currentPage().getParameters().get('PermitPackage'); 
         
          if(PermitPackage!=null && PermitPackage == 'True')
             renderPermitPackage = true;
             
        //  if(renderPermitPackagelabel == true)
        //     renderPermitPackage = false;     
       }  
        system.debug('renderPermitPackage@@@@'+renderPermitPackage);
        
         if(lineItemID != null)
        {
            lineItem = [SELECT Name, Application_Number__c, Intended_Use__c, Intended_Use__r.Name, Application_Number__r.Name FROM AC__c WHERE ID = :lineItemID LIMIT 1];
        }        
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Line Item found.'));}

         newAttachments=new List<DocumentWrap>{new DocumentWrap()};
        
        requiredDocuments = new List<SelectOption>();
        requiredDocuments = getRequiredDocument();

        if(prefix == '102'){
           renderCourtesyPermit = true; 
           renderPermitPackage = true;
        }
  
          showAttachDraftBtn=false;      
         showAttachBtn=true;        
         showtemplateBtn=true;  
         showLinks=true;
  

        if(au == null){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a related Authorization.'));}
        else
        {
             au.Date_Issued__c = System.Today();
             letterType = au.Authorization_Type__c;
             templateURL = 'apex/CARPOL_AuthorizationLetter?id=' + au.ID + '&tId='+ wfid +'&Type='+true; 
        }
   
        
        
        getExistingDocuments(); 
       //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
   
        
    }
    
    // Method for testing a hidden field functionality 
    public PageReference setRegulationType(){
        regulation.Type__c = regulationType;
        getResults();
        return null;
        
    }
    
    //get regulations
    public void getRegulation()
    {
        ID authID = ApexPages.currentPage().getParameters().get('id');
        scj = new List<Authorization_Junction__c>([Select id, is_Active__c, Authorization__c, Order_Number__c, Regulation_Short_Name__c, Regulation_Description__c,  Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :authID ORDER BY Order_Number__c ASC LIMIT 999]);
         scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        scjC = new List<Authorization_Junction__c>();
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Standard' ){scjA.add(SCJ);}
                if(SCJ.Regulation__r.Type__c=='Supplemental Conditions'){scjB.add(SCJ);}


            }
        }
    }
    
//add regulations
public PageReference getResults(){
        try
        {
            showPopUpMessage = false;
            selectedType = regulation.Type__c;
            selectedSubType = regulation.Sub_Type__c;
            Boolean flagWhereClause = false;
            
            if(selectedInputType != 'Group')
            {
                String allOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId=:regBRSRecordTypeId AND ';
                String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
                
                if(selectedType != null){
                    allOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    flagWhereClause = true;
                }
                if(selectedSubType != null){
                    if(flagWhereClause == true){
                        allOptionsQuery += 'AND ';
                        selectedOptionsQuery += 'AND ';
                    }
                    allOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    flagWhereClause = true;
                }
                
                if(flagWhereClause == true){
                    allOptionsQuery += 'AND ';
                    selectedOptionsQuery += 'AND ';
                }
                allOptionsQuery += 'ID NOT IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') Order By Custom_Name__c ASC LIMIT 999';
                selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') LIMIT 999';
                allOptions = new List<SelectOption>();
                List<Regulation__c> regulations = Database.query(allOptionsQuery);
                for (Regulation__c rgltn : regulations) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
                
                selectedOptions = new List<SelectOption>();
                List<Regulation__c> slctdRgltns = Database.query(selectedOptionsQuery);
                lstSavedRegulations = slctdRgltns;
                for (Regulation__c rgltn : slctdRgltns )
                {
                    //selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                    selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c));
                    mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
                }
            }
            return null;
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
    
    public PageReference updateSignature() {
        
        try{
            if(selectedInputType != 'Group')
            {
                updateRegulations();
                PageReference pageRef = Page.CARPOL_BRS_EPRegulations;
                if(ApexPages.currentPage().getParameters().get('wfid') != null){
                    wfid = ApexPages.currentPage().getParameters().get('wfid');
                    pageRef.getParameters().put('wfid',wfid);

                  }
                pageRef.getParameters().put('Id',au.Id);
                pageRef.setRedirect(true);
                return pageRef;
                return null;
                 
            }
            return null;
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
        
    }
    
    public void updateRegulations() {
 
        try
        {
 
            List<Authorization_Junction__c> regulationsToBeAdded = new List<Authorization_Junction__c>();
            List<Authorization_Junction__c> regulationsToBeRemoved = new List<Authorization_Junction__c>();
            Authorization_Junction__c sigRegulationMapp;
            
            // Logic to add Regulations and update the list
            for (SelectOption selReg : selectedOptions) {
                if(mapAllSavedRegulations.get(selReg.getValue()) == null)
                {
                     sigRegulationMapp = new Authorization_Junction__c();
                    RecordType rt = [SELECT ID FROM RecordType WHERE Name = 'Regulation Junction' AND SObjectType = 'Authorization_Junction__c' LIMIT 1];
                    sigRegulationMapp.RecordTypeID = rt.ID;
                    sigRegulationMapp.is_Active__c = 'Yes';
                    sigRegulationMapp.Authorization__c = signatureID;
                    sigRegulationMapp.Regulation__c = selReg.getValue();
                    Regulation__c reg = [SELECT Regulation_Description__c FROM Regulation__c WHERE ID =:selReg.getValue()];
                    sigRegulationMapp.Regulation_Description__c = reg.Regulation_Description__c;
                    sigRegulationMapp.Needed_BRS_Manager_Collaboration__c = 'Yes';
                    regulationsToBeAdded.add(sigRegulationMapp);
                }
            }
             insert(regulationsToBeAdded);
            
            // Update the saved result set for regulations
            Boolean flagWhereClause = false;
            String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
            
            if(selectedType != null){
                selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                flagWhereClause = true;
            }
            if(selectedSubType != null){
                if(flagWhereClause == true){
                    selectedOptionsQuery += 'AND ';
                }
                selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                flagWhereClause = true;
            }
            if(flagWhereClause == true){
                selectedOptionsQuery += 'AND ';
            }
            selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') LIMIT 999';
 
            lstSavedRegulations = Database.query(selectedOptionsQuery);
            
            // Logic to remove regulations and update the list
            String strRemoveSignatureRegulationJunctionIds = '(';
 
            if(selectedOptions.size() == 0)
            {    
                for (Regulation__c svdRegulation : lstSavedRegulations) {
                    strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(svdRegulation.ID) + '\',';
                }
            }
            else
            {
                for(Integer i = 0; i < lstSavedRegulations.size(); i++)
                {
                    Integer matchFound = 0;
                    for(Integer j = 0; j < selectedOptions.size(); j++)
                    {
                        if(lstSavedRegulations[i].ID == selectedOptions[j].getValue())
                        {
                            matchFound += 1;
                            break;
                        }
                    }
                    if(matchFound == 0)
                    {
                        strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(lstSavedRegulations[i].ID) + '\',';
                    }
                }
            }
            if(strRemoveSignatureRegulationJunctionIds != '(')
            {
                strRemoveSignatureRegulationJunctionIds = strRemoveSignatureRegulationJunctionIds.substring(0, strRemoveSignatureRegulationJunctionIds.length() - 1);
                strRemoveSignatureRegulationJunctionIds += ')';
                 String queryTemp = 'SELECT ID FROM Authorization_Junction__c WHERE Regulation__c IN ' + strRemoveSignatureRegulationJunctionIds + ' AND Authorization__c = :signatureID LIMIT 999';
                regulationsToBeRemoved = Database.query(queryTemp);
                 delete(regulationsToBeRemoved);
            }
            
            if(regulationsToBeAdded.size() != 0 || regulationsToBeRemoved.size() != 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
             }
    
        }
        catch(Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
        }
    }
    
    public List<SelectOption> getAddByOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Individually','Individually'));
        options.add(new SelectOption('Group','By Group'));
        return options;
    }
    
    public PageReference selectInput() {
        if(false){}
        else
        {
             leftLabel = 'Available Regulations';
            rightLabel = 'Selected Regulations';
            allOptions = new List<SelectOption>();
            List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regBRSRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            for (Regulation__c rgltn : regulations ) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
            
            selectedOptions = new List<SelectOption>();
            List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            lstSavedRegulations = slctdRgltns;
             for (Regulation__c rgltn : slctdRgltns)
            {
                selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
            }
        }
         return null;
    }
    
    public PageReference viewDraftPDF() { 
 
        PageReference pageRef;
        if(prefix == '101'){
            pageRef = Page.CARPOL_BRS_StandardPermit;
         }else if(prefix == '102'){
            pageRef = Page.CARPOL_BRS_CourtesyPermit;}
            
        if(renderPermitPackagelabel == true) 
           pageRef = Page.CARPOL_BRS_PDF_Labels;

        pageRef.getParameters().put('Id',au.Id);
        pageRef.setRedirect(true);
        return pageRef;     
        }

    
    public PageReference createNewRegulation(){
        PageReference pageRef = Page.CARPOL_AC_Create_RegulationOnAuth;
        pageRef.getParameters().put('Id',au.Id);
        pageRef.getParameters().put('wfid',wfid);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public PageReference BRSCollaboration(){
        PageReference pageRef = Page.CARPOL_BRS_ROP_Cond_Collaboration;
        pageRef.getParameters().put('Id',au.Id);
        pageRef.getParameters().put('wfid',wfid);
        pageRef.setRedirect(true);
        return pageRef; 
    }    

   
    public PageReference attachPDF(){
        toInsert.clear();
        system.debug('@@@Entering attachPDF@@@');
       //  PageReference pageRef = new pagereference('');  
         PageReference pageRef;
        if(prefix == '101'){
            pageRef = Page.CARPOL_BRS_StandardPermit;
         }else if(prefix == '102'){
            pageRef = Page.CARPOL_BRS_CourtesyPermit;}
            
          if(renderPermitPackagelabel == true){ 
               If(labelList.size()>0) 
                  pageRef = Page.CARPOL_BRS_PDF_Labels; 
               else{
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please generate the Labels before attaching.'));
                  return null;
                } 
           } 
         Attachment att;
         
         if( au.Authorization_Type__c != 'Permit' && prefix != '102'){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit cannot be issued. Please check Decision type again'));
             return null;
         }
         else {
                   DocumentWrap newAtt = new DocumentWrap(); 
                   newAtt.appattach.RecordTypeID = tmpRecordType;
                   newAtt.appAttach.Authorization__c = au.ID;
                   newAtt.appattach.Application__c = lineItem.Application_Number__c;
                   newAtt.appAttach.File_Name__c = 'Permit Document';
                   newAtt.appAttach.File_Description__c = 'Uploaded ' + System.Now();
                   newAtt.appAttach.Document_Types__c = 'Permit Document';
                   if(renderPermitPackagelabel == true){ 
                        newAtt.appAttach.File_Name__c = 'Labels Document';
                        newAtt.appAttach.Document_Types__c = 'Import Labels';} 
                  
                   toInsert.add(newAtt.appAttach);
                   insert toInsert;
                   toInsert.clear();
                  
               system.debug('@@@EnewAttachments@@@'+newAttachments);

            //new insert attachments
                    Blob b;
                    system.debug('@@@pageRef1@@@'+pageRef);
                    pageRef.getParameters().put('id',au.Id);
                    system.debug('@@@pageRef@@@'+pageRef); 
                   
                    if(!test.isRunningTest()){
                        b= pageRef.getContentAsPDF();
                      }else{
                        b= Blob.valueOf('Some Text');          
                   } 
                   
                  system.debug('@@@Blob b@@@'+b);
                  newAtt.attachment.body = b;
                  newAtt.appAttach.File_Name__c = 'Permit Document';   
                  newAtt.attachment.OwnerId = UserInfo.getUserId();             
                  newAtt.attachment.Name = 'Permit_'+date.today().format()+'.pdf'; 
                  newAtt.attachment.ParentId = newAtt.appAttach.ID;
                  newAtt.attachment.Description = 'Permit Document';
                  if(renderPermitPackagelabel == true){ 
                      newAtt.appAttach.File_Name__c = 'Import Labels'; 
                      newAtt.attachment.Name = 'Labels_'+date.today().format()+'.pdf';
                      newAtt.attachment.Description = 'Import Labels';
                  } 
                  system.debug('@@@ newAtt.attachment@@@'+newAtt.attachment);
                  toInsert.add(newAtt.attachment);
                  system.debug('@@@toInsert@@@'+toInsert); 

        insert toInsert;
        
        newAttachments.clear();
        newAttachments.add(new DocumentWrap());
        
        //refresh the list of required and existing documents        
        getExistingDocuments();  
        toInsert.clear();
         }
        return null; 
    }
    
    
    //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
    
     //Class to hold uploaded documents information
    public class documentResource{
        public ID docID {get;set;}
        public Boolean isUploaded {get;set;}
        public string docName {get;set;}
        public String docURL {get;set;} 
    } 
    
    //Subclass : Wrapper Class 
    @TestVisible public class DocumentWrap {
       public Applicant_Attachments__c appAttach{get;set;}
       public Attachment attachment {get;set;}    
       public List<String> documenttypes{get;set;}    

       //Wrapper  Class Controller
       public DocumentWrap() {
            appAttach = new Applicant_Attachments__c();
            attachment= new Attachment();
            documenttypes = new List<String>();
        }
    }
    
    
       //Getting existing uploaded documents if any
    public void getExistingDocuments() {
        remainingDocuments = new List<SelectOption>();
        
        system.debug('Authorization ID ****'+au.Id);
        
        appAttachments = [SELECT Id, Name, Document_Types__c, File_Name__c, File_Description__c,(SELECT ID, Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Authorization__c =: au.Id order by File_Description__c];
        System.Debug('<<<<<<< Total Uploaded Documents : ' + appAttachments.size() + ' >>>>>>>');
        
        if(appAttachments.size() > 0)
        {
            for(Applicant_Attachments__c appAtt : appAttachments)
            {
                System.Debug('<<<<<<< Uploaded Documents : ' + appAtt.Document_Types__c + ' >>>>>>>');
                String[] documents = appAtt.Document_Types__c.split(';');
                System.Debug('<<<<<<< Uploaded Documents Stack : ' + documents.size() + ' >>>>>>>');

                 documentResource docRes = new documentResource();
                if(appAtt.Attachments.size() > 0)
                {
                    docRes.docURL = '/servlet/servlet.FileDownload?file=' + appAtt.Attachments[0].ID;
                    docRes.isUploaded = true;
                }
                for(Integer j = 0; j < documents.size(); j++)
                {
                    mapUploadedDocuments.put(documents[j],docRes);
                    System.Debug('<<<<<<< mapUploadedDocuments Size  : ' + mapUploadedDocuments.size() + ' >>>>>>>');
                }
            }
        }

         if(requiredDocuments.size() > 0)
        {
            for(Integer i = 0 ; i < requiredDocuments.size(); i++)
            {
                String documentName = String.ValueOf(requiredDocuments[i].getLabel());
                documentResource docResTemp = mapUploadedDocuments.get(requiredDocuments[i].getLabel());

                  if(docResTemp == null)
                {
                    docResTemp = new documentResource();
                    docResTemp.isUploaded = false;
                    remainingDocuments.add(new SelectOption(documentName, documentName));
                }
            }
        }
         if(remainingDocuments.size() > 0){renderRemainingDocuments = true;}else{renderRemainingDocuments = false;}
    }
    
    
    public void save()
    {    toInsert.clear(); 
         system.debug('@@@newAttachments@@'+newAttachments);
         system.debug('@@@newAttachments.size@@'+newAttachments.size());
        for (DocumentWrap newAtt : newAttachments)
        {
             if (newAtt.attachment.Body!=null & newAtt.documenttypes.size() > 0)
            {    system.debug('@@@toInsert1@@@'+toInsert);
                 newAtt.appattach.RecordTypeID = tmpRecordType;
                 newAtt.appAttach.Authorization__c = au.ID;
                 newAtt.appattach.Application__c = lineItem.Application_Number__c;
                
                if(newAtt.appAttach.File_Name__c.length() == 0){
                     newAtt.appAttach.File_Name__c = newAtt.attachment.name;
                }
                
                   newAtt.appAttach.File_Description__c = 'Uploaded ' + System.Now();
                   newAtt.appAttach.Document_Types__c = flattenSelectedDocuments(newAtt.documenttypes);
                   system.debug('@@@newAtt.appattach.id@@@'+newAtt.appattach.id);
                   system.debug('@@@nnewAtt.appAttach@@@'+newAtt.appattach);
                      toInsert.add(newAtt.appAttach);
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You must add a file to upload and select a document type before selecting Save.'));            
             }
        }
        system.debug('@@@toInsert2@@@'+toInsert);
        insert toInsert;
        toInsert.clear();
        
        //new insert attachments
        for (DocumentWrap newAtt : newAttachments)
        {
            if (newAtt.attachment.Body!=null & newAtt.documenttypes.size() > 0)
            {
                  //handle the physical attachment
                  newAtt.attachment.OwnerId = UserInfo.getUserId();
                  newAtt.attachment.ParentId = newAtt.appAttach.ID;
                  newAtt.attachment.Description = 'Uploaded Document';
                // if(newAtt.attachment.id == null)
                   toInsert.add(newAtt.attachment);
            }
        }
        
        insert toInsert;
        
        newAttachments.clear();
        newAttachments.add(new DocumentWrap());
        
        //refresh the list of required and existing documents        
        getExistingDocuments();
     }
    
       public List<SelectOption> getRequiredDocument() {
    
            List<SelectOption> options = new List<SelectOption>();
 
                      options.add(new SelectOption('Draft Environmental Assessment','Draft Environmental Assessment',true));
                      options.add(new SelectOption('Variance Approval Letter (VA)','Variance Approval Letter (VA)',true));
                      options.add(new SelectOption('Variance Request (VR)','Variance Request (VR)',true));
                      options.add(new SelectOption('Non-CBI documents by the applicant','Non-CBI documents by the applicant',true));
                      options.add(new SelectOption('Final EA/FONSI','Final EA/FONSI',true));
 
            return options;
        }
    
        //flatten the document types
    public String flattenSelectedDocuments(List<String> documenttypes) {
    
        System.debug('<<<<<<< Selected Documents List Size : ' + documenttypes.size());
        String strSelectedDocuments = '';        
        if(documenttypes.size() != 0)
        {

            for(Integer i = 0; i < documenttypes.size(); i++)
            {
                strSelectedDocuments += documenttypes[i] + ';';
            }
            //removing the last '; '
            strSelectedDocuments = strSelectedDocuments.substring(0,strSelectedDocuments.length() - 1);
        }
        return strSelectedDocuments;
    }
           public PageReference deleteRecord(){
 
                  string  strqurey = 'select id from Applicant_Attachments__c where id=:delrecid';
                    list<sobject> lst = database.query(strqurey);
                   delete lst;
                    
                    PageReference dirpage= new PageReference('/apex/CARPOL_BRS_EPRegulations?id=' + au.ID+'&wfid='+wfid+'&PermitPackage=true');
                    dirpage.setRedirect(true);
                    return dirpage;
                }  
                
           public PageReference redirect(){
 
                    PageReference dirpage= new PageReference('/'+wfid);
                    dirpage.setRedirect(true);
                    return dirpage;
                }   
                
 
    
    public PageReference cancel()
    {
        System.Debug('<<<<<<< Cancel >>>>>>>');
        PageReference return2Auth = new PageReference('/' + au.Id);
        return2Auth.setRedirect(true);
        return return2Auth;
    }

 
  
        
        
        
        
        
        
        
        
        
        
        
        
        
    
    
}