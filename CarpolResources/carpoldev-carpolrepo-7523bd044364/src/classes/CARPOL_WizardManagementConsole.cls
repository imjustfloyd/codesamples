public class CARPOL_WizardManagementConsole {
    
        public Signature__c signature { get; set; } 
    
        public List<Regulation__c> allRegulations { get; set; }
    
        public List<Signature__c> allSignatures { get; set; }
        
        public List<Signature__c> getsignlst()
        {
            list<Signature__c> allSignatures =  new list<Signature__c>();
            string strquery = 'select id,Name,From_Country__c,Group__c,Plant_Part__c,To_Country__c,Regulated_Article__c,Selected_Import_Requirements__c,Selected_Instruction_for_CBP_Officers__c,Status__c from Signature__c where id!=null ';
            if(signature.Regulated_Article__c!=null)
            {
                string strarticle = signature.Regulated_Article__c;
                strquery += ' and Regulated_Article__c =: strarticle ';
            }
            if(signature.From_Country__c!=null)
            {
                string strfrmcountry = signature.From_Country__c;
                strquery += ' and From_Country__c=:strfrmcountry ';
            }
            allSignatures = Database.query(strquery);
            return allSignatures ;
        }
    
    public PageReference Save() {
        PageReference pr = new PageReference('/apex/CARPOL_WizardQuestions'); 
     pr.setRedirect(True); 
      return pr;
    } 
    public string strsignid {get;set;}
    public PageReference changesign() {
        PageReference pr = new PageReference('/apex/CARPOL_WizardSignatureChanges?id='+strsignid); 
     pr.setRedirect(True); 
      return pr;
    }  
    
    public pagereference getresults(){
        return null;
    }
    
    public CARPOL_WizardManagementConsole()
    {
        signature = new Signature__c();
    }


}