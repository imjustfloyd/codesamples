public class portal_LinkRegArt_detail {
 
 public Id LineItemId {get;set;}
 public Id LinkRegArtID{get;set;}
 public Link_Regulated_Articles__c obj {get;set;}
 
    public portal_LinkRegArt_detail (ApexPages.StandardController controller) {
     obj = (Link_Regulated_Articles__c)Controller.getRecord();
     LinkRegArtID=ApexPages.currentPage().getParameters().get('id');
     LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
    
    }

public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('ID')];
    }

public PageReference edit(){
    PageReference pg = new PageReference('/apex/portal_LinkRegArt_edit');
    LineItemId = obj.Line_Item__c;
    pg.getParameters().put('id',obj.ID);
    pg.getParameters().put('LineItemId',LineItemId);
    pg.setRedirect(true);
    return pg;
}

public PageReference ReturnToLineItem(){
PageReference pg = new PageReference('/apex/CARPOL_LineItemPageForApplicant');//Page.CARPOL_LineItemPageForApplicant;
    
       LineItemId = obj.Line_Item__c;
        List<AC__c> pathway = new List<AC__c>();
       pathway=[SELECT Program_Line_Item_Pathway__c FROM AC__c WHERE ID=: LineItemId];
       
       Id dummy;
       for(AC__c i : pathway){
       dummy = i.Program_Line_Item_Pathway__c;}
      
       pg.getParameters().put('strPathwayId',dummy);    
       pg.getParameters().put('LineId',LineItemId);
       pg.setRedirect(true);
       return pg;
    }


}