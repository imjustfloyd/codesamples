public with sharing class CARPOL_ApplicationProcessing {
    
    public Application__c application = new Application__c();
    public ID applicationID;
    //public Boolean attachPDF {get; set;}
    //Jialin20151008 - only save snapshot when application has AC line items
    //public List<AC__c> aclst {get; set;}
    //public List<AC__c> vslst {get; set;}
    //public List<AC__c> PPQlst {get;set;}
    
    public Integer acCount =0;
    public Integer vsCount =0;
    public Integer ppqCount =0;
    integer StopQuery = 0;
    List<AC__c> LineItems = new List<AC__c>();
    
    public CARPOL_ApplicationProcessing(ApexPages.StandardController controller) {
        
        applicationID = ApexPages.CurrentPage().getparameters().get('id');
        this.application = (Application__c)controller.getRecord();
        System.Debug('<<<<<<< Application ID : ' + application.ID + ' >>>>>>>');
        if(application.ID != null)
        {
            //aclst = [select ID, RecordType.Name from AC__c where RecordType.Name = 'Live Dogs' AND Application_Number__c = :applicationID];
            String rectype;
            for(AC__c lineitem : [Select Id, RecordType.Name from AC__c where Application_Number__c = :applicationID ])
            {
                rectype = lineitem.RecordType.Name;
                if(rectype.contains('Live Animal'))
                    vsCount++;
                if(rectype.contains('Live Dogs'))
                    acCount++;
                if(rectype.contains('PPQ'))
                    ppqCount++;
            }
        }
        else
        {
           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'No valid Application ID found.'));
        }
        System.Debug('<<<<<<< aclst Size : ' + acCount + ' >>>>>>>');
    
    }
    
    public PageReference processApplication()
    {
        try
        {
            application = [SELECT ID, Name, Application_Status__c, Applicant_Email__c, RecordType.Name, Applicant_Name__r.Name FROM Application__c WHERE ID =: application.Id LIMIT 1];
            if(application.RecordType.Name == 'Standard Application')
            {
            // Changed to open from saved 7/8 Angie
                if(application.Application_Status__c == 'Open')
                {
                    //application.Application_Status__c = 'Submitted';
                    // Causing errors for applicant attachments. Removed and updated on subbbbmitwithdraw trigger
                    
                    // start of changes Kishore
                      LineItems = [SELECT ID, Name, Departure_Time__c, Proposed_date_of_arrival__c, Arrival_Time__c, Status__c,Renew_Authorization__c,RecordType.Name,movement_type__c,
                                                    Construct_Status__c,SOP_Status__c,Regulated_Article_Status__c,Location_Status__c,Program_Line_Item_Pathway__r.program__r.name,
                                   (Select id from Link_Regulated_Articles__r),
                                   (Select id from locations__r where RecordType.Name =: 'Origin Location'),
                                   (Select id from Applicant_Attachments__r where RecordType.Name =: 'BRS Notification SOP')
                                   FROM AC__c WHERE Application_Number__c = :Application.ID];  
                      if(LineItems.size() <= 0)
                      {
                       Application.addError('Please create a line item before you submit the application for approval. Click on the link below to return to your application. ');
                               StopQuery = 1;
                               }
                      else
                      {
                        for(Integer i = 0; i < LineItems.size(); i++)
                        {
                            //for proposed date of departure
                            if(LineItems[i].Departure_Time__c < Date.Today() || LineItems[i].Arrival_Time__c < Date.Today() ||
                            LineItems[i].Proposed_date_of_arrival__c < Date.Today())
                            {
                                Application.addError('Please edit the Arrival and Departure dates to be later than today for '+ LineItems[i].Name  + 
                                ' before you submit the application for approval.');
                                StopQuery = 1;
                            }
                            
                            if(LineItems[i].Status__c != 'Ready to Submit' && LineItems[i].Status__c != 'Submitted' && LineItems[i].Program_Line_Item_Pathway__r.program__r.name != 'BRS')
                            { 
                               Application.addError('Please attach the required documents on the following line item ' + 
                               LineItems[i].Name  + ' before you submit the application for approval.' +
                               ' Click back to the application. Locate the line item and click on New Applicant Attachment.');
                               StopQuery = 1;
                             //  System.Debug('<<<<<<< StopQuery=1? : ' + StopQuery + ' >>>>>>>');                                 
                            }
                            if(LineItems[i].Regulated_Article_Status__c != 'Ready to Submit' &&  
                            (LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Notification' || 
                            LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit' ||
                            LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit')) 
                            {
                                Application.addError('Please add Regulated Article(s) on the following line item '+ LineItems[i].Name  + 
                                ' before you submit the application for approval.' +
                                ' Click back to the application.Access the line item,click "Item Details",and click "Add Regulated Articles"');
                                StopQuery = 1;
                            }
                          if((LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Notification' || 
                              LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit' ||
                              LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit') && 
                              LineItems[i].Construct_Status__c != 'Ready to Submit')
                            {
                                Application.addError('Please add atleast 1 Construct with Phenotypes/Genotypes on the following line item '+ LineItems[i].Name  +
                                ' before you submit the application for approval. Click back to the application. click "Item Details",and click "Add  Construct"');
                                StopQuery = 1; 
                            }                            
                          if((LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Notification' || 
                              LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Courtesy Permit' ||
                              LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit') && 
                              LineItems[i].Location_Status__c != 'Ready to Submit')
                            {
                                if(LineItems[i].movement_type__c == 'Import') {
                                Application.addError('Please add 1 Origin Location and 1 destination location on the following line item '+ LineItems[i].Name  +
                                ' before you submit the application for approval. Click back to the application. click "Item Details",and click "Add Location"');
                                StopQuery = 1; 
                                }else if(LineItems[i].movement_type__c == 'Interstate Movement') {
                                Application.addError('Please add atleast 1 Origin Location and 1 destination location on the following line item '+ LineItems[i].Name  +
                                ' before you submit the application for approval. Click back to the application. click "Item Details",and click "Add Location"');
                                StopQuery = 1; 
                                }else if(LineItems[i].movement_type__c == 'Interstate Movement and Release') {
                                Application.addError('Please add atleast 1 Origin Location, 1 destination location and 1 Release location on the following line item '+ LineItems[i].Name  +
                                ' before you submit the application for approval. Click back to the application. click "Item Details",and click "Add Location"');
                                StopQuery = 1; 
                                }else if(LineItems[i].movement_type__c == 'Release') {
                                Application.addError('Please add atleast 1 Release Location on the following line item '+ LineItems[i].Name  +
                                ' before you submit the application for approval. Click back to the application. click "Item Details",and click "Add Location".');
                                StopQuery = 1; 
                                }
                            } 
                             if((LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Notification' ||
                                 LineItems[i].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit') &&
                                 LineItems[i].SOP_Status__c != 'Ready to Submit')
                            {
                                Application.addError('Please add Standard Operating Procedures / Design Protocols on the following line item '+ 
                                LineItems[i].Name  + ' before you submit the application for approval. '+ 
                                'Click back to the application. click "Item Details",and click "Add SOP/Design protocols"');
                                StopQuery = 1;                                         
                            } 

                            
                        }
                    } 
                    // End of changes Kishore
                    
                    System.debug('<<<<<<<<<<<<<<<<<<<<<<<<<<< Attach PDF Process Start >>>>>>>>>>>>>>>>>>>');
                    //The Update now happens through payment page
                     If(StopQuery==0)
                     {
                         PageReference appPayment = new PageReference('/apex/CARPOL_VS_ApplicationFee_Payment?id=' + applicationID);
                         appPayment.setRedirect(true);
                         return appPayment;
                        //update application;
                        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Application submitted successfully.')); 
                     }
                    
                 }   
                
                else if(application.Application_Status__c == 'Submitted')
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Your application is already submitted and cannot be submitted again.'));
                }
            }
            return null;
        
        }    
        catch(Exception e)
        {
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception Occured : ' + e));
           // Application.addError('Something went wrong. Please re-try or contact your System Administrator.' + e);
            return null;
        }
    }
    
    public PageReference goBackApplication()
    {
        PageReference applicationDetailsRedirect = new PageReference('/' + application.Id);
        applicationDetailsRedirect.setRedirect(true);
        return applicationDetailsRedirect;
    }

}