@isTest(seealldata=true)
private class CARPOL_VS_Ports_extension_Test {
      @IsTest static void CARPOL_VS_Ports_extension_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

          CARPOL_VS_TestDataManager testData = new CARPOL_VS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          fac.account__C=objacct.id;
          update fac;
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp); 
         

          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          //objauth.Expiration_Date__c=date.today()+30;
          objauth.Effective_Date__c=date.today();
          objauth.Expiration_Timeframe__c='365';
          update objauth;
          ac1.Authorization__c = objauth.id;
          update ac1;    
         Facility_Vet_Services_Junction__c facvetjn = new Facility_Vet_Services_Junction__c();
          facvetjn.Facility__c= fac2.id;
          facvetjn.Vet_Services__c= objacct.id;
          facvetjn.Authorization__c=objauth.id;
          insert facvetjn;
          Facility_Vet_Services_Junction__c objFSV=new Facility_Vet_Services_Junction__c();
 
          objFSV.Vet_Services__c= objacct.id;
          objFSV.Authorization__c=objauth.id;
          objFSV.Facility__c=fac.id;
          insert objFSV;
          System.assert(facvetjn != null);
          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          Group__c objgroup = testData.newgroup();
          
          Test.startTest();
          PageReference pageRef = Page.CARPOL_AddPortsToGroup;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
          ApexPages.currentPage().getParameters().put('Id',objauth.id);
          CARPOL_VS_Ports_extension extclass = new CARPOL_VS_Ports_extension(sc);
                 
          extclass.getPorts();
          extclass.allportsete.add(new SelectOption(fac.id, fac.id)); 
          extclass.getVets();
          extclass.selectedVetOptions=new List<SelectOption>(); 
          extclass.selectedVetOptions.add(new SelectOption(fac.id, fac.Name));
          extclass.savevets();
          system.assert(extclass != null);    
          extclass.leftVetLabel='test';
          extclass.rightVetLabel='test';
          extclass.Val='test';
          extclass.selectedVetOptions=new List<SelectOption>(); 
          extclass.savevets();
           
          extclass.getVets();
    
          Test.stopTest();     
      }
}