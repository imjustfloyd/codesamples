@isTest(seealldata=true)
private class Portal_Link_Auth_Regulation_DetailTest {
      @IsTest static void Portal_Link_Auth_Regulation_DetailTest() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);          
          Authorizations__c objauth = testData.newauth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Link_Authorization_Regulation__c lar = new Link_Authorization_Regulation__c();
          lar.Authorization__c = objauth.id;
          lar.Regulation__c = objreg1.id;
          insert lar;
          
          Application_Condition__c ac = new Application_Condition__c();
        //ac.Status__c = 'Waiting on Customer';
        //ac.Associate_Application__c = objapp.id;
        //ac.Line_Item__c = objLineItem.id;
          ac.Condition_Number__c = 1;
          ac.Agree__c = 'Agree';
          ac.Link_Authorization_Regulation__c = lar.id;
          insert ac;
          
          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 

          //run as salesforce user
              PageReference pageRef = Page.Portal_Link_Auth_Regulation_Detail;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(lar);
              ApexPages.currentPage().getParameters().put('id',lar.id);

              Portal_Link_Auth_Regulation_Detail extclass = new Portal_Link_Auth_Regulation_Detail(sc);
              extclass.getConditions();
              extclass.getSelected();
              extclass.GetSelectedConditions();
              extclass.toSave();
              extclass.ID = lar.id;
              extclass.condition = ac;
              System.assert(extclass != null);                     
          Test.stopTest();   
      }
}