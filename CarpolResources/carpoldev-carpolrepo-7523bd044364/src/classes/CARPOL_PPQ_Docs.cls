public class CARPOL_PPQ_Docs {

        public Map<String,documentResource> mapRequiredDocuments { get; set;}
    public List<SelectOption> remainingDocuments { get; set;}
    public List<SelectOption> requiredDocuments { get; set; }
    public Boolean renderRemainingDocuments { get; set; }
    public String[] selectedDocuments { get; set; }
    public ID lineItemID;
    public AC__c lineItem {get; set;} 
    public String strFileContains {get; set;}
    public Map<String,documentResource> mapUploadedDocuments = new Map<String,documentResource>();
    public List<documentResource> docResource = new List<documentResource>();
    public String strSelectedDocuments = '';
    
    public Attachment uploadedFile {
        get {
            if (uploadedFile == null) uploadedFile = new Attachment();
            return uploadedFile;
        }
        set;
    }
    
    
    public CARPOL_PPQ_Docs(ApexPages.StandardController controller) {
    
         System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        lineItemID = ApexPages.currentPage().getParameters().get('id');
        
        if(lineItemID != null)
        {
            lineItem = [SELECT Name, Application_Number__c, Application_Number__r.Name FROM AC__c WHERE ID = :lineItemID LIMIT 1];
        }        
        else{ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Line Item found.'));}
        
        mapRequiredDocuments = new Map<String,documentResource>();
        //remainingDocuments = new List<SelectOption>();
        selectedDocuments = new List<String>();
        requiredDocuments = new List<SelectOption>();
        requiredDocuments = getRequiredDocument();
        System.Debug('<<<<<<< Total Required Documents : ' + requiredDocuments.size() + ' >>>>>>>');
        getExistingDocuments();
    }
    
    //Getting existing uploaded documents if any
    public void getExistingDocuments() {
        
        remainingDocuments = new List<SelectOption>();
        List<Applicant_Attachments__c> appAttachments = [SELECT Id, Name, Document_Types__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c = :lineItemID];
        System.Debug('<<<<<<< Total Uploaded Documents : ' + appAttachments.size() + ' >>>>>>>');
        if(appAttachments.size() > 0)
        {
            for(Applicant_Attachments__c appAtt : appAttachments)
            {
                System.Debug('<<<<<<< Uploaded Documents : ' + appAtt.Document_Types__c + ' >>>>>>>');
                String[] documents = appAtt.Document_Types__c.split(';');
                System.Debug('<<<<<<< Uploaded Documents Stack : ' + documents.size() + ' >>>>>>>');
                List<Attachment> att = [SELECT ID, Name FROM Attachment WHERE ParentId = :appAtt.ID LIMIT 1];
                documentResource docRes = new documentResource();
                if(att.size() > 0)
                {
                    docRes.docURL = '/servlet/servlet.FileDownload?file=' + att[0].ID;
                    docRes.isUploaded = true;
                }
                for(Integer j = 0; j < documents.size(); j++)
                {
                    mapUploadedDocuments.put(documents[j],docRes);
                    System.Debug('<<<<<<< mapUploadedDocuments Size  : ' + mapUploadedDocuments.size() + ' >>>>>>>');
                }
            }
        }

        if(requiredDocuments.size() > 0)
        {
            for(Integer i = 0 ; i < requiredDocuments.size(); i++)
            {
                String documentName = String.ValueOf(requiredDocuments[i].getLabel());
                System.Debug('<<<<<<< Display documentName  : ' + documentName + ' >>>>>>>');
                documentResource docResTemp = mapUploadedDocuments.get(requiredDocuments[i].getLabel());
                System.Debug('<<<<<<< docResTemp  : ' + docResTemp + ' >>>>>>>');
                if(docResTemp == null)
                {
                    docResTemp = new documentResource();
                    docResTemp.isUploaded = false;
                    remainingDocuments.add(new SelectOption(documentName, documentName));
                }
                mapRequiredDocuments.put(documentName, docResTemp);
                System.Debug('<<<<<<< mapRequiredDocuments Size  : ' + mapRequiredDocuments.size() + ' >>>>>>>');
            }
        }
        if(remainingDocuments.size() > 0){renderRemainingDocuments = true;}else{renderRemainingDocuments = false;}
    }
    
    public PageReference uploadFile() {
        
        System.Debug('<<<<<<< Selected File Name : ' + uploadedFile.Name + ' >>>>>>>');
        if(uploadedFile.Name != null)
        {
            selectedDocuments = getSelectedDocuments();
            System.Debug('<<<<<<< selectedDocuments  : ' + selectedDocuments.size() + ' >>>>>>>');
            System.Debug('<<<<<<< strSelectedDocuments  : ' + strSelectedDocuments + ' >>>>>>>');
            if(selectedDocuments.size() != 0)
            {
                Applicant_Attachments__c appAttach = new Applicant_Attachments__c();
                appAttach.RecordTypeID = [SELECT ID FROM RecordType WHERE sObjectType = 'Applicant_Attachments__c' AND Name = 'PPQ Attachments' LIMIT 1].ID;
                appAttach.Application__c = lineItem.Application_Number__c;
                appAttach.File_Name__c = uploadedFile.Name;
                appAttach.File_Description__c = 'Uploaded from a VF page on ' + System.Now();
                appAttach.Document_Types__c = strSelectedDocuments;
                appAttach.Animal_Care_AC__c = lineItemID;
                
                try
                {
                    insert appAttach;
                    
                    uploadedFile.OwnerId = UserInfo.getUserId();
                    uploadedFile.ParentId = appAttach.ID;
                    uploadedFile.Description = 'Uploaded Document';
                    insert uploadedFile;
                    uploadedFile = new Attachment();
                    
                    selectedDocuments.clear();
                    getExistingDocuments();
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully.'));
                    
                    //calling the future method to insert attachment to the right record
                    //insertAttachment();
                }
                catch(Exception e)
                {
                    System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
                }
            }
            else
            {
                uploadedFile = new Attachment();
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the documents which are avilable in the selected file.'));
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No file found. Please select a file to upload.'));
        }
        return null;
    }
    
    public List<SelectOption> getRequiredDocument() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Compliance Agreement','Compliance Agreement'));
        //options.add(new SelectOption('Rabies Vaccination Certificate (AC7042)','Rabies Vaccination Certificate (AC7042)'));
        //options.add(new SelectOption('IACUC Approved Research Proposal','IACUC Approved Research Proposal'));
        //options.add(new SelectOption('Research Justification','Research Justification'));
        //options.add(new SelectOption('Veterinary Treatment Agreement','Veterinary Treatment Agreement'));
        return options;
    }
    
    public String[] getSelectedDocuments() {
    
        System.debug('<<<<<<< Selected Documents List Size : ' + selectedDocuments.size());
        
        if(selectedDocuments.size() != 0)
        {
            strSelectedDocuments = '';
            for(Integer i = 0; i < selectedDocuments.size(); i++)
            {
                strSelectedDocuments += selectedDocuments[i] + ';';
            }
            //removing the last '; '
            strSelectedDocuments = strSelectedDocuments.substring(0,strSelectedDocuments.length() - 1);
        }
        return selectedDocuments;
    }
    
    
    //Class to hold uploaded documents information
    public class documentResource{
        public ID docID {get;set;}
        public Boolean isUploaded {get;set;}
        public string docName {get;set;}
        public String docURL {get;set;} 
    }

}