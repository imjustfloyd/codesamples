public class CARPOL_UNI_ViewReviewerOverride {
  public CARPOL_UNI_ViewReviewerOverride() {

  }


 String recordId;

public CARPOL_UNI_ViewReviewerOverride(ApexPages.StandardController
     controller) {recordId = controller.getId();}

public PageReference redirect() {
Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
//if ('State Reviewer'.equals(p.name))
if (p.name == 'State Reviewer' || p.name == 'State Plant Health Director' )
    {
     PageReference customPage =
Page.CARPOL_BRS_StateReviewRecords;
     customPage.setRedirect(true);
     customPage.getParameters().put('id', recordId);
     return customPage;
    } else {
    String hostname = ApexPages.currentPage().getHeaders().get('Host');
           String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
           pagereference pageref = new pagereference(optyURL2);
           pageref.setredirect(true);
           return pageref;

       return null; //otherwise stay on the same page
    }
 }
}