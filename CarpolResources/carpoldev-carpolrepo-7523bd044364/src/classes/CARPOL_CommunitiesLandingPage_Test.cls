/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - Community Landing Page Test Class
*/

@isTest(seealldata=true)
private class CARPOL_CommunitiesLandingPage_Test {
    
    static testMethod void CARPOL_CommunitiesLandingPageTest() {
      
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port'); 
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newlineitem('Personal Use',objapp);
        
       User usershare = new User();
       usershare.Username ='aphistestemail@test.com';
       usershare.LastName = 'APHISTestLastName';
       usershare.Email = 'APHISTestEmail@test.com';
       usershare.alias = 'APHItest';
       usershare.TimeZoneSidKey = 'America/New_York';
       usershare.LocaleSidKey = 'en_US';
       usershare.EmailEncodingKey = 'ISO-8859-1';
       usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
       usershare.LanguageLocaleKey = 'en_US';
       usershare.ContactId = objcont.id;
       insert usershare;    
       
       System.runAs(usershare){
        PageReference pageRef = Page.CARPOL_CommunityLandingPage;
        Test.setCurrentPage(pageRef);
        CARPOL_CommunitiesLandingPage commLandingPage = new CARPOL_CommunitiesLandingPage();         
        commLandingPage.editContact();
        system.assert(commLandingPage != null);
       }     
    }
}