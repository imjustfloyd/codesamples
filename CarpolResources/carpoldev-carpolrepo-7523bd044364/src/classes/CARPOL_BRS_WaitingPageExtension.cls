public class CARPOL_BRS_WaitingPageExtension {

/*
// Author : Angela Carr
// Created Date : 4/13/2015
// Purpose : This is used to display Original Application PDF pertaining to BRS
*/
   
    public Application__c application; 
    public AC__c LineItem; 
    public User getUser;  
    public Location__c location;
    public Construct__c construct; 
    public Design_Protocol_Record__c designProtocol; 
      
    public ID applicationID;
    public ID LineItemID;
    public Boolean disableButton{get; set;} 
    public list<Location__c> lstOrigins {get;set;}
    public list<Location__c> lstDestinations {get;set;}
    public list<Location__c> lstReleases {get;set;}
    public list<Location__c> destorgLocations {get;set;}
    public list<Construct__c> lstConstruct {get;set;} // 
    public list<Design_Protocol_Record__c> lstDesign {get;set;}
    public list<Applicant_Attachments__c> lstAppAttach {get;set;}
    public id AppId;
    public Id strPathwayId{get;set;}
    public Map<String, String> brsstatusurl{get;set;}
    string waitingStatus = 'Waiting on Customer';
    public list<Phenotype__c> lstPhenoTypes {get;set;}
    public list<Genotype__c> lstGenotypes {get;set;}
    public list<Id> conIdList = new list<Id>();
    
    // Standard Controller Constructor 
    public CARPOL_BRS_WaitingPageExtension(ApexPages.StandardController controller) {
        try
        {
            // Declare Variables
        // applicationID = ApexPages.currentPage().getParameters().get('ID');
        LineItemID = ApexPages.currentPage().getParameters().get('id');
        string waitingStatus = 'Waiting on Customer';
        
        lstConstruct = new list<Construct__c>();
        lstOrigins = new list<Location__c>();
        lstDestinations = new list<Location__c>();
        destorgLocations = new list<Location__c>();
        lstReleases = new list<Location__c>();   
        lstDesign = new list<Design_Protocol_Record__c>(); 
        lstAppAttach = new list<Applicant_Attachments__c>();
        lstPhenoTypes = new list<PhenoType__c>();
        lstGenotypes = new list<Genotype__c>();
        disableButton = true;  

            System.Debug('<<<<<<< Application ID : ' + LineItemId + ' >>>>>>>');
            if(LineItemID != null)
            {
                // Load Application Data
               // application = [SELECT ID, Applicant_Name__r.Name, Applicant_Instructions__c, Application_Status__c FROM Application__c where Application_Status__c=:waitingStatus and ID = :applicationID LIMIT 1];
                LineItem = [select ID,Applicant_Instructions__c,Status__c from AC__c where status__c=:waitingStatus and ID =: LineItemID limit 1 ];
                system.debug('LineItemId #####'+LineItemId);
                if (LineItem!= null) {
                 disableButton = false;
                // return disableButton;
                 }
               
                 
                
                //Load Locations
                    // Load Origins
                        lstOrigins = [select ID,Name,City__c,Applicant_Instructions__c, Status__c  from Location__c where  Status__c=:waitingStatus and RecordType.DeveloperName='Origin_Location' and Line_Item__c =:LineItemID];
  
                     // Load Destinations
                        lstDestinations = [select ID,Name, Applicant_Instructions__c, Description__c,  Location_Id__c, Status__c    from Location__c where  Status__c=:waitingStatus and RecordType.DeveloperName='Destination_Location' and Line_Item__c =:LineItemID];
                        
                      // Load Design Protocols
                    //    lstDesign = [select id,Name,Attaching_or_Entering_Design_Protocols__c,Corrections_Required__c,Design_Protocol_Name__c,Status__c  From Design_Protocol_Record__c where Status__c=:waitingStatus and Line_Item__c =:LineItemID];
  
                         // Load Destinations & Origins
                        destorgLocations = [select ID,Name,Applicant_Instructions__c,  Status__c  from Location__c where Status__c=:waitingStatus and RecordType.DeveloperName='Origin_and_Destination' and Line_Item__c =:LineItemID];
                      
  
                     // Load Release Sites
                        lstReleases = [select ID,Name,Applicant_Instructions__c,  Status__c  from Location__c where Status__c=:waitingStatus and RecordType.DeveloperName='Release_Location' and Line_Item__c =:LineItemID];
                        
                    // Load Constructs,Genotypes and Phenotypes   
                    //lstConstruct = [select id,Construct_s__c,Corrections_Required__c,Status__c From Construct__C where Status__c=:waitingStatus and Line_Item__c =:LineItemID];
                   lstConstruct = [select id,Construct_s__c,Corrections_Required__c,Status__c,(Select id, Name,Applicant_Instructions__c from Phenotypes__r),(Select id, Name, Applicant_Instructions__c from Genotypes__r) From Construct__C where Status__c=:waitingStatus and Line_Item__c =:LineItemID];
                    system.debug('lstConstruct #####'+lstConstruct);
                    
                    //Load Applicant Attachments
                    lstAppAttach = [select id,Name,Applicant_Instructions__c,Status__c from Applicant_Attachments__c where Status__c =:waitingStatus and (RecordType.DeveloperName='BRS_Notification_SOP' OR RecordType.DeveloperName='BRS_Standard_Permit_SOP') and Animal_Care_AC__c =:LineItemID];
              
                  for(Construct__c c: lstConstruct){
                  conIdList.add(c.Id);
                  }
                  
                  system.debug('conIdList $$$$'+conIdList);
                  lstPhenoTypes = [select id,Name,Applicant_Instructions__c,Construct__c,Construct__r.Name from Phenotype__c where Construct__c in:conIdList ];
                  system.debug('lstPhenoTypes $$$$'+lstPhenoTypes);
                  
                  lstGenotypes = [select id,Name,Applicant_Instructions__c,Related_Construct_Record_Number__c,Related_Construct_Record_Number__r.Name from Genotype__c where Related_Construct_Record_Number__c in:conIdList ];
                  system.debug('lstPhenoTypes $$$$'+lstGenotypes);
       
        }           
    }
    catch(Exception e)
    {
        System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>');
    }
    }   
    
   
    public pageReference Retlineitem(){
       System.Debug('<<<<<<< INSIDE Retlineitem() method :  >>>>>>'); 
       brsstatusurl = ApexPages.currentPage().getParameters();
        if(brsstatusurl.containsKey('appid')){
            AppId = brsstatusurl.get('appid');
        } 
        if(brsstatusurl.containsKey('id')){
            LineItemId = brsstatusurl.get('id');
        }        
        System.Debug('<<<<<<< appId : ' + appId + ' >>>>>>');
    pageReference pglineitem = Page.carpol_uni_lineitem;
    pglineitem.getParameters().put('appId',AppId);
    pglineitem.getParameters().put('ID',LineItemId);
    pglineitem.setRedirect(true);
    return pglineitem;
    } 
    
    public PageReference doSave() { 
  
    string newStatus = '';
    string subStatus = '';
    // save the ref to the agrement they agreed to
   //  application = [SELECT ID, Applicant_Name__r.Name,Bio_Tech_Reviewer__c,Biotechnologist_Reviewer_Email__c,Bio_Tech_Reviewer__r.Email,Bio_Tech_Reviewer__r.Contact.Email,Application_Status__c,name,Bio_Tech_Reviewer__r.FirstName,RecordType.DeveloperName FROM Application__c where  ID = :applicationID LIMIT 1];
     LineItem = [select ID,Authorization__c,Applicant_Instructions__c,Status__c,RecordType.DeveloperName from AC__c where ID =: LineItemID limit 1 ];
      system.debug('## LineItem.Authorization__c  ###'+LineItem.Authorization__c );
      Workflow_Task__c wt = [select id,Status__c,Status_Categories__c,Update_Record_to_Specific_Value__c,In_Progress_Status__c,Update_Related_Record_to_Specific_Value__c,Authorization__c from Workflow_Task__c where Status__c = 'Pending' and Status_Categories__c = 'Customer Feedback'  and Authorization__c =: LineItem.Authorization__c  limit 1];
   
     if((LineItem.RecordType.DeveloperName == 'BRS_NOTIF_RT') || (LineItem.RecordType.DeveloperName == 'BRS_COURTESY_RT') || (LineItem.RecordType.DeveloperName == 'BRS_PERMIT_RT')) {
        newStatus = 'Submitted';
       // subStatus = wt.Update_Record_to_Specific_Value__c; //Kishore Commented 10/21/2015
       // subStatus = wt.Update_Related_Record_to_Specific_Value__c; //Kishore Commented 10/26/2015
       subStatus = wt.In_Progress_Status__c;
        }
    
     LineItem.Status__c = newStatus;
     LineItem.Applicant_Instructions__c = '';
     
     System.Debug('<<<<<<< ' + applicationID+' >>>>>>');
       for(Location__c location : lstOrigins)
    {
      if(subStatus !=null)
      location.status__C = subStatus;
      location.Applicant_Instructions__c = '';
    }
    update lstOrigins;
    for(Location__c location : lstReleases)
    {
      location.status__C = subStatus;
      location.Applicant_Instructions__c = '';
    }
    update lstReleases;
    for(Location__c location : destorgLocations)
    {
      if(subStatus !=null)
      location.status__C = subStatus;
      location.Applicant_Instructions__c = '';
    }
    update destorgLocations;
    for(Location__c location : lstDestinations)
    {
      if(subStatus !=null)
      location.status__C = subStatus;
      location.Applicant_Instructions__c = '';
    }
    update lstDestinations;

     for(Construct__c construct : lstConstruct)
    {
      if(subStatus !=null)
      construct.status__C = subStatus;
      construct.Corrections_Required__c = '';
    }
    update lstConstruct;
    /*  for(Design_Protocol_Record__c designProtocol : lstDesign)
    {
      if(subStatus !=null)
      designProtocol.status__C = subStatus;
      designProtocol.Corrections_Required__c = '';
    }
    update lstDesign; */
    
    for(Applicant_Attachments__c att:lstAppAttach)
    {
        if(subStatus !=null)
      att.status__C = subStatus;
      att.Applicant_Instructions__c = '';
    }
    update lstAppAttach;
   
/*  Valid code BRS not interested in email any longer. Leaving for other domains; if needed    
  OrgWideEmailAddress owe = [SELECT ID,IsAllowAllProfiles,DisplayName,Address FROM OrgWideEmailAddress WHERE IsAllowAllProfiles = TRUE LIMIT 1];
  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       
       
       String emailAddr = application.Biotechnologist_Reviewer_Email__c;
        
        String newOwnerName = application.Bio_Tech_Reviewer__r.FirstName;
        String[] toAddresses = new String[] {emailAddr};
        mail.setOrgWideEmailAddressId(owe.Id);
        mail.setToAddresses(toAddresses);

        mail.setSubject('Application has been reviewed and resubmitted  : ' + application.Name);

        mail.setPlainTextBody('Application has been reviewed and resubmitted  : ' + application.Name);
        mail.setHtmlBody('Application has been reviewed and resubmitted  : <b>' + application.Name+'</b>');

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
 */ 
    system.debug('LineItem.Authorizations__c ####'+LineItem.Authorization__c);
    
    Authorizations__c auth = [select id,Locked__c from Authorizations__c where id =:LineItem.Authorization__c limit 1 ];
    auth.Locked__c = 'NO';
    update auth;
    
    
    wt.Status__c = 'In Progress';
    wt.Status_Categories__c = '';
    update wt;
    system.debug('Workflow Task #### '+ wt);
    
    
    update LineItem;       
    PageReference appDetails = new PageReference('/' + LineItemID);
    appDetails.setRedirect(true);
    return appDetails;
     
  }
    
}