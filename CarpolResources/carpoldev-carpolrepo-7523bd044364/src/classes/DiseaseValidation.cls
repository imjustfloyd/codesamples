//Disease Validation

public class DiseaseValidation {
    
    public Program_Line_Item_Pathway__c programlineItemPathway;
   // Public list<Disease_Sensitivity__c> Disease_SensitivityList;
     Public list<Regulations_Association_Matrix__c> decisionMatrixList;
    Public list<ThumbPrint> ThumbPrintList;
    public string diseaselist;
    //Set <ID> CountryGroupIds = new Set<ID>();

  
    public list<ThumbPrint> checkForDisease(String country,String state,String county,String ProgPathID,String selIntendedUse,String SelectedRegulatedArticle, Set <ID> CountryGroupIds){
       
       system.debug('Entering DiseaseValidation Class ###');
       system.debug('Entering country ###'+country);
       system.debug('Entering state ###'+state);
       system.debug('Entering county ###'+county);
       system.debug('Entering ProgPathID ###'+ProgPathID);
       system.debug('Entering selIntendedUse ###'+selIntendedUse);
       system.debug('Entering SelectedRegulatedArticle ###'+SelectedRegulatedArticle);
       system.debug('Entering CountryGroupIds ###'+CountryGroupIds);
       
       String selIntendedUseId;
       Set<String> setCountries=new Set<String>();
       String selRegulatedarticleID;
   /*    if(country!=null)
           setCountries.add(country);
       for(Country__c recCon:[select Id,Master_Country__c from Country__c where Id=:country])
           if(recCon.Master_Country__c!=null)
               setCountries.add(recCon.Master_Country__c); */
               
       if(selIntendedUse != null){
            
            selIntendedUseId = [Select Id from Intended_Use__c where Name =:selIntendedUse and Program_Line_Item_Pathway__c=:ProgPathID limit 1].ID;
            system.debug('--------selIntendedUseId'+selIntendedUseId);
        }
        if(SelectedRegulatedArticle !=null)
        {
            //selRegulatedarticleID = [Select Id from Regulated_Article__c where Name =:SelectedRegulatedArticle and  Program_Pathway__c=:ProgPathID limit 1].ID; //commented by niharika 3/29/2016 since we no more have pathway on RA object but using the Junction object.
            selRegulatedarticleID = [Select Regulated_Article__c from RA_Scientific_Name_Junction__c where Regulated_Article__c=:SelectedRegulatedArticle and  Program_Pathway__c=:ProgPathID limit 1].Regulated_Article__c;
            system.debug('--------selRegulatedarticleID'+selRegulatedarticleID);
        }
        
        programlineItemPathway = [SELECT ID,Validate_for_Disease__c,Is_Country_Required__c,Is_State_Required__c,Program__c,Parent_Id__c,Show_Intended_Use__c,Display_Regulated_Article_Search__c
                                  FROM Program_Line_Item_Pathway__c WHERE Id =: ProgPathID ];
        Boolean intededUse =  programlineItemPathway.Show_Intended_Use__c;                       
        Boolean regulatedarticle = programlineItemPathway.Display_Regulated_Article_Search__c;

        /*Disease_SensitivityList = new list<Disease_Sensitivity__c>();
        Disease_SensitivityList = [SELECT Country__c,Country__r.Name,Disease__c,Disease__r.Name,IsActive__c,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,
                                    Level_2_Region__r.Name,Outcome__c,Status__c,Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c
                                    FROM 
                                    Disease_Sensitivity__c 
                                    WHERE 
                                    Program_Line_Item_Pathway__c =:ProgPathID  AND Country__c in:country AND Level_1_Region__c=:state AND Level_2_Region__c=:county];*/
                                    
      decisionMatrixList = new  list<Regulations_Association_Matrix__c>();
     // system.debug('----------country,String state,String county,String ProgPathID--'+country+'---'+state+'-----'+ county+'----'+ ProgPathID+'---county.length()'+county.length());
     // system.debug('programlineItemPathway.Is_Country_Required__c--'+programlineItemPathway.Is_Country_Required__c);
     //  system.debug('programlineItemPathway.Is_State_Required__c--'+programlineItemPathway.Is_State_Required__c);
     // if(country != null && state != null && county != null  )
     system.debug('----county'+county);
      
      

      
      
      if((programlineItemPathway.Is_Country_Required__c)  && (programlineItemPathway.Is_State_Required__c) &&  (county == null|| county == '')){
          
      
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state and Condition__c!=:null and Disease_Statys__c!=null];
                            
      if(selIntendedUseId !=null && selRegulatedarticleID != null)
        {
          decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c,Regulated_Article__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                               Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state  AND  Intended_Use__c =:selIntendedUseId AND Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
      }
      else if(selIntendedUseId !=null)
          decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state  AND  Intended_Use__c =:selIntendedUseId and Condition__c!=:null and Disease_Statys__c!=null];
          
      
     }
     
     
     
     
     
     
     
     
     
     
     else if( selRegulatedarticleID != null)
     {
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c,Regulated_Article__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                               Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state  AND Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
      }
      
      
      
      
      
      
      
      
      
      
      
     if((programlineItemPathway.Is_Country_Required__c)  && (programlineItemPathway.Is_State_Required__c) && (county != null&& county !='') ){
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state AND Level_2_Region__c=:county and Condition__c!=:null and Disease_Statys__c!=null];
      if(selIntendedUseId !=null && selRegulatedarticleID != null)
          decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state AND Level_2_Region__c=:county AND  Intended_Use__c =:selIntendedUseId AND Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
       else if(selIntendedUseId != null)
          decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state AND Level_2_Region__c=:county AND  Intended_Use__c =:selIntendedUseId and Condition__c!=:null and Disease_Statys__c!=null];
        else if(selRegulatedarticleID != null)
          decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries AND Level_1_Region__c=:state AND Level_2_Region__c=:county  AND Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
              
     
     }                        
                            
                     
                     
                     
                     
                     
                     
                     
                     
                            
                            
      if((programlineItemPathway.Is_Country_Required__c)  && (programlineItemPathway.Is_State_Required__c==False)){

        
        if(selIntendedUseId !=null && selRegulatedarticleID != null){
            decisionMatrixList = [SELECT Country__c,Country__r.Name,Country_Group__c,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Program_Line_Item_Pathway__c =:ProgPathID AND Country__c =:country AND  Intended_Use__c =:selIntendedUseId AND Regulated_Article__c =:selRegulatedarticleID and Condition__c!=:null and Disease_Statys__c!=null];
            system.debug('-----decisionMatrixList1:--'+decisionMatrixList);
             if(decisionMatrixList.isEmpty() )
                decisionMatrixList = [SELECT Country__c,Country__r.Name,Country_Group__c,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                    Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                    FROM
                                    Regulations_Association_Matrix__c 
                                    WHERE 
                                    Program_Line_Item_Pathway__c =:ProgPathID AND Country_Group__c in: CountryGroupIds AND  Intended_Use__c =:selIntendedUseId AND Regulated_Article__c =:selRegulatedarticleID and Condition__c!=:null and Disease_Statys__c!=null];
                system.debug('-----decisionMatrixList1.5:--'+decisionMatrixList);
          }else if(selIntendedUseId != null){
                    decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                        Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                        FROM
                                        Regulations_Association_Matrix__c 
                                        WHERE 
                                        Program_Line_Item_Pathway__c =:ProgPathID AND Country__c =:country  AND  Intended_Use__c =:selIntendedUseId and Condition__c!=:null and Disease_Statys__c!=null ];
                  
                       system.debug('-----decisionMatrixList3:--'+decisionMatrixList)  ;
          }else if(selRegulatedarticleID != null){
                    decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                        Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                        FROM
                                        Regulations_Association_Matrix__c 
                                        WHERE 
                                        Program_Line_Item_Pathway__c =:ProgPathID AND Country__c =:country  AND Regulated_Article__c =:selRegulatedarticleID and Condition__c!=:null and Disease_Statys__c!=null ]; 
                               system.debug('-----decisionMatrixList4:--'+decisionMatrixList)  ;   
          }
         

        if(decisionMatrixList.isEmpty() )
              decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                    Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                    FROM
                                    Regulations_Association_Matrix__c 
                                    WHERE 
                                    Program_Line_Item_Pathway__c =:ProgPathID AND Country__c =:country and Condition__c!=:null and Disease_Statys__c!=null];
                            
        system.debug('-----decisionMatrixList1:--'+decisionMatrixList)  ;         
      } 
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
      if((programlineItemPathway.Is_Country_Required__c==false)  && (programlineItemPathway.Is_State_Required__c==False)){
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c in:setCountries and Condition__c!=:null and Disease_Statys__c!=null];
                            
        system.debug('-----decisionMatrixList:--'+decisionMatrixList)  ;
        
        if(selIntendedUseId !=null && selRegulatedarticleID != null)
            decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Intended_Use__c =:selIntendedUseId AND Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
        else if(selIntendedUseId != null)
            decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Intended_Use__c =:selIntendedUseId and Condition__c!=:null and Disease_Statys__c!=null];
        else if(selRegulatedarticleID != null)
            decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                                Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c,Warning_Message__c
                                FROM
                                Regulations_Association_Matrix__c 
                                WHERE 
                                Regulated_Article__c =:SelectedRegulatedArticle and Condition__c!=:null and Disease_Statys__c!=null];
         system.debug('-----decisionMatrixList:--'+decisionMatrixList)  ;   
      } 
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
                            
      /*if(country != null && state == null && county == null)
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c=:country
                            
        system.debug('-----'+decisionMatrixList)  ;                  
                            
      if(country != null && state != null && county == null)
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c=:country AND Level_1_Region__c=:state]; 
                            
        if(country != null && state == null && county != null)
      decisionMatrixList = [SELECT Country__c,Country__r.Name,Level_1_Region__c,Level_1_Region__r.Name,Level_2_Region__c,Level_2_Region__r.Name,Condition__c,
                            Thumbprint__c ,Thumbprint__r.Response_Type__c,Thumbprint__r.Communication_Manager__c,Disease_Statys__c
                            FROM
                            Regulations_Association_Matrix__c 
                            WHERE 
                            Program_Line_Item_Pathway__c =:ProgPathID AND Country__c=:country AND Level_2_Region__c=:county];                     
                            
         */                   
                                    
      ThumbPrintList = new list<ThumbPrint>();   
      
      for(Regulations_Association_Matrix__c ds: decisionMatrixList){
          
          if(diseaselist==null || diseaselist==''){
            diseaselist = ds.Condition__c;
          }
          else if(diseaselist!=null || diseaselist!=''){
              diseaselist = diseaselist+'_'+ds.Condition__c;
          }
          system.debug('<!!! DS Condition__c in disease validation class'+diseaselist);
          ThumbPrint tp = new ThumbPrint(diseaselist,ds.Country__r.Name,ds.Level_1_Region__r.Name,ds.Level_2_Region__r.Name,ds.Condition__c,ds.Thumbprint__r.Response_Type__c,ds.Thumbprint__c,ds.Disease_Statys__c,ds.Thumbprint__r.Communication_Manager__c,intededUse,ds.Warning_Message__c,ds.Condition__c);
          ThumbPrintList.add(tp);
      }
      
    system.debug('---------------ThumbPrintList'+ThumbPrintList);                                
    if(ThumbPrintList.size()>0) 
        return ThumbPrintList;
     else
        return null;
    }
     
    public class ThumbPrint{
        
        String country;
        String state;
        String county;
        public String disease;
        public String diseaselist;
        public String diseaseStatus;
        public String thumbPrintResponseType;
        public String thumbPrintID;
        String communicationManagerID;
        public Boolean showIntendeduse;
        public String Warning_Message;
        public String IdDiseasePest;
        public ThumbPrint(String diseaselist, String con,String st,String county1,String dis,String responseType,String thumbid,String diseaseStatus,String communicationManager,Boolean intendeduse,String Warning_Message,String IdDiseasePest){
            
            this.country = con;
            this.state = st;
            this.county = county1;
            this.disease =dis;
            this.thumbPrintResponseType=responseType;
            this.thumbPrintID=thumbid;
            this.diseaseStatus=diseaseStatus;
            this.communicationManagerID=communicationManager;
            this.showIntendeduse=intendeduse;
            this.Warning_Message=Warning_Message;
            this.IdDiseasePest=IdDiseasePest;
            this.diseaselist = diseaselist;
            
            
        }
    }

}