public class CARPOL_AC_AttachLineItemPDF{
    public ID appId {get; set;}
    public Application__c app {get; set;}
    List<AC__c> aclst {get; set;}
    Map<String,String> lineItemGrouping = new Map<String,String>();
    String strACLineItemsUnique;
    String strLineItemsGroupingIds;
    List<AC__c> groupLineItemslst {get; set;}
    
    
    public CARPOL_AC_AttachLineItemPDF(ApexPages.StandardController controller){
    app = (Application__c)controller.getRecord();
    appId = ApexPages.currentPage().getParameters().get('id');
    System.debug('<<<<<<<<<<<<<<<<< AppID ' + appId + '>>>>>>>>>>>>>>>>>>>>>>>>>');
    System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
    getACRecords();
    
     for(Integer i = 0; i < aclst.size(); i++)
        {
            strACLineItemsUnique = aclst[i].Port_of_Entry__r.ID + '_' + aclst[i].Importer_Last_Name__r.ID + '_' + aclst[i].Exporter_Last_Name__r.ID + '_' + aclst[i].Proposed_date_of_arrival__c + '_' + aclst[i].RecordType.Name;
            if(lineItemGrouping.get(strACLineItemsUnique)==null){
            lineItemGrouping.put(strACLineItemsUnique, aclst[i].Id);
            System.debug('<<<<<<<<<<<<<<<  adding new entry to lineItemGrouping Map ' + strACLineItemsUnique + '>>>>>>>>>>>>>>>');
            }
            else {
            string tempACId = lineItemGrouping.get(strACLineItemsUnique);
            lineItemGrouping.remove(strACLineItemsUnique);
            tempACId += ','+aclst[i].Id;
            lineItemGrouping.put(strACLineItemsUnique, tempACId);
            System.debug('<<<<<<<<<<<<<<<  removing & adding updated entry to lineItemGrouping Map ' + strACLineItemsUnique + '>>>>>>>>>>>>>>>'); 
            }
        }
       System.debug('<<<<<<<<<<<<<<<  lineItemGrouping Map ' + lineItemGrouping.size() + '>>>>>>>>>>>>>>>'); 
       strLineItemsGroupingIds = lineItemGrouping.get(strACLineItemsUnique);
       //String[] results = strLineItemsGroupingIds.split(",");
       //groupLineItemslst = [SELECT * From AC__c where Id = :results];
    }
    
    public List<AC__c> getACRecords(){
        if(app.Id != null){
            aclst = [select ID, RecordType.Id, RecordType.Name, Exporter_First_Name__c,Exporter_Last_Name__r.Name, 
            Exporter_Mailing_CountryLU__r.Name, Exporter_Mailing_Street__c, Exporter_Mailing_City__c, 
            Exporter_Mailing_State_ProvinceLU__r.Name, Exporter_Mailing_Zip_Postal_Code__c,Exporter_Phone__c,
            Exporter_Fax__c, Exporter_Email__c, Importer_First_Name__c, Importer_Last_Name__r.Name,
            Importer_Mailing_CountryLU__r.Name, Importer_Mailing_Street__c, Importer_Mailing_City__c,
            Importer_Mailing_State_ProvinceLU__r.Name, Importer_Phone__c, Importer_Mailing_ZIP_code__c,
            Importer_Fax__c, Importer_Email__c, Importer_USDA_Registration_Number__c,
            Importer_USDA_Registration_Exp_Date__c, Importer_USDA_License_Number__c, 
            Importer_USDA_License_Expiration_Date__c, Purpose_of_the_Importation__c,
            Breed_Name__c, Sex__c, Age_yr_months__c, Age_in_months__c, Color__c,  Microchip_Number__c, Tattoo_Number__c, Other_identifying_information__c, 
            Transporter_Type__c, Port_of_Embarkation__r.Name, Transporter_Name__c,
            Air_Transporter_Flight_Number__c, Arrival_Time__c, Departure_Time__c,
            USDA_Registration_Number__c, USDA_Expiration_Date__c, USDA_License_Number__c,
            USDA_License_Expiration_Date__c, Port_of_Entry__r.Name, Proposed_date_of_arrival__c,
            DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__r.Name, 
            DeliveryRecipient_Mailing_CountryLU__r.Name, DeliveryRecipient_Mailing_Street__c, 
            DeliveryRecipient_Mailing_City__c, Delivery_Recipient_State_ProvinceLU__r.Name, DeliveryRecipient_Mailing_Zip__c,
            DeliveryRecipient_Phone__c, DeliveryRecipient_Fax__c, DeliveryRecipient_Email__c,
            DeliveryRecipient_USDA_Registration_Num__c, DeliveryRec_USDA_Regist_Expiration_Date__c,
            DeliveryRecipient_USDA_License_Number__c, DeliveryRec_USDA_License_Expiration_Date__c
            from AC__c where Application_Number__c = :app.Id];
        }
        
        return aclst;
    }

    public void savePDF(){
        if(app.Id != null){
            System.debug('<<<<<<<<<<<<<<<<< ID ' + app.Id + '>>>>>>>>>>>>>>>>>>>>>>>>>');
            PageReference pdf = Page.CARPOL_AC_ApplicationPDF;
            pdf.getParameters().put('id', app.Id);
            Attachment att = new Attachment();
            Blob fileBody;
                try {
                     fileBody = pdf.getContentAsPDF();
    
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                } 
                catch (VisualforceException e) {
                    fileBody = Blob.valueOf('Some Text');
                    System.debug('<<<<<<<<<<<<<<< Body ' + fileBody + '>>>>>>>>>>>>>>>');
                }
            att.Name = app.Name+'.pdf';
            att.ParentId = app.Id;
            att.body = fileBody;
            insert att;
        }
    }
    
}