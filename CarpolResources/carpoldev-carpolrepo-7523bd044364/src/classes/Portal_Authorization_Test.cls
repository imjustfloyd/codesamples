@isTest(seealldata=true)
public class Portal_Authorization_Test{
    static testMethod void updateauthcreaterecords(){
    String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
     CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Authorizations__c objauth = testData.newAuth(objapp.id);

          Program_Line_Item_Section__c mSection1 = new Program_Line_Item_Section__c();
          mSection1.Section_Order__c = 1;
          mSection1.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection1.Destination_Object__c = 'Authorization';
         
          insert mSection1;
          Program_Line_Item_Section__c mSection2 = new Program_Line_Item_Section__c();
          mSection2.Section_Order__c = 1;
          mSection2.RecordTypeId = Schema.SObjectType.Program_Line_Item_Section__c.getRecordTypeInfosByName().get('Authorization').getRecordTypeId();
          mSection2.Destination_Object__c = 'Line Item';
          insert mSection2;
          
          Program_Line_Item_Field__c mField1 = new Program_Line_Item_Field__c();
          mField1.isActive__c = 'Yes';
          mField1.Field_API_Name__c = 'ID';
          mField1.Name = 'Test';
          mField1.Program_Line_Item_Section__c = mSection1.id;
          insert mField1;
        
          Program_Line_Item_Field__c mField2 = new Program_Line_Item_Field__c();
          mField2.isActive__c = 'Yes';
          mField2.Field_API_Name__c = 'ID';
          mField2.Name = 'Test';
          mField2.Program_Line_Item_Section__c = mSection2.id;
          insert mField2;
        
        

        PageReference pg = Page.Portal_Authorization_Detail;
        //system.currentPageReference().getParameters().put('Id', auth.Id);
        Test.setCurrentPage(pg);

        ApexPages.StandardController std=new ApexPages.StandardController(objauth);
        ApexPages.currentPage().getParameters().put('Id',objauth.id);
        Portal_Authorization_Detail_controller core=new Portal_Authorization_Detail_controller(std);
        core.getDynamicPageBlock();
        System.assert(core != null);                
 }
}