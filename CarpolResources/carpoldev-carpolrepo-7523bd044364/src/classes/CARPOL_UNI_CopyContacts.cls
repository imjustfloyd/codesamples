public class CARPOL_UNI_CopyContacts {

    public static void copying(List<AC__C> LineList){
    
                AC__c LineItem = LineList[0];
                If ((LineItem.status__c == 'Ready to Submit' || LineItem.status__c == 'Saved')){
                  
                            String rtName = Schema.SObjectType.AC__c.getRecordTypeInfosByID().get(LineItem.Recordtypeid).getName();
                            Application__c app = [select Application_Status__c from Application__c where ID = :LineItem.Application_Number__c];
                            if(app.Application_Status__c == 'Open' && LineItem.Importer_Last_Name__c != null && LineItem.Exporter_Last_Name__c != null && LineItem.DeliveryRecipient_Last_Name__c != null){
                                    List <Applicant_Contact__c> AppCon = [SELECT ID, Name, First_Name__c, Phone__c, Fax__c, 
                                                                        Email_Address__c, Mailing_Country_LR__c, Mailing_Country_LR__r.Name, Mailing_Street__c, Mailing_State_LR__r.Name, 
                                                                        Mailing_City__c, Mailing_State_LR__c, Mailing_Zip_Postal_Code__c, 
                                                                        USDA_License_Number__c, USDA_License_Expiration_Date__c, USDA_Registration_Number__c,
                                                                        USDA_Registration_Expiration_Date__c FROM Applicant_Contact__c WHERE ID = :LineItem.Importer_Last_Name__c OR 
                                                                        ID = :LineItem.Exporter_Last_Name__c OR ID = :LineItem.DeliveryRecipient_Last_Name__c];
                                    
                                    for(Integer i = 0; i < AppCon.size(); i++)
                                        {
                                            if(LineItem.Importer_Last_Name__c == AppCon[i].ID){
                                                LineItem.Importer_First_Name__c = AppCon[i].First_Name__c;
                                                //ac.Importer_Last_Name__c = AppCon[i].Last_Name__c;
                                                LineItem.Importer_Email__c = AppCon[i].Email_Address__c;
                                                LineItem.Importer_Phone__c = AppCon[i].Phone__c;
                                                LineItem.Importer_Fax__c = AppCon[i].Fax__c;
                                                LineItem.Importer_Mailing_CountryLU__c = AppCon[i].Mailing_Country_LR__c;
                                                LineItem.Importer_Mailing_Street__c = AppCon[i].Mailing_Street__c;
                                                LineItem.Importer_Mailing_City__c = AppCon[i].Mailing_City__c;
                                                LineItem.Importer_Mailing_State_ProvinceLU__c = AppCon[i].Mailing_State_LR__c;
                                                LineItem.Importer_Mailing_ZIP_code__c = AppCon[i].Mailing_Zip_Postal_Code__c;
                                                LineItem.Importer_USDA_License_Expiration_Date__c = AppCon[i].USDA_License_Expiration_Date__c;
                                                LineItem.Importer_USDA_License_Number__c = AppCon[i].USDA_License_Number__c;
                                                LineItem.Importer_USDA_Registration_Exp_Date__c = AppCon[i].USDA_Registration_Expiration_Date__c;
                                                LineItem.Importer_USDA_Registration_Number__c = AppCon[i].USDA_Registration_Number__c;
                                            }                                    
                                         
                                            if(LineItem.Exporter_Last_Name__c == AppCon[i].ID){
                                                LineItem.Exporter_First_Name__c = AppCon[i].First_Name__c;
                                                //ac.Exporter_Last_Name__c = AppCon[i].Last_Name__c;
                                                LineItem.Exporter_Email__c = AppCon[i].Email_Address__c;
                                                LineItem.Exporter_Phone__c = AppCon[i].Phone__c;
                                                LineItem.Exporter_Fax__c = AppCon[i].Fax__c;
                                                LineItem.Exporter_Mailing_CountryLU__c = AppCon[i].Mailing_Country_LR__c;
                                                LineItem.Exporter_Mailing_Street__c = AppCon[i].Mailing_Street__c;
                                                LineItem.Exporter_Mailing_City__c = AppCon[i].Mailing_City__c;
                                                LineItem.Exporter_Mailing_State_ProvinceLU__c = AppCon[i].Mailing_State_LR__c;
                                                LineItem.Exporter_Mailing_Zip_Postal_Code__c = AppCon[i].Mailing_Zip_Postal_Code__c;
                                            }
                                            
                                            if(LineItem.DeliveryRecipient_Last_Name__c == AppCon[i].ID){
                                                LineItem.DeliveryRecipient_First_Name__c = AppCon[i].First_Name__c;
                                                //ac.DeliveryRecipient_Last_Name__c = AppCon[i].Last_Name__c;
                                                LineItem.DeliveryRecipient_Email__c = AppCon[i].Email_Address__c;
                                                LineItem.DeliveryRecipient_Phone__c = AppCon[i].Phone__c;
                                                LineItem.DeliveryRecipient_Fax__c = AppCon[i].Fax__c;
                                                LineItem.DeliveryRecipient_Mailing_CountryLU__c = AppCon[i].Mailing_Country_LR__c;
                                                LineItem.DeliveryRecipient_Mailing_Street__c = AppCon[i].Mailing_Street__c;
                                                LineItem.DeliveryRecipient_Mailing_City__c = AppCon[i].Mailing_City__c;
                                                LineItem.Delivery_Recipient_State_ProvinceLU__c = AppCon[i].Mailing_State_LR__c;
                                                LineItem.DeliveryRecipient_Mailing_Zip__c = AppCon[i].Mailing_Zip_Postal_Code__c;
                                                LineItem.DeliveryRec_USDA_License_Expiration_Date__c = AppCon[i].USDA_License_Expiration_Date__c;
                                                LineItem.DeliveryRecipient_USDA_License_Number__c = AppCon[i].USDA_License_Number__c;
                                                LineItem.DeliveryRec_USDA_Regist_Expiration_Date__c = AppCon[i].USDA_Registration_Expiration_Date__c;
                                                LineItem.DeliveryRecipient_USDA_Registration_Num__c = AppCon[i].USDA_Registration_Number__c;
                                            }
                                         
                                        } 
                              
                                }
                }
            
    }
}