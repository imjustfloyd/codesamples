public with sharing class CARPOL_Internal_DenialPdfattach {

    public CARPOL_Internal_DenialPdfattach() {

    }


  //public Application__c objapp {get;set;}
    public CARPOL_Internal_DenialPdfattach(ApexPages.StandardController controller) {
           /* string rid = ApexPages.CurrentPage().getParameters().get('id');
            objapp = new Application__c();
           list<Application__c> lst = [select id,Name,Applicant_Name__r.Name,Applicant_Name__c,Applicant_Name__r.MailingStreet,Applicant_Name__r.MailingCity,Applicant_Name__r.MailingState,Applicant_Name__r.MailingPostalCode,
            Applicant_Name__r.MailingCountry,Regulated_Article__r.Name,Import_Country__r.Name from Application__c where id=:rid limit 1];
           objapp = lst[0];*/
  } 
     public Application__c objapp2 {get;set;}
    public PageReference intdenattach() {
    objapp2 = new Application__c();
    string rid = ApexPages.CurrentPage().getParameters().get('id');
     list<Application__c> lst = [select id,Name,Applicant_Name__r.Name,Applicant_Name__c,Applicant_Name__r.MailingStreet,Applicant_Name__r.MailingCity,Applicant_Name__r.MailingState,Applicant_Name__r.MailingPostalCode,
            Applicant_Name__r.MailingCountry,Regulated_Article__r.Name,Import_Country__r.Name from Application__c where id=:rid limit 1];
           objapp2 = lst[0];
    PageReference PDFPage = new PageReference('/apex/CARPOL_Internal_DenialPdfattach?id='+rid);
   // PDFPage.setredirect(true);
       Blob appattach = null;
       if(!test.isRunningTest()){
            appattach = PDFPage.getContent();
             } 
         else {
                appattach = Blob.valueOf('Some Text');
                // blobbody = 'Some Text';
                }
          Attachment objatt = new Attachment();
            objatt.Body = appattach ;
            objatt.Name = 'Denail Letter test.pdf';
            objatt.ParentId = rid;
            insert objatt;
       // return null;
        PageReference pagref = new PageReference('https://aphis--carpoldev--c.cs32.visual.force.com/apex/CARPOL_Internal_DenialPage?id='+rid);
               return pagref ;
    }
 
}