public class portal_Application_Junction_edit {
    public Construct_Application_Junction__c obj {get;set;}
    public Id LineItemId{get;set;}
    public Id strPathwayId{get;set;}
    public string redirect {get;set;}
    //public Id AppId {get;set;}
    public portal_Application_Junction_edit (ApexPages.StandardController controller) {
        obj = (Construct_Application_Junction__c)controller.getRecord();
        obj.Application__c = ApexPages.currentPage().getParameters().get('AppId');
        obj.Line_Item__c = ApexPages.currentPage().getParameters().get('LineItemId'); 
        LineItemId = ApexPages.currentPage().getParameters().get('LineItemId');
        strPathwayId = ApexPages.currentPage().getParameters().get('strPathwayId');
        redirect = ApexPages.currentPage().getParameters().get('redirect');
        //AppId = ApexPages.currentPage().getParameters().get('AppId');
    }
    Public PageReference cancelSOP(){
        if(redirect == 'Yes'){
            PageReference pg = Page.CARPOL_LineItemPageForApplicant;
            pg.getParameters().put('LineId',LineItemId);
            pg.getParameters().put('strPathwayId',strPathwayId);
            pg.setRedirect(true);
            return pg;
        }else{
            PageReference pg = new Pagereference ('/home/home.jsp');
            return pg;
        }
    }
}