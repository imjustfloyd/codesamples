/*
* ClassName: CARPOL_AC_Processing_Test
* CreatedBy: Dawn Sangree
* LastModifiedBy: Dawn Sangree
* LastModifiedOn: 26 April 2016
* Description: This is to Cover the code for the class: CARPOL_UniversalLineItemController
* *************************************************
* ---------------------------------------
* Revision History

*/
@IsTest(seeAlldata=false)
public class CARPOL_AC_Processing_Test{

static testMethod void testCARPOL_AC_Processing_Test(){
         //records populated as if external wizard fired
         CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();    
         ID appRT = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
         //custom settings insert
         testData.insertcustomsettings();
         CARPOL_External_Landing__c importLanding = testData.newExternalLanding('Import', 1);
         Program_Line_Item_Pathway__c testPathway = testData.newCaninePathway();
         testPathway.Display_Regulated_Article_Search__c = true;
         update testPathway;

         
         Intended_Use__c testIU = testData.newIntendedUse(testPathway.Id);
         Wizard_Questionnaire__c testWQ = testData.newWizardQuestionnaire(testPathway.Id, testIU.Id);
         Regulated_Article__c testRA = testData.newRegulatedArticle(testPathway.Id);
         Country__c testAF = testData.newcountryaf();
         Country__c testUS = testData.newcountryus();
         Contact testContact = testData.newContact();

         
         Contact objcont = testData.newcontact();
         User usershare = new User();
         usershare.Username ='aphistestemail@test.com';
         usershare.LastName = 'APHISTestLastName';
         usershare.Email = 'APHISTestEmail@test.com';
         usershare.alias = 'APHItest';
         usershare.TimeZoneSidKey = 'America/New_York';
         usershare.LocaleSidKey = 'en_US';
         usershare.EmailEncodingKey = 'ISO-8859-1';
         usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
         usershare.LanguageLocaleKey = 'en_US';
         usershare.ContactId = objcont.id;
         insert usershare;    

//Call all of this as a community portal user    
         System.runAs(usershare){   
             Test.startTest();         
             Application__c objapp = new Application__c();
             objapp.Applicant_Name__c = objcont.Id;
             objapp.Application_Status__c = 'Open';
             insert objapp;                         
             Authorizations__c objauth = new Authorizations__c();
             objauth.Application__c = objapp.id;
             insert objauth;
             PageReference pageRef = Page.CARPOL_UNI_LineItem;
             Cookie criteria = new Cookie('criteria', 'test data', null, 300, true);
             ApexPages.currentPage().setCookies(new Cookie[]{criteria});                   
             Test.setCurrentPage(pageRef);
             ApexPages.currentPage().getParameters().put('applicationID',objapp.id);                 
             ApexPages.currentPage().getParameters().put('processId','21');    
             ApexPages.currentPage().getParameters().put('Country_Of_Origin__c',testAF.id);                          
             ApexPages.currentPage().getParameters().put('Intended_Use__c',testIU.id);                                                    
             ApexPages.currentPage().getParameters().put('Movement_Type__c','Import');    
             ApexPages.currentPage().getParameters().put('strPathway',testPathway.Id);                 
             Cookie cookie = new Cookie('criteria', '[Please select country of origin]_[Please select Scientific Name]_[Select the regulated article that you wish to import]_[What do you want to Import?]_[Do you wish to import a Product ?]_[What do you want to Import?]_[I want to][Canada]_[Malus Spp.]_[Apple Tree]_[Plants and Seeds For Planting]_[No]_[Plants or Plant Products]_[Import];path=/;expires=-1;isSecure=false]',null,-1,false);
                ApexPages.currentPage().setCookies(new Cookie[]{cookie}); 
             CARPOL_AC_Processing objCARPOL =new CARPOL_AC_Processing();
             objCARPOL.pageLoadFunc();
             objCARPOL.attachLetter();
             ApexPages.currentPage().getParameters().put('processId','22');
             CARPOL_AC_Processing objCARPOL2 =new CARPOL_AC_Processing();
             objCARPOL2.pageLoadFunc();
             objCARPOL2.attachLetter();        
             ApexPages.currentPage().getParameters().put('processId','23');
             CARPOL_AC_Processing objCARPOL3 =new CARPOL_AC_Processing();
             objCARPOL3.pageLoadFunc();
             objCARPOL3.attachLetter();             
              ApexPages.currentPage().getParameters().put('processId','24');
              CARPOL_AC_Processing objCARPOL4 =new CARPOL_AC_Processing();
             objCARPOL4.pageLoadFunc();
             objCARPOL4.attachLetter();                 
             system.assert(objCARPOL != null);
             
             test.StopTest();
         }
    }   
       
}