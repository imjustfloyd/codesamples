@isTest
public class CARPOL_Applicant_Attachment_Count_Test{
    static testMethod void testAttachmentCount(){
        System.debug('<<<<<<<<<<<<<<<<< CARPOL_Applicant_Attachment_Count >>>>>>>>>>>>>>>>>>>>>>>');
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());     
        AC__c ac = testData.newLineItem('Personal Use',objapp);        
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);             
        RecordType ratt = [select ID from RecordType where sObjectType = 'Applicant_Attachments__c' AND Name = 'AC Attachment'];
        Applicant_Attachments__c appatt = new Applicant_Attachments__c(RecordTypeId = ratt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Veterinary Treatment Agreement');
        appatt.Animal_Care_AC__c = ac.Id;
        insert appatt;
        
        Attachment att = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = appatt.Id, Name = 'Test.pdf');
        
        insert att;
        system.assert(att != null);
        Applicant_Attachments__c attfiles = [select Total_Files__c from Applicant_Attachments__c where ID = :appatt.Id];
        System.debug('<<<<<<<<<<<<<< Attached File Number is ' + attfiles.Total_Files__c + ' >>>>>>>>>>>>>>>>>>>>>');
        system.debug(att != null);
    }
    
    static testMethod void testAttachmentCount2(){
        System.debug('<<<<<<<<<<<<<<<<< CARPOL_Applicant_Attachment_Count >>>>>>>>>>>>>>>>>>>>>>>');
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());     
        AC__c ac = testData.newLineItem('Personal Use',objapp);        
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);                     
        RecordType ratt = [select ID from RecordType where sObjectType = 'Applicant_Attachments__c' AND Name = 'AC Attachment'];
        Applicant_Attachments__c appatt = new Applicant_Attachments__c(Total_Files__c = 1, RecordTypeId = ratt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Veterinary Treatment Agreement');
        appatt.Animal_Care_AC__c = ac.Id;
        insert appatt;
        
        Attachment att = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = appatt.Id, Name = 'Test.pdf');
        insert att;
        
        Applicant_Attachments__c attfiles = [select Total_Files__c from Applicant_Attachments__c where ID = :appatt.Id];
        
        Attachment att1 = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = appatt.Id, Name = 'Test.pdf');
        insert att1;
        
        Applicant_Attachments__c attfiles2 = [select Total_Files__c from Applicant_Attachments__c where ID = :appatt.Id];
        system.assert(att1 != null);
        System.debug('<<<<<<<<<<<<<< Attached File Number is(2) ' + attfiles2.Total_Files__c + ' >>>>>>>>>>>>>>>>>>>>>');
    }
    
    static testMethod void testAttachmentCount3(){
        System.debug('<<<<<<<<<<<<<<<<< CARPOL_Applicant_Attachment_Count >>>>>>>>>>>>>>>>>>>>>>>');
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        Test.setMock(HttpCalloutMock.class, new ViolatorLookupServiceMockImpl());     
        AC__c ac = testData.newLineItem('Personal Use',objapp);        
        AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);          
        RecordType ratt = [select ID from RecordType where sObjectType = 'Applicant_Attachments__c' AND Name = 'AC Attachment'];
        Applicant_Attachments__c appatt = new Applicant_Attachments__c(RecordTypeId = ratt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Veterinary Treatment Agreement');
        appatt.Animal_Care_AC__c = ac.Id;
        insert appatt;
        Attachment att = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = appatt.Id, Name = 'Test.pdf');
        insert att;
        system.assert(att != null);        
        Applicant_Attachments__c attfiles = [select Total_Files__c from Applicant_Attachments__c where ID = :appatt.Id];
        delete att;

        System.debug('<<<<<<<<<<<<<< Attached File Number is(3) ' + attfiles.Total_Files__c + ' >>>>>>>>>>>>>>>>>>>>>');
    }
}