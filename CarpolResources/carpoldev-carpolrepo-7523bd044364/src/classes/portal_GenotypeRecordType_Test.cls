/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class portal_GenotypeRecordType_Test {

    static testMethod void myUnitTest() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Genotype__c geno = new Genotype__c();
          geno.Related_Construct_Record_Number__c = objcaj.id;
          geno.Construct_Component_Name__c = 'test';
          geno.Donor__c = 'test';
          insert geno;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          
          System.runAs(userShare)
          {
              PageReference pageRef = Page.portal_GenotypeRecordType;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(geno);
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              ApexPages.currentPage().getParameters().put('LineId',li.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');  
              ApexPages.currentPage().getParameters().put('constid',geno.id);       
              ApexPages.currentPage().getParameters().put('genid',geno.id);    
              portal_GenotypeRecordType extclass1 = new portal_GenotypeRecordType(sc);              
              List<SelectOption> lstOpt = extclass1.getRecordTypes();
              extclass1.continueAssociate();
              extclass1.cancel();                            
              System.assert(extclass1 != null);                      
          }	
          	//run as salesforce user
          	  PageReference pageRef1 = Page.portal_GenotypeRecordType;
              Test.setCurrentPage(pageRef1);
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(geno);
              ApexPages.currentPage().getParameters().put('LineItemId',li.id);
              ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              ApexPages.currentPage().getParameters().put('LineId',li.id);
              ApexPages.currentPage().getParameters().put('redirect','yes');  
              ApexPages.currentPage().getParameters().put('constid',geno.id);       
              //ApexPages.currentPage().getParameters().put('genid',geno.id);    
              portal_GenotypeRecordType extclass2 = new portal_GenotypeRecordType(sc1); 
              extclass2.rectype = 'Test';             
              List<SelectOption> lstOpt1 = extclass2.getRecordTypes();
              extclass2.continueAssociate();
              extclass2.cancel();                            
              System.assert(extclass2 != null);   
          Test.stopTest();          
    }
}