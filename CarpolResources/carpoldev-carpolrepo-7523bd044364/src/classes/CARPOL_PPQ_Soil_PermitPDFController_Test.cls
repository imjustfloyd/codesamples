@IsTest(SeeAllData=true)
public class CARPOL_PPQ_Soil_PermitPDFController_Test{
@IsTest static void soilPermitTest(){
String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Column_1_API_Name__c = 'Life_Stage__c';
          plip.Column_2_API_Name__c = 'Originally_Collected__c';
          plip.Column_3_API_Name__c = 'Culture_Designation__c';
          plip.Column_4_API_Name__c = 'Approved_Facility__c';                              
          plip.Column_5_API_Name__c = 'Application_Number__c';                              
          plip.Column_6_API_Name__c = 'Arrival_Date_and_Time__c';                                                  
          update plip;          
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          insert objdm;
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Final_Decision_Matrix_Id__c = objdm.id;
          objauth.Date_Issued__c = Date.today();          
          objauth.Program_Pathway__c = plip.id;
          //update objauth;
         // Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
         Regulations_Association_Matrix__c  objAssmtr = new Regulations_Association_Matrix__c();
         objAssmtr.Program_Line_Item_Pathway__c = plip.id;
         insert objAssmtr;
         objauth.Final_Decision_Matrix_Id__c = objAssmtr.id;
         update objauth;
          Test.startTest(); 

              PageReference pageRef = Page.CARPOL_PPQ_Soil_PermitPDF;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CARPOL_PPQ_Soil_PermitPDFController extclassempty = new CARPOL_PPQ_Soil_PermitPDFController();              
              CARPOL_PPQ_Soil_PermitPDFController extclass = new CARPOL_PPQ_Soil_PermitPDFController(sc);
              extclass.getTable();
              extclass.getConditions();
                
}
}