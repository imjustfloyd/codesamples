@isTest(seealldata=true)
public class CARPOL_BRS_UNIVERSAL_TRIGGER_TESTS {
/**
* CARPOL_BRS_UNIVERSAL_TRIGGER_TESTS.class
*
* @author  Dawn Sangree
* @date   4/4/2016
* @desc   Tests that triggers for Application, Line Item, Authorization, Associated Contacts, Facility objects and their child classes
* CARPOL_UNI_MasterApplicationTrigger, CARPOL_UNI_MasterLineItemTrigger
*

*/  

    
    //inserting a line item
    static testMethod void testCARPOL_UNI_MasterAuthorizationTriggerInsertUpdate(){
    
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
    
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();    
        testData.insertcustomsettings();        
        
        Test.startTest();
        //create an application

        Application__c objapp = testData.newapplication();
        
        //Application update trigger uses line items to build authorizations
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);

        Authorizations__c objauth = new Authorizations__c();
        objauth.Application__c = objapp.id;
        objauth.RecordTypeID = ACauthRecordTypeId;
        objauth.Status__c = 'Submitted';
        objauth.Date_Issued__c = date.today();
        objauth.Applicant_Alternate_Email__c = 'test@test.com';
        objauth.UNI_Zip__c = '32092';
        objauth.UNI_Country__c = 'United States';
        objauth.UNI_County_Province__c = 'Duval';
        objauth.BRS_Proposed_Start_Date__c = date.today()+30;
        objauth.BRS_Proposed_End_Date__c = date.today()+40;
        objauth.CBI__c = 'CBI Text';
        objauth.Application_CBI__c = 'No';
        objauth.Applicant_Alternate_Email__c = 'email2@test.com';
        objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
        objauth.AC_Applicant_Email__c = 'email2@test.com';
        objauth.AC_Applicant_Fax__c = '(904) 123-2345';
        objauth.AC_Applicant_Phone__c = '(904) 123-2345';
        objauth.AC_Organization__c = 'ABC Corp';
        objauth.Means_of_Movement__c = 'Hand Carry';
        objauth.Biological_Material_present_in_Article__c = 'No';
        objauth.If_Yes_Please_Describe__c = 'Text';
        objauth.Applicant_Instructions__c = 'Make corrections';
        objauth.BRS_Number_of_Labels__c = 10;
        objauth.BRS_Purpose_of_Permit__c = 'Importation';
        objauth.Documents_Sent_to_Applicant__c = false;
        objauth.BRS_Create_Labels__c = false;
        objauth.BRS_Create_State_Review_Records__c = false;
        objauth.BRS_State_Review_Notification__c = false;
        insert objauth;

        objLineItem.Authorization__c = objauth.id;
        update objLineItem;
        
        Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
        
        Link_Authorization_Regulation__c lar = new Link_Authorization_Regulation__c();
        lar.Authorization__c = objauth.id;
        lar.Regulation__c = objreg1.id;
        insert lar;
        
        objauth.Status__c = 'Approved';
        objauth.BRS_Create_Labels__c = true;
        objauth.BRS_Create_State_Review_Records__c = true;  
        objauth.BRS_State_Review_Notification__c = true;  
        objauth.BRS_Introduction_Type__c = 'Importation';                    
        //objauth.Total_Conditions__c = 1;  //rollup summary field 
        update objauth;
        system.assert(objauth != null);
        
        //Add another line item that is the same to see 
        Test.stopTest();
    }   
    
     static testMethod void testCARPOL_UNI_MasterConstructTriggerInsertUpdate(){
    
        String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Standard Permit').getRecordTypeId();
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
        
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();    
        testData.insertcustomsettings();        
        Breed__c b = testData.newbreed();
        Program_Line_Item_Pathway__c plip = testData.newBRSPathway();
        
        Contact objcont = testData.newcontact();
        User usershare = new User();
        usershare.Username ='aphistestemail@test.com';
        usershare.LastName = 'APHISTestLastName';
        usershare.Email = 'APHISTestEmail@test.com';
        usershare.alias = 'APHItest';
        usershare.TimeZoneSidKey = 'America/New_York';
        usershare.LocaleSidKey = 'en_US';
        usershare.EmailEncodingKey = 'ISO-8859-1';
        usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
        usershare.LanguageLocaleKey = 'en_US';
        usershare.ContactId = objcont.id;
        insert usershare;    
        
        RecordType portrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Ports' AND SobjectType = 'Facility__c'];
        Facility__c f = new Facility__c(Name = 'Halifax', Type__c = 'Foreign Port', RecordTypeId = portrt.Id);
        insert f;
        
        RecordType rtp = [select ID from RecordType where sObjectType = 'Facility__c' AND Name = 'Ports'];
        Facility__c port = new Facility__c(Name = 'Test', RecordTypeId = rtp.Id, Type__c ='Domestic Port');
        insert port; 
               
        Test.startTest();
        System.runAs(usershare){        
            Application__c objapp = new Application__c();
            objapp.Applicant_Name__c = objcont.Id;
            objapp.Recordtypeid = ACAppRecordTypeId;
            objapp.Application_Status__c = 'Open';
            insert objapp;  
            
            RecordType imprt  = [SELECT ID, Name FROM RecordType WHERE Name = 'Applicant Contact' AND SobjectType = 'Applicant_Contact__c'];
            Applicant_Contact__c imp = new Applicant_Contact__c(First_Name__c = 'Ace', Name = 'Ventura', Email_Address__c = 'atest@test.com', RecordTypeId = imprt.Id);
            insert imp;
        
            Authorizations__c objauth = new Authorizations__c();
            objauth.Application__c = objapp.id;
            objauth.RecordTypeID = ACauthRecordTypeId;
            objauth.Status__c = 'Submitted';
            objauth.Date_Issued__c = date.today();
            objauth.Applicant_Alternate_Email__c = 'test@test.com';
            objauth.UNI_Zip__c = '32092';
            objauth.UNI_Country__c = 'United States';
            objauth.UNI_County_Province__c = 'Duval';
            objauth.BRS_Proposed_Start_Date__c = date.today()+30;
            objauth.BRS_Proposed_End_Date__c = date.today()+40;
            objauth.CBI__c = 'CBI Text';
            objauth.Application_CBI__c = 'No';
            objauth.Applicant_Alternate_Email__c = 'email2@test.com';
            objauth.UNI_Alternate_Phone__c = '(904) 123-2345';
            objauth.AC_Applicant_Email__c = 'email2@test.com';
            objauth.AC_Applicant_Fax__c = '(904) 123-2345';
            objauth.AC_Applicant_Phone__c = '(904) 123-2345';
            objauth.AC_Organization__c = 'ABC Corp';
            objauth.Means_of_Movement__c = 'Hand Carry';
            objauth.Biological_Material_present_in_Article__c = 'No';
            objauth.If_Yes_Please_Describe__c = 'Text';
            objauth.Applicant_Instructions__c = 'Make corrections';
            objauth.BRS_Number_of_Labels__c = 10;
            objauth.BRS_Purpose_of_Permit__c = 'Importation';
            objauth.Documents_Sent_to_Applicant__c = false;
            objauth.BRS_Create_Labels__c = false;
            objauth.BRS_Create_State_Review_Records__c = false;
            objauth.BRS_State_Review_Notification__c = false;
            insert objauth;

            AC__c ac = new AC__c();
            ac.Program_Line_Item_Pathway__c = plip.id;
            ac.Application_Number__c = objapp.Id;
            ac.Breed__c = b.id;
            ac.Color__c = 'Brown';
            ac.Date_of_Birth__c = Date.newInstance(1960, 2, 17);
            ac.Sex__c = 'Male';
            ac.Purpose_of_the_Importation__c = 'Veterinary Treatment';
            ac.Treatment_available_in_Country_of_Origin__c  = 'No';
            ac.Exporter_Last_Name__c = imp.id;
            ac.DeliveryRecipient_Last_Name__c = imp.id;
            ac.Importer_Last_Name__c = imp.id;
            ac.Port_of_Embarkation__c = f.id;
            ac.Port_of_Entry__c = port.id;
            ac.Transporter_Type__c = 'Ground';
            ac.Proposed_date_of_arrival__c = Date.newInstance(2015, 10, 17);
            ac.Authorization__c = objauth.ID;
            ac.Status__c = 'Test';
            insert ac;

            Construct__c objcaj = new Construct__c();
            objcaj.Construct_s__c = 'Construct Test';
            objcaj.Line_Item__c = ac.id;
            objcaj.Status__c = 'In Review';
            insert objcaj; 
            
            objcaj.Status__c = 'Waiting on Customer';
            update objcaj;
            
            objcaj.Status__c = 'Review Complete';
            objcaj.BRS_Applicant_Response__c = true;
            update objcaj;
            system.assert(objcaj != null);
        } 
        //Add another line item that is the same to see 
        Test.stopTest();
    }    
    
     static testMethod void testCARPOL_UNI_MasterDesignProtocolTriggerInsertUpdate(){
        CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();    
        testData.insertcustomsettings();
        //making sure the future method in the trigger gets fired
        Test.startTest();  
        Application__c objapp = testData.newapplication();            
        AC__c objLineItem = testData.newLineItem('Resale/Adoption', objapp);
        
        Design_Protocol_Record__c dpr = new Design_Protocol_Record__c();
        dpr.Status__c = 'Waiting on Customer';
        dpr.Associate_Application__c = objapp.id;
        dpr.Line_Item__c = objLineItem.id;
        insert dpr;
        dpr.Status__c = 'Review Complete';
        dpr.Attaching_or_Entering_Design_Protocols__c = 'Attaching';
        dpr.BRS_Applicant_Response__c = true;
        update dpr;
        System.assert(dpr !=null);
        Test.stopTest();    

    }         
    
        


}