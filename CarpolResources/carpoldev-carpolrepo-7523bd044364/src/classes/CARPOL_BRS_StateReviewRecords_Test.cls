@isTest(seealldata=true)
private class CARPOL_BRS_StateReviewRecords_Test {
      @IsTest static void CARPOL_BRS_StateReviewRecords_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
          String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
          String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
          String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
          String ACLIRecordTypeId = Schema.SObjectType.AC__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services - Courtesy Permit').getRecordTypeId();

          CARPOL_BRS_TestDataManager testData = new CARPOL_BRS_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          breed__c objbrd = testData.newbreed(); 
          Applicant_Contact__c apcont = testData.newappcontact(); 
          Applicant_Contact__c apcont2 = testData.newappcontact();
          Facility__c fac = testData.newfacility('Domestic Port');  
          Facility__c fac2 = testData.newfacility('Foreign Port');
          Application__c objapp = testData.newapplication();
          AC__c ac1 = testData.newLineItem('Personal Use',objapp);
          AC__c ac2 = testData.newLineItem('Personal Use',objapp);        
          AC__c ac3 = testData.newLineItem('Personal Use',objapp);    
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
          Attachment attach = testData.newattachment(ac1.Id);           
          Authorizations__c objauth = testData.newAuth(objapp.Id); 
          ac1.Authorization__c = objauth.id;
          ac1.status__c = 'Waiting on Customer';
          //ac1.RecordTypeId = ACLIRecordTypeId;
          update ac1;
          Workflow_Task__c wft = testData.newworkflowtask('Test', objauth, 'Pending');
          wft.Status_Categories__c = 'Customer Feedback';
          update wft;
          Map<Id,Workflow_Task__c> mapId = new Map<Id,Workflow_Task__c>();
          mapId.put(wft.Id,wft);


          Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
          Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
          Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 
          Reviewer__c objrev = new Reviewer__c();
          objrev.Status__c= 'Open';
          objrev.Authorization__c = objauth.id;
          objrev.State_Regulatory_Official__c = objcont.id;
          objrev.BRS_State_Reviewer_Email__c = objcont.Email;          
          insert objrev;

          Test.startTest();
          PageReference pageRef = Page.CARPOL_BRS_StateReviewRecords;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objrev);
          ApexPages.currentPage().getParameters().put('ID',objrev.id);
          
          CARPOL_BRS_StateReviewRecords extclass = new CARPOL_BRS_StateReviewRecords(sc);
          extclass.getattach();
          extclass.getAppAttach();
          extclass.Save();
          extclass.getEditPage();
          extclass.SubmitReponses();
          system.assert(extclass != null);    
          Test.stopTest();     
      }
}