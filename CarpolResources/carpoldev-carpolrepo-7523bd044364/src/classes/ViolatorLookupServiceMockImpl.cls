@isTest
global class ViolatorLookupServiceMockImpl implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.

        HttpResponse res = new HttpResponse();
        //JSON Result
        String MessageBody = '{"subjectDetails": [],"message": "Success. Number of records returned are 0"}';   
        res.setBody(MessageBody);
        res.setStatusCode(200);
        System.assert(res != null);        
        return res;
    }
   }