/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CARPOL_UNI_Components_Test {

    static testMethod void test_CARPOL_UNI_Components() {
          
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
          
	      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
	        
	      Account objacct = testData.newAccount(AccountRecordTypeId); 
	      Contact objcont = testData.newcontact();
	      breed__c objbrd = testData.newbreed(); 
	      Applicant_Contact__c apcont = testData.newappcontact(); 
	      Applicant_Contact__c apcont2 = testData.newappcontact();
	      Applicant_Contact__c apcont3 = testData.newappcontact(); 
	      
	      Program_Line_Item_Pathway__c plObj = testData.newCaninePathwayImport();
	      Regulated_Article__c regartObj = testData.newRegulatedArticle(plObj.Id);
	      Component__c comObj = testData.newComponent();
	      RA_Component_Junction__c objRACJ = testData.newRACJ(regartObj.Id,comObj.Id);
	      
	      Test.startTest();
	      CARPOL_UNI_Components.ComponentList(regartObj.Name);
	      CARPOL_UNI_Components.ScientificList(regartObj.Name);	      	      
	      System.assert(regartObj != null);
	      Test.stopTest();
	      
	        
    }
}