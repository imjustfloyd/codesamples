public class CARPOL_UNI_ExtWiz_DecisionMatrix {
    
    public String conditionslist {get;set;}
    public Set<ID> regID = new Set<ID>();
    
    public CARPOL_UNI_ExtWiz_DecisionMatrix(ApexPages.StandardController controller) {
    conditionslist = ApexPages.currentPage().getParameters().get('conditionslist');
    if(conditionslist != null) {
     String[] results = conditionslist.split('_');
      for(String s: results){
        system.debug('<<<<<<<<<!!!!!!!!! in for '+s);
        regID.add(s);
        }
        }
    }
    
    
    
    public List<Regulation__c> rrrlist { get; set; }
    
    public List<Regulation__c> getrlist(){
    return [SELECT ID, Name, testinglong__c, isActive__c, Short_Name__c, Custom_Name__c, Title__c, Type__c, Regulation_Description__c FROM Regulation__c WHERE ID IN: regID];
    }
    
    
    }