public with sharing class CARPOL_PreviewIncident {

    public String templateURL { get; set; }
    public String letterType { get; set; }
    public String selectedTemplateType { get; set; }
    public String selectedTemplate { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    public String reviewerComments { get; set; }
    public String baseURL { get; set; }
    public Incident__c incident { get; set; }
    public Inspection__c     inspection { get; set; }
    public Workflow_Task__c wtask { get; set; } // Added by Kishore
   //added by niharika  
    public boolean showAttachDraftBtn{get;set;}     
    public boolean showAttachBtn{get;set;}      
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    public CARPOL_PreviewIncident(ApexPages.StandardController controller) {
        
         //Added by Kishore
        wtask = [select Id,Comments__c,OwnerId,Owner.Name,Owner.Phone,Owner.Email,Program__c,Status__c from Workflow_Task__c where Id =:apexpages.currentpage().getparameters().get('tId') LIMIT 1 ];
        //Added by niharika         
         showAttachDraftBtn=false;      
         showAttachBtn=true;        
         showtemplateBtn=true;  
         showLinks=false;
       /*  if(wtask.Program__c == 'BRS' && (wtask.Status__c == 'Not Started' || wtask.Status__c == 'Denied' ||wtask.Status__c == 'Pending')  ){       
            showAttachDraftBtn = true;      
            showAttachBtn=false;        
         }      
         if(wtask.Program__c == 'BRS' && wtask.Status__c == 'In Progress'  ){       
                showtemplateBtn=false;
                //showAttachDraftBtn = true;
         }*/
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        this.incident = (Incident__c)controller.getRecord();
      //  authorization = [SELECT ID, Name, Authorization_Type__c, AC_Applicant_Name__c, Application__r.Name, Authorized_User__r.FirstName, Authorized_User__r.LastName, AC_Applicant_Address__c, Template__c, Date_Issued__c,Applicant_Reference_Number__c,Bio_Tech_Reviewer__r.Name,Bio_Tech_Reviewer__c,Bio_Tech_Reviewer__r.Phone,Bio_Tech_Reviewer__r.Email, Thumbprint__c,        Thumbprint__r.Regulated_Article__r.name,Thumbprint__r.From_Country__r.name,Thumbprint__r.REF_Program_Name__c,Thumbprint__r.Line_Record_Type__c,Thumbprint__r.Regulated_Article__r.Scientific_Name__c FROM Authorizations__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
        
        incident=[SELECT Id,Name, Action__c, EFL_APHIS_Incident_Id__c, EFL_Template__c,EFL_Issued_Date__c,EFL_Compliance_letter_content__c,Application__c, EFL_Associated_Incidents__c, Authorization__c,Authorization__r.Name, EFL_Compliance_officer_Queue__c,  Facility__c, Facility__r.Name, Inspection__c, Inspection__r.Scheduled_Date_of_Inspection__c, Inspection__r.Inspector__r.Name ,Program_Compliance_officer__c,Program_Compliance_officer__r.Name, Related_Program__c, Requester_email__c, Requester_Name__c FROM Incident__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
        try{
            inspection=[SELECT Id, Corrective_Actions_Text__c , Actual_Inspection__c, Application__c, Authorization__c, Facility__c, Incident__c,Inspection_Comments__c, Inspection_Due_Date__c, Name, Inspection_Results_Summary__c, Inspector__c,Inspector__r.Name, Inspector_Email__c, Scheduled_Date_of_Inspection__c, Scheduled_Time_of_Inspection__c, Status__c, Time_Spent_Conducting_the_Inspection__c, Time_Spent_Creating_and_Revising_Report__c, Time_Spent_Preparing_for_the_Inspection__c, Time_Spent_Travelling_to_and_from_Site__c, Total_Miles_Driven_to_and_from_Site__c FROM Inspection__c WHERE Incident__c = :incident.ID LIMIT 1];
        }
        catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There has to be an Inspection associated to send out this letter'));
        }
        //System.Debug('<<<<<<< incident : ' + incident.Program_Compliance_officer__r.Name + ' >>>>>>>');
        //System.Debug('<<<<<<< inspection c ' + inspection.inspection__c + ' >>>>>>>');
        //System.Debug('<<<<<<< inapwction r : ' + inspection.Inspector__c + ' >>>>>>>');  
                //System.Debug('<<<<<<< inapwction r : ' + inspection.Inspector__r.Name + ' >>>>>>>');                
        if(incident == null){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a related incident.'));}
        else
        {
            //authorization.Date_Issued__c = System.Today();
            //System.Debug('<<<<<<< Authorization_Type__c : ' + authorization.Authorization_Type__c + ' >>>>>>>');
            //letterType = incident.LetterofNonCompliance;
            templateURL = 'apex/CARPOL_IncidentLetter?id=' + incident.ID + '&tId='+ wtask.Id;
        }
        selectedTemplateType = 'Letter of Non Compliance';
        System.Debug('<<<<<<< Template Type : ' + selectedTemplateType + ' >>>>>>>');
        getTemplates();
           }
    
    public PageReference getTemplates()
    {
        if(selectedTemplateType != null)
        {
            System.Debug('<<<<<<< Getting all the templates and generating preview >>>>>>>');
            allTemplateOptions = new List<SelectOption>();
            List<Communication_Manager__c> tempTemplates = [SELECT ID, Name FROM Communication_Manager__c WHERE Type__c = :selectedTemplateType];
            for (Communication_Manager__c template : tempTemplates ) 
            {allTemplateOptions.add(new SelectOption(template.ID, template.Name));}
            
        }
        return null;
    }
    
    public PageReference populateTemplate()
    {
       // System.Debug('<<<<<<< Template : ' + incident. EFL_Template__c + ' >>>>>>>');
        try
        {
            if(incident.EFL_Template__c != null)
            {
                incident.EFL_Compliance_letter_content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:incident.EFL_Template__c LIMIT 1].Content__c;
                
                //Replacing Variables
                if (incident.Requester_Name__c != null && incident.Requester_Name__c != 'null')
                incident.EFL_Compliance_letter_content__c = incident.EFL_Compliance_letter_content__c.replace('{!Responsible Party}', incident.Requester_Name__c);
               if (incident.Facility__r.Name != null && incident.Facility__r.Name != 'null')
               incident.EFL_Compliance_letter_content__c = incident.EFL_Compliance_letter_content__c.replace('{!Facility}', incident.Facility__r.Name);
               if (incident.Authorization__r.Name != null && incident.Authorization__r.Name != 'null')
               incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!AuthorizationNumber}', incident.Authorization__r.Name);
              if (incident.Program_Compliance_officer__r.Name != null && incident.Program_Compliance_officer__r.Name != 'null')
               incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!ProgramComplianceofficer}', incident.Program_Compliance_officer__r.Name);

                 if (inspection.Scheduled_Date_of_Inspection__c != null )
               incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!DateofInspection}', String.Valueof(inspection.Scheduled_Date_of_Inspection__c));
                if (inspection.Inspector__r.Name != null && inspection.Inspector__r.Name != 'null' )
               incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!Inspector}', inspection.Inspector__r.Name);
               
               if(inspection.Corrective_Actions_Text__c != null)
                   incident.EFL_Compliance_letter_content__c= incident.EFL_Compliance_letter_content__c.replace('{!CorrectiveActions}', String.Valueof(inspection.Corrective_Actions_Text__c));
                
               
                update incident;
                previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Template populated successfully.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a template to be populated.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid template to be populated.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference cancel()
    {
        System.Debug('<<<<<<< Cancel >>>>>>>');
        PageReference return2incident = new PageReference('/' + incident.ID);
        return2incident.setRedirect(true);
        return return2incident;
    }

    public PageReference attachLetter()
    {
        try{
            System.Debug('<<<<<<< incident : ' + incident + ' >>>>>>>');
            String attachmentName;
            PageReference pdfComplianceLetter = Page.CARPOL_IncidentLetter;
            attachmentName = 'NonComplianceLetter_' + System.TODAY().format();
          
            
            pdfComplianceLetter.getParameters().put('id', incident.ID);
            pdfComplianceLetter.getParameters().put('tId', wtask.Id);
            Blob pdfComplianceLetterBlob;
            
           if (Test.IsRunningTest())
           {
            pdfComplianceLetterBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfComplianceLetter + ' >>>>>>');
            pdfComplianceLetterBlob = pdfComplianceLetter.getContent();
            
            }
            
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :incident.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = incident.ID, name = attachmentName, body = pdfComplianceLetterBlob);
            insert attachment;
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Non Compliance Letter attached successfully.'));
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    //niharika      
    public PageReference attachDraftLetter()        
    {       
        showLinks=true;
        try{        
            System.Debug('<<<<<<< incident : ' + incident + ' >>>>>>>');      
            String draftattachmentName;     
            PageReference pdfcomplianceLetter = Page.CARPOL_IncidentLetter;     
            draftattachmentName = 'Draft Authorization Letter_' + System.TODAY().format();      
                    
                    
            pdfcomplianceLetter.getParameters().put('id', incident.ID);     
            pdfcomplianceLetter.getParameters().put('tId', wtask.Id);        
            Blob pdfcomplianceLetterBlob;        
                    
          //  pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent(); 
           if (Test.IsRunningTest())
           {
            pdfcomplianceLetterBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfcomplianceLetter + ' >>>>>>');
            pdfcomplianceLetterBlob = pdfcomplianceLetter.getContent();
            
            }            
                    
            String tempAttachmentName = draftattachmentName.substring(0, draftattachmentName.length()) + '%';       
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :wtask.Id AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];     
            if(prevAttachments.size() > 0)      
            {       
                if(prevAttachments.size() == 1){draftattachmentName = draftattachmentName + '_v2';}     
                else        
                {       
                    for(Integer i = 0; i < prevAttachments.size(); i++ )        
                    {       
                        if(i == prevAttachments.size() - 1)     
                        {       
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);       
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);      
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;       
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');      
                            draftattachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;        
                            System.Debug('<<<<<<< New Attachment Name : ' + draftattachmentName + ' >>>>>>');       
                        }       
                    }       
                }       
            }       
                    
            draftattachmentName = draftattachmentName + '.pdf';     
            Attachment attachment = new Attachment(parentId = wtask.Id, name = draftattachmentName, body = pdfcomplianceLetterBlob);     
            insert attachment;      
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Authorization Draft Letter attached successfully.'));
        }       
        catch(Exception e)      
        {       
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');      
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));     
        }       
        return null;        
    }
    
    public PageReference saveDraft()
    {
        try{
            if(incident.EFL_Issued_Date__c < System.Today())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Date Issued cannot be in the past.'));
            }
            else
            {
                update incident;
                //previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference previewTemplate()
    {
        try{
            System.Debug('<<<<<<< Generating Preview !! >>>>>>>');
            system.debug('<<< Task Id >>>'+wtask.Id);
            templateURL = '/apex/CARPOL_IncidentLetter?id=' + incident.ID + '&tId='+ wtask.Id ;
            
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }    
}