global with sharing class CARPOL_BRS_Approval {
 
    @future
    public static void BRSdenailatt(list<id> partids,id appid)
    {/*Kishore
        list<Attachment> denaillst = new list<Attachment>();
        list<id> attpartids = new list<id>();
        attpartids = partids;
        //for(id s:attpartids)
        for(integer i=0;i<attpartids.size();i++)
        {
            Attachment objdenail = new Attachment(); 
            Blob attbody = null;
            PageReference PDFPage = new PageReference('/apex/CARPOL_BRS_Notification_Letter_of_Denial?id='+appid);  
            attbody = PDFPage.getContent();
             if(!test.isRunningTest()){
                        attbody = PDFPage.getContentAsPDF();                         } 
                     else {
                            attbody = Blob.valueOf('Some Text');
                            }
            objdenail.Body = attbody ;
            objdenail.Name = 'CARPOL_BRS_Denial.pdf';
            system.debug('------blob body----'+string.valueof(attbody));
            objdenail.ContentType = 'pdf';
            objdenail.ParentId = attpartids[i];
            denaillst.add(objdenail);
        }
        if(denaillst.size()>0)
        {
            database.insert(denaillst);
        }
         
     Kishore*/  
    }  
        @future
        public static void updateapp(string brsappid,string brsappstatus)
        {
            Application__c objapp = [select id,Application_Status__c from Application__c where id=:brsappid ];
            objapp.Application_Status__c = brsappstatus;
            update objapp;
         } 
         
         // this method is used for labels pdf attachment and send labels to applicant
        // @future
         webservice static string brslabelspdf(id authid)
         {
           string strsuccess = '';
           try{
           // replaced applicant_email__c with AC_Applicant_Email__c  -- 6/11/2015 acarr
            Authorizations__c objauth = [select id,Name,BRS_Send_Labels__c,AC_Applicant_Email__c from Authorizations__c where id=:authid];
            string appemail = objauth.AC_Applicant_Email__c;
            objauth.BRS_Send_Labels__c = true;
            update objauth;
            attachment attlab = new attachment();
            Blob attbody = null;
            PageReference PDFPage = new PageReference('/apex/CARPOL_BRS_PDF_Labels?id='+authid);  
           // attbody = PDFPage.getContent();
             if(!test.isRunningTest()){
                        attbody = PDFPage.getContent(); 
                        } 
                     else {
                            attbody = Blob.valueOf('Some Text');
                            } 
                attlab.Body = attbody;
                attlab.Name = 'CARPOL_BRS_Labels.pdf';
               // attlab.contentType = 'pdf';
                attlab.ParentId = authid;
                
            insert attlab;
          /*  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            String[] toaddress = new string[]{objauth.AC_Applicant_Email__c};
            email.setSubject('BRS Labels');
            email.setToAddresses(toaddress);
            email.setPlainTextBody( 'BRS Labels' );
             Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
             efa.setFileName('CARPOL_BRS_Labels.pdf');
             efa.setBody(attbody);
             email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email }); */
           // strsuccess = 'success';
            //retrun strsuccess;
            return 'success';
            
            }
            catch(Exception e)
            {
                //strsuccess = e;
                return string.valueof(e);
            }
            return null;
            
         }

}