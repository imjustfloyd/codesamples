public class GenericHistoryClass {
 
  public static void CreateHistoryRecord(List<SObject> objList,map<Id,SObject> objMap,String objApiName)
   {
       Map <String, Map <Id, String>> lookupRecords = new Map <String, Map <Id, String>> ();
       
       String objName = objApiName;
       sObject aOld;
      
       if(objName == 'Authorizations__c')
         aOld = new Authorizations__c();       
       if(objName == 'Application__c')
         aOld = new Application__c(); 
       if(objName == 'AC__c')
         aOld = new AC__c();
       if(objName == 'Location__c')
         aOld = new Location__c(); 
       if(objName == 'Design_Protocol_Record__c')
         aOld = new Design_Protocol_Record__c(); 
          if(objName == 'Application_Condition__c')
         aOld = new Application_Condition__c(); 
       if(objName == 'Signature__c')
         aOld = new Signature__c(); 
       if(objName == 'Construct__c')
         aOld = new Construct__c(); 
       if(objName == 'Genotype__c')
         aOld = new Genotype__c(); 
       if(objName == 'Phenotype__c')
         aOld = new Phenotype__c(); 
       if(objName == 'Reviewer__c')
         aOld = new Reviewer__c(); 
       if(objName == 'Self_Reporting__c')
         aOld = new Self_Reporting__c(); 
     //  if(objName == 'Application__c')
     //    aOld = new Application__c();        
       
       
       List<Schema.FieldSetMember> TrackedFields = new List<Schema.FieldSetMember>();
       if(objName == 'Authorizations__c')
       TrackedFields =  SObjectType.Authorizations__c.FieldSets.TrackedFields.getFields();
       if(objName == 'Application__c')
       TrackedFields =  SObjectType.Application__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'AC__c')
       TrackedFields =  SObjectType.AC__c.FieldSets.TrackedFields.getFields();       
       if(objName == 'Location__c')
       TrackedFields =  SObjectType.Location__c.FieldSets.TrackedFields.getFields();  
       if(objName == 'Design_Protocol_Record__c')
       TrackedFields =  SObjectType.Design_Protocol_Record__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Signature__c')
       TrackedFields =  SObjectType.Signature__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Construct__c')
       TrackedFields =  SObjectType.Construct__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Genotype__c')
       TrackedFields =  SObjectType.Genotype__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Phenotype__c')
       TrackedFields =  SObjectType.Phenotype__c.FieldSets.TrackedFields.getFields();
       if(objName == 'Reviewer__c')
       TrackedFields =  SObjectType.Reviewer__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Self_Reporting__c')
       TrackedFields =  SObjectType.Self_Reporting__c.FieldSets.TrackedFields.getFields(); 
        if(objName == 'Application_Condition__c')
       TrackedFields =  SObjectType.Application_Condition__c.FieldSets.TrackedFields.getFields(); 
     /*  if(objName == 'Location__c')
       TrackedFields =  SObjectType.Location__c.FieldSets.TrackedFields.getFields(); 
       if(objName == 'Location__c')
       TrackedFields =  SObjectType.Location__c.FieldSets.TrackedFields.getFields();    */    

    
       if (trackedFields.isEmpty()) return;

       List<Change_History__c> fieldChanges = new List<Change_History__c>();

       List<string> apiNameList = new List<string>();        

       for (sObject aNew : objList) {
           
           if(objName  == 'Authorizations__c')
             aOld = (Authorizations__c)objMap.get(aNew.Id);          
           if(objName == 'Application__c')
             aOld = (Application__c)objMap.get(aNew.Id);
           if(objName == 'AC__c')
             aOld = (AC__c)objMap.get(aNew.Id);             
           if(objName == 'Location__c')
             aOld = (Location__c)objMap.get(aNew.Id);
           if(objName == 'Design_Protocol_Record__c')
             aOld = (Design_Protocol_Record__c)objMap.get(aNew.Id);
           if(objName == 'Signature__c')
             aOld = (Signature__c)objMap.get(aNew.Id);
           if(objName == 'Construct__c')
             aOld = (Construct__c)objMap.get(aNew.Id);
           if(objName == 'Genotype__c')
             aOld = (Genotype__c)objMap.get(aNew.Id);
           if(objName == 'Phenotype__c')
             aOld = (Phenotype__c)objMap.get(aNew.Id);             
           if(objName == 'Reviewer__c')
             aOld = (Reviewer__c)objMap.get(aNew.Id);
           if(objName == 'Self_Reporting__c')
             aOld = (Self_Reporting__c)objMap.get(aNew.Id);           
              if(objName == 'Application_Condition__c')
             aOld = (Application_Condition__c)objMap.get(aNew.Id);    
           system.debug('aOld ##'+aOld);
           system.debug('aNew ##'+aNew);

         for (Schema.FieldSetMember fsm : TrackedFields) {

          String fieldName  = fsm.getFieldPath();
          system.debug('fieldName ##'+ fieldName);   
          String fieldLabel = fsm.getLabel();

          if (aNew.get(fieldName) != aOld.get(fieldName)) {

           // Check whether field set memeber is of type Reference (Lookup)
           if (fsm.getType() == Schema.DisplayType.REFERENCE)
           {
            // Get the field name
           // String fieldName = fsm.getFieldPath();
            // Get the Ids of the parent records (old and new)
            Id newLookupId = Id.valueOf(String.valueof(aNew.get(fieldName)));
            Id oldLookupId; 
               if(aOld.get(fieldName) != null)
               oldLookupId = Id.valueOf(String.valueof(aOld.get(fieldName)));
            // Determine the object name that is lookin up to, based on the ID
            String sObjName = newLookupId.getSObjectType().getDescribe().getName();

            if (lookupRecords.get(sObjName) == null)
            {
             lookupRecords.put(sObjName, new Map <Id, String> ());
            }

             lookupRecords.get(sObjName).put(newLookupId, null);
             lookupRecords.get(sObjName).put(oldLookupId, null);
           }
           else{  
            
             String oldValue = String.valueOf(aOld.get(fieldName));
             String newValue = String.valueOf(aNew.get(fieldName));
            
             Change_History__c aht = new Change_History__c();
             aht.name = fieldLabel;
             aht.Field_API_Name__c   = fieldName;
             // aht.User__c      = aNew.Id;
             // aht.ChangedBy__c = UserInfo.getUserId();
             aht.Old_Value__c  = oldValue;
             aht.New_Value__c  = newValue;
             if(objName == 'Authorizations__c')
             aht.Authorization__c = aNew.Id; 
             if(objName == 'Application__c')
             aht.Application__c = aNew.Id;  
             if(objName == 'AC__c')
             aht.Line_Item__c = aNew.Id;              
             if(objName == 'Location__c')
             aht.Location__c = aNew.Id;  
             if(objName == 'Design_Protocol_Record__c')
             aht.Design_Protocol_Record__c = aNew.Id;  
             if(objName == 'Signature__c')
             aht.Thumbprint__c = aNew.Id;  
             if(objName == 'Construct__c')
             aht.Construct__c = aNew.Id;  
             if(objName == 'Genotype__c')
             aht.Genotype_Phenotype__c = aNew.Id; 
             if(objName == 'Phenotype__c')
             aht.Phenotype__c = aNew.Id;             
             if(objName == 'Reviewer__c')
             aht.Official_Review_Record__c = aNew.Id;  
             if(objName == 'Self_Reporting__c')
             aht.Self_Reporting__c = aNew.Id;  
               if(objName == 'Application_Condition__c')
             aht.Supplemental_Condition__c = aNew.Id;  
          //   if(objName == 'Application__c')
          //   aht.Application__c = aNew.Id;  
              
              
              
             system.debug('aht'+aht);
             apiNameList.add(aht.Field_API_Name__c);
             fieldChanges.add(aht);
            } 
           }        
          }
         }
       
       // Populate the map with ObjectName => RecordId => Name for both old and new lookups that have changed
   for (String objectName : lookupRecords.keySet())
   {
     Set <Id> recordIds = lookupRecords.get(objectName).keySet();
       system.debug('recordIds ##'+recordIds);
     for (sObject sObjectRecord : Database.query('SELECT Id, Name FROM ' + objectName + ' WHERE Id IN :recordIds'))
     {
        system.debug('sObjectRecord ##'+sObjectRecord); 
        lookupRecords.get(objectName).put((Id)sObjectRecord.get('Id'), (String)sObjectRecord.get('Name'));
     }
   }

   // Again loop like the first time
  for (sObject aNew : objList) 
  {
           if(objName  == 'Authorizations__c')
             aOld = (Authorizations__c)objMap.get(aNew.Id); 
             
              if(objName  == 'Application_Conditions__c')
             aOld = (Application_Condition__c)objMap.get(aNew.Id);
                      
           if(objName == 'Application__c')
             aOld = (Application__c)objMap.get(aNew.Id);
           if(objName == 'AC__c')
             aOld = (AC__c)objMap.get(aNew.Id);             
           if(objName == 'Location__c')
             aOld = (Location__c)objMap.get(aNew.Id);
           if(objName == 'Design_Protocol_Record__c')
             aOld = (Design_Protocol_Record__c)objMap.get(aNew.Id);
           if(objName == 'Signature__c')
             aOld = (Signature__c)objMap.get(aNew.Id);
           if(objName == 'Construct__c')
             aOld = (Construct__c)objMap.get(aNew.Id);
           if(objName == 'Genotype__c')
             aOld = (Genotype__c)objMap.get(aNew.Id);
           if(objName == 'Phenotype__c')
             aOld = (Phenotype__c)objMap.get(aNew.Id);             
           if(objName == 'Reviewer__c')
             aOld = (Reviewer__c)objMap.get(aNew.Id);
           if(objName == 'Self_Reporting__c')
             aOld = (Self_Reporting__c)objMap.get(aNew.Id);    
              if(objName == 'Application_Condition__c')
             aOld = (Application_Condition__c)objMap.get(aNew.Id); 
               
           system.debug('aOld ##'+aOld);
           system.debug('aNew ##'+aNew);
      
    for (Schema.FieldSetMember fsm : trackedFields) 
    {
        if (fsm.getType() == Schema.DisplayType.REFERENCE)
        {
            String fieldName  = fsm.getFieldPath();
            String fieldLabel = fsm.getLabel();

            if (aNew.get(fieldName) != null && aOld.get(fieldName) != null && aNew.get(fieldName) != aOld.get(fieldName)) 
            {
                Id newLookupId = Id.valueOf(String.valueof(aNew.get(fieldName)));
                Id oldLookupId = Id.valueOf(String.valueof(aOld.get(fieldName)));
                String sObjName = newLookupId.getSObjectType().getDescribe().getName();

                if (lookupRecords.get(sObjName) != null)
                {
                    String oldName = lookupRecords.get(sObjName).get(oldLookupId);
                    String newName = lookupRecords.get(sObjName).get(newLookupId);

                    // create the additional AccountHistoryTracking__c for lookups
                   // String oldValue = String.valueOf(aOld.get(fieldName));
            // String newValue = String.valueOf(aNew.get(fieldName));
            
             Change_History__c aht = new Change_History__c();
             aht.name = fieldLabel;
             aht.Field_API_Name__c   = fieldName;
             // aht.User__c      = aNew.Id;
             // aht.ChangedBy__c = UserInfo.getUserId();
             aht.Old_Value__c  = oldName;
             aht.New_Value__c  = newName;
             if(objName == 'Authorizations__c')
             aht.Authorization__c = aNew.Id; 
             if(objName == 'Application__c')
             aht.Application__c = aNew.Id;  
             if(objName == 'AC__c')
             aht.Line_Item__c = aNew.Id;              
             if(objName == 'Location__c')
             aht.Location__c = aNew.Id;  
             if(objName == 'Design_Protocol_Record__c')
             aht.Design_Protocol_Record__c = aNew.Id;  
             if(objName == 'Signature__c')
             aht.Thumbprint__c = aNew.Id;  
             if(objName == 'Construct__c')
             aht.Construct__c = aNew.Id; 
               if(objName == 'Application_Conditions__c')
             aht.Supplemental_Condition__c = aNew.Id; 
             if(objName == 'Genotype__c')
             aht.Genotype_Phenotype__c = aNew.Id;  
             if(objName == 'Phenotype__c')
             aht.Phenotype__c = aNew.Id;              
             if(objName == 'Reviewer__c')
             aht.Official_Review_Record__c = aNew.Id;  
             if(objName == 'Self_Reporting__c')
             aht.Self_Reporting__c = aNew.Id;  
          //   if(objName == 'Application__c')
          //   aht.Application__c = aNew.Id;  
              
              
             system.debug('aht'+aht);
             apiNameList.add(aht.Field_API_Name__c);
             fieldChanges.add(aht);
                }
            }
        }
    }
  }
    
     if (!fieldChanges.isEmpty()) {
        insert fieldChanges;
       }
   }
   
}