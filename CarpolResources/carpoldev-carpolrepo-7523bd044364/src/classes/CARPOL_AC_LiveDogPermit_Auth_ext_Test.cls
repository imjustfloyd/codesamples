@isTest(seealldata=true)
private class CARPOL_AC_LiveDogPermit_Auth_ext_Test {
    static testMethod void testCARPOL_AC_PermitAuth_extension() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      //String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Importer').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();

      Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
      Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
      Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      AC__c ac1 = testData.newLineItem('Personal Use',objapp,objauth);
      ac1.Authorization__c = objauth.id;
      update ac1;        
      Authorization_Junction__c objauthjun1 = testData.newAuthorizationJunction( objauth.Id, objreg1.Id);
      Authorization_Junction__c objauthjun2 = testData.newAuthorizationJunction( objauth.Id, objreg2.Id);
      Authorization_Junction__c objauthjun3 = testData.newAuthorizationJunction( objauth.Id, objreg3.Id); 

      //AC__c ac2 = testData.newLineItem('Personal Use',objapp,objauth);        
      //AC__c ac3 = testData.newLineItem('Personal Use',objapp,objauth);              
      Attachment attach = testData.newattachment(ac1.Id);           

      
      Test.startTest();
      PageReference pageRef = Page.CARPOL_AC_LiveDogPermit_Auth;
      Test.setCurrentPage(pageRef);
      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);

      CARPOL_AC_LiveDogPermit_Auth_extension acepreg = new CARPOL_AC_LiveDogPermit_Auth_extension(sc);
      acepreg.getLineItems();
      System.assertEquals(1, acepreg.oneLineItem.size());
      acepreg.getRegulation();
      Test.stopTest();
      
      }
      
      
      static testMethod void testSaveAttachment(){
       
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
      String ImpapcontRecordTypeId = Schema.SObjectType.Applicant_Contact__c.getRecordTypeInfosByName().get('Applicant Contact').getRecordTypeId();
      String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Ports').getRecordTypeId();
      String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();
      String ACauthRecordTypeId = Schema.SObjectType.Authorizations__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      String ACRegRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      Authorizations__c auth = testData.newAuth(objapp.Id); 
      AC__c ac = testData.newLineItem('Veterinary Treatment',objapp,auth);              
      ac.Authorization__c = auth.id;
      update ac;
      auth.Response_Type__c = 'Permit';
      update auth;
        System.debug ('<<<<<<<<<<<<<<<<<<<< Animal Care ' + ac + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        RecordType atrt  = [SELECT ID, Name FROM RecordType WHERE Name = 'AC Attachment' AND SobjectType = 'Applicant_Attachments__c'];
        Applicant_Attachments__c att = new Applicant_Attachments__c(RecordTypeId = atrt.Id, Document_Types__c = 'Health Certificate (AC7041); IACUC Approved Research Proposal; Rabies Vaccination Certificate (AC7042); Research Justification; Veterinary Treatment Agreement', Animal_Care_AC__c = ac.Id, Total_Files__c = 1);
        insert att;
        
        Attachment n = new Attachment(Body = Blob.valueOf('Some Text'), ParentId = att.Id, Name = 'Test.pdf');
        insert n;
        
        
        CARPOL_AC_LiveDogPermit_Auth_extension saveatt = new CARPOL_AC_LiveDogPermit_Auth_extension(new ApexPages.StandardController(auth));
        System.debug('<<<<<<<<<<<<<<<<<<< Save Attachment ' + saveatt + ' >>>>>>>>>>>>>>>>>>>>>>');
        saveatt.saveAttachment();
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:auth.id];
        System.debug ('<<<<<<<<<<<<<<<<<<<< Authorization attachments ' + attachments.size() + '>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
        
        System.assertEquals(1, attachments.size());
        ApexPages.Message[] msgs = ApexPages.getMessages();
        Boolean found=false;    
      
    }
    
    
}