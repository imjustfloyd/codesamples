/* 
// Author : Vinar Amrutia
// Date Created : 01/22/2015
// Purpose : Ability to add/remove Ports from a particular Port Group
*/
public class CARPOL_AddPortsToGroup {

    public Facility__c facility { get; set;}
    public Signature__c signature { get; set; }
    public SelectOption[] allPorts { get; set; }
    public SelectOption[] selectedPorts { get; set; }
    public String groupID = ApexPages.currentPage().getParameters().get('id');
    public Map<String, String> mapAllSavedPorts = new Map<String, String>();
    public List<Facility__c> lstSavedPorts = new List<Facility__c>();
    public String selectedType;
    
    public CARPOL_AddPortsToGroup(ApexPages.StandardController controller) {
        System.Debug('<<<<<<< Standard Controller Begins >>>>>>>');
        
        facility = new Facility__c();
        signature = new Signature__c();
        
        getPorts();
        
        /*
        allPorts = new List<SelectOption>();
        List<Facility__c> facilities = [SELECT ID, Name, Description__c FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID NOT IN (SELECT Facility__c FROM Group_Junction__c WHERE Port_Group__c = :groupID) LIMIT 999];
        for (Facility__c fclty : facilities ) {allPorts.add(new SelectOption(fclty.ID, fclty.Name));}
        
        selectedPorts = new List<SelectOption>();
        List<Facility__c> slctdFcltys = [SELECT ID, Name, Description__c FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Facility__c FROM Group_Junction__c WHERE Port_Group__c = :groupID) LIMIT 999];
        lstSavedPorts = slctdFcltys;
        System.Debug('<<<<<<< Total Saved Ports : ' + lstSavedPorts.size() + ' >>>>>>>');
        for (Facility__c fclty : slctdFcltys)
        {
            selectedPorts.add(new SelectOption(fclty.ID, fclty.Name));
            mapAllSavedPorts.put(fclty.Id,fclty.Id);
        }*/
    }
    
    public PageReference getPorts(){
        try
        {
            allPorts = new List<SelectOption>();
            List<Facility__c> facilities = [SELECT ID, Name, Description__c FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID NOT IN (SELECT Facility__c FROM Group_Junction__c WHERE Port_Group__c = :groupID) LIMIT 999];
            for (Facility__c fclty : facilities ) {allPorts.add(new SelectOption(fclty.ID, fclty.Name));}
            
            selectedPorts = new List<SelectOption>();
            List<Facility__c> slctdFcltys = [SELECT ID, Name, Description__c FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Facility__c FROM Group_Junction__c WHERE Port_Group__c = :groupID) LIMIT 999];
            lstSavedPorts = slctdFcltys;
            System.Debug('<<<<<<< Total Saved Ports : ' + lstSavedPorts.size() + ' >>>>>>>');
            for (Facility__c fclty : slctdFcltys)
            {
                selectedPorts.add(new SelectOption(fclty.ID, fclty.Name));
                mapAllSavedPorts.put(fclty.Id,fclty.Id);
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
    
    public PageReference updatePortGroup() { 
        try
        {
            System.Debug('<<<<<<< Add - All Saved Ports : ' + lstSavedPorts.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Add - Selected Ports : ' + selectedPorts.size() + ' >>>>>>>');
            List<Group_Junction__c> portsToBeAdded = new List<Group_Junction__c>();
            List<Group_Junction__c> portsToBeRemoved = new List<Group_Junction__c>();
            Group_Junction__c groupPortJunction;
            
            // Logic to add Ports and update the list
            for (SelectOption selReg : selectedPorts) {
                if(mapAllSavedPorts.get(selReg.getValue()) == null)
                {
                    System.Debug('<<<<<<< New Port Found >>>>>>>');
                    groupPortJunction = new Group_Junction__c();
                    groupPortJunction.Port_Group__c = groupID;
                    groupPortJunction.Facility__c = selReg.getValue();
                    portsToBeAdded.add(groupPortJunction);
                }
            }
            System.Debug('<<<<<<< Total New Ports to be Added : ' + portsToBeAdded.size() + ' >>>>>>>');
            insert(portsToBeAdded);
            lstSavedPorts = [SELECT ID, Name, Description__c FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Facility__c FROM Group_Junction__c WHERE Port_Group__c = :groupID)];
            
            // Logic to remove ports and update the list
            String strRemovePortGroupJunctionIds = '(';
            System.Debug('<<<<<<< Remove - Saved Ports : ' + lstSavedPorts.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Remove - Selected Ports : ' + selectedPorts.size() + ' >>>>>>>');
            if(selectedPorts.size() == 0)
            {    
                for (Facility__c svdPort : lstSavedPorts) {
                    strRemovePortGroupJunctionIds += '\'' + String.ValueOf(svdPort.ID) + '\',';
                }
            }
            else
            {
                for(Integer i = 0; i < lstSavedPorts.size(); i++)
                {
                    Integer matchFound = 0;
                    for(Integer j = 0; j < selectedPorts.size(); j++)
                    {
                        if(lstSavedPorts[i].ID == selectedPorts[j].getValue())
                        {
                            matchFound += 1;
                            break;
                        }
                    }
                    if(matchFound == 0)
                    {
                        strRemovePortGroupJunctionIds += '\'' + String.ValueOf(lstSavedPorts[i].ID) + '\',';
                    }
                }
            }
            if(strRemovePortGroupJunctionIds != '(')
            {
                strRemovePortGroupJunctionIds = strRemovePortGroupJunctionIds.substring(0, strRemovePortGroupJunctionIds.length() - 1);
                strRemovePortGroupJunctionIds += ')';
                System.Debug('<<<<<<< Ids to be Removed : ' + strRemovePortGroupJunctionIds + ' >>>>>>>');
                String queryTemp = 'SELECT ID FROM Group_Junction__c WHERE Facility__c IN ' + strRemovePortGroupJunctionIds + ' AND Port_Group__c = :groupID';
                System.Debug('<<<<<<< Final String : ' + queryTemp + ' >>>>>>>');
                portsToBeRemoved = Database.query(queryTemp);
                System.Debug('<<<<<<< Signature Port Junctions to be Removed : ' + portsToBeRemoved + ' >>>>>>>');
                delete(portsToBeRemoved);
            }
            
            if(portsToBeAdded.size() != 0 || portsToBeRemoved.size() != 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
            }
            getPorts();
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
        return null;
    }
}