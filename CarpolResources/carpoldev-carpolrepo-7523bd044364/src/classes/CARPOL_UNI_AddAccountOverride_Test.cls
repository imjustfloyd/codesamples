@isTest(seealldata=false)
private class CARPOL_UNI_AddAccountOverride_Test {
      @IsTest static void CARPOL_UNI_AddAccountOverride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();

          User usershare = new User();
          usershare.ProfileID = [Select Id From Profile Where Name='APHIS Applicant'].id;
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.LanguageLocaleKey = 'en_US';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.FirstName = 'first';
          usershare.LastName = 'last';
          usershare.Username = 'test@test.com';   
          usershare.CommunityNickname = 'testUser123';
          usershare.Alias = 't1';
          usershare.Email = 'no@email.com';
          usershare.IsActive = true;
          usershare.ContactId = objcont.id;
         
          insert usershare;  
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_AddAccount;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objacct);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('RecordType','Test');
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');                                                                      
              
              CARPOL_UNI_AddAccountOverride extclass = new CARPOL_UNI_AddAccountOverride(sc);
              extclass.redirect();
              system.assert(extclass != null);              
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_AddAccount;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objacct);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('RecordType','Test');
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');                                                                      
              CARPOL_UNI_AddAccountOverride extclassempty = new CARPOL_UNI_AddAccountOverride();
              CARPOL_UNI_AddAccountOverride extclass = new CARPOL_UNI_AddAccountOverride(sc);
              extclass.redirect();     
              system.assert(extclass != null);
          Test.stopTest();   
      }
}