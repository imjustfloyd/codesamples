public class CARPOL_AC_EPRegulations_extension2 {

    public List<Authorization_Junction__c> scj {get;set;}
    public List<Authorization_Junction__c> scjA {get;set;}
    public List<Authorization_Junction__c> scjB {get;set;}
    public List<Authorization_Junction__c> scjC {get;set;}
    public Integer index { get; set; }
    public String regulationType { get; set; }
    public boolean showportspage {get;set;}
    public boolean showmessagepanel {get;set;}
    public Regulation__c regulation { get; set; }
    public Group__c regulationGroup { get; set; }
    public Group_Junction__c regulationGroupLookup { get; set; }
    public SelectOption[] allOptions { get; set; }
    public SelectOption[] selectedOptions { get; set; }
    public String selectedInputType { get; set; }
    public String leftLabel { get; set; }
    public String rightLabel { get; set; }
    public String signatureID = ApexPages.currentPage().getParameters().get('id');
    public Map<String, String> mapAllSavedRegulations = new Map<String, String>();
    public Map<String, String> mapAllSavedRegulationsGroup = new Map<String, String>();
    public List<Regulation__c> lstSavedRegulations = new List<Regulation__c>();
    public List<Group__c> lstSavedRegulationsGroup = new List<Group__c>();
    public String selectedType;
    public String selectedSubType;
    public Boolean showPopUpMessage;
    public String portpage{ get; set; }
    private Authorizations__c au {get;set;}
    public boolean live_animal_nosave_port ;
    //public String pageType { get; set; }
    //public String pageSubType { get; set; }
    
    public String regACRecordTypeId { get; set; }
    public CARPOL_AC_EPRegulations_extension2(ApexPages.StandardController controller) {
        System.Debug('<<<<<<< Standard Controller Constructor Begins >>>>>>>');
        getRegulation();
        Authorizations__c rrt;
        recordtype artname;
        Regulation__c rrtname;
         AC__c line_item;
         recordtype liname;
         String     sliname;
         live_animal_nosave_port = false ;
        //popup add regulations
        regulation = new Regulation__c();
        this.au= (Authorizations__c)controller.getRecord();
        if (au.Id!=null){
            au = [Select Authorization_Type__c, Program_Pathway__r.Permit_PDF_Template__c, Prefix__c, Status__c, Id, Response_Type__c,RecordTypeId, RecordType.Name, Application__c, Thumbprint__c,Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c,Thumbprint__r.Program_Prefix__r.name,Effective_Date__c,Expiration_Date__c,Expiration_Timeframe__c FROM Authorizations__c Where Id =:au.Id];
            string recordTypeName=ApexPages.currentPage().getParameters().get('RecordType');
            artname=[select name from recordtype where id =: au.RecordTypeId ];//added by ram on 2/24/2016
            rrtname = [select recordtypeid from regulation__c where recordtype.name like :artname.name limit 1];//added by ram on 2/24/2016
            //system.debug('---recordTypeName--'+recordTypeName+'---artname--'+artname.name);
            line_item = [select recordtypeid,id,name, Program_Line_Item_Pathway__c, Program_Line_Item_Pathway__r.Program__r.Name, movement_type__c from AC__c where  Authorization__c =:au.Id limit 1];//added by ram on 3/10/2016
            if (line_item.movement_type__c == 'Transit' || au.Prefix__c == '602')
              {
                   showportspage = false;
                   showmessagepanel = true;
               } else
               {
                   showportspage = true;
                   showmessagepanel = false;
                  }

            liname = [select name from recordtype where id =: line_item.RecordTypeId ];//added by ram on 3/10/2016
            sliname = string.valueOf(liname.name);//added by ram on 3/10/2016


         }
        showPopUpMessage = false;
        selectedInputType = 'Test';
        leftLabel= 'Available Regulations';
        rightLabel = 'Selected Regulations'; 
        //start  of change by ram on 3/10/2016
        if(sliname!=null)
        {
        if(line_item.Program_Line_Item_Pathway__r.Program__r.Name.contains('VS'))
        {
          
          portpage='carpol_vs_editpermit';
          live_animal_nosave_port = true;
           
        }
        else
        {
          portpage='CARPOL_AC_EditPermit';
        }
        }
        //start  of change by ram on 3/10/2016
        if(rrtname != null)
        {
        regACRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get(artname.name).getRecordTypeId();
        }
        else
        {
        regACRecordTypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        }
        
        regulationGroupLookup = new Group_Junction__c();
        allOptions = new List<SelectOption>();
        List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regACRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = :au.Id) Order By Custom_Name__c ASC LIMIT 999];
        for (Regulation__c rgltn : regulations ) 
        {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
        
        selectedOptions = new List<SelectOption>();
        List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = :au.Id) LIMIT 999];
        lstSavedRegulations = slctdRgltns;
        System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
        for (Regulation__c rgltn : slctdRgltns)
        {
            selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
            mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
        }
    }
    
    // Method for testing a hidden field functionality 
    public PageReference setRegulationType(){
        System.debug('regulationType: ' + regulationType);
        regulation.Type__c = regulationType;
        getResults();
        return null;
        
    }
    
    //get regulations
    public void getRegulation()
    {
        ID authID = ApexPages.currentPage().getParameters().get('id');
        scj = new List<Authorization_Junction__c>([Select id, Sub_Type__c, is_Active__c, Authorization__c, Order_Number__c, Regulation_Short_Name__c, Regulation_Description__c,  Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :authID ORDER BY Order_Number__c ASC LIMIT 999]);
        System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
        
        scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        scjC = new List<Authorization_Junction__c>();
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' ){
                    scjA.add(SCJ);
                }
                
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){
                    scjB.add(SCJ);
                }
                
                if(SCJ.Regulation__r.Type__c=='Instruction for CBP Officers'){
                    scjC.add(SCJ);
                }
                
            }
        }
    }
    
//add regulations
public PageReference getResults(){
        try
        {
            System.Debug('<<<<<<< Getting Updated List >>>>>>>');
            showPopUpMessage = false;
            selectedType = regulation.Type__c;
            System.Debug('<<<<<<< Selected Type : ' + selectedType + ' >>>>>>>');
            selectedSubType = regulation.Sub_Type__c;
            System.Debug('<<<<<<< Selected Sub Type : ' + selectedSubType + ' >>>>>>>');
            Boolean flagWhereClause = false;
            
            if(selectedInputType != 'Group')
            {
                String allOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId=:regACRecordTypeId AND ';
                String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
                
                if(selectedType != null){
                    allOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                    flagWhereClause = true;
                }
                if(selectedSubType != null){
                    if(flagWhereClause == true){
                        allOptionsQuery += 'AND ';
                        selectedOptionsQuery += 'AND ';
                    }
                    allOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                    flagWhereClause = true;
                }
                
                if(flagWhereClause == true){
                    allOptionsQuery += 'AND ';
                    selectedOptionsQuery += 'AND ';
                }
                allOptionsQuery += 'ID NOT IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') Order By Custom_Name__c ASC LIMIT 999';
                selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') LIMIT 999';
                
                System.Debug('<<<<<<< Query - All Regulations : ' + allOptionsQuery + ' >>>>>>>');
                System.Debug('<<<<<<< Query - Saved Regulations : ' + selectedOptionsQuery + ' >>>>>>>');
                allOptions = new List<SelectOption>();
                List<Regulation__c> regulations = Database.query(allOptionsQuery);
                for (Regulation__c rgltn : regulations) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
                
                selectedOptions = new List<SelectOption>();
                List<Regulation__c> slctdRgltns = Database.query(selectedOptionsQuery);
                lstSavedRegulations = slctdRgltns;
                System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
                for (Regulation__c rgltn : slctdRgltns )
                {
                    //selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                    selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c));
                    mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
                }
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.1'));
            return null;
        }
    }
    
    public PageReference updateSignature() {
        
        try{
            if(selectedInputType != 'Group')
            {
                updateRegulations();
                //getRegulation();
                //showPopUpMessage = true;
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
                
                PageReference pageRef = Page.CARPOL_AC_EditPermit_Regulations2;
                pageRef.getParameters().put('Id',au.Id);
                pageRef.setRedirect(true);
                return pageRef;
                return null;
                 
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.2'));
            return null;
        }
        
    }
    
    public void updateRegulations() {
        
        System.Debug('<<<<<<< Updating Regulations on Signature >>>>>>>');
        try
        {
            System.Debug('<<<<<<< Add - All Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Add - Selected Regulations : ' + selectedOptions.size() + ' >>>>>>>');
            List<Authorization_Junction__c> regulationsToBeAdded = new List<Authorization_Junction__c>();
            List<Authorization_Junction__c> regulationsToBeRemoved = new List<Authorization_Junction__c>();
            Authorization_Junction__c sigRegulationMapp;
            
            // Logic to add Regulations and update the list
            for (SelectOption selReg : selectedOptions) {
                if(mapAllSavedRegulations.get(selReg.getValue()) == null)
                {
                    System.Debug('<<<<<<< New Regulation Found >>>>>>>');
                    sigRegulationMapp = new Authorization_Junction__c();
                    RecordType rt = [SELECT ID FROM RecordType WHERE Name = 'Regulation Junction' AND SObjectType = 'Authorization_Junction__c' LIMIT 1];
                    sigRegulationMapp.RecordTypeID = rt.ID;
                    sigRegulationMapp.is_Active__c = 'Yes';
                    sigRegulationMapp.Authorization__c = signatureID;
                    sigRegulationMapp.Regulation__c = selReg.getValue();
                    
                    
                    system.debug('<<<<<<<<<<<<<<<<<<<<<<<<+++++ Regulation : '+ sigRegulationMapp.Regulation__c);
                    
                    Regulation__c reg = [SELECT Regulation_Description__c FROM Regulation__c WHERE ID =:selReg.getValue()];
                    
                    system.debug('<<<<<<<<<<<<<<<<<<<<<<<<+++++ Regulation  Description: '+ reg.Regulation_Description__c);
                    
                    sigRegulationMapp.Regulation_Description__c = reg.Regulation_Description__c;
                    
                     system.debug('<<<<<<<<<<<<<<<<<<<<<<<<+++++ Regulation  Description: '+ sigRegulationMapp.Regulation_Description__c);
                     
                    //sigRegulationMapp.Regulation_Description__c= 'testing';
                    //sigRegulationMapp.Regulation_Description__c= selectedOptions.get(selReg.Regulation_Description__c);
                   //  sigRegulationMapp.Regulation_Description__c= sigRegulationMapp.Regulation__r.Regulation_Description__c;
                     
                    
                    
                    //o.Business_Division__c = myPickListMap.get(o.AccountId);
                    
                    regulationsToBeAdded.add(sigRegulationMapp);
                }
            }
            System.Debug('<<<<<<< Total New Regulations to be Added : ' + regulationsToBeAdded.size() + ' >>>>>>>');
            insert(regulationsToBeAdded);
            
            // Update the saved result set for regulations
            Boolean flagWhereClause = false;
            String selectedOptionsQuery = 'SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ';
            
            if(selectedType != null){
                selectedOptionsQuery += 'Type__c = \'' + selectedType + '\' ';
                flagWhereClause = true;
            }
            if(selectedSubType != null){
                if(flagWhereClause == true){
                    selectedOptionsQuery += 'AND ';
                }
                selectedOptionsQuery += 'Sub_Type__c = \'' + selectedSubType + '\' ';
                flagWhereClause = true;
            }
            if(flagWhereClause == true){
                selectedOptionsQuery += 'AND ';
            }
            selectedOptionsQuery += 'ID IN (SELECT Regulation__c FROM Authorization_Junction__c WHERE Authorization__c = \'' + signatureID + '\') LIMIT 999';
            System.Debug('<<<<<<< Query - Updated Saved Regulations : ' + selectedOptionsQuery + ' >>>>>>>');
            lstSavedRegulations = Database.query(selectedOptionsQuery);
            
            // Logic to remove regulations and update the list
            String strRemoveSignatureRegulationJunctionIds = '(';
            System.Debug('<<<<<<< Remove - Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            System.Debug('<<<<<<< Remove - Selected Regulations : ' + selectedOptions.size() + ' >>>>>>>');
            if(selectedOptions.size() == 0)
            {    
                for (Regulation__c svdRegulation : lstSavedRegulations) {
                    strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(svdRegulation.ID) + '\',';
                }
            }
            else
            {
                for(Integer i = 0; i < lstSavedRegulations.size(); i++)
                {
                    Integer matchFound = 0;
                    for(Integer j = 0; j < selectedOptions.size(); j++)
                    {
                        if(lstSavedRegulations[i].ID == selectedOptions[j].getValue())
                        {
                            matchFound += 1;
                            break;
                        }
                    }
                    if(matchFound == 0)
                    {
                        strRemoveSignatureRegulationJunctionIds += '\'' + String.ValueOf(lstSavedRegulations[i].ID) + '\',';
                    }
                }
            }
            if(strRemoveSignatureRegulationJunctionIds != '(')
            {
                strRemoveSignatureRegulationJunctionIds = strRemoveSignatureRegulationJunctionIds.substring(0, strRemoveSignatureRegulationJunctionIds.length() - 1);
                strRemoveSignatureRegulationJunctionIds += ')';
                System.Debug('<<<<<<< Regulations to be Removed : ' + strRemoveSignatureRegulationJunctionIds + ' >>>>>>>');
                String queryTemp = 'SELECT ID FROM Authorization_Junction__c WHERE Regulation__c IN ' + strRemoveSignatureRegulationJunctionIds + ' AND Authorization__c = :signatureID LIMIT 999';
                regulationsToBeRemoved = Database.query(queryTemp);
                System.Debug('<<<<<<< Authorization_Junction__c WHERE Authorization__c to be Removed : ' + regulationsToBeRemoved + ' >>>>>>>');
                delete(regulationsToBeRemoved);
            }
            
            if(regulationsToBeAdded.size() != 0 || regulationsToBeRemoved.size() != 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfuly Saved.'));
                //regulationsToBeAdded.clear();      
            }
            //regulationsToBeAdded.clear();
            //System.Debug('<<<<<<< RegulationsToBeAdded Size : ' + regulationsToBeAdded.size() + ' >>>>>>>');    
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.3'));
        }
    }
    
    public List<SelectOption> getAddByOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Individually','Individually'));
        options.add(new SelectOption('Group','By Group'));
        return options;
    }
    
    public PageReference selectInput() {
        System.Debug('<<<<<<< Selected Input Type : ' + selectedInputType + ' >>>>>>>');
        if(false){}
        else
        {
            //displayGroupInputs = false;
            leftLabel = 'Available Regulations';
            rightLabel = 'Selected Regulations';
            allOptions = new List<SelectOption>();
            List<Regulation__c> regulations = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE RecordTypeId =:regACRecordTypeId AND ID NOT IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            for (Regulation__c rgltn : regulations ) {allOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));}
            
            selectedOptions = new List<SelectOption>();
            List<Regulation__c> slctdRgltns = [SELECT ID, Name, Custom_Name__c, Short_Name__c FROM Regulation__c WHERE ID IN (SELECT Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c = :signatureID) LIMIT 999];
            lstSavedRegulations = slctdRgltns;
            System.Debug('<<<<<<< Total Saved Regulations : ' + lstSavedRegulations.size() + ' >>>>>>>');
            for (Regulation__c rgltn : slctdRgltns)
            {
                selectedOptions.add(new SelectOption(rgltn.ID, rgltn.Custom_Name__c + ' : ' + rgltn.Short_Name__c));
                mapAllSavedRegulations.put(rgltn.Id,rgltn.Id);
            }
        }
        //System.Debug('<<<<<<< Display Group Input Type :  : ' + displayGroupInputs + ' >>>>>>>');
        return null;
    }
    
    public PageReference viewDraftPDF() { 
   //niharika
   //@sahir: adding global permit configuration based selection of vf page
    PageReference pageRef;
   if(au.RecordType.Name =='Plant Protection & Quarantine (PPQ)' ){
       if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '251'){ // VV added on 7-12-16 for PEQ VF for template
             pageRef = Page.CARPOL_PPQ_PEQ_PermitPDF_DRAFT;
       }else if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '252'){ // VV added on 7-12-16 for PEQ VF for template
            pageRef = Page.CARPOL_PPQ_Transit_PermitPDF_DRAFT;
       }else if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '253'){ // VV added on 7-12-16 for PEQ VF for template
            pageRef = Page.carpol_cip_permit_pdf;
       }else if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null){
         pageRef = new PageReference('/apex/'+au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c);
       }else if(au.Program_Pathway__r.Permit_PDF_Template__c!=null){
             pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
       }else{
            pageRef = Page.CARPOL_PPQ_PermitPDF;
       }
    }
    else if(au.RecordType.Name =='Veterinary Services (VS)'){
        if(au.Program_Pathway__r.Permit_PDF_Template__c!=null)
               pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
        else
           pageRef = Page.CARPOL_VS_AuthorizationPDF;
    }
    else{
        if(au.Program_Pathway__r.Permit_PDF_Template__c!=null)
               pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
        else
            pageRef = Page.CARPOL_AC_LiveDogPermit_Auth;
    }
        pageRef.getParameters().put('Id',au.Id);
        pageRef.setRedirect(true);
        return pageRef;     
        }

    
    public PageReference createNewRegulation(){
        PageReference pageRef = Page.CARPOL_AC_Create_RegulationOnAuth;
        pageRef.getParameters().put('Id',au.Id);
        pageRef.setRedirect(true);
        return pageRef; 
    }

    public void attachPDF(){
        au.Expiration_Date__c = System.Today().AddDays(30);
        au.Effective_Date__c = System.Today();
         upsert this.au;
         System.debug('<<<<<<<<<<<<<<<<<< Saving Process >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
         List<Facility__c> lstselectedPorts = [SELECT ID, Name FROM Facility__c WHERE RecordType.Name = 'Ports' AND ID IN (SELECT Port__c FROM Authorization_Junction__c WHERE Authorization__c = :au.Id)];
         PageReference pageRef = new pagereference('');              
         Attachment att;
         // if(au.Response_Type__c != 'Permit') {
         if( au.Authorization_Type__c != 'Permit'){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit cannot be issued. Please check Decision type again'));
            // return null;
         }
         else {
             if(lstselectedPorts.size() == 0 && live_animal_nosave_port == false)
             {
               if(!showportspage){
                    pageRef = Page.CARPOL_PPQ_Transit_PermitPDF_DRAFT;
                    att = new Attachment();
                  pageRef.getParameters().put('id',au.Id);
                  pageRef.setRedirect(true);
                  Blob b;
                  if(!test.isRunningTest()){
                    b= pageRef.getContent();
                  }else{
                    b= Blob.valueOf('Some Text');          
                  }              
                  att.ParentId = au.id;//appid;
                  att.body = b;
                 // att.name='Permit_' + DateTime.now().format('yyyyMMdd') + '.pdf';
                  att.Name = 'Permit_'+date.today().format()+'.pdf';                
                  insert att;  
                  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Permit cannot be issued. Please check Decision type again'));
                  //return null;
                  //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved and Attached the Permit'));
               }
               /*if(au.RecordType.Name =='Plant Protection & Quarantine (PPQ)' ){
                 system.debug ('&*&* In loop');
                 CARPOL_PPQ_PermitPDFController page = new CARPOL_PPQ_PermitPDFController(new ApexPages.StandardController(au));
                 page.saveAttachment();}*/
                 else{  
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least 1 Port and click "Save Port" button before sending the Permit'));
                   // return null;
                 }
             }
             else 
             {
                System.debug('<<<<<<<<<<<<<<<<<< About to Save and Attach Document >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                //Added by Niharika
               if(au.RecordType.Name =='Plant Protection & Quarantine (PPQ)' ){
                //CARPOL_PPQ_PermitPDFController page = new CARPOL_PPQ_PermitPDFController(new ApexPages.StandardController(au));
                //page.saveAttachment();
                  if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '251'){ // VV added on 7-12-16 for PEQ VF for template
                        pageRef = Page.CARPOL_PPQ_PEQ_PermitPDF_DRAFT;
                   }else if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '252'){ // VV added on 7-12-16 for PEQ VF for template
                        pageRef = Page.CARPOL_PPQ_Transit_PermitPDF_DRAFT;
                   }else if (au.Thumbprint__r.Program_Prefix__r.Permit_PDF_Template__c!=null && au.Thumbprint__r.Program_Prefix__r.name  == '253'){ // VV added on 7-12-16 for PEQ VF for template
                        pageRef = Page.carpol_cip_permit_pdf;
                   }else if(au.Program_Pathway__r.Permit_PDF_Template__c!=null){
                        pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
                   } else {
                        pageRef = Page.CARPOL_PPQ_PermitPDF;
                   }
                   
               }else 
                if(au.RecordType.Name =='Veterinary Services (VS)' ){
                //CARPOL_VS_AttachLineItemPDF page = new CARPOL_VS_AttachLineItemPDF(new ApexPages.StandardController(au));
                //page.savePDF();
                      if(au.Program_Pathway__r.Permit_PDF_Template__c!=null)
                         pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
                      else
                         pageRef = Page.CARPOL_VS_AuthorizationPDF;
                    
                }       
               else{
                //CARPOL_AC_LiveDogPermit_Auth_extension page = new CARPOL_AC_LiveDogPermit_Auth_extension(new ApexPages.StandardController(au));
                //page.saveAttachment();
                if(au.Program_Pathway__r.Permit_PDF_Template__c!=null)
                    pageRef = new PageReference('/apex/'+au.Program_Pathway__r.Permit_PDF_Template__c);
                else
                    pageRef = Page.CARPOL_AC_LiveDogPermit_Auth;
              }
              system.debug('$$$$ pageRef = '+ pageRef ) ;
              att = new Attachment();
              pageRef.getParameters().put('id',au.Id);
              pageRef.setRedirect(true);
            try{
              // Take the PDF content
              Blob b;
              if(!test.isRunningTest()){
                b= pageRef.getContent();
              }else{
                b= Blob.valueOf('Some Text');          
              }              
              att.ParentId = au.id;//appid;
              att.body = b;
             // att.name='Permit_' + DateTime.now().format('yyyyMMdd') + '.pdf';
              att.Name = 'Permit_'+date.today().format()+'.pdf'; 
           
              insert att;
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved and Attached the Permit'));
            }catch(exception e){
            string CurrentClassName = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
            string exceptionMessage = e.getTypeName() + '\n' + e.getCause() + '\n' + e.getMessage() + '\n' + e.getLineNumber();
            EFLErrorLog.createerrorlog('ERROR',CurrentClassName,exceptionMessage);
                System.debug('<<<<<<<<<<<<<<<<<< End of Save and Attach Document >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Successfully Saved and Attached the Permit'));
            } }
           }
        }
}