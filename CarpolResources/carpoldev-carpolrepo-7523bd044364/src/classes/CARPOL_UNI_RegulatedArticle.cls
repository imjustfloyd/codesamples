public with sharing class CARPOL_UNI_RegulatedArticle {
    public List<RA_Scientific_Name_Junction__c> listRAPW=new List<RA_Scientific_Name_Junction__c>();
    public list<Id> RAIdList = new list<Id>();
    Public String status_active ='Active';
    
    
    public List<SelectOption> getRegulatedArticles(list<Regulated_Article__c > listRSA, Map<String, String> allAnswersMap){
        set<String> setNames=new set<String>();  
        string PriScientificName ;
        List<SelectOption> options = new List<SelectOption>();
             if(listRSA!= null && listRSA.size()>0){
                
                list<Id> RegIdList = new list<Id>();
                for(Regulated_Article__c recRA: listRSA){
                  RegIdList.add(recRA.Id);
                }
                
                list<RA_Scientific_Name_Junction__c > RASciJuncList = [Select Id,RA_Scientific_Name__r.name,Regulated_Article__c from RA_Scientific_Name_Junction__c where Regulated_Article__c in: RegIdList and Primary_Scientific_Name__c = TRUE];
                map<Id,String> regPrimaryMap = new map<id,String>();
                
                for(RA_Scientific_Name_Junction__c r: RASciJuncList){
                   if(!regPrimaryMap.containsKey(r.Regulated_Article__c)){
                    regPrimaryMap.put(r.Regulated_Article__c,r.RA_Scientific_Name__r.name);
                   }
                  }

                for(Regulated_Article__c recRA: listRSA){
                     if(regPrimaryMap.containsKey(recRA.Id)){
                      PriScientificName = regPrimaryMap.get(recRA.Id);
                     }
                    if(setNames.add(recRA.Name))
                    {
                    if(PriScientificName !=null){
                      options.add(new SelectOption(recRA.Id,recRA.Name+'{'+PriScientificName+'}'));
                    } else {
                      options.add(new SelectOption(recRA.Id,recRA.Name));
                    }
                    allAnswersMap.put (recRA.Name,recRA.Name);
                    }
                }
                
              /*  options.add(new SelectOption('Other', 'Other'));
                allAnswersMap.put ('Other', 'Other'); */
                return options;
        }
        else
            return null;
    }
    
    public List<Regulated_Article__c > searchRegulatedArticles(List<Regulated_Article__c > listRSA, string sSrhArticle, string sSelCat,string SelectedPathwayId){
        if(sSrhArticle!='' && sSrhArticle!=null)
        {
             listRAPW=[Select Regulated_Article__c,Primary_Scientific_Name__c from RA_Scientific_Name_Junction__c where Program_Pathway__c=:SelectedPathwayId];
             for(RA_Scientific_Name_Junction__c r:listRAPW ){
                 RAIdList.add(r.Regulated_Article__c);
             } 
             
            if(sSelCat!=null && sSelCat!='')
            {
                listRSA=database.query('select Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c from Regulated_Article__c where ID in:RAIdList and Category_Group_Ref__c=\''+sSelCat+'\' and status__c=\''+status_active+'\' and (Name like \'%'+sSrhArticle+'%\'OR Additional_Common_Name__c like \'%'+sSrhArticle+'%\'OR Family__c like \'%'+sSrhArticle+'%\'OR Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Additional_Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Common_Disease_Name__c like \'%'+sSrhArticle+'%\'OR Common_Name__c like \'%'+sSrhArticle+'%\')');
                
            }
            else
            {
                listRSA=database.query('select Name,Scientific_Name__c,Common_Name__c ,Additional_Common_Name__c,Additional_Scientific_Name__c from Regulated_Article__c  where ID in:RAIdList and status__c=\''+status_active+'\' and (Name like \'%'+sSrhArticle+'%\'OR Additional_Common_Name__c like \'%'+sSrhArticle+'%\'OR Family__c like \'%'+sSrhArticle+'%\'OR Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Additional_Scientific_Name__c like \'%'+sSrhArticle+'%\'OR Common_Disease_Name__c like \'%'+sSrhArticle+'%\'OR Common_Name__c like \'%'+sSrhArticle+'%\')');
            }
        }
        
        return listRSA;
    }
    
    public boolean showArticle (boolean bShowArticle, List<Regulated_Article__c > listRSA){
        if(listRSA!=null && listRSA.size()>0)
             bShowArticle=true;
         else
             bShowArticle=false;
             
         return bShowArticle;
    }
}