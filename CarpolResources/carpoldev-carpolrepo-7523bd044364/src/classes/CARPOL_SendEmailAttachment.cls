Public Class CARPOL_SendEmailAttachment {


 // These variables store Trigger.oldMap and Trigger.newMap
 // Map<Id, AC__c> oldLine;
  Map<Id, Application__c> newLine;
  Set<ID> setLineIds = new Set<ID>();

// This is the constructor
  // A map of the old and new records is expected as inputs
  public CARPOL_SendEmailAttachment(
   // Map<Id, AC__c> oldTriggerLines, 
    Map<Id, Application__c> newTriggerLines) {
    //  oldLine = oldTriggerLines;
      newLine = newTriggerLines;
      system.debug('#######################      ' + newTriggerLines + '     #############################');
  }
 
 
 public void SendEmailAttachment()  {
   for(Application__c app : newLine.values())
     {
    if (app.Sent_Application_to_Applicant__c == 'Yes')
    {


        List<Attachment> att = [select ID, Body from Attachment where ParentID = :app.Id ORDER BY CreatedDate DESC LIMIT 1];
        System.debug('<<<<<<<<<<<<<<<<<<< Number of Attachments: ' + att.size() + ' >>>>>>>>>>>>>>>>>>>>>>>>');
        System.debug('<<<<<<<<<<<<<<<<<<< Record Type: ' + app.RecordType.Name + ' >>>>>>>>>>>>>>>>>>>>>>>>');        
        System.debug('<<<<<<<<<<<<<<<<<<< Application Status: ' + app.Application_Status__c + ' >>>>>>>>>>>>>>>>>>>>>>>>');
        if(att.size() > 0 && app.Application_Status__c == 'Submitted' ){ // && app.RecordType.Name == 'Standard Application'){
            System.debug('<<<<<<<<<<<<<<<<<<< Sending Email Process Start >>>>>>>>>>>>>>>>>>>>');
            Messaging.SingleEmailMessage appEmail = new Messaging.SingleEmailMessage();
            
            // Strings to hold the email addresses to which you are sending the email.
            String[] toAddressesAppEmail = new String[] {'zabseitov@phaseonecg.com'}; 
            String[] ccAddressesAppEmail = new String[] {'vinar.amrutia@creativesyscon.com'};
        
            // Assign the addresses for the To and CC lists to the mail object.
            appEmail.setToAddresses(toAddressesAppEmail);
            appEmail.setCcAddresses(ccAddressesAppEmail);
            
            // Specify the address used when the recipients reply to the email. 
            appEmail.setReplyTo('support@efile.com');
            
            // Specify the name used as the display name.
            appEmail.setSenderDisplayName('eFile Support');
            
            // Specify the subject line for your email.
            appEmail.setSubject('USDA APHIS eFile - Application PDF : ' + app.Name);
            
            // Set to True if you want to BCC yourself on the email.
            appEmail.setBccSender(false);
            
            // Optionally append the salesforce.com email signature to the email.
            // The email address of the user executing the Apex Code will be used.
            appEmail.setUseSignature(false);
            
            String appEmailBody = 'Dear' + app.Applicant_Name__r.Name;
            appEmailBody += ',<br/><br/><br/>You have been issued a response for the application ' + app.Name + '. ';
            appEmailBody += 'Please find the response attached to this email.<br/><br/>If you are a registered user of the USDA APHIS eFile system, you will also find this document online <a href=' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + app.ID + '>click here.</a><br/><br/>';
            appEmailBody += 'If you have received this email and you are not the intended recipient, please reply to this email with the text "Wrong email".<br/><br/>';
            appEmailBody += 'Thank you,<br/>USDA APHIS eFile<br/><br/><br/>APHIS Animal Care<br/>301-851-3740<br/>Animal and Plant Health Inspection Service<br/>United States Department of Agriculture';
        
            appEmail.setHtmlBody(appEmailBody);
            
            Blob body = att[0].Body;
            
            // Create the email attachment
            Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
            emailAttachment.setFileName('Application.pdf');
            emailAttachment.setContentType('');
            emailAttachment.setBody(body);
            appEmail.setFileAttachments(new Messaging.EmailFileAttachment[] {emailAttachment});
            
            
            // Send the email you have created.
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { appEmail });
            System.debug('<<<<<<<<<<<<<<<<<<< Sending Email Process End >>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            }
        }
    }
}
}