public class portal_genotype_detail {
    public id genotypeID {get;set;}
    public Genotype__c obj {get;set;} 
    public ID conID;
    public string delrecid {get;set;}
    public Map<String,String> sobjectkeys {get;set;}
    public id LineitemId {get;set;}
    public Id strPathwayId{get;set;}
    public string redirect {get;set;}
    public ID genotypeRTID {get;set;}
    public portal_genotype_detail(ApexPages.StandardController controller) {
        Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
        sobjectkeys = new Map<String,String>();
            for(String s:describe.keyset()){
            sobjectkeys.put(describe.get(s).getDescribe().getKeyPrefix(),s);
            }        
        genotypeID = Apexpages.currentPage().getParameters().get('id');
        genotypeRTID = Apexpages.currentPage().getParameters().get('rectype');
        if(genotypeRTID == null)
            genotypeRTID = [select recordtypeid from genotype__c where id=:genotypeID].recordtypeid; 
        obj = (Genotype__c)controller.getRecord();
        conId = obj.Related_Construct_Record_Number__c;
        if(conId != null)  
           lineitemid = [select id, Line_Item__c from construct__c where id =: conId].Line_Item__c;    
        
    }

public List<Attachment> getattach(){
    return [SELECT   Name, Description, ContentType, CreatedById, LastModifiedByID, Id, ParentId FROM Attachment WHERE ParentId = : ApexPages.currentPage().getParameters().get('id')];
    }

    public pagereference Editgenotype(){
        PageReference pg = Page.portal_genotype_edit;
        pg.getParameters().put('genid',genotypeID);
        pg.getParameters().put('rectype',genotypeRTID);
        System.debug('Id sending from detail to edit page :' + genotypeID);
        return pg; 
    }
    public pageReference ret(){
          
       PageReference pg = Page.portal_construct_detail;
       //pg.getParameters().put('strPathwayId',strPathwayId); 
       pg.getParameters().put('id',conId);
       pg.setRedirect(true);
       return pg;
}

    
    public pagereference upload(){
        PageReference pg = Page.Portal_UploadAttachment;
        pg.getParameters().put('genid',genotypeID);
        pg.getParameters().put('retPage','portal_genotype_detail');
        System.debug('Id sending from detail to edit page :' + genotypeID);      
        return pg; 
    }
public PageReference NewGenotype(){
    PageReference pg = Page.portal_genotype_edit;
    pg.getParameters().put('constid', conId );
    pg.getParameters().put('rectype',genotypeRTID);    
    system.debug('Construct ID passing to genotype page : ' + conId );
    pg.setRedirect(true);
    return pg;    
    }
    
    
public List<Genotype__c> getgenotypes(){
return [SELECT ID,Name,Genotype__c,Construct_Component__c,Construct_Component_Text__c, Construct_Component_Name__c, Donor_list__c, Description__c, Related_Construct_Record_Number__c FROM Genotype__c WHERE Related_Construct_Record_Number__c =:conId  and recordtypeid =: genotypeRTID];
} 
  
 public PageReference deleteRecord(){
     string sobjkey = delrecid.substring(0,3);
     string sobjname = sobjectkeys.get(sobjkey);
     string  strqurey = 'select id from '+sobjname +' where id=:delrecid';
     
     list<sobject> lst = database.query(strqurey);
     delete lst;
     list<Genotype__c> Genolist = new list<Genotype__c>();
     
     Genolist = [SELECT ID,Related_Construct_Record_Number__c FROM Genotype__c WHERE Related_Construct_Record_Number__c =:conId];
         
     if(genotypeID != null && Genolist.size()>0){
          PageReference dirpage= new PageReference('/apex/portal_genotype_detail?id=' + genotypeID);
          dirpage.setRedirect(true);
          return dirpage;
     }else {
          PageReference dirpage= new PageReference('/apex/portal_construct_detail?id=' + conId);
          dirpage.setRedirect(true);
          return dirpage;         
         
     }
       
    }  
    
       
}