public with sharing class Portal_Authorization_Detail2_controller {

public Id authorizationID{get;set;}
public boolean displayCompAgreementButton {get;set;}
public boolean BRSlinkregulations {get;set;}
public List<Link_Authorization_Regulation__c> listlinkRegulations;
public List<Workflow_Task__c> wfLst {get;set;}
//Renewal code
public string Sectionheadertitle { get; set; }
public Authorizations__c objauthItem { get; set; }
//public AC__c objlineItem { get; set; }
//public AC__c[] oli { get; set; } 
public boolean renewButton {get;set;} 
public String Renewal {get;set;}    
 public Id lineitem {get;set;}  
 public string delrecid {get;set;}
 public Map<String,String> sobjectkeys {get;set;}
 public string redirect {get;set;}
public string recTypeName{get;set;}
public Id recordTypeId {get;set;}


//End Renewal code

    public Portal_Authorization_Detail2_controller(ApexPages.StandardController controller) {
        Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
        sobjectkeys = new Map<String,String>();
       
      authorizationID=ApexPages.currentPage().getParameters().get('id');
      
      //Show BRS Regulations when appropriate  
      listlinkRegulations =new List<Link_Authorization_Regulation__c>();
      listlinkRegulations = [SELECT ID, Name, Regulation__c FROM Link_Authorization_Regulation__c WHERE Authorization__c=: authorizationID];
      if(listlinkRegulations !=null && listlinkRegulations.size()>0)
       BRSlinkregulations = true;      
        
      // Changes below by Raghu for Applicant be able to click on Agree Compliance Agreement - PPQ F+V
      if(authorizationID != null){

          wfLst = [SELECT Id,Name,Status__c FROM Workflow_Task__c WHERE Authorization__c =:authorizationID AND (Workflow_Order__c = 3 OR Workflow_Order__c = 6) AND Status__c = 'In Progress' LIMIT 1];

          if(wfLst.size()>0){
              String wfStatus = wfLst[0].status__c;
              system.debug('workflow status : '+ wfStatus);
              if(wfStatus == 'In Progress'){
                  displayCompAgreementButton = true;
              }
          }
      }else{
          displayCompAgreementButton = false;
      }
      // Changes above by Raghu for Applicant be able to click on Agree Compliance Agreement - PPQ F+V
      // Changes below added for Renewal
      objauthitem =[select AC_Applicant_Address__c,Associated_Authorization__c,AC_Applicant_Email__c,AC_Applicant_Fax__c,AC_Applicant_Name__c,AC_Applicant_Phone__c,AC_Organization__c,AC_USDA_License_Expiration_Date__c,AC_USDA_License_Number__c,AC_USDA_Registration_Expiration_Date__c,
                    AC_USDA_Registration_Number__c,Applicant_Alternate_Email__c,Applicant_Instructions__c,Applicant_Organization__c,Applicant_Reference_Number__c,Applicant__c,Application_CBI__c,Application__c,
                    Authorization_Type__c,Authorized_User__c,Biological_Material_present_in_Article__c,Bio_Tech_Reviewer__c,BRAP_Manager1__c,BRAP_Manager2__c,BRAP_Manager_3__c,BRS_Create_Labels__c,BRS_Create_State_Review_Records__c,BRS_Hand_Carry_For_Importation_Only__c,
                    BRS_Introduction_Type__c,BRS_Number_of_Labels__c,BRS_Proposed_End_Date__c,BRS_Proposed_Start_Date__c,BRS_Purpose_of_Permit__c,BRS_Send_Labels__c,BRS_State_Review_Notification__c,CBI__c,Country_and_Locality_Information__c,CreatedById,CreatedDate,Date_Issued__c,Documents_Sent_to_Applicant__c,
                    Effective_Date__c,Email_Contents__c,EPA_Review_Required__c,Expiration_Date__c,Fee_Amount__c,Id,If_Yes_Please_Describe__c,IsDeleted,Issuer_Name__c,Issuer__c,Is_violator__c,Jurisdiction__c,LastActivityDate,LastModifiedById,LastModifiedDate,LastReferencedDate,
                    LastViewedDate,Locked__c,Mailing_Address__c,Means_of_Movement__c,Name,Number_of_New_Design_Protocols__c,Number_of_Release_Sites__c,Organization_Unique_ID__c,OwnerId,Permit_Number__c,Permit_Specialist__c,Plant_Inspection_Station__c,Ports_of_Arrival__c,Port_of_Entry_For_Importation_Only_del__c,
                    Port_of_Entry__c,Position__c,Prefix__c,RecordTypeId,Remaining_Agreement_Needed__c,Status__c,Sub_Status__c,SystemModstamp,Template__c,Thumbprint__c,Thumbprint__r.Program_Prefix__r.Renewal_Start_Period__c,Thumbprint__r.Program_Prefix__r.Renewal_End_Period__c,Total_Conditions__c,Total_of_New_Constructs__c,
                    Total_Supplemental_Conditions__c,UNI_Alternate_Phone__c,UNI_Country__c,UNI_County_Province__c,UNI_Zip__c,Expiry_Date__c,Share_Permit_Package_with_Applicant__c from authorizations__c WHERE Id =: authorizationID ];

      //if there is no expiry date we will never show the renew button
      if(objauthitem.Expiry_Date__c != null){
          //get the expiry rules from the prefix
          Integer startperiod;
          Integer endperiod;

          if(objauthitem.Thumbprint__r.Program_Prefix__r.Renewal_Start_Period__c != null){
              startperiod = Integer.valueof(objauthitem.Thumbprint__r.Program_Prefix__r.Renewal_Start_Period__c);
          } else {
              //default to 30 days
              startperiod = 30;
          }    
          if(objauthitem.Thumbprint__r.Program_Prefix__r.Renewal_End_Period__c != null){
              endperiod = Integer.valueof(objauthitem.Thumbprint__r.Program_Prefix__r.Renewal_End_Period__c);
          } else {
              //if no time period specified renewal period ends on expiration date
              endperiod = 0;
          }
      
          //set up dates
          Date today = Date.today();
          Date expiration = objauthitem.Expiry_Date__c;
          Date startdate = expiration-startperiod;     
               
          Date enddate;
          if(endperiod > 0){
              enddate = expiration.addDays(endperiod);
          } else {
              enddate = expiration;
          }
          
          if(today >= startdate && today <= enddate){
              renewButton = true;
          }
      }
    }

    public List<Attachment> getattach(){
        return [SELECT   Name, Description, ContentType, CreatedById, Id, ParentId FROM Attachment WHERE ParentId = : authorizationID order by createddate desc];
    }
    
    public List<AC__c> getList(){
        return [SELECT ID,Name, Scientific_Name__r.Name, Regulated_article__r.Name,Status__c, Color__c, Sex__c, Number_of_Labels__c, Signature__c FROM AC__c WHERE Authorization__c=: authorizationID];
    }
    public List<Applicant_Attachments__c> getappattach(){
        return [SELECT ID, Name, File_Name__c, RecordTypeID, Attachment_Type__c, CreatedById, Document_Types__c, File_Description__c, Tags__c, Total_Files__c,
                (SELECT   Name, Description, ContentType, CreatedById, Id, ParentId FROM Attachments) FROM Applicant_Attachments__c WHERE Authorization__c=: authorizationID];
    }
    public List<Link_Authorization_Regulation__c> getlinkRegulations(){
        return [SELECT ID, Name, Regulation__c, Regulation_Title__c,Regulation__r.Title__c, Introduction_Type__c,  Total_Conditions__c, Total_Agreed__c, Remaining_Agreement_Needed__c FROM Link_Authorization_Regulation__c WHERE Authorization__c=: authorizationID];
    }
    public pagereference agreeCompliance(){
        Workflow_Task__c wf = new Workflow_Task__c();
        wf.Id = wfLst[0].Id;
        wf.Status__c = 'Complete';
        update wf;
        pageReference pg = Page.Portal_Authorization_Detail2;
        pg.getParameters().put('Id',authorizationID);
        pg.setRedirect(true);
        return pg;
    }
    
    public pagereference rejectCompliance(){
        Workflow_Task__c wf = new Workflow_Task__c();
        wf.Id = wfLst[0].Id;
        wf.Status__c = 'Pending';
        wf.Status_Categories__c = 'Other';
        update wf;
        pageReference pg = Page.Portal_Authorization_Detail2;
        pg.getParameters().put('Id',authorizationID);
        pg.setRedirect(true);
        return pg;
    }
    
    //Renew button function
    public pageReference Renew(){
      PageReference pg = Page.Portal_Application_Detail;

      List<Application__c> applst = [SELECT Applicant_Name__c,RecordtypeId, Applicant_Email__c,Applicant_Phone__c,Applicant_Fax__c,Organization__c,Application_Status__c,
                                      Applicant_Address__c, US_Address__c FROM Application__c WHERE ID = :objauthitem.Application__c];
      //new application 
      List<Application__c> applst2 = new List<Application__c>();
      Application__c app = New Application__c();
      app.Applicant_Name__c = applst[0].Applicant_Name__c;
      app.Application_Status__c = 'open';
      app.RecordTypeId = applst[0].RecordTypeId;
      app.Renewal_Proposed_Start_Date__c = objauthitem.Expiry_Date__c+1;
      app.Renewal_Proposed_End_Date__c = objauthitem.Expiry_Date__c.addYears(1);
      app.Renewal_Application__c = true;
      insert app;
      DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
      List<String> fields = new List<String>(d.fields.getMap().keySet()); 
      String soql = 'select ' + String.join(fields, ', ') + ' from AC__c where Application_Number__c = \'' + objauthitem.Application__c+ '\'';        
      AC__c lineitemlst = Database.query(soql);        
/*      AC__c lineitemlst = [SELECT Id,Name,AC_Manager_Comments__c, AC_Unique_Id__c, Age_in_months__c, Air_Transporter_Flight_Number__c, 
                           Application_Number__c, Arrival_Time__c, Authorization_Content__c, Breed_Description__c, 
                           Breed_Name__c, Breed__c, Clerk_Comments__c, Color__c, Communication_Letter__c, Date_of_Birth__c, 
                           DeliveryRec_USDA_License_Expiration_Date__c, DeliveryRec_USDA_Regist_Expiration_Date__c, DeliveryRecipient_Email__c,
                           DeliveryRecipient_Fax__c, DeliveryRecipient_First_Name__c, DeliveryRecipient_Last_Name__c, 
                           DeliveryRecipient_Mailing_City__c, DeliveryRecipient_Mailing_Country__c, DeliveryRecipient_Mailing_State__c, 
                           DeliveryRecipient_Mailing_Street__c, DeliveryRecipient_Mailing_Zip__c, DeliveryRecipient_Phone__c, 
                           DeliveryRecipient_USDA_License_Number__c, DeliveryRecipient_USDA_Registration_Num__c, Deny_Resubmit__c, 
                           Departure_Time__c,Enforcement_Officer_Comments__c, Exporter_Email__c, Exporter_Fax__c, Exporter_First_Name__c, 
                           Exporter_Last_Name__c, Exporter_Mailing_City__c, Exporter_Mailing_Country__c, Exporter_Mailing_State__c, 
                           Exporter_Mailing_Street__c, Exporter_Mailing_Zip_Postal_Code__c, Exporter_Phone__c, F_Importer_First_Name__c, 
                           Importer_Email__c, Importer_Fax__c, Importer_First_Name__c, Importer_Last_Name__c, Importer_Mailing_City__c, 
                           Importer_Mailing_Country__c, Importer_Mailing_State__c, Importer_Mailing_Street__c, Importer_Mailing_ZIP_code__c, 
                           Importer_Phone__c, Importer_USDA_License_Expiration_Date__c, Importer_USDA_License_Number__c, 
                           Importer_USDA_Registration_Exp_Date__c, Importer_USDA_Registration_Number__c, Is_violator__c, Locked__c, 
                           Microchip_Number__c, Move_out_Hawaii__c, Other_identifying_information__c, Picture__c, Port_of_Embarkation__c, 
                           Port_of_Entry__c, Proposed_date_of_arrival__c, Purpose_of_the_Importation__c, Rejected_Date__c, Reviewer_Comments__c,
                           Sex__c, Status__c, Tattoo_Number__c, Transporter_Name__c, Transporter_Type__c, 
                           Treatment_available_in_Country_of_Origin__c, USDA_Expiration_Date__c, USDA_License_Expiration_Date__c, 
                           USDA_License_Number__c, USDA_Registration_Number__c, Applicant_Instructions__c, Arrival_Date_and_Time__c, 
                           CBI_Justification__c, DeliveryRecipient_Mailing_CountryLU__c, Delivery_Recipient_State_ProvinceLU__c, 
                           Departure_Date_and_Time__c, Does_This_Application_Contain_CBI__c, Exporter_Mailing_CountryLU__c, 
                           Exporter_Mailing_State_ProvinceLU__c, Hand_Carry__c, Health_Certificate_Attached__c, Importer_Mailing_CountryLU__c, 
                           Importer_Mailing_State_ProvinceLU__c, Introduction_Type__c, Number_of_Labels__c, Number_of_Release_Sites__c, 
                           Port_of_Entry_State__c, Proposed_End_Date__c, Proposed_Start_Date__c, Rabies_Vaccination_Certificate_Attached__c, 
                           Test_Field__c, Thumbprint__c, Time_Zone_of_Arriving_Place__c, Time_Zone_of_Departing_Place__c, Vet_Agreement_Attached__c, 
                           Contains_Applicant_Attachments__c, Number_of_New_Design_Protocols__c, Total_of_New_Constructs__c, Age_yr_months__c, 
                           Additional_Information__c, Application_Duration__c, Biological_Material_present_in_Article__c, 
                           Country_and_Locality_Information__c, Courtesy_Justification__c, If_yes_Please_Describe__c, Means_of_Movement__c, 
                           Processed_anyway_that_change_its_nature__c, Purpose_of_Permit__c, Quarantine_Facility__c, Seed_be_planted_or_propagated__c, 
                           USDA_License_Number_Format__c, USDA_Registration_Number_Format__c, Will_the_Seed_be_processed_in_any_way_th__c, 
                           Country_Of_Origin__c,Scientific_Name__r.Id FROM AC__c WHERE Application_Number__c = :objauthitem.Application__c]; */

      AC__c lineitem = lineitemlst.clone(false,true,false,false);
      lineitem.Application_Number__c = app.id;
      lineitem.Renew_Authorization__c = objauthitem.ID;
      lineitem.Status__c = 'Saved';
      lineitem.Proposed_Start_Date__c = objauthitem.Expiry_Date__c+1;
      lineitem.Proposed_End_Date__c = objauthitem.Expiry_Date__c.addYears(1);
      insert lineitem;

      List<Construct__c> conslst = [SELECT Id, Corrections_Required__c,BRS_Applicant_Response__c,BRS_Applicant_Response_Email__c,Construct_s__c,
                                    Contact__c,Default_Applicant_Eauth_Id__c,Identifying_Line_s__c,Line_Events__c,Mode_of_Transformation__c,
                                    Needs_Applicant_Attention__c,Line_Item__c,Other_Regulated_Article_s__c,Reason_Exipired_or_Inactive__c,Reason_for_disapproval__c,
                                    Status__c FROM Construct__c WHERE Line_Item__c = :lineitemlst.Id];

      List<Construct__c> conlst2 = new List<Construct__c>();                          
      if(conslst.size()>0){  
          for(integer i=0; i<conslst.size(); i++){                      
              Construct__c con = conslst[i].clone(false,true);
              con.Line_Item__c = lineitem.id;
              con.Construct_s__c = con.Construct_s__c + 'Ren';
              conlst2.add(con);
          }
      }
      insert conlst2;

      List<Construct_Application_Junction__c> cajlst = [SELECT Id,Design_Protocol_Record__c,Line_Item__c,Construct__c,Design_Protocol_Record_SOP__c
                                                        FROM Construct_Application_Junction__c WHERE Line_Item__c =:lineitemlst.Id];

      List<Construct_Application_Junction__c> cajlst2 = new List<Construct_Application_Junction__c>(); 
      if(cajlst.size()>0){
          for(integer i =0; i<cajlst.size(); i++){
              Construct_Application_Junction__c caj = cajlst[i].clone(false,true);
              caj.Line_Item__c = lineitem.id;
              cajlst2.add(caj);
          }
      }  
      insert cajlst2;                                       

      List<Location__c> loclst = [SELECT Id, Name,Active__c, Alternate_phone_number__c, Applicant_Instructions__c, City__c,Application__c, 
                                  Country__c,County__c, Level_2_Region__c, Critical_Habitat_Involved__c, Customer_Ref__c, Day_Phone_2__c, 
                                  Day_Phone__c,Email_1__c,Email_2__c,Fax_2__c,Fax__c,If_Yes_Please_Explain__c,Description__c,Line_Item__c,
                                  GPS_1__c, GPS_2__c, GPS_3__c, GPS_4__c, GPS_5__c, GPS_6__c, Location_Unique_Id__c, Material_Type__c, Proposed_Planting__c, 
                                  Org_L1__c, Org_L2__c, Other_Material_Types__c, Primary_Contact__c, Contact_Address1__c, Contact_City__c, Contact_County__c, 
                                  Contact_Name1__c, Contact_state__c, Contact_Zip__c, Proposed_end_date__c, Proposed_start_date__c, Quantity__c, 
                                  Release_History__c, Secondary_Contact__c, Contact_Address2__c, Secondary_Contact_City__c, Secondary_Contact_County__c, 
                                  Contact_Name2__c, Secondary_Contact_State__c, Secondary_Contact_Zip__c, State__c, Status__c, Street_Add1__c, Street_Add2__c, Street_Add3__c, 
                                  Street_Add4__c, Unit_of_Measure__c, Zip__c 
                                  FROM Location__c WHERE Line_Item__c = :lineitemlst.Id];

      List<Location__c> loclst2 = new List<Location__c>(); 
      if(loclst.size()>0){
          for(integer i =0; i<loclst.size(); i++){
              Location__c loc = loclst[i].clone(false,true);
              loc.Line_Item__c = lineitem.id;
              loc.Application__c = app.id;
              //loc.Active__c = loclst[i].Active__c;
              loclst2.add(loc);
          }
      }  
      insert loclst2;                         

      List<Applicant_Attachments__c> aalst = [SELECT Id, RecordTypeId,Applicant_Instructions__c,File_Name__c,Attachment_Type__c,Confinement_Protocols__c,
                                              Destination_Release_Description__c,Document_Types__c,File_Description__c,Final_Disposition_Description__c,
                                              Final_Disposition_Method__c,Internal_Only__c,NOTE_Notification__c,NOTE__c,Production_Design__c,
                                              Share_with_Applicant__c,Animal_Care_AC__c,Share_with_State__c,Status__c,Standard_Operating_Procedure__c,Tags__c,
                                              Total_Files__c FROM Applicant_Attachments__c WHERE Animal_Care_AC__c = :lineitemlst.Id];

      List<Applicant_Attachments__c> aalst2 = new List<Applicant_Attachments__c>();
      if(aalst.size()>0){
          for(integer i =0; i<aalst.size(); i++){
              Applicant_Attachments__c loc = aalst[i].clone(false,true);
              loc.Animal_Care_AC__c = lineitem.id;
              loc.Application__c = app.id;
              aalst2.add(loc);
          }
      }  
      insert aalst2; 

      List<Link_Regulated_Articles__c> lralst = [SELECT Cultivar_and_or_Breeding_Line__c,Regulated_Article__c,Line_Item__c,Application__c FROM Link_Regulated_Articles__c WHERE Line_Item__c = :lineitemlst.Id];
      System.debug('Regulated Articles :' + lralst);
      List<Link_Regulated_Articles__c> lralst2 = new List<Link_Regulated_Articles__c>();
      if(lralst.size()>0){
          for(integer i =0; i<lralst.size(); i++){
              Link_Regulated_Articles__c lr = lralst[i].clone(false,true);
              lr.Line_Item__c = lineitem.id;
              lr.Application__c = app.id;
              lralst2.add(lr);
          }
      }
      insert lralst2;

      Renewal = 'Renewal';
      pg.setRedirect(true);
      pg.getParameters().put('id',app.id);
      pg.getParameters().put('Renewal', Renewal);
      return pg;
      }
      
      
        //--------------------------------------------------------Self Reporting------------------------------------------------------------------------//
        
            public List<Applicant_Attachments__c > getSelfReporting(){
             return[SELECT ID,Name, Animal_Care_AC__c ,Attachment_Type__c ,File_Description__c, RecordType.Name, Document_Types__c,File_Name__c, Status__c ,Standard_Operating_Procedure__r.Name,Status_Graphical__c,Total_Files__c,(SELECT ID, Name FROM Attachments LIMIT 1) FROM Applicant_Attachments__c WHERE Authorization__c =:authorizationID];
         }
  
                   public PageReference deleteRecord(){
                         string sobjkey = delrecid.substring(0,3);
                         string sobjname = sobjectkeys.get(sobjkey);
                          string  strqurey = 'select id from '+sobjname +' where id=:delrecid';
                          list<sobject> lst = database.query(strqurey);
                          delete lst;
                    
                        PageReference dirpage= new PageReference('/apex/Portal_Authorization_Detail2?LineId='+authorizationID );
                        dirpage.setRedirect(true);
                        return dirpage;
                  }  
        
   public pageReference AddingSRP(){
        redirect = 'yes';
 
        
      //  if(Linelst[0].RecordType.Name == 'Biotechnology Regulatory Services - Standard Permit'){
            recTypeName = 'Self Reporting';
            recordTypeId = [SELECT ID FROM Recordtype WHERE Name=:recTypeName].ID;
            
            system.debug('<<<<<!!!!!record type id'+recordTypeId);
            //recordTypeId = '012350000000826';
      //  }
        
        pageReference pg = Page.portal_selfreporting;
      //  pg.getParameters().put('LineItemId',LineItemId);
        pg.getParameters().put('RecordType',recordTypeId);
      //   pg.getParameters().put('appId',AppId);
        pg.getParameters().put('authid',authorizationID);
        pg.getParameters().put('redirect',redirect);
        pg.setRedirect(true);
        return pg;
    }
      
    public String getreturnURL(){
        pageReference pg = ApexPages.currentPage();
        pg.getParameters().put('showReqDocsSection','true');

      /*  if(pg.getParameters().containsKey('message')){
            pg.getParameters().remove('message');
            pg.getParameters().put('message','File successfully removed');        
        } else {
            pg.getParameters().put('message','File successfully removed'); 
        }*/
        String newURL = pg.getUrl();        
        return newURL;
    }      
      
      
      
      
}