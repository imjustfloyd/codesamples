public class FutureAttachmentCreator {

    @Future(callout=true)
   public static void createAttachment(Id objpermitId,Id parentId,string PermitNumber,string sessionId){
   CARPOL_URLs__c base = CARPOL_URLs__c.getValues('Internal');   
   HttpRequest req = new HttpRequest();
   //req.setEndpoint('https://aphis--efilemerge.cs33.my.salesforce.com'+'/services/apexrest/createpdf/');
   req.setEndpoint(base.URL__c+'/services/apexrest/createpdf/');
   req.setMethod('POST');
   req.setBody('{"objpermitId":'+JSON.serialize(objpermitId)+'}'+','+'{"parentId":'+JSON.serialize(parentId)+'}'+','+'{"PermitNumber":'+JSON.serialize(PermitNumber)+'}');
   req.setHeader('Authorization', 'Bearer '+ sessionId);
   req.setHeader('Content-Type', 'application/json');
   Http http = new Http();
   HTTPResponse res = http.send(req);
   system.debug(res.getBody());
}

    @Future(callout=true)
    public static void ppqatt(string appid,string authid,string appstatus,Integer permitno,string sessionId)
    {
        CARPOL_URLs__c base = CARPOL_URLs__c.getValues('Internal');   
        HttpRequest req = new HttpRequest();
        //String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        /*if(baseURL.contains('district12'))
           req.setEndpoint('https://aphis--district12.cs33.my.salesforce.com'+'/services/apexrest/carpolppqattach/');
        else if(baseURL.contains('efilemerge'))
        req.setEndpoint('https://aphis--efilemerge.cs33.my.salesforce.com'+'/services/apexrest/carpolppqattach/');
        else if(baseURL.contains('efiledemo'))
        req.setEndpoint('https://aphis--efiledemo.cs32.my.salesforce.com'+'/services/apexrest/carpolppqattach/');
        */
       req.setEndpoint(base.URL__c+'/services/apexrest/carpolppqattach/');
       req.setMethod('POST');
       req.setBody('{"appid":'+JSON.serialize(appid)+'}'+','+'{"authid":'+JSON.serialize(authid)+'}'+','+'{"permitno":'+JSON.serialize(permitno)+'}');
       req.setHeader('Authorization', 'Bearer '+ sessionId);
       req.setHeader('Content-Type', 'application/json');
       Http http = new Http();
       HTTPResponse res = http.send(req);
    } 

     @future(callout=true)
    public static void sendVF(String Sessionid,string appid,string authid, string decisiontype)
    {
        System.debug('SesionId>>>'+Sessionid);
        
        //Replace below URL with your Salesforce instance host
        String addr;
        CARPOL_URLs__c base = CARPOL_URLs__c.getValues('Internal');   
        addr = base.URL__c + '/services/apexrest/carpolppqattach';
        /*String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('<<< baseURL is '+baseURL);
        if(baseURL.contains('District12')){
           addr = 'https://aphis--district12.cs33.my.salesforce.com'+'/services/apexrest/carpolppqattach';}
        else if(baseURL.contains('efilemerge')){
            addr = 'https://aphis--efilemerge.cs33.my.salesforce.com/services/apexrest/carpolppqattach';}
        else {
            addr = 'https://aphis--efiledemo.cs32.my.salesforce.com/services/apexrest/carpolppqattach';}
            //if(baseURL.contains('efiledemo'))
            system.debug('<!!!!! baseURL'+baseURL);
            system.debug('<!!! addr');
        */            
        HttpRequest req = new HttpRequest();
        req.setEndpoint(addr);
        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + Sessionid);
        req.setHeader('Content-Type','application/json');
        //string reqbody = '{"authid":'+authid+'}' ;
     //   String body =  '{"authid":"' + authid +'"}';
      //  String body =  '{"authid":"' + authid + '",'+ '"decisiontype" :"'+ decisiontype +'"}';
        String body =  '{"authid":"' + authid + '",'+ '"decisiontype" :"'+ decisiontype + '",'+ '"appid" :"'+ appid + '"}';
        
        
        //req.setBody('{"authid":'+authid+'}');
        req.setBody(body);
        Http http = new Http();
        HttpResponse response = http.send(req);
        
        System.debug('Status Code>>>'+response.getStatusCode()+'Message'+response.getStatus());
        System.debug('Session Id'+Sessionid);
    }
    
}