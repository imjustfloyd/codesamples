public class CARPOL_UNI_AddApplicationOverride{
private ApexPages.StandardController controller;
public String retURL {get; set;}
public String saveNewURL {get; set;}
public String rType {get; set;}
public String cancelURL {get; set;}
public String ent {get; set;}
public String confirmationToken {get; set;}

 public CARPOL_UNI_AddApplicationOverride() {

  }


 public String recordId;

public CARPOL_UNI_AddApplicationOverride(ApexPages.StandardController controller) {
     this.controller = controller;
     recordId = controller.getId();
     

    retURL = ApexPages.currentPage().getParameters().get('retURL');
    rType = ApexPages.currentPage().getParameters().get('RecordType');
    cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
    ent = ApexPages.currentPage().getParameters().get('ent');
    confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
    saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
     
     }

public PageReference redirect() {
PageReference customPage;

Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
if ('APHIS Applicant'.equals(p.name)
    || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name)||'State Reviewer'.equals(p.name))
    {
     customPage =New pagereference('/apex/Portal_Application_Edit');
     //customPage.setRedirect(true);
     //customPage.getParameters().put('id', recordId);
     //return customPage;
     system.debug('************0'+customPage);
    } else {
        customPage = new PageReference('/'+ SObjectType.Application__c.keyPrefix +'/e');
    }
    customPage.getParameters().put('retURL', retURL);
    customPage.getParameters().put('RecordType', rType);
    customPage.getParameters().put('cancelURL', cancelURL);
    customPage.getParameters().put('ent', ent);
    customPage.getParameters().put('CONFIRMATIONTOKEN', confirmationToken);
    customPage.getParameters().put('save_new_url', saveNewURL);
    customPage.getParameters().put('nooverride', '1');
    
    system.debug('************'+customPage);
   // String hostname = ApexPages.currentPage().getHeaders().get('Host');
          // String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
         //  String optyURL2 = 'https://'+hostname+'Pre/'+ SObjectType.Application__c.keyfix +'/?nooverride=1';
          // pagereference pageref = new pagereference(optyURL2);
           customPage.setredirect(true);
           return customPage;

     //  return null; //otherwise stay on the same page
   // }
 }
    
 }