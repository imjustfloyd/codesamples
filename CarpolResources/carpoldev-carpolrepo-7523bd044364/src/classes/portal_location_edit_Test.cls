@isTest(seealldata=false)
private class portal_location_edit_Test {
      @IsTest static void portal_location_edit_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c objpathway = testData.newCaninePathway();

          Application__c objapp = testData.newapplication();
          AC__c objac = testData.newLineItem('Personal Use',objapp);               
          Applicant_Attachments__c objaa = testData.newAttach(objac.id);
          Attachment objattach = testData.newAttachment(objaa.id);
          
          Trade_Agreement__c ta = new Trade_Agreement__c();
          ta.name='test';
          ta.Trade_Agreement_code__c='12';
          insert ta;          
          
          country__c c = new country__c();
          c.Name='Test';
          c.country_code__c='12';
          c.Trade_Agreement__c=ta.Id;
          insert c;

          Level_1_Region__c lr = new Level_1_Region__c();
          lr.Name='Test';
          lr.country__c=c.Id;
          insert lr;

          level_2_Region__c l2 = new level_2_Region__c();
          l2.Name='Test';
          l2.level_1_Region__c=lr.Id;
                  
          Location__c objloc = new Location__c(Name='testloc', Country__c=c.id, state__C= lr.id,
             Contact_Name1__c = 'Test',
             Day_Phone__c = '555-555-1212',
             Level_2_Region__c = l2.id, 
             GPS_1__c = 'GPS_1__c',
             GPS_2__c = 'GPS_2__c',
             GPS_3__c = 'GPS_3__c',
             GPS_4__c = 'GPS_4__c',
             GPS_5__c = 'GPS_5__c',
             GPS_6__c = 'GPS_6__c');
            insert objloc;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
        
          //run as salesforce user
              PageReference pageRef = Page.portal_location_edit;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objloc);
              ApexPages.currentPage().getParameters().put('LineitemId',objac.id);                            
              ApexPages.currentPage().getParameters().put('strPathwayId',objpathway.id);                                          
              ApexPages.currentPage().getParameters().put('redirect','Yes');
              ApexPages.currentPage().getParameters().put('LineId',objac.id);
              ApexPages.currentPage().getParameters().put('genid',objloc.id);  
              RecordType aRT = [Select Id,Name, DeveloperName From RecordType Where DeveloperName = 'Origin_and_Destination' LIMIT 1];            
              ApexPages.currentPage().getParameters().put('recType',aRT.Id);              

              portal_location_edit extclass = new portal_location_edit(sc);
              extclass.saveloc();          
              extclass.cancel();    
              System.assert(extclass != null);                      
              aRT = [Select Id,Name, DeveloperName From RecordType Where DeveloperName = 'Origin_Location' LIMIT 1];                          
              ApexPages.currentPage().getParameters().put('recType',aRT.Id);                            
              ApexPages.currentPage().getParameters().put('redirect','No');              
              portal_location_edit extclass1 = new portal_location_edit(sc);
              extclass1.saveloc();          
              extclass1.cancel();  
              extclass1.LineId = objac.id;
              extclass1.RelLoc = 'Test';
              extclass1.genid = objloc.id;
          Test.stopTest();   
      }
}