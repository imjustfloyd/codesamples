@isTest
    private class eAuthJITUserProvisioningTest {
    static testMethod void testCreateAndUpdateUser() {
        eAuthJITUserProvisioning handler = new eAuthJITUserProvisioning();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
            'testFirst testLast', 'testuser@example.org', null, 'testuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{'language' => 'en_US'});
        //need all the ids
        User u = handler.createUser(null, null, null,null, null, null); //(null, sampleData)
        System.assertEquals('testuserlong@salesforce.com', u.userName);
        System.assertEquals('testuser@example.org', u.email);
        System.assertEquals('testLast', u.lastName);
        System.assertEquals('testFirst', u.firstName);
        System.assertEquals('testuser', u.alias);
        insert(u);
        String uid = u.id;

        sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
            'testNewFirst testNewLast', 'testnewuser@example.org', null, 'testnewuserlong', 'en_US', 'facebook',
            null, new Map<String, String>{});
        handler.updateUser(null,null,null,null, null, null,null); //(uid, null, sampleData);
         
        User updatedUser = [SELECT userName, email, firstName, lastName, alias FROM user WHERE id=:uid];
        System.assertEquals('testnewuserlong@salesforce.com', updatedUser.userName);
        System.assertEquals('testnewuser@example.org', updatedUser.email);
        System.assertEquals('testNewLast', updatedUser.lastName);
        System.assertEquals('testNewFirst', updatedUser.firstName);
        System.assertEquals('testnewu', updatedUser.alias);
        system.assert(handler != null);                             
    }
    }