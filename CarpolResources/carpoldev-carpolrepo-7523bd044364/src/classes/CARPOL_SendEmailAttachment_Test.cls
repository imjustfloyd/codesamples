@isTest(seealldata=true)
public class CARPOL_SendEmailAttachment_Test {
      @IsTest static void CARPOL_SendEmailAttachment_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          objapp.Sent_Application_to_Applicant__c = 'Yes';
          objapp.Application_Status__c = 'Submitted';
          update objapp;
          Attachment att = new Attachment(parentId = objapp.Id, name = 'Test'+ '.pdf', body = Blob.valueOf('Some Text'));
          insert att;
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
         
              Test.startTest(); 
              Map<Id,Application__c> mapApp = new Map<Id,Application__c>();
              mapApp.put(objapp.id,objapp);                                                                                                                                                                              
              CARPOL_SendEmailAttachment extclass = new CARPOL_SendEmailAttachment(mapApp);
              extclass.SendEmailAttachment();
              system.assert(extclass != null);

          Test.stopTest();    

      }

}