public class portal_SelfReport_edit {
public Self_Reporting__c obj {get;set;}
public ID recordTypeId {get;set;}
public ID rectype {get;set;}
public String Volmeetrep {get;set;}
public String PlantingReport {get;set;}
public String Handcarry {get;set;}

    public portal_SelfReport_edit  (ApexPages.StandardController controller) {
                obj = (Self_Reporting__c)controller.getRecord();
                obj.Authorization__c = ApexPages.currentPage().getParameters().get('AuthoId');

                recordTypeId = ApexPages.currentPage().getParameters().get('rectype');
                list<RecordType> rts = [SELECT ID, DeveloperName FROM RecordType WHERE ID = :recordTypeId];
                if(rts.size()>0){
                    for(RecordType r : rts){
                    	if(r.DeveloperName == 'Hand_Carry_Information_Form'){
                    		Handcarry = 'Yes';
                    	}else if(r.DeveloperName == 'Planting_Report'){
                    		PlantingReport = 'Yes';
                    	}else if(r.DeveloperName == 'Volunteer_Monitoring_Report'){
                    		Volmeetrep = 'Yes';
                    	}

                   	}
                }
    }

}