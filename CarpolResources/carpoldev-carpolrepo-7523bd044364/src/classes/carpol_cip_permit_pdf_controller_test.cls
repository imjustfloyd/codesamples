/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class carpol_cip_permit_pdf_controller_test {

    static testMethod void test_carpol_cip_permit_pdf_controller() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Personal Use',objapp);      
      AC__c ac3 = testData.newLineItem('Veterinary Treatment',objapp);
      Authorizations__c objauth = testData.newAuth(objapp.Id); 
      ac.Authorization__c = objauth.Id;
      update ac;
      
      Test.startTest();
	      PageReference pageRef = Page.carpol_cip_permit_pdf;
	      Test.setCurrentPage(pageRef);
	      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
	      ApexPages.currentPage().getParameters().put('Id',objauth.id);
	      carpol_cip_permit_pdf_controller objCarpol = new carpol_cip_permit_pdf_controller(sc);
	      objCarpol.getTable();	      
	      List<Authorization_Junction__c> lstAJ = objCarpol.getConditions();
	      objCarpol.saveAttachment();	      
	  Test.stopTest();
       
       
        
    }
}