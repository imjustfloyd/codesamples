public class CARPOL_UNI_ViewLineOverride {
  public CARPOL_UNI_ViewLineOverride() {

  }

 String appId;
 String recordId;
 List<AC__c> aclst {get; set;}

public CARPOL_UNI_ViewLineOverride(ApexPages.StandardController
     controller) {
     recordId = controller.getId();
     aclst = [select application_number__c,Program_Line_Item_Pathway__c from AC__C where id=:recordID];
     appId = aclst[0].application_number__c;
     }

public PageReference redirect() {
Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
if ('APHIS Applicant'.equals(p.name)
    || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name))
    {
     PageReference customPage = page.carpol_uni_lineitem;//Page.CARPOL_UniversalLineItem;
     customPage.setRedirect(true);
     customPage.getParameters().put('ID', recordId);
     customPage.getParameters().put('strPathway', aclst[0].Program_Line_Item_Pathway__c);
     customPage.getParameters().put('appid', appId);
     return customPage;
    } else {
    String hostname = ApexPages.currentPage().getHeaders().get('Host');
           String sendURL = 'https://'+hostname+'/'+recordID +'?nooverride=1';
           pagereference pageref = new pagereference(sendURL);
           pageref.setredirect(true);
           return pageref;

       return null; //otherwise stay on the same page
    }
 }
}