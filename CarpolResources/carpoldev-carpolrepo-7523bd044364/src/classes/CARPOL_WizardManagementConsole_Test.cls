/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - AC Management Console Test Class
*/

@isTest(seealldata=false)
private class CARPOL_WizardManagementConsole_Test {
    
    static testMethod void wizManagementConsole() {
      
        PageReference pageRef = Page.CARPOL_WizardManagementConsole;
        Test.setCurrentPage(pageRef);
        CARPOL_WizardManagementConsole acMC = new CARPOL_WizardManagementConsole();
        acMC.getsignlst(); 
        List<Apexpages.Message> pageMessages1 = ApexPages.getMessages();
        
        String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
        
        CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
        testData.insertcustomsettings();
        Account objacct = testData.newAccount(AccountRecordTypeId); 
        Contact objcont = testData.newcontact();
        breed__c objbrd = testData.newbreed(); 
        Applicant_Contact__c apcont = testData.newappcontact(); 
        Applicant_Contact__c apcont2 = testData.newappcontact();
        Facility__c fac = testData.newfacility('Domestic Port');  
        Facility__c fac2 = testData.newfacility('Foreign Port');
        Application__c objapp = testData.newapplication();
        AC__c ac = testData.newLineItem('Personal Use',objapp);      
        Program_Prefix__c pp = testData.newPrefix();
        Signature__c tp = testData.newThumbprint();
        
                 
        acMC.getsignlst();
        acMC.Save();
        acMC.changesign();
        acMC.getresults();
        acMC.allRegulations = new List<Regulation__c>();
        acMC.allSignatures= new List<Signature__c>();        
        List<Apexpages.Message> pageMessages2 = ApexPages.getMessages();    
        system.assert(acMC != null);                                         
    }
}