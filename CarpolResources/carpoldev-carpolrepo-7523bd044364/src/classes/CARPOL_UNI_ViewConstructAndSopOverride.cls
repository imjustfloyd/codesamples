public class CARPOL_UNI_ViewConstructAndSopOverride{
  public CARPOL_UNI_ViewConstructAndSopOverride() {

  }


 String recordId;

public CARPOL_UNI_ViewConstructAndSopOverride(ApexPages.StandardController
     controller) {recordId = controller.getId();}

public PageReference redirect() {
Profile p = [select name from Profile where id =
             :UserInfo.getProfileId()];
if ('APHIS Applicant'.equals(p.name)
    || 'APHIS Account Admin'.equals(p.name) || 'APHIS Broker/Preparer'.equals(p.name) || 'State Reviewer'.equals(p.name))
    {
     PageReference customPage =
Page.portal_Application_Junction_detail;
     customPage.setRedirect(true);
     customPage.getParameters().put('id', recordId);
     return customPage;
    } else {
    String hostname = ApexPages.currentPage().getHeaders().get('Host');
           String optyURL2 = 'https://'+hostname+'/'+recordID +'?nooverride=1';
         //  String optyURL2 = 'https://'+hostname+'/'+ SObjectType.Construct_Application_Junction__c.keyPrefix +'/?nooverride=1';
           pagereference pageref = new pagereference(optyURL2);
           pageref.setredirect(true);
           return pageref;

       return null; //otherwise stay on the same page
    }
 }
    
 }