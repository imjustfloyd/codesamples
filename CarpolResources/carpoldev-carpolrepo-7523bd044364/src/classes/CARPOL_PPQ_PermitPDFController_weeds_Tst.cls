@IsTest(SeeAllData=true)
public class CARPOL_PPQ_PermitPDFController_weeds_Tst{
@IsTest static void weedsPermitTest(){
String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Column_1_API_Name__c = 'Life_Stage__c';
          plip.Column_2_API_Name__c = 'Originally_Collected__c';
          plip.Column_3_API_Name__c = 'Culture_Designation__c';
          plip.Column_4_API_Name__c = 'Approved_Facility__c';                              
          plip.Column_5_API_Name__c = 'Application_Number__c';                              
          plip.Column_6_API_Name__c = 'Arrival_Date_and_Time__c';                                                  
          update plip;
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          insert objdm;
          Level_1_Region__c FacState = testdata.newlevel1regionAL();
          Facility__c facility = new Facility__c();
          country__c FacCountry = testdata.newcountryus();
          facility.name = 'Weeds Test Facility';
          facility.Facility_ID__c = 1234567890 ;
          facility.Address_1__c = '123 Weeds Test St';
          facility.Address_2__c = 'Suite 123';
          facility.City__c = 'Fairfax';
          facility.State__c = FacState.id;         
          facility.Zip__c = '12345';        
          facility.Country__c = FacCountry.id;          
          facility.Approved__c = 'Yes';
          insert facility;          
          Application__c objapp = testData.newapplication();
          AC__c lineItem = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          lineItem.Authorization__c = objauth.id;
          lineitem.approved_facility__c = facility.id;
          update lineItem ;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Final_Decision_Matrix_Id__c = objdm.id;
          objauth.Date_Issued__c = Date.today();          
          objauth.Program_Pathway__c = plip.id;
          //update objauth;
         // Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
         Regulations_Association_Matrix__c  objAssmtr = new Regulations_Association_Matrix__c();
         objAssmtr.Program_Line_Item_Pathway__c = plip.id;
         insert objAssmtr;
         objauth.Final_Decision_Matrix_Id__c = objAssmtr.id;
         update objauth;
          Test.startTest(); 
              PageReference pageRef = Page.CARPOL_PWS_PERMITPDF;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CARPOL_PPQ_PermitPDFController_weeds extclassempty = new CARPOL_PPQ_PermitPDFController_weeds();              
              CARPOL_PPQ_PermitPDFController_weeds extclass = new CARPOL_PPQ_PermitPDFController_weeds(sc);
              extclass.getTable();
              extclass.getConditions();
              extclass.getACRecords();              
                
}
}