@isTest
private class CARPOL_UNI_Inspection_trigger_test {

	private static testMethod void test() {
	    Profile p = [SELECT ID,NAme FROM Profile WHERE Name=:'System Administrator' LIMIT 1];
         User u = [SELECT ID,Name FROM User WHERE Profileid=:p.id AND isActive=True LIMIT 1];
         
         system.runAs(u){
         String FacilityRecordTypeID = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
	     String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();
         CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          
           Facility__c fac = new Facility__c();
           fac.Name = 'Test Facility';
           fac.RecordTypeId = FacilityRecordTypeID;
           insert fac;
         
           Facility__c facc = [SELECT Id,Name FROM Facility__c WHERE ID=:fac.id];
          
          Inspection__c Ins = new Inspection__c();
          //Ins.Inspector__c = objcont.id;
          //ins.inspector_email__c=objcont.email;
          ins.Facility__c = facc.id;
          insert Ins;
          
          Contact con = [SELECT ID,Name FROM Contact WHERE ID=:objcont.id];
          Ins.Inspection_Comments__c = 'Comments test';
          Ins.Inspector__c = con.id;
          update Ins;
         }
	}

}