@isTest(SeeAlldata=false)
private class CARPOL_BRS_GenotypeTrigger_Test {
	@isTest
    private static void test_CARPOL_BRS_GenotypeTrigger(){
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      AC__c ac = testData.newLineItem('Release',objapp);      
      AC__c ac3 = testData.newLineItem('Import',objapp);            
      Authorizations__c objauth = testData.newAuth(objapp.Id);      
      ac.Authorization__c = objauth.Id;
      update ac;
      
      Construct__c con = new Construct__c();
      con.Line_Item__c = ac.Id;
      con.Construct_s__c = 'Description text goes here..';
      con.Mode_of_Transformation__c = 'Direct Injection';
      insert con;
      
      Genotype__c gen = new Genotype__c();
      gen.Construct_Name__c = 'Test Genotype';
      gen.Related_Construct_Record_Number__c = con.Id;
      gen.Description__c =  'Description text goes here..';
      gen.Construct_Component__c = 'Gene';
      gen.Construct_Component_Name__c = 'This is a test construct comp';
      insert gen;
      
      
      Test.startTest();
      gen.Construct_Component__c = 'Other';
      update gen;
      System.assert(gen != null);
      Test.stopTest();
      
      
      
    }
    
}