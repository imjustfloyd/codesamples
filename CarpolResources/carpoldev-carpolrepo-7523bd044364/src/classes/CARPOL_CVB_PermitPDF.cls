public class CARPOL_CVB_PermitPDF {

public ID appId {get; set;}
public ID authId {get; set;}

public Authorizations__c auth{get;set;}
public list<Authorizations__c> authList{get;set;}
List<AC__c> aclst {get; set;}
public AC__c lineitem{get;set;}

public Application__c appln {get;set;}
public List<Authorization_Junction__c> scj {get;set;}
public List<Authorization_Junction__c> scjA {get;set;}
public List<Authorization_Junction__c> scjB {get;set;}

public string portsSelected {get;set;}   

public CARPOL_CVB_PermitPDF(ApexPages.StandardController controller)
{
  auth = (Authorizations__c)controller.getRecord();
  
  authId = ApexPages.currentPage().getParameters().get('id');
  auth= [Select Id, Application__c,Program_Pathway__c from Authorizations__c where Id=: authId];
  
  authList = new list<Authorizations__c>();
  
   authList  = [SELECT Permit_Number__c,Date_Issued__c,Authorization_Letter_Content__c,Application__c, Application__r.Applicant_Name__r.name,Application__r.Applicant_Name__r.title,Application__r.Applicant_Address__c, Issuer_Name__c,  
                 Issuer_Title__c,Expiration_Date__c    FROM Authorizations__c WHERE id=:authId and Authorization_Type__c='Permit' ];
 // app = [Select Id, Application__c,Program_Pathway__c from Authorizations__c where Id=: authId];
  appId = auth.Application__c;
  getACRecords();
  
  list<Authorization_Junction__c> lstAuthjunct = new list<Authorization_Junction__c>();
        for(Authorization_Junction__c authjunct: [select id,Name,Port__c,Port__r.Name,Authorization__c from Authorization_Junction__c where Authorization__c=:authId  and Port__c!=null])
        {
            lstAuthjunct.add(authjunct);
        }
        if(lstAuthjunct.size()>0){
            for(integer i=0;i<lstAuthjunct.size();i++){
                if(i<lstAuthjunct.size()-1){
                    portsSelected += lstAuthjunct[i].Port__r.Name+'; ';
                }
                else{
                    portsSelected += lstAuthjunct[i].Port__r.Name;
                }
            }
        }
        system.debug('----portsSelected---'+portsSelected);   //5/16/16 END VV 

}

 public List<AC__c> getACRecords(){
        if(auth.Id != null){
            DescribeSObjectResult d = AC__c.SObjectType.getDescribe();
            List<String> fields = new List<String>(d.fields.getMap().keySet());
            String soql = 'select ' + String.join(fields, ', ') + ', RA_Scientific_Name__r.name, Country_Of_Origin__r.Name,Intended_Use__r.Name,Port_of_Entry__r.Name,Scientific_Name__r.Scientific_Name__c, Scientific_Name__r.Name,Group__r.Name,Component__r.Name ,Country_of_Export__r.Name,DeliveryRecipient_Last_Name__r.Name, DeliveryRecipient_Mailing_CountryLU__r.Name, Exporter_Last_Name__r.Name, Exporter_Mailing_CountryLU__r.Name,Port_of_Embarkation__r.Name from AC__c where Application_Number__c = \'' + appId + '\'';
            aclst = Database.query(soql);
            lineitem = aclst[0];
            
        }
     
        return aclst;
    }


public List<Authorization_Junction__c> getConditions(){
        
        try{
    
scj= new List<Authorization_Junction__c>();
    scj = [Select id, Is_Active__c, Regulation_Short_Name__c, Regulation_Description__c, Authorization__c, Order_Number__c, Thumbprint__c, Regulation__r.Title__c, Regulation__r.Type__c, Regulation__r.Sub_Type__c, Regulation__r.Short_Name__c, Regulation__r.Regulation_Description__c  from Authorization_Junction__c where Authorization__c = :authId ORDER BY Order_Number__c ASC];
    System.Debug('<<<<<<< Total scj : ' + scj.size() + ' >>>>>>>');
    
    scjA = new List<Authorization_Junction__c>();
        scjB = new List<Authorization_Junction__c>();
        
        
        if(scj.size()>0){
            for (Authorization_Junction__c SCJ: scj)
            {
                if(SCJ.Regulation__r.Type__c=='Import Requirements' && SCJ.Is_Active__c=='Yes' ){scjA.add(SCJ);}
                System.Debug('<<<<<<< Total Import Requirements : ' + scjA.size() + ' >>>>>>>');
                
                if(SCJ.Regulation__r.Type__c=='Additional Information'){scjB.add(SCJ);}
                System.Debug('<<<<<<< Total AI : ' + scjB.size() + ' >>>>>>>');
                
            
            }
        }
        
        }
            catch (Exception e){
                System.debug('ERROR:' + e);
            }
     return scj;
        
    }
  //End of Tharun changes --04/27/2016  


}