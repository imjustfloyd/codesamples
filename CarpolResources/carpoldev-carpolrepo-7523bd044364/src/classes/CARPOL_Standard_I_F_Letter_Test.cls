@isTest(seealldata=true)
public class CARPOL_Standard_I_F_Letter_Test {

    static testMethod void Test_CARPOL_Standard_I_F_Letter() {
      String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

      CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
      testData.insertcustomsettings();
      Account objacct = testData.newAccount(AccountRecordTypeId); 
      Contact objcont = testData.newcontact();
      breed__c objbrd = testData.newbreed(); 
      Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
      Regulated_Article__c regart1 = testData.newRegulatedArticle(plip.id);
      Country__c objcntry = testData.newcountryaf();
      Intended_Use__c testIU = testData.newIntendedUse(plip.Id);                
      Applicant_Contact__c apcont = testData.newappcontact(); 
      Applicant_Contact__c apcont2 = testData.newappcontact();
      Applicant_Contact__c apcont3 = testData.newappcontact();       
      Facility__c fac = testData.newfacility('Domestic Port');  
      Facility__c fac2 = testData.newfacility('Foreign Port');
      Application__c objapp = testData.newapplication();
      Authorizations__c objauth =testData.newAuth(objapp.Id);
      AC__c ac1 = testData.newlineitem('Personal Use', objapp, objauth);
      Signature__c tp = testData.newThumbprint();
      Communication_Manager__c cm = testData.newCommunicationmanager('No Permit Required');
      tp.Response_Type__c = 'No Permit Required';
      tp.Communication_Manager__c = cm.Id;
      tp.from_country__c = objcntry.id;
      update tp;
      objauth.Thumbprint__c = tp.Id;
      update objauth;
      group__c grp = new group__c (name = 'Khapra Beetle Countries'); insert grp;
      Country_Junction__c cntrygrp = new Country_Junction__c(Country__c = objcntry.id, group__c = grp.id);
      String ACWQRecordTypeId = Schema.SObjectType.Wizard_Questionnaire__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
      Wizard_Questionnaire__c objwq = new Wizard_Questionnaire__c();
      objwq.RecordTypeid = ACWQRecordTypeId;
      objwq.Type__c = 'NO PERMIT REQUIRED';
      objwq.Signature__c = tp.Id;
      insert objwq;
      Level_1_Region__c Lvl1rgn = new Level_1_Region__c(name = 'Test State', Country__c = objcntry.id);
      insert Lvl1rgn;  
      CARPOL_AC_Wizard acwizard = new CARPOL_AC_Wizard();
      acwizard.getLetterInformal();
      acwizard.getLetterFormal();
      acwizard.getLetterNJFormal();
      acwizard.getLODFormal();
      acwizard.getNPRFormal();
      
      PageReference pageRef = Page.CARPOL_Standard_Informal_Letter;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',objwq.id);
        ApexPages.currentPage().getParameters().put('strPathway',plip.id);        
        ApexPages.currentPage().getParameters().put('Scientific_Name__c',regart1.id);     
        ApexPages.currentPage().getParameters().put('Country_Of_Origin__c',objcntry.id);    
        ApexPages.currentPage().getParameters().put('Intended_Use__c',testIU.id);                                
        ApexPages.currentPage().getParameters().put('TPid',tp.id);                                        
        ApexPages.currentPage().getParameters().put('State_Territory_of_Destination__c',Lvl1rgn.id);                                                
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objwq);        
        CARPOL_Standard_Informal_Letter csil = new CARPOL_Standard_Informal_Letter(sc);

        
        //cls.saveAttachment();
        CARPOL_AC_Processing prcs = new CARPOL_AC_Processing();
        prcs.processID= '22';
        prcs.pageLoadFunc(); 
        prcs.attachLetter();
       PageReference pageRef2 = Page.CARPOL_Standard_Formal_Letter;
        Test.setCurrentPage(pageRef2);
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objapp);
        ApexPages.currentPage().getParameters().put('Id',objapp.id);
        ApexPages.currentPage().getParameters().put('authId',objauth.id);
        CARPOL_Standard_Formal_Letter csfl = new CARPOL_Standard_Formal_Letter(sc2);
        system.assert(csfl != null);
       
    }

}