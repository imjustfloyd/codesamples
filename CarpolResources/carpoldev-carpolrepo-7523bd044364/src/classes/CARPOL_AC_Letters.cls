/*
// Author : Vinar Amrutia
// Created Date : Feb 25th 2015
// Purpose : To capture information from CARPOL_AC_Wizard page and display on the NJ Letter
*/
public with sharing class CARPOL_AC_Letters {

    public Boolean showCriteria { get; set; }
    public Application__c application { get; set; }
    public Authorizations__c authorization{ get; set;}
    public AC__c acLineItem { get; set; }
    public List<answerTrace> allAnswerTrace { get; set; }
    public String tempString { get; set; }
    public String questionsTemp { get; set; }
    public String answersTemp { get; set; }
    public Signature__c tp {get; set;}
    public ID tpId {get; set;}
     public String parm1 {get;set;}
     public String strPathway {get;set;}
     public String Scientific_Name {get;set;}
     public String Country_Of_Origin {get;set;}
   public String Intended_Use {get;set;}
   public String State_Territory_of_Destination {get;set;}
    public Regulated_Article__c regulatedArticle{get;set;}
    public Country__c CountryOfOrigin{get;set;}
      public Program_Line_Item_Pathway__c ProgramPathway{get;set;}
        public Intended_Use__c IntendedUse{get;set;}
    public Level_1_Region__c StateTerritoryofDestination{get;set;}
    public List<Wizard_Questionnaire__c> wq {get;set;}  // VV 3-31-16
                public wizard_selections__c ws {get; set;} // VV 3-31-16
        public String RA_Scientific_Name{get;set;} //Tharun added 03-31-16
        public RA_Scientific_Names__c RAScienticName{get;set;} //Tharun added 03-31-16
    // Standard Constructor
    public CARPOL_AC_Letters(ApexPages.StandardController controller) {
        tpId = ApexPages.currentPage().getParameters().get('TPId');
        ID applicationID = apexpages.currentpage().getparameters().get('applicationID');
        //ID applicationID;
        ID acLineItemID = apexpages.currentpage().getparameters().get('id');
        //Niharika 3/8
         parm1  = apexpages.currentpage().getparameters().get('parm1');
         strPathway = apexpages.currentpage().getparameters().get('strPathway');
         System.Debug('<<<<<<< parm1   : ' + parm1  + ' >>>>>>>');
          Scientific_Name  = apexpages.currentpage().getparameters().get('Scientific_Name__c');
        Country_Of_Origin  = apexpages.currentpage().getparameters().get('Country_Of_Origin__c');
        Intended_Use  = apexpages.currentpage().getparameters().get('Intended_Use__c');
         State_Territory_of_Destination = apexpages.currentpage().getparameters().get('State_Territory_of_Destination__c');
         RA_Scientific_Name=  apexpages.currentpage().getparameters().get('RA_Scientific_Name__c');//Tharun added 03-31-16

        if(strPathway  != null && strPathway  != 'null' ){
            ProgramPathway= [select name from Program_Line_Item_Pathway__c where id=:strPathway  ];
            System.Debug('<<<<<<< ProgramPathway Name: ' + ProgramPathway.name+ ' >>>>>>>');
        }
        if(Scientific_Name  != null && Scientific_Name  != 'null'){
            regulatedArticle= [select name,Common_Name__c  from Regulated_Article__c where id=:Scientific_Name  ];
            System.Debug('<<<<<<< regulatedArticle Name: ' + regulatedArticle.name+ ' >>>>>>>');
        }
         if(Country_Of_Origin  != null && Country_Of_Origin  != 'null'){
            CountryOfOrigin= [select id,name from Country__c where id=:Country_Of_Origin  ]; // added Id by VV 4-12-16
            System.Debug('<<<<<<< CountryOfOrigin Name: ' + CountryOfOrigin.name+ ' >>>>>>>');
        }
         if(Intended_Use  != null && Intended_Use  != 'null'){
            IntendedUse= [select name from Intended_Use__c where id=:Intended_Use  ];
            System.Debug('<<<<<<< IntendedUse.Name: ' + IntendedUse.name+ ' >>>>>>>');
        }
        if(State_Territory_of_Destination  != null && State_Territory_of_Destination  != 'null' ){
            StateTerritoryofDestination= [select name from Level_1_Region__c where id=:State_Territory_of_Destination  ];
            System.Debug('<<<<<<< State_Territory_of_Destination.Name: ' + StateTerritoryofDestination.name+ ' >>>>>>>');
        }
        //Tharun added 03-31-16
        if(RA_Scientific_Name!= null && RA_Scientific_Name!= 'null' ){
             RAScienticName= [select Id,name from RA_Scientific_Names__c where id=:RA_Scientific_Name];
        }
        
        System.Debug('<<<<<<< AC Line Item ID : ' + acLineItemID + ' >>>>>>>');
        if(acLineItemID != null){
            acLineItem = [SELECT ID, Name, Application_Number__c FROM AC__c WHERE ID =:acLineItemID LIMIT 1];
            applicationID = acLineItem.Application_Number__c;
        }
        System.Debug('<<<<<<< Application ID : ' + applicationID + ' >>>>>>>');
        if(applicationID != null){
        application = [SELECT ID, Name, Comments__c, Applicant_Name__r.Name, Applicant_Name__r.Mailing_Street_LR__c,       
                        Applicant_Name__r.Mailing_City_LR__c, Applicant_Name__r.Mailing_State_Province_LR__r.name, 
                        Applicant_Name__r.Mailing_Country_LR__r.name, 
                        Applicant_Name__r.Mailing_Zip_Postalcode_LR__c,
                        Applicant_Name__r.MailingStreet, 
                        Applicant_Name__r.MailingCity, 
                        Applicant_Name__r.MailingState, 
                        Applicant_Name__r.MailingCountry, 
                        Applicant_Name__r.MailingPostalCode                        
                        FROM Application__c 
                        WHERE ID =:applicationID LIMIT 1];
         authorization =[SELECT ID,Expiration_Date__c FROM Authorizations__c WHERE Application__c =:application.Id];               
         //aclineitem = [SELECT ID, RA_Scientific_Name__r.name from ac__c where Application_Number__c= :applicationID  limit 1]; //7-25-16 VV Added for Sciname        
        }
        if(tpId!= null ){
             tp = [SELECT ID, Name, Communication_Manager__c, Communication_Manager__r.Name,Line_Record_Type__c,
                    Intended_Use__r.Name,To_Level_1_Region__r.Name,From_Level_1_Region__r.Name,
                    REF_Program_Name__c,Port__r.Name,Communication_Manager__r.Content__c, Regulated_Article__r.Name,
        From_Country__r.Name  FROM Signature__c WHERE ID = : tpId LIMIT 1];
            system.debug('***'+tp);
            if(tp.id != null){
               if(tp.Line_Record_Type__c!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLineRecordType}',tp.Line_Record_Type__c);
                // VV 4-12-16
                string strlineitem = tp.Line_Record_Type__c;
                if(strlineitem.contains('PPQ')){
                integer groupcountry = [select count() from Country_Junction__c where Group__r.Name='Khapra Beetle Countries' and Country__c=:CountryOfOrigin.id];
                    if(groupcountry>0){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpCountrygroup}','Do not use jute or burlap bags as packing material. Using packing materials that can harbor Khapra Beetle could result in the rejection of your shipment.');
                    }
                    else{
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpCountrygroup}','');
                    }
                }
                // VV 4-12-16
         /* //VV added 7-26-16
            if(aclineitem.RA_Scientific_Name__r.name!=null){
            tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', aclineitem.RA_Scientific_Name__r.name );
            }                
           */     }
               /* if(tp.To_Level_1_Region__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', tp.To_Level_1_Region__r.Name);
                }*/ //commented by niharika-replaced with state of destination field selection
                if(tp.From_Level_1_Region__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpFromLevel1Region}', tp.From_Level_1_Region__r.Name);
                }
                if(tp.REF_Program_Name__c!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpProgramName}', tp.REF_Program_Name__c);
                }
                if(tp.Port__r.Name!=null){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPorts}', tp.Port__r.Name);
                }
                if(Scientific_Name !=null && Scientific_Name  != 'null'){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRegulatedArticle}', regulatedArticle.name);
                    if(regulatedArticle.Common_Name__c != null && regulatedArticle.Common_Name__c != '')
                        tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpCommonName}', regulatedArticle.Common_Name__c);
                }
               if(Country_Of_Origin !=null && Country_Of_Origin  != 'null'){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpFromCountry}', CountryOfOrigin.Name );
                }
                 if(strPathway !=null  && strPathway  != 'null'){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpProgramPathway}', ProgramPathway.name );
                }
                if(Intended_Use!=null && Intended_Use  != 'null'){
                tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpPurposeOfImportation}', IntendedUse.Name);
                }
                 //Tharun added 03-31-16
                if(RA_Scientific_Name !=null && RA_Scientific_Name !='null'){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', RAScienticName.name );
                }
                else
                {
                   tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpRAScientificName}', '');
                }
                
                if(authorization.Expiration_Date__c !=null)
                {
                   tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpExpirationDate}', String.Valueof(authorization.Expiration_Date__c));
                }
                else
                {
                   tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpExpirationDate}', ' ');
                }
                
                //VV added 3-31-16 //Modified by Tharun-04/05/16
             wq = [select Id, signature__c from wizard_questionnaire__c where signature__c  = :tpid limit 1]; //vv
            if(wq != null && wq.size() != 0){
                List<wizard_selections__c> listWS=[select id, value__c from wizard_selections__c where Wizard_Next_Question__c = :wq[0].id limit 1];
                if(listWS!=null && listWS.size()>0)
                {
                if (wq[0].id != null ){
                 ws =listWS[0]; //vv
                 }
                if (ws.value__c != null){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!ProcessName}', ws.value__c );
                    system.debug('&&& WS = '+ws);
                  }    
                 }
             }   
            // VV added 3-31-16
                 }if(State_Territory_of_Destination  != null && State_Territory_of_Destination  != 'null' ){
                    tp.Communication_Manager__r.Content__c = tp.Communication_Manager__r.Content__c.replace('{!tpLevel1Region}', StateTerritoryofDestination.name);
                }
               
        }
        allAnswerTrace = new List<answerTrace>();
        showCriteria = false;
        getCriteria();
    }
    
    
    public void getCriteria()
    {
        Cookie criteria = ApexPages.currentPage().getCookies().get('criteria');
        System.Debug('<<<<<<< Cookie : ' + criteria + ' >>>>>>>');
        if(criteria == null && !test.isRunningTest()) {
            showCriteria = false;
        }
        else{
            Integer questionNo = 0;
            tempString = criteria.getValue();
            questionsTemp = tempString.substring(0,tempString.lastIndexOf('][') + 1);
            answersTemp = tempString.substring(tempString.lastIndexOf('][') + 1, tempString.length());
            
            while(questionsTemp.indexOf(']') > 0)
            {
                answerTrace ansTrace = new answerTrace();
                questionNo += 1; 
                ansTrace.questionNo = questionNo;
                ansTrace.question = questionsTemp.substring(1, questionsTemp.IndexOf(']'));
                ansTrace.answer = answersTemp.substring(1, answersTemp.IndexOf(']'));
                allAnswerTrace.add(ansTrace);
                System.Debug('<<<<<<< questionsTemp : ' + questionsTemp + ' >>>>>>>');
                System.Debug('<<<<<<< answersTemp : ' + answersTemp + ' >>>>>>>');
                if(questionsTemp.IndexOf(']_[') >= 0)
                {
                    questionsTemp = questionsTemp.substring(questionsTemp.IndexOf(']_[')+2, questionsTemp.length());
                    answersTemp = answersTemp.substring(answersTemp.IndexOf(']_[')+2, answersTemp.length());
                }else{break;}
            }
            
            showCriteria = true;
        }
        //return null;
    }
    
    public class answerTrace
    {
        public Integer questionNo {get;set;}
        public String question {get;set;}
        public String answer {get;set;}
        public String questionID {get;set;}
    }
     
    public PageReference saveAttachment()
    {
        CARPOL_AC_Processing process = new CARPOL_AC_Processing();
        process.attachLetter();
        return null;
    }
}