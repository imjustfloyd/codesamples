public class GetACLinetItems
{
    public Application__c app ;
    public List<AC__c> ac {get; set;}
    public GetACLinetItems(ApexPages.StandardController controller) {
        this.app = (Application__c)controller.getRecord();
    }
    public List<AC__c> getLineItems(){
        ac = [select ID, Name from AC__c where Application_Number__c = :app.id];
        return ac;
    }
}