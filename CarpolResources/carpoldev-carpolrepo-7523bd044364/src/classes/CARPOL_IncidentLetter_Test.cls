@isTest(seealldata=false)
private class CARPOL_IncidentLetter_Test {
      @IsTest static void CARPOL_IncidentLetter_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Application__c objapp = testData.newapplication();
          AC__c li = testData.newlineitem('Personal Use', objapp);
          Authorizations__c objauth = testData.newAuth(objapp.id);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
           String PortsFacRecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Building').getRecordTypeId();
         Facility__c fac = new Facility__c();
        fac.RecordTypeId = PortsFacRecordTypeId;
        fac.Name = 'entryport';
        fac.Type__c = 'Laboratory';  
        insert fac;
          
          
          //Incident__c
          Incident__c incident = new Incident__c();
          incident.Requester_Name__c='testReqName';
          incident.EFL_Template__c=objcm.id;
          incident.Facility__c =fac.id;
          incident.Authorization__c= objauth.id;
          incident.EFL_Issued_Date__c = Date.today();
          insert incident;
          //Create Inspection__c
          
          Inspection__c inspection = new Inspection__c();
          inspection.Inspector__c=objcont.Id;
          inspection.Facility__c=fac.id;
          inspection.Incident__c=incident.id;
          inspection.Scheduled_Date_of_Inspection__c = date.today();
          Insert inspection;
          
          Test.startTest(); 

          //run as salesforce user
              PageReference pageRef = Page.CARPOL_AC_ApplicationPDF;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(incident);
              ApexPages.currentPage().getParameters().put('id',incident.id);
              CARPOL_IncidentLetter extclass = new CARPOL_IncidentLetter(sc);
              System.assert(extclass != null);
                      
          Test.stopTest();   
      }
}