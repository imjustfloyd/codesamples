/*
// Author - Vinar Amrutia
// Date Created : 30th July 2015
// Purpose - AC Management Console Test Class
*/

@isTest(seealldata=false)
private class checkRecursive_Test {
    
    static testMethod void checkRecursive_Test() {
    checkRecursive mycheck = new checkRecursive();
    checkRecursive.runOnce();
    System.assert(mycheck != null);
    }
    
    static testMethod void checkRecursive1_Test() {
    checkRecursive mycheck = new checkRecursive();    
    checkRecursive.runOnce1();
    System.assert(mycheck != null);    
    }

}