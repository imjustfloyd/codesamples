global with sharing class CARPOL_PPQ_sendlabels{
    
     webservice static string brslabelspdf(id authid)
         {
           string strsuccess = '';
           try{
           // replaced applicant_email__c with AC_Applicant_Email__c  -- 6/11/2015 acarr
            Authorizations__c objauth = [select id,Name,BRS_Send_Labels__c,AC_Applicant_Email__c,Authorized_User__r.Name,Application__c from Authorizations__c where id=:authid];
            string appemail = objauth.AC_Applicant_Email__c;
            objauth.BRS_Send_Labels__c = true;
            update objauth;
            attachment attlab = new attachment();
            Blob attbody = null;
            PageReference PDFPage = new PageReference('/apex/CARPOL_Soil_PDF_Labels?id='+authid);  
           // attbody = PDFPage.getContent();
             if(!test.isRunningTest()){
                        attbody = PDFPage.getContent(); 
                        } 
                     else {
                            attbody = Blob.valueOf('Some Text');
                            } 
                attlab.Body = attbody;
                attlab.Name = 'CARPOL_PPQ_Labels.pdf';
               // attlab.contentType = 'pdf';
                attlab.ParentId = authid;
                
            insert attlab;
           Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
            String[] toaddress = new string[]{objauth.AC_Applicant_Email__c};
            email.setSubject('PPQ Labels');
            email.setToAddresses(toaddress);
             String appEmailBody = 'Dear ' + objauth.Authorized_User__r.Name;
              List<ConnectApi.Community> communities = ConnectApi.Communities.getCommunities().communities;
            
            String hostVal = '';
            String URLL;
            
            for(ConnectApi.Community community:communities){
                if(community.name == 'APHIS Customer Community')
                    
                    hostVal = community.siteUrl; 
            }
            
            String urlVal = '/portal_application_detail?id=';
           
            URLL =  hostVal + urlVal;
     
      appEmailBody += ',<br/><br/><br/>Below you will find a link for the shipping labels you have requested for Notification ' + authid;
       
              
             
      appEmailBody += '<br/> <a href="' +  URLL  +  objauth.Application__c+ '">Click Here</a> <br/><br/>';
        
      appEmailBody += 'Kindly refer to the Label Guidance listed under Permit Conditions. <br/><br/>';
          
      appEmailBody += 'Thank you,<br/>Plant Protection and Quarantine, Plant Health Programs<br/> USDA, Animal and Plant Health Inspection Service<br/> 4700 River Road <br/> Riverdale, MD 20737  <br/>United States Department of Agriculture<br/>Contact Number: 301-851-3740<br/>';
        
      email.setHtmlBody(appEmailBody);
           // email.setPlainTextBody( 'PPQ Labels' );
             Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
             efa.setFileName('CARPOL_PPQ_Labels.pdf');
             efa.setBody(attbody);
            // email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});

            // Sends the email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email }); 
           // strsuccess = 'success';
            //retrun strsuccess;
            return 'success';
            
            }
            catch(Exception e)
            {
                //strsuccess = e;
                return string.valueof(e);
            }
            return null;
            
         }

}