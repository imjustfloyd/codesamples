/**    
@Author: Vijay Vellaturi   
@name: CARPOL_BRSLinkConstructController_Test
@CreateDate: 9/23/15
@Description: Test class for CARPOL_BRSLinkConstructController
@Version: 1.0
@reference: CARPOL_BRSLinkConstructController(apex class)
*/

@isTest
public class CARPOL_BRSLinkConstructController_Test{

    public static testmethod void test(){
        CARPOL_BRSLinkConstructController ccempty = new CARPOL_BRSLinkConstructController();    
        ccempty.save();
        ccempty.cancel();        
        ApexPages.currentPage().getParameters().put('redirect','Yes');       
        CARPOL_BRSLinkConstructController cc = new CARPOL_BRSLinkConstructController();
        cc.save();
        cc.cancel();
        system.assert(cc != null);
    }

}