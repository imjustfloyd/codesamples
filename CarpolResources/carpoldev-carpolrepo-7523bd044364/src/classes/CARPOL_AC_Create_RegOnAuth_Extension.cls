public class CARPOL_AC_Create_RegOnAuth_Extension {

public Regulation__c reg { get; set; }
Public Authorization_Junction__c aj {get; set; }
public Authorizations__c au;
public ID RegRecordtypeId {get; set;}
public string RegRecordtypeName{get; set;}
public id wfid {get;set;}

public CARPOL_AC_Create_RegOnAuth_Extension(ApexPages.StandardController controller) {
    System.Debug('<<<<<<< Standard Controller Constructor Begins >>>>>>>');
reg = new Regulation__c();
aj = new Authorization_Junction__c();

this.au= (Authorizations__c)controller.getRecord();
wfid = ApexPages.currentPage().getParameters().get('wfid');

    if (au.Id!=null){
        au = [Select Id, RecordType.Name,Program__c, Application__c, Thumbprint__c FROM Authorizations__c Where Id =:au.Id];
        if(au.Program__c!=null && au.Program__c == 'AC'){
            RegRecordtypeName = 'Animal Care (AC)';
            RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
        }else if(au.Program__c!=null && au.Program__c == 'BRS'){
            RegRecordtypeName = 'Biotechnology Regulatory Services (BRS)';
            RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Biotechnology Regulatory Services (BRS)').getRecordTypeId();
        }else if(au.Program__c!=null && au.Program__c == 'PPQ'){
            RegRecordtypeName = 'Plant Protection & Quarantine (PPQ)';
            RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Plant Protection & Quarantine (PPQ)').getRecordTypeId();
        }else if(au.Program__c!=null && au.Program__c == 'VS'){
            RegRecordtypeName = 'Veterinary Services (VS))';
            RegRecordtypeId = Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Veterinary Services (VS)').getRecordTypeId();
        } 
    }
    System.Debug('<<<<<<< RegRecordtypeId >>>>>>>'+RegRecordtypeId);
}

public PageReference returnEditPermit() { 
    PageReference pageRef;// = Page.CARPOL_AC_EditPermit_Regulations2;
    if(au.Program__c!=null && au.Program__c == 'BRS'){
           pageRef = Page.CARPOL_BRS_EPRegulations;
      }else 
      { 
           pageRef = Page.CARPOL_AC_EditPermit_Regulations2; 
          
      }
    pageRef.getParameters().put('Id',au.Id);
    pageRef.getParameters().put('wfid',wfid);
    pageRef.setRedirect(true);
    return pageRef;     
    }
    
public PageReference createReg(){
    reg.RecordTypeId = RegRecordtypeId;//Schema.SObjectType.Regulation__c.getRecordTypeInfosByName().get('Animal Care (AC)').getRecordTypeId();
    reg.Status__c = 'Active';
    reg.Shared_Personal__c = 'Personal';
    insert reg;
    System.Debug('<<<<<<< reg.RecordTypeId >>>>>>>'+reg.RecordTypeId);
    aj.Authorization__c = au.Id;
    aj.Regulation__c = reg.Id;
    aj.RecordTypeId = Schema.SObjectType.Authorization_Junction__c.getRecordTypeInfosByName().get('Regulation Junction').getRecordTypeId();
    insert aj;
    PageReference pageRef;
    if(au.Program__c!=null && au.Program__c == 'BRS'){
           pageRef = Page.CARPOL_BRS_EPRegulations;
      }else 
      { 
           pageRef = Page.CARPOL_AC_EditPermit_Regulations2; 
          
      }
    pageRef.getParameters().put('Id',au.Id);
    pageRef.getParameters().put('wfid',wfid);
    pageRef.setRedirect(true);
    return pageRef;
}
}