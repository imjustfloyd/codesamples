@isTest(seealldata=false)
private class CARPOL_UNI_ExtWiz_DecisionMatrix_Test {
      @IsTest static void CARPOL_UNI_ExtWiz_DecisionMatrix_Test() {

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Regulation__c objreg1 = testData.newRegulation('Import Requirements','Import Permit Requirements');
          Regulation__c objreg2 = testData.newRegulation('Additional Information','Commercial Consignment Requirements');   
          Regulation__c objreg3 = testData.newRegulation('Instruction for CBP Officers','Pre-Clearance Requirements');  

          Test.startTest();
          PageReference pageRef = Page.CARPOL_UNI_Display_Conditions;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objreg1);
          String conditionlist = objreg1.id + '_' + objreg2.id + '_' + objreg3.id;
          ApexPages.currentPage().getParameters().put('conditionslist',conditionlist);
          CARPOL_UNI_ExtWiz_DecisionMatrix extclass = new CARPOL_UNI_ExtWiz_DecisionMatrix(sc);
          extclass.getrlist();
          system.assert(extclass != null);          
          Test.stopTest();     
      }
}