@isTest(seealldata=false)
private class CARPOL_UNI_AddContactOverride_Test {
      @IsTest static void CARPOL_UNI_AddContactOverride_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();


          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as portal user
          System.runAs(usershare) {
              PageReference pageRef = Page.CARPOL_UNI_AddContact;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcont);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('RecordType','Test');
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');                                                                      
              ApexPages.currentPage().getParameters().put('LineItemId','Test');                                                                                    
              
              CARPOL_UNI_AddContactOverride extclass = new CARPOL_UNI_AddContactOverride(sc);
              extclass.redirect();
              system.assert(extclass != null);              
          }  
          //run as salesforce user
              PageReference pageRef = Page.CARPOL_UNI_AddContact;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objcont);
              ApexPages.currentPage().getParameters().put('retURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('RecordType','Test');
              ApexPages.currentPage().getParameters().put('cancelURL','/apex/Test');
              ApexPages.currentPage().getParameters().put('ent','Test');
              ApexPages.currentPage().getParameters().put('_CONFIRMATIONTOKEN','0000000');
              ApexPages.currentPage().getParameters().put('save_new_url','/apex/Test');    
              ApexPages.currentPage().getParameters().put('LineItemId','Test');                                                                                                                                                      
              CARPOL_UNI_AddContactOverride extclassempty = new CARPOL_UNI_AddContactOverride();
              CARPOL_UNI_AddContactOverride extclass = new CARPOL_UNI_AddContactOverride(sc);
              extclass.redirect();  
              system.assert(extclass != null);                      
          Test.stopTest();   
      }
}