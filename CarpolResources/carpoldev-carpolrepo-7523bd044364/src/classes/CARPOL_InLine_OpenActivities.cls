public  class CARPOL_InLine_OpenActivities { 
  public Id authId {get;set;}
  public Authorizations__c objauth {get;set;}
   public Boolean refreshPage {get; set;}
   public static string inprogress = 'In Progress';
   public static string strcompleted = 'Completed';
    public CARPOL_InLine_OpenActivities(ApexPages.StandardController controller) {
       authId  =  ApexPages.currentPage().getParameters().get('Id');
       refreshPage=false;
    }
   
    public PageReference  Taskslist()
   {
       PageReference pageRef = new PageReference('https://aphis--carpoldev.cs32.my.salesforce.com/007?id='+ authId +'&rlid=RelatedActivityList&closed=0L&isdtp=lt');
       //return pageRef;
       objauth = [SELECT (SELECT id,ActivityDate,Comments__c,Subject,Status,Priority,ownerid FROM OpenActivities where status!=:strcompleted  order by Subject) FROM Authorizations__c where id=:authId];
       return null;


   }
   public PageReference newtask()
   {
       PageReference pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?ent=Task&retURL=%2F'+authId+'&save_new_url=%2F00T%2Fe%3Fwhat_id%3D'+authId+'%26retURL%3D%252F'+authId);
       return pageRef;
   }
   public PageReference newevent()
   {
       PageReference pageRef = new PageReference('/00U/e?what_id='+authId+'&retURL=%2F'+authId);
       return pageRef;
   }
 
}