public with sharing class CARPOL_PreviewAuthorization {

    public String templateURL { get; set; }
    public String letterType { get; set; }
    public String selectedTemplateType { get; set; }
    public String selectedTemplate { get; set; }
    public SelectOption[] allTemplateOptions { get; set; }
    public String reviewerComments { get; set; }
    public String baseURL { get; set; }
    public Authorizations__c authorization { get; set; }
    public Workflow_Task__c wtask { get; set; } // Added by Kishore
    public user wtaskowner { get; set; }
   //added by niharika  
    public boolean showAttachDraftBtn{get;set;}     
    public boolean showAttachBtn{get;set;}      
    public boolean showtemplateBtn{get;set;}
    public boolean showLinks{get;set;}
    
    public id lineitemid{get;set;}
    public list<Location__c> listlocations {get;set;}
    string olocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin Location').getRecordTypeId();
    string dlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Destination Location').getRecordTypeId();
    string oanddlocrectypeid = Schema.SObjectType.Location__c.getRecordTypeInfosByName().get('Origin and Destination Location').getRecordTypeId();  
    public string OriginLocation{get;set;}
    public string OriginandDestLocation{get;set;}
    public string DestinationLocation{get;set;}
    public string RegulatedArticle{get;set;}
    public string StateRegulatoryOfficialslist{get;set;}
    public string locationlist{get;set;}
    public list<Reviewer__c> listReviewRec {get;set;}
    public list<Link_Regulated_Articles__c> listRegArt {get; set;} 
    public id wfid {get;set;}

    public CARPOL_PreviewAuthorization(ApexPages.StandardController controller) {
        
        wfid = apexpages.currentpage().getparameters().get('tId');
        if(wfid == null)
          wfid = apexpages.currentpage().getparameters().get('wfid');
          
         //Added by Kishore
         if(wfid != null)
                wtask = [select Id,Comments__c,OwnerId,Owner.Name,Owner.Phone,Owner.Email,Program__c,Status__c,name from Workflow_Task__c where Id =:wfid LIMIT 1 ];
         system.debug('-------wtaskowner'+wtask.OwnerId);
        // wtaskowner=[select ID, Name, Phone,email from user where id=:wtask.OwnerId];
         System.debug('------'+wtask.Owner.Phone);
          System.debug('------'+wtask.Owner.Email);
        //Added by niharika         
         showAttachDraftBtn=false;      
         showAttachBtn=true;        
         showtemplateBtn=true;  
         showLinks=true;
       /*  if(wtask.Program__c == 'BRS' && (wtask.Status__c == 'Not Started' || wtask.Status__c == 'Denied' ||wtask.Status__c == 'Pending')  ){       
            showAttachDraftBtn = true;      
            showAttachBtn=false;        
         }      
         if(wtask.Program__c == 'BRS' && wtask.Status__c == 'In Progress'  ){       
                showtemplateBtn=false;
                //showAttachDraftBtn = true;
         }*/ // Commented to reiterate the code by KK Aug 2012 
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        this.authorization = (Authorizations__c)controller.getRecord();
        authorization = [SELECT ID, Name, Authorization_Type__c, AC_Applicant_Name__c, Application__r.Name, Authorized_User__r.FirstName, Authorized_User__r.LastName, AC_Applicant_Address__c, Template__c, 
                                Date_Issued__c,Applicant_Reference_Number__c,Bio_Tech_Reviewer__r.Name,Bio_Tech_Reviewer__c,Bio_Tech_Reviewer__r.Phone,Bio_Tech_Reviewer__r.Email, Thumbprint__c,BRS_Introduction_Type__c,       
                                Thumbprint__r.Regulated_Article__r.name,Thumbprint__r.From_Country__r.name,Thumbprint__r.REF_Program_Name__c,Thumbprint__r.Line_Record_Type__c,Thumbprint__r.Regulated_Article__r.Scientific_Name__c FROM Authorizations__c WHERE ID = :apexpages.currentpage().getparameters().get('id') LIMIT 1];
        System.Debug('<<<<<<< Authorization : ' + authorization + ' >>>>>>>');
        if(authorization == null){ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a related Authorization.'));}
        else
        {
            authorization.Date_Issued__c = System.Today();
            System.Debug('<<<<<<< Authorization_Type__c : ' + authorization.Authorization_Type__c + ' >>>>>>>');
            letterType = authorization.Authorization_Type__c;
            templateURL = 'apex/CARPOL_AuthorizationLetter?id=' + authorization.ID + '&tId='+ wtask.Id;
        }
        //selectedTemplateType = 'Letter of Denial';
        //System.Debug('<<<<<<< Template Type : ' + selectedTemplateType + ' >>>>>>>');
        //getTemplates();
      //----------------------Start BRS Specific Functionality------------------------------// 
        OriginLocation = '';
        OriginandDestLocation = '';
        DestinationLocation = '';
        RegulatedArticle = '';
        StateRegulatoryOfficialslist= '';
        locationlist= '';
        lineitemid = [SELECT Id FROM AC__c WHERE Authorization__c = :authorization.Id LIMIT 1].Id;
        system.debug('lineitemid@@@'+lineitemid);

                        //--location Information--//
                        listlocations = [select ID,Name,City__c,Country__c,Country__r.Name,County__c, State__c, Status__c,State__r.Name,recordtypeid from Location__c 
                        where  (RecordType.DeveloperName='Origin_and_Destination' or RecordType.DeveloperName='Origin_Location' or RecordType.DeveloperName='Destination_Location')
                        and Line_Item__c =:lineitemid]; 
                        
                       //--Regulated Articles--//
                       listRegArt = [SELECT Id, Name, Regulated_Article__r.Name FROM Link_Regulated_Articles__c WHERE Line_Item__c = :lineitemid];   
                       
                        //--Official Review Records--//
                        listReviewRec = [SELECT Id, Name,Notes_to_State__c,BRS_State_Reviewer_Email__c,BRS_State_Reviewer_Notification__c,Requirement_Desc__c,State_Comments__c,
                                        State_not_Responded__c,State_has_comments__c,No_Requirements__c,State_Regulatory_Official__r.FirstName,Review_Complete__c,Reviewer__c,
                                        Status__c,Reviewer_Unique_Id__c,State__c FROM Reviewer__c WHERE Authorization__c =:authorization.Id ];
                        
                    if(listlocations.size()>0){
                        
                        for(Location__c lc: listlocations){

                             if(lc.recordtypeid == olocrectypeid){ OriginLocation += lc.Country__r.name+'; ';  }
                                
                             if(lc.recordtypeid == oanddlocrectypeId){ OriginandDestLocation += lc.State__r.name+'; ';  }
                                 
                             if(lc.recordtypeid == dlocrectypeId){ DestinationLocation += lc.State__r.name+'; ';  }

                        }
                       
                         if(OriginLocation!='')
                            locationlist += 'Origin Location - '+OriginLocation+'<br/>';
                         if(OriginandDestLocation!='')
                            locationlist += 'Origin and Destination Location - '+OriginandDestLocation+'<br/>';
                         if(DestinationLocation!='')
                            locationlist += 'Destination Location - '+DestinationLocation+'<br/>';                            

                    }  
                    
                     if(listRegArt.size()>0){ 
                         
                          for(Link_Regulated_Articles__c lRA: listRegArt){ 
                              
                              RegulatedArticle += lRA.Regulated_Article__r.Name+'; ';
                          }
                         
                     }
                     
                     if(listReviewRec.size()>0){ 
                         
                          for(Reviewer__c sor: listReviewRec){ 
                              
                              StateRegulatoryOfficialslist += sor.State_Regulatory_Official__r.FirstName+','+sor.State__c+'; ';
                          }
                         
                     }                     
      
       //----------------------End BRS Specific Functionality------------------------------// 
        
     }
    
    /*public PageReference getTemplates()
    {
        if(selectedTemplateType != null)
        {
            System.Debug('<<<<<<< Getting all the templates and generating preview >>>>>>>');
            allTemplateOptions = new List<SelectOption>();
            List<Communication_Manager__c> tempTemplates = [SELECT ID, Name, Template_Name__c FROM Communication_Manager__c WHERE Type__c = :selectedTemplateType];
            for (Communication_Manager__c template : tempTemplates ) 
            {allTemplateOptions.add(new SelectOption(template.ID, template.Template_Name__c));}
            
        }
        return null;
    }*/
    
    public PageReference populateTemplate()
    {
        System.Debug('<<<<<<< Template : ' + authorization.Template__c + ' >>>>>>>');
        try
        {
            if(authorization.Template__c != null)
            {
                authorization.Authorization_Letter_Content__c = [SELECT ID, Name, Content__c FROM Communication_Manager__c WHERE ID =:authorization.Template__c LIMIT 1].Content__c;
                
                //Replacing Variables
                authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!ApplicantFirstName} {!ApplicantLastName}', authorization.Authorized_User__r.FirstName + ' ' + authorization.Authorized_User__r.LastName);
                authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!ApplicantAddress}', authorization.AC_Applicant_Address__c);
                authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!ApplicationNumber}', authorization.Application__r.Name);
                authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!IssuedDate}', String.ValueOf(authorization.Date_Issued__c.format()));
                // Start Kishore Changes 
                 if(authorization.Name != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!AuthorizationNotificationNumber}',authorization.Name);
                 if(authorization.Applicant_Reference_Number__c !=null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!Applicant_Reference_Number}',authorization.Applicant_Reference_Number__c);
                 if(wtask.Comments__c != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskComments}',wtask.Comments__c);
                                    
                 if(wtask.Owner.Name != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwner}',wtask.Owner.Name);
                 if(wtask.Owner.Phone != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerPhone}',wtask.Owner.Phone);
                 if(wtask.Owner.Email != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerEmail}',wtask.Owner.Email); 

                 if(authorization.Thumbprint__r.Regulated_Article__r.name != null){
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpRegulatedArticle}',authorization.Thumbprint__r.Regulated_Article__r.name); 
                 }else{
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpRegulatedArticle}',''); 
                 }     
                 
                 if(authorization.Thumbprint__r.From_Country__r.name != null){
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpFromCountry}',authorization.Thumbprint__r.From_Country__r.name); 
                 }else{
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpFromCountry}',''); 
                 } 
                 
                 if(authorization.Thumbprint__r.REF_Program_Name__c != null){
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpProgramName}',authorization.Thumbprint__r.REF_Program_Name__c); 
                 }else{
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpProgramName}',''); 
                 }   
                 
                 
                 if(authorization.Thumbprint__r.Line_Record_Type__c != null){
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpLineRecordType}',authorization.Thumbprint__r.Line_Record_Type__c); 
                 }else{
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpLineRecordType}',''); 
                 }       
                 
                 if(authorization.Thumbprint__r.Regulated_Article__r.Scientific_Name__c != null){
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpRAScientificName}',authorization.Thumbprint__r.Regulated_Article__r.Scientific_Name__c); 
                 }else{
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!tpRAScientificName}',''); 
                 } 
                  
                if(authorization.BRS_Introduction_Type__c != null){
                    authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!Movementtype}',authorization.BRS_Introduction_Type__c);
                }else{
                   authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!Movementtype}',''); 
                }
                    
                if(locationlist != null){
                  authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!locationlist}',locationlist);  
                }else{
                    authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!locationlist}','');
                }
                   
                 
                if(RegulatedArticle != null){
                   authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!RegulatedArticle}',RegulatedArticle);   
                }else{
                   authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!RegulatedArticle}','');    
                }
                 
                 
                if(StateRegulatoryOfficialslist!= null){
                   authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!StateRegulatoryOfficialslist}',StateRegulatoryOfficialslist);  
                }else{
                     authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!StateRegulatoryOfficialslist}','');
                }
                
                
                   /*Kishore Start Added 10/26/2015  
                 if(authorization.Bio_Tech_Reviewer__c != null) {
                 if(authorization.Bio_Tech_Reviewer__r.Name != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwner}',authorization.Bio_Tech_Reviewer__r.Name);
                 if(authorization.Bio_Tech_Reviewer__r.Phone != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerPhone}',authorization.Bio_Tech_Reviewer__r.Phone);
                 if(authorization.Bio_Tech_Reviewer__r.Email != null)
                 authorization.Authorization_Letter_Content__c = authorization.Authorization_Letter_Content__c.replace('{!WorkflowTaskOwnerEmail}',authorization.Bio_Tech_Reviewer__r.Email);                 
                 } 
                  Kishore Start Ended 10/26/2015 */ 
                // End Kishore Changes
                update authorization;
                previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Template populated successfully.'));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a template to be populated.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a valid template to be populated.'));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference cancel()
    {
        System.Debug('<<<<<<< Cancel >>>>>>>');
        PageReference return2Auth = new PageReference('/' + authorization.ID);
        return2Auth.setRedirect(true);
        return return2Auth;
    }

    public PageReference attachLetter()
    {
        try{
            System.Debug('<<<<<<< Authorization : ' + authorization + ' >>>>>>>');
            String attachmentName;
            PageReference pdfAuthorizationLetter = Page.CARPOL_AuthorizationLetter;
            attachmentName = 'Authorization Letter_' + System.TODAY().format();
            
            if(letterType == 'Notification' && letterType != ''){
                attachmentName = 'Acknowledgement Letter_' + System.TODAY().format();
             }
            /*
            if(selectedTemplateType == 'Letter of Denial')
            {
                pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogs_LOD_Formal;
                attachmentName = 'LetterOfDenial';
            }
            if(selectedTemplateType == 'Letter of No Jurisdiction')
            {
                pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogs_NJLetter_Formal;
                attachmentName = 'LetterOfNoJurisdiction';
            }
            if(selectedTemplateType == 'Permit Letter')
            {
                pdfACAuthorizationLetter = Page.CARPOL_AC_LiveDogsPermit;
                attachmentName = 'Permit';
            }
            
            */
            
            pdfAuthorizationLetter.getParameters().put('id', authorization.ID);
            pdfAuthorizationLetter.getParameters().put('tId', wtask.Id);
            Blob pdfAuthorizationLetterBlob;
            
           if (Test.IsRunningTest())
           {
            pdfAuthorizationLetterBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfAuthorizationLetter + ' >>>>>>');
            pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent();
            
            }
            
            
            String tempAttachmentName = attachmentName.substring(0, attachmentName.length()) + '%';
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :authorization.ID AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];
            if(prevAttachments.size() > 0)
            {
                if(prevAttachments.size() == 1){attachmentName = attachmentName + '_v2';}
                else
                {
                    for(Integer i = 0; i < prevAttachments.size(); i++ )
                    {
                        if(i == prevAttachments.size() - 1)
                        {
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');
                            attachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;
                            System.Debug('<<<<<<< New Attachment Name : ' + attachmentName + ' >>>>>>');
                        }
                    }
                }
            }
            
            attachmentName = attachmentName + '.pdf';
            Attachment attachment = new Attachment(parentId = authorization.ID, name = attachmentName, body = pdfAuthorizationLetterBlob);
            insert attachment;
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');
            if(letterType != 'Notification' && letterType != ''){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Authorization Letter attached successfully.'));
            }else{
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Acknowledgement Letter attached successfully.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    //niharika      
    public PageReference attachDraftLetter()        
    {       
        showLinks=true;
        try{        
            System.Debug('<<<<<<< Authorization : ' + authorization + ' >>>>>>>');      
            String draftattachmentName;     
            PageReference pdfAuthorizationLetter = Page.CARPOL_AuthorizationLetter;     
            draftattachmentName = 'Draft Authorization Letter_' + System.TODAY().format();      
                    
                    
            pdfAuthorizationLetter.getParameters().put('id', authorization.ID);     
            pdfAuthorizationLetter.getParameters().put('tId', wtask.Id);        
            Blob pdfAuthorizationLetterBlob;        
                    
          //  pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent(); 
           if (Test.IsRunningTest())
           {
            pdfAuthorizationLetterBlob =Blob.valueOf('UNIT.TEST');
           }
           else
           {
            System.Debug('<<<<<<< Page about to be loaded : ' + pdfAuthorizationLetter + ' >>>>>>');
            pdfAuthorizationLetterBlob = pdfAuthorizationLetter.getContent();
            
            }            
                    
            String tempAttachmentName = draftattachmentName.substring(0, draftattachmentName.length()) + '%';       
            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
            List<Attachment> prevAttachments = [SELECT Id, Name FROM Attachment WHERE ParentID = :wtask.Id AND Name LIKE :tempAttachmentName ORDER BY CreatedDate];     
            if(prevAttachments.size() > 0)      
            {       
                if(prevAttachments.size() == 1){draftattachmentName = draftattachmentName + '_v2';}     
                else        
                {       
                    for(Integer i = 0; i < prevAttachments.size(); i++ )        
                    {       
                        if(i == prevAttachments.size() - 1)     
                        {       
                            tempAttachmentName = String.ValueOf(prevAttachments[i].Name);       
                            System.Debug('<<<<<<< Temp Attachment Name : ' + tempAttachmentName + ' >>>>>>');       
                            tempAttachmentName = tempAttachmentName.substring(0, tempAttachmentName.length() - 4);      
                            Integer versionNumber = Integer.ValueOf(tempAttachmentName.substring(tempAttachmentName.indexOf('_v') + 2, tempAttachmentName.length())) + 1;       
                            System.Debug('<<<<<<< Version Number : ' + versionNumber + ' >>>>>>');      
                            draftattachmentName = tempAttachmentName.substring(0, tempAttachmentName.indexOf('_v') + 2) + versionNumber;        
                            System.Debug('<<<<<<< New Attachment Name : ' + draftattachmentName + ' >>>>>>');       
                        }       
                    }       
                }       
            }       
                    
            draftattachmentName = draftattachmentName + '.pdf';     
            Attachment attachment = new Attachment(parentId = wtask.Id, name = draftattachmentName, body = pdfAuthorizationLetterBlob);     
            insert attachment;      
            System.Debug('<<<<<<< Atttachment Inserted : ' + attachment.ID + ' >>>>>>');        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Authorization Draft Letter attached successfully.'));
        }       
        catch(Exception e)      
        {       
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');      
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));     
        }       
        return null;        
    }
    
    public PageReference saveDraft()
    {
        try{
            if(authorization.Date_Issued__c < System.Today())
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Date Issued cannot be in the past.'));
            }
            else
            {
                update authorization;
                //previewTemplate();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Draft saved successfully.'));
            }
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }
    
    public PageReference previewTemplate()
    {
        try{
            System.Debug('<<<<<<< Generating Preview !! >>>>>>>');
            system.debug('<<< Task Id >>>'+wtask.Id);
            templateURL = '/apex/CARPOL_AuthorizationLetter?id=' + authorization.ID + '&tId='+ wtask.Id ;
            
            /*
            if(selectedTemplate != '--None--')
            {
                update authorization;
                templateURL = 'apex/CARPOL_AuthorizationLetter?id=' + authorization.ID;
                if(selectedTemplateType == 'Letter of Denial')
                {
                    templateURL = '/apex/CARPOL_AC_LiveDogs_LOD_Formal?id=' + authorization.ID;
                }
                if(selectedTemplateType == 'Letter of No Jurisdiction')
                {
                    templateURL = 'apex/CARPOL_AC_LiveDogs_NJLetter_Formal?id=' + authorization.ID;
                }
                if(selectedTemplateType == 'Permit Letter')
                {
                    templateURL = '/apex/CARPOL_AC_LiveDogsPermit';
                }
            }*/
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.' + e));
        }
        return null;
    }    
}