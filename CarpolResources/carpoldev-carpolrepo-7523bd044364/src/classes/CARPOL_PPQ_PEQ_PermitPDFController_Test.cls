@isTest()
public class CARPOL_PPQ_PEQ_PermitPDFController_Test {
      @IsTest static void CARPOL_PPQ_PEQ_PermitPDFController_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Regulations_Association_Matrix__c objdm = new Regulations_Association_Matrix__c();
          objdm.Program_Line_Item_Pathway__c = plip.id;
          insert objdm;
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Final_Decision_Matrix_Id__c = objdm.id;
          objauth.Date_Issued__c = Date.today();          
          objauth.Program_Pathway__c = plip.id;
          update objauth;
          Facility__c  entry = testData.newfacility('Domestic Port');
          Authorization_Junction__c objauthjun = new Authorization_Junction__c();
          objauthjun.Authorization__c = objauth.id;
          objauthjun.Port__c = entry.id;
          insert objauthjun;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_PPQ_PEQ_PermitPDF;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              //ApexPages.currentPage().getParameters().put('strPathwayId',plip.id);
              //ApexPages.currentPage().getParameters().put('LineId',li.id);                                                                                                                                                      
              //ApexPages.currentPage().getParameters().put('genid',objcaj.id);                                   
              //ApexPages.currentPage().getParameters().put('retPage','Test');                                                                                                                                                                                  
              CARPOL_PPQ_PEQ_PermitPDFController extclassempty = new CARPOL_PPQ_PEQ_PermitPDFController();              
              CARPOL_PPQ_PEQ_PermitPDFController extclass = new CARPOL_PPQ_PEQ_PermitPDFController(sc);
              extclass.getACRecords();
              extclass.getPDFFields();
              extclass.getConditions();
              //extclass.saveAttachment();
              //extclass.getTable();
              CARPOL_PPQ_PEQ_PermitPDFController.getFieldValue(objacct,'Name');
              List<ID> lstAuth = new List<ID>();
              lstAuth.add(objauth.id);
              //CARPOL_PPQ_PermitPDFController.saveAutoAttachment(lstAuth);
              system.assert(extclass != null);

          Test.stopTest();    

      }

}