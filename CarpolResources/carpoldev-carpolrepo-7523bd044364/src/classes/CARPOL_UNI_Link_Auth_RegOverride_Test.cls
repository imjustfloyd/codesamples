/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CARPOL_UNI_Link_Auth_RegOverride_Test {

    static testMethod void test_CARPOL_UNI_Link_Auth_RegOverride() {
        String ACAppRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Standard Application').getRecordTypeId();  
	          CARPOL_AC_TestDataManager dataObj = new CARPOL_AC_TestDataManager ();
	          Application__c  newApp =dataObj.newapplication();
	          AC__c  newLine = dataObj.newLineItem('Personal Use', newApp);
	          Contact objCont = dataObj.newContact();
	          Contact objCont1 = dataObj.newContact();
	          	          
	          Authorizations__c objAuth = dataObj.newAuth(newApp.Id);
	          Link_Authorization_Regulation__c objLAR = new Link_Authorization_Regulation__c();
	          objLAR.Authorization__c = objAuth.Id;
	          objLAR.Introduction_Type__c = 'Importation';
	            
	          User usershare = new User();
	          usershare.Username ='aphistestemail@test.com';
	          usershare.LastName = 'APHISTestLastName';
	          usershare.Email = 'APHISTestEmail@test.com';
	          usershare.alias = 'APHItest';
	          usershare.TimeZoneSidKey = 'America/New_York';
	          usershare.LocaleSidKey = 'en_US';
	          usershare.EmailEncodingKey = 'ISO-8859-1';
	          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
	          usershare.LanguageLocaleKey = 'en_US';
	          usershare.ContactId = objCont.id;
	          insert usershare; 
	          
	          User u = new User();
	          u.Username ='aphistestemail1@test.com';
	          u.LastName = 'APHISTestLastName11';
	          u.Email = 'APHISTestEmail11@test.com';
	          u.alias = 'APHI111';
	          u.TimeZoneSidKey = 'America/New_York';
	          u.LocaleSidKey = 'en_US';
	          u.EmailEncodingKey = 'ISO-8859-1';
	          u.ProfileId = [select id from Profile where Name='Customer Community Login User'].Id;
	          u.LanguageLocaleKey = 'en_US';
	          u.ContactId = objCont1.id;
	          insert u; 
	            
	          system.runas(usershare)
	          {  
			      test.startTest();
			      Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_ViewLinkAuthRegulation'));
			      System.currentPageReference().getParameters().put('Id',objLAR.Id); 
			      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objLAR);
			      CARPOL_UNI_Link_Auth_RegulationOverride obj = new CARPOL_UNI_Link_Auth_RegulationOverride(sc);
			      obj.redirect();			        
			      system.assert(obj != null); 
	          }
	          
	          system.runas(u)
	          {  
			      Test.setCurrentPageReference(new PageReference('Page.CARPOL_UNI_ViewLinkAuthRegulation'));
			      System.currentPageReference().getParameters().put('Id',objLAR.Id); 
			      ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objLAR);
			      CARPOL_UNI_Link_Auth_RegulationOverride dummyObj = new CARPOL_UNI_Link_Auth_RegulationOverride();			      
			      CARPOL_UNI_Link_Auth_RegulationOverride obj = new CARPOL_UNI_Link_Auth_RegulationOverride(sc);
			      obj.redirect();			        
			      system.assert(obj != null); 
	          }               
			  test.stopTest();
			  
	      }    
    
}