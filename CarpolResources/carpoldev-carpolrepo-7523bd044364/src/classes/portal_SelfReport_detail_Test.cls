@isTest(seealldata=false)
private class portal_SelfReport_detail_Test {
      @IsTest static void portal_SelfReport_detail_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          AC__c li = TestData.newlineitem('Personal Use', null);
          Construct__c objcaj = new Construct__c();
          objcaj.Construct_s__c = 'Test';
          objcaj.Line_Item__c = li.id;
          insert objcaj;
          Self_Reporting__c sr= new Self_Reporting__c();
          sr.Line_Events__c = objcaj.id;
          sr.Monitoring_Period_Start__c = date.today();
          sr.Monitoring_Period_End__c = date.today() + 60;
          sr.Observation_Date__c = date.today() + 61;
          sr.Number_of_Volunteers__c = 5;
          sr.Action_Taken__c = 'Test description';
          insert sr;

          User usershare = new User();
          usershare.Username ='aphistestemail@test.com';
          usershare.LastName = 'APHISTestLastName';
          usershare.Email = 'APHISTestEmail@test.com';
          usershare.alias = 'APHItest';
          usershare.TimeZoneSidKey = 'America/New_York';
          usershare.LocaleSidKey = 'en_US';
          usershare.EmailEncodingKey = 'ISO-8859-1';
          usershare.ProfileId = [select id from Profile where Name='APHIS Applicant'].Id;
          usershare.LanguageLocaleKey = 'en_US';
          usershare.ContactId = objcont.id;
          insert usershare;    
          
          Test.startTest(); 
          //run as salesforce user
          PageReference pageRef = Page.Portal_SelfReport_detail;
          Test.setCurrentPage(pageRef);
          ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(sr);
          ApexPages.currentPage().getParameters().put('id',sr.id);

          portal_SelfReport_detail extclass = new portal_SelfReport_detail(sc);
          extclass.getattach();
          System.assert(extclass != null);        
          Test.stopTest();   
      }
}