public with sharing class CARPOL_VS_SelectPaymentType {
    
    public String paymentType{get;set;}
    public String APHISUserFeeAccountNumber {get;set;}
    public Boolean showUserFeeAccountNumber { get; set; }
    public Boolean showMailInCheck {get;set;}
    public Boolean showMoneyOrder {get;set;}
    public Application__c application { get; set; }
    
    public CARPOL_VS_SelectPaymentType(ApexPages.StandardController controller) {
    
        ID applicationID = ApexPages.CurrentPage().getparameters().get('id');
        showUserFeeAccountNumber = false;
        showMailInCheck = false;
        showMoneyOrder = false;
        if(applicationID != null)
        {
            application = new Application__c();
            Application = [SELECT Id, Payment_Type__c, APHIS_User_Fee_Account_Number__c, Application_Fee_Amount__c FROM Application__c WHERE Id = :applicationID LIMIT 1];
            paymentType = Application.Payment_Type__c;
            paymentTypeChangeAction();
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No valid Application ID found.'));
        }
    }
    
    public List<SelectOption> getPaymentTypeOptions() {
    
        List<SelectOption> paymentTypes = new List<SelectOption>();
        paymentTypes.add(new SelectOption('--Select--', '--Select--'));
        paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
        paymentTypes.add(new SelectOption('Mail-in Check', 'Mail-in Check'));
        paymentTypes.add(new SelectOption('Money Order', 'Money Order'));
        paymentTypes.add(new SelectOption('APHIS User Account', 'APHIS User Account'));
        
        Profile userProf = [SELECT ID, Name FROM Profile WHERE ID = :UserInfo.getProfileId() LIMIT 1];
        if(userProf.Name != 'APHIS Applicant')
        {
            paymentTypes.add(new SelectOption('Charge No Fee', 'Charge No Fee'));
            paymentTypes.add(new SelectOption('Applicant Did Not Provide Payment', 'Applicant Did Not Provide Payment'));
        }
        return paymentTypes;
    }
    
    public PageReference paymentTypeChangeAction()
    {
        if(paymentType == 'APHIS User Account')
        {
            showUserFeeAccountNumber = true;
        }
        else if(paymentType != 'APHIS User Account')
        {
            showUserFeeAccountNumber = false;
        }
            
        if(paymentType == 'Mail-in Check')
        {
            showMailInCheck = true;
        }
        else if (paymentType != 'Mail-in Check')
        {
            showMailInCheck = false;
        }
        if(paymentType == 'Money Order')
        {
            showMoneyOrder= true;
        }
        else if (paymentType != 'Money Order')
        {
            showMoneyOrder = false;
        }
        
        return null;
    }
    public PageReference savePaymentType()
    {
        try
        {             
            if(paymentType != '--Select--')
            {
                application.Payment_Type__c = paymentType;
                System.Debug('<<<<<<< APHIS_User_Fee_Account_Number__c : ' + application.APHIS_User_Fee_Account_Number__c + ' >>>>>>>');
                update(application);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Payement Type Saved Successfuly.'));
                
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Payment Type.'));
            }
        } 
        catch(System.DMLException e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
        }

    return null;
    }

}