@isTest(seealldata=true)
public class CARPOL_PPQ_Approval_Test {
      @IsTest static void CARPOL_PPQ_Approval_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_PPQ_TestDataManager testData = new CARPOL_PPQ_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();          
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Program_Prefix__c objPrefix = testData.newPrefix();
          objPrefix.Permit_PDF_Template__c = 'CARPOL_PPQ_PEQ_PermitPDF';
          update objPrefix;
          Signature__c objSig = testData.newThumbprint();
          objSig.Program_Prefix__c = objPrefix.Id;
          update objSig;
          Authorizations__c objauth = testData.newAuth(objapp.id);
          objauth.status__c = 'Issued';
          objauth.Thumbprint__c = objSig.Id;
          update objauth;
          li.Authorization__C = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_Internal_DenialPdfattach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CARPOL_PPQ_Approval extclass = new CARPOL_PPQ_Approval();
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Permit');              
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of Denial');
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of No Permit Required');
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of No Jurisdiction');
              
              objauth.status__c = 'Approved';
              update objauth;
              
              ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(objapp);
              ApexPages.StandardSetController ssc1 = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CARPOL_PPQ_Approval extclass1 = new CARPOL_PPQ_Approval();
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Permit');              
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of Denial');
              
                                         
              system.assert(extclass != null);
          Test.stopTest();    

      }
      @IsTest static void CARPOL_PPQ_Approval_CITES_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_PPQ_TestDataManager testData = new CARPOL_PPQ_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          plip.Permit_PDF_Template__c = 'CARPOL_PPQ_PermitPDF';
          update plip;
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__C = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.CITES_Needed__c = false;
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Issue Protected Plant Permit', objauth, 'Complete');
          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_Internal_DenialPdfattach;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objapp);
              ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(new List<Application__c>());
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              CARPOL_PPQ_Approval extclass = new CARPOL_PPQ_Approval();
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Permit');              
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of Denial');
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of No Permit Required');
              CARPOL_PPQ_Approval.sendEmail(objapp.id,objauth.id,'Letter of No Jurisdiction');                            
              system.assert(extclass != null);
          Test.stopTest();    

      }      
   

}