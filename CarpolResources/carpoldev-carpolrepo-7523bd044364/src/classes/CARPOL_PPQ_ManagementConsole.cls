// Author : Vinar Amrutia
// Date Created : 01/05/2015
// Purpose : Manageemnt Console and Grouping by Groups
public with sharing class CARPOL_PPQ_ManagementConsole {

    public PageReference Save() {
        return null;
    }


    public Signature__c signature { get; set; }    
    public List<Signature__c> allSignatures { get; set; }
    public List<Signature_Regulation_Junction__c> allConditionSignatureJunctions { get; set; }
    public List<Regulation__c> allRegulations { get; set; }
    public List<Regulation__c> allImportRequirements { get; set; }
    public List<Regulation__c> allInstructions { get; set; }
    public List<Regulation__c> allAdditionalInformation { get; set; }
    public List<Regulated_Article__c> allRegulatedArticles { get; set; }
    public List<Country__c> allCountries { get; set; }
    
    
    public CARPOL_PPQ_ManagementConsole()
    {
        System.Debug('<<<<<<< Constructor Begins >>>>>>>');
        signature = new Signature__c();
        
        allSignatures = new List<Signature__c>();
        allRegulatedArticles = new List<Regulated_Article__c>();
        allCountries = new List<Country__c>();
        
        allImportRequirements = new List<Regulation__c>();
        allInstructions = new List<Regulation__c>();
        allAdditionalInformation = new List<Regulation__c>();
    }
    
    public PageReference getResults() {
        
        try
        {
            allSignatures.clear();
            allRegulatedArticles.clear();
            allCountries.clear();
            allImportRequirements.clear();
            allInstructions.clear();
            allAdditionalInformation.clear();
            
            System.Debug('<<<<<<< Getting results >>>>>>>');
            
            ID regulatedArticleID = signature.Regulated_Article__c;
            ID countryID = signature.From_Country__c;
            ID groupID = signature.Group__c;
            
            System.Debug('<<<<<<< Selected Country ID is : ' + countryID + ' >>>>>>>');
            System.Debug('<<<<<<< Selected Regualted Article ID is : ' + regulatedArticleID + ' >>>>>>>');
            System.Debug('<<<<<<< Selected Group ID is : ' + groupID + ' >>>>>>>');
            
            String strWhereClause = '';
            Boolean flagWhereClause = false;
            
            if(countryID == null && regulatedArticleID == null && groupID == null)
            {
                strWhereClause = '';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Regulated Article, Country or a Group to search.'));
            }
            else
            {
                if(regulatedArticleID != null){
                    strWhereClause = ' WHERE Regulated_Article__c = \'' + regulatedArticleID + '\'';
                    flagWhereClause = true;
                }
                
                if(countryID != null){
                    if(flagWhereClause == true){
                        strWhereClause += 'AND From_Country__c = \'' + countryID + '\'';
                    }
                    else{
                        strWhereClause += ' WHERE From_Country__c = \'' + countryID + '\'';
                        flagWhereClause = true;
                    }
                }
                
                if(groupID != null)
                {
                    if(flagWhereClause == true){
                        strWhereClause += ' AND Group__c = \'' + groupID + '\'';
                    }
                    else{
                        strWhereClause += ' WHERE Group__c = \'' + groupID + '\'';
                        flagWhereClause = true;
                    }
                }
                
                
                System.Debug('<<<<<<< WHERE Clause : ' + strWhereClause + ' >>>>>>>');
                
                allSignatures = Database.query('SELECT ID, Name, Status__c, IsActive__c, Selected_Import_Requirements__c, Selected_Instruction_for_CBP_Officers__c FROM Signature__c ' + strWhereClause);
                
                if(allSignatures.size() == 0)
                {
                    System.Debug('<<<<<<< Signature List Size : ' + allSignatures.size() + ' >>>>>>>');
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Your search fetched no results. Please re-try  with different filter combinations.'));
                    return null;   
                }
                
                allCountries = Database.query('SELECT ID, Name, Country_Flag__c, Country_Code__c, Country_Status__c, IsActive__c FROM Country__c WHERE ID IN (SELECT From_Country__c FROM Signature__c ' + strWhereClause + ')');
                
                allRegulatedArticles = Database.query('SELECT ID, Name, Common_Name__c, Scientific_Name__c, Plant_Part__r.Name, IsActive__c FROM Regulated_Article__c WHERE ID IN (SELECT Regulated_Article__C FROM Signature__c ' + strWhereClause + ')');
                
                allConditionSignatureJunctions = Database.query('SELECT ID, Name, Regulation__c FROM Signature_Regulation_Junction__c WHERE Signature__c IN (SELECT ID FROM Signature__c ' + strWhereClause + ')');
                
                String strConditionSignatureJunctionQuery = '(';
                if(allConditionSignatureJunctions.size() > 0)
                {
                    
                    for(Integer i = 0; i < allConditionSignatureJunctions.size() ;i++)
                    {
                        strConditionSignatureJunctionQuery += '\'' + String.valueOf(allConditionSignatureJunctions[i].Regulation__c) + '\', ';
                        if(i == allConditionSignatureJunctions.size() - 1)
                        {
                            strConditionSignatureJunctionQuery = strConditionSignatureJunctionQuery.substring(0,strConditionSignatureJunctionQuery.length() - 2) + ')';
                        }
                    }
                }
                
                System.Debug('<<<<<<< Signature Condition Junction Ids : ' + strConditionSignatureJunctionQuery + ' >>>>>>>');
                if(strConditionSignatureJunctionQuery.length() > 1)
                {
                    strConditionSignatureJunctionQuery = 'SELECT ID, Name, Title__c, Custom_Name__c, Short_Name__c, Status__c, IsActive__c, Type__c FROM Regulation__c WHERE ID IN' + strConditionSignatureJunctionQuery;
                    System.Debug('<<<<<<< Signature Condition Junction Query : ' + strConditionSignatureJunctionQuery + ' >>>>>>>');
                    allRegulations = Database.query(strConditionSignatureJunctionQuery);
                    System.Debug('<<<<<<< All Regulations : ' + allRegulations.size() + ' >>>>>>>');
                    for(Integer i = 0; i < allRegulations.size() ;i++)
                    {
                        if(allRegulations[i].Type__c == 'Import Requirements'){allImportRequirements.add(allRegulations[i]);}
                        if(allRegulations[i].Type__c == 'Instruction for CBP Officers'){allInstructions.add(allRegulations[i]);}
                        if(allRegulations[i].Type__c == 'Additional Information'){allAdditionalInformation.add(allRegulations[i]);}
                    }
                }
                System.Debug('<<<<<<< All Instructions : ' + allInstructions.size() + ' >>>>>>>');
            }
            return null;
        }
        catch(Exception e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Something went wrong. Please re-try or contact your System Administrator.'));
            return null;
        }
    }
}