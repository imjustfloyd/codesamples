public class CARPOL_VS_AppFeePayment_extension {

public List <Authorizations__c> auList {get;set;}

public String paymentType{get;set;}
public String APHISUserFeeAccountNumber {get;set;}
public Boolean showUserFeeAccountNumber { get; set; }
public Boolean showMailInCheck {get;set;}
public Boolean showMoneyOrder {get;set;}
public ID applicationID;
Public Application__c app;
public Application__c application { get; set; }
public List<AC__c> lineItems { get; set; }
public List<Fee__c> itemFees { get; set; }
public Map<Signature__c, List<AC__c>> thumbPrintItems {get; set;}
public Map<Signature__c, Decimal> permitTotals {get; set;}
public Decimal totalAmount {get; set; }
public Boolean isEsigned {get; set;}
private ApexPages.StandardController appController;
integer StopQuery = 0;

    public CARPOL_VS_AppFeePayment_extension(ApexPages.StandardController controller) 
    {
        
        appController = controller;
        this.app = (Application__c)controller.getRecord();
        load();
        
        //instantiate variables
        itemFees = new List<Fee__c>();
        isEsigned = false;
        thumbPrintItems = new Map<Signature__c, List<AC__c>>();
        permitTotals = new Map<Signature__c, Decimal>();
        totalAmount = 0.0;
        
        applicationID = ApexPages.CurrentPage().getparameters().get('id');
        String strToken = ApexPages.CurrentPage().getparameters().get('token');
        showUserFeeAccountNumber = false;
        showMailInCheck = false;
        showMoneyOrder = false;
        
    }
    
    public PageReference computePaymentSummary()
    {
        if(applicationID != null)
        {
            lineItems = [SELECT ID, Name, Total_Fees__c, Authorization_Group_String__c, Transporter_Type__c, Item_Fee__c, RecordType.Name, Thumbprint__c 
            FROM AC__C WHERE Application_Number__c =: applicationID and Line_Item_Type_Hidden_Flag__c != 'Ingredient' ];//query line items for desired data
            if(lineItems == null || lineItems.size() == 0)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Line items associated with this Application.'));
            }
            else
            {
                for(AC__c o : lineItems)
                {
                  //this is necessary evil------------------------------------------------------------------------
                  List<Regulations_Association_Matrix__c> regDm = [Select id from Regulations_Association_Matrix__c where id in
                                                                    (Select Decision_Matrix__c from Related_Line_Decisions__c
                                                                    where Line_Item__c =: o.id)]; 
                     
                  o.Total_Fees__c = 0.00;                                              
                  List<Related_Fees__c> relatedFees = new List<Related_Fees__c>();
                  if(regDm.size() > 0)
                    relatedFees = [Select id, Amount__c, Decision_Matrix__c from Related_Fees__c where Decision_Matrix__r.id in : regDm];
                   
                  if(relatedFees.size() > 0)
                  {
                      for(Related_Fees__c fee: relatedFees) //add the related fees from the associated DM to the line item
                          o.Total_Fees__c += fee.Amount__c;
                  }
                  
                  //update o;    //update lineItems to recompute total fees;  //DMS, update the whole list, not individual line items
                }

                update lineItems; //update lineItems to recompute total fees;
                
                
                
                Set<String> authStrings = new Set<String>();
                Set<Signature__c> prints = new Set<Signature__c>(); //prevents duplicates
                Map<id, Signature__c> thumbprintsData = new Map<id, Signature__c>(); //This map will be used to retrieve the correct thumbprint
                for(AC__c item: lineItems)
                {
                    prints.add([Select Id, Name, Fee__c, Fee_Amount__c From Signature__c Where id=: item.Thumbprint__c]);
                    authStrings.add(item.Authorization_Group_String__c); //the grouping criteria for the permits
                }
                
                for(Signature__c print: prints) //store the thumbprints based on id from the UNIQUE set
                    thumbprintsData.put(print.id, print);
                
                for (String auth: authStrings)
                {
                    List<AC__c> tempItems = new List<AC__c>();
                    Signature__c tempSig = new Signature__c();
                    Decimal tempFees = 0.0;
                    for(AC__c item: lineItems)
                    {
                        if(item.Authorization_Group_String__c == auth)
                        {
                            tempItems.add(item);
                            tempSig = thumbprintsData.get(item.Thumbprint__c); //fetches the full Thumbprint
                            tempFees += item.Total_Fees__c;
                        }
                    }
                    thumbPrintItems.put(tempSig, tempItems); //put the thumbprint and the list in the Map
                    tempFees += tempSig.Fee_Amount__c; //adding processing fee
                    tempFees.setScale(2);
                    System.debug(tempFees);
                    permitTotals.put(tempSig, tempFees);
                    
                    //add the thumbprint fees
                    
                    // Fee__c n = [SELECT ID, Name, Fee_Amount__c, Fee_Code__c FROM Fee__c WHERE ID =: sig.Fee__c.ID LIMIT 1];
                    totalAmount += tempFees;
                    // itemFees.add(n);
                }

                totalAmount = totalAmount.setScale(2);
                
                if(totalAmount == 0)
                    return processAuthorization();
            }
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No valid Application ID found.'));
        }
        
        return null;
    }
    
    public void load()
    {
      auList = new List<Authorizations__c>([SELECT Id, Name, Application__c, Fee_Amount__c, Authorization_Type__c,
      (SELECT Id, Name, Quantity__c, Product_Category__c, Intended_Use__c FROM Veterinary_Services_VS__r)
      FROM Authorizations__c
      WHERE Application__c = :app.Id AND Fee_Amount__c <> null]);
    System.Debug('<<<<<<< Total VS Products : ' + auList.size() + ' >>>>>>>');
    }
    
    public PageReference processAuthorization()
    {
        PageReference appProcessing = new PageReference('/apex/UpdateApplication?id=' + applicationID);
        appProcessing.setRedirect(true);
        return appProcessing;
    }
    
    public PageReference processPayGov()
    {
        if(paymentType == 'Electronically (pay.gov)')
        {
            try
            {
                CARPOL_ProcessPaymentPayGov temp = new CARPOL_ProcessPaymentPayGov();
                // if(totalAmount != 0)
                // {
                    if(applicationID != null)
                    //if(tran.ID != null)
                    {
                        String strToken = temp.startOnlineCollection(String.valueOf(totalAmount), applicationID);
                        System.Debug('<<<<<<< VF Page strToken : ' + strToken + ' >>>>>>>');
                        if(strToken != 'NoToken' && strToken != 'Error')
                        {
                            //Create Transaction instance for the process and Pay.gov related info
                            Transaction__c tran = new Transaction__c();
                            tran.Application__c = applicationID;
                            tran.Transaction_Amount__c = totalAmount;
                            tran.Transaction_Date_Time__c = System.NOW();
                            tran.Payment_Type__c = 'CC';
                            tran.Pay_Gov_Token__c = strToken;
                            insert tran; //insert this data into the application
                            
                            PageReference payGovRedirect = new PageReference('https://qa.pay.gov/tcsonline/payment.do?token=' + strToken + '&tcsAppID=TCSAPHISEFILE');
                            payGovRedirect.setRedirect(true);
                            //return strToken;
                            return payGovRedirect;
                        }
                        //if token is not generated
                        else
                        {
                            PageReference payGovRedirectSomethingWentWrong = new PageReference('/apex/CARPOL_SomethingWentWrong');
                            payGovRedirectSomethingWentWrong.setRedirect(true);
                            return payGovRedirectSomethingWentWrong;
                        }
                    }
                    else
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Transaction was not created.'));
                        return null;
                    }

            }
            catch(Exception e)
            {
                System.Debug('<<<<<<< Exception : ' + e + '>>>>>>>');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception : ' + e));
                return null;       
            }
        }
        else if(paymentType == 'Mail-in Check/Money Order')
        {
            return null;
        }
        else if(paymentType == 'APHIS User Account')
        {
            return null;
        }
        else
            return null;
        
    }
    
    public List<SelectOption> getPaymentTypeOptions() 
    {
    
        List<SelectOption> paymentTypes = new List<SelectOption>();
        paymentTypes.add(new SelectOption('--Select--', '--Select--'));
        paymentTypes.add(new SelectOption('Electronically (pay.gov)', 'Electronically (pay.gov)'));
        // paymentTypes.add(new SelectOption('Mail-in Check/Money Order', 'Mail-in Check/Money Order'));
        paymentTypes.add(new SelectOption('APHIS User Account', 'APHIS User Account'));
        
        // Profile userProf = [SELECT ID, Name FROM Profile WHERE ID = :UserInfo.getProfileId() LIMIT 1];
        // if(userProf.Name != 'APHIS Applicant')
        // {
        //     paymentTypes.add(new SelectOption('Charge No Fee', 'Charge No Fee'));
        //     paymentTypes.add(new SelectOption('Applicant Did Not Provide Payment', 'Applicant Did Not Provide Payment'));
        // }
        return paymentTypes;
    }
    
    public PageReference doCancel()
    {
        return appController.cancel();
    }
    
    public PageReference paymentTypeChangeAction()
    {
        if(paymentType == 'APHIS User Account')
        {
            showUserFeeAccountNumber = true;
        }
        else if(paymentType != 'APHIS User Account')
        {
            showUserFeeAccountNumber = false;
        }
            
        if(paymentType == 'Mail-in Check/Money Order')
        {
            showMailInCheck = true;
        }
        else if (paymentType != 'Mail-in Check/Money Order')
        {
            showMailInCheck = false;
        }

        
        return null;
    }
    public PageReference savePaymentType()
    {
        try
        {             
            if(paymentType != '--Select--')
            {
                application.Payment_Type__c = paymentType;
                System.Debug('<<<<<<< APHIS_User_Fee_Account_Number__c : ' + application.APHIS_User_Fee_Account_Number__c + ' >>>>>>>');
                update(application);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Payement Type Saved Successfuly.'));
                
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select a Payment Type.'));
            }
        } 
        catch(System.DMLException e)
        {
            System.Debug('<<<<<<< Exception : ' + e + ' >>>>>>>');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Something went wrong. Please re-try or contact your system administraor.'));
        }

    return null;
    }

}