@isTest(seealldata=true)
public class CARPOL_PreviewAuthorization_Test {
      @IsTest static void CARPOL_PreviewAuthorization_Test() {
          String AccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Accounts').getRecordTypeId();

          CARPOL_AC_TestDataManager testData = new CARPOL_AC_TestDataManager();
          testData.insertcustomsettings();
          Account objacct = testData.newAccount(AccountRecordTypeId); 
          Contact objcont = testData.newcontact();
          Program_Line_Item_Pathway__c plip = testData.newCaninePathway();
          Application__c objapp = testData.newapplication();
          AC__c li = TestData.newlineitem('Personal Use', objapp);
          Communication_Manager__c objcm = testData.newCommunicationmanager('Permit');
          Signature__c objTP = testData.newThumbprint();
          Authorizations__c objauth = testData.newAuth(objapp.id);
          li.Authorization__c = objauth.id;
          update li;
          objauth.Authorization_Type__c = 'Permit';
          objauth.Template__c = objcm.id;
          objauth.Date_Issued__c = Date.today();
          objauth.Application__c = objapp.Id;
          objauth.Thumbprint__c = objTP.Id;
          update objauth;
          Workflow_Task__c objwft = testData.newworkflowtask('Test', objauth, 'Pending');

          
              Test.startTest(); 

              PageReference pageRef = Page.CARPOL_PreviewAuthorization;
              Test.setCurrentPage(pageRef);
              ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objauth);
              ApexPages.currentPage().getParameters().put('id',objauth.id);
              ApexPages.currentPage().getParameters().put('tId',objwft.id);
              ApexPages.currentPage().getParameters().put('wfid',objwft.id);
              

              CARPOL_PreviewAuthorization extclass = new CARPOL_PreviewAuthorization(sc);
              extclass.populateTemplate(); 
              extclass.cancel(); 
              extclass.attachLetter();
              extclass.attachDraftLetter();

              extclass.saveDraft();
              extclass.previewTemplate();
              extclass.reviewerComments = '';
              system.assert(extclass != null);
          Test.stopTest();    

      }

}