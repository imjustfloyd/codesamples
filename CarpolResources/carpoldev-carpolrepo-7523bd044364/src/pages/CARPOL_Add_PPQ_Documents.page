<apex:page sidebar="false" standardController="AC__c" extensions="CARPOL_PPQ_Docs" showHeader="false">
<!-- jQuery  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <!-- jQuery UI CSS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script>  
        // Semicolon (;) to ensure closing of earlier scripting
        // Encapsulation
        // $ is assigned to jQuery
        (function ($) {
            // DOM Ready
            $(function() {
                // Binding a click event
                // From jQuery v.1.7.0 use .on() instead of .bind()
                $('#tdUploadFile').on('click', function(e) {
                    // Prevents the default action to be triggered. 
                    e.preventDefault();
                    // Triggering bPopup when click event is fired
                    $('#element_to_pop_up').bPopup({
                        speed: 650,
                        transition: 'slideIn',
                        transitionClose: 'slideBack'
                    });
                });
                
                $('#trShowDetails').on('click', function(e) {
                    // Prevents the default action to be triggered. 
                    e.preventDefault();
                    // Triggering bPopup when click event is fired
                    $('#element_to_pop_up').bPopup({
                        speed: 650,
                        transition: 'slideIn',
                        transitionClose: 'slideBack'
                    });
                });
            });
        })(jQuery);
    </script>
    <script src="{!$Resource.bPopupJS}"></script>
    <apex:outputPanel id="messages">
        <div align="center">
            <apex:pageMessages />
        </div>
    </apex:outputPanel>
    <apex:form >
        <apex:pageBlock mode="edit">
            <apex:pageBlockSection columns="1">
                <apex:outputPanel rendered="{!renderRemainingDocuments}">
                    <apex:outputLabel style="color:#006600; font-size:2.5em;"><br/>Please upload the following documents to complete your line item {!lineItem.Name}</apex:outputLabel><br/>
                    <apex:outputLabel style="color:#737373; font-size:1.0em;">You can upload a file for each required document(s) or combine them and upload one single file.</apex:outputLabel>                        
                </apex:outputPanel>
                <apex:outputPanel rendered="{!NOT(renderRemainingDocuments)}">
                    <apex:outputLabel style="color:#006600; font-size:2.5em;">HURRAY !! No more documents to be uploaded for {!lineItem.Name}. You are all done.</apex:outputLabel>
                </apex:outputPanel>
                <br/>
                
                <apex:outputPanel id="documentList">
                    <br/>
                    <table>
                        <apex:repeat value="{!mapRequiredDocuments}" var="reqDoc">     
                            <tr id="trShowDetails">
                                <td width="24px"><apex:image url="{!$Resource.Active_Check}" rendered="{!mapRequiredDocuments[reqDoc].isUploaded}"  width="20" height="20"/></td>
                                <td>
                                    <apex:outputLink rendered="{!IF(mapRequiredDocuments[reqDoc].docURL != null, true, false)}" value="{!mapRequiredDocuments[reqDoc].docURL}" title="Click to view the document." target="_blank">
                                        <apex:outputLabel style="color:green; font-size:1.5em;cursor:pointer;">{!reqDoc}</apex:outputLabel>   
                                    </apex:outputLink>
                                    <apex:outputLabel style="color:#737373; font-size:1.5em;" rendered="{!IF(mapRequiredDocuments[reqDoc].docURL == null, true, false)}">{!reqDoc}</apex:outputLabel><br/>
                                </td>
                                <td>
                                    <apex:outputLabel rendered="{!IF(mapRequiredDocuments[reqDoc].docURL != null, true, false)}" style="color:green; font-size:1.5em;cursor:pointer;" title="Click to manage the document.">{!reqDoc}</apex:outputLabel>   
                                    <apex:outputLabel style="color:#737373; font-size:1.5em;" rendered="{!IF(mapRequiredDocuments[reqDoc].docURL == null, true, false)}">{!reqDoc}</apex:outputLabel><br/>
                                </td>
                            </tr>
                        </apex:repeat>
                    </table>
                    <br/><br/>
                </apex:outputPanel>
            </apex:pageBlockSection>
            <br/>
            <div>
                <table>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td id="tdUploadFile">
                            <apex:commandButton id="btn1" style="cursor:pointer;background:#006600;color:white;width:130px;height:35px;font-size:1.2em;font-style:verdana" rendered="{!renderRemainingDocuments}" disabled="{!NOT(renderRemainingDocuments)}" title="Add Documents" value="Add Documents"/>
                        </td>
                        <td>
                            <apex:commandButton style="cursor:pointer;background:#006600;color:white;width:80px;height:35px;font-size:1.2em;font-style:verdana" title="Close" value="Close" onclick="window.top.close()"/>
                        </td>
                    </tr>
                </table>
            </div>
            <br/><br/>
        </apex:pageBlock>
    </apex:form>
    
    <div id="element_to_pop_up" align="center">
        <a class="b-close">x</a>
        <br/>
        <!-- <apex:outputPanel id="popUpNoDocs" rendered="{!NOT(renderRemainingDocuments)}">
            <apex:outputLabel style="color:#006600; font-size:1.5em;">HURRAY !! <br/>No more documents to be uploaded. You are all done.</apex:outputLabel>
            <br/><br/>
            <apex:image url="http://33.media.tumblr.com/tumblr_m4p3fi8TuR1qbxlyfo1_r4_500.gif" width="200px" height="200px"/>
        </apex:outputPanel> -->
        
        <apex:outputPanel id="popUp" rendered="{!renderRemainingDocuments}">
            <apex:outputLabel style="color:#737373; font-size:1.1em;">
                Please select all the documents which are inlcuded in the file you have selected.
            </apex:outputLabel>
            <br/><br/>
            <apex:form >
                <apex:outputLabel style="color:#006600; font-size:1.2em;" title="Output" value="Upload File" for="file" />
                <apex:inputFile title="Select File" style="font-size:1.1em;" value="{!uploadedFile.body}" fileName="{!uploadedFile.Name}" id="file"/>
                <br/><br/>
                <apex:selectCheckboxes style="color:#737373; font-size:1.5em;" layout="pageDirection" title="Documents in selected file" value="{!selectedDocuments}">
                    <apex:selectOptions value="{!remainingDocuments}"/>
                </apex:selectCheckboxes>
                <table align="center">
                    <tr align="center">
                        <td>
                            <div id="loading">
                                <apex:image value="{!$Resource.USDAAPHISLoading}" width="50" height="50" /><br/><br/>
                            </div>
                            <apex:actionStatus id="actStatusId"><apex:facet name="start" ><img src="/img/loading.gif" /></apex:facet></apex:actionStatus>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <apex:commandButton id="uploadFile" style="cursor:pointer;background:#006600;color:white;width:100px;height:35px;font-size:1.2em;font-style:verdana" status="actStatusId" title="Upload File" onclick="startUpload();" action="{!uploadFile}" value="Upload File"/>
                            <apex:commandButton id="cancel" style="cursor:pointer;background:#006600;color:white;width:100px;height:35px;font-size:1.2em;font-style:verdana" status="actStatusId" onclick="$('#element_to_pop_up').bPopup().close(); return false;" title="Cancel" value="Cancel" />
                        </td>
                    </tr>
                </table>
        </apex:form>
        </apex:outputPanel>
    </div>
    <script>
        function startUpload()
        {
            document.getElementById('loading').style.display = "block";
        }
    </script>
    
    <style>
         #element_to_pop_up { 
            background-color:#fff;
            border-radius:15px;
            color:#000;
            display:none; 
            padding:20px;
            min-width:400px;
            min-height: 180px;
        }
        .b-close{
            cursor:pointer;
            position:absolute;
            right:10px;
            top:5px;
            font-size: large;
        }
        
        #loading { 
            align: center;
            vertical-align: middle;
            display: none;
        }
    </style>
</apex:page>