<apex:page showHeader="false" renderAs="pdf"  standardController="Authorizations__c">
<apex:stylesheet value="{!$Resource.CARPOL_PDFStyles}" />
  <c:CARPOL_PDFHeaderFooterStyle type="header" position="center">
        <div style="padding: 0px;">
            <c:CARPOL_PDFHeader />
        </div>
    </c:CARPOL_PDFHeaderFooterStyle>
    <div align="right">
         <apex:outputText value="{0,date,MMMM dd, yyyy}">
        <apex:param value="{!TODAY()}" />
        </apex:outputText>   
     </div><br/><br/>
      {!Authorizations__c.Application__r.Applicant_Name__r.MailingStreet}<br/>
          {!Authorizations__c.Application__r.Applicant_Name__r.MailingCity}<br/>
          {!Authorizations__c.Application__r.Applicant_Name__r.MailingState}<br/>
          {!Authorizations__c.Application__r.Applicant_Name__r.MailingPostalCode} {!Authorizations__c.Application__r.Applicant_Name__r.MailingCountry}   
      <br/><br/>
          Dear {!Authorizations__c.Application__r.Applicant_Name__r.Name}<br/><br/>
        <p>Your notification request has been acknowledged and may be executed according to 7 CFR § 340.3(c). You are authorized to perform
        the movement of the regulated article between  
        &nbsp;<apex:outputText value="{0,date,MMMM dd, yyyy}">
        <apex:param value="{!Authorizations__c.Date_Issued__c}" />
        </apex:outputText>  and &nbsp;<apex:outputText value="{0,date,MMMM dd, yyyy}">
        <apex:param value="{!Authorizations__c.Expiry_Date__c}" />
        </apex:outputText>. 
        All activities related to this introduction (except for
        any monitoring periods) must be completed by the expiration date (i.e. all shipments must have arrived at their destination, plants
        harvested, and all remaining plants and plant parts are either destroyed or moved into contained facilities).</p>
        <p>In the event of any accidental and/or unauthorized release of the regulated article, contact BRS Compliance Evaluation and
        Enforcement Branch by phone (301) 851-3935, fax (301) 734-8910, or e-mail at <a href="mailto:BRSCompliance@aphis.usda.gov" target="_top">BRSCompliance@aphis.usda.gov</a></p>
        <p>If you have a general question regarding your notification, contact BRS Permits unit by phone (301) 851-3935 or
        e-mail <a href="mailto:BRSPermits@aphis.usda.gov" target="_top">BRSPermits@aphis.usda.gov</a></p>
        <p>***This acknowledgement letter must be provided to all cooperators.***</p>
        <p>***Important - Import Notifications Only***</p>
        <p>Imports may require a phytosanitary certificate from the country of origin, a phytosanitary certificate of re-exportation (e.g. Ships from
        South Africa to France, then France to the U.S.), a Plant Protection and Quarantine (PPQ) permit, and/or other certifying PPQ
        documents. Also, some interstate movement and release notifications may also be subject to PPQ domestic permit and/or quarantine
        requirements. Please call PPQ at (877) 770-5990 for additional assistance in regards to their requirements.</p>
        <p>***Planting Report Information - All Releases &amp; Movement / Release Notifications Only***</p>
        <p>APHIS requires that responsible parties submit a Planting Report no later than the 15th day of the month that follows the date the
        environmental release occurred (e.g., a planting any time April 1-30 must be reported by May 15). This report provides APHIS with
        additional details about the actual releases that have taken place under an acknowledged notification. The report must include: the
        Notification number; name of the regulated article; trial site location data (provide state, county, northwest GPS coordinate, and site
        identification number (if available); acreage of regulated article planted; and planting date for each location. For additional information
        on planting reports see the Notification Users Guide:<br/>
        <a href="http://www.aphis.usda.gov/biotechnology/downloads/notification_guidance_0311.pdf" target="_blank">http://www.aphis.usda.gov/biotechnology/downloads/notification_guidance_0311.pdf</a></p>
        <p>Additionally, the planting report should list any sites included in the original notification that will not be planted. If there are multiple
            planting dates, you may submit reports monthly no later than the 15th day of the month that follows planting to inform APHIS of any
            new plantings. Planting reports can combine information from multiple notifications; i.e., only a single report need be submitted that
            lists all the plantings for the previous calendar month. Reports need not be submitted when no planting occurs.</p>
         <p>***Final Field Test Reports - All Release &amp; Movement / Release Notifications Only***</p>
         <p>Field test reports must be submitted to APHIS within 6 months after termination of the field test. Field test reports shall include the
            APHIS reference number, methods of observation, resulting data, and analysis regarding all deleterious effects on plants, nontarget
            organisms, or the environment. (7 C.F.R. § 340.3(d)(4)).</p>
          <p>All environmental releases of regulated articles under notification require the submission of a field test report within six months of the
            termination of the field test. Because APHIS does not always know the actual termination date in advance, APHIS considers the field
            test report to be due no later than six months after the expiration of the notification</p>
          <p>The following information must be included in the field test report:</p>
             <ul>
              <li>APHIS Notification number</li>
              <li>Location Name</li>
              <li>County</li>
              <li>State</li>
              <li>Indicate if any of the planted material was destroyed before harvest<br/>
                    If so, provide the pre-harvest destruction completion date and describe how the pre-harvested material was destroyed</li>
              <li>Indicate if any of the planted material was harvested and if so provide the harvest completion date. Describe how the harvested
                    material was terminated</li>
              <li>If the material was terminated in the field and not removed from the field, provide the date the field test was completely
                terminated and describe the method of termination</li>
              <li>If material was removed from the field and terminated off site describe how it was disposed and provide the date of off site
                destruction</li>
              <li>If material was removed from the field and placed in storage, provide the amount of material that was stored and provide a
                description of the storage location</li>
              <li>Describe any other disposition Methods that may be applicable</li>
              <li>Describe any deleterious effects on plants, non target organisms, or the environment</li>
              <li>Describe methods of observations and resulting data and analyses</li>
              <li>Indicate if you have submitted any of the following:<br/>
                  <ol>
                      <li>A report on the accidental or unauthorized release of the regulated article;</li>
                      <li>A report that characteristics of the permitted species are substantially different from those listed in the application; or</li>
                      <li>A report of any unusual occurrence.</li>
                   </ol>
              </li>
            </ul>  
         <p>For additional guidance on these requirements, see the BRS User’s Guide for Notifications at<br/>
         <a href="http://www.aphis.usda.gov/biotechnology/downloads/notification_guidance_0311.pdf" target="_blank">http://www.aphis.usda.gov/biotechnology/downloads/notification_guidance_0311.pdf</a></p>
         <p>Please submit all planting and final field test reports via ePermits using the link under “My Reports and Notices.”
            A link to instructions for submitting via ePermits is located here:<br/>
          <a href="https://epermits.aphis.usda.gov/epermits/xml_schema/BRS_Reports_and_Notices_User_Guide.pdf" target="_blank">https://epermits.aphis.usda.gov/epermits/xml_schema/BRS_Reports_and_Notices_User_Guide.pdf</a></p>
          <p>Other options are to submit reports via email or paper, however, we strongly encourage submission via ePermits. If submitting using
            any other method then CBI and CBI-deleted or non-CBI copies should be submitted via:<br/><br/>
            BRS E-mail:<br/>
           <a href="mailto:BRSCompliance@aphis.usda.gov" target="_top">BRSCompliance@aphis.usda.gov</a><br/><br/>
           BRS Mail:<br/>
           Animal and Plant Health Inspection Service (APHIS)<br/>
           Biotechnology Regulatory Services (BRS)<br/>
           Regulatory Operations Program<br/>
           4700 River Rd. Unit 91<br/>
           Riverdale, MD 20737<br/></p>
         <p>You must comply with the performance standards as stated in 7 CFR 340.3(c) and transcribed below:</p>
             <ol>
                  <li>If the plants or plant materials are shipped, they must be shipped in such a way that the viable plant material is unlikely to be
                    disseminated while in transit and must be maintained at the destination facility in such a way that there is no release into the
                    environment.</li>
                  <li>When the introduction is an environmental release, the regulated article must be planted in such a way that they are not
                    inadvertently mixed with non-regulated plant materials of any species which are not part of the environmental release.</li>
                  <li>The plants and plant parts must be maintained in such a way that the identity of all material is known while it is in use, and the
                    plant parts must be contained or devitalized when no longer in use.</li>
                  <li>There must be no viable vector agent associated with the regulated article.</li>
                  <li>The field trial must be conducted such that:
                      <ol type="i">
                      <li>The regulated article will not persist in the environment, and</li>
                      <li>No offspring can be produced that could persist in the environment</li>
                    </ol>
                  </li>
                  <li>Upon termination of the field test:
                      <ol type="i">
                      <li>No viable material shall remain which is likely to volunteer in subsequent seasons, or</li>
                      <li>Volunteers shall be managed to prevent persistence in the environment.</li>
                    </ol>
                  </li>
            </ol>
          <p>APHIS recommends the adoption of best management practices (BMPs) to delay or mitigate evolution of herbicide resistance, to the
                extent possible, while implementing design protocols for regulated authorized releases of genetically engineered crops. For
                information and resources on this topic, please see &nbsp;<a href="http://wssa.net/weed/resistance/" target="_blank">http://wssa.net/weed/resistance/.</a> 
                In particular, examples of BMPs are outlined by Norsworthy et al. in: <a href="http://www.wssajournals.org/doi/pdf/10.1614/WS-D-11-00155.1" target="_blank">Reducing the Risks of Herbicide Resistance: Best Management Practices and Recommendations.</a>
                 (Weed Sci. 2012,Special Issue: 31-62). See <a href="http://www.wssajournals.org/doi/pdf/10.1614/WS-D-11-00155.1" target="_blank">http://www.wssajournals.org/doi/pdf/10.1614/WS-D-11-00155.1</a></p>
           <p>To ensure compliance with performance standards, you or any of your cooperators who will be involved in handling the regulated
                article must be prepared with a written description of the methods to be employed to meet each performance standard. Although not
                requirements, all packages should be clearly labeled as to content, and the notification number should be prominently displayed on the
                package. Regulated articles introduced under notification are subject to the performance standards in the regulations even after the or
                expiration date of the notification.</p><br/><br/><br/><br/>
            <p>A copy of this letter of acknowledgement will be sent to the relevant State Regulatory Officials.</p>
            <p>Sincerely,</p>
            <p>Permits and Program Services Branch<br/>
            Regulatory Operations Programs<br/>
            Biotechnology Regulatory Services<br/></p>
            <p>cc:</p>
            <p>Robert Mungari, Schenectady, NY; Margaret Kelly, Albany, NY; Ethan Angell, Hoosick Falls, NY; Jan Morawski, , NY</p>

</apex:page>