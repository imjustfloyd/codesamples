$(document).ready(function(){
    //{!REQUIRESCRIPT("/soap/ajax/19.0/connection.js")}
    var pagePath = top.location.pathname;
    
    //we get the path of the page so that we can run functions depending on the page that User is on. 
    var lineItemPagePath = "/carpol_lineitempageforapplicant" || "/apex/carpol_lineitempageforapplicant";
    //this is the id of the list item that we want to move the button inside of for the line item page for applicant page.  
    var lineItemButtonIdName="li#returnToLineItem";
    // the switch below is using the page path to determine which button to move into the sidebar. 
    var uniLinePagePath = "/carpol_uni_lineItem" || "/apex/carpol_uni_lineitem";
    var appDetailPagePath = "/portal_application_detail" || "/apex/portal_application_detail";
    var brokerRequestPath = "/apex/brokerPrepareForm" || "/brokerPrepareForm" ;
    var constructPagePath = "/portal_construct_detail" || "/apex/portal_construct_detail";
    var brsWaitingPagePath = "/carpol_brs_waitingpage" || "/apex/carpol_brs_waitingpage";
    var appAttachEditPagePath = "/portal_applicantattachment_edit" || "/apex/portal_applicantattachment_edit";
    var locationEditPath = "/portal_location_edit" || "/apex/portal_location_edit";
    var externalWizardPath = "/CARPOL_HOMEPAGE" || "/apex/CARPOL_HOMEPAGE";
    console.log("Your Current Page Path is: " + pagePath);
    
    var popApplicantCurrentSec = '';
    var popApplicantCurrentSecName = '';
    
    
    switch (pagePath){
        case lineItemPagePath:
                moveButtonToActionModule(lineItemButtonIdName);
                console.log("Moving the Edit return to line item button to the action module in the sidebar");
            break;
        case uniLinePagePath:
                    testUniLineItem(); 
                    changeDocTitle(pagePath);
                    // collapseIcon();
                    // validateForm();;
                    // addPickListClass();
            break;
        case appDetailPagePath: 
                    getRegulatedArticlePackageId();

            break;
        case brokerRequestPath: 
                    disableClick(".brokerRequest");
            break;
        case constructPagePath:
                    addSpacer(".constructPanel");
            break; 
        case brsWaitingPagePath: 
                    disableButton();
            break;
        case appAttachEditPagePath: 
                    removeBootstrapFromPicklist();
                    addTitleToPicklistOptions();
                    addPickListClass();
            break;
        case locationEditPath: 
                    removeBootstrapFromPicklist();
                    addTitleToPicklistOptions();
                    addPickListClass();
            break;
        case externalWizardPath:
                    removeSectionBorder('div#noSectionLine.form-section');
            break;
        

    }
    try{
        disableTextBox();
    } catch(e){
        console.log(e);
        
    }
});

function setActivePage(e){
    $(e).addClass('active');
    $(e+'a').addClass('active');
}

function removeSectionBorder(e){
    var sectionName = j$(e);
    sectionName.attr('style', 'border:none;');
}

function validateForm(){
$('form .lineItemForm.requiredInput input').blur(function()
    {
        var reqField = $('em.required'); 
        var reqPanel = reqField.attr('id');
        if( $(this).val().length < 1 ) {
            validateRequiredPanels();
        }
    });
}

//if em has class required then validate form
function checkForRequiredFields(){
    var reqPanel = $('em.required');
    var vals = [];
    // get required panels from em element id. 
    $(reqPanel).each(function(){
        vals.push($(this).attr('id'));
    });
    //remove duplicates 
    var uniqueVals = vals.filter(function(elem, pos) {
        return vals.indexOf(elem) == pos;
    });
   
    var collapsePanel =('div.panel-collapse');
    var headerPanel = ('div.panel-heading');
    var panelTitle = $('div.panel-collapse').attr('title');
    var title = "";
    $.each( uniqueVals, function( key, value ) {
        title = value;
        if(title == panelTitle){
            $(headerPanel).attr('style','background-color:red;');
            $(collapsePanel).addClass('in');
        }
    });
}

/*function that retrieves panel header by title*/
function getErrPanel(e){
    var panel = $('div.panel-header');
    var panelTitle = panel.attr('title');
    //if panel title and the parameter return true then change the panel header background red. 
    if(panelTitle === e){
        panel.addClass('error');
        panel.attr('aria-expanded','true');
        console.log(panel.attr('class'));
    }
}


// function collapseIcon(){
//     $('.collapse').on('shown.bs.collapse', function(){
//         $(this).parent().find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
//     }).on('hidden.bs.collapse', function(){
//         $(this).parent().find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
//     });
//     var liTags = $('li');
//     if(liTags.parent().is("ul.noindent")){
//         liTags.unwrap();
//     }else{
//         liTags.wrap("");
//     }
// }

function closeMessageWindow(){
    $('.message.confirmM3').hide();
}

function disableTextBox(){
    $('.input-disabled').attr('readonly', true);
}

function changeDocTitle(e){
   $('title').html(e).replace(/\_/g, ' ');
   $('title').html(e).replace(/\//g, ' ');
   $("title").html(e);
}

function intializeToolTips(){
    //initialize tooltips when the document is in ready state . 
    $('[data-toggle="tooltip"]').tooltip(); 
}

// function removeBootstrapFromPicklist(){
//      //remove bootstrap form-control class from the picklist arrow images
//     $('.picklistArrowRight').removeClass('form-control');
//     $('.picklistArrowLeft').removeClass('form-control');
// }

function  addTitleToPicklistOptions(){
     $(".multiSelectPicklistTable select option").each(function(){
        $(this).attr("title", $(this).text());
        $(this).attr("data-toggle", "tooltip");
        });
}
function disableButton(){
    $("input.btnDisabled").addClass('disableClick').removeClass('btnDisabled');
    $("input.disableClick").addClass('disable').removeClass('customButtonGreen');
}
function addPickListClass(){$('jQuery selector').css({"css property name":"css property value"});
    var bootstrapStyle = $("tr.multiSelectPicklistRow td select.form-control");
    $('select.form-control').attr('style', 'width:180px');
    bootstrapStyle.attr("style","width:180px !important;");
    bootstrapStyle.addClass('ui-resizable-se');
    bootstrapStyle.addClass('pickListBox');
    
}
// function removeBtnClass(){
//         //This functionality removes the btn class so that we can fully customize the apex:commandButton directive. Since it adds the btn class when the document is ready.
//     // we can reuse this in the bottom of the output panels when the body rerenders on proceed with application. 
//         $('.customButton.customButtonBlue').removeClass('customButton customButtonBlue').addClass('btn btn-default background-blue');
//         $('.customButton.customButtonGreen').removeClass('customButton customButtonGreen').addClass('btn btn-default background-green');
    
// }

function moveButtonToActionModule(e){
    $('span#showButton').appendTo('li#returnToLineItem').show();
}
function getRegulatedArticlePackageId(){
    var id = $("[title='regulated article package id']").attr('class');
    console.log(id);
    if(id !== null){
        setRegulatedArticlePackageId(id);
    }
}
function setRegulatedArticlePackageId(e){
    var newurl ="carpol_uni_lineitem?appid=a0535000000PoHxAAK&regArticlePackageId=" + e;
    $('.btnLink').attr('href', newurl);
}
function testUniLineItem()
{
 var applicationId = $("a.retapp").attr('id');
            if (applicationId !== null){
                console.log(applicationId + "is undefined");
            }
            
}
//This is a function that can be used to disable a <a href> rendering it read-only. 
function disableClick(e){
    //link should be the jquery selector . Please use either class or id. Ex $('.myClassName'); or $('#myClassName'); 
    var linkToDisable = $(e).addClass('disableClick');
    console.log(linkToDisable);
}

function addSpacer(e){
    var addSpaceTo = $(e).addClass('spacer');
}

//These two functions work with the openLookupFunction on the page
function matchLookupElement(element, textbox){
    var result = false;
    //console.log('element.name>>>'+element.name);
    for(var i=0;i<element.length;i++){
        
              var valid = document.getElementById(element[i].id);
              var validName = valid.getAttribute('name');
              if(textbox == validName){
                  result = true;
                  break;
              }
    }
    return result; 
}


function customPickerReturn(formtag, lkid, textbox, recordID, recordName, jsonmap ,flag){
    var section;
    var importermatch = matchLookupElement(document.getElementsByClassName('Importer_Last_Name__c'), textbox);
    var exportermatch = matchLookupElement(document.getElementsByClassName('Exporter_Last_Name__c'), textbox);
    var delivrecipmatch = matchLookupElement(document.getElementsByClassName('DeliveryRecipient_Last_Name__c'), textbox);
    var handCarryrecipmatch = matchLookupElement(document.getElementsByClassName('Hand_Carrier_Last_Name__c'), textbox);
    
    //(List<String>)System.JSON.deserialize(json_string, List<String>.class)
    var arr_from_json = JSON.parse( jsonmap );
    
    console.log('The map is >>>'+arr_from_json);
    
    
    //var simmap = JSON.deserialize(jsonmap);
    console.log('This is inside >>> '+jsonmap);
    console.log('<>>importermatch>>>>'+importermatch);
    console.log('<>>exportermatch>>>>'+exportermatch);
    console.log('<>>delivrecipmatch>>>>'+delivrecipmatch);
    console.log('<>>handCarryrecipmatch>>>>'+handCarryrecipmatch);
    
   if (importermatch){ 
       section = 'Importer';
       
   }
    if (exportermatch){ 
        section = 'Exporter';
        var sec3 = arr_from_json['Exporter Information'];
        console.log('sec2 is >>>>'+sec3);
        section = sec3;
    }
    if (delivrecipmatch){ 
        //Program_Line_Item_Section__c progsec = [SELECT ID,Name,Section_Type__c FROM Program_Line_Item_Section__c WHERE Section_Type__c = 'Delivery Recipient Information'];
        
        
        // //var progsec = sforce.connection.query("SELECT ID,Name,Section_Type__c FROM Program_Line_Item_Section__c Limit 1");
        //   try{
        //   var result = sforce.connection.queryAll("Select Name, Id from User");
        //   console.log('progsec>>>>'+result);
              
        //   }
        //   catch(error){
        //       console.log('Eror is >'+error);
        //   }
        //     var records = result.getArray("records");

        // //records = progsec.getArray("records");
        
       
        // console.log('records >>>'+records);
        //console.log('records>>>>'+records);
          /*for (var i=0; i< records.length; i++) {
            var record = records[i];
            alert("Contact name"+ record.Name + "Contact Id -- " + record.Id);
          }
        
        */
        
       
       //section = 'Importer';
        
        section = 'Shipper';
         var sec2 = arr_from_json['Delivery Recipient Information'];
        console.log('sec2 is >>>>'+sec2);
        section = sec2;
        
    }
    if (handCarryrecipmatch) section = 'Hand Carrier';
     
     //section = sectName;
    popLastNameLookup(section, recordID);
}

function populateApplicant(sectNumber, sectName){
    // popApplicantCurrentSecName = sectName;
    // popApplicantCurrentSec = sectNumber;
    // console.log('popApplicantCurrentSecName>>>'+popApplicantCurrentSecName);
    popApplicant(sectName, sectNumber);
}
